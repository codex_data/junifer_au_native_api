#!/usr/bin/env sh

openapi-generator generate -i ./junifer-api.yaml -g javascript  -o ./ | tee generate-js.log
