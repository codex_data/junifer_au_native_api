"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody = _interopRequireDefault(require("../model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody"));

var _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse = _interopRequireDefault(require("../model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200"));

var _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse2 = _interopRequireDefault(require("../model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse400"));

var _AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody = _interopRequireDefault(require("../model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody"));

var _AusEnrolCustomerPostAuCustomersEnrolcustomerResponse = _interopRequireDefault(require("../model/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200"));

var _AusEnrolCustomerPostAuCustomersEnrolcustomerResponse2 = _interopRequireDefault(require("../model/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse400"));

var _AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody = _interopRequireDefault(require("../model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody"));

var _AusLifeSupportSensitiveLoadPostAuCustomersLifesupportResponse = _interopRequireDefault(require("../model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportResponse400"));

var _AusSiteAccessPostAuCustomersSiteaccessRequestBody = _interopRequireDefault(require("../model/AusSiteAccessPostAuCustomersSiteaccessRequestBody"));

var _AusSiteAccessPostAuCustomersSiteaccessResponse = _interopRequireDefault(require("../model/AusSiteAccessPostAuCustomersSiteaccessResponse400"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* CustomersAustralia service.
* @module api/CustomersAustraliaApi
* @version 1.61.1
*/
var CustomersAustraliaApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CustomersAustraliaApi. 
  * @alias module:api/CustomersAustraliaApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CustomersAustraliaApi(apiClient) {
    _classCallCheck(this, CustomersAustraliaApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAuCustomersEnrolBusinessCustomerPost operation.
   * @callback module:api/CustomersAustraliaApi~juniferAuCustomersEnrolBusinessCustomerPostCallback
   * @param {String} error Error message, if any.
   * @param {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Enrol business customer
   * Enrols a business customer for electricity and/or gas product. This is an Australia specific API.
   * @param {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody} requestBody Request body
   * @param {module:api/CustomersAustraliaApi~juniferAuCustomersEnrolBusinessCustomerPostCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200}
   */


  _createClass(CustomersAustraliaApi, [{
    key: "juniferAuCustomersEnrolBusinessCustomerPost",
    value: function juniferAuCustomersEnrolBusinessCustomerPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuCustomersEnrolBusinessCustomerPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse["default"];
      return this.apiClient.callApi('/junifer/au/customers/enrolBusinessCustomer', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuCustomersEnrolCustomerPost operation.
     * @callback module:api/CustomersAustraliaApi~juniferAuCustomersEnrolCustomerPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Enrol customer
     * Enrols customer for electricity and/or gas product. This is an Australia specific API.
     * @param {module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody} requestBody Request body
     * @param {module:api/CustomersAustraliaApi~juniferAuCustomersEnrolCustomerPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200}
     */

  }, {
    key: "juniferAuCustomersEnrolCustomerPost",
    value: function juniferAuCustomersEnrolCustomerPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuCustomersEnrolCustomerPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _AusEnrolCustomerPostAuCustomersEnrolcustomerResponse["default"];
      return this.apiClient.callApi('/junifer/au/customers/enrolCustomer', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuCustomersLifeSupportPost operation.
     * @callback module:api/CustomersAustraliaApi~juniferAuCustomersLifeSupportPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add life support and sensitive load details
     * Adds Life Support and Sensitive Load details to indicate economic, health or safety issues with loss of supply of the connection point. This is an Australia specific API.
     * @param {module:model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody} requestBody Request body
     * @param {module:api/CustomersAustraliaApi~juniferAuCustomersLifeSupportPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAuCustomersLifeSupportPost",
    value: function juniferAuCustomersLifeSupportPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuCustomersLifeSupportPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/au/customers/lifeSupport', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuCustomersSiteAccessPost operation.
     * @callback module:api/CustomersAustraliaApi~juniferAuCustomersSiteAccessPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add site access and hazard details
     * Adds Site Access Details and Hazard Description to Customer Property. This is an Australia specific API.
     * @param {module:model/AusSiteAccessPostAuCustomersSiteaccessRequestBody} requestBody Request body
     * @param {module:api/CustomersAustraliaApi~juniferAuCustomersSiteAccessPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAuCustomersSiteAccessPost",
    value: function juniferAuCustomersSiteAccessPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuCustomersSiteAccessPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/au/customers/siteAccess', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CustomersAustraliaApi;
}();

exports["default"] = CustomersAustraliaApi;