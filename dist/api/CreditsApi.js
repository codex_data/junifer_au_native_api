"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse = _interopRequireDefault(require("../model/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200"));

var _GetCreditFileGetCreditfilesIdResponse = _interopRequireDefault(require("../model/GetCreditFileGetCreditfilesIdResponse200"));

var _GetCreditFileGetCreditfilesIdResponse2 = _interopRequireDefault(require("../model/GetCreditFileGetCreditfilesIdResponse404"));

var _GetCreditFileImageGetCreditfilesIdImageResponse = _interopRequireDefault(require("../model/GetCreditFileImageGetCreditfilesIdImageResponse200"));

var _GetCreditFileImageGetCreditfilesIdImageResponse2 = _interopRequireDefault(require("../model/GetCreditFileImageGetCreditfilesIdImageResponse410"));

var _GetCreditGetCreditsIdResponse = _interopRequireDefault(require("../model/GetCreditGetCreditsIdResponse200"));

var _GetCreditGetCreditsIdResponse2 = _interopRequireDefault(require("../model/GetCreditGetCreditsIdResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Credits service.
* @module api/CreditsApi
* @version 1.61.1
*/
var CreditsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CreditsApi. 
  * @alias module:api/CreditsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CreditsApi(apiClient) {
    _classCallCheck(this, CreditsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferCreditFilesIdGet operation.
   * @callback module:api/CreditsApi~juniferCreditFilesIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetCreditFileGetCreditfilesIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get credit file
   * Get credit file record by its ID.  Note that the ID is of the credit file, NOT the credit. To get the correct credit files follow the link URL in the response of e.g. Get Credit API, where the correct credit file ID is prepopulated in the URL by the system.
   * @param {Number} id credit file ID
   * @param {module:api/CreditsApi~juniferCreditFilesIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetCreditFileGetCreditfilesIdResponse200}
   */


  _createClass(CreditsApi, [{
    key: "juniferCreditFilesIdGet",
    value: function juniferCreditFilesIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCreditFilesIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCreditFileGetCreditfilesIdResponse["default"];
      return this.apiClient.callApi('/junifer/creditFiles/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCreditFilesIdImageGet operation.
     * @callback module:api/CreditsApi~juniferCreditFilesIdImageGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCreditFileImageGetCreditfilesIdImageResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get credit file image
     * Initiates credit file image (usually PDF file) download if the file is available
     * @param {Number} id Credit file ID
     * @param {module:api/CreditsApi~juniferCreditFilesIdImageGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCreditFileImageGetCreditfilesIdImageResponse200}
     */

  }, {
    key: "juniferCreditFilesIdImageGet",
    value: function juniferCreditFilesIdImageGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCreditFilesIdImageGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCreditFileImageGetCreditfilesIdImageResponse["default"];
      return this.apiClient.callApi('/junifer/creditFiles/{id}/image', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCreditsIdCreditBreakdownLinesGet operation.
     * @callback module:api/CreditsApi~juniferCreditsIdCreditBreakdownLinesGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get credit's breakdown lines
     * Get credit's breakdown lines by its credit ID
     * @param {Number} id Credit ID
     * @param {module:api/CreditsApi~juniferCreditsIdCreditBreakdownLinesGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200}
     */

  }, {
    key: "juniferCreditsIdCreditBreakdownLinesGet",
    value: function juniferCreditsIdCreditBreakdownLinesGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCreditsIdCreditBreakdownLinesGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse["default"];
      return this.apiClient.callApi('/junifer/credits/{id}/creditBreakdownLines', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCreditsIdGet operation.
     * @callback module:api/CreditsApi~juniferCreditsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCreditGetCreditsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get Credit
     * Get credit record by its ID
     * @param {Number} id Credit ID
     * @param {Object} opts Optional parameters
     * @param {Number} opts.contactId Restrict creditFiles to a specific contact
     * @param {module:api/CreditsApi~juniferCreditsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCreditGetCreditsIdResponse200}
     */

  }, {
    key: "juniferCreditsIdGet",
    value: function juniferCreditsIdGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCreditsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'contactId': opts['contactId']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCreditGetCreditsIdResponse["default"];
      return this.apiClient.callApi('/junifer/credits/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CreditsApi;
}();

exports["default"] = CreditsApi;