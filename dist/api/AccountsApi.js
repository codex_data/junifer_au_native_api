"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse = _interopRequireDefault(require("../model/CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400"));

var _ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody = _interopRequireDefault(require("../model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody"));

var _ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse = _interopRequireDefault(require("../model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400"));

var _ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse2 = _interopRequireDefault(require("../model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404"));

var _ContactsGetAccountsIdContactsResponse = _interopRequireDefault(require("../model/ContactsGetAccountsIdContactsResponse200"));

var _ContactsGetAccountsIdContactsResponse2 = _interopRequireDefault(require("../model/ContactsGetAccountsIdContactsResponse400"));

var _CreateAccountCreditPostAccountsIdAccountcreditsRequestBody = _interopRequireDefault(require("../model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody"));

var _CreateAccountCreditPostAccountsIdAccountcreditsResponse = _interopRequireDefault(require("../model/CreateAccountCreditPostAccountsIdAccountcreditsResponse400"));

var _CreateAccountCreditPostAccountsIdAccountcreditsResponse2 = _interopRequireDefault(require("../model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404"));

var _CreateAccountDebitPostAccountsIdAccountdebitsRequestBody = _interopRequireDefault(require("../model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody"));

var _CreateAccountDebitPostAccountsIdAccountdebitsResponse = _interopRequireDefault(require("../model/CreateAccountDebitPostAccountsIdAccountdebitsResponse400"));

var _CreateAccountDebitPostAccountsIdAccountdebitsResponse2 = _interopRequireDefault(require("../model/CreateAccountDebitPostAccountsIdAccountdebitsResponse404"));

var _CreateAccountNotePostAccountsIdNoteRequestBody = _interopRequireDefault(require("../model/CreateAccountNotePostAccountsIdNoteRequestBody"));

var _CreateAccountNotePostAccountsIdNoteResponse = _interopRequireDefault(require("../model/CreateAccountNotePostAccountsIdNoteResponse200"));

var _CreateAccountNotePostAccountsIdNoteResponse2 = _interopRequireDefault(require("../model/CreateAccountNotePostAccountsIdNoteResponse400"));

var _CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody = _interopRequireDefault(require("../model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody"));

var _CreateAccountRepaymentPostAccountsIdRepaymentsResponse = _interopRequireDefault(require("../model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200"));

var _CreateAccountRepaymentPostAccountsIdRepaymentsResponse2 = _interopRequireDefault(require("../model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse400"));

var _CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody = _interopRequireDefault(require("../model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody"));

var _CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse = _interopRequireDefault(require("../model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200"));

var _CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse2 = _interopRequireDefault(require("../model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400"));

var _CreateAccountTicketPostAccountsIdTicketsRequestBody = _interopRequireDefault(require("../model/CreateAccountTicketPostAccountsIdTicketsRequestBody"));

var _CreateAccountTicketPostAccountsIdTicketsResponse = _interopRequireDefault(require("../model/CreateAccountTicketPostAccountsIdTicketsResponse200"));

var _CreateAccountTicketPostAccountsIdTicketsResponse2 = _interopRequireDefault(require("../model/CreateAccountTicketPostAccountsIdTicketsResponse400"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody = _interopRequireDefault(require("../model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse = _interopRequireDefault(require("../model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse2 = _interopRequireDefault(require("../model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400"));

var _GetAccountAgreementsGetAccountsIdAgreementsResponse = _interopRequireDefault(require("../model/GetAccountAgreementsGetAccountsIdAgreementsResponse200"));

var _GetAccountBillRequestsGetAccountsIdBillrequestsResponse = _interopRequireDefault(require("../model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200"));

var _GetAccountBillRequestsGetAccountsIdBillrequestsResponse2 = _interopRequireDefault(require("../model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse404"));

var _GetAccountBillsGetAccountsIdBillsResponse = _interopRequireDefault(require("../model/GetAccountBillsGetAccountsIdBillsResponse200"));

var _GetAccountByNumberGetAccountsAccountnumberNumResponse = _interopRequireDefault(require("../model/GetAccountByNumberGetAccountsAccountnumberNumResponse400"));

var _GetAccountByNumberGetAccountsAccountnumberNumResponse2 = _interopRequireDefault(require("../model/GetAccountByNumberGetAccountsAccountnumberNumResponse404"));

var _GetAccountCommunicationsGetAccountsIdCommunicationsResponse = _interopRequireDefault(require("../model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200"));

var _GetAccountCommunicationsGetAccountsIdCommunicationsResponse2 = _interopRequireDefault(require("../model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse400"));

var _GetAccountCreditNotesGetAccountsIdCreditsResponse = _interopRequireDefault(require("../model/GetAccountCreditNotesGetAccountsIdCreditsResponse200"));

var _GetAccountCreditsGetAccountsIdAccountcreditsResponse = _interopRequireDefault(require("../model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200"));

var _GetAccountDebitsGetAccountsIdAccountdebitsResponse = _interopRequireDefault(require("../model/GetAccountDebitsGetAccountsIdAccountdebitsResponse200"));

var _GetAccountGetAccountsIdResponse = _interopRequireDefault(require("../model/GetAccountGetAccountsIdResponse200"));

var _GetAccountGetAccountsIdResponse2 = _interopRequireDefault(require("../model/GetAccountGetAccountsIdResponse404"));

var _GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse = _interopRequireDefault(require("../model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200"));

var _GetAccountPaymentPlansGetAccountsIdPaymentplansResponse = _interopRequireDefault(require("../model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200"));

var _GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse = _interopRequireDefault(require("../model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200"));

var _GetAccountPaymentsGetAccountsIdPaymentsResponse = _interopRequireDefault(require("../model/GetAccountPaymentsGetAccountsIdPaymentsResponse200"));

var _GetAccountPaymentsGetAccountsIdPaymentsResponse2 = _interopRequireDefault(require("../model/GetAccountPaymentsGetAccountsIdPaymentsResponse400"));

var _GetAccountProductDetailsGetAccountsIdProductdetailsResponse = _interopRequireDefault(require("../model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200"));

var _GetAccountProductDetailsGetAccountsIdProductdetailsResponse2 = _interopRequireDefault(require("../model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse400"));

var _GetAccountPropertysGetAccountsIdPropertysResponse = _interopRequireDefault(require("../model/GetAccountPropertysGetAccountsIdPropertysResponse200"));

var _GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse = _interopRequireDefault(require("../model/GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200"));

var _GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse = _interopRequireDefault(require("../model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200"));

var _GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse2 = _interopRequireDefault(require("../model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400"));

var _GetAccountTicketsGetAccountsIdTicketsResponse = _interopRequireDefault(require("../model/GetAccountTicketsGetAccountsIdTicketsResponse200"));

var _GetAccountTicketsGetAccountsIdTicketsResponse2 = _interopRequireDefault(require("../model/GetAccountTicketsGetAccountsIdTicketsResponse400"));

var _GetAccountTransactionsGetAccountsIdTransactionsResponse = _interopRequireDefault(require("../model/GetAccountTransactionsGetAccountsIdTransactionsResponse200"));

var _GetAccountTransactionsGetAccountsIdTransactionsResponse2 = _interopRequireDefault(require("../model/GetAccountTransactionsGetAccountsIdTransactionsResponse400"));

var _GetAllPayReferenceGetAccountsIdAllpayreferenceResponse = _interopRequireDefault(require("../model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200"));

var _GetAllPayReferenceGetAccountsIdAllpayreferenceResponse2 = _interopRequireDefault(require("../model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400"));

var _GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse = _interopRequireDefault(require("../model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200"));

var _GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse2 = _interopRequireDefault(require("../model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404"));

var _HotbillPostAccountsIdHotbillResponse = _interopRequireDefault(require("../model/HotbillPostAccountsIdHotbillResponse400"));

var _LookUpAccountGetAccountsResponse = _interopRequireDefault(require("../model/LookUpAccountGetAccountsResponse200"));

var _LookUpAccountGetAccountsResponse2 = _interopRequireDefault(require("../model/LookUpAccountGetAccountsResponse400"));

var _PaymentPostAccountsIdPaymentRequestBody = _interopRequireDefault(require("../model/PaymentPostAccountsIdPaymentRequestBody"));

var _PaymentPostAccountsIdPaymentResponse = _interopRequireDefault(require("../model/PaymentPostAccountsIdPaymentResponse200"));

var _PaymentPostAccountsIdPaymentResponse2 = _interopRequireDefault(require("../model/PaymentPostAccountsIdPaymentResponse400"));

var _RenewAccountPostAccountsIdRenewalRequestBody = _interopRequireDefault(require("../model/RenewAccountPostAccountsIdRenewalRequestBody"));

var _RenewAccountPostAccountsIdRenewalResponse = _interopRequireDefault(require("../model/RenewAccountPostAccountsIdRenewalResponse200"));

var _RenewAccountPostAccountsIdRenewalResponse2 = _interopRequireDefault(require("../model/RenewAccountPostAccountsIdRenewalResponse400"));

var _TariffInformationGetAccountsIdTariffinformationResponse = _interopRequireDefault(require("../model/TariffInformationGetAccountsIdTariffinformationResponse200"));

var _TariffInformationGetAccountsIdTariffinformationResponse2 = _interopRequireDefault(require("../model/TariffInformationGetAccountsIdTariffinformationResponse400"));

var _UpdateAccountContactPutAccountsIdContactsContactidRequestBody = _interopRequireDefault(require("../model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody"));

var _UpdateAccountContactPutAccountsIdContactsContactidResponse = _interopRequireDefault(require("../model/UpdateAccountContactPutAccountsIdContactsContactidResponse400"));

var _UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody = _interopRequireDefault(require("../model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody"));

var _UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse = _interopRequireDefault(require("../model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Accounts service.
* @module api/AccountsApi
* @version 1.61.1
*/
var AccountsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new AccountsApi. 
  * @alias module:api/AccountsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function AccountsApi(apiClient) {
    _classCallCheck(this, AccountsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAccountsAccountNumberNumGet operation.
   * @callback module:api/AccountsApi~juniferAccountsAccountNumberNumGetCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get account by account number
   * Get an account by its account number (i.e. the customer-facing identifier).The success response is identical to the Get Account API.
   * @param {String} num Account Number
   * @param {module:api/AccountsApi~juniferAccountsAccountNumberNumGetCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(AccountsApi, [{
    key: "juniferAccountsAccountNumberNumGet",
    value: function juniferAccountsAccountNumberNumGet(num, callback) {
      var postBody = null; // verify the required parameter 'num' is set

      if (num === undefined || num === null) {
        throw new Error("Missing the required parameter 'num' when calling juniferAccountsAccountNumberNumGet");
      }

      var pathParams = {
        'num': num
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accounts/accountNumber/{num}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LookUpAccountGetAccountsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Account lookup
     * Searches for an account by one or more of the specified search terms. If no account can be found an empty `results` array is returned. The query parameters are :account_number, :surname, :billingPostcode, :email. The lookup can be performed using either of them, but at least one must be provided. If more than one are specified, the account contact details are cross-checked by looking if the provided search values number belong to the same account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.number number
     * @param {String} opts.surname surname
     * @param {String} opts.telephone Telephone number in any of the telephone fields in account primary contact. Right search allowed (i.e without international/area codes)d. If this parameter is present in the request it must be non-empty.
     * @param {String} opts.billingPostcode Billing postcode (no spaces)
     * @param {String} opts.email email address in account primary contact record
     * @param {module:api/AccountsApi~juniferAccountsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/LookUpAccountGetAccountsResponse200}
     */

  }, {
    key: "juniferAccountsGet",
    value: function juniferAccountsGet(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'number': opts['number'],
        'surname': opts['surname'],
        'telephone': opts['telephone'],
        'billingPostcode': opts['billingPostcode'],
        'email': opts['email']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _LookUpAccountGetAccountsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdAccountCreditsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountCreditsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account credits
     * Get account credits for an account
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAccountCreditsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200}
     */

  }, {
    key: "juniferAccountsIdAccountCreditsGet",
    value: function juniferAccountsIdAccountCreditsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountCreditsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountCreditsGetAccountsIdAccountcreditsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/accountCredits', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdAccountCreditsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountCreditsPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account credit
     * Create account credit for account. Authentication must be active
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdAccountCreditsPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAccountsIdAccountCreditsPost",
    value: function juniferAccountsIdAccountCreditsPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountCreditsPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdAccountCreditsPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accounts/{id}/accountCredits', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdAccountDebitsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountDebitsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountDebitsGetAccountsIdAccountdebitsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account debits
     * Get account debits for an account
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAccountDebitsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountDebitsGetAccountsIdAccountdebitsResponse200}
     */

  }, {
    key: "juniferAccountsIdAccountDebitsGet",
    value: function juniferAccountsIdAccountDebitsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountDebitsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountDebitsGetAccountsIdAccountdebitsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/accountDebits', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdAccountDebitsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountDebitsPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account debit
     * Create account debit for account. Authentication must be active
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdAccountDebitsPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAccountsIdAccountDebitsPost",
    value: function juniferAccountsIdAccountDebitsPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountDebitsPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdAccountDebitsPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accounts/{id}/accountDebits', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdAccountReviewPeriodsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountReviewPeriodsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's review periods
     * Gets account's review periods which belong to the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAccountReviewPeriodsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200}
     */

  }, {
    key: "juniferAccountsIdAccountReviewPeriodsGet",
    value: function juniferAccountsIdAccountReviewPeriodsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountReviewPeriodsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/accountReviewPeriods', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdAccountReviewPeriodsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountReviewPeriodsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account review period
     * Creates a new account review period for the account specified by the Id
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdAccountReviewPeriodsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200}
     */

  }, {
    key: "juniferAccountsIdAccountReviewPeriodsPost",
    value: function juniferAccountsIdAccountReviewPeriodsPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountReviewPeriodsPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdAccountReviewPeriodsPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/accountReviewPeriods', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdAgreementsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAgreementsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's agreements
     * Get agreements associated to the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAgreementsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200}
     */

  }, {
    key: "juniferAccountsIdAgreementsGet",
    value: function juniferAccountsIdAgreementsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAgreementsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountAgreementsGetAccountsIdAgreementsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/agreements', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdAllPayReferenceGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAllPayReferenceGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's AllPay Reference Number
     * Get the AllPay reference number
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAllPayReferenceGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200}
     */

  }, {
    key: "juniferAccountsIdAllPayReferenceGet",
    value: function juniferAccountsIdAllPayReferenceGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAllPayReferenceGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAllPayReferenceGetAccountsIdAllpayreferenceResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/allPayReference', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdBillRequestsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdBillRequestsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's bill requests
     * Gets bill requests which belong to the account specified by the ID. If no bill requests can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.statusCode Status Code of retrieved bill requests. Can contain the following: `Scheduled, Completed, Failed, Superseded, Retrying`. If not provided then this filter won't be applied
     * @param {String} opts.typeCode Type Code of retrieved bill requests. Can contain the following: `Normal, Hot, NewVersion`. If not provided then this filter won't be applied
     * @param {module:api/AccountsApi~juniferAccountsIdBillRequestsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200}
     */

  }, {
    key: "juniferAccountsIdBillRequestsGet",
    value: function juniferAccountsIdBillRequestsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdBillRequestsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'statusCode': opts['statusCode'],
        'typeCode': opts['typeCode']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountBillRequestsGetAccountsIdBillrequestsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/billRequests', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdBillsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdBillsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountBillsGetAccountsIdBillsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's bills
     * Gets bills which belong to the account specified by the ID. If no bills can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.status Status of retrieved bills. Can contain the following: `All, Accepted, Draft`. Defaults to `Accepted` if not provided.
     * @param {module:api/AccountsApi~juniferAccountsIdBillsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountBillsGetAccountsIdBillsResponse200}
     */

  }, {
    key: "juniferAccountsIdBillsGet",
    value: function juniferAccountsIdBillsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdBillsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'status': opts['status']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountBillsGetAccountsIdBillsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/bills', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdCancelAccountReviewPeriodDelete operation.
     * @callback module:api/AccountsApi~juniferAccountsIdCancelAccountReviewPeriodDeleteCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Cancel account review period
     * Cancel an existing account review period for an account
     * @param {Number} id Account ID.
     * @param {Number} accountReviewPeriodId Account Review Period ID
     * @param {module:api/AccountsApi~juniferAccountsIdCancelAccountReviewPeriodDeleteCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAccountsIdCancelAccountReviewPeriodDelete",
    value: function juniferAccountsIdCancelAccountReviewPeriodDelete(id, accountReviewPeriodId, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdCancelAccountReviewPeriodDelete");
      } // verify the required parameter 'accountReviewPeriodId' is set


      if (accountReviewPeriodId === undefined || accountReviewPeriodId === null) {
        throw new Error("Missing the required parameter 'accountReviewPeriodId' when calling juniferAccountsIdCancelAccountReviewPeriodDelete");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'accountReviewPeriodId': accountReviewPeriodId
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accounts/{id}/cancelAccountReviewPeriod', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdChangeAccountCustomerPut operation.
     * @callback module:api/AccountsApi~juniferAccountsIdChangeAccountCustomerPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Change the customer linked to an account
     * Change the customer linked to the account to the customer specified by the id
     * @param {Number} id Account ID
     * @param {module:model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdChangeAccountCustomerPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAccountsIdChangeAccountCustomerPut",
    value: function juniferAccountsIdChangeAccountCustomerPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdChangeAccountCustomerPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdChangeAccountCustomerPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accounts/{id}/changeAccountCustomer', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdCommercialProductDetailsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdCommercialProductDetailsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's product details for commercial customer
     * Get the details for products associated to the commercial account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdCommercialProductDetailsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200}
     */

  }, {
    key: "juniferAccountsIdCommercialProductDetailsGet",
    value: function juniferAccountsIdCommercialProductDetailsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdCommercialProductDetailsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/commercial/productDetails', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdCommunicationsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdCommunicationsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's communications
     * Get communications associated to the account specified by the ID
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.fromDate Date from
     * @param {String} opts.toDate Date to
     * @param {Boolean} opts.testFlag true will include test communications
     * @param {Array.<String>} opts.status Array of statuses strings used to filter the search of the communications. Can contain the following: `Pending, Successful, SuccessfulWithWarnings, NoDeliveries, Failed` and if left blank will default to `Successful, SuccessfulWithWarnings`
     * @param {module:api/AccountsApi~juniferAccountsIdCommunicationsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200}
     */

  }, {
    key: "juniferAccountsIdCommunicationsGet",
    value: function juniferAccountsIdCommunicationsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdCommunicationsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'from_date': opts['fromDate'],
        'to_date': opts['toDate'],
        'test_flag': opts['testFlag'],
        'status': this.apiClient.buildCollectionParam(opts['status'], 'csv')
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountCommunicationsGetAccountsIdCommunicationsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/communications', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdContactsContactIdPut operation.
     * @callback module:api/AccountsApi~juniferAccountsIdContactsContactIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update account's contact details
     * Updates a account's given contact details. Correct usage is to get the `contact` object from the accounts `contacts` call then modify the `contact` object and return it using this `Update Account's Contact` call. Our database is then updated to match the incoming `contact` object. All null fields in that incoming `contact` object are set to null in our database.  Note that setting billDelivery to \"None\" could mean that no one will be notified when a bill is generated. Other billing activity will continue as normal. Please use with caution for Residential contacts.
     * @param {Number} id Account ID
     * @param {Number} contactId Contact ID
     * @param {module:model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdContactsContactIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAccountsIdContactsContactIdPut",
    value: function juniferAccountsIdContactsContactIdPut(id, contactId, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdContactsContactIdPut");
      } // verify the required parameter 'contactId' is set


      if (contactId === undefined || contactId === null) {
        throw new Error("Missing the required parameter 'contactId' when calling juniferAccountsIdContactsContactIdPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdContactsContactIdPut");
      }

      var pathParams = {
        'id': id,
        'contactId': contactId
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accounts/{id}/contacts/{contactId}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdContactsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdContactsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ContactsGetAccountsIdContactsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Contacts
     * Get all contacts for given account. If no contacts can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdContactsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ContactsGetAccountsIdContactsResponse200}
     */

  }, {
    key: "juniferAccountsIdContactsGet",
    value: function juniferAccountsIdContactsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdContactsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _ContactsGetAccountsIdContactsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/contacts', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdCreditsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdCreditsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's credits
     * Gets credits which belong to the account specified by the ID. If no credits can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.status Status of retrieved credits. Can contain the following: `All,Draft, Authorised, Rejected`. Defaults to `Accepted` if not provided.
     * @param {module:api/AccountsApi~juniferAccountsIdCreditsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200}
     */

  }, {
    key: "juniferAccountsIdCreditsGet",
    value: function juniferAccountsIdCreditsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdCreditsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'status': opts['status']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountCreditNotesGetAccountsIdCreditsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/credits', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdEnrolMeterPointsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdEnrolMeterPointsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Enrol additional Meter Points to existing account/site
     * Enrols new Meter Point(s) to an existing account/site for electricity and/or gas product. Returns a list of the newly-enrolled Meter Points.
     * @param {Number} id Account ID
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdEnrolMeterPointsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200}
     */

  }, {
    key: "juniferAccountsIdEnrolMeterPointsPost",
    value: function juniferAccountsIdEnrolMeterPointsPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdEnrolMeterPointsPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdEnrolMeterPointsPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/enrolMeterPoints', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountGetAccountsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account
     * Gets account by its ID (not account number!)
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountGetAccountsIdResponse200}
     */

  }, {
    key: "juniferAccountsIdGet",
    value: function juniferAccountsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountGetAccountsIdResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdHotBillPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdHotBillPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Hot bill
     * Create a hot bill for a given Account. This will consolidate all open bill periods that precede today's date. Authentication must be active.
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdHotBillPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAccountsIdHotBillPost",
    value: function juniferAccountsIdHotBillPost(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdHotBillPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accounts/{id}/hotBill', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdNotePost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdNotePostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateAccountNotePostAccountsIdNoteResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account note
     * Creates a new note for the account specified by the Id
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountNotePostAccountsIdNoteRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdNotePostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateAccountNotePostAccountsIdNoteResponse200}
     */

  }, {
    key: "juniferAccountsIdNotePost",
    value: function juniferAccountsIdNotePost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdNotePost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdNotePost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _CreateAccountNotePostAccountsIdNoteResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/note', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentMethodsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentMethodsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payment methods
     * Get payment methods registered for the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentMethodsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200}
     */

  }, {
    key: "juniferAccountsIdPaymentMethodsGet",
    value: function juniferAccountsIdPaymentMethodsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentMethodsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/paymentMethods', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentPlansGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentPlansGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payment plans
     * Gets payment plans which belong to the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentPlansGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200}
     */

  }, {
    key: "juniferAccountsIdPaymentPlansGet",
    value: function juniferAccountsIdPaymentPlansGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentPlansGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountPaymentPlansGetAccountsIdPaymentplansResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/paymentPlans', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/PaymentPostAccountsIdPaymentResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create Payment Transaction
     * Creates a transaction for payments that occurs outside of Junifer. No payment collection will be triggered. Requires authentication.
     * @param {Number} id Account ID
     * @param {module:model/PaymentPostAccountsIdPaymentRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/PaymentPostAccountsIdPaymentResponse200}
     */

  }, {
    key: "juniferAccountsIdPaymentPost",
    value: function juniferAccountsIdPaymentPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdPaymentPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _PaymentPostAccountsIdPaymentResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/payment', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentSchedulePeriodsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentSchedulePeriodsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payment schedule periods
     * Get account's payment schedule periods
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.nextPaymentOnlyFl If `true` returns only the payment period that will be active when the next payment is collected, this may be the current payment period. If not provided then defaults to `false`.
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentSchedulePeriodsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200}
     */

  }, {
    key: "juniferAccountsIdPaymentSchedulePeriodsGet",
    value: function juniferAccountsIdPaymentSchedulePeriodsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentSchedulePeriodsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'nextPaymentOnlyFl': opts['nextPaymentOnlyFl']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/paymentSchedulePeriods', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's suggested payment amount
     * Get account's suggested payment amount given the payment schedule review calculation strategy
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.frequency The name of the frequency indicating when a payment should be collected, e.g. `Monthly, Weekly`. Defaults to `Monthly`
     * @param {Number} opts.frequencyMultiple Frequency multiplier. Defaults to 1
     * @param {Boolean} opts.ignoreWarnings If set to true, consumption estimation warnings will be ignored. If false, the API will return an error if there are consumption estimation warnings. Defaults to false
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200}
     */

  }, {
    key: "juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet",
    value: function juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'frequency': opts['frequency'],
        'frequencyMultiple': opts['frequencyMultiple'],
        'ignoreWarnings': opts['ignoreWarnings']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/paymentSchedules/suggestedPaymentAmount', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payments
     * Get payments associated to the account specified by the ID with the corresponding transaction status.
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {Array.<String>} opts.status Array of transaction status strings used to filter the search of our payments.  Can contain the following: `Pending, Accepted, Rejected, Authorising` and if left blank will default to using `Accepted`.
     * @param {Boolean} opts.cancelFl Returns cancelled transactions if `true`. If not provided then defaults to `false`
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200}
     */

  }, {
    key: "juniferAccountsIdPaymentsGet",
    value: function juniferAccountsIdPaymentsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'status': this.apiClient.buildCollectionParam(opts['status'], 'csv'),
        'cancelFl': opts['cancelFl']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountPaymentsGetAccountsIdPaymentsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/payments', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdPrimaryContactPut operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPrimaryContactPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update Accounts's primary contact details.
     * Updates Accounts's primary contact details. Correct usage is to get the `primaryContact` object from the `get account` call then modify the `primaryContact` information in it using this Update Account's Contact call. Our database is then updated to match the incoming `primaryContact` object. All null fields in that incoming `primaryContact` object are set to null in our database. Missing params are treated as nulls.  Note that setting billDelivery to \"None\" could mean that no one will be notified when a bill is generated. Other billing activity will continue as normal. Please use with caution for Residential contacts.
     * @param {Number} id Account ID
     * @param {module:model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdPrimaryContactPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAccountsIdPrimaryContactPut",
    value: function juniferAccountsIdPrimaryContactPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPrimaryContactPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdPrimaryContactPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accounts/{id}/primaryContact', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdProductDetailsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdProductDetailsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's product details
     * Get the details for products associated to the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdProductDetailsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200}
     */

  }, {
    key: "juniferAccountsIdProductDetailsGet",
    value: function juniferAccountsIdProductDetailsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdProductDetailsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountProductDetailsGetAccountsIdProductdetailsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/productDetails', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdPropertysGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPropertysGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's properties
     * Get account's properties. If no properties can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdPropertysGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPropertysGetAccountsIdPropertysResponse200}
     */

  }, {
    key: "juniferAccountsIdPropertysGet",
    value: function juniferAccountsIdPropertysGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPropertysGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountPropertysGetAccountsIdPropertysResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/propertys', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdRenewalPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdRenewalPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/RenewAccountPostAccountsIdRenewalResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Renew account
     * Renews an account with a new agreement based on the additional details supplied. Either electricity product details or gas product details need to be supplied for this to work.  Will raise a ticket, if one is specified in the property Agreement Creation Properties > API Agreement Setup Ticket.  Will trigger change of meter mode if new or existing agreements are SMART PAYG and meter contracted to the account is in DCC mode
     * @param {Number} id Account ID
     * @param {module:model/RenewAccountPostAccountsIdRenewalRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdRenewalPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/RenewAccountPostAccountsIdRenewalResponse200}
     */

  }, {
    key: "juniferAccountsIdRenewalPost",
    value: function juniferAccountsIdRenewalPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdRenewalPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdRenewalPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _RenewAccountPostAccountsIdRenewalResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/renewal', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdRepaymentsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdRepaymentsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account repayment
     * Create a refund for an existing account payment. The account must have a valid default payment method set. This call is only usable when authentication is active
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdRepaymentsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200}
     */

  }, {
    key: "juniferAccountsIdRepaymentsPost",
    value: function juniferAccountsIdRepaymentsPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdRepaymentsPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdRepaymentsPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _CreateAccountRepaymentPostAccountsIdRepaymentsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/repayments', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdTariffInformationGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdTariffInformationGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/TariffInformationGetAccountsIdTariffinformationResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's tariff Information
     * Get tariff information associated with this account at the comparison date, including tariffs that are active on the day and tariffs that end on the day.
     * @param {Number} id Account ID
     * @param {Boolean} ignoreWarnings If set to true, consumption estimation warnings will be ignored. If false, the API will return an error if there are consumption estimation warnings. Defaults to false
     * @param {Object} opts Optional parameters
     * @param {String} opts.comparisonDatetime Date to get information for. Default to today if not specified.
     * @param {module:api/AccountsApi~juniferAccountsIdTariffInformationGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/TariffInformationGetAccountsIdTariffinformationResponse200}
     */

  }, {
    key: "juniferAccountsIdTariffInformationGet",
    value: function juniferAccountsIdTariffInformationGet(id, ignoreWarnings, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdTariffInformationGet");
      } // verify the required parameter 'ignoreWarnings' is set


      if (ignoreWarnings === undefined || ignoreWarnings === null) {
        throw new Error("Missing the required parameter 'ignoreWarnings' when calling juniferAccountsIdTariffInformationGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'comparison_datetime': opts['comparisonDatetime'],
        'ignoreWarnings': ignoreWarnings
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _TariffInformationGetAccountsIdTariffinformationResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/tariffInformation', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdTicketsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdTicketsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountTicketsGetAccountsIdTicketsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's tickets
     * Get tickets associated with this account
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {Array.<String>} opts.status Array of status which is used to filter the search of our ticket status. Can contain the following: `Open, Error, Closed, Cancelled` and if left blank will default to display all the tickets.
     * @param {Boolean} opts.includeIndirect if false, only tickets directly linked to the Account will be returned. If true, will also return tickets that are indirectly linked to the account via Bill, AccountTransaction, DunningInst, Asset, Customer, ProductPropertyAsset and Property.
     * @param {module:api/AccountsApi~juniferAccountsIdTicketsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountTicketsGetAccountsIdTicketsResponse200}
     */

  }, {
    key: "juniferAccountsIdTicketsGet",
    value: function juniferAccountsIdTicketsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdTicketsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'status': this.apiClient.buildCollectionParam(opts['status'], 'csv'),
        'includeIndirect': opts['includeIndirect']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountTicketsGetAccountsIdTicketsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/tickets', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdTicketsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdTicketsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account ticket
     * Creates a new ticket for the account specified by the ID  Contact Gentrack before using this request. It can have serious unexpected consequences if not used correctly.
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountTicketPostAccountsIdTicketsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdTicketsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateAccountTicketPostAccountsIdTicketsResponse200}
     */

  }, {
    key: "juniferAccountsIdTicketsPost",
    value: function juniferAccountsIdTicketsPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdTicketsPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdTicketsPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _CreateAccountTicketPostAccountsIdTicketsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/tickets', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountsIdTransactionsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdTransactionsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's transactions
     * Gets accepted transactions for the account specified by the ID
     * @param {Number} id AccountID
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.cancelFl Returns cancelled transactions if `true`. If not provided then defaults to `false`
     * @param {Array.<String>} opts.status Array of transaction status strings used to filter the search of account transactions.  Can contain the following: `All, Pending, Accepted, Rejected, Authorising` and if left blank will default to using `Accepted`.
     * @param {String} opts.createdDt Date that specifies which transactions select from that date onwards
     * @param {module:api/AccountsApi~juniferAccountsIdTransactionsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200}
     */

  }, {
    key: "juniferAccountsIdTransactionsGet",
    value: function juniferAccountsIdTransactionsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdTransactionsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'cancelFl': opts['cancelFl'],
        'status': this.apiClient.buildCollectionParam(opts['status'], 'csv'),
        'created_dt': opts['createdDt']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountTransactionsGetAccountsIdTransactionsResponse["default"];
      return this.apiClient.callApi('/junifer/accounts/{id}/transactions', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return AccountsApi;
}();

exports["default"] = AccountsApi;