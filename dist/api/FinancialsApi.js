"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse = _interopRequireDefault(require("../model/CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400"));

var _CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody = _interopRequireDefault(require("../model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody"));

var _CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse = _interopRequireDefault(require("../model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200"));

var _CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse2 = _interopRequireDefault(require("../model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400"));

var _CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse3 = _interopRequireDefault(require("../model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404"));

var _GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse = _interopRequireDefault(require("../model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200"));

var _GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse2 = _interopRequireDefault(require("../model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400"));

var _GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse3 = _interopRequireDefault(require("../model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404"));

var _GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse = _interopRequireDefault(require("../model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200"));

var _GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse2 = _interopRequireDefault(require("../model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400"));

var _GetPaymentGetPaymentsIdResponse = _interopRequireDefault(require("../model/GetPaymentGetPaymentsIdResponse200"));

var _GetPaymentGetPaymentsIdResponse2 = _interopRequireDefault(require("../model/GetPaymentGetPaymentsIdResponse404"));

var _GetPaymentMethodGetPaymentmethodsIdResponse = _interopRequireDefault(require("../model/GetPaymentMethodGetPaymentmethodsIdResponse200"));

var _GetPaymentMethodGetPaymentmethodsIdResponse2 = _interopRequireDefault(require("../model/GetPaymentMethodGetPaymentmethodsIdResponse404"));

var _GetPaymentPlanGetPaymentplansIdResponse = _interopRequireDefault(require("../model/GetPaymentPlanGetPaymentplansIdResponse200"));

var _GetPaymentPlanGetPaymentplansIdResponse2 = _interopRequireDefault(require("../model/GetPaymentPlanGetPaymentplansIdResponse404"));

var _GetPaymentRequestGetPaymentrequestsIdResponse = _interopRequireDefault(require("../model/GetPaymentRequestGetPaymentrequestsIdResponse200"));

var _GetPaymentRequestGetPaymentrequestsIdResponse2 = _interopRequireDefault(require("../model/GetPaymentRequestGetPaymentrequestsIdResponse404"));

var _GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse = _interopRequireDefault(require("../model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200"));

var _GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse2 = _interopRequireDefault(require("../model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404"));

var _StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody = _interopRequireDefault(require("../model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody"));

var _StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse = _interopRequireDefault(require("../model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Financials service.
* @module api/FinancialsApi
* @version 1.61.1
*/
var FinancialsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new FinancialsApi. 
  * @alias module:api/FinancialsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function FinancialsApi(apiClient) {
    _classCallCheck(this, FinancialsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferActiveSeasonalDefinitionGet operation.
   * @callback module:api/FinancialsApi~juniferActiveSeasonalDefinitionGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get active seasonal definition
   * Gets the currently active seasonal definition for seasonally-variable Direct Debit payments
   * @param {module:api/FinancialsApi~juniferActiveSeasonalDefinitionGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200}
   */


  _createClass(FinancialsApi, [{
    key: "juniferActiveSeasonalDefinitionGet",
    value: function juniferActiveSeasonalDefinitionGet(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse["default"];
      return this.apiClient.callApi('/junifer/activeSeasonalDefinition', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPaymentMethodsIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentMethodsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentMethodGetPaymentmethodsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment method
     * Get payment method by its ID
     * @param {Number} id Payment method ID
     * @param {module:api/FinancialsApi~juniferPaymentMethodsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentMethodGetPaymentmethodsIdResponse200}
     */

  }, {
    key: "juniferPaymentMethodsIdGet",
    value: function juniferPaymentMethodsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentMethodsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetPaymentMethodGetPaymentmethodsIdResponse["default"];
      return this.apiClient.callApi('/junifer/paymentMethods/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPaymentPlansIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentPlansIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentPlanGetPaymentplansIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment plan
     * Get payment plan by its ID
     * @param {Number} id Payment plan ID
     * @param {module:api/FinancialsApi~juniferPaymentPlansIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentPlanGetPaymentplansIdResponse200}
     */

  }, {
    key: "juniferPaymentPlansIdGet",
    value: function juniferPaymentPlansIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentPlansIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetPaymentPlanGetPaymentplansIdResponse["default"];
      return this.apiClient.callApi('/junifer/paymentPlans/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPaymentRequestsIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentRequestsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment request
     * Get payment request by its ID
     * @param {Number} id Payment plan ID
     * @param {module:api/FinancialsApi~juniferPaymentRequestsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentRequestGetPaymentrequestsIdResponse200}
     */

  }, {
    key: "juniferPaymentRequestsIdGet",
    value: function juniferPaymentRequestsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentRequestsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetPaymentRequestGetPaymentrequestsIdResponse["default"];
      return this.apiClient.callApi('/junifer/paymentRequests/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsCalculateNextPaymentDateGetCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Calculate next payment schedule collection date
     * Calculates and returns next payment schedule date based on the parameters supplied
     * @param {String} frequency The name of the frequency indicating when a payment should be collected, e.g. `Monthly, Weekly, Daily`
     * @param {Number} multiple Frequency multiplier
     * @param {String} startDttm Payment schedule period start date
     * @param {String} alignmentDttm Alignment date
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsCalculateNextPaymentDateGetCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet",
    value: function juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet(frequency, multiple, startDttm, alignmentDttm, callback) {
      var postBody = null; // verify the required parameter 'frequency' is set

      if (frequency === undefined || frequency === null) {
        throw new Error("Missing the required parameter 'frequency' when calling juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet");
      } // verify the required parameter 'multiple' is set


      if (multiple === undefined || multiple === null) {
        throw new Error("Missing the required parameter 'multiple' when calling juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet");
      } // verify the required parameter 'startDttm' is set


      if (startDttm === undefined || startDttm === null) {
        throw new Error("Missing the required parameter 'startDttm' when calling juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet");
      } // verify the required parameter 'alignmentDttm' is set


      if (alignmentDttm === undefined || alignmentDttm === null) {
        throw new Error("Missing the required parameter 'alignmentDttm' when calling juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet");
      }

      var pathParams = {};
      var queryParams = {
        'frequency': frequency,
        'multiple': multiple,
        'startDttm': startDttm,
        'alignmentDttm': alignmentDttm
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/paymentSchedulePeriods/calculateNextPaymentDate', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment schedule period
     * Get payment schedule period by its ID
     * @param {Number} id Payment schedule period ID
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200}
     */

  }, {
    key: "juniferPaymentSchedulePeriodsIdGet",
    value: function juniferPaymentSchedulePeriodsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentSchedulePeriodsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse["default"];
      return this.apiClient.callApi('/junifer/paymentSchedulePeriods/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsIdPut operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Stop payment schedule period
     * Stops payment schedule period. Exactly the same JSON structure can be passed as retrieved in Get payment schedule period, but with `toDt` field added. This method will set `PaymentSchedulePeriod.StoppedDttm` to Now.
     * @param {Number} id Payment schedule ID
     * @param {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody} requestBody Request body
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferPaymentSchedulePeriodsIdPut",
    value: function juniferPaymentSchedulePeriodsIdPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentSchedulePeriodsIdPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferPaymentSchedulePeriodsIdPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/paymentSchedulePeriods/{id}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsPaymentScheduleItemsGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsPaymentScheduleItemsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payment schedule items
     * Get payment schedule items for a given account ID. If no payment schedules can be found an empty `results` array is returned.
     * @param {Number} accountId Account ID
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsPaymentScheduleItemsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200}
     */

  }, {
    key: "juniferPaymentSchedulePeriodsPaymentScheduleItemsGet",
    value: function juniferPaymentSchedulePeriodsPaymentScheduleItemsGet(accountId, callback) {
      var postBody = null; // verify the required parameter 'accountId' is set

      if (accountId === undefined || accountId === null) {
        throw new Error("Missing the required parameter 'accountId' when calling juniferPaymentSchedulePeriodsPaymentScheduleItemsGet");
      }

      var pathParams = {};
      var queryParams = {
        'accountId': accountId
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse["default"];
      return this.apiClient.callApi('/junifer/paymentSchedulePeriods/paymentScheduleItems', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsPost operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create new payment schedule period
     * Creates a new payment schedule period.  Will raise a ticket, if one is specified in the property Payment Schedule Period Properties > API Payment Schedule Period Change Ticket.
     * @param {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody} requestBody Request body
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200}
     */

  }, {
    key: "juniferPaymentSchedulePeriodsPost",
    value: function juniferPaymentSchedulePeriodsPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferPaymentSchedulePeriodsPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse["default"];
      return this.apiClient.callApi('/junifer/paymentSchedulePeriods', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPaymentsIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentGetPaymentsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment
     * Gets payment by its ID
     * @param {Number} id Payment ID
     * @param {module:api/FinancialsApi~juniferPaymentsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentGetPaymentsIdResponse200}
     */

  }, {
    key: "juniferPaymentsIdGet",
    value: function juniferPaymentsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetPaymentGetPaymentsIdResponse["default"];
      return this.apiClient.callApi('/junifer/payments/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return FinancialsApi;
}();

exports["default"] = FinancialsApi;