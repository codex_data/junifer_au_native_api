"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetMeterReadingsGetAuMeterpointsIdReadingsResponse = _interopRequireDefault(require("../model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200"));

var _GetMeterReadingsGetAuMeterpointsIdReadingsResponse2 = _interopRequireDefault(require("../model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse400"));

var _GetMeterStructureGetAuMeterpointsIdMeterstructureResponse = _interopRequireDefault(require("../model/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200"));

var _GetMeterStructureGetAuMeterpointsIdMeterstructureResponse2 = _interopRequireDefault(require("../model/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse400"));

var _GetMeterStructureGetAuMeterpointsMeterstructureResponse = _interopRequireDefault(require("../model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200"));

var _GetMeterStructureGetAuMeterpointsMeterstructureResponse2 = _interopRequireDefault(require("../model/GetMeterStructureGetAuMeterpointsMeterstructureResponse400"));

var _LookUpMeterPointGetAuMeterpointsResponse = _interopRequireDefault(require("../model/LookUpMeterPointGetAuMeterpointsResponse200"));

var _LookUpMeterPointGetAuMeterpointsResponse2 = _interopRequireDefault(require("../model/LookUpMeterPointGetAuMeterpointsResponse400"));

var _SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody = _interopRequireDefault(require("../model/SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody"));

var _SubmitMeterReadingPostAuMeterpointsIdReadingsResponse = _interopRequireDefault(require("../model/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* MeterpointsAustralia service.
* @module api/MeterpointsAustraliaApi
* @version 1.61.1
*/
var MeterpointsAustraliaApi = /*#__PURE__*/function () {
  /**
  * Constructs a new MeterpointsAustraliaApi. 
  * @alias module:api/MeterpointsAustraliaApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function MeterpointsAustraliaApi(apiClient) {
    _classCallCheck(this, MeterpointsAustraliaApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAuMeterPointsGet operation.
   * @callback module:api/MeterpointsAustraliaApi~juniferAuMeterPointsGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/LookUpMeterPointGetAuMeterpointsResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * MeterPoint lookup
   * Searches for a meterPoint by the specified meterSerialNumber and queryDttm. If no meterPoint can be found an empty `results` array is returned.
   * @param {Number} meterSerialNumber Serial number of the meter that is linked to the meterPoint
   * @param {String} queryDttm Time that the meter is linked to the meterPoint. Do not enter a Dttm that is before the SSD.
   * @param {module:api/MeterpointsAustraliaApi~juniferAuMeterPointsGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/LookUpMeterPointGetAuMeterpointsResponse200}
   */


  _createClass(MeterpointsAustraliaApi, [{
    key: "juniferAuMeterPointsGet",
    value: function juniferAuMeterPointsGet(meterSerialNumber, queryDttm, callback) {
      var postBody = null; // verify the required parameter 'meterSerialNumber' is set

      if (meterSerialNumber === undefined || meterSerialNumber === null) {
        throw new Error("Missing the required parameter 'meterSerialNumber' when calling juniferAuMeterPointsGet");
      } // verify the required parameter 'queryDttm' is set


      if (queryDttm === undefined || queryDttm === null) {
        throw new Error("Missing the required parameter 'queryDttm' when calling juniferAuMeterPointsGet");
      }

      var pathParams = {};
      var queryParams = {
        'meterSerialNumber': meterSerialNumber,
        'queryDttm': queryDttm
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _LookUpMeterPointGetAuMeterpointsResponse["default"];
      return this.apiClient.callApi('/junifer/au/meterPoints', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuMeterPointsIdMeterStructureGet operation.
     * @callback module:api/MeterpointsAustraliaApi~juniferAuMeterPointsIdMeterStructureGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get meterpoint structure
     * Get meterpoint structure
     * @param {Number} id Meterpoint ID
     * @param {String} effectiveDt Show meterpoint structure at the specific date
     * @param {module:api/MeterpointsAustraliaApi~juniferAuMeterPointsIdMeterStructureGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200}
     */

  }, {
    key: "juniferAuMeterPointsIdMeterStructureGet",
    value: function juniferAuMeterPointsIdMeterStructureGet(id, effectiveDt, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuMeterPointsIdMeterStructureGet");
      } // verify the required parameter 'effectiveDt' is set


      if (effectiveDt === undefined || effectiveDt === null) {
        throw new Error("Missing the required parameter 'effectiveDt' when calling juniferAuMeterPointsIdMeterStructureGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'effectiveDt': effectiveDt
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetMeterStructureGetAuMeterpointsIdMeterstructureResponse["default"];
      return this.apiClient.callApi('/junifer/au/meterPoints/{id}/meterStructure', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuMeterPointsIdReadingsGet operation.
     * @callback module:api/MeterpointsAustraliaApi~juniferAuMeterPointsIdReadingsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get meter readings
     * Get (billable) meter readings for the specified period
     * @param {Number} id Meterpoint ID
     * @param {String} fromDt The start date of the meter reading range
     * @param {String} toDt The end date of the meter reading range
     * @param {Object} opts Optional parameters
     * @param {Array.<String>} opts.status Array of reading status strings used to filter the search of our meter readings.  Can contain the following: `Accepted, Pending, Unknown, Removed` and if left blank will default to using `Accepted`.
     * @param {module:api/MeterpointsAustraliaApi~juniferAuMeterPointsIdReadingsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200}
     */

  }, {
    key: "juniferAuMeterPointsIdReadingsGet",
    value: function juniferAuMeterPointsIdReadingsGet(id, fromDt, toDt, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuMeterPointsIdReadingsGet");
      } // verify the required parameter 'fromDt' is set


      if (fromDt === undefined || fromDt === null) {
        throw new Error("Missing the required parameter 'fromDt' when calling juniferAuMeterPointsIdReadingsGet");
      } // verify the required parameter 'toDt' is set


      if (toDt === undefined || toDt === null) {
        throw new Error("Missing the required parameter 'toDt' when calling juniferAuMeterPointsIdReadingsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'fromDt': fromDt,
        'toDt': toDt,
        'status': this.apiClient.buildCollectionParam(opts['status'], 'csv')
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetMeterReadingsGetAuMeterpointsIdReadingsResponse["default"];
      return this.apiClient.callApi('/junifer/au/meterPoints/{id}/readings', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuMeterPointsIdReadingsPost operation.
     * @callback module:api/MeterpointsAustraliaApi~juniferAuMeterPointsIdReadingsPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Submit meter readings
     * Submits meter reading. If you don't know if a meter has technical details or not, first call \"Submit meter readings\", then if it fails with an error, call the \"Submit meter readings without meter technical details\".
     * @param {Number} id Meterpoint ID
     * @param {module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody} requestBody Request body
     * @param {module:api/MeterpointsAustraliaApi~juniferAuMeterPointsIdReadingsPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAuMeterPointsIdReadingsPost",
    value: function juniferAuMeterPointsIdReadingsPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuMeterPointsIdReadingsPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuMeterPointsIdReadingsPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/au/meterPoints/{id}/readings', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuMeterPointsMeterStructureGet operation.
     * @callback module:api/MeterpointsAustraliaApi~juniferAuMeterPointsMeterStructureGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get meterpoint structure
     * Get meterpoint structure
     * @param {String} id Meterpoint Identifier
     * @param {String} effectiveDt Show meterpoint structure at the specific date
     * @param {module:api/MeterpointsAustraliaApi~juniferAuMeterPointsMeterStructureGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200}
     */

  }, {
    key: "juniferAuMeterPointsMeterStructureGet",
    value: function juniferAuMeterPointsMeterStructureGet(id, effectiveDt, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuMeterPointsMeterStructureGet");
      } // verify the required parameter 'effectiveDt' is set


      if (effectiveDt === undefined || effectiveDt === null) {
        throw new Error("Missing the required parameter 'effectiveDt' when calling juniferAuMeterPointsMeterStructureGet");
      }

      var pathParams = {};
      var queryParams = {
        'id': id,
        'effectiveDt': effectiveDt
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetMeterStructureGetAuMeterpointsMeterstructureResponse["default"];
      return this.apiClient.callApi('/junifer/au/meterPoints/meterStructure', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return MeterpointsAustraliaApi;
}();

exports["default"] = MeterpointsAustraliaApi;