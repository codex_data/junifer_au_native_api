"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CancelTicketPostTicketsIdCancelticketResponse = _interopRequireDefault(require("../model/CancelTicketPostTicketsIdCancelticketResponse400"));

var _GetTicketGetTicketsIdResponse = _interopRequireDefault(require("../model/GetTicketGetTicketsIdResponse200"));

var _GetTicketGetTicketsIdResponse2 = _interopRequireDefault(require("../model/GetTicketGetTicketsIdResponse400"));

var _UpdateTicketPutTicketsIdRequestBody = _interopRequireDefault(require("../model/UpdateTicketPutTicketsIdRequestBody"));

var _UpdateTicketPutTicketsIdResponse = _interopRequireDefault(require("../model/UpdateTicketPutTicketsIdResponse400"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Tickets service.
* @module api/TicketsApi
* @version 1.61.1
*/
var TicketsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new TicketsApi. 
  * @alias module:api/TicketsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function TicketsApi(apiClient) {
    _classCallCheck(this, TicketsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferTicketsIdCancelTicketPost operation.
   * @callback module:api/TicketsApi~juniferTicketsIdCancelTicketPostCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Cancel Ticket
   * Cancels a ticket.
   * @param {Number} id Ticket ID
   * @param {String} ticketCancelReason The reason for the ticket cancellation
   * @param {module:api/TicketsApi~juniferTicketsIdCancelTicketPostCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(TicketsApi, [{
    key: "juniferTicketsIdCancelTicketPost",
    value: function juniferTicketsIdCancelTicketPost(id, ticketCancelReason, callback) {
      var postBody = ticketCancelReason; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferTicketsIdCancelTicketPost");
      } // verify the required parameter 'ticketCancelReason' is set


      if (ticketCancelReason === undefined || ticketCancelReason === null) {
        throw new Error("Missing the required parameter 'ticketCancelReason' when calling juniferTicketsIdCancelTicketPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/tickets/{id}/cancelTicket', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferTicketsIdGet operation.
     * @callback module:api/TicketsApi~juniferTicketsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetTicketGetTicketsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get ticket
     * Gets ticket by its ID
     * @param {Number} id Ticket ID
     * @param {module:api/TicketsApi~juniferTicketsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetTicketGetTicketsIdResponse200}
     */

  }, {
    key: "juniferTicketsIdGet",
    value: function juniferTicketsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferTicketsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetTicketGetTicketsIdResponse["default"];
      return this.apiClient.callApi('/junifer/tickets/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferTicketsIdPut operation.
     * @callback module:api/TicketsApi~juniferTicketsIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update Ticket
     * Update a ticket.
     * @param {Number} id Ticket ID
     * @param {module:model/UpdateTicketPutTicketsIdRequestBody} requestBody Request body
     * @param {module:api/TicketsApi~juniferTicketsIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferTicketsIdPut",
    value: function juniferTicketsIdPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferTicketsIdPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferTicketsIdPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/tickets/{id}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return TicketsApi;
}();

exports["default"] = TicketsApi;