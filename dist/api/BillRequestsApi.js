"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBillRequestGetBillrequestsIdResponse = _interopRequireDefault(require("../model/GetBillRequestGetBillrequestsIdResponse200"));

var _GetBillRequestGetBillrequestsIdResponse2 = _interopRequireDefault(require("../model/GetBillRequestGetBillrequestsIdResponse404"));

var _RestartFailedBillRequestPostBillrequestsIdRestartfailedResponse = _interopRequireDefault(require("../model/RestartFailedBillRequestPostBillrequestsIdRestartfailedResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* BillRequests service.
* @module api/BillRequestsApi
* @version 1.61.1
*/
var BillRequestsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new BillRequestsApi. 
  * @alias module:api/BillRequestsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function BillRequestsApi(apiClient) {
    _classCallCheck(this, BillRequestsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferBillRequestsIdGet operation.
   * @callback module:api/BillRequestsApi~juniferBillRequestsIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetBillRequestGetBillrequestsIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get bill request
   * Get bill request record by its ID
   * @param {Number} id BillRequest ID
   * @param {module:api/BillRequestsApi~juniferBillRequestsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetBillRequestGetBillrequestsIdResponse200}
   */


  _createClass(BillRequestsApi, [{
    key: "juniferBillRequestsIdGet",
    value: function juniferBillRequestsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillRequestsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBillRequestGetBillrequestsIdResponse["default"];
      return this.apiClient.callApi('/junifer/billRequests/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBillRequestsIdRestartFailedPost operation.
     * @callback module:api/BillRequestsApi~juniferBillRequestsIdRestartFailedPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Restart failed bill request
     * Restart failed bill request - bill request's status code must not be Completed or Superseded
     * @param {Number} id BillRequest ID
     * @param {module:api/BillRequestsApi~juniferBillRequestsIdRestartFailedPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferBillRequestsIdRestartFailedPost",
    value: function juniferBillRequestsIdRestartFailedPost(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillRequestsIdRestartFailedPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/billRequests/{id}/restartFailed', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return BillRequestsApi;
}();

exports["default"] = BillRequestsApi;