"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AddPropertiesPostProspectsIdAddPropertiesResponse = _interopRequireDefault(require("../model/AddPropertiesPostProspectsIdAddPropertiesResponse400"));

var _CreateProspectPostProspectsRequestBody = _interopRequireDefault(require("../model/CreateProspectPostProspectsRequestBody"));

var _CreateProspectPostProspectsResponse = _interopRequireDefault(require("../model/CreateProspectPostProspectsResponse200"));

var _CreateProspectPostProspectsResponse2 = _interopRequireDefault(require("../model/CreateProspectPostProspectsResponse400"));

var _GetBrokerGetBrokersIdResponse = _interopRequireDefault(require("../model/GetBrokerGetBrokersIdResponse200"));

var _GetBrokerGetBrokersIdResponse2 = _interopRequireDefault(require("../model/GetBrokerGetBrokersIdResponse404"));

var _GetBrokerLinkageGetBrokerlinkagesIdResponse = _interopRequireDefault(require("../model/GetBrokerLinkageGetBrokerlinkagesIdResponse200"));

var _GetBrokerLinkageGetBrokerlinkagesIdResponse2 = _interopRequireDefault(require("../model/GetBrokerLinkageGetBrokerlinkagesIdResponse404"));

var _GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse = _interopRequireDefault(require("../model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200"));

var _GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse2 = _interopRequireDefault(require("../model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse404"));

var _GetProspectGetProspectsIdResponse = _interopRequireDefault(require("../model/GetProspectGetProspectsIdResponse200"));

var _GetProspectGetProspectsIdResponse2 = _interopRequireDefault(require("../model/GetProspectGetProspectsIdResponse404"));

var _GetProspectOffersGetProspectsIdOffersResponse = _interopRequireDefault(require("../model/GetProspectOffersGetProspectsIdOffersResponse200"));

var _GetProspectOffersGetProspectsIdOffersResponse2 = _interopRequireDefault(require("../model/GetProspectOffersGetProspectsIdOffersResponse404"));

var _GetProspectPropertiesGetProspectsIdPropertysResponse = _interopRequireDefault(require("../model/GetProspectPropertiesGetProspectsIdPropertysResponse200"));

var _GetProspectPropertiesGetProspectsIdPropertysResponse2 = _interopRequireDefault(require("../model/GetProspectPropertiesGetProspectsIdPropertysResponse404"));

var _LookupBrokerGetBrokersResponse = _interopRequireDefault(require("../model/LookupBrokerGetBrokersResponse200"));

var _LookupBrokerGetBrokersResponse2 = _interopRequireDefault(require("../model/LookupBrokerGetBrokersResponse400"));

var _LookupBrokerGetBrokersResponse3 = _interopRequireDefault(require("../model/LookupBrokerGetBrokersResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Prospects service.
* @module api/ProspectsApi
* @version 1.61.1
*/
var ProspectsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ProspectsApi. 
  * @alias module:api/ProspectsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ProspectsApi(apiClient) {
    _classCallCheck(this, ProspectsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferBrokerLinkagesIdGet operation.
   * @callback module:api/ProspectsApi~juniferBrokerLinkagesIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get Broker Linkage
   * Returns Broker Linkage of a Broker by their id
   * @param {String} id Broker Linkage id number
   * @param {module:api/ProspectsApi~juniferBrokerLinkagesIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse200}
   */


  _createClass(ProspectsApi, [{
    key: "juniferBrokerLinkagesIdGet",
    value: function juniferBrokerLinkagesIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBrokerLinkagesIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBrokerLinkageGetBrokerlinkagesIdResponse["default"];
      return this.apiClient.callApi('/junifer/brokerLinkages/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBrokersGet operation.
     * @callback module:api/ProspectsApi~juniferBrokersGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LookupBrokerGetBrokersResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Lookup broker
     * Searches for a broker by the specified code. If no broker can be found an empty `results` array is returned.
     * @param {String} code Broker's unique code
     * @param {module:api/ProspectsApi~juniferBrokersGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/LookupBrokerGetBrokersResponse200}
     */

  }, {
    key: "juniferBrokersGet",
    value: function juniferBrokersGet(code, callback) {
      var postBody = null; // verify the required parameter 'code' is set

      if (code === undefined || code === null) {
        throw new Error("Missing the required parameter 'code' when calling juniferBrokersGet");
      }

      var pathParams = {};
      var queryParams = {
        'code': code
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _LookupBrokerGetBrokersResponse["default"];
      return this.apiClient.callApi('/junifer/brokers', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBrokersIdBrokerLinkagesGet operation.
     * @callback module:api/ProspectsApi~juniferBrokersIdBrokerLinkagesGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get BrokerLinkages for Broker
     * Returns customers and prospects owned by the supplied broker. If an empty results list is returned no customers or prospects could be found
     * @param {String} id Broker id number
     * @param {Object} opts Optional parameters
     * @param {String} opts.type Linkage Type. Can be one of the following: `Customer, Prospect`. Will return all if no type is specified.
     * @param {String} opts.effectiveDate The date for which broker linkages will be checked against. If no date is supplied the returned linkages will be valid for today's date.
     * @param {module:api/ProspectsApi~juniferBrokersIdBrokerLinkagesGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200}
     */

  }, {
    key: "juniferBrokersIdBrokerLinkagesGet",
    value: function juniferBrokersIdBrokerLinkagesGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBrokersIdBrokerLinkagesGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'type': opts['type'],
        'effectiveDate': opts['effectiveDate']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse["default"];
      return this.apiClient.callApi('/junifer/brokers/{id}/brokerLinkages', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBrokersIdGet operation.
     * @callback module:api/ProspectsApi~juniferBrokersIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBrokerGetBrokersIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get Broker
     * Gets the broker by its Id
     * @param {Number} id Broker id number
     * @param {module:api/ProspectsApi~juniferBrokersIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBrokerGetBrokersIdResponse200}
     */

  }, {
    key: "juniferBrokersIdGet",
    value: function juniferBrokersIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBrokersIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBrokerGetBrokersIdResponse["default"];
      return this.apiClient.callApi('/junifer/brokers/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferProspectsIdAddPropertiesPost operation.
     * @callback module:api/ProspectsApi~juniferProspectsIdAddPropertiesPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add Properties
     * Links properties to the prospect
     * @param {Number} id Prospect ID
     * @param {Object} requestBody Request body
     * @param {module:api/ProspectsApi~juniferProspectsIdAddPropertiesPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferProspectsIdAddPropertiesPost",
    value: function juniferProspectsIdAddPropertiesPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferProspectsIdAddPropertiesPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferProspectsIdAddPropertiesPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/prospects/{id}/add_properties', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferProspectsIdGet operation.
     * @callback module:api/ProspectsApi~juniferProspectsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetProspectGetProspectsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get prospect
     * Gets prospect by its ID
     * @param {Number} id Prospect ID
     * @param {module:api/ProspectsApi~juniferProspectsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetProspectGetProspectsIdResponse200}
     */

  }, {
    key: "juniferProspectsIdGet",
    value: function juniferProspectsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferProspectsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetProspectGetProspectsIdResponse["default"];
      return this.apiClient.callApi('/junifer/prospects/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferProspectsIdOffersGet operation.
     * @callback module:api/ProspectsApi~juniferProspectsIdOffersGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetProspectOffersGetProspectsIdOffersResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get prospect offers
     * Gets the list of offers that are linked to a prospect by the prospect id.
     * @param {Number} id Prospect's id
     * @param {module:api/ProspectsApi~juniferProspectsIdOffersGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetProspectOffersGetProspectsIdOffersResponse200}
     */

  }, {
    key: "juniferProspectsIdOffersGet",
    value: function juniferProspectsIdOffersGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferProspectsIdOffersGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetProspectOffersGetProspectsIdOffersResponse["default"];
      return this.apiClient.callApi('/junifer/prospects/{id}/offers', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferProspectsIdPropertysGet operation.
     * @callback module:api/ProspectsApi~juniferProspectsIdPropertysGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get prospect's properties
     * Returns properties owned by the supplied prospect. If an empty results list is returned no properties could be found
     * @param {Number} id Prospect ID
     * @param {module:api/ProspectsApi~juniferProspectsIdPropertysGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200}
     */

  }, {
    key: "juniferProspectsIdPropertysGet",
    value: function juniferProspectsIdPropertysGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferProspectsIdPropertysGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetProspectPropertiesGetProspectsIdPropertysResponse["default"];
      return this.apiClient.callApi('/junifer/prospects/{id}/propertys', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferProspectsPost operation.
     * @callback module:api/ProspectsApi~juniferProspectsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateProspectPostProspectsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create Prospect
     * Create a new prospect with a new customer
     * @param {module:model/CreateProspectPostProspectsRequestBody} requestBody Request body
     * @param {module:api/ProspectsApi~juniferProspectsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateProspectPostProspectsResponse200}
     */

  }, {
    key: "juniferProspectsPost",
    value: function juniferProspectsPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferProspectsPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _CreateProspectPostProspectsResponse["default"];
      return this.apiClient.callApi('/junifer/prospects', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ProspectsApi;
}();

exports["default"] = ProspectsApi;