"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AusGetDiscountsGetAuAccountsIdDiscountsResponse = _interopRequireDefault(require("../model/AusGetDiscountsGetAuAccountsIdDiscountsResponse200"));

var _AusLinkDiscountPostAuAccountsIdDiscountsRequestBody = _interopRequireDefault(require("../model/AusLinkDiscountPostAuAccountsIdDiscountsRequestBody"));

var _AusRenewAccountPostAuAccountsIdRenewalRequestBody = _interopRequireDefault(require("../model/AusRenewAccountPostAuAccountsIdRenewalRequestBody"));

var _AusRenewAccountPostAuAccountsIdRenewalResponse = _interopRequireDefault(require("../model/AusRenewAccountPostAuAccountsIdRenewalResponse200"));

var _AusRenewAccountPostAuAccountsIdRenewalResponse2 = _interopRequireDefault(require("../model/AusRenewAccountPostAuAccountsIdRenewalResponse400"));

var _AusRenewAccountPostAuAccountsIdRenewalResponse3 = _interopRequireDefault(require("../model/AusRenewAccountPostAuAccountsIdRenewalResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* AccountsAustralia service.
* @module api/AccountsAustraliaApi
* @version 1.61.1
*/
var AccountsAustraliaApi = /*#__PURE__*/function () {
  /**
  * Constructs a new AccountsAustraliaApi. 
  * @alias module:api/AccountsAustraliaApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function AccountsAustraliaApi(apiClient) {
    _classCallCheck(this, AccountsAustraliaApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAuAccountsIdDiscountsGet operation.
   * @callback module:api/AccountsAustraliaApi~juniferAuAccountsIdDiscountsGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/AusGetDiscountsGetAuAccountsIdDiscountsResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get discounts linked to the account specified by the ID
   * Get account's discounts. This is an Australia specific API.
   * @param {Number} id Account ID
   * @param {module:api/AccountsAustraliaApi~juniferAuAccountsIdDiscountsGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/AusGetDiscountsGetAuAccountsIdDiscountsResponse200}
   */


  _createClass(AccountsAustraliaApi, [{
    key: "juniferAuAccountsIdDiscountsGet",
    value: function juniferAuAccountsIdDiscountsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuAccountsIdDiscountsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _AusGetDiscountsGetAuAccountsIdDiscountsResponse["default"];
      return this.apiClient.callApi('/junifer/au/accounts/{id}/discounts', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuAccountsIdDiscountsPost operation.
     * @callback module:api/AccountsAustraliaApi~juniferAuAccountsIdDiscountsPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Links a discount to the account specified by the ID
     * Links a discount to the account specified by the ID. This is an Australia specific API.
     * @param {Number} id Account ID
     * @param {module:model/AusLinkDiscountPostAuAccountsIdDiscountsRequestBody} requestBody Request body
     * @param {module:api/AccountsAustraliaApi~juniferAuAccountsIdDiscountsPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAuAccountsIdDiscountsPost",
    value: function juniferAuAccountsIdDiscountsPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuAccountsIdDiscountsPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuAccountsIdDiscountsPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/junifer/au/accounts/{id}/discounts', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuAccountsIdRenewalPost operation.
     * @callback module:api/AccountsAustraliaApi~juniferAuAccountsIdRenewalPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Renew account with one or more new agreements
     * Renews the account specified by the account ID by creating one or more new agreements, which can optionally replace one or more existing agreements specified. Electricity product details of the new agreement(s) must be supplied. Will raise a ticket, if one is specified in the property `Agreement Creation Properties > API Agreement Setup Ticket`. This is an Australia specific API not to be confused with `junifer/accounts/{id}/renewal`.
     * @param {Number} id Account ID
     * @param {module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBody} requestBody Request body
     * @param {module:api/AccountsAustraliaApi~juniferAuAccountsIdRenewalPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200}
     */

  }, {
    key: "juniferAuAccountsIdRenewalPost",
    value: function juniferAuAccountsIdRenewalPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuAccountsIdRenewalPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuAccountsIdRenewalPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _AusRenewAccountPostAuAccountsIdRenewalResponse["default"];
      return this.apiClient.callApi('/junifer/au/accounts/{id}/renewal', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return AccountsAustraliaApi;
}();

exports["default"] = AccountsAustraliaApi;