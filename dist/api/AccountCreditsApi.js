"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CancelAccountCreditDeleteAccountcreditsIdResponse = _interopRequireDefault(require("../model/CancelAccountCreditDeleteAccountcreditsIdResponse400"));

var _GetAccountCreditGetAccountcreditsIdResponse = _interopRequireDefault(require("../model/GetAccountCreditGetAccountcreditsIdResponse200"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* AccountCredits service.
* @module api/AccountCreditsApi
* @version 1.61.1
*/
var AccountCreditsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new AccountCreditsApi. 
  * @alias module:api/AccountCreditsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function AccountCreditsApi(apiClient) {
    _classCallCheck(this, AccountCreditsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAccountCreditsIdDelete operation.
   * @callback module:api/AccountCreditsApi~juniferAccountCreditsIdDeleteCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Cancel account credit
   * Cancel an existing account credit for an account. This action is not reversible. Authentication must be active
   * @param {Number} id Account credit ID number
   * @param {module:api/AccountCreditsApi~juniferAccountCreditsIdDeleteCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(AccountCreditsApi, [{
    key: "juniferAccountCreditsIdDelete",
    value: function juniferAccountCreditsIdDelete(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountCreditsIdDelete");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accountCredits/{id}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountCreditsIdGet operation.
     * @callback module:api/AccountCreditsApi~juniferAccountCreditsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountCreditGetAccountcreditsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get Account Credit
     * Gets an account credit by its ID
     * @param {Number} id Account Credit ID
     * @param {module:api/AccountCreditsApi~juniferAccountCreditsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountCreditGetAccountcreditsIdResponse200}
     */

  }, {
    key: "juniferAccountCreditsIdGet",
    value: function juniferAccountCreditsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountCreditsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountCreditGetAccountcreditsIdResponse["default"];
      return this.apiClient.callApi('/junifer/accountCredits/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return AccountCreditsApi;
}();

exports["default"] = AccountCreditsApi;