"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetQuoteFileGetQuotefilesIdResponse = _interopRequireDefault(require("../model/GetQuoteFileGetQuotefilesIdResponse200"));

var _GetQuoteFileGetQuotefilesIdResponse2 = _interopRequireDefault(require("../model/GetQuoteFileGetQuotefilesIdResponse404"));

var _GetQuoteFileImageGetQuotefilesIdImageResponse = _interopRequireDefault(require("../model/GetQuoteFileImageGetQuotefilesIdImageResponse200"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* QuoteFiles service.
* @module api/QuoteFilesApi
* @version 1.61.1
*/
var QuoteFilesApi = /*#__PURE__*/function () {
  /**
  * Constructs a new QuoteFilesApi. 
  * @alias module:api/QuoteFilesApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function QuoteFilesApi(apiClient) {
    _classCallCheck(this, QuoteFilesApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferQuoteFilesIdGet operation.
   * @callback module:api/QuoteFilesApi~juniferQuoteFilesIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetQuoteFileGetQuotefilesIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get quote file
   * Get quote file record by its ID
   * @param {Number} id Quote file ID
   * @param {module:api/QuoteFilesApi~juniferQuoteFilesIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetQuoteFileGetQuotefilesIdResponse200}
   */


  _createClass(QuoteFilesApi, [{
    key: "juniferQuoteFilesIdGet",
    value: function juniferQuoteFilesIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferQuoteFilesIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetQuoteFileGetQuotefilesIdResponse["default"];
      return this.apiClient.callApi('/junifer/quoteFiles/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferQuoteFilesIdImageGet operation.
     * @callback module:api/QuoteFilesApi~juniferQuoteFilesIdImageGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetQuoteFileImageGetQuotefilesIdImageResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get quote file image
     * Initiates quote file image (usually PDF file) download if the file is available
     * @param {Number} id Quote file ID
     * @param {module:api/QuoteFilesApi~juniferQuoteFilesIdImageGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetQuoteFileImageGetQuotefilesIdImageResponse200}
     */

  }, {
    key: "juniferQuoteFilesIdImageGet",
    value: function juniferQuoteFilesIdImageGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferQuoteFilesIdImageGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetQuoteFileImageGetQuotefilesIdImageResponse["default"];
      return this.apiClient.callApi('/junifer/quoteFiles/{id}/image', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return QuoteFilesApi;
}();

exports["default"] = QuoteFilesApi;