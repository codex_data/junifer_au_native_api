"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse = _interopRequireDefault(require("../model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200"));

var _CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse2 = _interopRequireDefault(require("../model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse400"));

var _CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse3 = _interopRequireDefault(require("../model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401"));

var _GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse = _interopRequireDefault(require("../model/GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200"));

var _GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse = _interopRequireDefault(require("../model/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200"));

var _GetSepaDirectDebitGetSepadirectdebitsIdResponse = _interopRequireDefault(require("../model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200"));

var _GetStripePaymentCardGetStripepaymentcardsIdResponse = _interopRequireDefault(require("../model/GetStripePaymentCardGetStripepaymentcardsIdResponse200"));

var _GetStripePaymentCardGetStripepaymentcardsIdResponse2 = _interopRequireDefault(require("../model/GetStripePaymentCardGetStripepaymentcardsIdResponse404"));

var _GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse = _interopRequireDefault(require("../model/GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200"));

var _NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody = _interopRequireDefault(require("../model/NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody"));

var _NewBpointDirectDebitPostAuBpointdirectdebitsResponse = _interopRequireDefault(require("../model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200"));

var _NewBpointDirectDebitPostAuBpointdirectdebitsResponse2 = _interopRequireDefault(require("../model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse400"));

var _NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody = _interopRequireDefault(require("../model/NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody"));

var _NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse = _interopRequireDefault(require("../model/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200"));

var _NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse2 = _interopRequireDefault(require("../model/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse400"));

var _NewSepaDirectDebitPostSepadirectdebitsRequestBody = _interopRequireDefault(require("../model/NewSepaDirectDebitPostSepadirectdebitsRequestBody"));

var _NewSepaDirectDebitPostSepadirectdebitsResponse = _interopRequireDefault(require("../model/NewSepaDirectDebitPostSepadirectdebitsResponse200"));

var _NewSepaDirectDebitPostSepadirectdebitsResponse2 = _interopRequireDefault(require("../model/NewSepaDirectDebitPostSepadirectdebitsResponse400"));

var _NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody = _interopRequireDefault(require("../model/NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody"));

var _NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse = _interopRequireDefault(require("../model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200"));

var _NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse2 = _interopRequireDefault(require("../model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400"));

var _StoreCardPostStripepaymentcardsRequestBody = _interopRequireDefault(require("../model/StoreCardPostStripepaymentcardsRequestBody"));

var _StoreCardPostStripepaymentcardsResponse = _interopRequireDefault(require("../model/StoreCardPostStripepaymentcardsResponse400"));

var _UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody = _interopRequireDefault(require("../model/UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody"));

var _UpdateSepaDirectDebitPutSepadirectdebitsIdResponse = _interopRequireDefault(require("../model/UpdateSepaDirectDebitPutSepadirectdebitsIdResponse400"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* CustomerPayments service.
* @module api/CustomerPaymentsApi
* @version 1.61.1
*/
var CustomerPaymentsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CustomerPaymentsApi. 
  * @alias module:api/CustomerPaymentsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CustomerPaymentsApi(apiClient) {
    _classCallCheck(this, CustomerPaymentsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAuBpointDirectDebitsIdDelete operation.
   * @callback module:api/CustomerPaymentsApi~juniferAuBpointDirectDebitsIdDeleteCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Cancel BPOINT Direct Debit Instruction
   * Cancel BPOINT Direct Debit, delete future account payment methods for this Direct Debit
   * @param {Number} id BPOINT Direct Debit ID
   * @param {module:api/CustomerPaymentsApi~juniferAuBpointDirectDebitsIdDeleteCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(CustomerPaymentsApi, [{
    key: "juniferAuBpointDirectDebitsIdDelete",
    value: function juniferAuBpointDirectDebitsIdDelete(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuBpointDirectDebitsIdDelete");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/junifer/au/bpointDirectDebits/{id}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuBpointDirectDebitsIdGet operation.
     * @callback module:api/CustomerPaymentsApi~juniferAuBpointDirectDebitsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Existing BPOINT Direct Debit Instruction
     * Gets a BPOINT Direct Debit mandate by its ID
     * @param {Number} id BPOINT Direct Debit ID
     * @param {module:api/CustomerPaymentsApi~juniferAuBpointDirectDebitsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200}
     */

  }, {
    key: "juniferAuBpointDirectDebitsIdGet",
    value: function juniferAuBpointDirectDebitsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuBpointDirectDebitsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse["default"];
      return this.apiClient.callApi('/junifer/au/bpointDirectDebits/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuBpointDirectDebitsPost operation.
     * @callback module:api/CustomerPaymentsApi~juniferAuBpointDirectDebitsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * New BPOINT Direct Debit
     * Creates a new BPOINT Direct Debit payment method for the account specified in the `account.id` field.
     * @param {module:model/NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody} requestBody Request body
     * @param {module:api/CustomerPaymentsApi~juniferAuBpointDirectDebitsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200}
     */

  }, {
    key: "juniferAuBpointDirectDebitsPost",
    value: function juniferAuBpointDirectDebitsPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuBpointDirectDebitsPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _NewBpointDirectDebitPostAuBpointdirectdebitsResponse["default"];
      return this.apiClient.callApi('/junifer/au/bpointDirectDebits', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuPayPalDirectDebitsIdDelete operation.
     * @callback module:api/CustomerPaymentsApi~juniferAuPayPalDirectDebitsIdDeleteCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Cancel PayPal Direct Debit Instruction
     * Cancel PayPal Direct Debit, delete future account payment methods for this Direct Debit
     * @param {Number} id PayPal Direct Debit ID
     * @param {module:api/CustomerPaymentsApi~juniferAuPayPalDirectDebitsIdDeleteCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAuPayPalDirectDebitsIdDelete",
    value: function juniferAuPayPalDirectDebitsIdDelete(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuPayPalDirectDebitsIdDelete");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/junifer/au/payPalDirectDebits/{id}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuPayPalDirectDebitsIdGet operation.
     * @callback module:api/CustomerPaymentsApi~juniferAuPayPalDirectDebitsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Existing PayPal Direct Debit Instruction
     * Gets a PayPal Direct Debit mandate by its ID
     * @param {Number} id PayPal Direct Debit ID
     * @param {module:api/CustomerPaymentsApi~juniferAuPayPalDirectDebitsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200}
     */

  }, {
    key: "juniferAuPayPalDirectDebitsIdGet",
    value: function juniferAuPayPalDirectDebitsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuPayPalDirectDebitsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse["default"];
      return this.apiClient.callApi('/junifer/au/payPalDirectDebits/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuPayPalDirectDebitsPost operation.
     * @callback module:api/CustomerPaymentsApi~juniferAuPayPalDirectDebitsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * New PayPal Direct Debit
     * Creates a new PayPal Direct Debit payment method for the account specified in the `account.id` field.
     * @param {module:model/NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody} requestBody Request body
     * @param {module:api/CustomerPaymentsApi~juniferAuPayPalDirectDebitsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200}
     */

  }, {
    key: "juniferAuPayPalDirectDebitsPost",
    value: function juniferAuPayPalDirectDebitsPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuPayPalDirectDebitsPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse["default"];
      return this.apiClient.callApi('/junifer/au/payPalDirectDebits', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuWestpacDirectDebitsIdDelete operation.
     * @callback module:api/CustomerPaymentsApi~juniferAuWestpacDirectDebitsIdDeleteCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Cancel Westpac Direct Debit Instruction
     * Cancel Westpac Direct Debit, delete future account payment methods for this Direct Debit
     * @param {Number} id Westpac Direct Debit ID
     * @param {module:api/CustomerPaymentsApi~juniferAuWestpacDirectDebitsIdDeleteCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferAuWestpacDirectDebitsIdDelete",
    value: function juniferAuWestpacDirectDebitsIdDelete(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuWestpacDirectDebitsIdDelete");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/junifer/au/westpacDirectDebits/{id}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuWestpacDirectDebitsIdGet operation.
     * @callback module:api/CustomerPaymentsApi~juniferAuWestpacDirectDebitsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Existing Westpac Direct Debit Instruction
     * Gets a Westpac Direct Debit mandate by its ID
     * @param {Number} id Westpac Direct Debit ID
     * @param {module:api/CustomerPaymentsApi~juniferAuWestpacDirectDebitsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200}
     */

  }, {
    key: "juniferAuWestpacDirectDebitsIdGet",
    value: function juniferAuWestpacDirectDebitsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAuWestpacDirectDebitsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse["default"];
      return this.apiClient.callApi('/junifer/au/westpacDirectDebits/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAuWestpacDirectDebitsPost operation.
     * @callback module:api/CustomerPaymentsApi~juniferAuWestpacDirectDebitsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * New Westpac Direct Debit
     * Creates a new Westpac Direct Debit payment method for the account specified in the `account.id` field.
     * @param {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody} requestBody Request body
     * @param {module:api/CustomerPaymentsApi~juniferAuWestpacDirectDebitsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200}
     */

  }, {
    key: "juniferAuWestpacDirectDebitsPost",
    value: function juniferAuWestpacDirectDebitsPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuWestpacDirectDebitsPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse["default"];
      return this.apiClient.callApi('/junifer/au/westpacDirectDebits', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferSepaDirectDebitsIdDelete operation.
     * @callback module:api/CustomerPaymentsApi~juniferSepaDirectDebitsIdDeleteCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Cancel SEPA Direct Debit Instruction
     * Cancel SEPA Direct Debit, delete future account payment methods for this Direct Debit
     * @param {Number} id SEPA Direct Debit ID
     * @param {module:api/CustomerPaymentsApi~juniferSepaDirectDebitsIdDeleteCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferSepaDirectDebitsIdDelete",
    value: function juniferSepaDirectDebitsIdDelete(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferSepaDirectDebitsIdDelete");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/junifer/sepaDirectDebits/{id}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferSepaDirectDebitsIdGet operation.
     * @callback module:api/CustomerPaymentsApi~juniferSepaDirectDebitsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Existing SEPA Direct Debit Instruction
     * Gets a SEPA Direct Debit mandate by its ID
     * @param {Number} id SEPA Direct Debit ID
     * @param {module:api/CustomerPaymentsApi~juniferSepaDirectDebitsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200}
     */

  }, {
    key: "juniferSepaDirectDebitsIdGet",
    value: function juniferSepaDirectDebitsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferSepaDirectDebitsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetSepaDirectDebitGetSepadirectdebitsIdResponse["default"];
      return this.apiClient.callApi('/junifer/sepaDirectDebits/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferSepaDirectDebitsIdPut operation.
     * @callback module:api/CustomerPaymentsApi~juniferSepaDirectDebitsIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update SEPA Direct Debit Mandate
     * Updates SEPA Direct Debit details
     * @param {Number} id SEPA Direct Debit ID
     * @param {module:model/UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody} requestBody Request body
     * @param {module:api/CustomerPaymentsApi~juniferSepaDirectDebitsIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferSepaDirectDebitsIdPut",
    value: function juniferSepaDirectDebitsIdPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferSepaDirectDebitsIdPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferSepaDirectDebitsIdPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/sepaDirectDebits/{id}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferSepaDirectDebitsPost operation.
     * @callback module:api/CustomerPaymentsApi~juniferSepaDirectDebitsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * New SEPA Direct Debit Mandate
     * Creates a new SEPA Direct Debit instruction for the account specified in the `account.id` field.
     * @param {module:model/NewSepaDirectDebitPostSepadirectdebitsRequestBody} requestBody Request body
     * @param {module:api/CustomerPaymentsApi~juniferSepaDirectDebitsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/NewSepaDirectDebitPostSepadirectdebitsResponse200}
     */

  }, {
    key: "juniferSepaDirectDebitsPost",
    value: function juniferSepaDirectDebitsPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferSepaDirectDebitsPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _NewSepaDirectDebitPostSepadirectdebitsResponse["default"];
      return this.apiClient.callApi('/junifer/sepaDirectDebits', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferStripePaymentCardsIdDelete operation.
     * @callback module:api/CustomerPaymentsApi~juniferStripePaymentCardsIdDeleteCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Cancel Stripe Payment Card
     * Cancels Go Stripe Payment Card, delete future account payment methods for this Stripe Payment Card
     * @param {Number} id Stripe Payment Card ID
     * @param {module:api/CustomerPaymentsApi~juniferStripePaymentCardsIdDeleteCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferStripePaymentCardsIdDelete",
    value: function juniferStripePaymentCardsIdDelete(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferStripePaymentCardsIdDelete");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/junifer/stripePaymentCards/{id}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferStripePaymentCardsIdGet operation.
     * @callback module:api/CustomerPaymentsApi~juniferStripePaymentCardsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Existing Stripe Payment Card Instruction
     * Gets a Stripe Payment Card by its ID for further instruction
     * @param {Number} id Stripe Payment Card  ID
     * @param {module:api/CustomerPaymentsApi~juniferStripePaymentCardsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200}
     */

  }, {
    key: "juniferStripePaymentCardsIdGet",
    value: function juniferStripePaymentCardsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferStripePaymentCardsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetStripePaymentCardGetStripepaymentcardsIdResponse["default"];
      return this.apiClient.callApi('/junifer/stripePaymentCards/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferStripePaymentCardsIdPaymentsPost operation.
     * @callback module:api/CustomerPaymentsApi~juniferStripePaymentCardsIdPaymentsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create Stripe Card Payment Collection
     * Schedules a Stripe Card payment. Authentication must be active
     * @param {Number} id Stripe Card Payment ID
     * @param {Number} amount Payment collection amount
     * @param {module:api/CustomerPaymentsApi~juniferStripePaymentCardsIdPaymentsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200}
     */

  }, {
    key: "juniferStripePaymentCardsIdPaymentsPost",
    value: function juniferStripePaymentCardsIdPaymentsPost(id, amount, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferStripePaymentCardsIdPaymentsPost");
      } // verify the required parameter 'amount' is set


      if (amount === undefined || amount === null) {
        throw new Error("Missing the required parameter 'amount' when calling juniferStripePaymentCardsIdPaymentsPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'amount': amount
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse["default"];
      return this.apiClient.callApi('/junifer/stripePaymentCards/{id}/payments', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferStripePaymentCardsPost operation.
     * @callback module:api/CustomerPaymentsApi~juniferStripePaymentCardsPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Store Stripe Payment Card
     * Add a new Stripe Payment Card. Authentication must be activeFrom September 14th 2019 onward expMonth, expYear and last4 should not be provided.
     * @param {module:model/StoreCardPostStripepaymentcardsRequestBody} requestBody Request body
     * @param {module:api/CustomerPaymentsApi~juniferStripePaymentCardsPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferStripePaymentCardsPost",
    value: function juniferStripePaymentCardsPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferStripePaymentCardsPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/stripePaymentCards', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CustomerPaymentsApi;
}();

exports["default"] = CustomerPaymentsApi;