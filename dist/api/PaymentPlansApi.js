"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse = _interopRequireDefault(require("../model/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* PaymentPlans service.
* @module api/PaymentPlansApi
* @version 1.61.1
*/
var PaymentPlansApi = /*#__PURE__*/function () {
  /**
  * Constructs a new PaymentPlansApi. 
  * @alias module:api/PaymentPlansApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function PaymentPlansApi(apiClient) {
    _classCallCheck(this, PaymentPlansApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferPaymentPlansIdPaymentPlanPaymentsGet operation.
   * @callback module:api/PaymentPlansApi~juniferPaymentPlansIdPaymentPlanPaymentsGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get paymentPlan's payments
   * Get paymentPlan's payments
   * @param {Number} id PaymentPlan ID
   * @param {module:api/PaymentPlansApi~juniferPaymentPlansIdPaymentPlanPaymentsGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200}
   */


  _createClass(PaymentPlansApi, [{
    key: "juniferPaymentPlansIdPaymentPlanPaymentsGet",
    value: function juniferPaymentPlansIdPaymentPlanPaymentsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentPlansIdPaymentPlanPaymentsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse["default"];
      return this.apiClient.callApi('/junifer/paymentPlans/{id}/paymentPlanPayments', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return PaymentPlansApi;
}();

exports["default"] = PaymentPlansApi;