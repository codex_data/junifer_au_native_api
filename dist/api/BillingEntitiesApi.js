"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBillingEntityGetBillingentitiesIdResponse = _interopRequireDefault(require("../model/GetBillingEntityGetBillingentitiesIdResponse200"));

var _GetBillingEntityGetBillingentitiesIdResponse2 = _interopRequireDefault(require("../model/GetBillingEntityGetBillingentitiesIdResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* BillingEntities service.
* @module api/BillingEntitiesApi
* @version 1.61.1
*/
var BillingEntitiesApi = /*#__PURE__*/function () {
  /**
  * Constructs a new BillingEntitiesApi. 
  * @alias module:api/BillingEntitiesApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function BillingEntitiesApi(apiClient) {
    _classCallCheck(this, BillingEntitiesApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferBillingEntitiesIdGet operation.
   * @callback module:api/BillingEntitiesApi~juniferBillingEntitiesIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetBillingEntityGetBillingentitiesIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get billing entity
   * Get billing entity by its ID
   * @param {Number} id Billing Entity ID
   * @param {module:api/BillingEntitiesApi~juniferBillingEntitiesIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetBillingEntityGetBillingentitiesIdResponse200}
   */


  _createClass(BillingEntitiesApi, [{
    key: "juniferBillingEntitiesIdGet",
    value: function juniferBillingEntitiesIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillingEntitiesIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBillingEntityGetBillingentitiesIdResponse["default"];
      return this.apiClient.callApi('/junifer/billingEntities/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return BillingEntitiesApi;
}();

exports["default"] = BillingEntitiesApi;