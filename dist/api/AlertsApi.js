"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAlertGetAlertsIdResponse = _interopRequireDefault(require("../model/GetAlertGetAlertsIdResponse200"));

var _GetAlertGetAlertsIdResponse2 = _interopRequireDefault(require("../model/GetAlertGetAlertsIdResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Alerts service.
* @module api/AlertsApi
* @version 1.61.1
*/
var AlertsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new AlertsApi. 
  * @alias module:api/AlertsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function AlertsApi(apiClient) {
    _classCallCheck(this, AlertsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAlertsIdGet operation.
   * @callback module:api/AlertsApi~juniferAlertsIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetAlertGetAlertsIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get alert
   * Get alert by its ID
   * @param {Number} id Alert ID
   * @param {module:api/AlertsApi~juniferAlertsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetAlertGetAlertsIdResponse200}
   */


  _createClass(AlertsApi, [{
    key: "juniferAlertsIdGet",
    value: function juniferAlertsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAlertsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAlertGetAlertsIdResponse["default"];
      return this.apiClient.callApi('/junifer/alerts/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return AlertsApi;
}();

exports["default"] = AlertsApi;