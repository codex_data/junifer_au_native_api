"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse = _interopRequireDefault(require("../model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200"));

var _GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse2 = _interopRequireDefault(require("../model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse400"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Nmis service.
* @module api/NmisApi
* @version 1.61.1
*/
var NmisApi = /*#__PURE__*/function () {
  /**
  * Constructs a new NmisApi. 
  * @alias module:api/NmisApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function NmisApi(apiClient) {
    _classCallCheck(this, NmisApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAuNmisStandingDataForNMIGet operation.
   * @callback module:api/NmisApi~juniferAuNmisStandingDataForNMIGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get NMIs including standing data
   * Returns a list of NMIs including standing data for the NMI details given.
   * @param {String} jurisdictionCode The jurisdiction the NMI falls under
   * @param {Object} opts Optional parameters
   * @param {String} opts.meterSerial The serial number of the meter
   * @param {String} opts.dpid The Delivery Point Identifier for the address the meter is at
   * @param {String} opts.buildingOrPropertyName Property name for the address the meter is located at
   * @param {String} opts.locationDescriptor Location descriptor for the address the meter is located at
   * @param {String} opts.lotNumber Lot number for the address the meter is located at
   * @param {String} opts.flatOrUnitType Unit type for the address the meter is located at
   * @param {String} opts.flatOrUnitNumber Unit number for the address the meter is located at
   * @param {String} opts.floorOrLevelType Floor type for the address the meter is located at
   * @param {String} opts.floorOrLevelNumber Floor number for the address the meter is located at
   * @param {String} opts.houseNumber House number for the address the meter is located at
   * @param {String} opts.houseNumberSuffix House suffix for the address the meter is located at
   * @param {String} opts.streetName Street name for the address the meter is located at
   * @param {String} opts.streetSuffix Street suffix for the address the meter is located at
   * @param {String} opts.streetType Street type for the address the meter is located at
   * @param {String} opts.suburbOrPlaceOrLocality Suburb for the address the meter is located at
   * @param {String} opts.postcode Postcode of the address the meter is located at
   * @param {String} opts.stateOrTerritory State for the address the meter is located at
   * @param {module:api/NmisApi~juniferAuNmisStandingDataForNMIGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200}
   */


  _createClass(NmisApi, [{
    key: "juniferAuNmisStandingDataForNMIGet",
    value: function juniferAuNmisStandingDataForNMIGet(jurisdictionCode, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'jurisdictionCode' is set

      if (jurisdictionCode === undefined || jurisdictionCode === null) {
        throw new Error("Missing the required parameter 'jurisdictionCode' when calling juniferAuNmisStandingDataForNMIGet");
      }

      var pathParams = {};
      var queryParams = {
        'jurisdictionCode': jurisdictionCode,
        'meterSerial': opts['meterSerial'],
        'dpid': opts['dpid'],
        'buildingOrPropertyName': opts['buildingOrPropertyName'],
        'locationDescriptor': opts['locationDescriptor'],
        'lotNumber': opts['lotNumber'],
        'flatOrUnitType': opts['flatOrUnitType'],
        'flatOrUnitNumber': opts['flatOrUnitNumber'],
        'floorOrLevelType': opts['floorOrLevelType'],
        'floorOrLevelNumber': opts['floorOrLevelNumber'],
        'houseNumber': opts['houseNumber'],
        'houseNumberSuffix': opts['houseNumberSuffix'],
        'streetName': opts['streetName'],
        'streetSuffix': opts['streetSuffix'],
        'streetType': opts['streetType'],
        'suburbOrPlaceOrLocality': opts['suburbOrPlaceOrLocality'],
        'postcode': opts['postcode'],
        'stateOrTerritory': opts['stateOrTerritory']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse["default"];
      return this.apiClient.callApi('/junifer/au/nmis/standingDataForNMI', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return NmisApi;
}();

exports["default"] = NmisApi;