"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBillPeriodGetBillperiodsIdResponse = _interopRequireDefault(require("../model/GetBillPeriodGetBillperiodsIdResponse200"));

var _GetBillPeriodGetBillperiodsIdResponse2 = _interopRequireDefault(require("../model/GetBillPeriodGetBillperiodsIdResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* BillPeriods service.
* @module api/BillPeriodsApi
* @version 1.61.1
*/
var BillPeriodsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new BillPeriodsApi. 
  * @alias module:api/BillPeriodsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function BillPeriodsApi(apiClient) {
    _classCallCheck(this, BillPeriodsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferBillPeriodsIdGet operation.
   * @callback module:api/BillPeriodsApi~juniferBillPeriodsIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetBillPeriodGetBillperiodsIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get Bill Period
   * Gets bill period by ID
   * @param {Number} id Bill Period ID
   * @param {module:api/BillPeriodsApi~juniferBillPeriodsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetBillPeriodGetBillperiodsIdResponse200}
   */


  _createClass(BillPeriodsApi, [{
    key: "juniferBillPeriodsIdGet",
    value: function juniferBillPeriodsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillPeriodsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBillPeriodGetBillperiodsIdResponse["default"];
      return this.apiClient.callApi('/junifer/billPeriods/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return BillPeriodsApi;
}();

exports["default"] = BillPeriodsApi;