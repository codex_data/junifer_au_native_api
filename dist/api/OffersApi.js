"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetOfferGetOffersIdResponse = _interopRequireDefault(require("../model/GetOfferGetOffersIdResponse200"));

var _GetOfferGetOffersIdResponse2 = _interopRequireDefault(require("../model/GetOfferGetOffersIdResponse404"));

var _GetQuotesGetOffersIdQuotesResponse = _interopRequireDefault(require("../model/GetQuotesGetOffersIdQuotesResponse200"));

var _GetQuotesGetOffersIdQuotesResponse2 = _interopRequireDefault(require("../model/GetQuotesGetOffersIdQuotesResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Offers service.
* @module api/OffersApi
* @version 1.61.1
*/
var OffersApi = /*#__PURE__*/function () {
  /**
  * Constructs a new OffersApi. 
  * @alias module:api/OffersApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function OffersApi(apiClient) {
    _classCallCheck(this, OffersApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferOffersIdGet operation.
   * @callback module:api/OffersApi~juniferOffersIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetOfferGetOffersIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get Offer
   * Get the offer details that is associated with the given offer id
   * @param {Number} id Offer id
   * @param {module:api/OffersApi~juniferOffersIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetOfferGetOffersIdResponse200}
   */


  _createClass(OffersApi, [{
    key: "juniferOffersIdGet",
    value: function juniferOffersIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferOffersIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetOfferGetOffersIdResponse["default"];
      return this.apiClient.callApi('/junifer/offers/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferOffersIdQuotesGet operation.
     * @callback module:api/OffersApi~juniferOffersIdQuotesGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetQuotesGetOffersIdQuotesResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get quotes
     * Get offer's quotes by its offer ID. If no quotes can be found an empty `results` array is returned.This API uses pagination and as such results may be restricted.
     * @param {Number} id Offer ID
     * @param {module:api/OffersApi~juniferOffersIdQuotesGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetQuotesGetOffersIdQuotesResponse200}
     */

  }, {
    key: "juniferOffersIdQuotesGet",
    value: function juniferOffersIdQuotesGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferOffersIdQuotesGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetQuotesGetOffersIdQuotesResponse["default"];
      return this.apiClient.callApi('/junifer/offers/{id}/quotes', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return OffersApi;
}();

exports["default"] = OffersApi;