"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AcceptDraftBillPostBillsIdAcceptdraftResponse = _interopRequireDefault(require("../model/AcceptDraftBillPostBillsIdAcceptdraftResponse404"));

var _BillEmailsGetBillemailsIdResponse = _interopRequireDefault(require("../model/BillEmailsGetBillemailsIdResponse200"));

var _BillEmailsGetBillemailsIdResponse2 = _interopRequireDefault(require("../model/BillEmailsGetBillemailsIdResponse404"));

var _GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse = _interopRequireDefault(require("../model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200"));

var _GetBillFileGetBillfilesIdResponse = _interopRequireDefault(require("../model/GetBillFileGetBillfilesIdResponse200"));

var _GetBillFileGetBillfilesIdResponse2 = _interopRequireDefault(require("../model/GetBillFileGetBillfilesIdResponse404"));

var _GetBillFileImageGetBillfilesIdImageResponse = _interopRequireDefault(require("../model/GetBillFileImageGetBillfilesIdImageResponse200"));

var _GetBillFileImageGetBillfilesIdImageResponse2 = _interopRequireDefault(require("../model/GetBillFileImageGetBillfilesIdImageResponse410"));

var _GetBillGetBillsIdResponse = _interopRequireDefault(require("../model/GetBillGetBillsIdResponse200"));

var _GetBillGetBillsIdResponse2 = _interopRequireDefault(require("../model/GetBillGetBillsIdResponse404"));

var _ReversionBillPostBillsIdReversionResponse = _interopRequireDefault(require("../model/ReversionBillPostBillsIdReversionResponse200"));

var _ReversionBillPostBillsIdReversionResponse2 = _interopRequireDefault(require("../model/ReversionBillPostBillsIdReversionResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Bills service.
* @module api/BillsApi
* @version 1.61.1
*/
var BillsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new BillsApi. 
  * @alias module:api/BillsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function BillsApi(apiClient) {
    _classCallCheck(this, BillsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferBillEmailsIdGet operation.
   * @callback module:api/BillsApi~juniferBillEmailsIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/BillEmailsGetBillemailsIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get bill email
   * Gets the email associated with a bill
   * @param {Number} id Bill ID (not email id)
   * @param {module:api/BillsApi~juniferBillEmailsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/BillEmailsGetBillemailsIdResponse200}
   */


  _createClass(BillsApi, [{
    key: "juniferBillEmailsIdGet",
    value: function juniferBillEmailsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillEmailsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _BillEmailsGetBillemailsIdResponse["default"];
      return this.apiClient.callApi('/junifer/billEmails/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBillFilesIdGet operation.
     * @callback module:api/BillsApi~juniferBillFilesIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBillFileGetBillfilesIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get bill file
     * Get bill file record by its ID.  Note that the ID is of the bill file, NOT the bill. To get the correct bill files follow the link URL in the response of e.g. Get Bill API, where the correct bill file ID is prepopulated in the URL by the system.
     * @param {Number} id Bill file ID
     * @param {module:api/BillsApi~juniferBillFilesIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBillFileGetBillfilesIdResponse200}
     */

  }, {
    key: "juniferBillFilesIdGet",
    value: function juniferBillFilesIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillFilesIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBillFileGetBillfilesIdResponse["default"];
      return this.apiClient.callApi('/junifer/billFiles/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBillFilesIdImageGet operation.
     * @callback module:api/BillsApi~juniferBillFilesIdImageGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBillFileImageGetBillfilesIdImageResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get bill file image
     * Initiates bill file image (usually PDF file) download if the file is available
     * @param {Number} id Bill file ID
     * @param {module:api/BillsApi~juniferBillFilesIdImageGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBillFileImageGetBillfilesIdImageResponse200}
     */

  }, {
    key: "juniferBillFilesIdImageGet",
    value: function juniferBillFilesIdImageGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillFilesIdImageGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBillFileImageGetBillfilesIdImageResponse["default"];
      return this.apiClient.callApi('/junifer/billFiles/{id}/image', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBillsIdAcceptDraftPost operation.
     * @callback module:api/BillsApi~juniferBillsIdAcceptDraftPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Accept Draft bill
     * Accept Draft bill
     * @param {Number} id Bill ID to accept
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.closeAssociatedTicketsFl Flag to close tickets associated to the bill when it is accepted. Defaults to false if not set
     * @param {module:api/BillsApi~juniferBillsIdAcceptDraftPostCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferBillsIdAcceptDraftPost",
    value: function juniferBillsIdAcceptDraftPost(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillsIdAcceptDraftPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'closeAssociatedTicketsFl': opts['closeAssociatedTicketsFl']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/bills/{id}/acceptDraft', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBillsIdBillBreakdownLinesGet operation.
     * @callback module:api/BillsApi~juniferBillsIdBillBreakdownLinesGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get bill's breakdown lines
     * Get bill's breakdown lines by its bill ID
     * @param {Number} id Bill ID
     * @param {module:api/BillsApi~juniferBillsIdBillBreakdownLinesGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200}
     */

  }, {
    key: "juniferBillsIdBillBreakdownLinesGet",
    value: function juniferBillsIdBillBreakdownLinesGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillsIdBillBreakdownLinesGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse["default"];
      return this.apiClient.callApi('/junifer/bills/{id}/billBreakdownLines', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBillsIdGet operation.
     * @callback module:api/BillsApi~juniferBillsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBillGetBillsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get bill
     * Get bill record by its ID
     * @param {Number} id Bill ID
     * @param {Object} opts Optional parameters
     * @param {Number} opts.contactId Restrict billFiles to a specific contact
     * @param {module:api/BillsApi~juniferBillsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBillGetBillsIdResponse200}
     */

  }, {
    key: "juniferBillsIdGet",
    value: function juniferBillsIdGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'contactId': opts['contactId']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetBillGetBillsIdResponse["default"];
      return this.apiClient.callApi('/junifer/bills/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferBillsIdReversionPost operation.
     * @callback module:api/BillsApi~juniferBillsIdReversionPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ReversionBillPostBillsIdReversionResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Reversion bill
     * Create new version of bill
     * @param {Number} id Bill ID
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.consolidateAllClosedBillPeriodsFl Flag to consolidate all Closed bill periods onto this bill period. Defaults to false if not specified
     * @param {Boolean} opts.consolidateAllOpenBillPeriodsFl Flag to consolidate all Open bill periods onto this bill period. Defaults to false if not specified
     * @param {module:api/BillsApi~juniferBillsIdReversionPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ReversionBillPostBillsIdReversionResponse200}
     */

  }, {
    key: "juniferBillsIdReversionPost",
    value: function juniferBillsIdReversionPost(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillsIdReversionPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'consolidateAllClosedBillPeriodsFl': opts['consolidateAllClosedBillPeriodsFl'],
        'consolidateAllOpenBillPeriodsFl': opts['consolidateAllOpenBillPeriodsFl']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _ReversionBillPostBillsIdReversionResponse["default"];
      return this.apiClient.callApi('/junifer/bills/{id}/reversion', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return BillsApi;
}();

exports["default"] = BillsApi;