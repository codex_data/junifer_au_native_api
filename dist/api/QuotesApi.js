"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AcceptQuotePostQuotesIdAcceptquoteResponse = _interopRequireDefault(require("../model/AcceptQuotePostQuotesIdAcceptquoteResponse400"));

var _AcceptQuotePostQuotesIdAcceptquoteResponse2 = _interopRequireDefault(require("../model/AcceptQuotePostQuotesIdAcceptquoteResponse404"));

var _GetQuoteGetQuotesIdResponse = _interopRequireDefault(require("../model/GetQuoteGetQuotesIdResponse200"));

var _GetQuoteGetQuotesIdResponse2 = _interopRequireDefault(require("../model/GetQuoteGetQuotesIdResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Quotes service.
* @module api/QuotesApi
* @version 1.61.1
*/
var QuotesApi = /*#__PURE__*/function () {
  /**
  * Constructs a new QuotesApi. 
  * @alias module:api/QuotesApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function QuotesApi(apiClient) {
    _classCallCheck(this, QuotesApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferQuotesIdAcceptQuotePost operation.
   * @callback module:api/QuotesApi~juniferQuotesIdAcceptQuotePostCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Accepts a quote given the quote ID
   * @param {Number} id Path Quote ID
   * @param {Number} id2 Quote ID
   * @param {module:api/QuotesApi~juniferQuotesIdAcceptQuotePostCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(QuotesApi, [{
    key: "juniferQuotesIdAcceptQuotePost",
    value: function juniferQuotesIdAcceptQuotePost(id, id2, callback) {
      var postBody = id2; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferQuotesIdAcceptQuotePost");
      } // verify the required parameter 'id2' is set


      if (id2 === undefined || id2 === null) {
        throw new Error("Missing the required parameter 'id2' when calling juniferQuotesIdAcceptQuotePost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/quotes/{id}/acceptQuote', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferQuotesIdGet operation.
     * @callback module:api/QuotesApi~juniferQuotesIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetQuoteGetQuotesIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get quote
     * Returns a quote given the quote ID
     * @param {Number} id Quote ID
     * @param {module:api/QuotesApi~juniferQuotesIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetQuoteGetQuotesIdResponse200}
     */

  }, {
    key: "juniferQuotesIdGet",
    value: function juniferQuotesIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferQuotesIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetQuoteGetQuotesIdResponse["default"];
      return this.apiClient.callApi('/junifer/quotes/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return QuotesApi;
}();

exports["default"] = QuotesApi;