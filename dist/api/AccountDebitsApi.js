"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CancelAccountDebitDeleteAccountdebitsIdResponse = _interopRequireDefault(require("../model/CancelAccountDebitDeleteAccountdebitsIdResponse400"));

var _GetAccountDebitGetAccountdebitsIdResponse = _interopRequireDefault(require("../model/GetAccountDebitGetAccountdebitsIdResponse200"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* AccountDebits service.
* @module api/AccountDebitsApi
* @version 1.61.1
*/
var AccountDebitsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new AccountDebitsApi. 
  * @alias module:api/AccountDebitsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function AccountDebitsApi(apiClient) {
    _classCallCheck(this, AccountDebitsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAccountDebitsIdDelete operation.
   * @callback module:api/AccountDebitsApi~juniferAccountDebitsIdDeleteCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Cancel account debit
   * Cancel an existing account debit for an account. This action is not reversible. Authentication must be active
   * @param {Number} id Account debit ID number
   * @param {module:api/AccountDebitsApi~juniferAccountDebitsIdDeleteCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(AccountDebitsApi, [{
    key: "juniferAccountDebitsIdDelete",
    value: function juniferAccountDebitsIdDelete(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountDebitsIdDelete");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/accountDebits/{id}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferAccountDebitsIdGet operation.
     * @callback module:api/AccountDebitsApi~juniferAccountDebitsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountDebitGetAccountdebitsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get Account Debit
     * Gets an account debit by its ID
     * @param {Number} id Account Debit ID
     * @param {module:api/AccountDebitsApi~juniferAccountDebitsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountDebitGetAccountdebitsIdResponse200}
     */

  }, {
    key: "juniferAccountDebitsIdGet",
    value: function juniferAccountDebitsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountDebitsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetAccountDebitGetAccountdebitsIdResponse["default"];
      return this.apiClient.callApi('/junifer/accountDebits/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return AccountDebitsApi;
}();

exports["default"] = AccountDebitsApi;