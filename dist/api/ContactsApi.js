"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetContactGetContactsIdResponse = _interopRequireDefault(require("../model/GetContactGetContactsIdResponse200"));

var _GetContactGetContactsIdResponse2 = _interopRequireDefault(require("../model/GetContactGetContactsIdResponse400"));

var _GetContactsAccountsGetContactsIdAccountsResponse = _interopRequireDefault(require("../model/GetContactsAccountsGetContactsIdAccountsResponse200"));

var _GetContactsAccountsGetContactsIdAccountsResponse2 = _interopRequireDefault(require("../model/GetContactsAccountsGetContactsIdAccountsResponse400"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse = _interopRequireDefault(require("../model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse2 = _interopRequireDefault(require("../model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse404"));

var _UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody = _interopRequireDefault(require("../model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody"));

var _UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse = _interopRequireDefault(require("../model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse400"));

var _UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse2 = _interopRequireDefault(require("../model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Contacts service.
* @module api/ContactsApi
* @version 1.61.1
*/
var ContactsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ContactsApi. 
  * @alias module:api/ContactsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ContactsApi(apiClient) {
    _classCallCheck(this, ContactsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferContactsIdAccountsGet operation.
   * @callback module:api/ContactsApi~juniferContactsIdAccountsGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetContactsAccountsGetContactsIdAccountsResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get accounts
   * Get list of accounts by contact ID
   * @param {Number} id Contact ID
   * @param {Object} opts Optional parameters
   * @param {String} opts.date Date for which there is a valid account. Will default to today if not provided
   * @param {Boolean} opts.includeFutureFl If true, the results will include any accounts' contacts that start in the future (after the specified date)
   * @param {Boolean} opts.includePastFl If true, the results will include any accounts' contacts that ended in the past (before the specified date)
   * @param {module:api/ContactsApi~juniferContactsIdAccountsGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetContactsAccountsGetContactsIdAccountsResponse200}
   */


  _createClass(ContactsApi, [{
    key: "juniferContactsIdAccountsGet",
    value: function juniferContactsIdAccountsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferContactsIdAccountsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'date': opts['date'],
        'includeFutureFl': opts['includeFutureFl'],
        'includePastFl': opts['includePastFl']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetContactsAccountsGetContactsIdAccountsResponse["default"];
      return this.apiClient.callApi('/junifer/contacts/{id}/accounts', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferContactsIdGet operation.
     * @callback module:api/ContactsApi~juniferContactsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetContactGetContactsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get contact
     * Get contact by its ID
     * @param {Number} id Contact ID
     * @param {module:api/ContactsApi~juniferContactsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetContactGetContactsIdResponse200}
     */

  }, {
    key: "juniferContactsIdGet",
    value: function juniferContactsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferContactsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetContactGetContactsIdResponse["default"];
      return this.apiClient.callApi('/junifer/contacts/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferContactsIdMarketingPreferencesGet operation.
     * @callback module:api/ContactsApi~juniferContactsIdMarketingPreferencesGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get a contact's marketing preferences
     * Gets a contact's marketing preferences
     * @param {Number} id Contact ID
     * @param {module:api/ContactsApi~juniferContactsIdMarketingPreferencesGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200}
     */

  }, {
    key: "juniferContactsIdMarketingPreferencesGet",
    value: function juniferContactsIdMarketingPreferencesGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferContactsIdMarketingPreferencesGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse["default"];
      return this.apiClient.callApi('/junifer/contacts/{id}/marketingPreferences', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferContactsIdMarketingPreferencesPut operation.
     * @callback module:api/ContactsApi~juniferContactsIdMarketingPreferencesPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update a contact's marketing preferences
     * Updates a contact's marketing preferences. Only the values provided will be updated.
     * @param {Number} id Contact ID
     * @param {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody} requestBody Request body
     * @param {module:api/ContactsApi~juniferContactsIdMarketingPreferencesPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferContactsIdMarketingPreferencesPut",
    value: function juniferContactsIdMarketingPreferencesPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferContactsIdMarketingPreferencesPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferContactsIdMarketingPreferencesPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/contacts/{id}/marketingPreferences', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ContactsApi;
}();

exports["default"] = ContactsApi;