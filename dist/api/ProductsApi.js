"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetProductsForNmiGetAuProductsTariffsfornmiResponse = _interopRequireDefault(require("../model/GetProductsForNmiGetAuProductsTariffsfornmiResponse200"));

var _GetProductsForNmiGetAuProductsTariffsfornmiResponse2 = _interopRequireDefault(require("../model/GetProductsForNmiGetAuProductsTariffsfornmiResponse400"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Products service.
* @module api/ProductsApi
* @version 1.61.1
*/
var ProductsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ProductsApi. 
  * @alias module:api/ProductsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ProductsApi(apiClient) {
    _classCallCheck(this, ProductsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAuProductsTariffsForNMIGet operation.
   * @callback module:api/ProductsApi~juniferAuProductsTariffsForNMIGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetProductsForNmiGetAuProductsTariffsfornmiResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get products for nmi
   * Returns a list of products that are available for the NMI details given. All prices include sales tax.
   * @param {String} nmi Identifier of the NMI
   * @param {Object} opts Optional parameters
   * @param {Boolean} opts.includeNonPublic Include only those products available to the public
   * @param {Boolean} opts.expiredOnly Include only those products where the \"Available To\" date has passed
   * @param {String} opts.billingEntityCode Code of the billing entity owning the products
   * @param {module:api/ProductsApi~juniferAuProductsTariffsForNMIGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetProductsForNmiGetAuProductsTariffsfornmiResponse200}
   */


  _createClass(ProductsApi, [{
    key: "juniferAuProductsTariffsForNMIGet",
    value: function juniferAuProductsTariffsForNMIGet(nmi, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'nmi' is set

      if (nmi === undefined || nmi === null) {
        throw new Error("Missing the required parameter 'nmi' when calling juniferAuProductsTariffsForNMIGet");
      }

      var pathParams = {};
      var queryParams = {
        'nmi': nmi,
        'includeNonPublic': opts['includeNonPublic'],
        'expiredOnly': opts['expiredOnly'],
        'billingEntityCode': opts['billingEntityCode']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetProductsForNmiGetAuProductsTariffsfornmiResponse["default"];
      return this.apiClient.callApi('/junifer/au/products/tariffsForNMI', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ProductsApi;
}();

exports["default"] = ProductsApi;