"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreatePropertyPostPropertysRequestBody = _interopRequireDefault(require("../model/CreatePropertyPostPropertysRequestBody"));

var _CreatePropertyPostPropertysResponse = _interopRequireDefault(require("../model/CreatePropertyPostPropertysResponse200"));

var _CreatePropertyPostPropertysResponse2 = _interopRequireDefault(require("../model/CreatePropertyPostPropertysResponse400"));

var _GetMeterPointsGetPropertysIdMeterpointsResponse = _interopRequireDefault(require("../model/GetMeterPointsGetPropertysIdMeterpointsResponse200"));

var _GetMeterPointsGetPropertysIdMeterpointsResponse2 = _interopRequireDefault(require("../model/GetMeterPointsGetPropertysIdMeterpointsResponse404"));

var _GetPropertyGetPropertysIdResponse = _interopRequireDefault(require("../model/GetPropertyGetPropertysIdResponse200"));

var _GetPropertyGetPropertysIdResponse2 = _interopRequireDefault(require("../model/GetPropertyGetPropertysIdResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Properties service.
* @module api/PropertiesApi
* @version 1.61.1
*/
var PropertiesApi = /*#__PURE__*/function () {
  /**
  * Constructs a new PropertiesApi. 
  * @alias module:api/PropertiesApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function PropertiesApi(apiClient) {
    _classCallCheck(this, PropertiesApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferPropertysIdGet operation.
   * @callback module:api/PropertiesApi~juniferPropertysIdGetCallback
   * @param {String} error Error message, if any.
   * @param {module:model/GetPropertyGetPropertysIdResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Get property
   * Get property by its ID
   * @param {Number} id Property ID
   * @param {module:api/PropertiesApi~juniferPropertysIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/GetPropertyGetPropertysIdResponse200}
   */


  _createClass(PropertiesApi, [{
    key: "juniferPropertysIdGet",
    value: function juniferPropertysIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPropertysIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetPropertyGetPropertysIdResponse["default"];
      return this.apiClient.callApi('/junifer/propertys/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPropertysIdMeterPointsGet operation.
     * @callback module:api/PropertiesApi~juniferPropertysIdMeterPointsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetMeterPointsGetPropertysIdMeterpointsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get meter points
     * Get meter points for property on the current date, or lookupDttm if provided. If none can be found, will return an empty array.
     * @param {Number} id Property ID
     * @param {Object} opts Optional parameters
     * @param {Object} opts.lookupDttm Will look for meterpoints on this date, if not provided it will search using today's date.
     * @param {module:api/PropertiesApi~juniferPropertysIdMeterPointsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetMeterPointsGetPropertysIdMeterpointsResponse200}
     */

  }, {
    key: "juniferPropertysIdMeterPointsGet",
    value: function juniferPropertysIdMeterPointsGet(id, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPropertysIdMeterPointsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {
        'lookupDttm': opts['lookupDttm']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetMeterPointsGetPropertysIdMeterpointsResponse["default"];
      return this.apiClient.callApi('/junifer/propertys/{id}/meterPoints', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferPropertysPost operation.
     * @callback module:api/PropertiesApi~juniferPropertysPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreatePropertyPostPropertysResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Creates a new property
     * @param {module:model/CreatePropertyPostPropertysRequestBody} requestBody Request body
     * @param {module:api/PropertiesApi~juniferPropertysPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreatePropertyPostPropertysResponse200}
     */

  }, {
    key: "juniferPropertysPost",
    value: function juniferPropertysPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferPropertysPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _CreatePropertyPostPropertysResponse["default"];
      return this.apiClient.callApi('/junifer/propertys', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return PropertiesApi;
}();

exports["default"] = PropertiesApi;