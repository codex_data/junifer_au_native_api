"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CommunicationsEmailsGetCommunicationsemailsIdResponse = _interopRequireDefault(require("../model/CommunicationsEmailsGetCommunicationsemailsIdResponse200"));

var _CommunicationsEmailsGetCommunicationsemailsIdResponse2 = _interopRequireDefault(require("../model/CommunicationsEmailsGetCommunicationsemailsIdResponse404"));

var _EmailPostCommunicationsEmailRequestBody = _interopRequireDefault(require("../model/EmailPostCommunicationsEmailRequestBody"));

var _EmailPostCommunicationsEmailResponse = _interopRequireDefault(require("../model/EmailPostCommunicationsEmailResponse400"));

var _GetCommunicationsFileGetCommunicationsfilesIdResponse = _interopRequireDefault(require("../model/GetCommunicationsFileGetCommunicationsfilesIdResponse200"));

var _GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse = _interopRequireDefault(require("../model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200"));

var _GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse2 = _interopRequireDefault(require("../model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse410"));

var _GetCommunicationsGetCommunicationsIdResponse = _interopRequireDefault(require("../model/GetCommunicationsGetCommunicationsIdResponse200"));

var _GetCommunicationsGetCommunicationsIdResponse2 = _interopRequireDefault(require("../model/GetCommunicationsGetCommunicationsIdResponse404"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Communications service.
* @module api/CommunicationsApi
* @version 1.61.1
*/
var CommunicationsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CommunicationsApi. 
  * @alias module:api/CommunicationsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CommunicationsApi(apiClient) {
    _classCallCheck(this, CommunicationsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferCommunicationsEmailPost operation.
   * @callback module:api/CommunicationsApi~juniferCommunicationsEmailPostCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Send email
   * Send an instantaneous email from Junifer server
   * @param {module:model/EmailPostCommunicationsEmailRequestBody} requestBody Request body
   * @param {module:api/CommunicationsApi~juniferCommunicationsEmailPostCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(CommunicationsApi, [{
    key: "juniferCommunicationsEmailPost",
    value: function juniferCommunicationsEmailPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCommunicationsEmailPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/communications/email', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCommunicationsEmailsIdGet operation.
     * @callback module:api/CommunicationsApi~juniferCommunicationsEmailsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CommunicationsEmailsGetCommunicationsemailsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get communication email
     * Gets the email associated with a communication. Only one email/sms per communication is returned (in case of multiple contacts)
     * @param {Number} id Communication ID (not email id)
     * @param {module:api/CommunicationsApi~juniferCommunicationsEmailsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CommunicationsEmailsGetCommunicationsemailsIdResponse200}
     */

  }, {
    key: "juniferCommunicationsEmailsIdGet",
    value: function juniferCommunicationsEmailsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCommunicationsEmailsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _CommunicationsEmailsGetCommunicationsemailsIdResponse["default"];
      return this.apiClient.callApi('/junifer/communicationsEmails/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCommunicationsFilesIdGet operation.
     * @callback module:api/CommunicationsApi~juniferCommunicationsFilesIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCommunicationsFileGetCommunicationsfilesIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get communication file
     * Get Communication specified by ID
     * @param {Number} id Communication ID
     * @param {module:api/CommunicationsApi~juniferCommunicationsFilesIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCommunicationsFileGetCommunicationsfilesIdResponse200}
     */

  }, {
    key: "juniferCommunicationsFilesIdGet",
    value: function juniferCommunicationsFilesIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCommunicationsFilesIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCommunicationsFileGetCommunicationsfilesIdResponse["default"];
      return this.apiClient.callApi('/junifer/communicationsFiles/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCommunicationsFilesIdImageGet operation.
     * @callback module:api/CommunicationsApi~juniferCommunicationsFilesIdImageGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get communication file image
     * Initiates communication file image (usually PDF file) download if the file is available
     * @param {Number} id Communication file ID
     * @param {module:api/CommunicationsApi~juniferCommunicationsFilesIdImageGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200}
     */

  }, {
    key: "juniferCommunicationsFilesIdImageGet",
    value: function juniferCommunicationsFilesIdImageGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCommunicationsFilesIdImageGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse["default"];
      return this.apiClient.callApi('/junifer/communicationsFiles/{id}/image', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCommunicationsIdGet operation.
     * @callback module:api/CommunicationsApi~juniferCommunicationsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCommunicationsGetCommunicationsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get Communication
     * Get Communication specified by ID
     * @param {Number} id Communication ID
     * @param {module:api/CommunicationsApi~juniferCommunicationsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCommunicationsGetCommunicationsIdResponse200}
     */

  }, {
    key: "juniferCommunicationsIdGet",
    value: function juniferCommunicationsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCommunicationsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCommunicationsGetCommunicationsIdResponse["default"];
      return this.apiClient.callApi('/junifer/communications/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CommunicationsApi;
}();

exports["default"] = CommunicationsApi;