"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _ContactsGetCustomersIdContactsResponse = _interopRequireDefault(require("../model/ContactsGetCustomersIdContactsResponse200"));

var _ContactsGetCustomersIdContactsResponse2 = _interopRequireDefault(require("../model/ContactsGetCustomersIdContactsResponse400"));

var _CreateProspectPutCustomersIdCreateprospectRequestBody = _interopRequireDefault(require("../model/CreateProspectPutCustomersIdCreateprospectRequestBody"));

var _CreateProspectPutCustomersIdCreateprospectResponse = _interopRequireDefault(require("../model/CreateProspectPutCustomersIdCreateprospectResponse200"));

var _CreateProspectPutCustomersIdCreateprospectResponse2 = _interopRequireDefault(require("../model/CreateProspectPutCustomersIdCreateprospectResponse400"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody = _interopRequireDefault(require("../model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse = _interopRequireDefault(require("../model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200"));

var _EnrolCustomerPostCustomersEnrolcustomerRequestBody = _interopRequireDefault(require("../model/EnrolCustomerPostCustomersEnrolcustomerRequestBody"));

var _EnrolCustomerPostCustomersEnrolcustomerResponse = _interopRequireDefault(require("../model/EnrolCustomerPostCustomersEnrolcustomerResponse200"));

var _EnrolCustomerPostCustomersEnrolcustomerResponse2 = _interopRequireDefault(require("../model/EnrolCustomerPostCustomersEnrolcustomerResponse400"));

var _GetCustomerAccountsGetCustomersIdAccountsResponse = _interopRequireDefault(require("../model/GetCustomerAccountsGetCustomersIdAccountsResponse200"));

var _GetCustomerAccountsGetCustomersIdAccountsResponse2 = _interopRequireDefault(require("../model/GetCustomerAccountsGetCustomersIdAccountsResponse400"));

var _GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse = _interopRequireDefault(require("../model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200"));

var _GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse2 = _interopRequireDefault(require("../model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400"));

var _GetCustomerConsentsGetCustomersIdConsentsResponse = _interopRequireDefault(require("../model/GetCustomerConsentsGetCustomersIdConsentsResponse200"));

var _GetCustomerConsentsGetCustomersIdConsentsResponse2 = _interopRequireDefault(require("../model/GetCustomerConsentsGetCustomersIdConsentsResponse400"));

var _GetCustomerGetCustomersIdResponse = _interopRequireDefault(require("../model/GetCustomerGetCustomersIdResponse200"));

var _GetCustomerGetCustomersIdResponse2 = _interopRequireDefault(require("../model/GetCustomerGetCustomersIdResponse400"));

var _GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse = _interopRequireDefault(require("../model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200"));

var _GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse2 = _interopRequireDefault(require("../model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400"));

var _LookUpCustomerGetCustomersResponse = _interopRequireDefault(require("../model/LookUpCustomerGetCustomersResponse200"));

var _LookUpCustomerGetCustomersResponse2 = _interopRequireDefault(require("../model/LookUpCustomerGetCustomersResponse400"));

var _UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody = _interopRequireDefault(require("../model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody"));

var _UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse = _interopRequireDefault(require("../model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400"));

var _UpdateCustomerConsentsPutCustomersIdConsentsRequestBody = _interopRequireDefault(require("../model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody"));

var _UpdateCustomerConsentsPutCustomersIdConsentsResponse = _interopRequireDefault(require("../model/UpdateCustomerConsentsPutCustomersIdConsentsResponse400"));

var _UpdateCustomerContactPutCustomersIdContactsContactidRequestBody = _interopRequireDefault(require("../model/UpdateCustomerContactPutCustomersIdContactsContactidRequestBody"));

var _UpdateCustomerContactPutCustomersIdContactsContactidResponse = _interopRequireDefault(require("../model/UpdateCustomerContactPutCustomersIdContactsContactidResponse400"));

var _UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody = _interopRequireDefault(require("../model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody"));

var _UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse = _interopRequireDefault(require("../model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400"));

var _UpdateCustomerPutCustomersIdRequestBody = _interopRequireDefault(require("../model/UpdateCustomerPutCustomersIdRequestBody"));

var _UpdateCustomerPutCustomersIdResponse = _interopRequireDefault(require("../model/UpdateCustomerPutCustomersIdResponse400"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Customers service.
* @module api/CustomersApi
* @version 1.61.1
*/
var CustomersApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CustomersApi. 
  * @alias module:api/CustomersApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CustomersApi(apiClient) {
    _classCallCheck(this, CustomersApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferCustomersEnrolCustomerPost operation.
   * @callback module:api/CustomersApi~juniferCustomersEnrolCustomerPostCallback
   * @param {String} error Error message, if any.
   * @param {module:model/EnrolCustomerPostCustomersEnrolcustomerResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Enrol customer
   * Enrols customer for electricity and/or gas product. This is a Great Britain specific API.
   * @param {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBody} requestBody Request body
   * @param {module:api/CustomersApi~juniferCustomersEnrolCustomerPostCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/EnrolCustomerPostCustomersEnrolcustomerResponse200}
   */


  _createClass(CustomersApi, [{
    key: "juniferCustomersEnrolCustomerPost",
    value: function juniferCustomersEnrolCustomerPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersEnrolCustomerPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _EnrolCustomerPostCustomersEnrolcustomerResponse["default"];
      return this.apiClient.callApi('/junifer/customers/enrolCustomer', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersGet operation.
     * @callback module:api/CustomersApi~juniferCustomersGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LookUpCustomerGetCustomersResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Customer lookup
     * Searches for a customer by the specified customer number. If no customer can be found an empty `results` array is returned.
     * @param {Number} customerNumber Customer number
     * @param {module:api/CustomersApi~juniferCustomersGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/LookUpCustomerGetCustomersResponse200}
     */

  }, {
    key: "juniferCustomersGet",
    value: function juniferCustomersGet(customerNumber, callback) {
      var postBody = null; // verify the required parameter 'customerNumber' is set

      if (customerNumber === undefined || customerNumber === null) {
        throw new Error("Missing the required parameter 'customerNumber' when calling juniferCustomersGet");
      }

      var pathParams = {};
      var queryParams = {
        'customer_number': customerNumber
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _LookUpCustomerGetCustomersResponse["default"];
      return this.apiClient.callApi('/junifer/customers', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdAccountsGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdAccountsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer's accounts
     * List customer accounts
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdAccountsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200}
     */

  }, {
    key: "juniferCustomersIdAccountsGet",
    value: function juniferCustomersIdAccountsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdAccountsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCustomerAccountsGetCustomersIdAccountsResponse["default"];
      return this.apiClient.callApi('/junifer/customers/{id}/accounts', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdBillingEntitiesGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdBillingEntitiesGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer's billing entities
     * Get customer's billing entities
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdBillingEntitiesGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200}
     */

  }, {
    key: "juniferCustomersIdBillingEntitiesGet",
    value: function juniferCustomersIdBillingEntitiesGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdBillingEntitiesGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse["default"];
      return this.apiClient.callApi('/junifer/customers/{id}/billingEntities', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdConsentsGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdConsentsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerConsentsGetCustomersIdConsentsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer's consents
     * Get customer's consents for DCC meters i.e. meter reading frequency. If no consents can be found an empty `results` array is returned
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdConsentsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerConsentsGetCustomersIdConsentsResponse200}
     */

  }, {
    key: "juniferCustomersIdConsentsGet",
    value: function juniferCustomersIdConsentsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdConsentsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCustomerConsentsGetCustomersIdConsentsResponse["default"];
      return this.apiClient.callApi('/junifer/customers/{id}/consents', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdConsentsPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdConsentsPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer's consents
     * Updates customer's consents for DCC meters i.e. meter reading frequency.
     * @param {Number} id Customer ID
     * @param {module:model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdConsentsPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferCustomersIdConsentsPut",
    value: function juniferCustomersIdConsentsPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdConsentsPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdConsentsPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/customers/{id}/consents', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdContactsContactIdPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdContactsContactIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer's contact details
     * Updates a customer's given contact details. Correct usage is to get the `contact` object from the `get customer contact` call then modify the `contact` object and return it using this `Update Customer's Contact` call. Our database is then updated to match the incoming `contact` object. All null fields in that incoming `contact` object are set to null in our database.
     * @param {Number} id Customer ID
     * @param {Number} contactId Contact ID
     * @param {module:model/UpdateCustomerContactPutCustomersIdContactsContactidRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdContactsContactIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferCustomersIdContactsContactIdPut",
    value: function juniferCustomersIdContactsContactIdPut(id, contactId, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdContactsContactIdPut");
      } // verify the required parameter 'contactId' is set


      if (contactId === undefined || contactId === null) {
        throw new Error("Missing the required parameter 'contactId' when calling juniferCustomersIdContactsContactIdPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdContactsContactIdPut");
      }

      var pathParams = {
        'id': id,
        'contactId': contactId
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/customers/{id}/contacts/{contactId}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdContactsGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdContactsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ContactsGetCustomersIdContactsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Contacts
     * Get all contacts for given customer. If no contacts can be found an empty `results` array is returned.
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdContactsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ContactsGetCustomersIdContactsResponse200}
     */

  }, {
    key: "juniferCustomersIdContactsGet",
    value: function juniferCustomersIdContactsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdContactsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _ContactsGetCustomersIdContactsResponse["default"];
      return this.apiClient.callApi('/junifer/customers/{id}/contacts', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdCreateProspectPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdCreateProspectPutCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateProspectPutCustomersIdCreateprospectResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create Prospect
     * Create new prospect for existing customer
     * @param {Number} id Customer ID
     * @param {module:model/CreateProspectPutCustomersIdCreateprospectRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdCreateProspectPutCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateProspectPutCustomersIdCreateprospectResponse200}
     */

  }, {
    key: "juniferCustomersIdCreateProspectPut",
    value: function juniferCustomersIdCreateProspectPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdCreateProspectPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdCreateProspectPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _CreateProspectPutCustomersIdCreateprospectResponse["default"];
      return this.apiClient.callApi('/junifer/customers/{id}/createProspect', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdEnrolAdditionalAccountPost operation.
     * @callback module:api/CustomersApi~juniferCustomersIdEnrolAdditionalAccountPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Enrol a new account to an existing customer
     * Creates a new account for existing customer and enrols it for electricity and/or gas product
     * @param {Number} id ID of additional account to enrol
     * @param {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdEnrolAdditionalAccountPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200}
     */

  }, {
    key: "juniferCustomersIdEnrolAdditionalAccountPost",
    value: function juniferCustomersIdEnrolAdditionalAccountPost(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdEnrolAdditionalAccountPost");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdEnrolAdditionalAccountPost");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse["default"];
      return this.apiClient.callApi('/junifer/customers/{id}/enrolAdditionalAccount', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerGetCustomersIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer
     * Get customer by its ID
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerGetCustomersIdResponse200}
     */

  }, {
    key: "juniferCustomersIdGet",
    value: function juniferCustomersIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCustomerGetCustomersIdResponse["default"];
      return this.apiClient.callApi('/junifer/customers/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdPrimaryContactPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdPrimaryContactPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer's primary contact details
     * Updates customer's primary contact details. Correct usage is to get the `primaryContact` object from the `get customer` call then modify the `primaryContact` object and return it using this `Update Customer's Primary Contact` call. Our database is then updated to match the incoming `primaryContact` object. All null fields in that incoming `primaryContact` object are set to null in our database.
     * @param {Number} id Customer ID
     * @param {module:model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdPrimaryContactPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferCustomersIdPrimaryContactPut",
    value: function juniferCustomersIdPrimaryContactPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdPrimaryContactPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdPrimaryContactPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/customers/{id}/primaryContact', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdProspectsGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdProspectsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer's Prospects
     * Gets prospects for a customer
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdProspectsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200}
     */

  }, {
    key: "juniferCustomersIdProspectsGet",
    value: function juniferCustomersIdProspectsGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdProspectsGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse["default"];
      return this.apiClient.callApi('/junifer/customers/{id}/prospects', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer
     * Updates customer record in Junifer. Only `title`, `forename`, `surname` and `marketingOptOutFl` will be updated. The rest of the fields in the request are ignored.
     * @param {Number} id Customer ID
     * @param {module:model/UpdateCustomerPutCustomersIdRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferCustomersIdPut",
    value: function juniferCustomersIdPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/customers/{id}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the juniferCustomersIdUpdateBillingAddressPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdUpdateBillingAddressPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer's Billing Address.
     * This Api call will change a customers billing address to what is supplied in the send address, if a postcode is sent it must be a valid UK postcode and if a Country name is sent it must be one that is saved into junifer already.Although postcode, Address lines and Country parameters are marked as optional, atleast one of them must be supplied to the call.
     * @param {Number} id Customer ID
     * @param {module:model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdUpdateBillingAddressPutCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "juniferCustomersIdUpdateBillingAddressPut",
    value: function juniferCustomersIdUpdateBillingAddressPut(id, requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdUpdateBillingAddressPut");
      } // verify the required parameter 'requestBody' is set


      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdUpdateBillingAddressPut");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi('/junifer/customers/{id}/updateBillingAddress', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CustomersApi;
}();

exports["default"] = CustomersApi;