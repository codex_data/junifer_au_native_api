"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody = _interopRequireDefault(require("../model/AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody"));

var _AusCreateConcessionPostAuConcessionsCreateconcessionResponse = _interopRequireDefault(require("../model/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* ConcessionsAustralia service.
* @module api/ConcessionsAustraliaApi
* @version 1.61.1
*/
var ConcessionsAustraliaApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ConcessionsAustraliaApi. 
  * @alias module:api/ConcessionsAustraliaApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ConcessionsAustraliaApi(apiClient) {
    _classCallCheck(this, ConcessionsAustraliaApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the juniferAuConcessionsCreateConcessionPost operation.
   * @callback module:api/ConcessionsAustraliaApi~juniferAuConcessionsCreateConcessionPostCallback
   * @param {String} error Error message, if any.
   * @param {module:model/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * Create concession
   * Creates a concession. This is an Australia specific API.
   * @param {module:model/AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody} requestBody Request body
   * @param {module:api/ConcessionsAustraliaApi~juniferAuConcessionsCreateConcessionPostCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200}
   */


  _createClass(ConcessionsAustraliaApi, [{
    key: "juniferAuConcessionsCreateConcessionPost",
    value: function juniferAuConcessionsCreateConcessionPost(requestBody, callback) {
      var postBody = requestBody; // verify the required parameter 'requestBody' is set

      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAuConcessionsCreateConcessionPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['tokenAuth'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = _AusCreateConcessionPostAuConcessionsCreateconcessionResponse["default"];
      return this.apiClient.callApi('/junifer/au/concessions/createConcession', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ConcessionsAustraliaApi;
}();

exports["default"] = ConcessionsAustraliaApi;