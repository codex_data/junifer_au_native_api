"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountTransactionsGetAccountsIdTransactionsResponse200Links model module.
 * @module model/GetAccountTransactionsGetAccountsIdTransactionsResponse200Links
 * @version 1.61.1
 */
var GetAccountTransactionsGetAccountsIdTransactionsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountTransactionsGetAccountsIdTransactionsResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200Links
   */
  function GetAccountTransactionsGetAccountsIdTransactionsResponse200Links() {
    _classCallCheck(this, GetAccountTransactionsGetAccountsIdTransactionsResponse200Links);

    GetAccountTransactionsGetAccountsIdTransactionsResponse200Links.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountTransactionsGetAccountsIdTransactionsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>GetAccountTransactionsGetAccountsIdTransactionsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200Links} The populated <code>GetAccountTransactionsGetAccountsIdTransactionsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountTransactionsGetAccountsIdTransactionsResponse200Links();

        if (data.hasOwnProperty('bill')) {
          obj['bill'] = _ApiClient["default"].convertToType(data['bill'], 'String');
        }

        if (data.hasOwnProperty('payment')) {
          obj['payment'] = _ApiClient["default"].convertToType(data['payment'], 'String');
        }

        if (data.hasOwnProperty('accountCredit')) {
          obj['accountCredit'] = _ApiClient["default"].convertToType(data['accountCredit'], 'String');
        }

        if (data.hasOwnProperty('accountDebit')) {
          obj['accountDebit'] = _ApiClient["default"].convertToType(data['accountDebit'], 'String');
        }

        if (data.hasOwnProperty('credit')) {
          obj['credit'] = _ApiClient["default"].convertToType(data['credit'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountTransactionsGetAccountsIdTransactionsResponse200Links;
}();
/**
 * Link referring to associated bill if applicable
 * @member {String} bill
 */


GetAccountTransactionsGetAccountsIdTransactionsResponse200Links.prototype['bill'] = undefined;
/**
 * Link referring to associated payment if applicable
 * @member {String} payment
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200Links.prototype['payment'] = undefined;
/**
 * Link referring to associated account credit if applicable
 * @member {String} accountCredit
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200Links.prototype['accountCredit'] = undefined;
/**
 * Link referring to associated account debit if applicable
 * @member {String} accountDebit
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200Links.prototype['accountDebit'] = undefined;
/**
 * Link referring to associated credit if applicable
 * @member {String} credit
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200Links.prototype['credit'] = undefined;
var _default = GetAccountTransactionsGetAccountsIdTransactionsResponse200Links;
exports["default"] = _default;