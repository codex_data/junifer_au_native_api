"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetProspectGetProspectsIdResponse200Links = _interopRequireDefault(require("./GetProspectGetProspectsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetProspectGetProspectsIdResponse200 model module.
 * @module model/GetProspectGetProspectsIdResponse200
 * @version 1.61.1
 */
var GetProspectGetProspectsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetProspectGetProspectsIdResponse200</code>.
   * @alias module:model/GetProspectGetProspectsIdResponse200
   * @param id {Number} Prospect ID
   * @param number {String} Prospect number
   * @param customerClass {String} Class of the prospected customer
   * @param companyType {String} Type of the prospected company
   * @param registeredAddress {Object} Registered address of the prospected company
   * @param links {module:model/GetProspectGetProspectsIdResponse200Links} 
   */
  function GetProspectGetProspectsIdResponse200(id, number, customerClass, companyType, registeredAddress, links) {
    _classCallCheck(this, GetProspectGetProspectsIdResponse200);

    GetProspectGetProspectsIdResponse200.initialize(this, id, number, customerClass, companyType, registeredAddress, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetProspectGetProspectsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, number, customerClass, companyType, registeredAddress, links) {
      obj['id'] = id;
      obj['number'] = number;
      obj['customerClass'] = customerClass;
      obj['companyType'] = companyType;
      obj['registeredAddress'] = registeredAddress;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetProspectGetProspectsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetProspectGetProspectsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetProspectGetProspectsIdResponse200} The populated <code>GetProspectGetProspectsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetProspectGetProspectsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('companyName')) {
          obj['companyName'] = _ApiClient["default"].convertToType(data['companyName'], 'String');
        }

        if (data.hasOwnProperty('customerClass')) {
          obj['customerClass'] = _ApiClient["default"].convertToType(data['customerClass'], 'String');
        }

        if (data.hasOwnProperty('companyType')) {
          obj['companyType'] = _ApiClient["default"].convertToType(data['companyType'], 'String');
        }

        if (data.hasOwnProperty('companyNumber')) {
          obj['companyNumber'] = _ApiClient["default"].convertToType(data['companyNumber'], 'String');
        }

        if (data.hasOwnProperty('registeredAddress')) {
          obj['registeredAddress'] = _ApiClient["default"].convertToType(data['registeredAddress'], Object);
        }

        if (data.hasOwnProperty('billingAddress')) {
          obj['billingAddress'] = _ApiClient["default"].convertToType(data['billingAddress'], Object);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetProspectGetProspectsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetProspectGetProspectsIdResponse200;
}();
/**
 * Prospect ID
 * @member {Number} id
 */


GetProspectGetProspectsIdResponse200.prototype['id'] = undefined;
/**
 * Prospect number
 * @member {String} number
 */

GetProspectGetProspectsIdResponse200.prototype['number'] = undefined;
/**
 * Name of the prospected company
 * @member {String} companyName
 */

GetProspectGetProspectsIdResponse200.prototype['companyName'] = undefined;
/**
 * Class of the prospected customer
 * @member {String} customerClass
 */

GetProspectGetProspectsIdResponse200.prototype['customerClass'] = undefined;
/**
 * Type of the prospected company
 * @member {String} companyType
 */

GetProspectGetProspectsIdResponse200.prototype['companyType'] = undefined;
/**
 * Number of the prospected company
 * @member {String} companyNumber
 */

GetProspectGetProspectsIdResponse200.prototype['companyNumber'] = undefined;
/**
 * Registered address of the prospected company
 * @member {Object} registeredAddress
 */

GetProspectGetProspectsIdResponse200.prototype['registeredAddress'] = undefined;
/**
 * Billing address of the prospected company
 * @member {Object} billingAddress
 */

GetProspectGetProspectsIdResponse200.prototype['billingAddress'] = undefined;
/**
 * @member {module:model/GetProspectGetProspectsIdResponse200Links} links
 */

GetProspectGetProspectsIdResponse200.prototype['links'] = undefined;
var _default = GetProspectGetProspectsIdResponse200;
exports["default"] = _default;