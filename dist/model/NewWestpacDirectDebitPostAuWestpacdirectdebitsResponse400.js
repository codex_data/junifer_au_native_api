"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400 model module.
 * @module model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400
 * @version 1.61.1
 */
var NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400</code>.
   * @alias module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400
   * @param errorCode {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400.ErrorCodeEnum} Field: * `AccountNotFound` - \"Account with such 'account.id' does not exist\" * `RequiredParameterMissing` - \"The required 'mandateReference' field is missing\" * `InvalidParameter` - \"The type field 'paymentType' is not a valid note type\" * `InvalidBankAccountDetails` - \"The 'bsbNumber' must be provided if 'mandateReference' is not specified\" * `BankAccountNumberExceedsLength` - \"The length of the 'bankAccountNumber' parameter exceeds length of 17 characters\" * `AccountNameExceedsLength` - \"The length of the 'accountName' parameter exceeds length of 22 characters\" * `CustomerNumberExceedsLength` - \"The length of the 'customerNumber' parameter exceeds length of 15 characters\"
   * @param errorSeverity {String} The error severity
   */
  function NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400(errorCode, errorSeverity) {
    _classCallCheck(this, NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400);

    NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400} obj Optional instance to populate.
     * @return {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400} The populated <code>NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400;
}();
/**
 * Field: * `AccountNotFound` - \"Account with such 'account.id' does not exist\" * `RequiredParameterMissing` - \"The required 'mandateReference' field is missing\" * `InvalidParameter` - \"The type field 'paymentType' is not a valid note type\" * `InvalidBankAccountDetails` - \"The 'bsbNumber' must be provided if 'mandateReference' is not specified\" * `BankAccountNumberExceedsLength` - \"The length of the 'bankAccountNumber' parameter exceeds length of 17 characters\" * `AccountNameExceedsLength` - \"The length of the 'accountName' parameter exceeds length of 22 characters\" * `CustomerNumberExceedsLength` - \"The length of the 'customerNumber' parameter exceeds length of 15 characters\"
 * @member {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400.ErrorCodeEnum} errorCode
 */


NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400['ErrorCodeEnum'] = {
  /**
   * value: "AccountNotFound"
   * @const
   */
  "AccountNotFound": "AccountNotFound",

  /**
   * value: "RequiredParameterMissing"
   * @const
   */
  "RequiredParameterMissing": "RequiredParameterMissing",

  /**
   * value: "InvalidParameter"
   * @const
   */
  "InvalidParameter": "InvalidParameter",

  /**
   * value: "InvalidBankAccountDetails"
   * @const
   */
  "InvalidBankAccountDetails": "InvalidBankAccountDetails",

  /**
   * value: "BankAccountNumberExceedsLength"
   * @const
   */
  "BankAccountNumberExceedsLength": "BankAccountNumberExceedsLength",

  /**
   * value: "AccountNameExceedsLength"
   * @const
   */
  "AccountNameExceedsLength": "AccountNameExceedsLength",

  /**
   * value: "CustomerNumberExceedsLength"
   * @const
   */
  "CustomerNumberExceedsLength": "CustomerNumberExceedsLength"
};
var _default = NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400;
exports["default"] = _default;