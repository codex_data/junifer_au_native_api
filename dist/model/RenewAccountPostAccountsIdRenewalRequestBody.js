"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The RenewAccountPostAccountsIdRenewalRequestBody model module.
 * @module model/RenewAccountPostAccountsIdRenewalRequestBody
 * @version 1.61.1
 */
var RenewAccountPostAccountsIdRenewalRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>RenewAccountPostAccountsIdRenewalRequestBody</code>.
   * @alias module:model/RenewAccountPostAccountsIdRenewalRequestBody
   */
  function RenewAccountPostAccountsIdRenewalRequestBody() {
    _classCallCheck(this, RenewAccountPostAccountsIdRenewalRequestBody);

    RenewAccountPostAccountsIdRenewalRequestBody.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(RenewAccountPostAccountsIdRenewalRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>RenewAccountPostAccountsIdRenewalRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RenewAccountPostAccountsIdRenewalRequestBody} obj Optional instance to populate.
     * @return {module:model/RenewAccountPostAccountsIdRenewalRequestBody} The populated <code>RenewAccountPostAccountsIdRenewalRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new RenewAccountPostAccountsIdRenewalRequestBody();

        if (data.hasOwnProperty('electricityProductCode')) {
          obj['electricityProductCode'] = _ApiClient["default"].convertToType(data['electricityProductCode'], 'String');
        }

        if (data.hasOwnProperty('electricitySupplyProductSubType')) {
          obj['electricitySupplyProductSubType'] = _ApiClient["default"].convertToType(data['electricitySupplyProductSubType'], 'String');
        }

        if (data.hasOwnProperty('electricityStartDate')) {
          obj['electricityStartDate'] = _ApiClient["default"].convertToType(data['electricityStartDate'], 'String');
        }

        if (data.hasOwnProperty('gasProductCode')) {
          obj['gasProductCode'] = _ApiClient["default"].convertToType(data['gasProductCode'], 'String');
        }

        if (data.hasOwnProperty('gasSupplyProductSubType')) {
          obj['gasSupplyProductSubType'] = _ApiClient["default"].convertToType(data['gasSupplyProductSubType'], 'String');
        }

        if (data.hasOwnProperty('gasStartDate')) {
          obj['gasStartDate'] = _ApiClient["default"].convertToType(data['gasStartDate'], 'String');
        }

        if (data.hasOwnProperty('creditExitFee')) {
          obj['creditExitFee'] = _ApiClient["default"].convertToType(data['creditExitFee'], 'Boolean');
        }
      }

      return obj;
    }
  }]);

  return RenewAccountPostAccountsIdRenewalRequestBody;
}();
/**
 * Product code for new electricity agreement
 * @member {String} electricityProductCode
 */


RenewAccountPostAccountsIdRenewalRequestBody.prototype['electricityProductCode'] = undefined;
/**
 * Product sub type for new electricity agreement. Required if electricityProductCode supplied
 * @member {String} electricitySupplyProductSubType
 */

RenewAccountPostAccountsIdRenewalRequestBody.prototype['electricitySupplyProductSubType'] = undefined;
/**
 * Start date of new electricity agreement. Required if electricityProductCode supplied
 * @member {String} electricityStartDate
 */

RenewAccountPostAccountsIdRenewalRequestBody.prototype['electricityStartDate'] = undefined;
/**
 * Product code for new gas agreement
 * @member {String} gasProductCode
 */

RenewAccountPostAccountsIdRenewalRequestBody.prototype['gasProductCode'] = undefined;
/**
 * Product sub type for new gas agreement. Required if gasProductCode supplied
 * @member {String} gasSupplyProductSubType
 */

RenewAccountPostAccountsIdRenewalRequestBody.prototype['gasSupplyProductSubType'] = undefined;
/**
 * Start date of new gas agreement. Required if gasProductCode supplied
 * @member {String} gasStartDate
 */

RenewAccountPostAccountsIdRenewalRequestBody.prototype['gasStartDate'] = undefined;
/**
 * Boolean specifying if early exit fees should be refunded, this is done by adding account credit equal to the exit fee. If not supplied a default of false is used.
 * @member {Boolean} creditExitFee
 */

RenewAccountPostAccountsIdRenewalRequestBody.prototype['creditExitFee'] = undefined;
var _default = RenewAccountPostAccountsIdRenewalRequestBody;
exports["default"] = _default;