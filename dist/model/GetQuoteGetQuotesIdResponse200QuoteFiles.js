"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetQuoteGetQuotesIdResponse200QuoteFilesLinks = _interopRequireDefault(require("./GetQuoteGetQuotesIdResponse200QuoteFilesLinks"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetQuoteGetQuotesIdResponse200QuoteFiles model module.
 * @module model/GetQuoteGetQuotesIdResponse200QuoteFiles
 * @version 1.61.1
 */
var GetQuoteGetQuotesIdResponse200QuoteFiles = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetQuoteGetQuotesIdResponse200QuoteFiles</code>.
   * Array of quote files associated with this quote
   * @alias module:model/GetQuoteGetQuotesIdResponse200QuoteFiles
   * @param id {Number} Quote file ID
   * @param display {String} Quote file description
   * @param viewable {Boolean} A boolean flag to indicate if the quote file is viewable
   * @param createdDttm {Date} Date and time when the quote file was created
   * @param links {module:model/GetQuoteGetQuotesIdResponse200QuoteFilesLinks} 
   */
  function GetQuoteGetQuotesIdResponse200QuoteFiles(id, display, viewable, createdDttm, links) {
    _classCallCheck(this, GetQuoteGetQuotesIdResponse200QuoteFiles);

    GetQuoteGetQuotesIdResponse200QuoteFiles.initialize(this, id, display, viewable, createdDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetQuoteGetQuotesIdResponse200QuoteFiles, null, [{
    key: "initialize",
    value: function initialize(obj, id, display, viewable, createdDttm, links) {
      obj['id'] = id;
      obj['display'] = display;
      obj['viewable'] = viewable;
      obj['createdDttm'] = createdDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetQuoteGetQuotesIdResponse200QuoteFiles</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetQuoteGetQuotesIdResponse200QuoteFiles} obj Optional instance to populate.
     * @return {module:model/GetQuoteGetQuotesIdResponse200QuoteFiles} The populated <code>GetQuoteGetQuotesIdResponse200QuoteFiles</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetQuoteGetQuotesIdResponse200QuoteFiles();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('display')) {
          obj['display'] = _ApiClient["default"].convertToType(data['display'], 'String');
        }

        if (data.hasOwnProperty('viewable')) {
          obj['viewable'] = _ApiClient["default"].convertToType(data['viewable'], 'Boolean');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetQuoteGetQuotesIdResponse200QuoteFilesLinks["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetQuoteGetQuotesIdResponse200QuoteFiles;
}();
/**
 * Quote file ID
 * @member {Number} id
 */


GetQuoteGetQuotesIdResponse200QuoteFiles.prototype['id'] = undefined;
/**
 * Quote file description
 * @member {String} display
 */

GetQuoteGetQuotesIdResponse200QuoteFiles.prototype['display'] = undefined;
/**
 * A boolean flag to indicate if the quote file is viewable
 * @member {Boolean} viewable
 */

GetQuoteGetQuotesIdResponse200QuoteFiles.prototype['viewable'] = undefined;
/**
 * Date and time when the quote file was created
 * @member {Date} createdDttm
 */

GetQuoteGetQuotesIdResponse200QuoteFiles.prototype['createdDttm'] = undefined;
/**
 * @member {module:model/GetQuoteGetQuotesIdResponse200QuoteFilesLinks} links
 */

GetQuoteGetQuotesIdResponse200QuoteFiles.prototype['links'] = undefined;
var _default = GetQuoteGetQuotesIdResponse200QuoteFiles;
exports["default"] = _default;