"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The RenewAccountPostAccountsIdRenewalResponse400 model module.
 * @module model/RenewAccountPostAccountsIdRenewalResponse400
 * @version 1.61.1
 */
var RenewAccountPostAccountsIdRenewalResponse400 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>RenewAccountPostAccountsIdRenewalResponse400</code>.
   * @alias module:model/RenewAccountPostAccountsIdRenewalResponse400
   * @param errorCode {module:model/RenewAccountPostAccountsIdRenewalResponse400.ErrorCodeEnum} Field: * `MissingDetails` - If a gasProductCode is supplied then a gasSupplyProductSubType must also be supplied * `NoCreditReasonSet` - You have tried to refund an exit fee but there is no default exit fee reason for renewals set * `NotFound` - Account supplied was not found * `CanNotCreditZero` - You can not credit a value of zero * `BadRequest` - There was an error processing your request
   * @param errorSeverity {String} The error severity
   */
  function RenewAccountPostAccountsIdRenewalResponse400(errorCode, errorSeverity) {
    _classCallCheck(this, RenewAccountPostAccountsIdRenewalResponse400);

    RenewAccountPostAccountsIdRenewalResponse400.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(RenewAccountPostAccountsIdRenewalResponse400, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>RenewAccountPostAccountsIdRenewalResponse400</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RenewAccountPostAccountsIdRenewalResponse400} obj Optional instance to populate.
     * @return {module:model/RenewAccountPostAccountsIdRenewalResponse400} The populated <code>RenewAccountPostAccountsIdRenewalResponse400</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new RenewAccountPostAccountsIdRenewalResponse400();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return RenewAccountPostAccountsIdRenewalResponse400;
}();
/**
 * Field: * `MissingDetails` - If a gasProductCode is supplied then a gasSupplyProductSubType must also be supplied * `NoCreditReasonSet` - You have tried to refund an exit fee but there is no default exit fee reason for renewals set * `NotFound` - Account supplied was not found * `CanNotCreditZero` - You can not credit a value of zero * `BadRequest` - There was an error processing your request
 * @member {module:model/RenewAccountPostAccountsIdRenewalResponse400.ErrorCodeEnum} errorCode
 */


RenewAccountPostAccountsIdRenewalResponse400.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

RenewAccountPostAccountsIdRenewalResponse400.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

RenewAccountPostAccountsIdRenewalResponse400.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

RenewAccountPostAccountsIdRenewalResponse400['ErrorCodeEnum'] = {
  /**
   * value: "MissingDetails"
   * @const
   */
  "MissingDetails": "MissingDetails",

  /**
   * value: "NoCreditReasonSet"
   * @const
   */
  "NoCreditReasonSet": "NoCreditReasonSet",

  /**
   * value: "NotFound"
   * @const
   */
  "NotFound": "NotFound",

  /**
   * value: "CanNotCreditZero"
   * @const
   */
  "CanNotCreditZero": "CanNotCreditZero",

  /**
   * value: "BadRequest"
   * @const
   */
  "BadRequest": "BadRequest"
};
var _default = RenewAccountPostAccountsIdRenewalResponse400;
exports["default"] = _default;