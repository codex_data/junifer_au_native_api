"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody model module.
 * @module model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody
 * @version 1.61.1
 */
var StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody</code>.
   * @alias module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody
   * @param toDt {Date} The date when payment schedule period should stop, usually today
   */
  function StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody(toDt) {
    _classCallCheck(this, StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody);

    StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody.initialize(this, toDt);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, toDt) {
      obj['toDt'] = toDt;
    }
    /**
     * Constructs a <code>StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody} obj Optional instance to populate.
     * @return {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody} The populated <code>StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody();

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody;
}();
/**
 * The date when payment schedule period should stop, usually today
 * @member {Date} toDt
 */


StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody.prototype['toDt'] = undefined;
var _default = StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody;
exports["default"] = _default;