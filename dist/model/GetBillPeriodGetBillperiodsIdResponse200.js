"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillPeriodGetBillperiodsIdResponse200 model module.
 * @module model/GetBillPeriodGetBillperiodsIdResponse200
 * @version 1.61.1
 */
var GetBillPeriodGetBillperiodsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillPeriodGetBillperiodsIdResponse200</code>.
   * @alias module:model/GetBillPeriodGetBillperiodsIdResponse200
   * @param id {Number} Bill Period ID
   * @param accountId {Number} Account ID
   * @param fromDttm {Date} From date of the Bill Period
   * @param toDttm {Date} To date of the Bill Period
   */
  function GetBillPeriodGetBillperiodsIdResponse200(id, accountId, fromDttm, toDttm) {
    _classCallCheck(this, GetBillPeriodGetBillperiodsIdResponse200);

    GetBillPeriodGetBillperiodsIdResponse200.initialize(this, id, accountId, fromDttm, toDttm);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillPeriodGetBillperiodsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, accountId, fromDttm, toDttm) {
      obj['id'] = id;
      obj['accountId'] = accountId;
      obj['fromDttm'] = fromDttm;
      obj['toDttm'] = toDttm;
    }
    /**
     * Constructs a <code>GetBillPeriodGetBillperiodsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillPeriodGetBillperiodsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetBillPeriodGetBillperiodsIdResponse200} The populated <code>GetBillPeriodGetBillperiodsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillPeriodGetBillperiodsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('accountId')) {
          obj['accountId'] = _ApiClient["default"].convertToType(data['accountId'], 'Number');
        }

        if (data.hasOwnProperty('statusCode')) {
          obj['statusCode'] = _ApiClient["default"].convertToType(data['statusCode'], 'String');
        }

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('consolidatedBillPeriodId')) {
          obj['consolidatedBillPeriodId'] = _ApiClient["default"].convertToType(data['consolidatedBillPeriodId'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return GetBillPeriodGetBillperiodsIdResponse200;
}();
/**
 * Bill Period ID
 * @member {Number} id
 */


GetBillPeriodGetBillperiodsIdResponse200.prototype['id'] = undefined;
/**
 * Account ID
 * @member {Number} accountId
 */

GetBillPeriodGetBillperiodsIdResponse200.prototype['accountId'] = undefined;
/**
 * Bill Period status code Can either be ` Open, Closed`
 * @member {String} statusCode
 */

GetBillPeriodGetBillperiodsIdResponse200.prototype['statusCode'] = undefined;
/**
 * From date of the Bill Period
 * @member {Date} fromDttm
 */

GetBillPeriodGetBillperiodsIdResponse200.prototype['fromDttm'] = undefined;
/**
 * To date of the Bill Period
 * @member {Date} toDttm
 */

GetBillPeriodGetBillperiodsIdResponse200.prototype['toDttm'] = undefined;
/**
 * Consolidated Bill Period ID
 * @member {Number} consolidatedBillPeriodId
 */

GetBillPeriodGetBillperiodsIdResponse200.prototype['consolidatedBillPeriodId'] = undefined;
var _default = GetBillPeriodGetBillperiodsIdResponse200;
exports["default"] = _default;