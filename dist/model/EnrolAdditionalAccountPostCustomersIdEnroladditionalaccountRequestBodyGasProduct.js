"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct model module.
 * @module model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct
 * @version 1.61.1
 */
var EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct</code>.
   * Gas product the customer chose
   * @alias module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct
   * @param productCode {String} Gas product code (reference in Junifer)
   * @param mprns {Array.<module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns>} List of MPRNs to associate with the new gas agreement
   */
  function EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct(productCode, mprns) {
    _classCallCheck(this, EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct);

    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.initialize(this, productCode, mprns);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct, null, [{
    key: "initialize",
    value: function initialize(obj, productCode, mprns) {
      obj['productCode'] = productCode;
      obj['mprns'] = mprns;
    }
    /**
     * Constructs a <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct} obj Optional instance to populate.
     * @return {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct} The populated <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct();

        if (data.hasOwnProperty('previousSupplier')) {
          obj['previousSupplier'] = _ApiClient["default"].convertToType(data['previousSupplier'], 'String');
        }

        if (data.hasOwnProperty('previousTariff')) {
          obj['previousTariff'] = _ApiClient["default"].convertToType(data['previousTariff'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('productCode')) {
          obj['productCode'] = _ApiClient["default"].convertToType(data['productCode'], 'String');
        }

        if (data.hasOwnProperty('mprns')) {
          obj['mprns'] = _ApiClient["default"].convertToType(data['mprns'], [_EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns["default"]]);
        }

        if (data.hasOwnProperty('domesticLargeSupplyPointFl')) {
          obj['domesticLargeSupplyPointFl'] = _ApiClient["default"].convertToType(data['domesticLargeSupplyPointFl'], 'Boolean');
        }
      }

      return obj;
    }
  }]);

  return EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct;
}();
/**
 * Previous supplier to the MPRN
 * @member {String} previousSupplier
 */


EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.prototype['previousSupplier'] = undefined;
/**
 * Previous Tariff of the MPRN
 * @member {String} previousTariff
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.prototype['previousTariff'] = undefined;
/**
 * External reference code
 * @member {String} reference
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.prototype['reference'] = undefined;
/**
 * Gas product code (reference in Junifer)
 * @member {String} productCode
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.prototype['productCode'] = undefined;
/**
 * List of MPRNs to associate with the new gas agreement
 * @member {Array.<module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns>} mprns
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.prototype['mprns'] = undefined;
/**
 * Is the meterpoint a large domestic supply point? Overrides the market sector specified in DES(Request Parameter)
 * @member {Boolean} domesticLargeSupplyPointFl
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.prototype['domesticLargeSupplyPointFl'] = undefined;
var _default = EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct;
exports["default"] = _default;