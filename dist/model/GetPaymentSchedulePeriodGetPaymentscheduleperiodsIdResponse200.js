"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links = _interopRequireDefault(require("./GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links"));

var _GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails = _interopRequireDefault(require("./GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200 model module.
 * @module model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200
 * @version 1.61.1
 */
var GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200</code>.
   * @alias module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200
   * @param id {Number} The ID of the payment schedule period
   * @param fromDt {Date} Date when the payment schedule period becomes active
   * @param frequencyMultiple {Number} Frequency multiplier
   * @param frequency {String} The frequency of payments
   * @param frequencyAlignmentDt {Date} Payment schedule alignment/pivot date
   * @param nextPaymentDt {Date} Date when the next payment is schedule to be collected
   * @param seasonalPaymentFl {Boolean} if the payment amount varies by season
   * @param amount {Number} The amount each payment collection will try to collect. Important: If a seasonal definition is present for the payment schedulethen the amount returned will be adjusted accordingly (if the payment at nextPaymentDt is in the high season then the returned amount will be the base amount increased by highPercentage, if the payment is in the low seasonthen the returned amount will be the base amount decreased by lowPercentage).
   * @param createdDttm {Date} Date and time when this payment schedule period was created
   * @param links {module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links} 
   */
  function GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200(id, fromDt, frequencyMultiple, frequency, frequencyAlignmentDt, nextPaymentDt, seasonalPaymentFl, amount, createdDttm, links) {
    _classCallCheck(this, GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200);

    GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.initialize(this, id, fromDt, frequencyMultiple, frequency, frequencyAlignmentDt, nextPaymentDt, seasonalPaymentFl, amount, createdDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, fromDt, frequencyMultiple, frequency, frequencyAlignmentDt, nextPaymentDt, seasonalPaymentFl, amount, createdDttm, links) {
      obj['id'] = id;
      obj['fromDt'] = fromDt;
      obj['frequencyMultiple'] = frequencyMultiple;
      obj['frequency'] = frequency;
      obj['frequencyAlignmentDt'] = frequencyAlignmentDt;
      obj['nextPaymentDt'] = nextPaymentDt;
      obj['seasonalPaymentFl'] = seasonalPaymentFl;
      obj['amount'] = amount;
      obj['createdDttm'] = createdDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200} The populated <code>GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }

        if (data.hasOwnProperty('frequencyMultiple')) {
          obj['frequencyMultiple'] = _ApiClient["default"].convertToType(data['frequencyMultiple'], 'Number');
        }

        if (data.hasOwnProperty('frequency')) {
          obj['frequency'] = _ApiClient["default"].convertToType(data['frequency'], 'String');
        }

        if (data.hasOwnProperty('frequencyAlignmentDt')) {
          obj['frequencyAlignmentDt'] = _ApiClient["default"].convertToType(data['frequencyAlignmentDt'], 'Date');
        }

        if (data.hasOwnProperty('nextPaymentDt')) {
          obj['nextPaymentDt'] = _ApiClient["default"].convertToType(data['nextPaymentDt'], 'Date');
        }

        if (data.hasOwnProperty('seasonalPaymentFl')) {
          obj['seasonalPaymentFl'] = _ApiClient["default"].convertToType(data['seasonalPaymentFl'], 'Boolean');
        }

        if (data.hasOwnProperty('highPercentage')) {
          obj['highPercentage'] = _ApiClient["default"].convertToType(data['highPercentage'], 'Number');
        }

        if (data.hasOwnProperty('lowPercentage')) {
          obj['lowPercentage'] = _ApiClient["default"].convertToType(data['lowPercentage'], 'Number');
        }

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('debtRecoveryDetails')) {
          obj['debtRecoveryDetails'] = _ApiClient["default"].convertToType(data['debtRecoveryDetails'], [_GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails["default"]]);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200;
}();
/**
 * The ID of the payment schedule period
 * @member {Number} id
 */


GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['id'] = undefined;
/**
 * Date when the payment schedule period becomes active
 * @member {Date} fromDt
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['fromDt'] = undefined;
/**
 * Date when the payment schedule period terminates
 * @member {Date} toDt
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['toDt'] = undefined;
/**
 * Frequency multiplier
 * @member {Number} frequencyMultiple
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['frequencyMultiple'] = undefined;
/**
 * The frequency of payments
 * @member {String} frequency
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['frequency'] = undefined;
/**
 * Payment schedule alignment/pivot date
 * @member {Date} frequencyAlignmentDt
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['frequencyAlignmentDt'] = undefined;
/**
 * Date when the next payment is schedule to be collected
 * @member {Date} nextPaymentDt
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['nextPaymentDt'] = undefined;
/**
 * if the payment amount varies by season
 * @member {Boolean} seasonalPaymentFl
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['seasonalPaymentFl'] = undefined;
/**
 * The percentage increase of direct debit amount during the high usage months (specified in the seasonal definition associated with the payment schedule)
 * @member {Number} highPercentage
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['highPercentage'] = undefined;
/**
 * The percentage decrease of direct debit amount during the low usage months (specified in the seasonal definition associated with the payment schedule)
 * @member {Number} lowPercentage
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['lowPercentage'] = undefined;
/**
 * The amount each payment collection will try to collect. Important: If a seasonal definition is present for the payment schedulethen the amount returned will be adjusted accordingly (if the payment at nextPaymentDt is in the high season then the returned amount will be the base amount increased by highPercentage, if the payment is in the low seasonthen the returned amount will be the base amount decreased by lowPercentage).
 * @member {Number} amount
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['amount'] = undefined;
/**
 * Date and time when this payment schedule period was created
 * @member {Date} createdDttm
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * debt recovery details
 * @member {Array.<module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails>} debtRecoveryDetails
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['debtRecoveryDetails'] = undefined;
/**
 * @member {module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links} links
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.prototype['links'] = undefined;
var _default = GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200;
exports["default"] = _default;