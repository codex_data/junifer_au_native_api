"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountCreditPostAccountsIdAccountcreditsResponse404 model module.
 * @module model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404
 * @version 1.61.1
 */
var CreateAccountCreditPostAccountsIdAccountcreditsResponse404 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountCreditPostAccountsIdAccountcreditsResponse404</code>.
   * @alias module:model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404
   * @param errorCode {module:model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404.ErrorCodeEnum} Field: * `AccountCreditReasonNotFound` - 'accountCreditReasonName' not recognised. Account credit reason must be a valid type * `AccountTransactionTypeNotFound` - No AccountTransactionType for AccountCredit
   * @param errorSeverity {String} The error severity
   */
  function CreateAccountCreditPostAccountsIdAccountcreditsResponse404(errorCode, errorSeverity) {
    _classCallCheck(this, CreateAccountCreditPostAccountsIdAccountcreditsResponse404);

    CreateAccountCreditPostAccountsIdAccountcreditsResponse404.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountCreditPostAccountsIdAccountcreditsResponse404, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>CreateAccountCreditPostAccountsIdAccountcreditsResponse404</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404} obj Optional instance to populate.
     * @return {module:model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404} The populated <code>CreateAccountCreditPostAccountsIdAccountcreditsResponse404</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountCreditPostAccountsIdAccountcreditsResponse404();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountCreditPostAccountsIdAccountcreditsResponse404;
}();
/**
 * Field: * `AccountCreditReasonNotFound` - 'accountCreditReasonName' not recognised. Account credit reason must be a valid type * `AccountTransactionTypeNotFound` - No AccountTransactionType for AccountCredit
 * @member {module:model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404.ErrorCodeEnum} errorCode
 */


CreateAccountCreditPostAccountsIdAccountcreditsResponse404.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

CreateAccountCreditPostAccountsIdAccountcreditsResponse404.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

CreateAccountCreditPostAccountsIdAccountcreditsResponse404.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

CreateAccountCreditPostAccountsIdAccountcreditsResponse404['ErrorCodeEnum'] = {
  /**
   * value: "AccountCreditReasonNotFound"
   * @const
   */
  "AccountCreditReasonNotFound": "AccountCreditReasonNotFound",

  /**
   * value: "AccountTransactionTypeNotFound"
   * @const
   */
  "AccountTransactionTypeNotFound": "AccountTransactionTypeNotFound"
};
var _default = CreateAccountCreditPostAccountsIdAccountcreditsResponse404;
exports["default"] = _default;