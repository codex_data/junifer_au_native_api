"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints = _interopRequireDefault(require("./GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints"));

var _GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress = _interopRequireDefault(require("./GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress"));

var _GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates = _interopRequireDefault(require("./GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType model module.
 * @module model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType
 * @version 1.61.1
 */
var GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType</code>.
   * @alias module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType
   * @param id {Number} The ID of the agreement
   * @param number {String} Agreement number
   * @param code {String} Product code
   * @param name {String} Product name
   * @param display {String} Product display name
   * @param durationMonths {Number} Duration
   * @param gspGroup {String} Grid supply point group
   * @param standingCharge {Number} Standing charge
   * @param unitRate {Number} The unit rate for Gas
   * @param unitRates {Array.<module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates>} The unit rates for Electricity
   * @param fromDt {Number} Date that the account started being supplied by this product
   * @param followOnCode {String} Following Product Bundle code.
   * @param supplyAddress {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress} 
   * @param meterPoints {Array.<module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints>} Meter points
   */
  function GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType(id, number, code, name, display, durationMonths, gspGroup, standingCharge, unitRate, unitRates, fromDt, followOnCode, supplyAddress, meterPoints) {
    _classCallCheck(this, GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType);

    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.initialize(this, id, number, code, name, display, durationMonths, gspGroup, standingCharge, unitRate, unitRates, fromDt, followOnCode, supplyAddress, meterPoints);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType, null, [{
    key: "initialize",
    value: function initialize(obj, id, number, code, name, display, durationMonths, gspGroup, standingCharge, unitRate, unitRates, fromDt, followOnCode, supplyAddress, meterPoints) {
      obj['id'] = id;
      obj['number'] = number;
      obj['code'] = code;
      obj['name'] = name;
      obj['display'] = display;
      obj['durationMonths'] = durationMonths;
      obj['gspGroup'] = gspGroup;
      obj['standingCharge'] = standingCharge;
      obj['unitRate'] = unitRate;
      obj['unitRates'] = unitRates;
      obj['fromDt'] = fromDt;
      obj['followOnCode'] = followOnCode;
      obj['supplyAddress'] = supplyAddress;
      obj['meterPoints'] = meterPoints;
    }
    /**
     * Constructs a <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType} obj Optional instance to populate.
     * @return {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType} The populated <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('code')) {
          obj['code'] = _ApiClient["default"].convertToType(data['code'], 'String');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('display')) {
          obj['display'] = _ApiClient["default"].convertToType(data['display'], 'String');
        }

        if (data.hasOwnProperty('durationMonths')) {
          obj['durationMonths'] = _ApiClient["default"].convertToType(data['durationMonths'], 'Number');
        }

        if (data.hasOwnProperty('endDttm')) {
          obj['endDttm'] = _ApiClient["default"].convertToType(data['endDttm'], 'Date');
        }

        if (data.hasOwnProperty('gspGroup')) {
          obj['gspGroup'] = _ApiClient["default"].convertToType(data['gspGroup'], 'String');
        }

        if (data.hasOwnProperty('directDebit')) {
          obj['directDebit'] = _ApiClient["default"].convertToType(data['directDebit'], 'Boolean');
        }

        if (data.hasOwnProperty('availableToPublic')) {
          obj['availableToPublic'] = _ApiClient["default"].convertToType(data['availableToPublic'], 'Boolean');
        }

        if (data.hasOwnProperty('standingCharge')) {
          obj['standingCharge'] = _ApiClient["default"].convertToType(data['standingCharge'], 'Number');
        }

        if (data.hasOwnProperty('onlineDiscount')) {
          obj['onlineDiscount'] = _ApiClient["default"].convertToType(data['onlineDiscount'], 'Number');
        }

        if (data.hasOwnProperty('earlyTerminationCharge')) {
          obj['earlyTerminationCharge'] = _ApiClient["default"].convertToType(data['earlyTerminationCharge'], 'Number');
        }

        if (data.hasOwnProperty('dualFuelDiscount')) {
          obj['dualFuelDiscount'] = _ApiClient["default"].convertToType(data['dualFuelDiscount'], 'Number');
        }

        if (data.hasOwnProperty('unitRate')) {
          obj['unitRate'] = _ApiClient["default"].convertToType(data['unitRate'], 'Number');
        }

        if (data.hasOwnProperty('unitRates')) {
          obj['unitRates'] = _ApiClient["default"].convertToType(data['unitRates'], [_GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates["default"]]);
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Number');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Number');
        }

        if (data.hasOwnProperty('contractedToDt')) {
          obj['contractedToDt'] = _ApiClient["default"].convertToType(data['contractedToDt'], 'Number');
        }

        if (data.hasOwnProperty('earlyTerminationChargeToDt')) {
          obj['earlyTerminationChargeToDt'] = _ApiClient["default"].convertToType(data['earlyTerminationChargeToDt'], 'Number');
        }

        if (data.hasOwnProperty('followOnCode')) {
          obj['followOnCode'] = _ApiClient["default"].convertToType(data['followOnCode'], 'String');
        }

        if (data.hasOwnProperty('supplyAddress')) {
          obj['supplyAddress'] = _GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress["default"].constructFromObject(data['supplyAddress']);
        }

        if (data.hasOwnProperty('meterPoints')) {
          obj['meterPoints'] = _ApiClient["default"].convertToType(data['meterPoints'], [_GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints["default"]]);
        }
      }

      return obj;
    }
  }]);

  return GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType;
}();
/**
 * The ID of the agreement
 * @member {Number} id
 */


GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['id'] = undefined;
/**
 * Agreement number
 * @member {String} number
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['number'] = undefined;
/**
 * Product code
 * @member {String} code
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['code'] = undefined;
/**
 * Product name
 * @member {String} name
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['name'] = undefined;
/**
 * Product display name
 * @member {String} display
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['display'] = undefined;
/**
 * Duration
 * @member {Number} durationMonths
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['durationMonths'] = undefined;
/**
 * The date when the product terminates
 * @member {Date} endDttm
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['endDttm'] = undefined;
/**
 * Grid supply point group
 * @member {String} gspGroup
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['gspGroup'] = undefined;
/**
 * Direct debit product
 * @member {Boolean} directDebit
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['directDebit'] = undefined;
/**
 * Available to public
 * @member {Boolean} availableToPublic
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['availableToPublic'] = undefined;
/**
 * Standing charge
 * @member {Number} standingCharge
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['standingCharge'] = undefined;
/**
 * Online discount
 * @member {Number} onlineDiscount
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['onlineDiscount'] = undefined;
/**
 * Early termination charge
 * @member {Number} earlyTerminationCharge
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['earlyTerminationCharge'] = undefined;
/**
 * Dual fuel discount
 * @member {Number} dualFuelDiscount
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['dualFuelDiscount'] = undefined;
/**
 * The unit rate for Gas
 * @member {Number} unitRate
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['unitRate'] = undefined;
/**
 * The unit rates for Electricity
 * @member {Array.<module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates>} unitRates
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['unitRates'] = undefined;
/**
 * Date that the account started being supplied by this product
 * @member {Number} fromDt
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['fromDt'] = undefined;
/**
 * Date that the account will stop being supplied by this product
 * @member {Number} toDt
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['toDt'] = undefined;
/**
 * Date that the account is contracted to this product
 * @member {Number} contractedToDt
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['contractedToDt'] = undefined;
/**
 * Date that the account termination fee will stop to apply
 * @member {Number} earlyTerminationChargeToDt
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['earlyTerminationChargeToDt'] = undefined;
/**
 * Following Product Bundle code.
 * @member {String} followOnCode
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['followOnCode'] = undefined;
/**
 * @member {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress} supplyAddress
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['supplyAddress'] = undefined;
/**
 * Meter points
 * @member {Array.<module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints>} meterPoints
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.prototype['meterPoints'] = undefined;
var _default = GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType;
exports["default"] = _default;