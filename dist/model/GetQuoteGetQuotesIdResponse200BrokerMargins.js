"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetQuoteGetQuotesIdResponse200BrokerMargins model module.
 * @module model/GetQuoteGetQuotesIdResponse200BrokerMargins
 * @version 1.61.1
 */
var GetQuoteGetQuotesIdResponse200BrokerMargins = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetQuoteGetQuotesIdResponse200BrokerMargins</code>.
   * An array of broker margins that are associated with the quote
   * @alias module:model/GetQuoteGetQuotesIdResponse200BrokerMargins
   * @param brokerName {String} The name of the broker which is associated with the broker margin
   * @param marginValue {Number} The value of the broker margin
   * @param marginType {String} The metric type of the broker margin. Examples include `PencePerkWh, PoundPerYearPerMeterPoint`
   * @param links {String} The list of links associated with the broker margins
   */
  function GetQuoteGetQuotesIdResponse200BrokerMargins(brokerName, marginValue, marginType, links) {
    _classCallCheck(this, GetQuoteGetQuotesIdResponse200BrokerMargins);

    GetQuoteGetQuotesIdResponse200BrokerMargins.initialize(this, brokerName, marginValue, marginType, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetQuoteGetQuotesIdResponse200BrokerMargins, null, [{
    key: "initialize",
    value: function initialize(obj, brokerName, marginValue, marginType, links) {
      obj['brokerName'] = brokerName;
      obj['marginValue'] = marginValue;
      obj['marginType'] = marginType;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetQuoteGetQuotesIdResponse200BrokerMargins</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetQuoteGetQuotesIdResponse200BrokerMargins} obj Optional instance to populate.
     * @return {module:model/GetQuoteGetQuotesIdResponse200BrokerMargins} The populated <code>GetQuoteGetQuotesIdResponse200BrokerMargins</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetQuoteGetQuotesIdResponse200BrokerMargins();

        if (data.hasOwnProperty('brokerName')) {
          obj['brokerName'] = _ApiClient["default"].convertToType(data['brokerName'], 'String');
        }

        if (data.hasOwnProperty('marginValue')) {
          obj['marginValue'] = _ApiClient["default"].convertToType(data['marginValue'], 'Number');
        }

        if (data.hasOwnProperty('marginType')) {
          obj['marginType'] = _ApiClient["default"].convertToType(data['marginType'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _ApiClient["default"].convertToType(data['links'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetQuoteGetQuotesIdResponse200BrokerMargins;
}();
/**
 * The name of the broker which is associated with the broker margin
 * @member {String} brokerName
 */


GetQuoteGetQuotesIdResponse200BrokerMargins.prototype['brokerName'] = undefined;
/**
 * The value of the broker margin
 * @member {Number} marginValue
 */

GetQuoteGetQuotesIdResponse200BrokerMargins.prototype['marginValue'] = undefined;
/**
 * The metric type of the broker margin. Examples include `PencePerkWh, PoundPerYearPerMeterPoint`
 * @member {String} marginType
 */

GetQuoteGetQuotesIdResponse200BrokerMargins.prototype['marginType'] = undefined;
/**
 * The list of links associated with the broker margins
 * @member {String} links
 */

GetQuoteGetQuotesIdResponse200BrokerMargins.prototype['links'] = undefined;
var _default = GetQuoteGetQuotesIdResponse200BrokerMargins;
exports["default"] = _default;