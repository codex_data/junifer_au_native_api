"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200 model module.
 * @module model/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200
 * @version 1.61.1
 */
var GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200</code>.
   * @alias module:model/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200
   * @param id {Number} PayPal Direct Debit object id
   * @param accountName {String} Account name
   * @param mandateReference {String} Mandate Reference
   * @param status {String} Current status of the Direct Debit
   */
  function GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200(id, accountName, mandateReference, status) {
    _classCallCheck(this, GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200);

    GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200.initialize(this, id, accountName, mandateReference, status);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, accountName, mandateReference, status) {
      obj['id'] = id;
      obj['accountName'] = accountName;
      obj['mandateReference'] = mandateReference;
      obj['status'] = status;
    }
    /**
     * Constructs a <code>GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200} The populated <code>GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('accountName')) {
          obj['accountName'] = _ApiClient["default"].convertToType(data['accountName'], 'String');
        }

        if (data.hasOwnProperty('mandateReference')) {
          obj['mandateReference'] = _ApiClient["default"].convertToType(data['mandateReference'], 'String');
        }

        if (data.hasOwnProperty('authorisedDttm')) {
          obj['authorisedDttm'] = _ApiClient["default"].convertToType(data['authorisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('terminatedDttm')) {
          obj['terminatedDttm'] = _ApiClient["default"].convertToType(data['terminatedDttm'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200;
}();
/**
 * PayPal Direct Debit object id
 * @member {Number} id
 */


GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200.prototype['id'] = undefined;
/**
 * Account name
 * @member {String} accountName
 */

GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200.prototype['accountName'] = undefined;
/**
 * Mandate Reference
 * @member {String} mandateReference
 */

GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200.prototype['mandateReference'] = undefined;
/**
 * Date when the Direct Debit was authorised
 * @member {Date} authorisedDttm
 */

GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200.prototype['authorisedDttm'] = undefined;
/**
 * Date when the Direct Debit was terminated
 * @member {Date} terminatedDttm
 */

GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200.prototype['terminatedDttm'] = undefined;
/**
 * Current status of the Direct Debit
 * @member {String} status
 */

GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200.prototype['status'] = undefined;
var _default = GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200;
exports["default"] = _default;