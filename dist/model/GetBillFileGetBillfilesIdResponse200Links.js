"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillFileGetBillfilesIdResponse200Links model module.
 * @module model/GetBillFileGetBillfilesIdResponse200Links
 * @version 1.61.1
 */
var GetBillFileGetBillfilesIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillFileGetBillfilesIdResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetBillFileGetBillfilesIdResponse200Links
   * @param self {String} Link to this file
   * @param image {String} Link to associated bill file image
   */
  function GetBillFileGetBillfilesIdResponse200Links(self, image) {
    _classCallCheck(this, GetBillFileGetBillfilesIdResponse200Links);

    GetBillFileGetBillfilesIdResponse200Links.initialize(this, self, image);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillFileGetBillfilesIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, image) {
      obj['self'] = self;
      obj['image'] = image;
    }
    /**
     * Constructs a <code>GetBillFileGetBillfilesIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillFileGetBillfilesIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetBillFileGetBillfilesIdResponse200Links} The populated <code>GetBillFileGetBillfilesIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillFileGetBillfilesIdResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('image')) {
          obj['image'] = _ApiClient["default"].convertToType(data['image'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetBillFileGetBillfilesIdResponse200Links;
}();
/**
 * Link to this file
 * @member {String} self
 */


GetBillFileGetBillfilesIdResponse200Links.prototype['self'] = undefined;
/**
 * Link to associated bill file image
 * @member {String} image
 */

GetBillFileGetBillfilesIdResponse200Links.prototype['image'] = undefined;
var _default = GetBillFileGetBillfilesIdResponse200Links;
exports["default"] = _default;