"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200 model module.
 * @module model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200
 * @version 1.61.1
 */
var CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200</code>.
   * @alias module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200
   * @param id {Number} Account review period Id
   * @param fromDttm {Date} Account review period start date time
   * @param reason {String} The name of an account review reason as shown in the Account Review Reason ref table
   * @param suppressDunningFl {Boolean} A flag indicating whether dunning should be suppressed
   * @param suppressBillingFl {Boolean} A flag indicating whether billing should be suppressed
   * @param suppressPaymentCollectionFl {Boolean} A flag indicating whether payment collection should be suppressed
   * @param suppressPaymentReviewFl {Boolean} A flag indicating whether payment review should be suppressed
   * @param propertySuppressionFl {Boolean} Property-specific billing suppression
   * @param description {String} Account review period description
   * @param createdDttm {Date} Account review period creation date and time
   * @param createdBy {String} Username that created the account review period
   */
  function CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200(id, fromDttm, reason, suppressDunningFl, suppressBillingFl, suppressPaymentCollectionFl, suppressPaymentReviewFl, propertySuppressionFl, description, createdDttm, createdBy) {
    _classCallCheck(this, CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200);

    CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.initialize(this, id, fromDttm, reason, suppressDunningFl, suppressBillingFl, suppressPaymentCollectionFl, suppressPaymentReviewFl, propertySuppressionFl, description, createdDttm, createdBy);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, fromDttm, reason, suppressDunningFl, suppressBillingFl, suppressPaymentCollectionFl, suppressPaymentReviewFl, propertySuppressionFl, description, createdDttm, createdBy) {
      obj['id'] = id;
      obj['fromDttm'] = fromDttm;
      obj['reason'] = reason;
      obj['suppressDunningFl'] = suppressDunningFl;
      obj['suppressBillingFl'] = suppressBillingFl;
      obj['suppressPaymentCollectionFl'] = suppressPaymentCollectionFl;
      obj['suppressPaymentReviewFl'] = suppressPaymentReviewFl;
      obj['propertySuppressionFl'] = propertySuppressionFl;
      obj['description'] = description;
      obj['createdDttm'] = createdDttm;
      obj['createdBy'] = createdBy;
    }
    /**
     * Constructs a <code>CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200} obj Optional instance to populate.
     * @return {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200} The populated <code>CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('reason')) {
          obj['reason'] = _ApiClient["default"].convertToType(data['reason'], 'String');
        }

        if (data.hasOwnProperty('suppressDunningFl')) {
          obj['suppressDunningFl'] = _ApiClient["default"].convertToType(data['suppressDunningFl'], 'Boolean');
        }

        if (data.hasOwnProperty('suppressBillingFl')) {
          obj['suppressBillingFl'] = _ApiClient["default"].convertToType(data['suppressBillingFl'], 'Boolean');
        }

        if (data.hasOwnProperty('suppressPaymentCollectionFl')) {
          obj['suppressPaymentCollectionFl'] = _ApiClient["default"].convertToType(data['suppressPaymentCollectionFl'], 'Boolean');
        }

        if (data.hasOwnProperty('suppressPaymentReviewFl')) {
          obj['suppressPaymentReviewFl'] = _ApiClient["default"].convertToType(data['suppressPaymentReviewFl'], 'Boolean');
        }

        if (data.hasOwnProperty('propertySuppressionFl')) {
          obj['propertySuppressionFl'] = _ApiClient["default"].convertToType(data['propertySuppressionFl'], 'Boolean');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('createdBy')) {
          obj['createdBy'] = _ApiClient["default"].convertToType(data['createdBy'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200;
}();
/**
 * Account review period Id
 * @member {Number} id
 */


CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['id'] = undefined;
/**
 * Account review period start date time
 * @member {Date} fromDttm
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['fromDttm'] = undefined;
/**
 * Account review period end date time
 * @member {Date} toDttm
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['toDttm'] = undefined;
/**
 * The name of an account review reason as shown in the Account Review Reason ref table
 * @member {String} reason
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['reason'] = undefined;
/**
 * A flag indicating whether dunning should be suppressed
 * @member {Boolean} suppressDunningFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['suppressDunningFl'] = undefined;
/**
 * A flag indicating whether billing should be suppressed
 * @member {Boolean} suppressBillingFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['suppressBillingFl'] = undefined;
/**
 * A flag indicating whether payment collection should be suppressed
 * @member {Boolean} suppressPaymentCollectionFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['suppressPaymentCollectionFl'] = undefined;
/**
 * A flag indicating whether payment review should be suppressed
 * @member {Boolean} suppressPaymentReviewFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['suppressPaymentReviewFl'] = undefined;
/**
 * Property-specific billing suppression
 * @member {Boolean} propertySuppressionFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['propertySuppressionFl'] = undefined;
/**
 * Account review period description
 * @member {String} description
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['description'] = undefined;
/**
 * Account review period creation date and time
 * @member {Date} createdDttm
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['createdDttm'] = undefined;
/**
 * Username that created the account review period
 * @member {String} createdBy
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.prototype['createdBy'] = undefined;
var _default = CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200;
exports["default"] = _default;