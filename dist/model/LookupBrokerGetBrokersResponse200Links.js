"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The LookupBrokerGetBrokersResponse200Links model module.
 * @module model/LookupBrokerGetBrokersResponse200Links
 * @version 1.61.1
 */
var LookupBrokerGetBrokersResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>LookupBrokerGetBrokersResponse200Links</code>.
   * Links to related resources
   * @alias module:model/LookupBrokerGetBrokersResponse200Links
   * @param self {String} Link referring to this broker
   * @param customerBrokers {String} Link referring to this broker's live customerBrokers with a valid Letter of Authority for today's date
   */
  function LookupBrokerGetBrokersResponse200Links(self, customerBrokers) {
    _classCallCheck(this, LookupBrokerGetBrokersResponse200Links);

    LookupBrokerGetBrokersResponse200Links.initialize(this, self, customerBrokers);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(LookupBrokerGetBrokersResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, customerBrokers) {
      obj['self'] = self;
      obj['customerBrokers'] = customerBrokers;
    }
    /**
     * Constructs a <code>LookupBrokerGetBrokersResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LookupBrokerGetBrokersResponse200Links} obj Optional instance to populate.
     * @return {module:model/LookupBrokerGetBrokersResponse200Links} The populated <code>LookupBrokerGetBrokersResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new LookupBrokerGetBrokersResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('customerBrokers')) {
          obj['customerBrokers'] = _ApiClient["default"].convertToType(data['customerBrokers'], 'String');
        }
      }

      return obj;
    }
  }]);

  return LookupBrokerGetBrokersResponse200Links;
}();
/**
 * Link referring to this broker
 * @member {String} self
 */


LookupBrokerGetBrokersResponse200Links.prototype['self'] = undefined;
/**
 * Link referring to this broker's live customerBrokers with a valid Letter of Authority for today's date
 * @member {String} customerBrokers
 */

LookupBrokerGetBrokersResponse200Links.prototype['customerBrokers'] = undefined;
var _default = LookupBrokerGetBrokersResponse200Links;
exports["default"] = _default;