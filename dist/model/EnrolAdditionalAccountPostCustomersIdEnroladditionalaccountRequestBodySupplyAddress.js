"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress model module.
 * @module model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress
 * @version 1.61.1
 */
var EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress</code>.
   * Supply address.
   * @alias module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress
   * @param address1 {String} First address line
   * @param postcode {String} Post code
   * @param countryCode {String} An ISO country code. Currently only GB is supported thus is the the only acceptable value!(Request Parameter)
   */
  function EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress(address1, postcode, countryCode) {
    _classCallCheck(this, EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress);

    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.initialize(this, address1, postcode, countryCode);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress, null, [{
    key: "initialize",
    value: function initialize(obj, address1, postcode, countryCode) {
      obj['address1'] = address1;
      obj['postcode'] = postcode;
      obj['countryCode'] = countryCode;
    }
    /**
     * Constructs a <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress} obj Optional instance to populate.
     * @return {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress} The populated <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress();

        if (data.hasOwnProperty('careOf')) {
          obj['careOf'] = _ApiClient["default"].convertToType(data['careOf'], 'String');
        }

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('town')) {
          obj['town'] = _ApiClient["default"].convertToType(data['town'], 'String');
        }

        if (data.hasOwnProperty('county')) {
          obj['county'] = _ApiClient["default"].convertToType(data['county'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('countryCode')) {
          obj['countryCode'] = _ApiClient["default"].convertToType(data['countryCode'], 'String');
        }
      }

      return obj;
    }
  }]);

  return EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress;
}();
/**
 * The 'Care of' line for the address
 * @member {String} careOf
 */


EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.prototype['careOf'] = undefined;
/**
 * First address line
 * @member {String} address1
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.prototype['address1'] = undefined;
/**
 * Second address line
 * @member {String} address2
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.prototype['address2'] = undefined;
/**
 * Third address line
 * @member {String} address3
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.prototype['address3'] = undefined;
/**
 * Town
 * @member {String} town
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.prototype['town'] = undefined;
/**
 * County
 * @member {String} county
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.prototype['county'] = undefined;
/**
 * Post code
 * @member {String} postcode
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.prototype['postcode'] = undefined;
/**
 * An ISO country code. Currently only GB is supported thus is the the only acceptable value!(Request Parameter)
 * @member {String} countryCode
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.prototype['countryCode'] = undefined;
var _default = EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress;
exports["default"] = _default;