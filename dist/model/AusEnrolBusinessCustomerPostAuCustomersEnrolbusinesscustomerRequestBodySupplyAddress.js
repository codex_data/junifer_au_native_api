"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress model module.
 * @module model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress
 * @version 1.61.1
 */
var AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress</code>.
   * Supply address.
   * @alias module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress
   * @param careOf {String} The 'Care of' line for the address
   * @param countryCode {String} An ISO country code. AU is the the only acceptable value!
   */
  function AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress(careOf, countryCode) {
    _classCallCheck(this, AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress);

    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.initialize(this, careOf, countryCode);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress, null, [{
    key: "initialize",
    value: function initialize(obj, careOf, countryCode) {
      obj['careOf'] = careOf;
      obj['countryCode'] = countryCode;
    }
    /**
     * Constructs a <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress} obj Optional instance to populate.
     * @return {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress} The populated <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress();

        if (data.hasOwnProperty('careOf')) {
          obj['careOf'] = _ApiClient["default"].convertToType(data['careOf'], 'String');
        }

        if (data.hasOwnProperty('buildingOrPropertyName')) {
          obj['buildingOrPropertyName'] = _ApiClient["default"].convertToType(data['buildingOrPropertyName'], 'String');
        }

        if (data.hasOwnProperty('buildingOrPropertyName2')) {
          obj['buildingOrPropertyName2'] = _ApiClient["default"].convertToType(data['buildingOrPropertyName2'], 'String');
        }

        if (data.hasOwnProperty('lotNumber')) {
          obj['lotNumber'] = _ApiClient["default"].convertToType(data['lotNumber'], 'String');
        }

        if (data.hasOwnProperty('flatOrUnitType')) {
          obj['flatOrUnitType'] = _ApiClient["default"].convertToType(data['flatOrUnitType'], 'String');
        }

        if (data.hasOwnProperty('flatOrUnitNumber')) {
          obj['flatOrUnitNumber'] = _ApiClient["default"].convertToType(data['flatOrUnitNumber'], 'String');
        }

        if (data.hasOwnProperty('floorOrLevelType')) {
          obj['floorOrLevelType'] = _ApiClient["default"].convertToType(data['floorOrLevelType'], 'String');
        }

        if (data.hasOwnProperty('floorOrLevelNumber')) {
          obj['floorOrLevelNumber'] = _ApiClient["default"].convertToType(data['floorOrLevelNumber'], 'String');
        }

        if (data.hasOwnProperty('houseNumber')) {
          obj['houseNumber'] = _ApiClient["default"].convertToType(data['houseNumber'], 'String');
        }

        if (data.hasOwnProperty('houseNumberSuffix')) {
          obj['houseNumberSuffix'] = _ApiClient["default"].convertToType(data['houseNumberSuffix'], 'String');
        }

        if (data.hasOwnProperty('houseNumber2')) {
          obj['houseNumber2'] = _ApiClient["default"].convertToType(data['houseNumber2'], 'String');
        }

        if (data.hasOwnProperty('houseNumberSuffix2')) {
          obj['houseNumberSuffix2'] = _ApiClient["default"].convertToType(data['houseNumberSuffix2'], 'String');
        }

        if (data.hasOwnProperty('streetName')) {
          obj['streetName'] = _ApiClient["default"].convertToType(data['streetName'], 'String');
        }

        if (data.hasOwnProperty('streetType')) {
          obj['streetType'] = _ApiClient["default"].convertToType(data['streetType'], 'String');
        }

        if (data.hasOwnProperty('streetSuffix')) {
          obj['streetSuffix'] = _ApiClient["default"].convertToType(data['streetSuffix'], 'String');
        }

        if (data.hasOwnProperty('suburbOrPlaceOrLocality')) {
          obj['suburbOrPlaceOrLocality'] = _ApiClient["default"].convertToType(data['suburbOrPlaceOrLocality'], 'String');
        }

        if (data.hasOwnProperty('locationDescriptor')) {
          obj['locationDescriptor'] = _ApiClient["default"].convertToType(data['locationDescriptor'], 'String');
        }

        if (data.hasOwnProperty('stateOrTerritory')) {
          obj['stateOrTerritory'] = _ApiClient["default"].convertToType(data['stateOrTerritory'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('countryCode')) {
          obj['countryCode'] = _ApiClient["default"].convertToType(data['countryCode'], 'String');
        }
      }

      return obj;
    }
  }]);

  return AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress;
}();
/**
 * The 'Care of' line for the address
 * @member {String} careOf
 */


AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['careOf'] = undefined;
/**
 * A free text description of the full name used to identify the physical building or property as part of its location.
 * @member {String} buildingOrPropertyName
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['buildingOrPropertyName'] = undefined;
/**
 * The overflow from above
 * @member {String} buildingOrPropertyName2
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['buildingOrPropertyName2'] = undefined;
/**
 * The lot reference number allocated to an address prior to street numbering.
 * @member {String} lotNumber
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['lotNumber'] = undefined;
/**
 * Specification of the type of flat or unit which is a separately identifiable portion within a building/complex.
 * @member {String} flatOrUnitType
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['flatOrUnitType'] = undefined;
/**
 * Specification of the number of the flat or unit which is a separately identifiable portion within a building/complex.
 * @member {String} flatOrUnitNumber
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['flatOrUnitNumber'] = undefined;
/**
 * Floor Type is used to identify the floor or level of a multi-storey building/complex..
 * @member {String} floorOrLevelType
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['floorOrLevelType'] = undefined;
/**
 * Floor Number is used to identify the floor or level of a multi-storey building/complex.
 * @member {String} floorOrLevelNumber
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['floorOrLevelNumber'] = undefined;
/**
 * The numeric reference of a house or property.
 * @member {String} houseNumber
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['houseNumber'] = undefined;
/**
 * The numeric reference of a house or property.
 * @member {String} houseNumberSuffix
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['houseNumberSuffix'] = undefined;
/**
 * The numeric reference of a house or property.
 * @member {String} houseNumber2
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['houseNumber2'] = undefined;
/**
 * The numeric reference of a house or property.
 * @member {String} houseNumberSuffix2
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['houseNumberSuffix2'] = undefined;
/**
 * Records the thoroughfare name.
 * @member {String} streetName
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['streetName'] = undefined;
/**
 * Records the street type abbreviation.
 * @member {String} streetType
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['streetType'] = undefined;
/**
 * Records street suffixes.
 * @member {String} streetSuffix
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['streetSuffix'] = undefined;
/**
 * The full name of the general locality containing the specific address
 * @member {String} suburbOrPlaceOrLocality
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['suburbOrPlaceOrLocality'] = undefined;
/**
 * A general field to capture various references to address locations alongside another physical location.
 * @member {String} locationDescriptor
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['locationDescriptor'] = undefined;
/**
 * Defined State or Territory abbreviation.
 * @member {String} stateOrTerritory
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['stateOrTerritory'] = undefined;
/**
 * Post code
 * @member {String} postcode
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['postcode'] = undefined;
/**
 * An ISO country code. AU is the the only acceptable value!
 * @member {String} countryCode
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.prototype['countryCode'] = undefined;
var _default = AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress;
exports["default"] = _default;