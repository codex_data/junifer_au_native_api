"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The StoreCardPostStripepaymentcardsRequestBody model module.
 * @module model/StoreCardPostStripepaymentcardsRequestBody
 * @version 1.61.1
 */
var StoreCardPostStripepaymentcardsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>StoreCardPostStripepaymentcardsRequestBody</code>.
   * @alias module:model/StoreCardPostStripepaymentcardsRequestBody
   * @param stripeCusId {String} The stripe customer id string
   */
  function StoreCardPostStripepaymentcardsRequestBody(stripeCusId) {
    _classCallCheck(this, StoreCardPostStripepaymentcardsRequestBody);

    StoreCardPostStripepaymentcardsRequestBody.initialize(this, stripeCusId);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(StoreCardPostStripepaymentcardsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, stripeCusId) {
      obj['stripeCusId'] = stripeCusId;
    }
    /**
     * Constructs a <code>StoreCardPostStripepaymentcardsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/StoreCardPostStripepaymentcardsRequestBody} obj Optional instance to populate.
     * @return {module:model/StoreCardPostStripepaymentcardsRequestBody} The populated <code>StoreCardPostStripepaymentcardsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new StoreCardPostStripepaymentcardsRequestBody();

        if (data.hasOwnProperty('last4')) {
          obj['last4'] = _ApiClient["default"].convertToType(data['last4'], 'String');
        }

        if (data.hasOwnProperty('expMonth')) {
          obj['expMonth'] = _ApiClient["default"].convertToType(data['expMonth'], 'String');
        }

        if (data.hasOwnProperty('expYear')) {
          obj['expYear'] = _ApiClient["default"].convertToType(data['expYear'], 'String');
        }

        if (data.hasOwnProperty('stripeCusId')) {
          obj['stripeCusId'] = _ApiClient["default"].convertToType(data['stripeCusId'], 'String');
        }

        if (data.hasOwnProperty('stripePaymentMethodId')) {
          obj['stripePaymentMethodId'] = _ApiClient["default"].convertToType(data['stripePaymentMethodId'], 'String');
        }
      }

      return obj;
    }
  }]);

  return StoreCardPostStripepaymentcardsRequestBody;
}();
/**
 * (deprecated) Last 4 digits of the card number
 * @member {String} last4
 */


StoreCardPostStripepaymentcardsRequestBody.prototype['last4'] = undefined;
/**
 * (deprecated) Cards expiry month
 * @member {String} expMonth
 */

StoreCardPostStripepaymentcardsRequestBody.prototype['expMonth'] = undefined;
/**
 * (deprecated) Cards expiry year
 * @member {String} expYear
 */

StoreCardPostStripepaymentcardsRequestBody.prototype['expYear'] = undefined;
/**
 * The stripe customer id string
 * @member {String} stripeCusId
 */

StoreCardPostStripepaymentcardsRequestBody.prototype['stripeCusId'] = undefined;
/**
 * The stripe payment method id string
 * @member {String} stripePaymentMethodId
 */

StoreCardPostStripepaymentcardsRequestBody.prototype['stripePaymentMethodId'] = undefined;
var _default = StoreCardPostStripepaymentcardsRequestBody;
exports["default"] = _default;