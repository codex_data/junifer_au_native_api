"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The PaymentPostAccountsIdPaymentResponse400 model module.
 * @module model/PaymentPostAccountsIdPaymentResponse400
 * @version 1.61.1
 */
var PaymentPostAccountsIdPaymentResponse400 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>PaymentPostAccountsIdPaymentResponse400</code>.
   * @alias module:model/PaymentPostAccountsIdPaymentResponse400
   * @param errorCode {module:model/PaymentPostAccountsIdPaymentResponse400.ErrorCodeEnum} Field: * `PaymentMethodTypeNotMatch` - The payment method type specified doesn't match anything in Junifer * `PaymentMethodTypeNotSupported` - The payment method type specified is not supported through this API * `InvalidStatus` - The status can only be 'Successful' or 'Cancelled' * `StatusNotMatch` - The status specified does not match anything in Junifer
   * @param errorSeverity {String} The error severity
   */
  function PaymentPostAccountsIdPaymentResponse400(errorCode, errorSeverity) {
    _classCallCheck(this, PaymentPostAccountsIdPaymentResponse400);

    PaymentPostAccountsIdPaymentResponse400.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(PaymentPostAccountsIdPaymentResponse400, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>PaymentPostAccountsIdPaymentResponse400</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/PaymentPostAccountsIdPaymentResponse400} obj Optional instance to populate.
     * @return {module:model/PaymentPostAccountsIdPaymentResponse400} The populated <code>PaymentPostAccountsIdPaymentResponse400</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new PaymentPostAccountsIdPaymentResponse400();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return PaymentPostAccountsIdPaymentResponse400;
}();
/**
 * Field: * `PaymentMethodTypeNotMatch` - The payment method type specified doesn't match anything in Junifer * `PaymentMethodTypeNotSupported` - The payment method type specified is not supported through this API * `InvalidStatus` - The status can only be 'Successful' or 'Cancelled' * `StatusNotMatch` - The status specified does not match anything in Junifer
 * @member {module:model/PaymentPostAccountsIdPaymentResponse400.ErrorCodeEnum} errorCode
 */


PaymentPostAccountsIdPaymentResponse400.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

PaymentPostAccountsIdPaymentResponse400.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

PaymentPostAccountsIdPaymentResponse400.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

PaymentPostAccountsIdPaymentResponse400['ErrorCodeEnum'] = {
  /**
   * value: "PaymentMethodTypeNotMatch"
   * @const
   */
  "PaymentMethodTypeNotMatch": "PaymentMethodTypeNotMatch",

  /**
   * value: "PaymentMethodTypeNotSupported"
   * @const
   */
  "PaymentMethodTypeNotSupported": "PaymentMethodTypeNotSupported",

  /**
   * value: "InvalidStatus"
   * @const
   */
  "InvalidStatus": "InvalidStatus",

  /**
   * value: "StatusNotMatch"
   * @const
   */
  "StatusNotMatch": "StatusNotMatch"
};
var _default = PaymentPostAccountsIdPaymentResponse400;
exports["default"] = _default;