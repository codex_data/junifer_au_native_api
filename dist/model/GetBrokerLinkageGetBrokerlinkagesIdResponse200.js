"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBrokerLinkageGetBrokerlinkagesIdResponse200Links = _interopRequireDefault(require("./GetBrokerLinkageGetBrokerlinkagesIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBrokerLinkageGetBrokerlinkagesIdResponse200 model module.
 * @module model/GetBrokerLinkageGetBrokerlinkagesIdResponse200
 * @version 1.61.1
 */
var GetBrokerLinkageGetBrokerlinkagesIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBrokerLinkageGetBrokerlinkagesIdResponse200</code>.
   * @alias module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse200
   * @param id {Number} Broker Linkage's ID
   * @param fromDt {Date} The date from which the customer/prospect's consent to be managed is valid
   * @param toDt {Date} The date to which the customer/prospect's consent to be managed is valid
   * @param quotingPermissions {Boolean} Indicates whether the customer/prospect has Quoting Permissions
   * @param billingPermissions {Boolean} Indicates whether the customer/prospect has Billing Permissions
   * @param acceptQuotingPermissions {Boolean} Indicates whether the customer/prospect has Accept Quoting Permissions
   * @param links {module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse200Links} 
   */
  function GetBrokerLinkageGetBrokerlinkagesIdResponse200(id, fromDt, toDt, quotingPermissions, billingPermissions, acceptQuotingPermissions, links) {
    _classCallCheck(this, GetBrokerLinkageGetBrokerlinkagesIdResponse200);

    GetBrokerLinkageGetBrokerlinkagesIdResponse200.initialize(this, id, fromDt, toDt, quotingPermissions, billingPermissions, acceptQuotingPermissions, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBrokerLinkageGetBrokerlinkagesIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, fromDt, toDt, quotingPermissions, billingPermissions, acceptQuotingPermissions, links) {
      obj['id'] = id;
      obj['fromDt'] = fromDt;
      obj['toDt'] = toDt;
      obj['quotingPermissions'] = quotingPermissions;
      obj['billingPermissions'] = billingPermissions;
      obj['acceptQuotingPermissions'] = acceptQuotingPermissions;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetBrokerLinkageGetBrokerlinkagesIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse200} The populated <code>GetBrokerLinkageGetBrokerlinkagesIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBrokerLinkageGetBrokerlinkagesIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }

        if (data.hasOwnProperty('quotingPermissions')) {
          obj['quotingPermissions'] = _ApiClient["default"].convertToType(data['quotingPermissions'], 'Boolean');
        }

        if (data.hasOwnProperty('billingPermissions')) {
          obj['billingPermissions'] = _ApiClient["default"].convertToType(data['billingPermissions'], 'Boolean');
        }

        if (data.hasOwnProperty('acceptQuotingPermissions')) {
          obj['acceptQuotingPermissions'] = _ApiClient["default"].convertToType(data['acceptQuotingPermissions'], 'Boolean');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetBrokerLinkageGetBrokerlinkagesIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetBrokerLinkageGetBrokerlinkagesIdResponse200;
}();
/**
 * Broker Linkage's ID
 * @member {Number} id
 */


GetBrokerLinkageGetBrokerlinkagesIdResponse200.prototype['id'] = undefined;
/**
 * The date from which the customer/prospect's consent to be managed is valid
 * @member {Date} fromDt
 */

GetBrokerLinkageGetBrokerlinkagesIdResponse200.prototype['fromDt'] = undefined;
/**
 * The date to which the customer/prospect's consent to be managed is valid
 * @member {Date} toDt
 */

GetBrokerLinkageGetBrokerlinkagesIdResponse200.prototype['toDt'] = undefined;
/**
 * Indicates whether the customer/prospect has Quoting Permissions
 * @member {Boolean} quotingPermissions
 */

GetBrokerLinkageGetBrokerlinkagesIdResponse200.prototype['quotingPermissions'] = undefined;
/**
 * Indicates whether the customer/prospect has Billing Permissions
 * @member {Boolean} billingPermissions
 */

GetBrokerLinkageGetBrokerlinkagesIdResponse200.prototype['billingPermissions'] = undefined;
/**
 * Indicates whether the customer/prospect has Accept Quoting Permissions
 * @member {Boolean} acceptQuotingPermissions
 */

GetBrokerLinkageGetBrokerlinkagesIdResponse200.prototype['acceptQuotingPermissions'] = undefined;
/**
 * @member {module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse200Links} links
 */

GetBrokerLinkageGetBrokerlinkagesIdResponse200.prototype['links'] = undefined;
var _default = GetBrokerLinkageGetBrokerlinkagesIdResponse200;
exports["default"] = _default;