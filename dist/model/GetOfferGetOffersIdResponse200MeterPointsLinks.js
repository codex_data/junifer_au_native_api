"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetOfferGetOffersIdResponse200MeterPointsLinks model module.
 * @module model/GetOfferGetOffersIdResponse200MeterPointsLinks
 * @version 1.61.1
 */
var GetOfferGetOffersIdResponse200MeterPointsLinks = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetOfferGetOffersIdResponse200MeterPointsLinks</code>.
   * The links to any related content associated with the meterpoint.
   * @alias module:model/GetOfferGetOffersIdResponse200MeterPointsLinks
   * @param self {String} link to the meterpoint itself.
   */
  function GetOfferGetOffersIdResponse200MeterPointsLinks(self) {
    _classCallCheck(this, GetOfferGetOffersIdResponse200MeterPointsLinks);

    GetOfferGetOffersIdResponse200MeterPointsLinks.initialize(this, self);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetOfferGetOffersIdResponse200MeterPointsLinks, null, [{
    key: "initialize",
    value: function initialize(obj, self) {
      obj['self'] = self;
    }
    /**
     * Constructs a <code>GetOfferGetOffersIdResponse200MeterPointsLinks</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetOfferGetOffersIdResponse200MeterPointsLinks} obj Optional instance to populate.
     * @return {module:model/GetOfferGetOffersIdResponse200MeterPointsLinks} The populated <code>GetOfferGetOffersIdResponse200MeterPointsLinks</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetOfferGetOffersIdResponse200MeterPointsLinks();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetOfferGetOffersIdResponse200MeterPointsLinks;
}();
/**
 * link to the meterpoint itself.
 * @member {String} self
 */


GetOfferGetOffersIdResponse200MeterPointsLinks.prototype['self'] = undefined;
var _default = GetOfferGetOffersIdResponse200MeterPointsLinks;
exports["default"] = _default;