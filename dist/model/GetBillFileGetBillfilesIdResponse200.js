"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBillFileGetBillfilesIdResponse200Links = _interopRequireDefault(require("./GetBillFileGetBillfilesIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillFileGetBillfilesIdResponse200 model module.
 * @module model/GetBillFileGetBillfilesIdResponse200
 * @version 1.61.1
 */
var GetBillFileGetBillfilesIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillFileGetBillfilesIdResponse200</code>.
   * @alias module:model/GetBillFileGetBillfilesIdResponse200
   * @param id {Number} Bill File Id
   * @param display {String} Can be one of `PDF`
   * @param viewable {Boolean} Available to download
   * @param links {module:model/GetBillFileGetBillfilesIdResponse200Links} 
   */
  function GetBillFileGetBillfilesIdResponse200(id, display, viewable, links) {
    _classCallCheck(this, GetBillFileGetBillfilesIdResponse200);

    GetBillFileGetBillfilesIdResponse200.initialize(this, id, display, viewable, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillFileGetBillfilesIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, display, viewable, links) {
      obj['id'] = id;
      obj['display'] = display;
      obj['viewable'] = viewable;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetBillFileGetBillfilesIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillFileGetBillfilesIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetBillFileGetBillfilesIdResponse200} The populated <code>GetBillFileGetBillfilesIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillFileGetBillfilesIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('display')) {
          obj['display'] = _ApiClient["default"].convertToType(data['display'], 'String');
        }

        if (data.hasOwnProperty('viewable')) {
          obj['viewable'] = _ApiClient["default"].convertToType(data['viewable'], 'Boolean');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetBillFileGetBillfilesIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetBillFileGetBillfilesIdResponse200;
}();
/**
 * Bill File Id
 * @member {Number} id
 */


GetBillFileGetBillfilesIdResponse200.prototype['id'] = undefined;
/**
 * Can be one of `PDF`
 * @member {String} display
 */

GetBillFileGetBillfilesIdResponse200.prototype['display'] = undefined;
/**
 * Available to download
 * @member {Boolean} viewable
 */

GetBillFileGetBillfilesIdResponse200.prototype['viewable'] = undefined;
/**
 * @member {module:model/GetBillFileGetBillfilesIdResponse200Links} links
 */

GetBillFileGetBillfilesIdResponse200.prototype['links'] = undefined;
var _default = GetBillFileGetBillfilesIdResponse200;
exports["default"] = _default;