"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillingEntityGetBillingentitiesIdResponse404 model module.
 * @module model/GetBillingEntityGetBillingentitiesIdResponse404
 * @version 1.61.1
 */
var GetBillingEntityGetBillingentitiesIdResponse404 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillingEntityGetBillingentitiesIdResponse404</code>.
   * @alias module:model/GetBillingEntityGetBillingentitiesIdResponse404
   * @param errorCode {module:model/GetBillingEntityGetBillingentitiesIdResponse404.ErrorCodeEnum} Field: * `NotFound` - Could not find '\"billingEntity\"' with Id 'billingEntityId'
   * @param errorSeverity {String} The error severity
   */
  function GetBillingEntityGetBillingentitiesIdResponse404(errorCode, errorSeverity) {
    _classCallCheck(this, GetBillingEntityGetBillingentitiesIdResponse404);

    GetBillingEntityGetBillingentitiesIdResponse404.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillingEntityGetBillingentitiesIdResponse404, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>GetBillingEntityGetBillingentitiesIdResponse404</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillingEntityGetBillingentitiesIdResponse404} obj Optional instance to populate.
     * @return {module:model/GetBillingEntityGetBillingentitiesIdResponse404} The populated <code>GetBillingEntityGetBillingentitiesIdResponse404</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillingEntityGetBillingentitiesIdResponse404();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetBillingEntityGetBillingentitiesIdResponse404;
}();
/**
 * Field: * `NotFound` - Could not find '\"billingEntity\"' with Id 'billingEntityId'
 * @member {module:model/GetBillingEntityGetBillingentitiesIdResponse404.ErrorCodeEnum} errorCode
 */


GetBillingEntityGetBillingentitiesIdResponse404.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

GetBillingEntityGetBillingentitiesIdResponse404.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

GetBillingEntityGetBillingentitiesIdResponse404.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

GetBillingEntityGetBillingentitiesIdResponse404['ErrorCodeEnum'] = {
  /**
   * value: "NotFound"
   * @const
   */
  "NotFound": "NotFound"
};
var _default = GetBillingEntityGetBillingentitiesIdResponse404;
exports["default"] = _default;