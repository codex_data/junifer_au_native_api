"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CommunicationsEmailsGetCommunicationsemailsIdResponse200Links model module.
 * @module model/CommunicationsEmailsGetCommunicationsemailsIdResponse200Links
 * @version 1.61.1
 */
var CommunicationsEmailsGetCommunicationsemailsIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CommunicationsEmailsGetCommunicationsemailsIdResponse200Links</code>.
   * Links to related resources
   * @alias module:model/CommunicationsEmailsGetCommunicationsemailsIdResponse200Links
   * @param self {String} Link to this email
   * @param communication {String} Link to the communication this email belongs to
   */
  function CommunicationsEmailsGetCommunicationsemailsIdResponse200Links(self, communication) {
    _classCallCheck(this, CommunicationsEmailsGetCommunicationsemailsIdResponse200Links);

    CommunicationsEmailsGetCommunicationsemailsIdResponse200Links.initialize(this, self, communication);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CommunicationsEmailsGetCommunicationsemailsIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, communication) {
      obj['self'] = self;
      obj['communication'] = communication;
    }
    /**
     * Constructs a <code>CommunicationsEmailsGetCommunicationsemailsIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CommunicationsEmailsGetCommunicationsemailsIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/CommunicationsEmailsGetCommunicationsemailsIdResponse200Links} The populated <code>CommunicationsEmailsGetCommunicationsemailsIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CommunicationsEmailsGetCommunicationsemailsIdResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('communication')) {
          obj['communication'] = _ApiClient["default"].convertToType(data['communication'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CommunicationsEmailsGetCommunicationsemailsIdResponse200Links;
}();
/**
 * Link to this email
 * @member {String} self
 */


CommunicationsEmailsGetCommunicationsemailsIdResponse200Links.prototype['self'] = undefined;
/**
 * Link to the communication this email belongs to
 * @member {String} communication
 */

CommunicationsEmailsGetCommunicationsemailsIdResponse200Links.prototype['communication'] = undefined;
var _default = CommunicationsEmailsGetCommunicationsemailsIdResponse200Links;
exports["default"] = _default;