"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers = _interopRequireDefault(require("./GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters model module.
 * @module model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters
 * @version 1.61.1
 */
var GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters</code>.
   * @alias module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters
   * @param id {Number} Meter ID
   * @param identifier {String} Meter identifier
   * @param registers {Array.<module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers>} Array with meter's registers
   */
  function GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters(id, identifier, registers) {
    _classCallCheck(this, GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters);

    GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters.initialize(this, id, identifier, registers);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters, null, [{
    key: "initialize",
    value: function initialize(obj, id, identifier, registers) {
      obj['id'] = id;
      obj['identifier'] = identifier;
      obj['registers'] = registers;
    }
    /**
     * Constructs a <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters} obj Optional instance to populate.
     * @return {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters} The populated <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('identifier')) {
          obj['identifier'] = _ApiClient["default"].convertToType(data['identifier'], 'String');
        }

        if (data.hasOwnProperty('meterType')) {
          obj['meterType'] = _ApiClient["default"].convertToType(data['meterType'], 'String');
        }

        if (data.hasOwnProperty('meterTypeDescription')) {
          obj['meterTypeDescription'] = _ApiClient["default"].convertToType(data['meterTypeDescription'], 'String');
        }

        if (data.hasOwnProperty('registers')) {
          obj['registers'] = _ApiClient["default"].convertToType(data['registers'], [_GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers["default"]]);
        }
      }

      return obj;
    }
  }]);

  return GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters;
}();
/**
 * Meter ID
 * @member {Number} id
 */


GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters.prototype['id'] = undefined;
/**
 * Meter identifier
 * @member {String} identifier
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters.prototype['identifier'] = undefined;
/**
 * Meter type code. A full list of possible values can be found in the Meter Type screen.
 * @member {String} meterType
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters.prototype['meterType'] = undefined;
/**
 * Meter type description.
 * @member {String} meterTypeDescription
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters.prototype['meterTypeDescription'] = undefined;
/**
 * Array with meter's registers
 * @member {Array.<module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers>} registers
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters.prototype['registers'] = undefined;
var _default = GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters;
exports["default"] = _default;