"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files = _interopRequireDefault(require("./GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountCommunicationsGetAccountsIdCommunicationsResponse200 model module.
 * @module model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200
 * @version 1.61.1
 */
var GetAccountCommunicationsGetAccountsIdCommunicationsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountCommunicationsGetAccountsIdCommunicationsResponse200</code>.
   * @alias module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200
   * @param id {Number} The ID of the communication
   * @param type {String} the type of communication
   * @param status {String} the status of the communication
   * @param createdDttm {Date} date and time that the communication was sent
   * @param testFl {Boolean} Is this communication a test (never sent to the customer)
   * @param files {Array.<module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files>} the communication files
   * @param links {String} 
   */
  function GetAccountCommunicationsGetAccountsIdCommunicationsResponse200(id, type, status, createdDttm, testFl, files, links) {
    _classCallCheck(this, GetAccountCommunicationsGetAccountsIdCommunicationsResponse200);

    GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.initialize(this, id, type, status, createdDttm, testFl, files, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountCommunicationsGetAccountsIdCommunicationsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, type, status, createdDttm, testFl, files, links) {
      obj['id'] = id;
      obj['type'] = type;
      obj['status'] = status;
      obj['createdDttm'] = createdDttm;
      obj['testFl'] = testFl;
      obj['files'] = files;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountCommunicationsGetAccountsIdCommunicationsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200} The populated <code>GetAccountCommunicationsGetAccountsIdCommunicationsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountCommunicationsGetAccountsIdCommunicationsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('testFl')) {
          obj['testFl'] = _ApiClient["default"].convertToType(data['testFl'], 'Boolean');
        }

        if (data.hasOwnProperty('files')) {
          obj['files'] = _ApiClient["default"].convertToType(data['files'], [_GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files["default"]]);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _ApiClient["default"].convertToType(data['links'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountCommunicationsGetAccountsIdCommunicationsResponse200;
}();
/**
 * The ID of the communication
 * @member {Number} id
 */


GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.prototype['id'] = undefined;
/**
 * the type of communication
 * @member {String} type
 */

GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.prototype['type'] = undefined;
/**
 * the status of the communication
 * @member {String} status
 */

GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.prototype['status'] = undefined;
/**
 * date and time that the communication was sent
 * @member {Date} createdDttm
 */

GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.prototype['createdDttm'] = undefined;
/**
 * Is this communication a test (never sent to the customer)
 * @member {Boolean} testFl
 */

GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.prototype['testFl'] = undefined;
/**
 * the communication files
 * @member {Array.<module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files>} files
 */

GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.prototype['files'] = undefined;
/**
 * 
 * @member {String} links
 */

GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.prototype['links'] = undefined;
var _default = GetAccountCommunicationsGetAccountsIdCommunicationsResponse200;
exports["default"] = _default;