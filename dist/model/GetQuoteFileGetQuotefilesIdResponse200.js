"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetQuoteFileGetQuotefilesIdResponse200Links = _interopRequireDefault(require("./GetQuoteFileGetQuotefilesIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetQuoteFileGetQuotefilesIdResponse200 model module.
 * @module model/GetQuoteFileGetQuotefilesIdResponse200
 * @version 1.61.1
 */
var GetQuoteFileGetQuotefilesIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetQuoteFileGetQuotefilesIdResponse200</code>.
   * @alias module:model/GetQuoteFileGetQuotefilesIdResponse200
   * @param id {Number} Quote file ID
   * @param display {String} Quote file description
   * @param viewable {Boolean} Flag indicating whether the quote file can be viewed
   * @param createdDttm {Date} The date and time the quote file was created
   * @param links {module:model/GetQuoteFileGetQuotefilesIdResponse200Links} 
   */
  function GetQuoteFileGetQuotefilesIdResponse200(id, display, viewable, createdDttm, links) {
    _classCallCheck(this, GetQuoteFileGetQuotefilesIdResponse200);

    GetQuoteFileGetQuotefilesIdResponse200.initialize(this, id, display, viewable, createdDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetQuoteFileGetQuotefilesIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, display, viewable, createdDttm, links) {
      obj['id'] = id;
      obj['display'] = display;
      obj['viewable'] = viewable;
      obj['createdDttm'] = createdDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetQuoteFileGetQuotefilesIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetQuoteFileGetQuotefilesIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetQuoteFileGetQuotefilesIdResponse200} The populated <code>GetQuoteFileGetQuotefilesIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetQuoteFileGetQuotefilesIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('display')) {
          obj['display'] = _ApiClient["default"].convertToType(data['display'], 'String');
        }

        if (data.hasOwnProperty('viewable')) {
          obj['viewable'] = _ApiClient["default"].convertToType(data['viewable'], 'Boolean');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetQuoteFileGetQuotefilesIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetQuoteFileGetQuotefilesIdResponse200;
}();
/**
 * Quote file ID
 * @member {Number} id
 */


GetQuoteFileGetQuotefilesIdResponse200.prototype['id'] = undefined;
/**
 * Quote file description
 * @member {String} display
 */

GetQuoteFileGetQuotefilesIdResponse200.prototype['display'] = undefined;
/**
 * Flag indicating whether the quote file can be viewed
 * @member {Boolean} viewable
 */

GetQuoteFileGetQuotefilesIdResponse200.prototype['viewable'] = undefined;
/**
 * The date and time the quote file was created
 * @member {Date} createdDttm
 */

GetQuoteFileGetQuotefilesIdResponse200.prototype['createdDttm'] = undefined;
/**
 * @member {module:model/GetQuoteFileGetQuotefilesIdResponse200Links} links
 */

GetQuoteFileGetQuotefilesIdResponse200.prototype['links'] = undefined;
var _default = GetQuoteFileGetQuotefilesIdResponse200;
exports["default"] = _default;