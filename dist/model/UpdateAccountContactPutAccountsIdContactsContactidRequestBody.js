"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateAccountContactPutAccountsIdContactsContactidRequestBody model module.
 * @module model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody
 * @version 1.61.1
 */
var UpdateAccountContactPutAccountsIdContactsContactidRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateAccountContactPutAccountsIdContactsContactidRequestBody</code>.
   * @alias module:model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody
   * @param surname {String} Contact's surname
   * @param receivePost {Boolean} Contacts communication postal delivery preference. If left out, will be set to false.
   * @param receiveEmail {Boolean} Contacts communication email delivery preference. If left out, will be set to false.
   * @param receiveSMS {Boolean} Contacts communication SMS delivery preference. If left out, will be set to false.
   */
  function UpdateAccountContactPutAccountsIdContactsContactidRequestBody(surname, receivePost, receiveEmail, receiveSMS) {
    _classCallCheck(this, UpdateAccountContactPutAccountsIdContactsContactidRequestBody);

    UpdateAccountContactPutAccountsIdContactsContactidRequestBody.initialize(this, surname, receivePost, receiveEmail, receiveSMS);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateAccountContactPutAccountsIdContactsContactidRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, surname, receivePost, receiveEmail, receiveSMS) {
      obj['surname'] = surname;
      obj['receivePost'] = receivePost;
      obj['receiveEmail'] = receiveEmail;
      obj['receiveSMS'] = receiveSMS;
    }
    /**
     * Constructs a <code>UpdateAccountContactPutAccountsIdContactsContactidRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody} obj Optional instance to populate.
     * @return {module:model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody} The populated <code>UpdateAccountContactPutAccountsIdContactsContactidRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateAccountContactPutAccountsIdContactsContactidRequestBody();

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('phoneNumber1')) {
          obj['phoneNumber1'] = _ApiClient["default"].convertToType(data['phoneNumber1'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber2')) {
          obj['phoneNumber2'] = _ApiClient["default"].convertToType(data['phoneNumber2'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber3')) {
          obj['phoneNumber3'] = _ApiClient["default"].convertToType(data['phoneNumber3'], 'String');
        }

        if (data.hasOwnProperty('billDelivery')) {
          obj['billDelivery'] = _ApiClient["default"].convertToType(data['billDelivery'], 'String');
        }

        if (data.hasOwnProperty('receivePost')) {
          obj['receivePost'] = _ApiClient["default"].convertToType(data['receivePost'], 'Boolean');
        }

        if (data.hasOwnProperty('receiveEmail')) {
          obj['receiveEmail'] = _ApiClient["default"].convertToType(data['receiveEmail'], 'Boolean');
        }

        if (data.hasOwnProperty('receiveSMS')) {
          obj['receiveSMS'] = _ApiClient["default"].convertToType(data['receiveSMS'], 'Boolean');
        }
      }

      return obj;
    }
  }]);

  return UpdateAccountContactPutAccountsIdContactsContactidRequestBody;
}();
/**
 * Contact's title. Can be `Dr`, `Miss`, `Mr`, `Mrs`, `Ms`, `Prof`
 * @member {String} title
 */


UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['title'] = undefined;
/**
 * Contact's first name
 * @member {String} forename
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['forename'] = undefined;
/**
 * Contact's surname
 * @member {String} surname
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['surname'] = undefined;
/**
 * Email address
 * @member {String} email
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['email'] = undefined;
/**
 * Contact's date of birth
 * @member {Date} dateOfBirth
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['dateOfBirth'] = undefined;
/**
 * Phone number 1
 * @member {String} phoneNumber1
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['phoneNumber1'] = undefined;
/**
 * Phone number 2
 * @member {String} phoneNumber2
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['phoneNumber2'] = undefined;
/**
 * Phone number 3
 * @member {String} phoneNumber3
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['phoneNumber3'] = undefined;
/**
 * Contacts Bill Delivery Preference. Can be `Email`, `Mail`, `Both`, `None`
 * @member {String} billDelivery
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['billDelivery'] = undefined;
/**
 * Contacts communication postal delivery preference. If left out, will be set to false.
 * @member {Boolean} receivePost
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['receivePost'] = undefined;
/**
 * Contacts communication email delivery preference. If left out, will be set to false.
 * @member {Boolean} receiveEmail
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['receiveEmail'] = undefined;
/**
 * Contacts communication SMS delivery preference. If left out, will be set to false.
 * @member {Boolean} receiveSMS
 */

UpdateAccountContactPutAccountsIdContactsContactidRequestBody.prototype['receiveSMS'] = undefined;
var _default = UpdateAccountContactPutAccountsIdContactsContactidRequestBody;
exports["default"] = _default;