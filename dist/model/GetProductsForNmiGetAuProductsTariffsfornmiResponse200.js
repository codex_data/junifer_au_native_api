"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetProductsForNmiGetAuProductsTariffsfornmiResponse200 model module.
 * @module model/GetProductsForNmiGetAuProductsTariffsfornmiResponse200
 * @version 1.61.1
 */
var GetProductsForNmiGetAuProductsTariffsfornmiResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetProductsForNmiGetAuProductsTariffsfornmiResponse200</code>.
   * @alias module:model/GetProductsForNmiGetAuProductsTariffsfornmiResponse200
   * @param nmi {String} Identifier of the NMI identified from the supplied details
   * @param code {String} Product code, also known as product reference in Junifer
   * @param name {String} Product name
   * @param display {String} Product display string
   * @param availableToPublic {Boolean} Flag indicating whether the product was set as publicly available in Junifer
   * @param endDttm {Date} The date the product is available until
   * @param unitRates {Object} Object holding unit rates in dollars for the product. There will be one line per unit rate on the price plan
   */
  function GetProductsForNmiGetAuProductsTariffsfornmiResponse200(nmi, code, name, display, availableToPublic, endDttm, unitRates) {
    _classCallCheck(this, GetProductsForNmiGetAuProductsTariffsfornmiResponse200);

    GetProductsForNmiGetAuProductsTariffsfornmiResponse200.initialize(this, nmi, code, name, display, availableToPublic, endDttm, unitRates);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetProductsForNmiGetAuProductsTariffsfornmiResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, nmi, code, name, display, availableToPublic, endDttm, unitRates) {
      obj['nmi'] = nmi;
      obj['code'] = code;
      obj['name'] = name;
      obj['display'] = display;
      obj['availableToPublic'] = availableToPublic;
      obj['endDttm'] = endDttm;
      obj['unitRates'] = unitRates;
    }
    /**
     * Constructs a <code>GetProductsForNmiGetAuProductsTariffsfornmiResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetProductsForNmiGetAuProductsTariffsfornmiResponse200} obj Optional instance to populate.
     * @return {module:model/GetProductsForNmiGetAuProductsTariffsfornmiResponse200} The populated <code>GetProductsForNmiGetAuProductsTariffsfornmiResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetProductsForNmiGetAuProductsTariffsfornmiResponse200();

        if (data.hasOwnProperty('nmi')) {
          obj['nmi'] = _ApiClient["default"].convertToType(data['nmi'], 'String');
        }

        if (data.hasOwnProperty('code')) {
          obj['code'] = _ApiClient["default"].convertToType(data['code'], 'String');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('display')) {
          obj['display'] = _ApiClient["default"].convertToType(data['display'], 'String');
        }

        if (data.hasOwnProperty('availableToPublic')) {
          obj['availableToPublic'] = _ApiClient["default"].convertToType(data['availableToPublic'], 'Boolean');
        }

        if (data.hasOwnProperty('endDttm')) {
          obj['endDttm'] = _ApiClient["default"].convertToType(data['endDttm'], 'Date');
        }

        if (data.hasOwnProperty('durationMonths')) {
          obj['durationMonths'] = _ApiClient["default"].convertToType(data['durationMonths'], 'Number');
        }

        if (data.hasOwnProperty('directDebit')) {
          obj['directDebit'] = _ApiClient["default"].convertToType(data['directDebit'], 'Boolean');
        }

        if (data.hasOwnProperty('prepay')) {
          obj['prepay'] = _ApiClient["default"].convertToType(data['prepay'], 'Boolean');
        }

        if (data.hasOwnProperty('standingCharge')) {
          obj['standingCharge'] = _ApiClient["default"].convertToType(data['standingCharge'], 'Number');
        }

        if (data.hasOwnProperty('unitRates')) {
          obj['unitRates'] = _ApiClient["default"].convertToType(data['unitRates'], Object);
        }
      }

      return obj;
    }
  }]);

  return GetProductsForNmiGetAuProductsTariffsfornmiResponse200;
}();
/**
 * Identifier of the NMI identified from the supplied details
 * @member {String} nmi
 */


GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['nmi'] = undefined;
/**
 * Product code, also known as product reference in Junifer
 * @member {String} code
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['code'] = undefined;
/**
 * Product name
 * @member {String} name
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['name'] = undefined;
/**
 * Product display string
 * @member {String} display
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['display'] = undefined;
/**
 * Flag indicating whether the product was set as publicly available in Junifer
 * @member {Boolean} availableToPublic
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['availableToPublic'] = undefined;
/**
 * The date the product is available until
 * @member {Date} endDttm
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['endDttm'] = undefined;
/**
 * The duration of the product if the product is a fixed term product
 * @member {Number} durationMonths
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['durationMonths'] = undefined;
/**
 * A Direct Debit flag indicating whether the product is for the Direct Debit customer.
 * @member {Boolean} directDebit
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['directDebit'] = undefined;
/**
 * A prepay flag indicating whether the product is for the prepay customer.
 * @member {Boolean} prepay
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['prepay'] = undefined;
/**
 * The standing charge in cents for the product expressed as p/day. Present only if Standing Charge product item is configured for the product
 * @member {Number} standingCharge
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['standingCharge'] = undefined;
/**
 * Object holding unit rates in dollars for the product. There will be one line per unit rate on the price plan
 * @member {Object} unitRates
 */

GetProductsForNmiGetAuProductsTariffsfornmiResponse200.prototype['unitRates'] = undefined;
var _default = GetProductsForNmiGetAuProductsTariffsfornmiResponse200;
exports["default"] = _default;