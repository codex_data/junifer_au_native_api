"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBillGetBillsIdResponse200BillFiles = _interopRequireDefault(require("./GetBillGetBillsIdResponse200BillFiles"));

var _GetBillGetBillsIdResponse200Links = _interopRequireDefault(require("./GetBillGetBillsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillGetBillsIdResponse200 model module.
 * @module model/GetBillGetBillsIdResponse200
 * @version 1.61.1
 */
var GetBillGetBillsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillGetBillsIdResponse200</code>.
   * @alias module:model/GetBillGetBillsIdResponse200
   * @param id {Number} The id of bill
   * @param number {String} The unique bill number
   * @param createdDttm {Date} Date and time when the bill was created within Junifer
   * @param status {String} Status of the Bill
   * @param currency {String} Currency of the bill
   * @param grossAmount {Number} Gross amount shown on the bill
   * @param netAmount {Number} Net amount shown on the bill
   * @param salesTaxAmount {Number} Sales tax amount shown on the bill
   * @param issueDt {Date} Date the bill was issued on
   * @param supersededFl {Boolean} Flag indicating if the bill was superseded
   * @param _final {Boolean} Flag indicating if this bill is final
   * @param dirtyBillFl {Boolean} Flag indicating if this bill is dirty
   * @param versionNumber {Number} Bill's version number
   * @param balance {Number} Bill's balance
   * @param links {module:model/GetBillGetBillsIdResponse200Links} 
   * @param billFiles {Array.<module:model/GetBillGetBillsIdResponse200BillFiles>} Downloadable bill files
   */
  function GetBillGetBillsIdResponse200(id, number, createdDttm, status, currency, grossAmount, netAmount, salesTaxAmount, issueDt, supersededFl, _final, dirtyBillFl, versionNumber, balance, links, billFiles) {
    _classCallCheck(this, GetBillGetBillsIdResponse200);

    GetBillGetBillsIdResponse200.initialize(this, id, number, createdDttm, status, currency, grossAmount, netAmount, salesTaxAmount, issueDt, supersededFl, _final, dirtyBillFl, versionNumber, balance, links, billFiles);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillGetBillsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, number, createdDttm, status, currency, grossAmount, netAmount, salesTaxAmount, issueDt, supersededFl, _final, dirtyBillFl, versionNumber, balance, links, billFiles) {
      obj['id'] = id;
      obj['number'] = number;
      obj['createdDttm'] = createdDttm;
      obj['status'] = status;
      obj['currency'] = currency;
      obj['grossAmount'] = grossAmount;
      obj['netAmount'] = netAmount;
      obj['salesTaxAmount'] = salesTaxAmount;
      obj['issueDt'] = issueDt;
      obj['supersededFl'] = supersededFl;
      obj['final'] = _final;
      obj['dirtyBillFl'] = dirtyBillFl;
      obj['versionNumber'] = versionNumber;
      obj['balance'] = balance;
      obj['links'] = links;
      obj['billFiles'] = billFiles;
    }
    /**
     * Constructs a <code>GetBillGetBillsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillGetBillsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetBillGetBillsIdResponse200} The populated <code>GetBillGetBillsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillGetBillsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('billPeriodId')) {
          obj['billPeriodId'] = _ApiClient["default"].convertToType(data['billPeriodId'], 'Number');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('draftReason')) {
          obj['draftReason'] = _ApiClient["default"].convertToType(data['draftReason'], 'String');
        }

        if (data.hasOwnProperty('acceptedDttm')) {
          obj['acceptedDttm'] = _ApiClient["default"].convertToType(data['acceptedDttm'], 'Date');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('grossAmount')) {
          obj['grossAmount'] = _ApiClient["default"].convertToType(data['grossAmount'], 'Number');
        }

        if (data.hasOwnProperty('netAmount')) {
          obj['netAmount'] = _ApiClient["default"].convertToType(data['netAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTaxAmount')) {
          obj['salesTaxAmount'] = _ApiClient["default"].convertToType(data['salesTaxAmount'], 'Number');
        }

        if (data.hasOwnProperty('lastShownAccountTransactionId')) {
          obj['lastShownAccountTransactionId'] = _ApiClient["default"].convertToType(data['lastShownAccountTransactionId'], 'Number');
        }

        if (data.hasOwnProperty('periodFrom')) {
          obj['periodFrom'] = _ApiClient["default"].convertToType(data['periodFrom'], 'Date');
        }

        if (data.hasOwnProperty('periodTo')) {
          obj['periodTo'] = _ApiClient["default"].convertToType(data['periodTo'], 'Date');
        }

        if (data.hasOwnProperty('issueDt')) {
          obj['issueDt'] = _ApiClient["default"].convertToType(data['issueDt'], 'Date');
        }

        if (data.hasOwnProperty('dueDt')) {
          obj['dueDt'] = _ApiClient["default"].convertToType(data['dueDt'], 'Date');
        }

        if (data.hasOwnProperty('lessThanMinimumFl')) {
          obj['lessThanMinimumFl'] = _ApiClient["default"].convertToType(data['lessThanMinimumFl'], 'Boolean');
        }

        if (data.hasOwnProperty('supersededFl')) {
          obj['supersededFl'] = _ApiClient["default"].convertToType(data['supersededFl'], 'Boolean');
        }

        if (data.hasOwnProperty('supersededByBillId')) {
          obj['supersededByBillId'] = _ApiClient["default"].convertToType(data['supersededByBillId'], 'Number');
        }

        if (data.hasOwnProperty('final')) {
          obj['final'] = _ApiClient["default"].convertToType(data['final'], 'Boolean');
        }

        if (data.hasOwnProperty('dirtyBillFl')) {
          obj['dirtyBillFl'] = _ApiClient["default"].convertToType(data['dirtyBillFl'], 'Boolean');
        }

        if (data.hasOwnProperty('consolidatedBillFl')) {
          obj['consolidatedBillFl'] = _ApiClient["default"].convertToType(data['consolidatedBillFl'], 'Boolean');
        }

        if (data.hasOwnProperty('versionNumber')) {
          obj['versionNumber'] = _ApiClient["default"].convertToType(data['versionNumber'], 'Number');
        }

        if (data.hasOwnProperty('balance')) {
          obj['balance'] = _ApiClient["default"].convertToType(data['balance'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetBillGetBillsIdResponse200Links["default"].constructFromObject(data['links']);
        }

        if (data.hasOwnProperty('billFiles')) {
          obj['billFiles'] = _ApiClient["default"].convertToType(data['billFiles'], [_GetBillGetBillsIdResponse200BillFiles["default"]]);
        }
      }

      return obj;
    }
  }]);

  return GetBillGetBillsIdResponse200;
}();
/**
 * The id of bill
 * @member {Number} id
 */


GetBillGetBillsIdResponse200.prototype['id'] = undefined;
/**
 * The id of bill Period
 * @member {Number} billPeriodId
 */

GetBillGetBillsIdResponse200.prototype['billPeriodId'] = undefined;
/**
 * The unique bill number
 * @member {String} number
 */

GetBillGetBillsIdResponse200.prototype['number'] = undefined;
/**
 * Date and time when the bill was created within Junifer
 * @member {Date} createdDttm
 */

GetBillGetBillsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * Status of the Bill
 * @member {String} status
 */

GetBillGetBillsIdResponse200.prototype['status'] = undefined;
/**
 * A description of why the bill was put into draft, such as suspicious data or default behaviours
 * @member {String} draftReason
 */

GetBillGetBillsIdResponse200.prototype['draftReason'] = undefined;
/**
 * Datetime the bill was accepted
 * @member {Date} acceptedDttm
 */

GetBillGetBillsIdResponse200.prototype['acceptedDttm'] = undefined;
/**
 * Currency of the bill
 * @member {String} currency
 */

GetBillGetBillsIdResponse200.prototype['currency'] = undefined;
/**
 * Gross amount shown on the bill
 * @member {Number} grossAmount
 */

GetBillGetBillsIdResponse200.prototype['grossAmount'] = undefined;
/**
 * Net amount shown on the bill
 * @member {Number} netAmount
 */

GetBillGetBillsIdResponse200.prototype['netAmount'] = undefined;
/**
 * Sales tax amount shown on the bill
 * @member {Number} salesTaxAmount
 */

GetBillGetBillsIdResponse200.prototype['salesTaxAmount'] = undefined;
/**
 * The latest account transaction that was included in this Bill's brought forward balance statement
 * @member {Number} lastShownAccountTransactionId
 */

GetBillGetBillsIdResponse200.prototype['lastShownAccountTransactionId'] = undefined;
/**
 * The start date of the billed period
 * @member {Date} periodFrom
 */

GetBillGetBillsIdResponse200.prototype['periodFrom'] = undefined;
/**
 * The end date of the billed period
 * @member {Date} periodTo
 */

GetBillGetBillsIdResponse200.prototype['periodTo'] = undefined;
/**
 * Date the bill was issued on
 * @member {Date} issueDt
 */

GetBillGetBillsIdResponse200.prototype['issueDt'] = undefined;
/**
 * Date the bill was due on
 * @member {Date} dueDt
 */

GetBillGetBillsIdResponse200.prototype['dueDt'] = undefined;
/**
 * Set to true if this Bill did not meet the 'minimum bill amount' specified for an Account
 * @member {Boolean} lessThanMinimumFl
 */

GetBillGetBillsIdResponse200.prototype['lessThanMinimumFl'] = undefined;
/**
 * Flag indicating if the bill was superseded
 * @member {Boolean} supersededFl
 */

GetBillGetBillsIdResponse200.prototype['supersededFl'] = undefined;
/**
 * Id of the Bill that has been replaced this version
 * @member {Number} supersededByBillId
 */

GetBillGetBillsIdResponse200.prototype['supersededByBillId'] = undefined;
/**
 * Flag indicating if this bill is final
 * @member {Boolean} final
 */

GetBillGetBillsIdResponse200.prototype['final'] = undefined;
/**
 * Flag indicating if this bill is dirty
 * @member {Boolean} dirtyBillFl
 */

GetBillGetBillsIdResponse200.prototype['dirtyBillFl'] = undefined;
/**
 * Flag indicating if this bill is consolidated
 * @member {Boolean} consolidatedBillFl
 */

GetBillGetBillsIdResponse200.prototype['consolidatedBillFl'] = undefined;
/**
 * Bill's version number
 * @member {Number} versionNumber
 */

GetBillGetBillsIdResponse200.prototype['versionNumber'] = undefined;
/**
 * Bill's balance
 * @member {Number} balance
 */

GetBillGetBillsIdResponse200.prototype['balance'] = undefined;
/**
 * @member {module:model/GetBillGetBillsIdResponse200Links} links
 */

GetBillGetBillsIdResponse200.prototype['links'] = undefined;
/**
 * Downloadable bill files
 * @member {Array.<module:model/GetBillGetBillsIdResponse200BillFiles>} billFiles
 */

GetBillGetBillsIdResponse200.prototype['billFiles'] = undefined;
var _default = GetBillGetBillsIdResponse200;
exports["default"] = _default;