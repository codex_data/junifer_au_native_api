"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCommunicationsGetCommunicationsIdResponse200Links model module.
 * @module model/GetCommunicationsGetCommunicationsIdResponse200Links
 * @version 1.61.1
 */
var GetCommunicationsGetCommunicationsIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCommunicationsGetCommunicationsIdResponse200Links</code>.
   * 
   * @alias module:model/GetCommunicationsGetCommunicationsIdResponse200Links
   * @param image {String} link to the communication message
   */
  function GetCommunicationsGetCommunicationsIdResponse200Links(image) {
    _classCallCheck(this, GetCommunicationsGetCommunicationsIdResponse200Links);

    GetCommunicationsGetCommunicationsIdResponse200Links.initialize(this, image);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCommunicationsGetCommunicationsIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, image) {
      obj['image'] = image;
    }
    /**
     * Constructs a <code>GetCommunicationsGetCommunicationsIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCommunicationsGetCommunicationsIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetCommunicationsGetCommunicationsIdResponse200Links} The populated <code>GetCommunicationsGetCommunicationsIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCommunicationsGetCommunicationsIdResponse200Links();

        if (data.hasOwnProperty('image')) {
          obj['image'] = _ApiClient["default"].convertToType(data['image'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetCommunicationsGetCommunicationsIdResponse200Links;
}();
/**
 * link to the communication message
 * @member {String} image
 */


GetCommunicationsGetCommunicationsIdResponse200Links.prototype['image'] = undefined;
var _default = GetCommunicationsGetCommunicationsIdResponse200Links;
exports["default"] = _default;