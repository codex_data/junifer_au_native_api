"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCustomerGetCustomersIdResponse200CompanyAddress model module.
 * @module model/GetCustomerGetCustomersIdResponse200CompanyAddress
 * @version 1.61.1
 */
var GetCustomerGetCustomersIdResponse200CompanyAddress = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCustomerGetCustomersIdResponse200CompanyAddress</code>.
   * Customer&#39;s company registered address
   * @alias module:model/GetCustomerGetCustomersIdResponse200CompanyAddress
   */
  function GetCustomerGetCustomersIdResponse200CompanyAddress() {
    _classCallCheck(this, GetCustomerGetCustomersIdResponse200CompanyAddress);

    GetCustomerGetCustomersIdResponse200CompanyAddress.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCustomerGetCustomersIdResponse200CompanyAddress, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>GetCustomerGetCustomersIdResponse200CompanyAddress</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCustomerGetCustomersIdResponse200CompanyAddress} obj Optional instance to populate.
     * @return {module:model/GetCustomerGetCustomersIdResponse200CompanyAddress} The populated <code>GetCustomerGetCustomersIdResponse200CompanyAddress</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCustomerGetCustomersIdResponse200CompanyAddress();

        if (data.hasOwnProperty('addressType')) {
          obj['addressType'] = _ApiClient["default"].convertToType(data['addressType'], 'String');
        }

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('countryCode')) {
          obj['countryCode'] = _ApiClient["default"].convertToType(data['countryCode'], 'String');
        }

        if (data.hasOwnProperty('country')) {
          obj['country'] = _ApiClient["default"].convertToType(data['country'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetCustomerGetCustomersIdResponse200CompanyAddress;
}();
/**
 * Internally used address type which defines number of lines and format used etc.
 * @member {String} addressType
 */


GetCustomerGetCustomersIdResponse200CompanyAddress.prototype['addressType'] = undefined;
/**
 * First address line
 * @member {String} address1
 */

GetCustomerGetCustomersIdResponse200CompanyAddress.prototype['address1'] = undefined;
/**
 * Second address line
 * @member {String} address2
 */

GetCustomerGetCustomersIdResponse200CompanyAddress.prototype['address2'] = undefined;
/**
 * Third address line
 * @member {String} address3
 */

GetCustomerGetCustomersIdResponse200CompanyAddress.prototype['address3'] = undefined;
/**
 * Fourth address line
 * @member {String} address4
 */

GetCustomerGetCustomersIdResponse200CompanyAddress.prototype['address4'] = undefined;
/**
 * Fifth address line
 * @member {String} address5
 */

GetCustomerGetCustomersIdResponse200CompanyAddress.prototype['address5'] = undefined;
/**
 * Post code
 * @member {String} postcode
 */

GetCustomerGetCustomersIdResponse200CompanyAddress.prototype['postcode'] = undefined;
/**
 * Country code
 * @member {String} countryCode
 */

GetCustomerGetCustomersIdResponse200CompanyAddress.prototype['countryCode'] = undefined;
/**
 * Country name
 * @member {String} country
 */

GetCustomerGetCustomersIdResponse200CompanyAddress.prototype['country'] = undefined;
var _default = GetCustomerGetCustomersIdResponse200CompanyAddress;
exports["default"] = _default;