"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200 model module.
 * @module model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200
 * @version 1.61.1
 */
var CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200</code>.
   * @alias module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200
   * @param requestId {String} Payment request ID
   */
  function CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200(requestId) {
    _classCallCheck(this, CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200);

    CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200.initialize(this, requestId);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, requestId) {
      obj['requestId'] = requestId;
    }
    /**
     * Constructs a <code>CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200} obj Optional instance to populate.
     * @return {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200} The populated <code>CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200();

        if (data.hasOwnProperty('requestId')) {
          obj['requestId'] = _ApiClient["default"].convertToType(data['requestId'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200;
}();
/**
 * Payment request ID
 * @member {String} requestId
 */


CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200.prototype['requestId'] = undefined;
var _default = CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200;
exports["default"] = _default;