"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetQuoteGetQuotesIdResponse200QuoteFilesLinks model module.
 * @module model/GetQuoteGetQuotesIdResponse200QuoteFilesLinks
 * @version 1.61.1
 */
var GetQuoteGetQuotesIdResponse200QuoteFilesLinks = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetQuoteGetQuotesIdResponse200QuoteFilesLinks</code>.
   * Links to related resources for this quote file
   * @alias module:model/GetQuoteGetQuotesIdResponse200QuoteFilesLinks
   * @param self {String} Link to this quote file only
   * @param image {String} Link to the quote file image (usually PDF)
   */
  function GetQuoteGetQuotesIdResponse200QuoteFilesLinks(self, image) {
    _classCallCheck(this, GetQuoteGetQuotesIdResponse200QuoteFilesLinks);

    GetQuoteGetQuotesIdResponse200QuoteFilesLinks.initialize(this, self, image);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetQuoteGetQuotesIdResponse200QuoteFilesLinks, null, [{
    key: "initialize",
    value: function initialize(obj, self, image) {
      obj['self'] = self;
      obj['image'] = image;
    }
    /**
     * Constructs a <code>GetQuoteGetQuotesIdResponse200QuoteFilesLinks</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetQuoteGetQuotesIdResponse200QuoteFilesLinks} obj Optional instance to populate.
     * @return {module:model/GetQuoteGetQuotesIdResponse200QuoteFilesLinks} The populated <code>GetQuoteGetQuotesIdResponse200QuoteFilesLinks</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetQuoteGetQuotesIdResponse200QuoteFilesLinks();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('image')) {
          obj['image'] = _ApiClient["default"].convertToType(data['image'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetQuoteGetQuotesIdResponse200QuoteFilesLinks;
}();
/**
 * Link to this quote file only
 * @member {String} self
 */


GetQuoteGetQuotesIdResponse200QuoteFilesLinks.prototype['self'] = undefined;
/**
 * Link to the quote file image (usually PDF)
 * @member {String} image
 */

GetQuoteGetQuotesIdResponse200QuoteFilesLinks.prototype['image'] = undefined;
var _default = GetQuoteGetQuotesIdResponse200QuoteFilesLinks;
exports["default"] = _default;