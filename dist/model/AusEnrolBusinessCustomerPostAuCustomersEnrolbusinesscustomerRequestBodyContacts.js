"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress = _interopRequireDefault(require("./AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts model module.
 * @module model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts
 * @version 1.61.1
 */
var AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts</code>.
   * @alias module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts
   * @param surname {String} Contact's second name
   * @param billingMethod {String} A way for the contact to receive communications. Can be one of the following: `Both, Paper, eBilling`
   * @param phone1 {String} Contact's primary contact telephone number
   * @param primaryContact {Boolean} A boolean flag indicating whether this contact is the primary for this customer
   */
  function AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts(surname, billingMethod, phone1, primaryContact) {
    _classCallCheck(this, AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts);

    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.initialize(this, surname, billingMethod, phone1, primaryContact);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts, null, [{
    key: "initialize",
    value: function initialize(obj, surname, billingMethod, phone1, primaryContact) {
      obj['surname'] = surname;
      obj['billingMethod'] = billingMethod;
      obj['phone1'] = phone1;
      obj['primaryContact'] = primaryContact;
    }
    /**
     * Constructs a <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts} obj Optional instance to populate.
     * @return {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts} The populated <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts();

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('billingMethod')) {
          obj['billingMethod'] = _ApiClient["default"].convertToType(data['billingMethod'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('phone1')) {
          obj['phone1'] = _ApiClient["default"].convertToType(data['phone1'], 'String');
        }

        if (data.hasOwnProperty('phone2')) {
          obj['phone2'] = _ApiClient["default"].convertToType(data['phone2'], 'String');
        }

        if (data.hasOwnProperty('phone3')) {
          obj['phone3'] = _ApiClient["default"].convertToType(data['phone3'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('primaryContact')) {
          obj['primaryContact'] = _ApiClient["default"].convertToType(data['primaryContact'], 'Boolean');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress["default"].constructFromObject(data['address']);
        }
      }

      return obj;
    }
  }]);

  return AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts;
}();
/**
 * Contact title. Can be `Dr, Miss, Mr, Mrs, Ms, Prof`
 * @member {String} title
 */


AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['title'] = undefined;
/**
 * Contact's first name
 * @member {String} forename
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['forename'] = undefined;
/**
 * Contact's second name
 * @member {String} surname
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['surname'] = undefined;
/**
 * A way for the contact to receive communications. Can be one of the following: `Both, Paper, eBilling`
 * @member {String} billingMethod
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['billingMethod'] = undefined;
/**
 * Contact's email address to which communications will be sent in the case where `billingMethod` is `Both` or `eBilling`
 * @member {String} email
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['email'] = undefined;
/**
 * Contact's primary contact telephone number
 * @member {String} phone1
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['phone1'] = undefined;
/**
 * Contact's second contact telephone number
 * @member {String} phone2
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['phone2'] = undefined;
/**
 * Contact's third contact telephone number
 * @member {String} phone3
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['phone3'] = undefined;
/**
 * Contact's date of birth
 * @member {Date} dateOfBirth
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['dateOfBirth'] = undefined;
/**
 * A boolean flag indicating whether this contact is the primary for this customer
 * @member {Boolean} primaryContact
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['primaryContact'] = undefined;
/**
 * @member {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress} address
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.prototype['address'] = undefined;
var _default = AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts;
exports["default"] = _default;