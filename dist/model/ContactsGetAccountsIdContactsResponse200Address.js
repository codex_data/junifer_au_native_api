"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The ContactsGetAccountsIdContactsResponse200Address model module.
 * @module model/ContactsGetAccountsIdContactsResponse200Address
 * @version 1.61.1
 */
var ContactsGetAccountsIdContactsResponse200Address = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ContactsGetAccountsIdContactsResponse200Address</code>.
   * Contacts&#39;s address
   * @alias module:model/ContactsGetAccountsIdContactsResponse200Address
   */
  function ContactsGetAccountsIdContactsResponse200Address() {
    _classCallCheck(this, ContactsGetAccountsIdContactsResponse200Address);

    ContactsGetAccountsIdContactsResponse200Address.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ContactsGetAccountsIdContactsResponse200Address, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>ContactsGetAccountsIdContactsResponse200Address</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ContactsGetAccountsIdContactsResponse200Address} obj Optional instance to populate.
     * @return {module:model/ContactsGetAccountsIdContactsResponse200Address} The populated <code>ContactsGetAccountsIdContactsResponse200Address</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ContactsGetAccountsIdContactsResponse200Address();

        if (data.hasOwnProperty('careOf')) {
          obj['careOf'] = _ApiClient["default"].convertToType(data['careOf'], 'String');
        }

        if (data.hasOwnProperty('addressType')) {
          obj['addressType'] = _ApiClient["default"].convertToType(data['addressType'], 'String');
        }

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('country')) {
          obj['country'] = _ApiClient["default"].convertToType(data['country'], 'String');
        }

        if (data.hasOwnProperty('countryCode')) {
          obj['countryCode'] = _ApiClient["default"].convertToType(data['countryCode'], 'String');
        }
      }

      return obj;
    }
  }]);

  return ContactsGetAccountsIdContactsResponse200Address;
}();
/**
 * The 'Care of' line of the address
 * @member {String} careOf
 */


ContactsGetAccountsIdContactsResponse200Address.prototype['careOf'] = undefined;
/**
 * Internally used address type which defines number of lines and format used etc.
 * @member {String} addressType
 */

ContactsGetAccountsIdContactsResponse200Address.prototype['addressType'] = undefined;
/**
 * Address 1
 * @member {String} address1
 */

ContactsGetAccountsIdContactsResponse200Address.prototype['address1'] = undefined;
/**
 * Address 2
 * @member {String} address2
 */

ContactsGetAccountsIdContactsResponse200Address.prototype['address2'] = undefined;
/**
 * Address 3
 * @member {String} address3
 */

ContactsGetAccountsIdContactsResponse200Address.prototype['address3'] = undefined;
/**
 * Address 4
 * @member {String} address4
 */

ContactsGetAccountsIdContactsResponse200Address.prototype['address4'] = undefined;
/**
 * Address 5
 * @member {String} address5
 */

ContactsGetAccountsIdContactsResponse200Address.prototype['address5'] = undefined;
/**
 * Post code
 * @member {String} postcode
 */

ContactsGetAccountsIdContactsResponse200Address.prototype['postcode'] = undefined;
/**
 * Country name
 * @member {String} country
 */

ContactsGetAccountsIdContactsResponse200Address.prototype['country'] = undefined;
/**
 * Country code
 * @member {String} countryCode
 */

ContactsGetAccountsIdContactsResponse200Address.prototype['countryCode'] = undefined;
var _default = ContactsGetAccountsIdContactsResponse200Address;
exports["default"] = _default;