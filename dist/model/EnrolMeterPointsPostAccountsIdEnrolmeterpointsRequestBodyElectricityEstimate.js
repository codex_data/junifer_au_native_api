"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate model module.
 * @module model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate
 * @version 1.61.1
 */
var EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate</code>.
   * Estimated electricity spend/consumption values provided by the customer. Can be provided only for gas or dual-fuel enrolments when electricity product is supplied
   * @alias module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate
   * @param period {String} The period for the estimated electricity values. Must be `Month` or `Year`
   */
  function EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate(period) {
    _classCallCheck(this, EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate);

    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate.initialize(this, period);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate, null, [{
    key: "initialize",
    value: function initialize(obj, period) {
      obj['period'] = period;
    }
    /**
     * Constructs a <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate} obj Optional instance to populate.
     * @return {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate} The populated <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate();

        if (data.hasOwnProperty('estimatedCost')) {
          obj['estimatedCost'] = _ApiClient["default"].convertToType(data['estimatedCost'], 'Number');
        }

        if (data.hasOwnProperty('estimatedSaving')) {
          obj['estimatedSaving'] = _ApiClient["default"].convertToType(data['estimatedSaving'], 'Number');
        }

        if (data.hasOwnProperty('consumption')) {
          obj['consumption'] = _ApiClient["default"].convertToType(data['consumption'], 'Number');
        }

        if (data.hasOwnProperty('previousCost')) {
          obj['previousCost'] = _ApiClient["default"].convertToType(data['previousCost'], 'Number');
        }

        if (data.hasOwnProperty('e7DaytimeConsumptionProportion')) {
          obj['e7DaytimeConsumptionProportion'] = _ApiClient["default"].convertToType(data['e7DaytimeConsumptionProportion'], 'Number');
        }

        if (data.hasOwnProperty('period')) {
          obj['period'] = _ApiClient["default"].convertToType(data['period'], 'String');
        }
      }

      return obj;
    }
  }]);

  return EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate;
}();
/**
 * Estimated cost of the new Electricity Tariff
 * @member {Number} estimatedCost
 */


EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate.prototype['estimatedCost'] = undefined;
/**
 * Estimated saving of the new Electricity Tariff
 * @member {Number} estimatedSaving
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate.prototype['estimatedSaving'] = undefined;
/**
 * Customer-provided estimation of their current electricity consumption
 * @member {Number} consumption
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate.prototype['consumption'] = undefined;
/**
 * Customer-provided estimation of their current electricity spend
 * @member {Number} previousCost
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate.prototype['previousCost'] = undefined;
/**
 * The assumed proportion of estimated usage attributable to the 'Day' register of an E7 meter (a number from `0.00` to `1.00`)
 * @member {Number} e7DaytimeConsumptionProportion
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate.prototype['e7DaytimeConsumptionProportion'] = undefined;
/**
 * The period for the estimated electricity values. Must be `Month` or `Year`
 * @member {String} period
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate.prototype['period'] = undefined;
var _default = EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate;
exports["default"] = _default;