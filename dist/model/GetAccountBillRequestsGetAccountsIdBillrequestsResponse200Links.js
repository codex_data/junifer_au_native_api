"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links model module.
 * @module model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links
 * @version 1.61.1
 */
var GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links</code>.
   * Links to related sources
   * @alias module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links
   * @param self {String} Link referring to this bill request
   */
  function GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links(self) {
    _classCallCheck(this, GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links);

    GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links.initialize(this, self);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self) {
      obj['self'] = self;
    }
    /**
     * Constructs a <code>GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links} The populated <code>GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('bill')) {
          obj['bill'] = _ApiClient["default"].convertToType(data['bill'], 'String');
        }

        if (data.hasOwnProperty('account')) {
          obj['account'] = _ApiClient["default"].convertToType(data['account'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links;
}();
/**
 * Link referring to this bill request
 * @member {String} self
 */


GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links.prototype['self'] = undefined;
/**
 * Link referring to this bill request's bill, if exists
 * @member {String} bill
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links.prototype['bill'] = undefined;
/**
 * Link referring to this bill request's account
 * @member {String} account
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links.prototype['account'] = undefined;
var _default = GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links;
exports["default"] = _default;