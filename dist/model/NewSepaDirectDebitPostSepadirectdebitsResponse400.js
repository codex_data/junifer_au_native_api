"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The NewSepaDirectDebitPostSepadirectdebitsResponse400 model module.
 * @module model/NewSepaDirectDebitPostSepadirectdebitsResponse400
 * @version 1.61.1
 */
var NewSepaDirectDebitPostSepadirectdebitsResponse400 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>NewSepaDirectDebitPostSepadirectdebitsResponse400</code>.
   * @alias module:model/NewSepaDirectDebitPostSepadirectdebitsResponse400
   * @param errorCode {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse400.ErrorCodeEnum} Field: * `IbanNotValid` - \"IBAN is invalid: \" + errorMessage * `AccountNotFound` - \"Account with such 'account.id' does not exist\" * `RequiredParameterMissing` - \"The required 'account.id' field is missing\"
   * @param errorSeverity {String} The error severity
   */
  function NewSepaDirectDebitPostSepadirectdebitsResponse400(errorCode, errorSeverity) {
    _classCallCheck(this, NewSepaDirectDebitPostSepadirectdebitsResponse400);

    NewSepaDirectDebitPostSepadirectdebitsResponse400.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(NewSepaDirectDebitPostSepadirectdebitsResponse400, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>NewSepaDirectDebitPostSepadirectdebitsResponse400</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse400} obj Optional instance to populate.
     * @return {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse400} The populated <code>NewSepaDirectDebitPostSepadirectdebitsResponse400</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new NewSepaDirectDebitPostSepadirectdebitsResponse400();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return NewSepaDirectDebitPostSepadirectdebitsResponse400;
}();
/**
 * Field: * `IbanNotValid` - \"IBAN is invalid: \" + errorMessage * `AccountNotFound` - \"Account with such 'account.id' does not exist\" * `RequiredParameterMissing` - \"The required 'account.id' field is missing\"
 * @member {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse400.ErrorCodeEnum} errorCode
 */


NewSepaDirectDebitPostSepadirectdebitsResponse400.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

NewSepaDirectDebitPostSepadirectdebitsResponse400.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

NewSepaDirectDebitPostSepadirectdebitsResponse400.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

NewSepaDirectDebitPostSepadirectdebitsResponse400['ErrorCodeEnum'] = {
  /**
   * value: "IbanNotValid"
   * @const
   */
  "IbanNotValid": "IbanNotValid",

  /**
   * value: "AccountNotFound"
   * @const
   */
  "AccountNotFound": "AccountNotFound",

  /**
   * value: "RequiredParameterMissing"
   * @const
   */
  "RequiredParameterMissing": "RequiredParameterMissing"
};
var _default = NewSepaDirectDebitPostSepadirectdebitsResponse400;
exports["default"] = _default;