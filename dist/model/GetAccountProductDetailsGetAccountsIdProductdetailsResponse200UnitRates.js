"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates model module.
 * @module model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates
 * @version 1.61.1
 */
var GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates</code>.
   * @alias module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates
   * @param rate {Number} Rate. Can be one of the following: `Standard, Day, Night`
   */
  function GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates(rate) {
    _classCallCheck(this, GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates);

    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates.initialize(this, rate);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates, null, [{
    key: "initialize",
    value: function initialize(obj, rate) {
      obj['Rate'] = rate;
    }
    /**
     * Constructs a <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates} obj Optional instance to populate.
     * @return {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates} The populated <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates();

        if (data.hasOwnProperty('Rate')) {
          obj['Rate'] = _ApiClient["default"].convertToType(data['Rate'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates;
}();
/**
 * Rate. Can be one of the following: `Standard, Day, Night`
 * @member {Number} Rate
 */


GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates.prototype['Rate'] = undefined;
var _default = GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates;
exports["default"] = _default;