"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files = _interopRequireDefault(require("./GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files"));

var _GetCommunicationsGetCommunicationsIdResponse200Links = _interopRequireDefault(require("./GetCommunicationsGetCommunicationsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCommunicationsGetCommunicationsIdResponse200 model module.
 * @module model/GetCommunicationsGetCommunicationsIdResponse200
 * @version 1.61.1
 */
var GetCommunicationsGetCommunicationsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCommunicationsGetCommunicationsIdResponse200</code>.
   * @alias module:model/GetCommunicationsGetCommunicationsIdResponse200
   * @param id {Number} Communication id
   * @param type {String} Communication type
   * @param status {String} the status of the communication
   * @param createdDttm {Date} Date and time that the communication was created
   * @param files {Array.<module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files>} the files associated with this communication
   * @param links {module:model/GetCommunicationsGetCommunicationsIdResponse200Links} 
   */
  function GetCommunicationsGetCommunicationsIdResponse200(id, type, status, createdDttm, files, links) {
    _classCallCheck(this, GetCommunicationsGetCommunicationsIdResponse200);

    GetCommunicationsGetCommunicationsIdResponse200.initialize(this, id, type, status, createdDttm, files, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCommunicationsGetCommunicationsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, type, status, createdDttm, files, links) {
      obj['id'] = id;
      obj['type'] = type;
      obj['status'] = status;
      obj['createdDttm'] = createdDttm;
      obj['files'] = files;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetCommunicationsGetCommunicationsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCommunicationsGetCommunicationsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetCommunicationsGetCommunicationsIdResponse200} The populated <code>GetCommunicationsGetCommunicationsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCommunicationsGetCommunicationsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('files')) {
          obj['files'] = _ApiClient["default"].convertToType(data['files'], [_GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files["default"]]);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetCommunicationsGetCommunicationsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetCommunicationsGetCommunicationsIdResponse200;
}();
/**
 * Communication id
 * @member {Number} id
 */


GetCommunicationsGetCommunicationsIdResponse200.prototype['id'] = undefined;
/**
 * Communication type
 * @member {String} type
 */

GetCommunicationsGetCommunicationsIdResponse200.prototype['type'] = undefined;
/**
 * the status of the communication
 * @member {String} status
 */

GetCommunicationsGetCommunicationsIdResponse200.prototype['status'] = undefined;
/**
 * Date and time that the communication was created
 * @member {Date} createdDttm
 */

GetCommunicationsGetCommunicationsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * the files associated with this communication
 * @member {Array.<module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files>} files
 */

GetCommunicationsGetCommunicationsIdResponse200.prototype['files'] = undefined;
/**
 * @member {module:model/GetCommunicationsGetCommunicationsIdResponse200Links} links
 */

GetCommunicationsGetCommunicationsIdResponse200.prototype['links'] = undefined;
var _default = GetCommunicationsGetCommunicationsIdResponse200;
exports["default"] = _default;