"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200 model module.
 * @module model/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200
 * @version 1.61.1
 */
var GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200</code>.
   * @alias module:model/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200
   * @param id {Number} ID of credit Breakdown Line
   * @param description {String} Description of the credit Breakdown Line
   * @param currency {String} Currency ISO code
   * @param billCurrencyAmount {Number} credit Breakdown amount
   * @param salesTaxRate {Number} Sales Tax rate for the credit Breakdown
   * @param linkage {String} Identifier for linked Asset or Property's address
   */
  function GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200(id, description, currency, billCurrencyAmount, salesTaxRate, linkage) {
    _classCallCheck(this, GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200);

    GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200.initialize(this, id, description, currency, billCurrencyAmount, salesTaxRate, linkage);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, description, currency, billCurrencyAmount, salesTaxRate, linkage) {
      obj['id'] = id;
      obj['description'] = description;
      obj['currency'] = currency;
      obj['billCurrencyAmount'] = billCurrencyAmount;
      obj['salesTaxRate'] = salesTaxRate;
      obj['linkage'] = linkage;
    }
    /**
     * Constructs a <code>GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200} obj Optional instance to populate.
     * @return {module:model/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200} The populated <code>GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('billCurrencyAmount')) {
          obj['billCurrencyAmount'] = _ApiClient["default"].convertToType(data['billCurrencyAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTaxRate')) {
          obj['salesTaxRate'] = _ApiClient["default"].convertToType(data['salesTaxRate'], 'Number');
        }

        if (data.hasOwnProperty('linkage')) {
          obj['linkage'] = _ApiClient["default"].convertToType(data['linkage'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200;
}();
/**
 * ID of credit Breakdown Line
 * @member {Number} id
 */


GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200.prototype['id'] = undefined;
/**
 * Description of the credit Breakdown Line
 * @member {String} description
 */

GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200.prototype['description'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200.prototype['currency'] = undefined;
/**
 * credit Breakdown amount
 * @member {Number} billCurrencyAmount
 */

GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200.prototype['billCurrencyAmount'] = undefined;
/**
 * Sales Tax rate for the credit Breakdown
 * @member {Number} salesTaxRate
 */

GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200.prototype['salesTaxRate'] = undefined;
/**
 * Identifier for linked Asset or Property's address
 * @member {String} linkage
 */

GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200.prototype['linkage'] = undefined;
var _default = GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200;
exports["default"] = _default;