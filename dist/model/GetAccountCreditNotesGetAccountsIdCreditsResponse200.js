"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountCreditNotesGetAccountsIdCreditsResponse200Links = _interopRequireDefault(require("./GetAccountCreditNotesGetAccountsIdCreditsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountCreditNotesGetAccountsIdCreditsResponse200 model module.
 * @module model/GetAccountCreditNotesGetAccountsIdCreditsResponse200
 * @version 1.61.1
 */
var GetAccountCreditNotesGetAccountsIdCreditsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountCreditNotesGetAccountsIdCreditsResponse200</code>.
   * @alias module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200
   * @param id {Number} The id of credit
   * @param number {String} The unique credit number
   * @param createdDttm {Date} Date and time when the credit was created within Junifer
   * @param status {String} Status of the credit
   * @param currency {String} Currency of the credit
   * @param grossAmount {Number} Gross amount shown on the credit
   * @param netAmount {Number} Net amount shown on the credit
   * @param salesTaxAmount {Number} Sales tax amount shown on the credit
   * @param issueDt {Date} Date the credit was issued o
   * @param links {module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200Links} 
   */
  function GetAccountCreditNotesGetAccountsIdCreditsResponse200(id, number, createdDttm, status, currency, grossAmount, netAmount, salesTaxAmount, issueDt, links) {
    _classCallCheck(this, GetAccountCreditNotesGetAccountsIdCreditsResponse200);

    GetAccountCreditNotesGetAccountsIdCreditsResponse200.initialize(this, id, number, createdDttm, status, currency, grossAmount, netAmount, salesTaxAmount, issueDt, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountCreditNotesGetAccountsIdCreditsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, number, createdDttm, status, currency, grossAmount, netAmount, salesTaxAmount, issueDt, links) {
      obj['id'] = id;
      obj['number'] = number;
      obj['createdDttm'] = createdDttm;
      obj['status'] = status;
      obj['currency'] = currency;
      obj['grossAmount'] = grossAmount;
      obj['netAmount'] = netAmount;
      obj['salesTaxAmount'] = salesTaxAmount;
      obj['issueDt'] = issueDt;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountCreditNotesGetAccountsIdCreditsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200} The populated <code>GetAccountCreditNotesGetAccountsIdCreditsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountCreditNotesGetAccountsIdCreditsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('authorisedDttm')) {
          obj['authorisedDttm'] = _ApiClient["default"].convertToType(data['authorisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('grossAmount')) {
          obj['grossAmount'] = _ApiClient["default"].convertToType(data['grossAmount'], 'Number');
        }

        if (data.hasOwnProperty('netAmount')) {
          obj['netAmount'] = _ApiClient["default"].convertToType(data['netAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTaxAmount')) {
          obj['salesTaxAmount'] = _ApiClient["default"].convertToType(data['salesTaxAmount'], 'Number');
        }

        if (data.hasOwnProperty('issueDt')) {
          obj['issueDt'] = _ApiClient["default"].convertToType(data['issueDt'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountCreditNotesGetAccountsIdCreditsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountCreditNotesGetAccountsIdCreditsResponse200;
}();
/**
 * The id of credit
 * @member {Number} id
 */


GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['id'] = undefined;
/**
 * The unique credit number
 * @member {String} number
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['number'] = undefined;
/**
 * Date and time when the credit was created within Junifer
 * @member {Date} createdDttm
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['createdDttm'] = undefined;
/**
 * Status of the credit
 * @member {String} status
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['status'] = undefined;
/**
 * Datetime the credit was authorised
 * @member {Date} authorisedDttm
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['authorisedDttm'] = undefined;
/**
 * Currency of the credit
 * @member {String} currency
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['currency'] = undefined;
/**
 * Gross amount shown on the credit
 * @member {Number} grossAmount
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['grossAmount'] = undefined;
/**
 * Net amount shown on the credit
 * @member {Number} netAmount
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['netAmount'] = undefined;
/**
 * Sales tax amount shown on the credit
 * @member {Number} salesTaxAmount
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['salesTaxAmount'] = undefined;
/**
 * Date the credit was issued o
 * @member {Date} issueDt
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['issueDt'] = undefined;
/**
 * @member {module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200Links} links
 */

GetAccountCreditNotesGetAccountsIdCreditsResponse200.prototype['links'] = undefined;
var _default = GetAccountCreditNotesGetAccountsIdCreditsResponse200;
exports["default"] = _default;