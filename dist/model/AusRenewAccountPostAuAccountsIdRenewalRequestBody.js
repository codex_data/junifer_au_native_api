"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements = _interopRequireDefault(require("./AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusRenewAccountPostAuAccountsIdRenewalRequestBody model module.
 * @module model/AusRenewAccountPostAuAccountsIdRenewalRequestBody
 * @version 1.61.1
 */
var AusRenewAccountPostAuAccountsIdRenewalRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusRenewAccountPostAuAccountsIdRenewalRequestBody</code>.
   * @alias module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBody
   * @param agreements {Array.<module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements>} Array of agreements to create
   */
  function AusRenewAccountPostAuAccountsIdRenewalRequestBody(agreements) {
    _classCallCheck(this, AusRenewAccountPostAuAccountsIdRenewalRequestBody);

    AusRenewAccountPostAuAccountsIdRenewalRequestBody.initialize(this, agreements);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusRenewAccountPostAuAccountsIdRenewalRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, agreements) {
      obj['agreements'] = agreements;
    }
    /**
     * Constructs a <code>AusRenewAccountPostAuAccountsIdRenewalRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBody} obj Optional instance to populate.
     * @return {module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBody} The populated <code>AusRenewAccountPostAuAccountsIdRenewalRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusRenewAccountPostAuAccountsIdRenewalRequestBody();

        if (data.hasOwnProperty('agreements')) {
          obj['agreements'] = _ApiClient["default"].convertToType(data['agreements'], [_AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements["default"]]);
        }
      }

      return obj;
    }
  }]);

  return AusRenewAccountPostAuAccountsIdRenewalRequestBody;
}();
/**
 * Array of agreements to create
 * @member {Array.<module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements>} agreements
 */


AusRenewAccountPostAuAccountsIdRenewalRequestBody.prototype['agreements'] = undefined;
var _default = AusRenewAccountPostAuAccountsIdRenewalRequestBody;
exports["default"] = _default;