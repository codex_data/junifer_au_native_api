"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400 model module.
 * @module model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400
 * @version 1.61.1
 */
var StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400</code>.
   * @alias module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400
   * @param errorCode {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400.ErrorCodeEnum} Field: * `InvalidToDate` - toDt cannot be before 'fromDt'. Set it to 'fromDt' to delete the payment schedule period
   * @param errorSeverity {String} The error severity
   */
  function StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400(errorCode, errorSeverity) {
    _classCallCheck(this, StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400);

    StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400} obj Optional instance to populate.
     * @return {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400} The populated <code>StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400;
}();
/**
 * Field: * `InvalidToDate` - toDt cannot be before 'fromDt'. Set it to 'fromDt' to delete the payment schedule period
 * @member {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400.ErrorCodeEnum} errorCode
 */


StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400['ErrorCodeEnum'] = {
  /**
   * value: "InvalidToDate"
   * @const
   */
  "InvalidToDate": "InvalidToDate"
};
var _default = StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400;
exports["default"] = _default;