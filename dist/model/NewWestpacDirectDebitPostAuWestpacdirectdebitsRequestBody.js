"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody model module.
 * @module model/NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody
 * @version 1.61.1
 */
var NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody</code>.
   * @alias module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody
   * @param paymentType {String} Payment type. Can be either `BankAccount` or `CreditCard`
   */
  function NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody(paymentType) {
    _classCallCheck(this, NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody);

    NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.initialize(this, paymentType);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, paymentType) {
      obj['paymentType'] = paymentType;
    }
    /**
     * Constructs a <code>NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody} obj Optional instance to populate.
     * @return {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody} The populated <code>NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody();

        if (data.hasOwnProperty('mandateReference')) {
          obj['mandateReference'] = _ApiClient["default"].convertToType(data['mandateReference'], 'String');
        }

        if (data.hasOwnProperty('bankAccountNumber')) {
          obj['bankAccountNumber'] = _ApiClient["default"].convertToType(data['bankAccountNumber'], 'String');
        }

        if (data.hasOwnProperty('bsbNumber')) {
          obj['bsbNumber'] = _ApiClient["default"].convertToType(data['bsbNumber'], 'String');
        }

        if (data.hasOwnProperty('customerNumber')) {
          obj['customerNumber'] = _ApiClient["default"].convertToType(data['customerNumber'], 'String');
        }

        if (data.hasOwnProperty('accountName')) {
          obj['accountName'] = _ApiClient["default"].convertToType(data['accountName'], 'String');
        }

        if (data.hasOwnProperty('paymentType')) {
          obj['paymentType'] = _ApiClient["default"].convertToType(data['paymentType'], 'String');
        }

        if (data.hasOwnProperty('cardType')) {
          obj['cardType'] = _ApiClient["default"].convertToType(data['cardType'], 'String');
        }
      }

      return obj;
    }
  }]);

  return NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody;
}();
/**
 * Westpac Direct Debit mandate reference
 * @member {String} mandateReference
 */


NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.prototype['mandateReference'] = undefined;
/**
 * Number of the bank account used to create the direct debit
 * @member {String} bankAccountNumber
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.prototype['bankAccountNumber'] = undefined;
/**
 * BSB number associated with the bank account used to create the direct debit
 * @member {String} bsbNumber
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.prototype['bsbNumber'] = undefined;
/**
 * Customer's unique ID
 * @member {String} customerNumber
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.prototype['customerNumber'] = undefined;
/**
 * Customer's account name registered with Westpac
 * @member {String} accountName
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.prototype['accountName'] = undefined;
/**
 * Payment type. Can be either `BankAccount` or `CreditCard`
 * @member {String} paymentType
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.prototype['paymentType'] = undefined;
/**
 * Credit card type. Must be provided if the payment type is CreditCard. Acceptable values are `Visa,Mastercard,Amex,Diners,JBC,UnionPay`
 * @member {String} cardType
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.prototype['cardType'] = undefined;
var _default = NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody;
exports["default"] = _default;