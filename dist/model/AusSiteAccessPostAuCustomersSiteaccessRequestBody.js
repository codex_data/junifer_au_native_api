"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusSiteAccessPostAuCustomersSiteaccessRequestBody model module.
 * @module model/AusSiteAccessPostAuCustomersSiteaccessRequestBody
 * @version 1.61.1
 */
var AusSiteAccessPostAuCustomersSiteaccessRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusSiteAccessPostAuCustomersSiteaccessRequestBody</code>.
   * @alias module:model/AusSiteAccessPostAuCustomersSiteaccessRequestBody
   * @param nmi {String} Identifier of the NMI including checksum
   */
  function AusSiteAccessPostAuCustomersSiteaccessRequestBody(nmi) {
    _classCallCheck(this, AusSiteAccessPostAuCustomersSiteaccessRequestBody);

    AusSiteAccessPostAuCustomersSiteaccessRequestBody.initialize(this, nmi);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusSiteAccessPostAuCustomersSiteaccessRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, nmi) {
      obj['nmi'] = nmi;
    }
    /**
     * Constructs a <code>AusSiteAccessPostAuCustomersSiteaccessRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusSiteAccessPostAuCustomersSiteaccessRequestBody} obj Optional instance to populate.
     * @return {module:model/AusSiteAccessPostAuCustomersSiteaccessRequestBody} The populated <code>AusSiteAccessPostAuCustomersSiteaccessRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusSiteAccessPostAuCustomersSiteaccessRequestBody();

        if (data.hasOwnProperty('nmi')) {
          obj['nmi'] = _ApiClient["default"].convertToType(data['nmi'], 'String');
        }

        if (data.hasOwnProperty('accessDetails')) {
          obj['accessDetails'] = _ApiClient["default"].convertToType(data['accessDetails'], 'String');
        }

        if (data.hasOwnProperty('hazardDescription')) {
          obj['hazardDescription'] = _ApiClient["default"].convertToType(data['hazardDescription'], 'String');
        }
      }

      return obj;
    }
  }]);

  return AusSiteAccessPostAuCustomersSiteaccessRequestBody;
}();
/**
 * Identifier of the NMI including checksum
 * @member {String} nmi
 */


AusSiteAccessPostAuCustomersSiteaccessRequestBody.prototype['nmi'] = undefined;
/**
 * Text describing access to the site
 * @member {String} accessDetails
 */

AusSiteAccessPostAuCustomersSiteaccessRequestBody.prototype['accessDetails'] = undefined;
/**
 * Text identifying hazards associated with reading the meter
 * @member {String} hazardDescription
 */

AusSiteAccessPostAuCustomersSiteaccessRequestBody.prototype['hazardDescription'] = undefined;
var _default = AusSiteAccessPostAuCustomersSiteaccessRequestBody;
exports["default"] = _default;