"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetProspectPropertiesGetProspectsIdPropertysResponse200Links = _interopRequireDefault(require("./GetProspectPropertiesGetProspectsIdPropertysResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetProspectPropertiesGetProspectsIdPropertysResponse200 model module.
 * @module model/GetProspectPropertiesGetProspectsIdPropertysResponse200
 * @version 1.61.1
 */
var GetProspectPropertiesGetProspectsIdPropertysResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetProspectPropertiesGetProspectsIdPropertysResponse200</code>.
   * @alias module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200
   * @param id {Number} Property ID
   * @param propertyType {String} Property type
   * @param addressType {String} Address type
   * @param address {Object} Address of the property
   * @param links {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200Links} 
   */
  function GetProspectPropertiesGetProspectsIdPropertysResponse200(id, propertyType, addressType, address, links) {
    _classCallCheck(this, GetProspectPropertiesGetProspectsIdPropertysResponse200);

    GetProspectPropertiesGetProspectsIdPropertysResponse200.initialize(this, id, propertyType, addressType, address, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetProspectPropertiesGetProspectsIdPropertysResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, propertyType, addressType, address, links) {
      obj['id'] = id;
      obj['propertyType'] = propertyType;
      obj['addressType'] = addressType;
      obj['address'] = address;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetProspectPropertiesGetProspectsIdPropertysResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200} obj Optional instance to populate.
     * @return {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200} The populated <code>GetProspectPropertiesGetProspectsIdPropertysResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetProspectPropertiesGetProspectsIdPropertysResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('propertyType')) {
          obj['propertyType'] = _ApiClient["default"].convertToType(data['propertyType'], 'String');
        }

        if (data.hasOwnProperty('addressType')) {
          obj['addressType'] = _ApiClient["default"].convertToType(data['addressType'], 'String');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _ApiClient["default"].convertToType(data['address'], Object);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetProspectPropertiesGetProspectsIdPropertysResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetProspectPropertiesGetProspectsIdPropertysResponse200;
}();
/**
 * Property ID
 * @member {Number} id
 */


GetProspectPropertiesGetProspectsIdPropertysResponse200.prototype['id'] = undefined;
/**
 * Property type
 * @member {String} propertyType
 */

GetProspectPropertiesGetProspectsIdPropertysResponse200.prototype['propertyType'] = undefined;
/**
 * Address type
 * @member {String} addressType
 */

GetProspectPropertiesGetProspectsIdPropertysResponse200.prototype['addressType'] = undefined;
/**
 * Address of the property
 * @member {Object} address
 */

GetProspectPropertiesGetProspectsIdPropertysResponse200.prototype['address'] = undefined;
/**
 * @member {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200Links} links
 */

GetProspectPropertiesGetProspectsIdPropertysResponse200.prototype['links'] = undefined;
var _default = GetProspectPropertiesGetProspectsIdPropertysResponse200;
exports["default"] = _default;