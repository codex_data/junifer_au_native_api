"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _TariffInformationGetAccountsIdTariffinformationResponse200TIL = _interopRequireDefault(require("./TariffInformationGetAccountsIdTariffinformationResponse200TIL"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The TariffInformationGetAccountsIdTariffinformationResponse200 model module.
 * @module model/TariffInformationGetAccountsIdTariffinformationResponse200
 * @version 1.61.1
 */
var TariffInformationGetAccountsIdTariffinformationResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>TariffInformationGetAccountsIdTariffinformationResponse200</code>.
   * @alias module:model/TariffInformationGetAccountsIdTariffinformationResponse200
   */
  function TariffInformationGetAccountsIdTariffinformationResponse200() {
    _classCallCheck(this, TariffInformationGetAccountsIdTariffinformationResponse200);

    TariffInformationGetAccountsIdTariffinformationResponse200.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(TariffInformationGetAccountsIdTariffinformationResponse200, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>TariffInformationGetAccountsIdTariffinformationResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/TariffInformationGetAccountsIdTariffinformationResponse200} obj Optional instance to populate.
     * @return {module:model/TariffInformationGetAccountsIdTariffinformationResponse200} The populated <code>TariffInformationGetAccountsIdTariffinformationResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new TariffInformationGetAccountsIdTariffinformationResponse200();

        if (data.hasOwnProperty('productType')) {
          obj['productType'] = _ApiClient["default"].convertToType(data['productType'], [Object]);
        }

        if (data.hasOwnProperty('productCode')) {
          obj['productCode'] = _ApiClient["default"].convertToType(data['productCode'], 'String');
        }

        if (data.hasOwnProperty('rateCount')) {
          obj['rateCount'] = _ApiClient["default"].convertToType(data['rateCount'], 'String');
        }

        if (data.hasOwnProperty('productGroup')) {
          obj['productGroup'] = _ApiClient["default"].convertToType(data['productGroup'], 'String');
        }

        if (data.hasOwnProperty('agreementType')) {
          obj['agreementType'] = _ApiClient["default"].convertToType(data['agreementType'], 'String');
        }

        if (data.hasOwnProperty('agreementMonths')) {
          obj['agreementMonths'] = _ApiClient["default"].convertToType(data['agreementMonths'], 'String');
        }

        if (data.hasOwnProperty('paymentMethod')) {
          obj['paymentMethod'] = _ApiClient["default"].convertToType(data['paymentMethod'], 'String');
        }

        if (data.hasOwnProperty('accountManagementType')) {
          obj['accountManagementType'] = _ApiClient["default"].convertToType(data['accountManagementType'], 'String');
        }

        if (data.hasOwnProperty('gspGroupCode')) {
          obj['gspGroupCode'] = _ApiClient["default"].convertToType(data['gspGroupCode'], 'String');
        }

        if (data.hasOwnProperty('TIL')) {
          obj['TIL'] = _ApiClient["default"].convertToType(data['TIL'], [_TariffInformationGetAccountsIdTariffinformationResponse200TIL["default"]]);
        }
      }

      return obj;
    }
  }]);

  return TariffInformationGetAccountsIdTariffinformationResponse200;
}();
/**
 * The product type. Can be \"Gas\" or \"Electricity\"
 * @member {Array.<Object>} productType
 */


TariffInformationGetAccountsIdTariffinformationResponse200.prototype['productType'] = undefined;
/**
 * The product code.
 * @member {String} productCode
 */

TariffInformationGetAccountsIdTariffinformationResponse200.prototype['productCode'] = undefined;
/**
 * The number of rates.
 * @member {String} rateCount
 */

TariffInformationGetAccountsIdTariffinformationResponse200.prototype['rateCount'] = undefined;
/**
 * The product's group.
 * @member {String} productGroup
 */

TariffInformationGetAccountsIdTariffinformationResponse200.prototype['productGroup'] = undefined;
/**
 * The agreement's type.
 * @member {String} agreementType
 */

TariffInformationGetAccountsIdTariffinformationResponse200.prototype['agreementType'] = undefined;
/**
 * The number of months for the agreement.
 * @member {String} agreementMonths
 */

TariffInformationGetAccountsIdTariffinformationResponse200.prototype['agreementMonths'] = undefined;
/**
 * The payment method eg DD.
 * @member {String} paymentMethod
 */

TariffInformationGetAccountsIdTariffinformationResponse200.prototype['paymentMethod'] = undefined;
/**
 * The way the account is managed eg \"Online\"
 * @member {String} accountManagementType
 */

TariffInformationGetAccountsIdTariffinformationResponse200.prototype['accountManagementType'] = undefined;
/**
 * The GSP group for the product.
 * @member {String} gspGroupCode
 */

TariffInformationGetAccountsIdTariffinformationResponse200.prototype['gspGroupCode'] = undefined;
/**
 * The tarif information labels.
 * @member {Array.<module:model/TariffInformationGetAccountsIdTariffinformationResponse200TIL>} TIL
 */

TariffInformationGetAccountsIdTariffinformationResponse200.prototype['TIL'] = undefined;
var _default = TariffInformationGetAccountsIdTariffinformationResponse200;
exports["default"] = _default;