"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress model module.
 * @module model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress
 * @version 1.61.1
 */
var EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress</code>.
   * Supply address.
   * @alias module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress
   * @param careOf {String} The 'Care of' line for the address
   * @param address1 {String} First address line
   * @param postcode {String} Post code
   * @param countryCode {String} An ISO country code. Currently only GB is supported thus is the the only acceptable value!(Request Parameter)
   */
  function EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress(careOf, address1, postcode, countryCode) {
    _classCallCheck(this, EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress);

    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.initialize(this, careOf, address1, postcode, countryCode);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress, null, [{
    key: "initialize",
    value: function initialize(obj, careOf, address1, postcode, countryCode) {
      obj['careOf'] = careOf;
      obj['address1'] = address1;
      obj['postcode'] = postcode;
      obj['countryCode'] = countryCode;
    }
    /**
     * Constructs a <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress} obj Optional instance to populate.
     * @return {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress} The populated <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress();

        if (data.hasOwnProperty('careOf')) {
          obj['careOf'] = _ApiClient["default"].convertToType(data['careOf'], 'String');
        }

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('town')) {
          obj['town'] = _ApiClient["default"].convertToType(data['town'], 'String');
        }

        if (data.hasOwnProperty('county')) {
          obj['county'] = _ApiClient["default"].convertToType(data['county'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('countryCode')) {
          obj['countryCode'] = _ApiClient["default"].convertToType(data['countryCode'], 'String');
        }
      }

      return obj;
    }
  }]);

  return EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress;
}();
/**
 * The 'Care of' line for the address
 * @member {String} careOf
 */


EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.prototype['careOf'] = undefined;
/**
 * First address line
 * @member {String} address1
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.prototype['address1'] = undefined;
/**
 * Second address line
 * @member {String} address2
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.prototype['address2'] = undefined;
/**
 * Third address line
 * @member {String} address3
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.prototype['address3'] = undefined;
/**
 * Town
 * @member {String} town
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.prototype['town'] = undefined;
/**
 * County
 * @member {String} county
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.prototype['county'] = undefined;
/**
 * Post code
 * @member {String} postcode
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.prototype['postcode'] = undefined;
/**
 * An ISO country code. Currently only GB is supported thus is the the only acceptable value!(Request Parameter)
 * @member {String} countryCode
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.prototype['countryCode'] = undefined;
var _default = EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress;
exports["default"] = _default;