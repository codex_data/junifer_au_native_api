"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _UpdateCustomerPutCustomersIdRequestBodyPrimaryContact = _interopRequireDefault(require("./UpdateCustomerPutCustomersIdRequestBodyPrimaryContact"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateCustomerPutCustomersIdRequestBody model module.
 * @module model/UpdateCustomerPutCustomersIdRequestBody
 * @version 1.61.1
 */
var UpdateCustomerPutCustomersIdRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateCustomerPutCustomersIdRequestBody</code>.
   * @alias module:model/UpdateCustomerPutCustomersIdRequestBody
   * @param id {Number} Customer ID (This field is ignored)
   * @param name {String} Customer's full name (This field is ignored)
   * @param number {String} Customer number (This field is ignored)
   * @param customerClass {String} Customer class this customer belongs to (This field is ignored)
   * @param marketingOptOutFl {Boolean} Customer's choice for receiving marketing communications
   * @param primaryContact {module:model/UpdateCustomerPutCustomersIdRequestBodyPrimaryContact} 
   */
  function UpdateCustomerPutCustomersIdRequestBody(id, name, number, customerClass, marketingOptOutFl, primaryContact) {
    _classCallCheck(this, UpdateCustomerPutCustomersIdRequestBody);

    UpdateCustomerPutCustomersIdRequestBody.initialize(this, id, name, number, customerClass, marketingOptOutFl, primaryContact);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateCustomerPutCustomersIdRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, id, name, number, customerClass, marketingOptOutFl, primaryContact) {
      obj['id'] = id;
      obj['name'] = name;
      obj['number'] = number;
      obj['customerClass'] = customerClass;
      obj['marketingOptOutFl'] = marketingOptOutFl;
      obj['primaryContact'] = primaryContact;
    }
    /**
     * Constructs a <code>UpdateCustomerPutCustomersIdRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateCustomerPutCustomersIdRequestBody} obj Optional instance to populate.
     * @return {module:model/UpdateCustomerPutCustomersIdRequestBody} The populated <code>UpdateCustomerPutCustomersIdRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateCustomerPutCustomersIdRequestBody();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('customerClass')) {
          obj['customerClass'] = _ApiClient["default"].convertToType(data['customerClass'], 'String');
        }

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('marketingOptOutFl')) {
          obj['marketingOptOutFl'] = _ApiClient["default"].convertToType(data['marketingOptOutFl'], 'Boolean');
        }

        if (data.hasOwnProperty('companyName')) {
          obj['companyName'] = _ApiClient["default"].convertToType(data['companyName'], 'String');
        }

        if (data.hasOwnProperty('companyNumber')) {
          obj['companyNumber'] = _ApiClient["default"].convertToType(data['companyNumber'], 'String');
        }

        if (data.hasOwnProperty('primaryContact')) {
          obj['primaryContact'] = _UpdateCustomerPutCustomersIdRequestBodyPrimaryContact["default"].constructFromObject(data['primaryContact']);
        }
      }

      return obj;
    }
  }]);

  return UpdateCustomerPutCustomersIdRequestBody;
}();
/**
 * Customer ID (This field is ignored)
 * @member {Number} id
 */


UpdateCustomerPutCustomersIdRequestBody.prototype['id'] = undefined;
/**
 * Customer's full name (This field is ignored)
 * @member {String} name
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['name'] = undefined;
/**
 * Customer number (This field is ignored)
 * @member {String} number
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['number'] = undefined;
/**
 * Customer class this customer belongs to (This field is ignored)
 * @member {String} customerClass
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['customerClass'] = undefined;
/**
 * Customer's title
 * @member {String} title
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['title'] = undefined;
/**
 * Customer's first name
 * @member {String} forename
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['forename'] = undefined;
/**
 * Customer's last name
 * @member {String} surname
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['surname'] = undefined;
/**
 * Customer's choice for receiving marketing communications
 * @member {Boolean} marketingOptOutFl
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['marketingOptOutFl'] = undefined;
/**
 * Customer's company name (This field is ignored)
 * @member {String} companyName
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['companyName'] = undefined;
/**
 * Customer's company number (This field is ignored)
 * @member {String} companyNumber
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['companyNumber'] = undefined;
/**
 * @member {module:model/UpdateCustomerPutCustomersIdRequestBodyPrimaryContact} primaryContact
 */

UpdateCustomerPutCustomersIdRequestBody.prototype['primaryContact'] = undefined;
var _default = UpdateCustomerPutCustomersIdRequestBody;
exports["default"] = _default;