"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The LookUpAccountGetAccountsResponse200Links model module.
 * @module model/LookUpAccountGetAccountsResponse200Links
 * @version 1.61.1
 */
var LookUpAccountGetAccountsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>LookUpAccountGetAccountsResponse200Links</code>.
   * Links to related resources
   * @alias module:model/LookUpAccountGetAccountsResponse200Links
   * @param self {String} Link referring to this account
   * @param customer {String} Link to this account's customer record
   * @param contacts {String} Link to this account's contacts
   * @param bills {String} Link to account's bills
   * @param credits {String} Link to account's credits
   * @param payments {String} Link to account's payments
   * @param paymentMethods {String} Link to account's payment methods
   * @param agreements {String} Link to account's agreements
   * @param paymentSchedulePeriods {String} Link to account's payment schedule periods
   * @param tickets {String} Link to account's tickets
   * @param productDetails {String} Link to account's product details
   * @param propertys {String} Link to account's propertys
   */
  function LookUpAccountGetAccountsResponse200Links(self, customer, contacts, bills, credits, payments, paymentMethods, agreements, paymentSchedulePeriods, tickets, productDetails, propertys) {
    _classCallCheck(this, LookUpAccountGetAccountsResponse200Links);

    LookUpAccountGetAccountsResponse200Links.initialize(this, self, customer, contacts, bills, credits, payments, paymentMethods, agreements, paymentSchedulePeriods, tickets, productDetails, propertys);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(LookUpAccountGetAccountsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, customer, contacts, bills, credits, payments, paymentMethods, agreements, paymentSchedulePeriods, tickets, productDetails, propertys) {
      obj['self'] = self;
      obj['customer'] = customer;
      obj['contacts'] = contacts;
      obj['bills'] = bills;
      obj['credits'] = credits;
      obj['payments'] = payments;
      obj['paymentMethods'] = paymentMethods;
      obj['agreements'] = agreements;
      obj['paymentSchedulePeriods'] = paymentSchedulePeriods;
      obj['tickets'] = tickets;
      obj['productDetails'] = productDetails;
      obj['propertys'] = propertys;
    }
    /**
     * Constructs a <code>LookUpAccountGetAccountsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LookUpAccountGetAccountsResponse200Links} obj Optional instance to populate.
     * @return {module:model/LookUpAccountGetAccountsResponse200Links} The populated <code>LookUpAccountGetAccountsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new LookUpAccountGetAccountsResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('customer')) {
          obj['customer'] = _ApiClient["default"].convertToType(data['customer'], 'String');
        }

        if (data.hasOwnProperty('contacts')) {
          obj['contacts'] = _ApiClient["default"].convertToType(data['contacts'], 'String');
        }

        if (data.hasOwnProperty('bills')) {
          obj['bills'] = _ApiClient["default"].convertToType(data['bills'], 'String');
        }

        if (data.hasOwnProperty('credits')) {
          obj['credits'] = _ApiClient["default"].convertToType(data['credits'], 'String');
        }

        if (data.hasOwnProperty('payments')) {
          obj['payments'] = _ApiClient["default"].convertToType(data['payments'], 'String');
        }

        if (data.hasOwnProperty('paymentMethods')) {
          obj['paymentMethods'] = _ApiClient["default"].convertToType(data['paymentMethods'], 'String');
        }

        if (data.hasOwnProperty('agreements')) {
          obj['agreements'] = _ApiClient["default"].convertToType(data['agreements'], 'String');
        }

        if (data.hasOwnProperty('paymentSchedulePeriods')) {
          obj['paymentSchedulePeriods'] = _ApiClient["default"].convertToType(data['paymentSchedulePeriods'], 'String');
        }

        if (data.hasOwnProperty('tickets')) {
          obj['tickets'] = _ApiClient["default"].convertToType(data['tickets'], 'String');
        }

        if (data.hasOwnProperty('productDetails')) {
          obj['productDetails'] = _ApiClient["default"].convertToType(data['productDetails'], 'String');
        }

        if (data.hasOwnProperty('propertys')) {
          obj['propertys'] = _ApiClient["default"].convertToType(data['propertys'], 'String');
        }
      }

      return obj;
    }
  }]);

  return LookUpAccountGetAccountsResponse200Links;
}();
/**
 * Link referring to this account
 * @member {String} self
 */


LookUpAccountGetAccountsResponse200Links.prototype['self'] = undefined;
/**
 * Link to this account's customer record
 * @member {String} customer
 */

LookUpAccountGetAccountsResponse200Links.prototype['customer'] = undefined;
/**
 * Link to this account's contacts
 * @member {String} contacts
 */

LookUpAccountGetAccountsResponse200Links.prototype['contacts'] = undefined;
/**
 * Link to account's bills
 * @member {String} bills
 */

LookUpAccountGetAccountsResponse200Links.prototype['bills'] = undefined;
/**
 * Link to account's credits
 * @member {String} credits
 */

LookUpAccountGetAccountsResponse200Links.prototype['credits'] = undefined;
/**
 * Link to account's payments
 * @member {String} payments
 */

LookUpAccountGetAccountsResponse200Links.prototype['payments'] = undefined;
/**
 * Link to account's payment methods
 * @member {String} paymentMethods
 */

LookUpAccountGetAccountsResponse200Links.prototype['paymentMethods'] = undefined;
/**
 * Link to account's agreements
 * @member {String} agreements
 */

LookUpAccountGetAccountsResponse200Links.prototype['agreements'] = undefined;
/**
 * Link to account's payment schedule periods
 * @member {String} paymentSchedulePeriods
 */

LookUpAccountGetAccountsResponse200Links.prototype['paymentSchedulePeriods'] = undefined;
/**
 * Link to account's tickets
 * @member {String} tickets
 */

LookUpAccountGetAccountsResponse200Links.prototype['tickets'] = undefined;
/**
 * Link to account's product details
 * @member {String} productDetails
 */

LookUpAccountGetAccountsResponse200Links.prototype['productDetails'] = undefined;
/**
 * Link to account's propertys
 * @member {String} propertys
 */

LookUpAccountGetAccountsResponse200Links.prototype['propertys'] = undefined;
var _default = LookUpAccountGetAccountsResponse200Links;
exports["default"] = _default;