"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCustomerConsentsGetCustomersIdConsentsResponse200 model module.
 * @module model/GetCustomerConsentsGetCustomersIdConsentsResponse200
 * @version 1.61.1
 */
var GetCustomerConsentsGetCustomersIdConsentsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCustomerConsentsGetCustomersIdConsentsResponse200</code>.
   * @alias module:model/GetCustomerConsentsGetCustomersIdConsentsResponse200
   * @param id {Number} Consent ID
   * @param consentDefinition {String} Consent Definition
   * @param setting {Boolean} Consent Setting can be Y or N
   * @param fromDt {Date} Consent from date
   */
  function GetCustomerConsentsGetCustomersIdConsentsResponse200(id, consentDefinition, setting, fromDt) {
    _classCallCheck(this, GetCustomerConsentsGetCustomersIdConsentsResponse200);

    GetCustomerConsentsGetCustomersIdConsentsResponse200.initialize(this, id, consentDefinition, setting, fromDt);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCustomerConsentsGetCustomersIdConsentsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, consentDefinition, setting, fromDt) {
      obj['id'] = id;
      obj['consentDefinition'] = consentDefinition;
      obj['setting'] = setting;
      obj['fromDt'] = fromDt;
    }
    /**
     * Constructs a <code>GetCustomerConsentsGetCustomersIdConsentsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCustomerConsentsGetCustomersIdConsentsResponse200} obj Optional instance to populate.
     * @return {module:model/GetCustomerConsentsGetCustomersIdConsentsResponse200} The populated <code>GetCustomerConsentsGetCustomersIdConsentsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCustomerConsentsGetCustomersIdConsentsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('consentDefinition')) {
          obj['consentDefinition'] = _ApiClient["default"].convertToType(data['consentDefinition'], 'String');
        }

        if (data.hasOwnProperty('setting')) {
          obj['setting'] = _ApiClient["default"].convertToType(data['setting'], 'Boolean');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return GetCustomerConsentsGetCustomersIdConsentsResponse200;
}();
/**
 * Consent ID
 * @member {Number} id
 */


GetCustomerConsentsGetCustomersIdConsentsResponse200.prototype['id'] = undefined;
/**
 * Consent Definition
 * @member {String} consentDefinition
 */

GetCustomerConsentsGetCustomersIdConsentsResponse200.prototype['consentDefinition'] = undefined;
/**
 * Consent Setting can be Y or N
 * @member {Boolean} setting
 */

GetCustomerConsentsGetCustomersIdConsentsResponse200.prototype['setting'] = undefined;
/**
 * Consent from date
 * @member {Date} fromDt
 */

GetCustomerConsentsGetCustomersIdConsentsResponse200.prototype['fromDt'] = undefined;
/**
 * Consent to date
 * @member {Date} toDt
 */

GetCustomerConsentsGetCustomersIdConsentsResponse200.prototype['toDt'] = undefined;
var _default = GetCustomerConsentsGetCustomersIdConsentsResponse200;
exports["default"] = _default;