"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateAccountTicketPostAccountsIdTicketsResponse200Links = _interopRequireDefault(require("./CreateAccountTicketPostAccountsIdTicketsResponse200Links"));

var _CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities = _interopRequireDefault(require("./CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The LookUpTicketsGetTicketsResponse200 model module.
 * @module model/LookUpTicketsGetTicketsResponse200
 * @version 1.61.1
 */
var LookUpTicketsGetTicketsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>LookUpTicketsGetTicketsResponse200</code>.
   * @alias module:model/LookUpTicketsGetTicketsResponse200
   * @param ticketId {Number} Ticket Identifier
   * @param ticketDefinitionCode {String} Ticket definition code
   * @param ticketDefinitionName {String} Ticket definition name
   * @param ticketStepCode {String} Ticket step code
   * @param ticketStepName {String} Ticket step name
   * @param status {String} Ticket status `Open, Closed, Cancelled, Error`
   * @param keyIdentifier {String} Ticket key identifier
   * @param summary {String} Ticket summary
   * @param description {String} Ticket description
   * @param createdDttm {Date} Ticket creation date and time
   * @param assignedUserTeam {String} Team assigned to deal with this ticket
   * @param stepStartDttm {Date} Ticket step start date and time
   * @param stepScheduleDttm {Date} Ticket step scheduled date and time
   * @param ticketEntities {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities} 
   * @param links {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200Links} 
   * @param searchLimitReached {Boolean} Indicates if the result set was restricted to 500 tickets (False: result set less than 500, True: result set more than 500)
   */
  function LookUpTicketsGetTicketsResponse200(ticketId, ticketDefinitionCode, ticketDefinitionName, ticketStepCode, ticketStepName, status, keyIdentifier, summary, description, createdDttm, assignedUserTeam, stepStartDttm, stepScheduleDttm, ticketEntities, links, searchLimitReached) {
    _classCallCheck(this, LookUpTicketsGetTicketsResponse200);

    LookUpTicketsGetTicketsResponse200.initialize(this, ticketId, ticketDefinitionCode, ticketDefinitionName, ticketStepCode, ticketStepName, status, keyIdentifier, summary, description, createdDttm, assignedUserTeam, stepStartDttm, stepScheduleDttm, ticketEntities, links, searchLimitReached);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(LookUpTicketsGetTicketsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, ticketId, ticketDefinitionCode, ticketDefinitionName, ticketStepCode, ticketStepName, status, keyIdentifier, summary, description, createdDttm, assignedUserTeam, stepStartDttm, stepScheduleDttm, ticketEntities, links, searchLimitReached) {
      obj['ticketId'] = ticketId;
      obj['ticketDefinitionCode'] = ticketDefinitionCode;
      obj['ticketDefinitionName'] = ticketDefinitionName;
      obj['ticketStepCode'] = ticketStepCode;
      obj['ticketStepName'] = ticketStepName;
      obj['status'] = status;
      obj['keyIdentifier'] = keyIdentifier;
      obj['summary'] = summary;
      obj['description'] = description;
      obj['createdDttm'] = createdDttm;
      obj['assignedUserTeam'] = assignedUserTeam;
      obj['stepStartDttm'] = stepStartDttm;
      obj['stepScheduleDttm'] = stepScheduleDttm;
      obj['ticketEntities'] = ticketEntities;
      obj['links'] = links;
      obj['searchLimitReached'] = searchLimitReached;
    }
    /**
     * Constructs a <code>LookUpTicketsGetTicketsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LookUpTicketsGetTicketsResponse200} obj Optional instance to populate.
     * @return {module:model/LookUpTicketsGetTicketsResponse200} The populated <code>LookUpTicketsGetTicketsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new LookUpTicketsGetTicketsResponse200();

        if (data.hasOwnProperty('ticketId')) {
          obj['ticketId'] = _ApiClient["default"].convertToType(data['ticketId'], 'Number');
        }

        if (data.hasOwnProperty('parentTicketId')) {
          obj['parentTicketId'] = _ApiClient["default"].convertToType(data['parentTicketId'], 'Number');
        }

        if (data.hasOwnProperty('ticketDefinitionCode')) {
          obj['ticketDefinitionCode'] = _ApiClient["default"].convertToType(data['ticketDefinitionCode'], 'String');
        }

        if (data.hasOwnProperty('ticketDefinitionName')) {
          obj['ticketDefinitionName'] = _ApiClient["default"].convertToType(data['ticketDefinitionName'], 'String');
        }

        if (data.hasOwnProperty('ticketStepCode')) {
          obj['ticketStepCode'] = _ApiClient["default"].convertToType(data['ticketStepCode'], 'String');
        }

        if (data.hasOwnProperty('ticketStepName')) {
          obj['ticketStepName'] = _ApiClient["default"].convertToType(data['ticketStepName'], 'String');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('keyIdentifier')) {
          obj['keyIdentifier'] = _ApiClient["default"].convertToType(data['keyIdentifier'], 'String');
        }

        if (data.hasOwnProperty('summary')) {
          obj['summary'] = _ApiClient["default"].convertToType(data['summary'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('dueDttm')) {
          obj['dueDttm'] = _ApiClient["default"].convertToType(data['dueDttm'], 'Date');
        }

        if (data.hasOwnProperty('cancelDttm')) {
          obj['cancelDttm'] = _ApiClient["default"].convertToType(data['cancelDttm'], 'Date');
        }

        if (data.hasOwnProperty('ticketCancelReason')) {
          obj['ticketCancelReason'] = _ApiClient["default"].convertToType(data['ticketCancelReason'], 'String');
        }

        if (data.hasOwnProperty('assignedUserTeam')) {
          obj['assignedUserTeam'] = _ApiClient["default"].convertToType(data['assignedUserTeam'], 'String');
        }

        if (data.hasOwnProperty('stepStartDttm')) {
          obj['stepStartDttm'] = _ApiClient["default"].convertToType(data['stepStartDttm'], 'Date');
        }

        if (data.hasOwnProperty('stepScheduleDttm')) {
          obj['stepScheduleDttm'] = _ApiClient["default"].convertToType(data['stepScheduleDttm'], 'Date');
        }

        if (data.hasOwnProperty('stepDueDttm')) {
          obj['stepDueDttm'] = _ApiClient["default"].convertToType(data['stepDueDttm'], 'Date');
        }

        if (data.hasOwnProperty('ticketEntities')) {
          obj['ticketEntities'] = _CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities["default"].constructFromObject(data['ticketEntities']);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _CreateAccountTicketPostAccountsIdTicketsResponse200Links["default"].constructFromObject(data['links']);
        }

        if (data.hasOwnProperty('searchLimitReached')) {
          obj['searchLimitReached'] = _ApiClient["default"].convertToType(data['searchLimitReached'], 'Boolean');
        }
      }

      return obj;
    }
  }]);

  return LookUpTicketsGetTicketsResponse200;
}();
/**
 * Ticket Identifier
 * @member {Number} ticketId
 */


LookUpTicketsGetTicketsResponse200.prototype['ticketId'] = undefined;
/**
 * Id of any parent ticket
 * @member {Number} parentTicketId
 */

LookUpTicketsGetTicketsResponse200.prototype['parentTicketId'] = undefined;
/**
 * Ticket definition code
 * @member {String} ticketDefinitionCode
 */

LookUpTicketsGetTicketsResponse200.prototype['ticketDefinitionCode'] = undefined;
/**
 * Ticket definition name
 * @member {String} ticketDefinitionName
 */

LookUpTicketsGetTicketsResponse200.prototype['ticketDefinitionName'] = undefined;
/**
 * Ticket step code
 * @member {String} ticketStepCode
 */

LookUpTicketsGetTicketsResponse200.prototype['ticketStepCode'] = undefined;
/**
 * Ticket step name
 * @member {String} ticketStepName
 */

LookUpTicketsGetTicketsResponse200.prototype['ticketStepName'] = undefined;
/**
 * Ticket status `Open, Closed, Cancelled, Error`
 * @member {String} status
 */

LookUpTicketsGetTicketsResponse200.prototype['status'] = undefined;
/**
 * Ticket key identifier
 * @member {String} keyIdentifier
 */

LookUpTicketsGetTicketsResponse200.prototype['keyIdentifier'] = undefined;
/**
 * Ticket summary
 * @member {String} summary
 */

LookUpTicketsGetTicketsResponse200.prototype['summary'] = undefined;
/**
 * Ticket description
 * @member {String} description
 */

LookUpTicketsGetTicketsResponse200.prototype['description'] = undefined;
/**
 * Ticket creation date and time
 * @member {Date} createdDttm
 */

LookUpTicketsGetTicketsResponse200.prototype['createdDttm'] = undefined;
/**
 * Ticket due date and time
 * @member {Date} dueDttm
 */

LookUpTicketsGetTicketsResponse200.prototype['dueDttm'] = undefined;
/**
 * Date and time when ticket was cancelled
 * @member {Date} cancelDttm
 */

LookUpTicketsGetTicketsResponse200.prototype['cancelDttm'] = undefined;
/**
 * The reason ticket was cancelled
 * @member {String} ticketCancelReason
 */

LookUpTicketsGetTicketsResponse200.prototype['ticketCancelReason'] = undefined;
/**
 * Team assigned to deal with this ticket
 * @member {String} assignedUserTeam
 */

LookUpTicketsGetTicketsResponse200.prototype['assignedUserTeam'] = undefined;
/**
 * Ticket step start date and time
 * @member {Date} stepStartDttm
 */

LookUpTicketsGetTicketsResponse200.prototype['stepStartDttm'] = undefined;
/**
 * Ticket step scheduled date and time
 * @member {Date} stepScheduleDttm
 */

LookUpTicketsGetTicketsResponse200.prototype['stepScheduleDttm'] = undefined;
/**
 * Ticket step due date and time
 * @member {Date} stepDueDttm
 */

LookUpTicketsGetTicketsResponse200.prototype['stepDueDttm'] = undefined;
/**
 * @member {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities} ticketEntities
 */

LookUpTicketsGetTicketsResponse200.prototype['ticketEntities'] = undefined;
/**
 * @member {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200Links} links
 */

LookUpTicketsGetTicketsResponse200.prototype['links'] = undefined;
/**
 * Indicates if the result set was restricted to 500 tickets (False: result set less than 500, True: result set more than 500)
 * @member {Boolean} searchLimitReached
 */

LookUpTicketsGetTicketsResponse200.prototype['searchLimitReached'] = undefined;
var _default = LookUpTicketsGetTicketsResponse200;
exports["default"] = _default;