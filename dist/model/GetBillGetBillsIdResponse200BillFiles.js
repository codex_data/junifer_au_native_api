"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBillGetBillsIdResponse200Links = _interopRequireDefault(require("./GetBillGetBillsIdResponse200Links1"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillGetBillsIdResponse200BillFiles model module.
 * @module model/GetBillGetBillsIdResponse200BillFiles
 * @version 1.61.1
 */
var GetBillGetBillsIdResponse200BillFiles = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillGetBillsIdResponse200BillFiles</code>.
   * @alias module:model/GetBillGetBillsIdResponse200BillFiles
   * @param id {Number} Bill file ID
   * @param display {String} Bill file type
   * @param viewable {Boolean} Flag indicating whether the bill file can be viewed
   * @param links {module:model/GetBillGetBillsIdResponse200Links1} 
   */
  function GetBillGetBillsIdResponse200BillFiles(id, display, viewable, links) {
    _classCallCheck(this, GetBillGetBillsIdResponse200BillFiles);

    GetBillGetBillsIdResponse200BillFiles.initialize(this, id, display, viewable, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillGetBillsIdResponse200BillFiles, null, [{
    key: "initialize",
    value: function initialize(obj, id, display, viewable, links) {
      obj['id'] = id;
      obj['display'] = display;
      obj['viewable'] = viewable;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetBillGetBillsIdResponse200BillFiles</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillGetBillsIdResponse200BillFiles} obj Optional instance to populate.
     * @return {module:model/GetBillGetBillsIdResponse200BillFiles} The populated <code>GetBillGetBillsIdResponse200BillFiles</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillGetBillsIdResponse200BillFiles();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('display')) {
          obj['display'] = _ApiClient["default"].convertToType(data['display'], 'String');
        }

        if (data.hasOwnProperty('viewable')) {
          obj['viewable'] = _ApiClient["default"].convertToType(data['viewable'], 'Boolean');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetBillGetBillsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetBillGetBillsIdResponse200BillFiles;
}();
/**
 * Bill file ID
 * @member {Number} id
 */


GetBillGetBillsIdResponse200BillFiles.prototype['id'] = undefined;
/**
 * Bill file type
 * @member {String} display
 */

GetBillGetBillsIdResponse200BillFiles.prototype['display'] = undefined;
/**
 * Flag indicating whether the bill file can be viewed
 * @member {Boolean} viewable
 */

GetBillGetBillsIdResponse200BillFiles.prototype['viewable'] = undefined;
/**
 * @member {module:model/GetBillGetBillsIdResponse200Links1} links
 */

GetBillGetBillsIdResponse200BillFiles.prototype['links'] = undefined;
var _default = GetBillGetBillsIdResponse200BillFiles;
exports["default"] = _default;