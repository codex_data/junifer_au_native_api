"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountCreditsGetAccountsIdAccountcreditsResponse200 model module.
 * @module model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200
 * @version 1.61.1
 */
var GetAccountCreditsGetAccountsIdAccountcreditsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountCreditsGetAccountsIdAccountcreditsResponse200</code>.
   * @alias module:model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200
   * @param id {Number} Account credit Identifier
   * @param createdDttm {Date} Date and time when the credit was created
   * @param acceptedDttm {Date} Date and time when the credit was accepted
   * @param grossAmount {Number} Gross amount of account credit
   * @param netAmount {Number} Net amount of account credit
   * @param salesTax {String} Name of linked sales tax to this account credit
   * @param salesTaxAmount {Number} Amount of sales tax for this account credit
   * @param reason {String} Name of linked account credit reason
   * @param reference {String} Custom text reference for account credit
   */
  function GetAccountCreditsGetAccountsIdAccountcreditsResponse200(id, createdDttm, acceptedDttm, grossAmount, netAmount, salesTax, salesTaxAmount, reason, reference) {
    _classCallCheck(this, GetAccountCreditsGetAccountsIdAccountcreditsResponse200);

    GetAccountCreditsGetAccountsIdAccountcreditsResponse200.initialize(this, id, createdDttm, acceptedDttm, grossAmount, netAmount, salesTax, salesTaxAmount, reason, reference);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountCreditsGetAccountsIdAccountcreditsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, createdDttm, acceptedDttm, grossAmount, netAmount, salesTax, salesTaxAmount, reason, reference) {
      obj['id'] = id;
      obj['createdDttm'] = createdDttm;
      obj['acceptedDttm'] = acceptedDttm;
      obj['grossAmount'] = grossAmount;
      obj['netAmount'] = netAmount;
      obj['salesTax'] = salesTax;
      obj['salesTaxAmount'] = salesTaxAmount;
      obj['reason'] = reason;
      obj['reference'] = reference;
    }
    /**
     * Constructs a <code>GetAccountCreditsGetAccountsIdAccountcreditsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200} The populated <code>GetAccountCreditsGetAccountsIdAccountcreditsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountCreditsGetAccountsIdAccountcreditsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('acceptedDttm')) {
          obj['acceptedDttm'] = _ApiClient["default"].convertToType(data['acceptedDttm'], 'Date');
        }

        if (data.hasOwnProperty('grossAmount')) {
          obj['grossAmount'] = _ApiClient["default"].convertToType(data['grossAmount'], 'Number');
        }

        if (data.hasOwnProperty('netAmount')) {
          obj['netAmount'] = _ApiClient["default"].convertToType(data['netAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTax')) {
          obj['salesTax'] = _ApiClient["default"].convertToType(data['salesTax'], 'String');
        }

        if (data.hasOwnProperty('salesTaxAmount')) {
          obj['salesTaxAmount'] = _ApiClient["default"].convertToType(data['salesTaxAmount'], 'Number');
        }

        if (data.hasOwnProperty('reason')) {
          obj['reason'] = _ApiClient["default"].convertToType(data['reason'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('cancelledDttm')) {
          obj['cancelledDttm'] = _ApiClient["default"].convertToType(data['cancelledDttm'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return GetAccountCreditsGetAccountsIdAccountcreditsResponse200;
}();
/**
 * Account credit Identifier
 * @member {Number} id
 */


GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['id'] = undefined;
/**
 * Date and time when the credit was created
 * @member {Date} createdDttm
 */

GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['createdDttm'] = undefined;
/**
 * Date and time when the credit was accepted
 * @member {Date} acceptedDttm
 */

GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['acceptedDttm'] = undefined;
/**
 * Gross amount of account credit
 * @member {Number} grossAmount
 */

GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['grossAmount'] = undefined;
/**
 * Net amount of account credit
 * @member {Number} netAmount
 */

GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['netAmount'] = undefined;
/**
 * Name of linked sales tax to this account credit
 * @member {String} salesTax
 */

GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['salesTax'] = undefined;
/**
 * Amount of sales tax for this account credit
 * @member {Number} salesTaxAmount
 */

GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['salesTaxAmount'] = undefined;
/**
 * Name of linked account credit reason
 * @member {String} reason
 */

GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['reason'] = undefined;
/**
 * Custom text reference for account credit
 * @member {String} reference
 */

GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['reference'] = undefined;
/**
 * Date and time when the account credit was cancelled
 * @member {Date} cancelledDttm
 */

GetAccountCreditsGetAccountsIdAccountcreditsResponse200.prototype['cancelledDttm'] = undefined;
var _default = GetAccountCreditsGetAccountsIdAccountcreditsResponse200;
exports["default"] = _default;