"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetCreditGetCreditsIdResponse200CreditFiles = _interopRequireDefault(require("./GetCreditGetCreditsIdResponse200CreditFiles"));

var _GetCreditGetCreditsIdResponse200Links = _interopRequireDefault(require("./GetCreditGetCreditsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCreditGetCreditsIdResponse200 model module.
 * @module model/GetCreditGetCreditsIdResponse200
 * @version 1.61.1
 */
var GetCreditGetCreditsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCreditGetCreditsIdResponse200</code>.
   * @alias module:model/GetCreditGetCreditsIdResponse200
   * @param id {Number} credit ID
   * @param number {String} Credit number
   * @param createdDttm {Date} Credit creation date and time
   * @param status {String} Status of the Credit
   * @param currency {String} Currency ISO code
   * @param grossAmount {Number} Gross amount shown on the credit
   * @param netAmount {Number} Net amount shown on the credit
   * @param salesTaxAmount {Number} Sales tax amount shown on the credit
   * @param issueDt {Date} Credit issue date
   * @param links {module:model/GetCreditGetCreditsIdResponse200Links} 
   * @param creditFiles {Array.<module:model/GetCreditGetCreditsIdResponse200CreditFiles>} Downloadable credit files
   */
  function GetCreditGetCreditsIdResponse200(id, number, createdDttm, status, currency, grossAmount, netAmount, salesTaxAmount, issueDt, links, creditFiles) {
    _classCallCheck(this, GetCreditGetCreditsIdResponse200);

    GetCreditGetCreditsIdResponse200.initialize(this, id, number, createdDttm, status, currency, grossAmount, netAmount, salesTaxAmount, issueDt, links, creditFiles);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCreditGetCreditsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, number, createdDttm, status, currency, grossAmount, netAmount, salesTaxAmount, issueDt, links, creditFiles) {
      obj['id'] = id;
      obj['number'] = number;
      obj['createdDttm'] = createdDttm;
      obj['status'] = status;
      obj['currency'] = currency;
      obj['grossAmount'] = grossAmount;
      obj['netAmount'] = netAmount;
      obj['salesTaxAmount'] = salesTaxAmount;
      obj['issueDt'] = issueDt;
      obj['links'] = links;
      obj['creditFiles'] = creditFiles;
    }
    /**
     * Constructs a <code>GetCreditGetCreditsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCreditGetCreditsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetCreditGetCreditsIdResponse200} The populated <code>GetCreditGetCreditsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCreditGetCreditsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('authorisedDttm')) {
          obj['authorisedDttm'] = _ApiClient["default"].convertToType(data['authorisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('grossAmount')) {
          obj['grossAmount'] = _ApiClient["default"].convertToType(data['grossAmount'], 'Number');
        }

        if (data.hasOwnProperty('netAmount')) {
          obj['netAmount'] = _ApiClient["default"].convertToType(data['netAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTaxAmount')) {
          obj['salesTaxAmount'] = _ApiClient["default"].convertToType(data['salesTaxAmount'], 'Number');
        }

        if (data.hasOwnProperty('issueDt')) {
          obj['issueDt'] = _ApiClient["default"].convertToType(data['issueDt'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetCreditGetCreditsIdResponse200Links["default"].constructFromObject(data['links']);
        }

        if (data.hasOwnProperty('creditFiles')) {
          obj['creditFiles'] = _ApiClient["default"].convertToType(data['creditFiles'], [_GetCreditGetCreditsIdResponse200CreditFiles["default"]]);
        }
      }

      return obj;
    }
  }]);

  return GetCreditGetCreditsIdResponse200;
}();
/**
 * credit ID
 * @member {Number} id
 */


GetCreditGetCreditsIdResponse200.prototype['id'] = undefined;
/**
 * Credit number
 * @member {String} number
 */

GetCreditGetCreditsIdResponse200.prototype['number'] = undefined;
/**
 * Credit creation date and time
 * @member {Date} createdDttm
 */

GetCreditGetCreditsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * Status of the Credit
 * @member {String} status
 */

GetCreditGetCreditsIdResponse200.prototype['status'] = undefined;
/**
 * Datetime the Credit was authorised
 * @member {Date} authorisedDttm
 */

GetCreditGetCreditsIdResponse200.prototype['authorisedDttm'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

GetCreditGetCreditsIdResponse200.prototype['currency'] = undefined;
/**
 * Gross amount shown on the credit
 * @member {Number} grossAmount
 */

GetCreditGetCreditsIdResponse200.prototype['grossAmount'] = undefined;
/**
 * Net amount shown on the credit
 * @member {Number} netAmount
 */

GetCreditGetCreditsIdResponse200.prototype['netAmount'] = undefined;
/**
 * Sales tax amount shown on the credit
 * @member {Number} salesTaxAmount
 */

GetCreditGetCreditsIdResponse200.prototype['salesTaxAmount'] = undefined;
/**
 * Credit issue date
 * @member {Date} issueDt
 */

GetCreditGetCreditsIdResponse200.prototype['issueDt'] = undefined;
/**
 * @member {module:model/GetCreditGetCreditsIdResponse200Links} links
 */

GetCreditGetCreditsIdResponse200.prototype['links'] = undefined;
/**
 * Downloadable credit files
 * @member {Array.<module:model/GetCreditGetCreditsIdResponse200CreditFiles>} creditFiles
 */

GetCreditGetCreditsIdResponse200.prototype['creditFiles'] = undefined;
var _default = GetCreditGetCreditsIdResponse200;
exports["default"] = _default;