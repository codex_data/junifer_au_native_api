"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links model module.
 * @module model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links
 * @version 1.61.1
 */
var GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links
   * @param self {String} Link referring to this meterpoint
   * @param readings {String} Link to the readings
   * @param meterStructure {String} Link to resource which lists the meterpoint structure - meters attached, meter registers, etc.
   */
  function GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links(self, readings, meterStructure) {
    _classCallCheck(this, GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links);

    GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links.initialize(this, self, readings, meterStructure);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, readings, meterStructure) {
      obj['self'] = self;
      obj['readings'] = readings;
      obj['meterStructure'] = meterStructure;
    }
    /**
     * Constructs a <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links} The populated <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('readings')) {
          obj['readings'] = _ApiClient["default"].convertToType(data['readings'], 'String');
        }

        if (data.hasOwnProperty('meterStructure')) {
          obj['meterStructure'] = _ApiClient["default"].convertToType(data['meterStructure'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links;
}();
/**
 * Link referring to this meterpoint
 * @member {String} self
 */


GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links.prototype['self'] = undefined;
/**
 * Link to the readings
 * @member {String} readings
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links.prototype['readings'] = undefined;
/**
 * Link to resource which lists the meterpoint structure - meters attached, meter registers, etc.
 * @member {String} meterStructure
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links.prototype['meterStructure'] = undefined;
var _default = GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links;
exports["default"] = _default;