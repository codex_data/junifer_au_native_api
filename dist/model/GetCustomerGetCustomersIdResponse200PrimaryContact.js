"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _ContactsGetAccountsIdContactsResponse200Links = _interopRequireDefault(require("./ContactsGetAccountsIdContactsResponse200Links"));

var _GetCustomerGetCustomersIdResponse200PrimaryContactAddress = _interopRequireDefault(require("./GetCustomerGetCustomersIdResponse200PrimaryContactAddress"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCustomerGetCustomersIdResponse200PrimaryContact model module.
 * @module model/GetCustomerGetCustomersIdResponse200PrimaryContact
 * @version 1.61.1
 */
var GetCustomerGetCustomersIdResponse200PrimaryContact = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCustomerGetCustomersIdResponse200PrimaryContact</code>.
   * Customer&#39;s primary contact
   * @alias module:model/GetCustomerGetCustomersIdResponse200PrimaryContact
   * @param id {Number} Contact Id
   * @param contactType {String} Contact Type
   * @param primary {Boolean} Whether contact is the primary contact
   * @param surname {String} Customer's primary contact surname
   * @param initials {String} Customer's primary contact initials
   * @param jobTitle {String} Customer's primary contact jobTitle
   * @param customerContactId {Number} Customer contact Id
   * @param cancelled {Boolean} True if this contact is no longer associated with the customer
   * @param fromDttm {Date} Contacts from date time
   * @param links {module:model/ContactsGetAccountsIdContactsResponse200Links} 
   */
  function GetCustomerGetCustomersIdResponse200PrimaryContact(id, contactType, primary, surname, initials, jobTitle, customerContactId, cancelled, fromDttm, links) {
    _classCallCheck(this, GetCustomerGetCustomersIdResponse200PrimaryContact);

    GetCustomerGetCustomersIdResponse200PrimaryContact.initialize(this, id, contactType, primary, surname, initials, jobTitle, customerContactId, cancelled, fromDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCustomerGetCustomersIdResponse200PrimaryContact, null, [{
    key: "initialize",
    value: function initialize(obj, id, contactType, primary, surname, initials, jobTitle, customerContactId, cancelled, fromDttm, links) {
      obj['id'] = id;
      obj['contactType'] = contactType;
      obj['primary'] = primary;
      obj['surname'] = surname;
      obj['initials'] = initials;
      obj['jobTitle'] = jobTitle;
      obj['customerContactId'] = customerContactId;
      obj['cancelled'] = cancelled;
      obj['fromDttm'] = fromDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetCustomerGetCustomersIdResponse200PrimaryContact</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCustomerGetCustomersIdResponse200PrimaryContact} obj Optional instance to populate.
     * @return {module:model/GetCustomerGetCustomersIdResponse200PrimaryContact} The populated <code>GetCustomerGetCustomersIdResponse200PrimaryContact</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCustomerGetCustomersIdResponse200PrimaryContact();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('contactType')) {
          obj['contactType'] = _ApiClient["default"].convertToType(data['contactType'], 'String');
        }

        if (data.hasOwnProperty('primary')) {
          obj['primary'] = _ApiClient["default"].convertToType(data['primary'], 'Boolean');
        }

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('initials')) {
          obj['initials'] = _ApiClient["default"].convertToType(data['initials'], 'String');
        }

        if (data.hasOwnProperty('jobTitle')) {
          obj['jobTitle'] = _ApiClient["default"].convertToType(data['jobTitle'], 'String');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _GetCustomerGetCustomersIdResponse200PrimaryContactAddress["default"].constructFromObject(data['address']);
        }

        if (data.hasOwnProperty('addressType')) {
          obj['addressType'] = _ApiClient["default"].convertToType(data['addressType'], 'String');
        }

        if (data.hasOwnProperty('countryCode')) {
          obj['countryCode'] = _ApiClient["default"].convertToType(data['countryCode'], 'String');
        }

        if (data.hasOwnProperty('country')) {
          obj['country'] = _ApiClient["default"].convertToType(data['country'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('phoneNumber1')) {
          obj['phoneNumber1'] = _ApiClient["default"].convertToType(data['phoneNumber1'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber2')) {
          obj['phoneNumber2'] = _ApiClient["default"].convertToType(data['phoneNumber2'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber3')) {
          obj['phoneNumber3'] = _ApiClient["default"].convertToType(data['phoneNumber3'], 'String');
        }

        if (data.hasOwnProperty('customerContactId')) {
          obj['customerContactId'] = _ApiClient["default"].convertToType(data['customerContactId'], 'Number');
        }

        if (data.hasOwnProperty('cancelled')) {
          obj['cancelled'] = _ApiClient["default"].convertToType(data['cancelled'], 'Boolean');
        }

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _ContactsGetAccountsIdContactsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetCustomerGetCustomersIdResponse200PrimaryContact;
}();
/**
 * Contact Id
 * @member {Number} id
 */


GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['id'] = undefined;
/**
 * Contact Type
 * @member {String} contactType
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['contactType'] = undefined;
/**
 * Whether contact is the primary contact
 * @member {Boolean} primary
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['primary'] = undefined;
/**
 * Customer's primary contact title
 * @member {String} title
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['title'] = undefined;
/**
 * Customer's primary contact forename
 * @member {String} forename
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['forename'] = undefined;
/**
 * Customer's primary contact surname
 * @member {String} surname
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['surname'] = undefined;
/**
 * Customer's primary contact initials
 * @member {String} initials
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['initials'] = undefined;
/**
 * Customer's primary contact jobTitle
 * @member {String} jobTitle
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['jobTitle'] = undefined;
/**
 * @member {module:model/GetCustomerGetCustomersIdResponse200PrimaryContactAddress} address
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['address'] = undefined;
/**
 * Internally used address type which defines number of lines and format used etc.
 * @member {String} addressType
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['addressType'] = undefined;
/**
 * Country code
 * @member {String} countryCode
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['countryCode'] = undefined;
/**
 * Country name
 * @member {String} country
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['country'] = undefined;
/**
 * Customer's primary contact email
 * @member {String} email
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['email'] = undefined;
/**
 * Customer's date of birth
 * @member {Date} dateOfBirth
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['dateOfBirth'] = undefined;
/**
 * Customer's primary contact phone number
 * @member {String} phoneNumber1
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['phoneNumber1'] = undefined;
/**
 * Customer's secondary contact phone number
 * @member {String} phoneNumber2
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['phoneNumber2'] = undefined;
/**
 * Customer's third contact phone number
 * @member {String} phoneNumber3
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['phoneNumber3'] = undefined;
/**
 * Customer contact Id
 * @member {Number} customerContactId
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['customerContactId'] = undefined;
/**
 * True if this contact is no longer associated with the customer
 * @member {Boolean} cancelled
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['cancelled'] = undefined;
/**
 * Contacts from date time
 * @member {Date} fromDttm
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['fromDttm'] = undefined;
/**
 * Contacts to date time
 * @member {Date} toDttm
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['toDttm'] = undefined;
/**
 * @member {module:model/ContactsGetAccountsIdContactsResponse200Links} links
 */

GetCustomerGetCustomersIdResponse200PrimaryContact.prototype['links'] = undefined;
var _default = GetCustomerGetCustomersIdResponse200PrimaryContact;
exports["default"] = _default;