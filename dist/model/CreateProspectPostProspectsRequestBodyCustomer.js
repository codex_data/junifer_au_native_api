"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateProspectPostProspectsRequestBodyCustomerCompanyAddress = _interopRequireDefault(require("./CreateProspectPostProspectsRequestBodyCustomerCompanyAddress"));

var _CreateProspectPostProspectsRequestBodyCustomerPrimaryContact = _interopRequireDefault(require("./CreateProspectPostProspectsRequestBodyCustomerPrimaryContact"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateProspectPostProspectsRequestBodyCustomer model module.
 * @module model/CreateProspectPostProspectsRequestBodyCustomer
 * @version 1.61.1
 */
var CreateProspectPostProspectsRequestBodyCustomer = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProspectPostProspectsRequestBodyCustomer</code>.
   * New customer
   * @alias module:model/CreateProspectPostProspectsRequestBodyCustomer
   * @param customerClass {String} Customer class name - see the Customer Class ref table for valid values
   * @param customerType {String} Customer type name - see the Customer Type ref table for valid values
   * @param primaryContact {module:model/CreateProspectPostProspectsRequestBodyCustomerPrimaryContact} 
   */
  function CreateProspectPostProspectsRequestBodyCustomer(customerClass, customerType, primaryContact) {
    _classCallCheck(this, CreateProspectPostProspectsRequestBodyCustomer);

    CreateProspectPostProspectsRequestBodyCustomer.initialize(this, customerClass, customerType, primaryContact);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProspectPostProspectsRequestBodyCustomer, null, [{
    key: "initialize",
    value: function initialize(obj, customerClass, customerType, primaryContact) {
      obj['customerClass'] = customerClass;
      obj['customerType'] = customerType;
      obj['primaryContact'] = primaryContact;
    }
    /**
     * Constructs a <code>CreateProspectPostProspectsRequestBodyCustomer</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProspectPostProspectsRequestBodyCustomer} obj Optional instance to populate.
     * @return {module:model/CreateProspectPostProspectsRequestBodyCustomer} The populated <code>CreateProspectPostProspectsRequestBodyCustomer</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProspectPostProspectsRequestBodyCustomer();

        if (data.hasOwnProperty('billingEntity')) {
          obj['billingEntity'] = _ApiClient["default"].convertToType(data['billingEntity'], 'String');
        }

        if (data.hasOwnProperty('customerClass')) {
          obj['customerClass'] = _ApiClient["default"].convertToType(data['customerClass'], 'String');
        }

        if (data.hasOwnProperty('customerType')) {
          obj['customerType'] = _ApiClient["default"].convertToType(data['customerType'], 'String');
        }

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('companyNumber')) {
          obj['companyNumber'] = _ApiClient["default"].convertToType(data['companyNumber'], 'String');
        }

        if (data.hasOwnProperty('companyAddress')) {
          obj['companyAddress'] = _CreateProspectPostProspectsRequestBodyCustomerCompanyAddress["default"].constructFromObject(data['companyAddress']);
        }

        if (data.hasOwnProperty('primaryContact')) {
          obj['primaryContact'] = _CreateProspectPostProspectsRequestBodyCustomerPrimaryContact["default"].constructFromObject(data['primaryContact']);
        }
      }

      return obj;
    }
  }]);

  return CreateProspectPostProspectsRequestBodyCustomer;
}();
/**
 * Billing entity name - see the Billing Entity screen for valid values
 * @member {String} billingEntity
 */


CreateProspectPostProspectsRequestBodyCustomer.prototype['billingEntity'] = undefined;
/**
 * Customer class name - see the Customer Class ref table for valid values
 * @member {String} customerClass
 */

CreateProspectPostProspectsRequestBodyCustomer.prototype['customerClass'] = undefined;
/**
 * Customer type name - see the Customer Type ref table for valid values
 * @member {String} customerType
 */

CreateProspectPostProspectsRequestBodyCustomer.prototype['customerType'] = undefined;
/**
 * Customer title - see the Title ref table for valid values
 * @member {String} title
 */

CreateProspectPostProspectsRequestBodyCustomer.prototype['title'] = undefined;
/**
 * Customer forename
 * @member {String} forename
 */

CreateProspectPostProspectsRequestBodyCustomer.prototype['forename'] = undefined;
/**
 * Customer surname
 * @member {String} surname
 */

CreateProspectPostProspectsRequestBodyCustomer.prototype['surname'] = undefined;
/**
 * Company number
 * @member {String} companyNumber
 */

CreateProspectPostProspectsRequestBodyCustomer.prototype['companyNumber'] = undefined;
/**
 * @member {module:model/CreateProspectPostProspectsRequestBodyCustomerCompanyAddress} companyAddress
 */

CreateProspectPostProspectsRequestBodyCustomer.prototype['companyAddress'] = undefined;
/**
 * @member {module:model/CreateProspectPostProspectsRequestBodyCustomerPrimaryContact} primaryContact
 */

CreateProspectPostProspectsRequestBodyCustomer.prototype['primaryContact'] = undefined;
var _default = CreateProspectPostProspectsRequestBodyCustomer;
exports["default"] = _default;