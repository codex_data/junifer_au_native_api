"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody model module.
 * @module model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody
 * @version 1.61.1
 */
var UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody</code>.
   * @alias module:model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody
   */
  function UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody() {
    _classCallCheck(this, UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody);

    UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody} obj Optional instance to populate.
     * @return {module:model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody} The populated <code>UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody();

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postCode')) {
          obj['postCode'] = _ApiClient["default"].convertToType(data['postCode'], 'String');
        }

        if (data.hasOwnProperty('country')) {
          obj['country'] = _ApiClient["default"].convertToType(data['country'], 'String');
        }
      }

      return obj;
    }
  }]);

  return UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody;
}();
/**
 * Address line 1
 * @member {String} address1
 */


UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.prototype['address1'] = undefined;
/**
 * Address line 2
 * @member {String} address2
 */

UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.prototype['address2'] = undefined;
/**
 * Address line 3
 * @member {String} address3
 */

UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.prototype['address3'] = undefined;
/**
 * Address line 4
 * @member {String} address4
 */

UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.prototype['address4'] = undefined;
/**
 * Address line 5
 * @member {String} address5
 */

UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.prototype['address5'] = undefined;
/**
 * Address post code
 * @member {String} postCode
 */

UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.prototype['postCode'] = undefined;
/**
 * Address Country
 * @member {String} country
 */

UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.prototype['country'] = undefined;
var _default = UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody;
exports["default"] = _default;