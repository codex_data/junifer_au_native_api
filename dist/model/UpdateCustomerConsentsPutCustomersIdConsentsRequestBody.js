"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateCustomerConsentsPutCustomersIdConsentsRequestBody model module.
 * @module model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody
 * @version 1.61.1
 */
var UpdateCustomerConsentsPutCustomersIdConsentsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateCustomerConsentsPutCustomersIdConsentsRequestBody</code>.
   * @alias module:model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody
   * @param consentDefinition {String} Consent definition. Can be `Dcc Daily Read, Dcc Half Hourly Read`
   * @param setting {Boolean} true or false. Can be `true, false`
   * @param fromDt {Date} Consent from date. Example: `2019-02-14`
   */
  function UpdateCustomerConsentsPutCustomersIdConsentsRequestBody(consentDefinition, setting, fromDt) {
    _classCallCheck(this, UpdateCustomerConsentsPutCustomersIdConsentsRequestBody);

    UpdateCustomerConsentsPutCustomersIdConsentsRequestBody.initialize(this, consentDefinition, setting, fromDt);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateCustomerConsentsPutCustomersIdConsentsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, consentDefinition, setting, fromDt) {
      obj['consentDefinition'] = consentDefinition;
      obj['setting'] = setting;
      obj['fromDt'] = fromDt;
    }
    /**
     * Constructs a <code>UpdateCustomerConsentsPutCustomersIdConsentsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody} obj Optional instance to populate.
     * @return {module:model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody} The populated <code>UpdateCustomerConsentsPutCustomersIdConsentsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateCustomerConsentsPutCustomersIdConsentsRequestBody();

        if (data.hasOwnProperty('consentDefinition')) {
          obj['consentDefinition'] = _ApiClient["default"].convertToType(data['consentDefinition'], 'String');
        }

        if (data.hasOwnProperty('setting')) {
          obj['setting'] = _ApiClient["default"].convertToType(data['setting'], 'Boolean');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return UpdateCustomerConsentsPutCustomersIdConsentsRequestBody;
}();
/**
 * Consent definition. Can be `Dcc Daily Read, Dcc Half Hourly Read`
 * @member {String} consentDefinition
 */


UpdateCustomerConsentsPutCustomersIdConsentsRequestBody.prototype['consentDefinition'] = undefined;
/**
 * true or false. Can be `true, false`
 * @member {Boolean} setting
 */

UpdateCustomerConsentsPutCustomersIdConsentsRequestBody.prototype['setting'] = undefined;
/**
 * Consent from date. Example: `2019-02-14`
 * @member {Date} fromDt
 */

UpdateCustomerConsentsPutCustomersIdConsentsRequestBody.prototype['fromDt'] = undefined;
/**
 * Consent to date, defaults to null date (meaning there is no end date). Example: `2021-02-14`
 * @member {Date} toDt
 */

UpdateCustomerConsentsPutCustomersIdConsentsRequestBody.prototype['toDt'] = undefined;
var _default = UpdateCustomerConsentsPutCustomersIdConsentsRequestBody;
exports["default"] = _default;