"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _ContactsGetAccountsIdContactsResponse200Links = _interopRequireDefault(require("./ContactsGetAccountsIdContactsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The ContactsGetCustomersIdContactsResponse200 model module.
 * @module model/ContactsGetCustomersIdContactsResponse200
 * @version 1.61.1
 */
var ContactsGetCustomersIdContactsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ContactsGetCustomersIdContactsResponse200</code>.
   * @alias module:model/ContactsGetCustomersIdContactsResponse200
   * @param id {Number} Contact Id
   * @param contactType {String} Contact Type
   * @param primary {Boolean} Whether the contact is the primary
   * @param surname {String} Customer's contact surname
   * @param email {String} Customer's contact email
   * @param customerContactId {Number} Customer contact Id
   * @param cancelled {Boolean} True if this contact is no longer associated with the customer
   * @param fromDttm {Date} Contacts from date time
   * @param links {module:model/ContactsGetAccountsIdContactsResponse200Links} 
   */
  function ContactsGetCustomersIdContactsResponse200(id, contactType, primary, surname, email, customerContactId, cancelled, fromDttm, links) {
    _classCallCheck(this, ContactsGetCustomersIdContactsResponse200);

    ContactsGetCustomersIdContactsResponse200.initialize(this, id, contactType, primary, surname, email, customerContactId, cancelled, fromDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ContactsGetCustomersIdContactsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, contactType, primary, surname, email, customerContactId, cancelled, fromDttm, links) {
      obj['id'] = id;
      obj['contactType'] = contactType;
      obj['primary'] = primary;
      obj['surname'] = surname;
      obj['email'] = email;
      obj['customerContactId'] = customerContactId;
      obj['cancelled'] = cancelled;
      obj['fromDttm'] = fromDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>ContactsGetCustomersIdContactsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ContactsGetCustomersIdContactsResponse200} obj Optional instance to populate.
     * @return {module:model/ContactsGetCustomersIdContactsResponse200} The populated <code>ContactsGetCustomersIdContactsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ContactsGetCustomersIdContactsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('contactType')) {
          obj['contactType'] = _ApiClient["default"].convertToType(data['contactType'], 'String');
        }

        if (data.hasOwnProperty('primary')) {
          obj['primary'] = _ApiClient["default"].convertToType(data['primary'], 'Boolean');
        }

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('initials')) {
          obj['initials'] = _ApiClient["default"].convertToType(data['initials'], 'String');
        }

        if (data.hasOwnProperty('jobTitle')) {
          obj['jobTitle'] = _ApiClient["default"].convertToType(data['jobTitle'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('phoneNumber1')) {
          obj['phoneNumber1'] = _ApiClient["default"].convertToType(data['phoneNumber1'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber2')) {
          obj['phoneNumber2'] = _ApiClient["default"].convertToType(data['phoneNumber2'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber3')) {
          obj['phoneNumber3'] = _ApiClient["default"].convertToType(data['phoneNumber3'], 'String');
        }

        if (data.hasOwnProperty('customerContactId')) {
          obj['customerContactId'] = _ApiClient["default"].convertToType(data['customerContactId'], 'Number');
        }

        if (data.hasOwnProperty('cancelled')) {
          obj['cancelled'] = _ApiClient["default"].convertToType(data['cancelled'], 'Boolean');
        }

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _ContactsGetAccountsIdContactsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return ContactsGetCustomersIdContactsResponse200;
}();
/**
 * Contact Id
 * @member {Number} id
 */


ContactsGetCustomersIdContactsResponse200.prototype['id'] = undefined;
/**
 * Contact Type
 * @member {String} contactType
 */

ContactsGetCustomersIdContactsResponse200.prototype['contactType'] = undefined;
/**
 * Whether the contact is the primary
 * @member {Boolean} primary
 */

ContactsGetCustomersIdContactsResponse200.prototype['primary'] = undefined;
/**
 * Customer's contact title
 * @member {String} title
 */

ContactsGetCustomersIdContactsResponse200.prototype['title'] = undefined;
/**
 * Customer's contact forename
 * @member {String} forename
 */

ContactsGetCustomersIdContactsResponse200.prototype['forename'] = undefined;
/**
 * Customer's contact surname
 * @member {String} surname
 */

ContactsGetCustomersIdContactsResponse200.prototype['surname'] = undefined;
/**
 * Customer's contact initials
 * @member {String} initials
 */

ContactsGetCustomersIdContactsResponse200.prototype['initials'] = undefined;
/**
 * Customer's contact job title
 * @member {String} jobTitle
 */

ContactsGetCustomersIdContactsResponse200.prototype['jobTitle'] = undefined;
/**
 * Customer's contact email
 * @member {String} email
 */

ContactsGetCustomersIdContactsResponse200.prototype['email'] = undefined;
/**
 * Customer's date of birth
 * @member {Date} dateOfBirth
 */

ContactsGetCustomersIdContactsResponse200.prototype['dateOfBirth'] = undefined;
/**
 * Customer's primary contact phone number
 * @member {String} phoneNumber1
 */

ContactsGetCustomersIdContactsResponse200.prototype['phoneNumber1'] = undefined;
/**
 * Customer's secondary contact phone number
 * @member {String} phoneNumber2
 */

ContactsGetCustomersIdContactsResponse200.prototype['phoneNumber2'] = undefined;
/**
 * Customer's third contact phone number
 * @member {String} phoneNumber3
 */

ContactsGetCustomersIdContactsResponse200.prototype['phoneNumber3'] = undefined;
/**
 * Customer contact Id
 * @member {Number} customerContactId
 */

ContactsGetCustomersIdContactsResponse200.prototype['customerContactId'] = undefined;
/**
 * True if this contact is no longer associated with the customer
 * @member {Boolean} cancelled
 */

ContactsGetCustomersIdContactsResponse200.prototype['cancelled'] = undefined;
/**
 * Contacts from date time
 * @member {Date} fromDttm
 */

ContactsGetCustomersIdContactsResponse200.prototype['fromDttm'] = undefined;
/**
 * Contacts to date time
 * @member {Date} toDttm
 */

ContactsGetCustomersIdContactsResponse200.prototype['toDttm'] = undefined;
/**
 * @member {module:model/ContactsGetAccountsIdContactsResponse200Links} links
 */

ContactsGetCustomersIdContactsResponse200.prototype['links'] = undefined;
var _default = ContactsGetCustomersIdContactsResponse200;
exports["default"] = _default;