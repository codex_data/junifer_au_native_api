"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The PaymentPostAccountsIdPaymentRequestBody model module.
 * @module model/PaymentPostAccountsIdPaymentRequestBody
 * @version 1.61.1
 */
var PaymentPostAccountsIdPaymentRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>PaymentPostAccountsIdPaymentRequestBody</code>.
   * @alias module:model/PaymentPostAccountsIdPaymentRequestBody
   * @param amount {Number} 
   * @param paymentMethodType {String} Please ask Junifer to find out what payment method types are supported in your installation.
   * @param description {String} A description of the payment request. Will be displayed against the transaction in the financials screen.
   */
  function PaymentPostAccountsIdPaymentRequestBody(amount, paymentMethodType, description) {
    _classCallCheck(this, PaymentPostAccountsIdPaymentRequestBody);

    PaymentPostAccountsIdPaymentRequestBody.initialize(this, amount, paymentMethodType, description);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(PaymentPostAccountsIdPaymentRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, amount, paymentMethodType, description) {
      obj['amount'] = amount;
      obj['paymentMethodType'] = paymentMethodType;
      obj['description'] = description;
    }
    /**
     * Constructs a <code>PaymentPostAccountsIdPaymentRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/PaymentPostAccountsIdPaymentRequestBody} obj Optional instance to populate.
     * @return {module:model/PaymentPostAccountsIdPaymentRequestBody} The populated <code>PaymentPostAccountsIdPaymentRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new PaymentPostAccountsIdPaymentRequestBody();

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }

        if (data.hasOwnProperty('paymentMethodType')) {
          obj['paymentMethodType'] = _ApiClient["default"].convertToType(data['paymentMethodType'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('postedDt')) {
          obj['postedDt'] = _ApiClient["default"].convertToType(data['postedDt'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return PaymentPostAccountsIdPaymentRequestBody;
}();
/**
 * 
 * @member {Number} amount
 */


PaymentPostAccountsIdPaymentRequestBody.prototype['amount'] = undefined;
/**
 * Please ask Junifer to find out what payment method types are supported in your installation.
 * @member {String} paymentMethodType
 */

PaymentPostAccountsIdPaymentRequestBody.prototype['paymentMethodType'] = undefined;
/**
 * A description of the payment request. Will be displayed against the transaction in the financials screen.
 * @member {String} description
 */

PaymentPostAccountsIdPaymentRequestBody.prototype['description'] = undefined;
/**
 * Can be Successful, Pending or PendingAuthorisation for a Direct Debit payment method type. For all other supported payment method types can be either Successful or Cancelled. If unsupplied status will default to Successful.
 * @member {String} status
 */

PaymentPostAccountsIdPaymentRequestBody.prototype['status'] = undefined;
/**
 * Date of when the payment was posted.
 * @member {Date} postedDt
 */

PaymentPostAccountsIdPaymentRequestBody.prototype['postedDt'] = undefined;
var _default = PaymentPostAccountsIdPaymentRequestBody;
exports["default"] = _default;