"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints = _interopRequireDefault(require("./AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct model module.
 * @module model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct
 * @version 1.61.1
 */
var AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct</code>.
   * Electricity product the customer chose
   * @alias module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct
   * @param meterPoints {Array.<module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints>} List of NMIs to associate with a new electricity agreement
   */
  function AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct(meterPoints) {
    _classCallCheck(this, AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct);

    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct.initialize(this, meterPoints);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct, null, [{
    key: "initialize",
    value: function initialize(obj, meterPoints) {
      obj['meterPoints'] = meterPoints;
    }
    /**
     * Constructs a <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct} obj Optional instance to populate.
     * @return {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct} The populated <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct();

        if (data.hasOwnProperty('previousSupplier')) {
          obj['previousSupplier'] = _ApiClient["default"].convertToType(data['previousSupplier'], 'String');
        }

        if (data.hasOwnProperty('previousTariff')) {
          obj['previousTariff'] = _ApiClient["default"].convertToType(data['previousTariff'], 'String');
        }

        if (data.hasOwnProperty('productCode')) {
          obj['productCode'] = _ApiClient["default"].convertToType(data['productCode'], 'String');
        }

        if (data.hasOwnProperty('newConnectionFl')) {
          obj['newConnectionFl'] = _ApiClient["default"].convertToType(data['newConnectionFl'], 'Boolean');
        }

        if (data.hasOwnProperty('upgradeMeteringFl')) {
          obj['upgradeMeteringFl'] = _ApiClient["default"].convertToType(data['upgradeMeteringFl'], 'Boolean');
        }

        if (data.hasOwnProperty('meterPoints')) {
          obj['meterPoints'] = _ApiClient["default"].convertToType(data['meterPoints'], [_AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints["default"]]);
        }
      }

      return obj;
    }
  }]);

  return AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct;
}();
/**
 * Previous supplier to the meterpoint
 * @member {String} previousSupplier
 */


AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct.prototype['previousSupplier'] = undefined;
/**
 * Previous Tariff of the meterpoint
 * @member {String} previousTariff
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct.prototype['previousTariff'] = undefined;
/**
 * Electricity product code (reference in Junifer)
 * @member {String} productCode
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct.prototype['productCode'] = undefined;
/**
 * Is this a new connection requiring a meter be fitted?
 * @member {Boolean} newConnectionFl
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct.prototype['newConnectionFl'] = undefined;
/**
 * Is the customer upgrading to a Smart meter?
 * @member {Boolean} upgradeMeteringFl
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct.prototype['upgradeMeteringFl'] = undefined;
/**
 * List of NMIs to associate with a new electricity agreement
 * @member {Array.<module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints>} meterPoints
 */

AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct.prototype['meterPoints'] = undefined;
var _default = AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct;
exports["default"] = _default;