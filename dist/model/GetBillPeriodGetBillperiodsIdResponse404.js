"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillPeriodGetBillperiodsIdResponse404 model module.
 * @module model/GetBillPeriodGetBillperiodsIdResponse404
 * @version 1.61.1
 */
var GetBillPeriodGetBillperiodsIdResponse404 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillPeriodGetBillperiodsIdResponse404</code>.
   * @alias module:model/GetBillPeriodGetBillperiodsIdResponse404
   * @param errorCode {module:model/GetBillPeriodGetBillperiodsIdResponse404.ErrorCodeEnum} Field: * `NotFound` - Could not find 'billPeriod' with Id 'billPeriodId'
   * @param errorSeverity {String} The error severity
   */
  function GetBillPeriodGetBillperiodsIdResponse404(errorCode, errorSeverity) {
    _classCallCheck(this, GetBillPeriodGetBillperiodsIdResponse404);

    GetBillPeriodGetBillperiodsIdResponse404.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillPeriodGetBillperiodsIdResponse404, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>GetBillPeriodGetBillperiodsIdResponse404</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillPeriodGetBillperiodsIdResponse404} obj Optional instance to populate.
     * @return {module:model/GetBillPeriodGetBillperiodsIdResponse404} The populated <code>GetBillPeriodGetBillperiodsIdResponse404</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillPeriodGetBillperiodsIdResponse404();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetBillPeriodGetBillperiodsIdResponse404;
}();
/**
 * Field: * `NotFound` - Could not find 'billPeriod' with Id 'billPeriodId'
 * @member {module:model/GetBillPeriodGetBillperiodsIdResponse404.ErrorCodeEnum} errorCode
 */


GetBillPeriodGetBillperiodsIdResponse404.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

GetBillPeriodGetBillperiodsIdResponse404.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

GetBillPeriodGetBillperiodsIdResponse404.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

GetBillPeriodGetBillperiodsIdResponse404['ErrorCodeEnum'] = {
  /**
   * value: "NotFound"
   * @const
   */
  "NotFound": "NotFound"
};
var _default = GetBillPeriodGetBillperiodsIdResponse404;
exports["default"] = _default;