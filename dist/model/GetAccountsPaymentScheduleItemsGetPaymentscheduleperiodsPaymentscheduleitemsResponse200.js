"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links = _interopRequireDefault(require("./GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200 model module.
 * @module model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200
 * @version 1.61.1
 */
var GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200</code>.
   * @alias module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200
   * @param id {Number} Payment Schedule Item ID
   * @param currency {String} Currency ISO code
   * @param debtRecoveryAmount {Number} The debt recovery amount associated with the bill period
   * @param amount {Number} The amount associated with the bill period
   * @param dueDt {Date} The date the payment schedule item is due
   * @param links {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links} 
   */
  function GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200(id, currency, debtRecoveryAmount, amount, dueDt, links) {
    _classCallCheck(this, GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200);

    GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200.initialize(this, id, currency, debtRecoveryAmount, amount, dueDt, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, currency, debtRecoveryAmount, amount, dueDt, links) {
      obj['id'] = id;
      obj['currency'] = currency;
      obj['debtRecoveryAmount'] = debtRecoveryAmount;
      obj['amount'] = amount;
      obj['dueDt'] = dueDt;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200} The populated <code>GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('debtRecoveryAmount')) {
          obj['debtRecoveryAmount'] = _ApiClient["default"].convertToType(data['debtRecoveryAmount'], 'Number');
        }

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }

        if (data.hasOwnProperty('dueDt')) {
          obj['dueDt'] = _ApiClient["default"].convertToType(data['dueDt'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200;
}();
/**
 * Payment Schedule Item ID
 * @member {Number} id
 */


GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200.prototype['id'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200.prototype['currency'] = undefined;
/**
 * The debt recovery amount associated with the bill period
 * @member {Number} debtRecoveryAmount
 */

GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200.prototype['debtRecoveryAmount'] = undefined;
/**
 * The amount associated with the bill period
 * @member {Number} amount
 */

GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200.prototype['amount'] = undefined;
/**
 * The date the payment schedule item is due
 * @member {Date} dueDt
 */

GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200.prototype['dueDt'] = undefined;
/**
 * @member {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links} links
 */

GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200.prototype['links'] = undefined;
var _default = GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200;
exports["default"] = _default;