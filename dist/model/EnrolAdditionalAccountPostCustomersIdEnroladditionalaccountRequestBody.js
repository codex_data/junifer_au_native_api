"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody model module.
 * @module model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody
 * @version 1.61.1
 */
var EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody</code>.
   * @alias module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody
   * @param senderReference {String} Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected
   * @param reference {String} Unique reference string identifying this particular enrolment
   * @param accountName {String} Name for the new account. Must be different from existing account names for the customer
   * @param supplyAddress {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress} 
   * @param electricityProduct {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct} 
   * @param gasProduct {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct} 
   */
  function EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody(senderReference, reference, accountName, supplyAddress, electricityProduct, gasProduct) {
    _classCallCheck(this, EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody);

    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.initialize(this, senderReference, reference, accountName, supplyAddress, electricityProduct, gasProduct);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, senderReference, reference, accountName, supplyAddress, electricityProduct, gasProduct) {
      obj['senderReference'] = senderReference;
      obj['reference'] = reference;
      obj['accountName'] = accountName;
      obj['supplyAddress'] = supplyAddress;
      obj['electricityProduct'] = electricityProduct;
      obj['gasProduct'] = gasProduct;
    }
    /**
     * Constructs a <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody} obj Optional instance to populate.
     * @return {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody} The populated <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody();

        if (data.hasOwnProperty('senderReference')) {
          obj['senderReference'] = _ApiClient["default"].convertToType(data['senderReference'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('billingEntityCode')) {
          obj['billingEntityCode'] = _ApiClient["default"].convertToType(data['billingEntityCode'], 'String');
        }

        if (data.hasOwnProperty('accountName')) {
          obj['accountName'] = _ApiClient["default"].convertToType(data['accountName'], 'String');
        }

        if (data.hasOwnProperty('submittedSource')) {
          obj['submittedSource'] = _ApiClient["default"].convertToType(data['submittedSource'], 'String');
        }

        if (data.hasOwnProperty('changeOfTenancyFl')) {
          obj['changeOfTenancyFl'] = _ApiClient["default"].convertToType(data['changeOfTenancyFl'], 'Boolean');
        }

        if (data.hasOwnProperty('supplyAddress')) {
          obj['supplyAddress'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress["default"].constructFromObject(data['supplyAddress']);
        }

        if (data.hasOwnProperty('directDebitInfo')) {
          obj['directDebitInfo'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo["default"].constructFromObject(data['directDebitInfo']);
        }

        if (data.hasOwnProperty('electricityProduct')) {
          obj['electricityProduct'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct["default"].constructFromObject(data['electricityProduct']);
        }

        if (data.hasOwnProperty('gasProduct')) {
          obj['gasProduct'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct["default"].constructFromObject(data['gasProduct']);
        }

        if (data.hasOwnProperty('gasEstimate')) {
          obj['gasEstimate'] = _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate["default"].constructFromObject(data['gasEstimate']);
        }

        if (data.hasOwnProperty('electricityEstimate')) {
          obj['electricityEstimate'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate["default"].constructFromObject(data['electricityEstimate']);
        }
      }

      return obj;
    }
  }]);

  return EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody;
}();
/**
 * Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected
 * @member {String} senderReference
 */


EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['senderReference'] = undefined;
/**
 * Unique reference string identifying this particular enrolment
 * @member {String} reference
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['reference'] = undefined;
/**
 * Code of the billing entity to which the account must be attached. If this is empty it will default to the customer billing entity
 * @member {String} billingEntityCode
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['billingEntityCode'] = undefined;
/**
 * Name for the new account. Must be different from existing account names for the customer
 * @member {String} accountName
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['accountName'] = undefined;
/**
 * The source of the enrolment. This overrides the default source (WebService) if set
 * @member {String} submittedSource
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['submittedSource'] = undefined;
/**
 * A boolean flag indicating whether new account is for a change of tenancy.
 * @member {Boolean} changeOfTenancyFl
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['changeOfTenancyFl'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress} supplyAddress
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['supplyAddress'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo} directDebitInfo
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['directDebitInfo'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct} electricityProduct
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['electricityProduct'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct} gasProduct
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['gasProduct'] = undefined;
/**
 * @member {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate} gasEstimate
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['gasEstimate'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate} electricityEstimate
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.prototype['electricityEstimate'] = undefined;
var _default = EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody;
exports["default"] = _default;