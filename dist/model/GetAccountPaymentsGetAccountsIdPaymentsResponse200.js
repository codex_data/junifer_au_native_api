"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountPaymentsGetAccountsIdPaymentsResponse200Links = _interopRequireDefault(require("./GetAccountPaymentsGetAccountsIdPaymentsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountPaymentsGetAccountsIdPaymentsResponse200 model module.
 * @module model/GetAccountPaymentsGetAccountsIdPaymentsResponse200
 * @version 1.61.1
 */
var GetAccountPaymentsGetAccountsIdPaymentsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountPaymentsGetAccountsIdPaymentsResponse200</code>.
   * @alias module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200
   * @param id {Number} The ID of the payment
   * @param amount {Number} The amount of the payment
   * @param currency {String} Currency ISO code
   * @param paymentMethod {String} The payment method
   * @param createdDttm {Date} The date when the payment was created
   * @param requestStatus {String} The status of the associated payment request. Can be one of following: `Pending, Requesting, Successful, Cancelled, FailedPendingRetry, FailedRequestingAgain`
   * @param createdBy {String} The source of the payment. Can be one of the following: `API User, SYSTEM User`
   * @param links {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links} 
   */
  function GetAccountPaymentsGetAccountsIdPaymentsResponse200(id, amount, currency, paymentMethod, createdDttm, requestStatus, createdBy, links) {
    _classCallCheck(this, GetAccountPaymentsGetAccountsIdPaymentsResponse200);

    GetAccountPaymentsGetAccountsIdPaymentsResponse200.initialize(this, id, amount, currency, paymentMethod, createdDttm, requestStatus, createdBy, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountPaymentsGetAccountsIdPaymentsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, amount, currency, paymentMethod, createdDttm, requestStatus, createdBy, links) {
      obj['id'] = id;
      obj['amount'] = amount;
      obj['currency'] = currency;
      obj['paymentMethod'] = paymentMethod;
      obj['createdDttm'] = createdDttm;
      obj['requestStatus'] = requestStatus;
      obj['createdBy'] = createdBy;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountPaymentsGetAccountsIdPaymentsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200} The populated <code>GetAccountPaymentsGetAccountsIdPaymentsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountPaymentsGetAccountsIdPaymentsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('apiIdempotencyKey')) {
          obj['apiIdempotencyKey'] = _ApiClient["default"].convertToType(data['apiIdempotencyKey'], 'String');
        }

        if (data.hasOwnProperty('paymentMethod')) {
          obj['paymentMethod'] = _ApiClient["default"].convertToType(data['paymentMethod'], 'String');
        }

        if (data.hasOwnProperty('acceptedDttm')) {
          obj['acceptedDttm'] = _ApiClient["default"].convertToType(data['acceptedDttm'], 'Date');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('cancelledDttm')) {
          obj['cancelledDttm'] = _ApiClient["default"].convertToType(data['cancelledDttm'], 'Date');
        }

        if (data.hasOwnProperty('postedDt')) {
          obj['postedDt'] = _ApiClient["default"].convertToType(data['postedDt'], 'Date');
        }

        if (data.hasOwnProperty('requestStatus')) {
          obj['requestStatus'] = _ApiClient["default"].convertToType(data['requestStatus'], 'String');
        }

        if (data.hasOwnProperty('createdBy')) {
          obj['createdBy'] = _ApiClient["default"].convertToType(data['createdBy'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountPaymentsGetAccountsIdPaymentsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountPaymentsGetAccountsIdPaymentsResponse200;
}();
/**
 * The ID of the payment
 * @member {Number} id
 */


GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['id'] = undefined;
/**
 * The amount of the payment
 * @member {Number} amount
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['amount'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['currency'] = undefined;
/**
 * Key in UUID format (to prevent duplicate payment requests) if specified
 * @member {String} apiIdempotencyKey
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['apiIdempotencyKey'] = undefined;
/**
 * The payment method
 * @member {String} paymentMethod
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['paymentMethod'] = undefined;
/**
 * The date when the payment has been accepted
 * @member {Date} acceptedDttm
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['acceptedDttm'] = undefined;
/**
 * The date when the payment was created
 * @member {Date} createdDttm
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['createdDttm'] = undefined;
/**
 * The date when the payment was cancelled
 * @member {Date} cancelledDttm
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['cancelledDttm'] = undefined;
/**
 * The date when the payment has been posted
 * @member {Date} postedDt
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['postedDt'] = undefined;
/**
 * The status of the associated payment request. Can be one of following: `Pending, Requesting, Successful, Cancelled, FailedPendingRetry, FailedRequestingAgain`
 * @member {String} requestStatus
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['requestStatus'] = undefined;
/**
 * The source of the payment. Can be one of the following: `API User, SYSTEM User`
 * @member {String} createdBy
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['createdBy'] = undefined;
/**
 * @member {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links} links
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200.prototype['links'] = undefined;
var _default = GetAccountPaymentsGetAccountsIdPaymentsResponse200;
exports["default"] = _default;