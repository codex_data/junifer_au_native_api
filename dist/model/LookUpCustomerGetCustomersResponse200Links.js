"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The LookUpCustomerGetCustomersResponse200Links model module.
 * @module model/LookUpCustomerGetCustomersResponse200Links
 * @version 1.61.1
 */
var LookUpCustomerGetCustomersResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>LookUpCustomerGetCustomersResponse200Links</code>.
   * Links to related resources
   * @alias module:model/LookUpCustomerGetCustomersResponse200Links
   * @param self {String} Link referring to this customer
   * @param accounts {String} Link to customer's accounts
   * @param securityQuestions {String} Link to customers security questions and answers
   * @param billingEntities {String} Link to customer's billing entities
   */
  function LookUpCustomerGetCustomersResponse200Links(self, accounts, securityQuestions, billingEntities) {
    _classCallCheck(this, LookUpCustomerGetCustomersResponse200Links);

    LookUpCustomerGetCustomersResponse200Links.initialize(this, self, accounts, securityQuestions, billingEntities);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(LookUpCustomerGetCustomersResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, accounts, securityQuestions, billingEntities) {
      obj['self'] = self;
      obj['accounts'] = accounts;
      obj['securityQuestions'] = securityQuestions;
      obj['billingEntities'] = billingEntities;
    }
    /**
     * Constructs a <code>LookUpCustomerGetCustomersResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LookUpCustomerGetCustomersResponse200Links} obj Optional instance to populate.
     * @return {module:model/LookUpCustomerGetCustomersResponse200Links} The populated <code>LookUpCustomerGetCustomersResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new LookUpCustomerGetCustomersResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('accounts')) {
          obj['accounts'] = _ApiClient["default"].convertToType(data['accounts'], 'String');
        }

        if (data.hasOwnProperty('securityQuestions')) {
          obj['securityQuestions'] = _ApiClient["default"].convertToType(data['securityQuestions'], 'String');
        }

        if (data.hasOwnProperty('billingEntities')) {
          obj['billingEntities'] = _ApiClient["default"].convertToType(data['billingEntities'], 'String');
        }
      }

      return obj;
    }
  }]);

  return LookUpCustomerGetCustomersResponse200Links;
}();
/**
 * Link referring to this customer
 * @member {String} self
 */


LookUpCustomerGetCustomersResponse200Links.prototype['self'] = undefined;
/**
 * Link to customer's accounts
 * @member {String} accounts
 */

LookUpCustomerGetCustomersResponse200Links.prototype['accounts'] = undefined;
/**
 * Link to customers security questions and answers
 * @member {String} securityQuestions
 */

LookUpCustomerGetCustomersResponse200Links.prototype['securityQuestions'] = undefined;
/**
 * Link to customer's billing entities
 * @member {String} billingEntities
 */

LookUpCustomerGetCustomersResponse200Links.prototype['billingEntities'] = undefined;
var _default = LookUpCustomerGetCustomersResponse200Links;
exports["default"] = _default;