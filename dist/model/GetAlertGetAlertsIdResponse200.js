"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAlertGetAlertsIdResponse200Links = _interopRequireDefault(require("./GetAlertGetAlertsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAlertGetAlertsIdResponse200 model module.
 * @module model/GetAlertGetAlertsIdResponse200
 * @version 1.61.1
 */
var GetAlertGetAlertsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAlertGetAlertsIdResponse200</code>.
   * @alias module:model/GetAlertGetAlertsIdResponse200
   * @param id {Number} Alert ID
   * @param group {String} Alert group
   * @param code {String} Alert code
   * @param summary {String} Alert summary
   * @param key {String} Unique alert key
   * @param severity {String} Alert severity
   * @param links {module:model/GetAlertGetAlertsIdResponse200Links} 
   */
  function GetAlertGetAlertsIdResponse200(id, group, code, summary, key, severity, links) {
    _classCallCheck(this, GetAlertGetAlertsIdResponse200);

    GetAlertGetAlertsIdResponse200.initialize(this, id, group, code, summary, key, severity, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAlertGetAlertsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, group, code, summary, key, severity, links) {
      obj['id'] = id;
      obj['group'] = group;
      obj['code'] = code;
      obj['summary'] = summary;
      obj['key'] = key;
      obj['severity'] = severity;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAlertGetAlertsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAlertGetAlertsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetAlertGetAlertsIdResponse200} The populated <code>GetAlertGetAlertsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAlertGetAlertsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('group')) {
          obj['group'] = _ApiClient["default"].convertToType(data['group'], 'String');
        }

        if (data.hasOwnProperty('code')) {
          obj['code'] = _ApiClient["default"].convertToType(data['code'], 'String');
        }

        if (data.hasOwnProperty('summary')) {
          obj['summary'] = _ApiClient["default"].convertToType(data['summary'], 'String');
        }

        if (data.hasOwnProperty('key')) {
          obj['key'] = _ApiClient["default"].convertToType(data['key'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('severity')) {
          obj['severity'] = _ApiClient["default"].convertToType(data['severity'], 'String');
        }

        if (data.hasOwnProperty('firstRaisedDttm')) {
          obj['firstRaisedDttm'] = _ApiClient["default"].convertToType(data['firstRaisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('lastRaisedDttm')) {
          obj['lastRaisedDttm'] = _ApiClient["default"].convertToType(data['lastRaisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('raisedCount')) {
          obj['raisedCount'] = _ApiClient["default"].convertToType(data['raisedCount'], 'Number');
        }

        if (data.hasOwnProperty('clearedDttm')) {
          obj['clearedDttm'] = _ApiClient["default"].convertToType(data['clearedDttm'], 'Date');
        }

        if (data.hasOwnProperty('clearedBy')) {
          obj['clearedBy'] = _ApiClient["default"].convertToType(data['clearedBy'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAlertGetAlertsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAlertGetAlertsIdResponse200;
}();
/**
 * Alert ID
 * @member {Number} id
 */


GetAlertGetAlertsIdResponse200.prototype['id'] = undefined;
/**
 * Alert group
 * @member {String} group
 */

GetAlertGetAlertsIdResponse200.prototype['group'] = undefined;
/**
 * Alert code
 * @member {String} code
 */

GetAlertGetAlertsIdResponse200.prototype['code'] = undefined;
/**
 * Alert summary
 * @member {String} summary
 */

GetAlertGetAlertsIdResponse200.prototype['summary'] = undefined;
/**
 * Unique alert key
 * @member {String} key
 */

GetAlertGetAlertsIdResponse200.prototype['key'] = undefined;
/**
 * Alert description
 * @member {String} description
 */

GetAlertGetAlertsIdResponse200.prototype['description'] = undefined;
/**
 * Alert severity
 * @member {String} severity
 */

GetAlertGetAlertsIdResponse200.prototype['severity'] = undefined;
/**
 * The date and time this alert was first raised (if not cleared)
 * @member {Date} firstRaisedDttm
 */

GetAlertGetAlertsIdResponse200.prototype['firstRaisedDttm'] = undefined;
/**
 * The date and time this alert was last raised (if not cleared)
 * @member {Date} lastRaisedDttm
 */

GetAlertGetAlertsIdResponse200.prototype['lastRaisedDttm'] = undefined;
/**
 * The number of times this alert has been raised (if not cleared)
 * @member {Number} raisedCount
 */

GetAlertGetAlertsIdResponse200.prototype['raisedCount'] = undefined;
/**
 * The date and time this alert was cleared (if cleared)
 * @member {Date} clearedDttm
 */

GetAlertGetAlertsIdResponse200.prototype['clearedDttm'] = undefined;
/**
 * The Junifer user that cleared this alert (if cleared)
 * @member {String} clearedBy
 */

GetAlertGetAlertsIdResponse200.prototype['clearedBy'] = undefined;
/**
 * @member {module:model/GetAlertGetAlertsIdResponse200Links} links
 */

GetAlertGetAlertsIdResponse200.prototype['links'] = undefined;
var _default = GetAlertGetAlertsIdResponse200;
exports["default"] = _default;