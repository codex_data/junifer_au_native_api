"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetStripePaymentCardGetStripepaymentcardsIdResponse200Links = _interopRequireDefault(require("./GetStripePaymentCardGetStripepaymentcardsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetStripePaymentCardGetStripepaymentcardsIdResponse200 model module.
 * @module model/GetStripePaymentCardGetStripepaymentcardsIdResponse200
 * @version 1.61.1
 */
var GetStripePaymentCardGetStripepaymentcardsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetStripePaymentCardGetStripepaymentcardsIdResponse200</code>.
   * @alias module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200
   * @param id {Number} The ID of the Stripe Payment Card
   * @param createdDttm {Date} The date and time this Stripe Payment Card was created
   * @param stripeCusId {String} The Stripe customer id of the associated customer
   * @param links {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200Links} 
   */
  function GetStripePaymentCardGetStripepaymentcardsIdResponse200(id, createdDttm, stripeCusId, links) {
    _classCallCheck(this, GetStripePaymentCardGetStripepaymentcardsIdResponse200);

    GetStripePaymentCardGetStripepaymentcardsIdResponse200.initialize(this, id, createdDttm, stripeCusId, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetStripePaymentCardGetStripepaymentcardsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, createdDttm, stripeCusId, links) {
      obj['id'] = id;
      obj['createdDttm'] = createdDttm;
      obj['stripeCusId'] = stripeCusId;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetStripePaymentCardGetStripepaymentcardsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200} The populated <code>GetStripePaymentCardGetStripepaymentcardsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetStripePaymentCardGetStripepaymentcardsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('expiryDt')) {
          obj['expiryDt'] = _ApiClient["default"].convertToType(data['expiryDt'], 'Date');
        }

        if (data.hasOwnProperty('lastFourDigits')) {
          obj['lastFourDigits'] = _ApiClient["default"].convertToType(data['lastFourDigits'], 'Number');
        }

        if (data.hasOwnProperty('stripeCusId')) {
          obj['stripeCusId'] = _ApiClient["default"].convertToType(data['stripeCusId'], 'String');
        }

        if (data.hasOwnProperty('stripePaymentMethodId')) {
          obj['stripePaymentMethodId'] = _ApiClient["default"].convertToType(data['stripePaymentMethodId'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetStripePaymentCardGetStripepaymentcardsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetStripePaymentCardGetStripepaymentcardsIdResponse200;
}();
/**
 * The ID of the Stripe Payment Card
 * @member {Number} id
 */


GetStripePaymentCardGetStripepaymentcardsIdResponse200.prototype['id'] = undefined;
/**
 * The date and time this Stripe Payment Card was created
 * @member {Date} createdDttm
 */

GetStripePaymentCardGetStripepaymentcardsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * (deprecated) Date when the payment method expires
 * @member {Date} expiryDt
 */

GetStripePaymentCardGetStripepaymentcardsIdResponse200.prototype['expiryDt'] = undefined;
/**
 * (deprecated) Last four digits of the Stripe Payment Card number
 * @member {Number} lastFourDigits
 */

GetStripePaymentCardGetStripepaymentcardsIdResponse200.prototype['lastFourDigits'] = undefined;
/**
 * The Stripe customer id of the associated customer
 * @member {String} stripeCusId
 */

GetStripePaymentCardGetStripepaymentcardsIdResponse200.prototype['stripeCusId'] = undefined;
/**
 * the the Stripe payment method id of the associated customer
 * @member {String} stripePaymentMethodId
 */

GetStripePaymentCardGetStripepaymentcardsIdResponse200.prototype['stripePaymentMethodId'] = undefined;
/**
 * @member {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200Links} links
 */

GetStripePaymentCardGetStripepaymentcardsIdResponse200.prototype['links'] = undefined;
var _default = GetStripePaymentCardGetStripepaymentcardsIdResponse200;
exports["default"] = _default;