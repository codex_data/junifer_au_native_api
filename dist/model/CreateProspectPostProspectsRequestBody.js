"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateProspectPostProspectsRequestBodyCustomer = _interopRequireDefault(require("./CreateProspectPostProspectsRequestBodyCustomer"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateProspectPostProspectsRequestBody model module.
 * @module model/CreateProspectPostProspectsRequestBody
 * @version 1.61.1
 */
var CreateProspectPostProspectsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProspectPostProspectsRequestBody</code>.
   * @alias module:model/CreateProspectPostProspectsRequestBody
   * @param brokerId {Number} Broker ID
   * @param customer {module:model/CreateProspectPostProspectsRequestBodyCustomer} 
   */
  function CreateProspectPostProspectsRequestBody(brokerId, customer) {
    _classCallCheck(this, CreateProspectPostProspectsRequestBody);

    CreateProspectPostProspectsRequestBody.initialize(this, brokerId, customer);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProspectPostProspectsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, brokerId, customer) {
      obj['brokerId'] = brokerId;
      obj['customer'] = customer;
    }
    /**
     * Constructs a <code>CreateProspectPostProspectsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProspectPostProspectsRequestBody} obj Optional instance to populate.
     * @return {module:model/CreateProspectPostProspectsRequestBody} The populated <code>CreateProspectPostProspectsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProspectPostProspectsRequestBody();

        if (data.hasOwnProperty('brokerId')) {
          obj['brokerId'] = _ApiClient["default"].convertToType(data['brokerId'], 'Number');
        }

        if (data.hasOwnProperty('customer')) {
          obj['customer'] = _CreateProspectPostProspectsRequestBodyCustomer["default"].constructFromObject(data['customer']);
        }
      }

      return obj;
    }
  }]);

  return CreateProspectPostProspectsRequestBody;
}();
/**
 * Broker ID
 * @member {Number} brokerId
 */


CreateProspectPostProspectsRequestBody.prototype['brokerId'] = undefined;
/**
 * @member {module:model/CreateProspectPostProspectsRequestBodyCustomer} customer
 */

CreateProspectPostProspectsRequestBody.prototype['customer'] = undefined;
var _default = CreateProspectPostProspectsRequestBody;
exports["default"] = _default;