"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities model module.
 * @module model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities
 * @version 1.61.1
 */
var CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities</code>.
   * Ticket entities related with the ticket
   * @alias module:model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities
   */
  function CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities() {
    _classCallCheck(this, CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities);

    CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities} obj Optional instance to populate.
     * @return {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities} The populated <code>CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities();

        if (data.hasOwnProperty('accounts')) {
          obj['accounts'] = _ApiClient["default"].convertToType(data['accounts'], [Object]);
        }

        if (data.hasOwnProperty('customers')) {
          obj['customers'] = _ApiClient["default"].convertToType(data['customers'], [Object]);
        }

        if (data.hasOwnProperty('tickets')) {
          obj['tickets'] = _ApiClient["default"].convertToType(data['tickets'], [Object]);
        }

        if (data.hasOwnProperty('communications')) {
          obj['communications'] = _ApiClient["default"].convertToType(data['communications'], [Object]);
        }

        if (data.hasOwnProperty('bills')) {
          obj['bills'] = _ApiClient["default"].convertToType(data['bills'], [Object]);
        }

        if (data.hasOwnProperty('paymentRequests')) {
          obj['paymentRequests'] = _ApiClient["default"].convertToType(data['paymentRequests'], [Object]);
        }

        if (data.hasOwnProperty('paymentSchedulePeriods')) {
          obj['paymentSchedulePeriods'] = _ApiClient["default"].convertToType(data['paymentSchedulePeriods'], [Object]);
        }

        if (data.hasOwnProperty('paymentMethods')) {
          obj['paymentMethods'] = _ApiClient["default"].convertToType(data['paymentMethods'], [Object]);
        }

        if (data.hasOwnProperty('alerts')) {
          obj['alerts'] = _ApiClient["default"].convertToType(data['alerts'], [Object]);
        }

        if (data.hasOwnProperty('billPeriods')) {
          obj['billPeriods'] = _ApiClient["default"].convertToType(data['billPeriods'], [Object]);
        }

        if (data.hasOwnProperty('billingEntities')) {
          obj['billingEntities'] = _ApiClient["default"].convertToType(data['billingEntities'], [Object]);
        }

        if (data.hasOwnProperty('billEmails')) {
          obj['billEmails'] = _ApiClient["default"].convertToType(data['billEmails'], [Object]);
        }

        if (data.hasOwnProperty('changeOfTenancys')) {
          obj['changeOfTenancys'] = _ApiClient["default"].convertToType(data['changeOfTenancys'], [Object]);
        }

        if (data.hasOwnProperty('assets')) {
          obj['assets'] = _ApiClient["default"].convertToType(data['assets'], [Object]);
        }

        if (data.hasOwnProperty('contacts')) {
          obj['contacts'] = _ApiClient["default"].convertToType(data['contacts'], [Object]);
        }
      }

      return obj;
    }
  }]);

  return CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities;
}();
/**
 * The account(s) that the ticket is associated with
 * @member {Array.<Object>} accounts
 */


CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['accounts'] = undefined;
/**
 * The customer(s) that the ticket is associated with
 * @member {Array.<Object>} customers
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['customers'] = undefined;
/**
 * The ticket(s) that the ticket is associated with
 * @member {Array.<Object>} tickets
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['tickets'] = undefined;
/**
 * The communication(s) that the ticket is associated with
 * @member {Array.<Object>} communications
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['communications'] = undefined;
/**
 * The bill(s) that the ticket is associated with
 * @member {Array.<Object>} bills
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['bills'] = undefined;
/**
 * The payment request(s) that the ticket is associated with
 * @member {Array.<Object>} paymentRequests
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['paymentRequests'] = undefined;
/**
 * The payment schedule period(s) that the ticket is associated with
 * @member {Array.<Object>} paymentSchedulePeriods
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['paymentSchedulePeriods'] = undefined;
/**
 * The payment method(s) that the ticket is associated with
 * @member {Array.<Object>} paymentMethods
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['paymentMethods'] = undefined;
/**
 * The alert(s) that the ticket is associated with
 * @member {Array.<Object>} alerts
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['alerts'] = undefined;
/**
 * The bill period(s) that the ticket is associated with
 * @member {Array.<Object>} billPeriods
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['billPeriods'] = undefined;
/**
 * The billing entities that the ticket is associated with
 * @member {Array.<Object>} billingEntities
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['billingEntities'] = undefined;
/**
 * The bill email(s) that the ticket is associated with
 * @member {Array.<Object>} billEmails
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['billEmails'] = undefined;
/**
 * The change of tenancies that the ticket is associated with
 * @member {Array.<Object>} changeOfTenancys
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['changeOfTenancys'] = undefined;
/**
 * The assets/meterpoints that the ticket is associated with
 * @member {Array.<Object>} assets
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['assets'] = undefined;
/**
 * The contacts that the ticket is associated with
 * @member {Array.<Object>} contacts
 */

CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.prototype['contacts'] = undefined;
var _default = CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities;
exports["default"] = _default;