"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _ReversionBillPostBillsIdReversionResponse200Links = _interopRequireDefault(require("./ReversionBillPostBillsIdReversionResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The ReversionBillPostBillsIdReversionResponse200 model module.
 * @module model/ReversionBillPostBillsIdReversionResponse200
 * @version 1.61.1
 */
var ReversionBillPostBillsIdReversionResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ReversionBillPostBillsIdReversionResponse200</code>.
   * @alias module:model/ReversionBillPostBillsIdReversionResponse200
   * @param newVersionBillRequestId {Number} Id of bill request for reversioned bill
   * @param links {module:model/ReversionBillPostBillsIdReversionResponse200Links} 
   */
  function ReversionBillPostBillsIdReversionResponse200(newVersionBillRequestId, links) {
    _classCallCheck(this, ReversionBillPostBillsIdReversionResponse200);

    ReversionBillPostBillsIdReversionResponse200.initialize(this, newVersionBillRequestId, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ReversionBillPostBillsIdReversionResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, newVersionBillRequestId, links) {
      obj['newVersionBillRequestId'] = newVersionBillRequestId;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>ReversionBillPostBillsIdReversionResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ReversionBillPostBillsIdReversionResponse200} obj Optional instance to populate.
     * @return {module:model/ReversionBillPostBillsIdReversionResponse200} The populated <code>ReversionBillPostBillsIdReversionResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ReversionBillPostBillsIdReversionResponse200();

        if (data.hasOwnProperty('newVersionBillRequestId')) {
          obj['newVersionBillRequestId'] = _ApiClient["default"].convertToType(data['newVersionBillRequestId'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _ReversionBillPostBillsIdReversionResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return ReversionBillPostBillsIdReversionResponse200;
}();
/**
 * Id of bill request for reversioned bill
 * @member {Number} newVersionBillRequestId
 */


ReversionBillPostBillsIdReversionResponse200.prototype['newVersionBillRequestId'] = undefined;
/**
 * @member {module:model/ReversionBillPostBillsIdReversionResponse200Links} links
 */

ReversionBillPostBillsIdReversionResponse200.prototype['links'] = undefined;
var _default = ReversionBillPostBillsIdReversionResponse200;
exports["default"] = _default;