"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody model module.
 * @module model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody
 * @version 1.61.1
 */
var CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody</code>.
   * @alias module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody
   * @param fromDttm {Date} Account review period start date time
   * @param reason {String} Reason for the created review period. Must be the name of an account review reason as shown in the Account Review Reason ref table
   */
  function CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody(fromDttm, reason) {
    _classCallCheck(this, CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody);

    CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.initialize(this, fromDttm, reason);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, fromDttm, reason) {
      obj['fromDttm'] = fromDttm;
      obj['reason'] = reason;
    }
    /**
     * Constructs a <code>CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody} obj Optional instance to populate.
     * @return {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody} The populated <code>CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody();

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('reason')) {
          obj['reason'] = _ApiClient["default"].convertToType(data['reason'], 'String');
        }

        if (data.hasOwnProperty('suppressDunningFl')) {
          obj['suppressDunningFl'] = _ApiClient["default"].convertToType(data['suppressDunningFl'], 'Boolean');
        }

        if (data.hasOwnProperty('suppressBillingFl')) {
          obj['suppressBillingFl'] = _ApiClient["default"].convertToType(data['suppressBillingFl'], 'Boolean');
        }

        if (data.hasOwnProperty('suppressPaymentCollectionFl')) {
          obj['suppressPaymentCollectionFl'] = _ApiClient["default"].convertToType(data['suppressPaymentCollectionFl'], 'Boolean');
        }

        if (data.hasOwnProperty('suppressPaymentReviewFl')) {
          obj['suppressPaymentReviewFl'] = _ApiClient["default"].convertToType(data['suppressPaymentReviewFl'], 'Boolean');
        }

        if (data.hasOwnProperty('propertySuppressionFl')) {
          obj['propertySuppressionFl'] = _ApiClient["default"].convertToType(data['propertySuppressionFl'], 'Boolean');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody;
}();
/**
 * Account review period start date time
 * @member {Date} fromDttm
 */


CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.prototype['fromDttm'] = undefined;
/**
 * Account review period end date time
 * @member {Date} toDttm
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.prototype['toDttm'] = undefined;
/**
 * Reason for the created review period. Must be the name of an account review reason as shown in the Account Review Reason ref table
 * @member {String} reason
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.prototype['reason'] = undefined;
/**
 * A flag indicating whether dunning should be suppressed
 * @member {Boolean} suppressDunningFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.prototype['suppressDunningFl'] = undefined;
/**
 * A flag indicating whether billing should be suppressed
 * @member {Boolean} suppressBillingFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.prototype['suppressBillingFl'] = undefined;
/**
 * A flag indicating whether payment collection should be suppressed
 * @member {Boolean} suppressPaymentCollectionFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.prototype['suppressPaymentCollectionFl'] = undefined;
/**
 * A flag indicating whether payment review should be suppressed
 * @member {Boolean} suppressPaymentReviewFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.prototype['suppressPaymentReviewFl'] = undefined;
/**
 * Property-specific billing suppression
 * @member {Boolean} propertySuppressionFl
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.prototype['propertySuppressionFl'] = undefined;
/**
 * Free text description
 * @member {String} description
 */

CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.prototype['description'] = undefined;
var _default = CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody;
exports["default"] = _default;