"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetCreditFileGetCreditfilesIdResponse200Links = _interopRequireDefault(require("./GetCreditFileGetCreditfilesIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCreditFileGetCreditfilesIdResponse200 model module.
 * @module model/GetCreditFileGetCreditfilesIdResponse200
 * @version 1.61.1
 */
var GetCreditFileGetCreditfilesIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCreditFileGetCreditfilesIdResponse200</code>.
   * @alias module:model/GetCreditFileGetCreditfilesIdResponse200
   * @param id {Number} Credit File Id
   * @param display {String} Can be one of `PDF`
   * @param viewable {Boolean} Available to download
   * @param links {module:model/GetCreditFileGetCreditfilesIdResponse200Links} 
   */
  function GetCreditFileGetCreditfilesIdResponse200(id, display, viewable, links) {
    _classCallCheck(this, GetCreditFileGetCreditfilesIdResponse200);

    GetCreditFileGetCreditfilesIdResponse200.initialize(this, id, display, viewable, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCreditFileGetCreditfilesIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, display, viewable, links) {
      obj['id'] = id;
      obj['display'] = display;
      obj['viewable'] = viewable;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetCreditFileGetCreditfilesIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCreditFileGetCreditfilesIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetCreditFileGetCreditfilesIdResponse200} The populated <code>GetCreditFileGetCreditfilesIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCreditFileGetCreditfilesIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('display')) {
          obj['display'] = _ApiClient["default"].convertToType(data['display'], 'String');
        }

        if (data.hasOwnProperty('viewable')) {
          obj['viewable'] = _ApiClient["default"].convertToType(data['viewable'], 'Boolean');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetCreditFileGetCreditfilesIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetCreditFileGetCreditfilesIdResponse200;
}();
/**
 * Credit File Id
 * @member {Number} id
 */


GetCreditFileGetCreditfilesIdResponse200.prototype['id'] = undefined;
/**
 * Can be one of `PDF`
 * @member {String} display
 */

GetCreditFileGetCreditfilesIdResponse200.prototype['display'] = undefined;
/**
 * Available to download
 * @member {Boolean} viewable
 */

GetCreditFileGetCreditfilesIdResponse200.prototype['viewable'] = undefined;
/**
 * @member {module:model/GetCreditFileGetCreditfilesIdResponse200Links} links
 */

GetCreditFileGetCreditfilesIdResponse200.prototype['links'] = undefined;
var _default = GetCreditFileGetCreditfilesIdResponse200;
exports["default"] = _default;