"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody model module.
 * @module model/NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody
 * @version 1.61.1
 */
var NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody</code>.
   * @alias module:model/NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody
   * @param mandateReference {String} BPOINT Direct Debit mandate reference
   * @param paymentType {String} Payment type. Can be either `BankAccount` or `CreditCard`
   */
  function NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody(mandateReference, paymentType) {
    _classCallCheck(this, NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody);

    NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody.initialize(this, mandateReference, paymentType);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, mandateReference, paymentType) {
      obj['mandateReference'] = mandateReference;
      obj['paymentType'] = paymentType;
    }
    /**
     * Constructs a <code>NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody} obj Optional instance to populate.
     * @return {module:model/NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody} The populated <code>NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody();

        if (data.hasOwnProperty('mandateReference')) {
          obj['mandateReference'] = _ApiClient["default"].convertToType(data['mandateReference'], 'String');
        }

        if (data.hasOwnProperty('paymentType')) {
          obj['paymentType'] = _ApiClient["default"].convertToType(data['paymentType'], 'String');
        }
      }

      return obj;
    }
  }]);

  return NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody;
}();
/**
 * BPOINT Direct Debit mandate reference
 * @member {String} mandateReference
 */


NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody.prototype['mandateReference'] = undefined;
/**
 * Payment type. Can be either `BankAccount` or `CreditCard`
 * @member {String} paymentType
 */

NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody.prototype['paymentType'] = undefined;
var _default = NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody;
exports["default"] = _default;