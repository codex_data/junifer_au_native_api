"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months model module.
 * @module model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months
 * @version 1.61.1
 */
var GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months</code>.
   * Breakdown of high usage season by month
   * @alias module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months
   * @param _1 {Boolean} Indicates if January is a high usage month
   * @param _2 {Boolean} Indicates if February is a high usage month
   * @param _3 {Boolean} Indicates if March is a high usage month
   * @param _4 {Boolean} Indicates if April is a high usage month
   * @param _5 {Boolean} Indicates if May is a high usage month
   * @param _6 {Boolean} Indicates if June is a high usage month
   * @param _7 {Boolean} Indicates if July is a high usage month
   * @param _8 {Boolean} Indicates if August is a high usage month
   * @param _9 {Boolean} Indicates if September is a high usage month
   * @param _10 {Boolean} Indicates if October is a high usage month
   * @param _11 {Boolean} Indicates if November is a high usage month
   * @param _12 {Boolean} Indicates if December is a high usage month
   */
  function GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12) {
    _classCallCheck(this, GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months);

    GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.initialize(this, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months, null, [{
    key: "initialize",
    value: function initialize(obj, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12) {
      obj['1'] = _1;
      obj['2'] = _2;
      obj['3'] = _3;
      obj['4'] = _4;
      obj['5'] = _5;
      obj['6'] = _6;
      obj['7'] = _7;
      obj['8'] = _8;
      obj['9'] = _9;
      obj['10'] = _10;
      obj['11'] = _11;
      obj['12'] = _12;
    }
    /**
     * Constructs a <code>GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months} obj Optional instance to populate.
     * @return {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months} The populated <code>GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months();

        if (data.hasOwnProperty('1')) {
          obj['1'] = _ApiClient["default"].convertToType(data['1'], 'Boolean');
        }

        if (data.hasOwnProperty('2')) {
          obj['2'] = _ApiClient["default"].convertToType(data['2'], 'Boolean');
        }

        if (data.hasOwnProperty('3')) {
          obj['3'] = _ApiClient["default"].convertToType(data['3'], 'Boolean');
        }

        if (data.hasOwnProperty('4')) {
          obj['4'] = _ApiClient["default"].convertToType(data['4'], 'Boolean');
        }

        if (data.hasOwnProperty('5')) {
          obj['5'] = _ApiClient["default"].convertToType(data['5'], 'Boolean');
        }

        if (data.hasOwnProperty('6')) {
          obj['6'] = _ApiClient["default"].convertToType(data['6'], 'Boolean');
        }

        if (data.hasOwnProperty('7')) {
          obj['7'] = _ApiClient["default"].convertToType(data['7'], 'Boolean');
        }

        if (data.hasOwnProperty('8')) {
          obj['8'] = _ApiClient["default"].convertToType(data['8'], 'Boolean');
        }

        if (data.hasOwnProperty('9')) {
          obj['9'] = _ApiClient["default"].convertToType(data['9'], 'Boolean');
        }

        if (data.hasOwnProperty('10')) {
          obj['10'] = _ApiClient["default"].convertToType(data['10'], 'Boolean');
        }

        if (data.hasOwnProperty('11')) {
          obj['11'] = _ApiClient["default"].convertToType(data['11'], 'Boolean');
        }

        if (data.hasOwnProperty('12')) {
          obj['12'] = _ApiClient["default"].convertToType(data['12'], 'Boolean');
        }
      }

      return obj;
    }
  }]);

  return GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months;
}();
/**
 * Indicates if January is a high usage month
 * @member {Boolean} 1
 */


GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['1'] = undefined;
/**
 * Indicates if February is a high usage month
 * @member {Boolean} 2
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['2'] = undefined;
/**
 * Indicates if March is a high usage month
 * @member {Boolean} 3
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['3'] = undefined;
/**
 * Indicates if April is a high usage month
 * @member {Boolean} 4
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['4'] = undefined;
/**
 * Indicates if May is a high usage month
 * @member {Boolean} 5
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['5'] = undefined;
/**
 * Indicates if June is a high usage month
 * @member {Boolean} 6
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['6'] = undefined;
/**
 * Indicates if July is a high usage month
 * @member {Boolean} 7
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['7'] = undefined;
/**
 * Indicates if August is a high usage month
 * @member {Boolean} 8
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['8'] = undefined;
/**
 * Indicates if September is a high usage month
 * @member {Boolean} 9
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['9'] = undefined;
/**
 * Indicates if October is a high usage month
 * @member {Boolean} 10
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['10'] = undefined;
/**
 * Indicates if November is a high usage month
 * @member {Boolean} 11
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['11'] = undefined;
/**
 * Indicates if December is a high usage month
 * @member {Boolean} 12
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.prototype['12'] = undefined;
var _default = GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months;
exports["default"] = _default;