"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountNotePostAccountsIdNoteResponse200 model module.
 * @module model/CreateAccountNotePostAccountsIdNoteResponse200
 * @version 1.61.1
 */
var CreateAccountNotePostAccountsIdNoteResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountNotePostAccountsIdNoteResponse200</code>.
   * @alias module:model/CreateAccountNotePostAccountsIdNoteResponse200
   * @param id {String} The new Note ID
   * @param createdDttm {Date} Note creation date and time
   * @param subject {String} Note Subject
   * @param type {String} Note Type. Can be one of the following: `Note, Email, File, Phone, Letter`
   * @param summary {String} A brief summary of the Note
   */
  function CreateAccountNotePostAccountsIdNoteResponse200(id, createdDttm, subject, type, summary) {
    _classCallCheck(this, CreateAccountNotePostAccountsIdNoteResponse200);

    CreateAccountNotePostAccountsIdNoteResponse200.initialize(this, id, createdDttm, subject, type, summary);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountNotePostAccountsIdNoteResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, createdDttm, subject, type, summary) {
      obj['id'] = id;
      obj['createdDttm'] = createdDttm;
      obj['subject'] = subject;
      obj['type'] = type;
      obj['summary'] = summary;
    }
    /**
     * Constructs a <code>CreateAccountNotePostAccountsIdNoteResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountNotePostAccountsIdNoteResponse200} obj Optional instance to populate.
     * @return {module:model/CreateAccountNotePostAccountsIdNoteResponse200} The populated <code>CreateAccountNotePostAccountsIdNoteResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountNotePostAccountsIdNoteResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('subject')) {
          obj['subject'] = _ApiClient["default"].convertToType(data['subject'], 'String');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('summary')) {
          obj['summary'] = _ApiClient["default"].convertToType(data['summary'], 'String');
        }

        if (data.hasOwnProperty('content')) {
          obj['content'] = _ApiClient["default"].convertToType(data['content'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountNotePostAccountsIdNoteResponse200;
}();
/**
 * The new Note ID
 * @member {String} id
 */


CreateAccountNotePostAccountsIdNoteResponse200.prototype['id'] = undefined;
/**
 * Note creation date and time
 * @member {Date} createdDttm
 */

CreateAccountNotePostAccountsIdNoteResponse200.prototype['createdDttm'] = undefined;
/**
 * Note Subject
 * @member {String} subject
 */

CreateAccountNotePostAccountsIdNoteResponse200.prototype['subject'] = undefined;
/**
 * Note Type. Can be one of the following: `Note, Email, File, Phone, Letter`
 * @member {String} type
 */

CreateAccountNotePostAccountsIdNoteResponse200.prototype['type'] = undefined;
/**
 * A brief summary of the Note
 * @member {String} summary
 */

CreateAccountNotePostAccountsIdNoteResponse200.prototype['summary'] = undefined;
/**
 * The detailed content of the Note
 * @member {String} content
 */

CreateAccountNotePostAccountsIdNoteResponse200.prototype['content'] = undefined;
var _default = CreateAccountNotePostAccountsIdNoteResponse200;
exports["default"] = _default;