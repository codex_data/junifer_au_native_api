"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links = _interopRequireDefault(require("./GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200 model module.
 * @module model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200
 * @version 1.61.1
 */
var GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200</code>.
   * @alias module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200
   * @param id {Number} BrokerLinkages ID
   * @param fromDt {Date} The date from which the customer/prospect's consent to be managed is valid
   * @param toDt {Date} The date to which the customer/prospect's consent to be managed is valid
   * @param quotingPermissions {Boolean} Indicates whether the customer/prospect has Quoting Permissions
   * @param billingPermissions {Boolean} Indicates whether the customer/prospect has Billing Permissions
   * @param acceptQuotingPermissions {Boolean} Indicates whether the customer/prospect has Accept Quoting Permissions
   * @param links {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links} 
   */
  function GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200(id, fromDt, toDt, quotingPermissions, billingPermissions, acceptQuotingPermissions, links) {
    _classCallCheck(this, GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200);

    GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.initialize(this, id, fromDt, toDt, quotingPermissions, billingPermissions, acceptQuotingPermissions, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, fromDt, toDt, quotingPermissions, billingPermissions, acceptQuotingPermissions, links) {
      obj['id'] = id;
      obj['fromDt'] = fromDt;
      obj['toDt'] = toDt;
      obj['quotingPermissions'] = quotingPermissions;
      obj['billingPermissions'] = billingPermissions;
      obj['acceptQuotingPermissions'] = acceptQuotingPermissions;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200} obj Optional instance to populate.
     * @return {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200} The populated <code>GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }

        if (data.hasOwnProperty('quotingPermissions')) {
          obj['quotingPermissions'] = _ApiClient["default"].convertToType(data['quotingPermissions'], 'Boolean');
        }

        if (data.hasOwnProperty('billingPermissions')) {
          obj['billingPermissions'] = _ApiClient["default"].convertToType(data['billingPermissions'], 'Boolean');
        }

        if (data.hasOwnProperty('acceptQuotingPermissions')) {
          obj['acceptQuotingPermissions'] = _ApiClient["default"].convertToType(data['acceptQuotingPermissions'], 'Boolean');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200;
}();
/**
 * BrokerLinkages ID
 * @member {Number} id
 */


GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.prototype['id'] = undefined;
/**
 * The date from which the customer/prospect's consent to be managed is valid
 * @member {Date} fromDt
 */

GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.prototype['fromDt'] = undefined;
/**
 * The date to which the customer/prospect's consent to be managed is valid
 * @member {Date} toDt
 */

GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.prototype['toDt'] = undefined;
/**
 * Indicates whether the customer/prospect has Quoting Permissions
 * @member {Boolean} quotingPermissions
 */

GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.prototype['quotingPermissions'] = undefined;
/**
 * Indicates whether the customer/prospect has Billing Permissions
 * @member {Boolean} billingPermissions
 */

GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.prototype['billingPermissions'] = undefined;
/**
 * Indicates whether the customer/prospect has Accept Quoting Permissions
 * @member {Boolean} acceptQuotingPermissions
 */

GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.prototype['acceptQuotingPermissions'] = undefined;
/**
 * @member {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links} links
 */

GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.prototype['links'] = undefined;
var _default = GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200;
exports["default"] = _default;