"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links model module.
 * @module model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links
 * @version 1.61.1
 */
var EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links</code>.
   * Links to related resources
   * @alias module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links
   * @param self {String} Link referring to this meterpoint
   * @param readings {String} Link to the readings
   * @param meterStructure {String} Link to resource which lists the meterpoint structure - meters attached, meter registers, etc.
   * @param supplyAddress {String} Link to the supply address
   * @param supplyStatusHistory {String} Link to the supply status history
   */
  function EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links(self, readings, meterStructure, supplyAddress, supplyStatusHistory) {
    _classCallCheck(this, EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links);

    EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links.initialize(this, self, readings, meterStructure, supplyAddress, supplyStatusHistory);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, readings, meterStructure, supplyAddress, supplyStatusHistory) {
      obj['self'] = self;
      obj['readings'] = readings;
      obj['meterStructure'] = meterStructure;
      obj['supplyAddress'] = supplyAddress;
      obj['supplyStatusHistory'] = supplyStatusHistory;
    }
    /**
     * Constructs a <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links} obj Optional instance to populate.
     * @return {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links} The populated <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('readings')) {
          obj['readings'] = _ApiClient["default"].convertToType(data['readings'], 'String');
        }

        if (data.hasOwnProperty('meterStructure')) {
          obj['meterStructure'] = _ApiClient["default"].convertToType(data['meterStructure'], 'String');
        }

        if (data.hasOwnProperty('supplyAddress')) {
          obj['supplyAddress'] = _ApiClient["default"].convertToType(data['supplyAddress'], 'String');
        }

        if (data.hasOwnProperty('supplyStatusHistory')) {
          obj['supplyStatusHistory'] = _ApiClient["default"].convertToType(data['supplyStatusHistory'], 'String');
        }

        if (data.hasOwnProperty('estimatedUsage')) {
          obj['estimatedUsage'] = _ApiClient["default"].convertToType(data['estimatedUsage'], 'String');
        }
      }

      return obj;
    }
  }]);

  return EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links;
}();
/**
 * Link referring to this meterpoint
 * @member {String} self
 */


EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links.prototype['self'] = undefined;
/**
 * Link to the readings
 * @member {String} readings
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links.prototype['readings'] = undefined;
/**
 * Link to resource which lists the meterpoint structure - meters attached, meter registers, etc.
 * @member {String} meterStructure
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links.prototype['meterStructure'] = undefined;
/**
 * Link to the supply address
 * @member {String} supplyAddress
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links.prototype['supplyAddress'] = undefined;
/**
 * Link to the supply status history
 * @member {String} supplyStatusHistory
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links.prototype['supplyStatusHistory'] = undefined;
/**
 * Link to the estimated usage
 * @member {String} estimatedUsage
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links.prototype['estimatedUsage'] = undefined;
var _default = EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links;
exports["default"] = _default;