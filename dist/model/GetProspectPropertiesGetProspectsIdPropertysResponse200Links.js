"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetProspectPropertiesGetProspectsIdPropertysResponse200Links model module.
 * @module model/GetProspectPropertiesGetProspectsIdPropertysResponse200Links
 * @version 1.61.1
 */
var GetProspectPropertiesGetProspectsIdPropertysResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetProspectPropertiesGetProspectsIdPropertysResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200Links
   * @param self {String} Link referring to this prospect
   * @param meterPoints {String} Link to this property's meter point information
   */
  function GetProspectPropertiesGetProspectsIdPropertysResponse200Links(self, meterPoints) {
    _classCallCheck(this, GetProspectPropertiesGetProspectsIdPropertysResponse200Links);

    GetProspectPropertiesGetProspectsIdPropertysResponse200Links.initialize(this, self, meterPoints);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetProspectPropertiesGetProspectsIdPropertysResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, meterPoints) {
      obj['self'] = self;
      obj['meterPoints'] = meterPoints;
    }
    /**
     * Constructs a <code>GetProspectPropertiesGetProspectsIdPropertysResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200Links} The populated <code>GetProspectPropertiesGetProspectsIdPropertysResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetProspectPropertiesGetProspectsIdPropertysResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('meterPoints')) {
          obj['meterPoints'] = _ApiClient["default"].convertToType(data['meterPoints'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetProspectPropertiesGetProspectsIdPropertysResponse200Links;
}();
/**
 * Link referring to this prospect
 * @member {String} self
 */


GetProspectPropertiesGetProspectsIdPropertysResponse200Links.prototype['self'] = undefined;
/**
 * Link to this property's meter point information
 * @member {String} meterPoints
 */

GetProspectPropertiesGetProspectsIdPropertysResponse200Links.prototype['meterPoints'] = undefined;
var _default = GetProspectPropertiesGetProspectsIdPropertysResponse200Links;
exports["default"] = _default;