"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetStripePaymentCardGetStripepaymentcardsIdResponse200Links model module.
 * @module model/GetStripePaymentCardGetStripepaymentcardsIdResponse200Links
 * @version 1.61.1
 */
var GetStripePaymentCardGetStripepaymentcardsIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetStripePaymentCardGetStripepaymentcardsIdResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200Links
   * @param self {String} Link referring to this Stripe Payment Card
   * @param paymentMethod {String} Link to the paymentMethod to which this Stripe Payment Card belongs
   */
  function GetStripePaymentCardGetStripepaymentcardsIdResponse200Links(self, paymentMethod) {
    _classCallCheck(this, GetStripePaymentCardGetStripepaymentcardsIdResponse200Links);

    GetStripePaymentCardGetStripepaymentcardsIdResponse200Links.initialize(this, self, paymentMethod);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetStripePaymentCardGetStripepaymentcardsIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, paymentMethod) {
      obj['self'] = self;
      obj['paymentMethod'] = paymentMethod;
    }
    /**
     * Constructs a <code>GetStripePaymentCardGetStripepaymentcardsIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200Links} The populated <code>GetStripePaymentCardGetStripepaymentcardsIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetStripePaymentCardGetStripepaymentcardsIdResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('paymentMethod')) {
          obj['paymentMethod'] = _ApiClient["default"].convertToType(data['paymentMethod'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetStripePaymentCardGetStripepaymentcardsIdResponse200Links;
}();
/**
 * Link referring to this Stripe Payment Card
 * @member {String} self
 */


GetStripePaymentCardGetStripepaymentcardsIdResponse200Links.prototype['self'] = undefined;
/**
 * Link to the paymentMethod to which this Stripe Payment Card belongs
 * @member {String} paymentMethod
 */

GetStripePaymentCardGetStripepaymentcardsIdResponse200Links.prototype['paymentMethod'] = undefined;
var _default = GetStripePaymentCardGetStripepaymentcardsIdResponse200Links;
exports["default"] = _default;