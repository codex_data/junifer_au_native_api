"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links model module.
 * @module model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links
 * @version 1.61.1
 */
var GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links
   * @param self {String} Link referring to this CustomerBroker
   * @param broker {String} Link to this customer broker's broker
   */
  function GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links(self, broker) {
    _classCallCheck(this, GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links);

    GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links.initialize(this, self, broker);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, broker) {
      obj['self'] = self;
      obj['broker'] = broker;
    }
    /**
     * Constructs a <code>GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links} The populated <code>GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('broker')) {
          obj['broker'] = _ApiClient["default"].convertToType(data['broker'], 'String');
        }

        if (data.hasOwnProperty('customer')) {
          obj['customer'] = _ApiClient["default"].convertToType(data['customer'], 'String');
        }

        if (data.hasOwnProperty('prospect')) {
          obj['prospect'] = _ApiClient["default"].convertToType(data['prospect'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links;
}();
/**
 * Link referring to this CustomerBroker
 * @member {String} self
 */


GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links.prototype['self'] = undefined;
/**
 * Link to this customer broker's broker
 * @member {String} broker
 */

GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links.prototype['broker'] = undefined;
/**
 * Link to the customer if the linkage is for a customer
 * @member {String} customer
 */

GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links.prototype['customer'] = undefined;
/**
 * Link to the prospect if the linkage is for a prospect
 * @member {String} prospect
 */

GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links.prototype['prospect'] = undefined;
var _default = GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links;
exports["default"] = _default;