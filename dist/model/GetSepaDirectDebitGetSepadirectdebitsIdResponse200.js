"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links = _interopRequireDefault(require("./GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetSepaDirectDebitGetSepadirectdebitsIdResponse200 model module.
 * @module model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200
 * @version 1.61.1
 */
var GetSepaDirectDebitGetSepadirectdebitsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetSepaDirectDebitGetSepadirectdebitsIdResponse200</code>.
   * @alias module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200
   * @param id {Number} SEPA Direct Debit Id
   * @param accountName {String} account holder name
   * @param iban {Number} IBAN
   * @param mandateReference {String} Mandate Reference
   * @param links {module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links} 
   */
  function GetSepaDirectDebitGetSepadirectdebitsIdResponse200(id, accountName, iban, mandateReference, links) {
    _classCallCheck(this, GetSepaDirectDebitGetSepadirectdebitsIdResponse200);

    GetSepaDirectDebitGetSepadirectdebitsIdResponse200.initialize(this, id, accountName, iban, mandateReference, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetSepaDirectDebitGetSepadirectdebitsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, accountName, iban, mandateReference, links) {
      obj['id'] = id;
      obj['accountName'] = accountName;
      obj['iban'] = iban;
      obj['mandateReference'] = mandateReference;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetSepaDirectDebitGetSepadirectdebitsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200} The populated <code>GetSepaDirectDebitGetSepadirectdebitsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetSepaDirectDebitGetSepadirectdebitsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('accountName')) {
          obj['accountName'] = _ApiClient["default"].convertToType(data['accountName'], 'String');
        }

        if (data.hasOwnProperty('iban')) {
          obj['iban'] = _ApiClient["default"].convertToType(data['iban'], 'Number');
        }

        if (data.hasOwnProperty('mandateReference')) {
          obj['mandateReference'] = _ApiClient["default"].convertToType(data['mandateReference'], 'String');
        }

        if (data.hasOwnProperty('authorisedDttm')) {
          obj['authorisedDttm'] = _ApiClient["default"].convertToType(data['authorisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('terminatedDttm')) {
          obj['terminatedDttm'] = _ApiClient["default"].convertToType(data['terminatedDttm'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetSepaDirectDebitGetSepadirectdebitsIdResponse200;
}();
/**
 * SEPA Direct Debit Id
 * @member {Number} id
 */


GetSepaDirectDebitGetSepadirectdebitsIdResponse200.prototype['id'] = undefined;
/**
 * account holder name
 * @member {String} accountName
 */

GetSepaDirectDebitGetSepadirectdebitsIdResponse200.prototype['accountName'] = undefined;
/**
 * IBAN
 * @member {Number} iban
 */

GetSepaDirectDebitGetSepadirectdebitsIdResponse200.prototype['iban'] = undefined;
/**
 * Mandate Reference
 * @member {String} mandateReference
 */

GetSepaDirectDebitGetSepadirectdebitsIdResponse200.prototype['mandateReference'] = undefined;
/**
 * Date when the mandate was authorised
 * @member {Date} authorisedDttm
 */

GetSepaDirectDebitGetSepadirectdebitsIdResponse200.prototype['authorisedDttm'] = undefined;
/**
 * Date when the mandate was terminated
 * @member {Date} terminatedDttm
 */

GetSepaDirectDebitGetSepadirectdebitsIdResponse200.prototype['terminatedDttm'] = undefined;
/**
 * @member {module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links} links
 */

GetSepaDirectDebitGetSepadirectdebitsIdResponse200.prototype['links'] = undefined;
var _default = GetSepaDirectDebitGetSepadirectdebitsIdResponse200;
exports["default"] = _default;