"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links = _interopRequireDefault(require("./GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountBillRequestsGetAccountsIdBillrequestsResponse200 model module.
 * @module model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200
 * @version 1.61.1
 */
var GetAccountBillRequestsGetAccountsIdBillrequestsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountBillRequestsGetAccountsIdBillrequestsResponse200</code>.
   * @alias module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200
   * @param id {Number} Id of bill request
   * @param billPeriodFromDttm {Date} From date time of bill period linked to bill request
   * @param billPeriodToDttm {Date} the To date time of bill period linked to bill request
   * @param statusCode {String} The status code of the bill request
   * @param typeCode {String} The type code of the bill request
   * @param taskCreatedDttm {Date} The created date time of task linked to bill request
   * @param taskStartDttm {Date} The start date time of task linked to bill request
   * @param taskFinishDttm {Date} The finish date time of task linked to bill request
   * @param taskUserTblUsername {String} The username of the UserTbl of the task linked to bill request
   * @param testFl {Boolean} Flag indicating whether the bill request is for a test bill
   * @param links {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links} 
   */
  function GetAccountBillRequestsGetAccountsIdBillrequestsResponse200(id, billPeriodFromDttm, billPeriodToDttm, statusCode, typeCode, taskCreatedDttm, taskStartDttm, taskFinishDttm, taskUserTblUsername, testFl, links) {
    _classCallCheck(this, GetAccountBillRequestsGetAccountsIdBillrequestsResponse200);

    GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.initialize(this, id, billPeriodFromDttm, billPeriodToDttm, statusCode, typeCode, taskCreatedDttm, taskStartDttm, taskFinishDttm, taskUserTblUsername, testFl, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountBillRequestsGetAccountsIdBillrequestsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, billPeriodFromDttm, billPeriodToDttm, statusCode, typeCode, taskCreatedDttm, taskStartDttm, taskFinishDttm, taskUserTblUsername, testFl, links) {
      obj['id'] = id;
      obj['billPeriodFromDttm'] = billPeriodFromDttm;
      obj['billPeriodToDttm'] = billPeriodToDttm;
      obj['statusCode'] = statusCode;
      obj['typeCode'] = typeCode;
      obj['taskCreatedDttm'] = taskCreatedDttm;
      obj['taskStartDttm'] = taskStartDttm;
      obj['taskFinishDttm'] = taskFinishDttm;
      obj['taskUserTblUsername'] = taskUserTblUsername;
      obj['testFl'] = testFl;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountBillRequestsGetAccountsIdBillrequestsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200} The populated <code>GetAccountBillRequestsGetAccountsIdBillrequestsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountBillRequestsGetAccountsIdBillrequestsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('billId')) {
          obj['billId'] = _ApiClient["default"].convertToType(data['billId'], 'Number');
        }

        if (data.hasOwnProperty('billPeriodFromDttm')) {
          obj['billPeriodFromDttm'] = _ApiClient["default"].convertToType(data['billPeriodFromDttm'], 'Date');
        }

        if (data.hasOwnProperty('billPeriodToDttm')) {
          obj['billPeriodToDttm'] = _ApiClient["default"].convertToType(data['billPeriodToDttm'], 'Date');
        }

        if (data.hasOwnProperty('statusCode')) {
          obj['statusCode'] = _ApiClient["default"].convertToType(data['statusCode'], 'String');
        }

        if (data.hasOwnProperty('typeCode')) {
          obj['typeCode'] = _ApiClient["default"].convertToType(data['typeCode'], 'String');
        }

        if (data.hasOwnProperty('taskCreatedDttm')) {
          obj['taskCreatedDttm'] = _ApiClient["default"].convertToType(data['taskCreatedDttm'], 'Date');
        }

        if (data.hasOwnProperty('taskStartDttm')) {
          obj['taskStartDttm'] = _ApiClient["default"].convertToType(data['taskStartDttm'], 'Date');
        }

        if (data.hasOwnProperty('taskFinishDttm')) {
          obj['taskFinishDttm'] = _ApiClient["default"].convertToType(data['taskFinishDttm'], 'Date');
        }

        if (data.hasOwnProperty('taskUserTblUsername')) {
          obj['taskUserTblUsername'] = _ApiClient["default"].convertToType(data['taskUserTblUsername'], 'String');
        }

        if (data.hasOwnProperty('serverExceptionMessage')) {
          obj['serverExceptionMessage'] = _ApiClient["default"].convertToType(data['serverExceptionMessage'], 'String');
        }

        if (data.hasOwnProperty('testFl')) {
          obj['testFl'] = _ApiClient["default"].convertToType(data['testFl'], 'Boolean');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountBillRequestsGetAccountsIdBillrequestsResponse200;
}();
/**
 * Id of bill request
 * @member {Number} id
 */


GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['id'] = undefined;
/**
 * The id of bill linked to bill request (if the associated bill has been generated)
 * @member {Number} billId
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['billId'] = undefined;
/**
 * From date time of bill period linked to bill request
 * @member {Date} billPeriodFromDttm
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['billPeriodFromDttm'] = undefined;
/**
 * the To date time of bill period linked to bill request
 * @member {Date} billPeriodToDttm
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['billPeriodToDttm'] = undefined;
/**
 * The status code of the bill request
 * @member {String} statusCode
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['statusCode'] = undefined;
/**
 * The type code of the bill request
 * @member {String} typeCode
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['typeCode'] = undefined;
/**
 * The created date time of task linked to bill request
 * @member {Date} taskCreatedDttm
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['taskCreatedDttm'] = undefined;
/**
 * The start date time of task linked to bill request
 * @member {Date} taskStartDttm
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['taskStartDttm'] = undefined;
/**
 * The finish date time of task linked to bill request
 * @member {Date} taskFinishDttm
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['taskFinishDttm'] = undefined;
/**
 * The username of the UserTbl of the task linked to bill request
 * @member {String} taskUserTblUsername
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['taskUserTblUsername'] = undefined;
/**
 * Server exception message of bill request if it failed
 * @member {String} serverExceptionMessage
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['serverExceptionMessage'] = undefined;
/**
 * Flag indicating whether the bill request is for a test bill
 * @member {Boolean} testFl
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['testFl'] = undefined;
/**
 * @member {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links} links
 */

GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.prototype['links'] = undefined;
var _default = GetAccountBillRequestsGetAccountsIdBillrequestsResponse200;
exports["default"] = _default;