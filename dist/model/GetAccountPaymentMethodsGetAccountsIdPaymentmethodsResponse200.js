"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links = _interopRequireDefault(require("./GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200 model module.
 * @module model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200
 * @version 1.61.1
 */
var GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200</code>.
   * @alias module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200
   * @param id {Number} The ID of the payment method
   * @param paymentMethodType {String} The type of the payment method
   * @param status {String} The status of the payment method. Can be one of the following: `Pending, Active, Cancelled, Failed, RequestingActivation, PendingCancellation, RequestingCancellation, FailedCancellation, Paused, CoolingOff`
   * @param createdDttm {Date} The date and time this payment method was created
   * @param fromDttm {Date} Date when the payment method becomes active
   * @param defaultStatus {String} Status indicating whether this payment method is the default one for this account. Can be one of the following: `Pending,Default,NotDefault`
   * @param links {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links} 
   */
  function GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200(id, paymentMethodType, status, createdDttm, fromDttm, defaultStatus, links) {
    _classCallCheck(this, GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200);

    GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.initialize(this, id, paymentMethodType, status, createdDttm, fromDttm, defaultStatus, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, paymentMethodType, status, createdDttm, fromDttm, defaultStatus, links) {
      obj['id'] = id;
      obj['paymentMethodType'] = paymentMethodType;
      obj['status'] = status;
      obj['createdDttm'] = createdDttm;
      obj['fromDttm'] = fromDttm;
      obj['defaultStatus'] = defaultStatus;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200} The populated <code>GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('paymentMethodType')) {
          obj['paymentMethodType'] = _ApiClient["default"].convertToType(data['paymentMethodType'], 'String');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('defaultStatus')) {
          obj['defaultStatus'] = _ApiClient["default"].convertToType(data['defaultStatus'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200;
}();
/**
 * The ID of the payment method
 * @member {Number} id
 */


GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.prototype['id'] = undefined;
/**
 * The type of the payment method
 * @member {String} paymentMethodType
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.prototype['paymentMethodType'] = undefined;
/**
 * The status of the payment method. Can be one of the following: `Pending, Active, Cancelled, Failed, RequestingActivation, PendingCancellation, RequestingCancellation, FailedCancellation, Paused, CoolingOff`
 * @member {String} status
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.prototype['status'] = undefined;
/**
 * The date and time this payment method was created
 * @member {Date} createdDttm
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.prototype['createdDttm'] = undefined;
/**
 * Date when the payment method becomes active
 * @member {Date} fromDttm
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.prototype['fromDttm'] = undefined;
/**
 * Date when the payment method will terminate
 * @member {Date} toDttm
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.prototype['toDttm'] = undefined;
/**
 * Status indicating whether this payment method is the default one for this account. Can be one of the following: `Pending,Default,NotDefault`
 * @member {String} defaultStatus
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.prototype['defaultStatus'] = undefined;
/**
 * @member {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links} links
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.prototype['links'] = undefined;
var _default = GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200;
exports["default"] = _default;