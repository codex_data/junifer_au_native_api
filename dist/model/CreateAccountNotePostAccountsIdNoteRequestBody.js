"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountNotePostAccountsIdNoteRequestBody model module.
 * @module model/CreateAccountNotePostAccountsIdNoteRequestBody
 * @version 1.61.1
 */
var CreateAccountNotePostAccountsIdNoteRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountNotePostAccountsIdNoteRequestBody</code>.
   * @alias module:model/CreateAccountNotePostAccountsIdNoteRequestBody
   * @param summary {String} A brief summary of the Note
   */
  function CreateAccountNotePostAccountsIdNoteRequestBody(summary) {
    _classCallCheck(this, CreateAccountNotePostAccountsIdNoteRequestBody);

    CreateAccountNotePostAccountsIdNoteRequestBody.initialize(this, summary);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountNotePostAccountsIdNoteRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, summary) {
      obj['summary'] = summary;
    }
    /**
     * Constructs a <code>CreateAccountNotePostAccountsIdNoteRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountNotePostAccountsIdNoteRequestBody} obj Optional instance to populate.
     * @return {module:model/CreateAccountNotePostAccountsIdNoteRequestBody} The populated <code>CreateAccountNotePostAccountsIdNoteRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountNotePostAccountsIdNoteRequestBody();

        if (data.hasOwnProperty('subject')) {
          obj['subject'] = _ApiClient["default"].convertToType(data['subject'], 'String');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('summary')) {
          obj['summary'] = _ApiClient["default"].convertToType(data['summary'], 'String');
        }

        if (data.hasOwnProperty('content')) {
          obj['content'] = _ApiClient["default"].convertToType(data['content'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountNotePostAccountsIdNoteRequestBody;
}();
/**
 * Note Subject. Will default to 'General Note'
 * @member {String} subject
 */


CreateAccountNotePostAccountsIdNoteRequestBody.prototype['subject'] = undefined;
/**
 * Note Type. Can be one of the following: `Note, Email, File, Phone, Letter`. Will default to 'Note'.
 * @member {String} type
 */

CreateAccountNotePostAccountsIdNoteRequestBody.prototype['type'] = undefined;
/**
 * A brief summary of the Note
 * @member {String} summary
 */

CreateAccountNotePostAccountsIdNoteRequestBody.prototype['summary'] = undefined;
/**
 * The detailed content of the Note
 * @member {String} content
 */

CreateAccountNotePostAccountsIdNoteRequestBody.prototype['content'] = undefined;
var _default = CreateAccountNotePostAccountsIdNoteRequestBody;
exports["default"] = _default;