"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _NewSepaDirectDebitPostSepadirectdebitsResponse200Links = _interopRequireDefault(require("./NewSepaDirectDebitPostSepadirectdebitsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The NewSepaDirectDebitPostSepadirectdebitsResponse200 model module.
 * @module model/NewSepaDirectDebitPostSepadirectdebitsResponse200
 * @version 1.61.1
 */
var NewSepaDirectDebitPostSepadirectdebitsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>NewSepaDirectDebitPostSepadirectdebitsResponse200</code>.
   * @alias module:model/NewSepaDirectDebitPostSepadirectdebitsResponse200
   * @param id {Number} SEPA Direct Debit object id
   * @param accountName {String} Account name
   * @param iban {String} IBAN, e.g. IE29AIBK93115212345678
   * @param mandateReference {String} SEPA Direct Debit mandate reference
   * @param links {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse200Links} 
   */
  function NewSepaDirectDebitPostSepadirectdebitsResponse200(id, accountName, iban, mandateReference, links) {
    _classCallCheck(this, NewSepaDirectDebitPostSepadirectdebitsResponse200);

    NewSepaDirectDebitPostSepadirectdebitsResponse200.initialize(this, id, accountName, iban, mandateReference, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(NewSepaDirectDebitPostSepadirectdebitsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, accountName, iban, mandateReference, links) {
      obj['id'] = id;
      obj['accountName'] = accountName;
      obj['iban'] = iban;
      obj['mandateReference'] = mandateReference;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>NewSepaDirectDebitPostSepadirectdebitsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse200} obj Optional instance to populate.
     * @return {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse200} The populated <code>NewSepaDirectDebitPostSepadirectdebitsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new NewSepaDirectDebitPostSepadirectdebitsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('accountName')) {
          obj['accountName'] = _ApiClient["default"].convertToType(data['accountName'], 'String');
        }

        if (data.hasOwnProperty('iban')) {
          obj['iban'] = _ApiClient["default"].convertToType(data['iban'], 'String');
        }

        if (data.hasOwnProperty('mandateReference')) {
          obj['mandateReference'] = _ApiClient["default"].convertToType(data['mandateReference'], 'String');
        }

        if (data.hasOwnProperty('authorisedDttm')) {
          obj['authorisedDttm'] = _ApiClient["default"].convertToType(data['authorisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('terminatedDttm')) {
          obj['terminatedDttm'] = _ApiClient["default"].convertToType(data['terminatedDttm'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _NewSepaDirectDebitPostSepadirectdebitsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return NewSepaDirectDebitPostSepadirectdebitsResponse200;
}();
/**
 * SEPA Direct Debit object id
 * @member {Number} id
 */


NewSepaDirectDebitPostSepadirectdebitsResponse200.prototype['id'] = undefined;
/**
 * Account name
 * @member {String} accountName
 */

NewSepaDirectDebitPostSepadirectdebitsResponse200.prototype['accountName'] = undefined;
/**
 * IBAN, e.g. IE29AIBK93115212345678
 * @member {String} iban
 */

NewSepaDirectDebitPostSepadirectdebitsResponse200.prototype['iban'] = undefined;
/**
 * SEPA Direct Debit mandate reference
 * @member {String} mandateReference
 */

NewSepaDirectDebitPostSepadirectdebitsResponse200.prototype['mandateReference'] = undefined;
/**
 * Date when the mandate was authorised
 * @member {Date} authorisedDttm
 */

NewSepaDirectDebitPostSepadirectdebitsResponse200.prototype['authorisedDttm'] = undefined;
/**
 * Date when the mandate was terminated
 * @member {Date} terminatedDttm
 */

NewSepaDirectDebitPostSepadirectdebitsResponse200.prototype['terminatedDttm'] = undefined;
/**
 * @member {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse200Links} links
 */

NewSepaDirectDebitPostSepadirectdebitsResponse200.prototype['links'] = undefined;
var _default = NewSepaDirectDebitPostSepadirectdebitsResponse200;
exports["default"] = _default;