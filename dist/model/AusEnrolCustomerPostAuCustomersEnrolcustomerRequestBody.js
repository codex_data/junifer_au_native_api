"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts = _interopRequireDefault(require("./AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts"));

var _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts = _interopRequireDefault(require("./AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts"));

var _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress = _interopRequireDefault(require("./AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress"));

var _AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct = _interopRequireDefault(require("./AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody model module.
 * @module model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody
 * @version 1.61.1
 */
var AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody</code>.
   * @alias module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody
   * @param proposedDate {Date} The proposed date for customer to transfer or move in
   * @param moveInFl {Boolean} Whether the account is for move-in or not
   * @param autoRaiseMeterExchangeServiceOrderFl {Boolean} Whether the Meter Exchange Service Order should be automatically raised or not
   * @param senderReference {String} Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected
   * @param reference {String} Unique reference string identifying this particular enrolment
   * @param marketingOptOutFl {Boolean} A boolean flag indicating whether the customer would like to opt out of any marketing communications
   * @param contacts {Array.<module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts>} Customer contacts' detail's array. One of the contacts must be primary contact and must be present in the request.
   * @param supplyAddress {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress} 
   */
  function AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody(proposedDate, moveInFl, autoRaiseMeterExchangeServiceOrderFl, senderReference, reference, marketingOptOutFl, contacts, supplyAddress) {
    _classCallCheck(this, AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody);

    AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.initialize(this, proposedDate, moveInFl, autoRaiseMeterExchangeServiceOrderFl, senderReference, reference, marketingOptOutFl, contacts, supplyAddress);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, proposedDate, moveInFl, autoRaiseMeterExchangeServiceOrderFl, senderReference, reference, marketingOptOutFl, contacts, supplyAddress) {
      obj['proposedDate'] = proposedDate;
      obj['moveInFl'] = moveInFl;
      obj['autoRaiseMeterExchangeServiceOrderFl'] = autoRaiseMeterExchangeServiceOrderFl;
      obj['senderReference'] = senderReference;
      obj['reference'] = reference;
      obj['marketingOptOutFl'] = marketingOptOutFl;
      obj['contacts'] = contacts;
      obj['supplyAddress'] = supplyAddress;
    }
    /**
     * Constructs a <code>AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody} obj Optional instance to populate.
     * @return {module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody} The populated <code>AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody();

        if (data.hasOwnProperty('proposedDate')) {
          obj['proposedDate'] = _ApiClient["default"].convertToType(data['proposedDate'], 'Date');
        }

        if (data.hasOwnProperty('moveInFl')) {
          obj['moveInFl'] = _ApiClient["default"].convertToType(data['moveInFl'], 'Boolean');
        }

        if (data.hasOwnProperty('autoRaiseMeterExchangeServiceOrderFl')) {
          obj['autoRaiseMeterExchangeServiceOrderFl'] = _ApiClient["default"].convertToType(data['autoRaiseMeterExchangeServiceOrderFl'], 'Boolean');
        }

        if (data.hasOwnProperty('nominatedRpParticipantCode')) {
          obj['nominatedRpParticipantCode'] = _ApiClient["default"].convertToType(data['nominatedRpParticipantCode'], 'String');
        }

        if (data.hasOwnProperty('nominatedMpbParticipantCode')) {
          obj['nominatedMpbParticipantCode'] = _ApiClient["default"].convertToType(data['nominatedMpbParticipantCode'], 'String');
        }

        if (data.hasOwnProperty('nominatedMdpParticipantCode')) {
          obj['nominatedMdpParticipantCode'] = _ApiClient["default"].convertToType(data['nominatedMdpParticipantCode'], 'String');
        }

        if (data.hasOwnProperty('nominatedMpcParticipantCode')) {
          obj['nominatedMpcParticipantCode'] = _ApiClient["default"].convertToType(data['nominatedMpcParticipantCode'], 'String');
        }

        if (data.hasOwnProperty('customerNumber')) {
          obj['customerNumber'] = _ApiClient["default"].convertToType(data['customerNumber'], 'String');
        }

        if (data.hasOwnProperty('accountNumber')) {
          obj['accountNumber'] = _ApiClient["default"].convertToType(data['accountNumber'], 'String');
        }

        if (data.hasOwnProperty('accountTypeCode')) {
          obj['accountTypeCode'] = _ApiClient["default"].convertToType(data['accountTypeCode'], 'String');
        }

        if (data.hasOwnProperty('paymentTermName')) {
          obj['paymentTermName'] = _ApiClient["default"].convertToType(data['paymentTermName'], 'String');
        }

        if (data.hasOwnProperty('billingCycle')) {
          obj['billingCycle'] = _ApiClient["default"].convertToType(data['billingCycle'], 'String');
        }

        if (data.hasOwnProperty('changeReasonCode')) {
          obj['changeReasonCode'] = _ApiClient["default"].convertToType(data['changeReasonCode'], 'String');
        }

        if (data.hasOwnProperty('meterReadingConfig')) {
          obj['meterReadingConfig'] = _ApiClient["default"].convertToType(data['meterReadingConfig'], 'String');
        }

        if (data.hasOwnProperty('senderReference')) {
          obj['senderReference'] = _ApiClient["default"].convertToType(data['senderReference'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('billingEntityCode')) {
          obj['billingEntityCode'] = _ApiClient["default"].convertToType(data['billingEntityCode'], 'String');
        }

        if (data.hasOwnProperty('marketingOptOutFl')) {
          obj['marketingOptOutFl'] = _ApiClient["default"].convertToType(data['marketingOptOutFl'], 'Boolean');
        }

        if (data.hasOwnProperty('changeOfTenancyFl')) {
          obj['changeOfTenancyFl'] = _ApiClient["default"].convertToType(data['changeOfTenancyFl'], 'Boolean');
        }

        if (data.hasOwnProperty('changeOfTenancyDate')) {
          obj['changeOfTenancyDate'] = _ApiClient["default"].convertToType(data['changeOfTenancyDate'], 'Date');
        }

        if (data.hasOwnProperty('contacts')) {
          obj['contacts'] = _ApiClient["default"].convertToType(data['contacts'], [_AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts["default"]]);
        }

        if (data.hasOwnProperty('supplyAddress')) {
          obj['supplyAddress'] = _AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress["default"].constructFromObject(data['supplyAddress']);
        }

        if (data.hasOwnProperty('electricityProduct')) {
          obj['electricityProduct'] = _AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct["default"].constructFromObject(data['electricityProduct']);
        }

        if (data.hasOwnProperty('multipleElectricityProducts')) {
          obj['multipleElectricityProducts'] = _ApiClient["default"].convertToType(data['multipleElectricityProducts'], [_AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts["default"]]);
        }
      }

      return obj;
    }
  }]);

  return AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody;
}();
/**
 * The proposed date for customer to transfer or move in
 * @member {Date} proposedDate
 */


AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['proposedDate'] = undefined;
/**
 * Whether the account is for move-in or not
 * @member {Boolean} moveInFl
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['moveInFl'] = undefined;
/**
 * Whether the Meter Exchange Service Order should be automatically raised or not
 * @member {Boolean} autoRaiseMeterExchangeServiceOrderFl
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['autoRaiseMeterExchangeServiceOrderFl'] = undefined;
/**
 * The RP Market Participant Code nominated for onboarding
 * @member {String} nominatedRpParticipantCode
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['nominatedRpParticipantCode'] = undefined;
/**
 * The MPB Market Participant Code nominated for onboarding
 * @member {String} nominatedMpbParticipantCode
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['nominatedMpbParticipantCode'] = undefined;
/**
 * The MDP Market Participant Code nominated for onboarding
 * @member {String} nominatedMdpParticipantCode
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['nominatedMdpParticipantCode'] = undefined;
/**
 * The MPC Market Participant Code nomianted for onboarding
 * @member {String} nominatedMpcParticipantCode
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['nominatedMpcParticipantCode'] = undefined;
/**
 * External customer number to be used
 * @member {String} customerNumber
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['customerNumber'] = undefined;
/**
 * External account number to be used
 * @member {String} accountNumber
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['accountNumber'] = undefined;
/**
 * Code of the account type to be used for the account
 * @member {String} accountTypeCode
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['accountTypeCode'] = undefined;
/**
 * Name of the payment term to be used for the account
 * @member {String} paymentTermName
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['paymentTermName'] = undefined;
/**
 * Reference code of the billing cycle to be used for the account
 * @member {String} billingCycle
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['billingCycle'] = undefined;
/**
 * CATS change reason code used for the onboarding
 * @member {String} changeReasonCode
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['changeReasonCode'] = undefined;
/**
 * Meter reading configuration
 * @member {String} meterReadingConfig
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['meterReadingConfig'] = undefined;
/**
 * Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected
 * @member {String} senderReference
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['senderReference'] = undefined;
/**
 * Unique reference string identifying this particular enrolment
 * @member {String} reference
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['reference'] = undefined;
/**
 * Code of the billing entity to which the customer must be attached. In systems with more than one billing entity this field is not optional
 * @member {String} billingEntityCode
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['billingEntityCode'] = undefined;
/**
 * A boolean flag indicating whether the customer would like to opt out of any marketing communications
 * @member {Boolean} marketingOptOutFl
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['marketingOptOutFl'] = undefined;
/**
 * A boolean flag indicating whether the tenant is also undergoing a change of tenancy
 * @member {Boolean} changeOfTenancyFl
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['changeOfTenancyFl'] = undefined;
/**
 * The date of change of Tenancy
 * @member {Date} changeOfTenancyDate
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['changeOfTenancyDate'] = undefined;
/**
 * Customer contacts' detail's array. One of the contacts must be primary contact and must be present in the request.
 * @member {Array.<module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts>} contacts
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['contacts'] = undefined;
/**
 * @member {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress} supplyAddress
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['supplyAddress'] = undefined;
/**
 * @member {module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct} electricityProduct
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['electricityProduct'] = undefined;
/**
 * Multiple electricity products the customer chose. Can be used for unbundled agreements.
 * @member {Array.<module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts>} multipleElectricityProducts
 */

AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.prototype['multipleElectricityProducts'] = undefined;
var _default = AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody;
exports["default"] = _default;