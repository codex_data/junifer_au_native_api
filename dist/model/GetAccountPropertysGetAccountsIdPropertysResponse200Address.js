"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountPropertysGetAccountsIdPropertysResponse200Address model module.
 * @module model/GetAccountPropertysGetAccountsIdPropertysResponse200Address
 * @version 1.61.1
 */
var GetAccountPropertysGetAccountsIdPropertysResponse200Address = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountPropertysGetAccountsIdPropertysResponse200Address</code>.
   * The address of the property
   * @alias module:model/GetAccountPropertysGetAccountsIdPropertysResponse200Address
   * @param addressType {String} Internally used address type which defines number of lines and format used etc.
   * @param address1 {String} Property address line 1
   * @param address2 {String} Property address line 2
   * @param address3 {String} Property address line 3
   * @param address4 {String} Property address line 4
   * @param address5 {String} Property address line 5
   * @param postcode {String} Property address post code
   */
  function GetAccountPropertysGetAccountsIdPropertysResponse200Address(addressType, address1, address2, address3, address4, address5, postcode) {
    _classCallCheck(this, GetAccountPropertysGetAccountsIdPropertysResponse200Address);

    GetAccountPropertysGetAccountsIdPropertysResponse200Address.initialize(this, addressType, address1, address2, address3, address4, address5, postcode);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountPropertysGetAccountsIdPropertysResponse200Address, null, [{
    key: "initialize",
    value: function initialize(obj, addressType, address1, address2, address3, address4, address5, postcode) {
      obj['addressType'] = addressType;
      obj['address1'] = address1;
      obj['address2'] = address2;
      obj['address3'] = address3;
      obj['address4'] = address4;
      obj['address5'] = address5;
      obj['postcode'] = postcode;
    }
    /**
     * Constructs a <code>GetAccountPropertysGetAccountsIdPropertysResponse200Address</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200Address} obj Optional instance to populate.
     * @return {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200Address} The populated <code>GetAccountPropertysGetAccountsIdPropertysResponse200Address</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountPropertysGetAccountsIdPropertysResponse200Address();

        if (data.hasOwnProperty('addressType')) {
          obj['addressType'] = _ApiClient["default"].convertToType(data['addressType'], 'String');
        }

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountPropertysGetAccountsIdPropertysResponse200Address;
}();
/**
 * Internally used address type which defines number of lines and format used etc.
 * @member {String} addressType
 */


GetAccountPropertysGetAccountsIdPropertysResponse200Address.prototype['addressType'] = undefined;
/**
 * Property address line 1
 * @member {String} address1
 */

GetAccountPropertysGetAccountsIdPropertysResponse200Address.prototype['address1'] = undefined;
/**
 * Property address line 2
 * @member {String} address2
 */

GetAccountPropertysGetAccountsIdPropertysResponse200Address.prototype['address2'] = undefined;
/**
 * Property address line 3
 * @member {String} address3
 */

GetAccountPropertysGetAccountsIdPropertysResponse200Address.prototype['address3'] = undefined;
/**
 * Property address line 4
 * @member {String} address4
 */

GetAccountPropertysGetAccountsIdPropertysResponse200Address.prototype['address4'] = undefined;
/**
 * Property address line 5
 * @member {String} address5
 */

GetAccountPropertysGetAccountsIdPropertysResponse200Address.prototype['address5'] = undefined;
/**
 * Property address post code
 * @member {String} postcode
 */

GetAccountPropertysGetAccountsIdPropertysResponse200Address.prototype['postcode'] = undefined;
var _default = GetAccountPropertysGetAccountsIdPropertysResponse200Address;
exports["default"] = _default;