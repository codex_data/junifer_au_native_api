"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountTransactionsGetAccountsIdTransactionsResponse200Links = _interopRequireDefault(require("./GetAccountTransactionsGetAccountsIdTransactionsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountTransactionsGetAccountsIdTransactionsResponse200 model module.
 * @module model/GetAccountTransactionsGetAccountsIdTransactionsResponse200
 * @version 1.61.1
 */
var GetAccountTransactionsGetAccountsIdTransactionsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountTransactionsGetAccountsIdTransactionsResponse200</code>.
   * @alias module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200
   * @param id {Number} Account transaction identifier
   * @param acceptedDttm {Date} Date and time the transaction was accepted
   * @param status {String} Status of the single transaction - Pending, Accepted, Rejected, Authorising
   * @param createdDttm {Date} Date and time the transaction was created
   * @param type {String} Transaction type
   * @param currency {String} Currency transaction was made in
   * @param currencyISO {String} Currency ISO code
   * @param debitAmount {Number} The amount the account was debited by the transaction
   * @param createdUser {String} The name of the user who created the transaction.
   * @param reference {String} Transaction reference
   */
  function GetAccountTransactionsGetAccountsIdTransactionsResponse200(id, acceptedDttm, status, createdDttm, type, currency, currencyISO, debitAmount, createdUser, reference) {
    _classCallCheck(this, GetAccountTransactionsGetAccountsIdTransactionsResponse200);

    GetAccountTransactionsGetAccountsIdTransactionsResponse200.initialize(this, id, acceptedDttm, status, createdDttm, type, currency, currencyISO, debitAmount, createdUser, reference);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountTransactionsGetAccountsIdTransactionsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, acceptedDttm, status, createdDttm, type, currency, currencyISO, debitAmount, createdUser, reference) {
      obj['id'] = id;
      obj['acceptedDttm'] = acceptedDttm;
      obj['status'] = status;
      obj['createdDttm'] = createdDttm;
      obj['type'] = type;
      obj['currency'] = currency;
      obj['currencyISO'] = currencyISO;
      obj['debitAmount'] = debitAmount;
      obj['createdUser'] = createdUser;
      obj['reference'] = reference;
    }
    /**
     * Constructs a <code>GetAccountTransactionsGetAccountsIdTransactionsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200} The populated <code>GetAccountTransactionsGetAccountsIdTransactionsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountTransactionsGetAccountsIdTransactionsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('acceptedDttm')) {
          obj['acceptedDttm'] = _ApiClient["default"].convertToType(data['acceptedDttm'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('dueDt')) {
          obj['dueDt'] = _ApiClient["default"].convertToType(data['dueDt'], 'Date');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('currencyISO')) {
          obj['currencyISO'] = _ApiClient["default"].convertToType(data['currencyISO'], 'String');
        }

        if (data.hasOwnProperty('debitAmount')) {
          obj['debitAmount'] = _ApiClient["default"].convertToType(data['debitAmount'], 'Number');
        }

        if (data.hasOwnProperty('debitBalance')) {
          obj['debitBalance'] = _ApiClient["default"].convertToType(data['debitBalance'], 'Number');
        }

        if (data.hasOwnProperty('creditAmount')) {
          obj['creditAmount'] = _ApiClient["default"].convertToType(data['creditAmount'], 'Number');
        }

        if (data.hasOwnProperty('creditBalance')) {
          obj['creditBalance'] = _ApiClient["default"].convertToType(data['creditBalance'], 'Number');
        }

        if (data.hasOwnProperty('billId')) {
          obj['billId'] = _ApiClient["default"].convertToType(data['billId'], 'Number');
        }

        if (data.hasOwnProperty('accountBalance')) {
          obj['accountBalance'] = _ApiClient["default"].convertToType(data['accountBalance'], 'Number');
        }

        if (data.hasOwnProperty('createdUser')) {
          obj['createdUser'] = _ApiClient["default"].convertToType(data['createdUser'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('cancelledDttm')) {
          obj['cancelledDttm'] = _ApiClient["default"].convertToType(data['cancelledDttm'], 'Date');
        }

        if (data.hasOwnProperty('cancelAccountTransactionId')) {
          obj['cancelAccountTransactionId'] = _ApiClient["default"].convertToType(data['cancelAccountTransactionId'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountTransactionsGetAccountsIdTransactionsResponse200Links["default"].constructFromObject(data['links']);
        }

        if (data.hasOwnProperty('orderNo')) {
          obj['orderNo'] = _ApiClient["default"].convertToType(data['orderNo'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return GetAccountTransactionsGetAccountsIdTransactionsResponse200;
}();
/**
 * Account transaction identifier
 * @member {Number} id
 */


GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['id'] = undefined;
/**
 * Date and time the transaction was accepted
 * @member {Date} acceptedDttm
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['acceptedDttm'] = undefined;
/**
 * Status of the single transaction - Pending, Accepted, Rejected, Authorising
 * @member {String} status
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['status'] = undefined;
/**
 * Date and time the transaction was created
 * @member {Date} createdDttm
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['createdDttm'] = undefined;
/**
 * Date that transaction should be paid before
 * @member {Date} dueDt
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['dueDt'] = undefined;
/**
 * Transaction type
 * @member {String} type
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['type'] = undefined;
/**
 * Transaction description
 * @member {String} description
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['description'] = undefined;
/**
 * Currency transaction was made in
 * @member {String} currency
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['currency'] = undefined;
/**
 * Currency ISO code
 * @member {String} currencyISO
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['currencyISO'] = undefined;
/**
 * The amount the account was debited by the transaction
 * @member {Number} debitAmount
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['debitAmount'] = undefined;
/**
 * Unpaid balance of this transaction
 * @member {Number} debitBalance
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['debitBalance'] = undefined;
/**
 * The amount that the account was credited by the transaction
 * @member {Number} creditAmount
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['creditAmount'] = undefined;
/**
 * Unallocated balance of this transaction
 * @member {Number} creditBalance
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['creditBalance'] = undefined;
/**
 * The id of the bill linked to the transaction
 * @member {Number} billId
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['billId'] = undefined;
/**
 * The account balance after the transaction was applied
 * @member {Number} accountBalance
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['accountBalance'] = undefined;
/**
 * The name of the user who created the transaction.
 * @member {String} createdUser
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['createdUser'] = undefined;
/**
 * Transaction reference
 * @member {String} reference
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['reference'] = undefined;
/**
 * The date when the transaction was cancelled
 * @member {Date} cancelledDttm
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['cancelledDttm'] = undefined;
/**
 * ID of the transaction that this transaction is cancelling. The referenced transaction will have cancelled date/time.
 * @member {Number} cancelAccountTransactionId
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['cancelAccountTransactionId'] = undefined;
/**
 * @member {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200Links} links
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['links'] = undefined;
/**
 * Order in which the transaction was accepted, null if not yet accepted
 * @member {Number} orderNo
 */

GetAccountTransactionsGetAccountsIdTransactionsResponse200.prototype['orderNo'] = undefined;
var _default = GetAccountTransactionsGetAccountsIdTransactionsResponse200;
exports["default"] = _default;