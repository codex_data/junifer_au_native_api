"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EmailPostCommunicationsEmailRequestBody model module.
 * @module model/EmailPostCommunicationsEmailRequestBody
 * @version 1.61.1
 */
var EmailPostCommunicationsEmailRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EmailPostCommunicationsEmailRequestBody</code>.
   * @alias module:model/EmailPostCommunicationsEmailRequestBody
   * @param billingEntity {String} Code of the billing entity for this web portal
   * @param toAddress {String} Recipient's email address
   * @param subject {String} Subject text
   * @param body {String} Email's body text
   */
  function EmailPostCommunicationsEmailRequestBody(billingEntity, toAddress, subject, body) {
    _classCallCheck(this, EmailPostCommunicationsEmailRequestBody);

    EmailPostCommunicationsEmailRequestBody.initialize(this, billingEntity, toAddress, subject, body);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EmailPostCommunicationsEmailRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, billingEntity, toAddress, subject, body) {
      obj['billingEntity'] = billingEntity;
      obj['toAddress'] = toAddress;
      obj['subject'] = subject;
      obj['body'] = body;
    }
    /**
     * Constructs a <code>EmailPostCommunicationsEmailRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EmailPostCommunicationsEmailRequestBody} obj Optional instance to populate.
     * @return {module:model/EmailPostCommunicationsEmailRequestBody} The populated <code>EmailPostCommunicationsEmailRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EmailPostCommunicationsEmailRequestBody();

        if (data.hasOwnProperty('billingEntity')) {
          obj['billingEntity'] = _ApiClient["default"].convertToType(data['billingEntity'], 'String');
        }

        if (data.hasOwnProperty('toAddress')) {
          obj['toAddress'] = _ApiClient["default"].convertToType(data['toAddress'], 'String');
        }

        if (data.hasOwnProperty('subject')) {
          obj['subject'] = _ApiClient["default"].convertToType(data['subject'], 'String');
        }

        if (data.hasOwnProperty('body')) {
          obj['body'] = _ApiClient["default"].convertToType(data['body'], 'String');
        }
      }

      return obj;
    }
  }]);

  return EmailPostCommunicationsEmailRequestBody;
}();
/**
 * Code of the billing entity for this web portal
 * @member {String} billingEntity
 */


EmailPostCommunicationsEmailRequestBody.prototype['billingEntity'] = undefined;
/**
 * Recipient's email address
 * @member {String} toAddress
 */

EmailPostCommunicationsEmailRequestBody.prototype['toAddress'] = undefined;
/**
 * Subject text
 * @member {String} subject
 */

EmailPostCommunicationsEmailRequestBody.prototype['subject'] = undefined;
/**
 * Email's body text
 * @member {String} body
 */

EmailPostCommunicationsEmailRequestBody.prototype['body'] = undefined;
var _default = EmailPostCommunicationsEmailRequestBody;
exports["default"] = _default;