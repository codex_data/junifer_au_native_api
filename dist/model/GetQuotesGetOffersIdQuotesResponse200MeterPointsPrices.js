"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents = _interopRequireDefault(require("./GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices model module.
 * @module model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices
 * @version 1.61.1
 */
var GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices</code>.
   * Array of prices associated with this meter point for this quote
   * @alias module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices
   * @param name {String} Group name
   * @param _class {String} Group class
   * @param priceContents {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents} 
   */
  function GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices(name, _class, priceContents) {
    _classCallCheck(this, GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices);

    GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices.initialize(this, name, _class, priceContents);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices, null, [{
    key: "initialize",
    value: function initialize(obj, name, _class, priceContents) {
      obj['name'] = name;
      obj['class'] = _class;
      obj['priceContents'] = priceContents;
    }
    /**
     * Constructs a <code>GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices} obj Optional instance to populate.
     * @return {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices} The populated <code>GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices();

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('class')) {
          obj['class'] = _ApiClient["default"].convertToType(data['class'], 'String');
        }

        if (data.hasOwnProperty('priceContents')) {
          obj['priceContents'] = _GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents["default"].constructFromObject(data['priceContents']);
        }
      }

      return obj;
    }
  }]);

  return GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices;
}();
/**
 * Group name
 * @member {String} name
 */


GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices.prototype['name'] = undefined;
/**
 * Group class
 * @member {String} class
 */

GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices.prototype['class'] = undefined;
/**
 * @member {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents} priceContents
 */

GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices.prototype['priceContents'] = undefined;
var _default = GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices;
exports["default"] = _default;