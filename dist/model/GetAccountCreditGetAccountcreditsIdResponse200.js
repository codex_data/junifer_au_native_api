"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountCreditGetAccountcreditsIdResponse200 model module.
 * @module model/GetAccountCreditGetAccountcreditsIdResponse200
 * @version 1.61.1
 */
var GetAccountCreditGetAccountcreditsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountCreditGetAccountcreditsIdResponse200</code>.
   * @alias module:model/GetAccountCreditGetAccountcreditsIdResponse200
   * @param id {Number} Account credit Identifier
   * @param createdDttm {Date} Date and time when the credit was created
   * @param acceptedDttm {Date} Date and time when the credit was accepted
   * @param grossAmount {Number} Gross amount of account credit
   * @param netAmount {Number} Net amount of account credit
   * @param salesTaxAmount {Number} Amount of sales tax for this account credit
   * @param reason {String} Name of linked account credit reason
   */
  function GetAccountCreditGetAccountcreditsIdResponse200(id, createdDttm, acceptedDttm, grossAmount, netAmount, salesTaxAmount, reason) {
    _classCallCheck(this, GetAccountCreditGetAccountcreditsIdResponse200);

    GetAccountCreditGetAccountcreditsIdResponse200.initialize(this, id, createdDttm, acceptedDttm, grossAmount, netAmount, salesTaxAmount, reason);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountCreditGetAccountcreditsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, createdDttm, acceptedDttm, grossAmount, netAmount, salesTaxAmount, reason) {
      obj['id'] = id;
      obj['createdDttm'] = createdDttm;
      obj['acceptedDttm'] = acceptedDttm;
      obj['grossAmount'] = grossAmount;
      obj['netAmount'] = netAmount;
      obj['salesTaxAmount'] = salesTaxAmount;
      obj['reason'] = reason;
    }
    /**
     * Constructs a <code>GetAccountCreditGetAccountcreditsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountCreditGetAccountcreditsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountCreditGetAccountcreditsIdResponse200} The populated <code>GetAccountCreditGetAccountcreditsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountCreditGetAccountcreditsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('acceptedDttm')) {
          obj['acceptedDttm'] = _ApiClient["default"].convertToType(data['acceptedDttm'], 'Date');
        }

        if (data.hasOwnProperty('grossAmount')) {
          obj['grossAmount'] = _ApiClient["default"].convertToType(data['grossAmount'], 'Number');
        }

        if (data.hasOwnProperty('netAmount')) {
          obj['netAmount'] = _ApiClient["default"].convertToType(data['netAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTax')) {
          obj['salesTax'] = _ApiClient["default"].convertToType(data['salesTax'], 'String');
        }

        if (data.hasOwnProperty('salesTaxAmount')) {
          obj['salesTaxAmount'] = _ApiClient["default"].convertToType(data['salesTaxAmount'], 'Number');
        }

        if (data.hasOwnProperty('reason')) {
          obj['reason'] = _ApiClient["default"].convertToType(data['reason'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('cancelledDttm')) {
          obj['cancelledDttm'] = _ApiClient["default"].convertToType(data['cancelledDttm'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return GetAccountCreditGetAccountcreditsIdResponse200;
}();
/**
 * Account credit Identifier
 * @member {Number} id
 */


GetAccountCreditGetAccountcreditsIdResponse200.prototype['id'] = undefined;
/**
 * Date and time when the credit was created
 * @member {Date} createdDttm
 */

GetAccountCreditGetAccountcreditsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * Date and time when the credit was accepted
 * @member {Date} acceptedDttm
 */

GetAccountCreditGetAccountcreditsIdResponse200.prototype['acceptedDttm'] = undefined;
/**
 * Gross amount of account credit
 * @member {Number} grossAmount
 */

GetAccountCreditGetAccountcreditsIdResponse200.prototype['grossAmount'] = undefined;
/**
 * Net amount of account credit
 * @member {Number} netAmount
 */

GetAccountCreditGetAccountcreditsIdResponse200.prototype['netAmount'] = undefined;
/**
 * Name of linked sales tax to this account credit
 * @member {String} salesTax
 */

GetAccountCreditGetAccountcreditsIdResponse200.prototype['salesTax'] = undefined;
/**
 * Amount of sales tax for this account credit
 * @member {Number} salesTaxAmount
 */

GetAccountCreditGetAccountcreditsIdResponse200.prototype['salesTaxAmount'] = undefined;
/**
 * Name of linked account credit reason
 * @member {String} reason
 */

GetAccountCreditGetAccountcreditsIdResponse200.prototype['reason'] = undefined;
/**
 * Custom text reference for account credit
 * @member {String} reference
 */

GetAccountCreditGetAccountcreditsIdResponse200.prototype['reference'] = undefined;
/**
 * Date and time when the account debit was cancelled
 * @member {Date} cancelledDttm
 */

GetAccountCreditGetAccountcreditsIdResponse200.prototype['cancelledDttm'] = undefined;
var _default = GetAccountCreditGetAccountcreditsIdResponse200;
exports["default"] = _default;