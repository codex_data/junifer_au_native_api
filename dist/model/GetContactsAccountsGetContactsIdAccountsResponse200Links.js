"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetContactsAccountsGetContactsIdAccountsResponse200Links model module.
 * @module model/GetContactsAccountsGetContactsIdAccountsResponse200Links
 * @version 1.61.1
 */
var GetContactsAccountsGetContactsIdAccountsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetContactsAccountsGetContactsIdAccountsResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetContactsAccountsGetContactsIdAccountsResponse200Links
   * @param self {String} Link referring to this account
   * @param customer {String} Link to this account's customer record
   * @param contacts {String} Link to this account's contacts
   * @param bills {String} Link to account's bills
   * @param paymentPlans {String} Link to account's payment plans
   * @param payments {String} Link to account's payments
   * @param paymentMethods {String} Link to account's payment methods
   * @param agreements {String} Link to account's agreements
   * @param paymentSchedulePeriods {String} Link to account's payment schedule periods
   * @param paymentSchedulesSuggestedPaymentAmount {String} Link to account's suggested payment amount
   * @param tickets {String} Link to account's tickets
   * @param productDetails {String} Link to account's product details
   * @param propertys {String} Link to account's propertys
   */
  function GetContactsAccountsGetContactsIdAccountsResponse200Links(self, customer, contacts, bills, paymentPlans, payments, paymentMethods, agreements, paymentSchedulePeriods, paymentSchedulesSuggestedPaymentAmount, tickets, productDetails, propertys) {
    _classCallCheck(this, GetContactsAccountsGetContactsIdAccountsResponse200Links);

    GetContactsAccountsGetContactsIdAccountsResponse200Links.initialize(this, self, customer, contacts, bills, paymentPlans, payments, paymentMethods, agreements, paymentSchedulePeriods, paymentSchedulesSuggestedPaymentAmount, tickets, productDetails, propertys);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetContactsAccountsGetContactsIdAccountsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, customer, contacts, bills, paymentPlans, payments, paymentMethods, agreements, paymentSchedulePeriods, paymentSchedulesSuggestedPaymentAmount, tickets, productDetails, propertys) {
      obj['self'] = self;
      obj['customer'] = customer;
      obj['contacts'] = contacts;
      obj['bills'] = bills;
      obj['paymentPlans'] = paymentPlans;
      obj['payments'] = payments;
      obj['paymentMethods'] = paymentMethods;
      obj['agreements'] = agreements;
      obj['paymentSchedulePeriods'] = paymentSchedulePeriods;
      obj['paymentSchedules/suggestedPaymentAmount'] = paymentSchedulesSuggestedPaymentAmount;
      obj['tickets'] = tickets;
      obj['productDetails'] = productDetails;
      obj['propertys'] = propertys;
    }
    /**
     * Constructs a <code>GetContactsAccountsGetContactsIdAccountsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetContactsAccountsGetContactsIdAccountsResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetContactsAccountsGetContactsIdAccountsResponse200Links} The populated <code>GetContactsAccountsGetContactsIdAccountsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetContactsAccountsGetContactsIdAccountsResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('customer')) {
          obj['customer'] = _ApiClient["default"].convertToType(data['customer'], 'String');
        }

        if (data.hasOwnProperty('contacts')) {
          obj['contacts'] = _ApiClient["default"].convertToType(data['contacts'], 'String');
        }

        if (data.hasOwnProperty('bills')) {
          obj['bills'] = _ApiClient["default"].convertToType(data['bills'], 'String');
        }

        if (data.hasOwnProperty('paymentPlans')) {
          obj['paymentPlans'] = _ApiClient["default"].convertToType(data['paymentPlans'], 'String');
        }

        if (data.hasOwnProperty('payments')) {
          obj['payments'] = _ApiClient["default"].convertToType(data['payments'], 'String');
        }

        if (data.hasOwnProperty('paymentMethods')) {
          obj['paymentMethods'] = _ApiClient["default"].convertToType(data['paymentMethods'], 'String');
        }

        if (data.hasOwnProperty('agreements')) {
          obj['agreements'] = _ApiClient["default"].convertToType(data['agreements'], 'String');
        }

        if (data.hasOwnProperty('paymentSchedulePeriods')) {
          obj['paymentSchedulePeriods'] = _ApiClient["default"].convertToType(data['paymentSchedulePeriods'], 'String');
        }

        if (data.hasOwnProperty('paymentSchedules/suggestedPaymentAmount')) {
          obj['paymentSchedules/suggestedPaymentAmount'] = _ApiClient["default"].convertToType(data['paymentSchedules/suggestedPaymentAmount'], 'String');
        }

        if (data.hasOwnProperty('tickets')) {
          obj['tickets'] = _ApiClient["default"].convertToType(data['tickets'], 'String');
        }

        if (data.hasOwnProperty('productDetails')) {
          obj['productDetails'] = _ApiClient["default"].convertToType(data['productDetails'], 'String');
        }

        if (data.hasOwnProperty('propertys')) {
          obj['propertys'] = _ApiClient["default"].convertToType(data['propertys'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetContactsAccountsGetContactsIdAccountsResponse200Links;
}();
/**
 * Link referring to this account
 * @member {String} self
 */


GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['self'] = undefined;
/**
 * Link to this account's customer record
 * @member {String} customer
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['customer'] = undefined;
/**
 * Link to this account's contacts
 * @member {String} contacts
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['contacts'] = undefined;
/**
 * Link to account's bills
 * @member {String} bills
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['bills'] = undefined;
/**
 * Link to account's payment plans
 * @member {String} paymentPlans
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['paymentPlans'] = undefined;
/**
 * Link to account's payments
 * @member {String} payments
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['payments'] = undefined;
/**
 * Link to account's payment methods
 * @member {String} paymentMethods
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['paymentMethods'] = undefined;
/**
 * Link to account's agreements
 * @member {String} agreements
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['agreements'] = undefined;
/**
 * Link to account's payment schedule periods
 * @member {String} paymentSchedulePeriods
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['paymentSchedulePeriods'] = undefined;
/**
 * Link to account's suggested payment amount
 * @member {String} paymentSchedules/suggestedPaymentAmount
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['paymentSchedules/suggestedPaymentAmount'] = undefined;
/**
 * Link to account's tickets
 * @member {String} tickets
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['tickets'] = undefined;
/**
 * Link to account's product details
 * @member {String} productDetails
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['productDetails'] = undefined;
/**
 * Link to account's propertys
 * @member {String} propertys
 */

GetContactsAccountsGetContactsIdAccountsResponse200Links.prototype['propertys'] = undefined;
var _default = GetContactsAccountsGetContactsIdAccountsResponse200Links;
exports["default"] = _default;