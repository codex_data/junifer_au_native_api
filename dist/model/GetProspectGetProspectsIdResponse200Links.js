"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetProspectGetProspectsIdResponse200Links model module.
 * @module model/GetProspectGetProspectsIdResponse200Links
 * @version 1.61.1
 */
var GetProspectGetProspectsIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetProspectGetProspectsIdResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetProspectGetProspectsIdResponse200Links
   * @param self {String} Link referring to this prospect
   * @param customer {String} Link to this prospect's customer information
   * @param propertys {String} Link to this prospect's property information
   */
  function GetProspectGetProspectsIdResponse200Links(self, customer, propertys) {
    _classCallCheck(this, GetProspectGetProspectsIdResponse200Links);

    GetProspectGetProspectsIdResponse200Links.initialize(this, self, customer, propertys);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetProspectGetProspectsIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, customer, propertys) {
      obj['self'] = self;
      obj['customer'] = customer;
      obj['propertys'] = propertys;
    }
    /**
     * Constructs a <code>GetProspectGetProspectsIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetProspectGetProspectsIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetProspectGetProspectsIdResponse200Links} The populated <code>GetProspectGetProspectsIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetProspectGetProspectsIdResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('customer')) {
          obj['customer'] = _ApiClient["default"].convertToType(data['customer'], 'String');
        }

        if (data.hasOwnProperty('propertys')) {
          obj['propertys'] = _ApiClient["default"].convertToType(data['propertys'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetProspectGetProspectsIdResponse200Links;
}();
/**
 * Link referring to this prospect
 * @member {String} self
 */


GetProspectGetProspectsIdResponse200Links.prototype['self'] = undefined;
/**
 * Link to this prospect's customer information
 * @member {String} customer
 */

GetProspectGetProspectsIdResponse200Links.prototype['customer'] = undefined;
/**
 * Link to this prospect's property information
 * @member {String} propertys
 */

GetProspectGetProspectsIdResponse200Links.prototype['propertys'] = undefined;
var _default = GetProspectGetProspectsIdResponse200Links;
exports["default"] = _default;