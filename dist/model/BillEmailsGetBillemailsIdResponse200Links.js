"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The BillEmailsGetBillemailsIdResponse200Links model module.
 * @module model/BillEmailsGetBillemailsIdResponse200Links
 * @version 1.61.1
 */
var BillEmailsGetBillemailsIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>BillEmailsGetBillemailsIdResponse200Links</code>.
   * Links to resources related to the attachment
   * @alias module:model/BillEmailsGetBillemailsIdResponse200Links
   * @param self {String} Link to attachment itself
   * @param image {String} Link to attachment image
   */
  function BillEmailsGetBillemailsIdResponse200Links(self, image) {
    _classCallCheck(this, BillEmailsGetBillemailsIdResponse200Links);

    BillEmailsGetBillemailsIdResponse200Links.initialize(this, self, image);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(BillEmailsGetBillemailsIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, image) {
      obj['self'] = self;
      obj['image'] = image;
    }
    /**
     * Constructs a <code>BillEmailsGetBillemailsIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/BillEmailsGetBillemailsIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/BillEmailsGetBillemailsIdResponse200Links} The populated <code>BillEmailsGetBillemailsIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new BillEmailsGetBillemailsIdResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('image')) {
          obj['image'] = _ApiClient["default"].convertToType(data['image'], 'String');
        }
      }

      return obj;
    }
  }]);

  return BillEmailsGetBillemailsIdResponse200Links;
}();
/**
 * Link to attachment itself
 * @member {String} self
 */


BillEmailsGetBillemailsIdResponse200Links.prototype['self'] = undefined;
/**
 * Link to attachment image
 * @member {String} image
 */

BillEmailsGetBillemailsIdResponse200Links.prototype['image'] = undefined;
var _default = BillEmailsGetBillemailsIdResponse200Links;
exports["default"] = _default;