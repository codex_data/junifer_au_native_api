"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200 model module.
 * @module model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200
 * @version 1.61.1
 */
var GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200</code>.
   * @alias module:model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200
   * @param utilityMarket {Array.<Object>} Utility Market. Can be one of the following: `UK Electricity or UK Gas`
   */
  function GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200(utilityMarket) {
    _classCallCheck(this, GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200);

    GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200.initialize(this, utilityMarket);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, utilityMarket) {
      obj['utilityMarket'] = utilityMarket;
    }
    /**
     * Constructs a <code>GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200} obj Optional instance to populate.
     * @return {module:model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200} The populated <code>GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200();

        if (data.hasOwnProperty('utilityMarket')) {
          obj['utilityMarket'] = _ApiClient["default"].convertToType(data['utilityMarket'], [Object]);
        }
      }

      return obj;
    }
  }]);

  return GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200;
}();
/**
 * Utility Market. Can be one of the following: `UK Electricity or UK Gas`
 * @member {Array.<Object>} utilityMarket
 */


GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200.prototype['utilityMarket'] = undefined;
var _default = GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200;
exports["default"] = _default;