"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetProspectOffersGetProspectsIdOffersResponse200Links = _interopRequireDefault(require("./GetProspectOffersGetProspectsIdOffersResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetProspectOffersGetProspectsIdOffersResponse200 model module.
 * @module model/GetProspectOffersGetProspectsIdOffersResponse200
 * @version 1.61.1
 */
var GetProspectOffersGetProspectsIdOffersResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetProspectOffersGetProspectsIdOffersResponse200</code>.
   * @alias module:model/GetProspectOffersGetProspectsIdOffersResponse200
   * @param number {Number} The offer's number.
   * @param fromDttm {Date} The offer's from date and time.
   * @param toDttm {Date} The offer's to date and time.
   * @param links {module:model/GetProspectOffersGetProspectsIdOffersResponse200Links} 
   */
  function GetProspectOffersGetProspectsIdOffersResponse200(number, fromDttm, toDttm, links) {
    _classCallCheck(this, GetProspectOffersGetProspectsIdOffersResponse200);

    GetProspectOffersGetProspectsIdOffersResponse200.initialize(this, number, fromDttm, toDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetProspectOffersGetProspectsIdOffersResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, number, fromDttm, toDttm, links) {
      obj['number'] = number;
      obj['fromDttm'] = fromDttm;
      obj['toDttm'] = toDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetProspectOffersGetProspectsIdOffersResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetProspectOffersGetProspectsIdOffersResponse200} obj Optional instance to populate.
     * @return {module:model/GetProspectOffersGetProspectsIdOffersResponse200} The populated <code>GetProspectOffersGetProspectsIdOffersResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetProspectOffersGetProspectsIdOffersResponse200();

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'Number');
        }

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetProspectOffersGetProspectsIdOffersResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetProspectOffersGetProspectsIdOffersResponse200;
}();
/**
 * The offer's number.
 * @member {Number} number
 */


GetProspectOffersGetProspectsIdOffersResponse200.prototype['number'] = undefined;
/**
 * The offer's from date and time.
 * @member {Date} fromDttm
 */

GetProspectOffersGetProspectsIdOffersResponse200.prototype['fromDttm'] = undefined;
/**
 * The offer's to date and time.
 * @member {Date} toDttm
 */

GetProspectOffersGetProspectsIdOffersResponse200.prototype['toDttm'] = undefined;
/**
 * @member {module:model/GetProspectOffersGetProspectsIdOffersResponse200Links} links
 */

GetProspectOffersGetProspectsIdOffersResponse200.prototype['links'] = undefined;
var _default = GetProspectOffersGetProspectsIdOffersResponse200;
exports["default"] = _default;