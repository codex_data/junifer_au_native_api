"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetOfferGetOffersIdResponse200QuotesLinks model module.
 * @module model/GetOfferGetOffersIdResponse200QuotesLinks
 * @version 1.61.1
 */
var GetOfferGetOffersIdResponse200QuotesLinks = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetOfferGetOffersIdResponse200QuotesLinks</code>.
   * The links to any related content associated with the quote.
   * @alias module:model/GetOfferGetOffersIdResponse200QuotesLinks
   * @param self {String} The link to the quote itself.
   */
  function GetOfferGetOffersIdResponse200QuotesLinks(self) {
    _classCallCheck(this, GetOfferGetOffersIdResponse200QuotesLinks);

    GetOfferGetOffersIdResponse200QuotesLinks.initialize(this, self);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetOfferGetOffersIdResponse200QuotesLinks, null, [{
    key: "initialize",
    value: function initialize(obj, self) {
      obj['self'] = self;
    }
    /**
     * Constructs a <code>GetOfferGetOffersIdResponse200QuotesLinks</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetOfferGetOffersIdResponse200QuotesLinks} obj Optional instance to populate.
     * @return {module:model/GetOfferGetOffersIdResponse200QuotesLinks} The populated <code>GetOfferGetOffersIdResponse200QuotesLinks</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetOfferGetOffersIdResponse200QuotesLinks();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetOfferGetOffersIdResponse200QuotesLinks;
}();
/**
 * The link to the quote itself.
 * @member {String} self
 */


GetOfferGetOffersIdResponse200QuotesLinks.prototype['self'] = undefined;
var _default = GetOfferGetOffersIdResponse200QuotesLinks;
exports["default"] = _default;