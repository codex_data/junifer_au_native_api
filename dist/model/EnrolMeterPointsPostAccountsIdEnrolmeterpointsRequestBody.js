"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody model module.
 * @module model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody
 * @version 1.61.1
 */
var EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody</code>.
   * @alias module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody
   * @param senderReference {String} Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected
   * @param reference {String} Unique reference string identifying this particular enrolment
   * @param marketingOptOutFl {Boolean} A boolean flag indicating whether the customer would like to opt out of any marketing communications
   * @param supplyAddress {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress} 
   * @param electricityProduct {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct} 
   * @param gasProduct {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct} 
   */
  function EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody(senderReference, reference, marketingOptOutFl, supplyAddress, electricityProduct, gasProduct) {
    _classCallCheck(this, EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody);

    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.initialize(this, senderReference, reference, marketingOptOutFl, supplyAddress, electricityProduct, gasProduct);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, senderReference, reference, marketingOptOutFl, supplyAddress, electricityProduct, gasProduct) {
      obj['senderReference'] = senderReference;
      obj['reference'] = reference;
      obj['marketingOptOutFl'] = marketingOptOutFl;
      obj['supplyAddress'] = supplyAddress;
      obj['electricityProduct'] = electricityProduct;
      obj['gasProduct'] = gasProduct;
    }
    /**
     * Constructs a <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody} obj Optional instance to populate.
     * @return {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody} The populated <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody();

        if (data.hasOwnProperty('senderReference')) {
          obj['senderReference'] = _ApiClient["default"].convertToType(data['senderReference'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('billingEntityCode')) {
          obj['billingEntityCode'] = _ApiClient["default"].convertToType(data['billingEntityCode'], 'String');
        }

        if (data.hasOwnProperty('marketingOptOutFl')) {
          obj['marketingOptOutFl'] = _ApiClient["default"].convertToType(data['marketingOptOutFl'], 'Boolean');
        }

        if (data.hasOwnProperty('submittedSource')) {
          obj['submittedSource'] = _ApiClient["default"].convertToType(data['submittedSource'], 'String');
        }

        if (data.hasOwnProperty('changeOfTenancyFl')) {
          obj['changeOfTenancyFl'] = _ApiClient["default"].convertToType(data['changeOfTenancyFl'], 'Boolean');
        }

        if (data.hasOwnProperty('supplyAddress')) {
          obj['supplyAddress'] = _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress["default"].constructFromObject(data['supplyAddress']);
        }

        if (data.hasOwnProperty('electricityProduct')) {
          obj['electricityProduct'] = _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct["default"].constructFromObject(data['electricityProduct']);
        }

        if (data.hasOwnProperty('gasProduct')) {
          obj['gasProduct'] = _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct["default"].constructFromObject(data['gasProduct']);
        }

        if (data.hasOwnProperty('gasEstimate')) {
          obj['gasEstimate'] = _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate["default"].constructFromObject(data['gasEstimate']);
        }

        if (data.hasOwnProperty('electricityEstimate')) {
          obj['electricityEstimate'] = _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate["default"].constructFromObject(data['electricityEstimate']);
        }
      }

      return obj;
    }
  }]);

  return EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody;
}();
/**
 * Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected
 * @member {String} senderReference
 */


EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['senderReference'] = undefined;
/**
 * Unique reference string identifying this particular enrolment
 * @member {String} reference
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['reference'] = undefined;
/**
 * Code of the billing entity to which the customer must be attached
 * @member {String} billingEntityCode
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['billingEntityCode'] = undefined;
/**
 * A boolean flag indicating whether the customer would like to opt out of any marketing communications
 * @member {Boolean} marketingOptOutFl
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['marketingOptOutFl'] = undefined;
/**
 * The source of the enrolment. This overrides the default source (WebService) if set
 * @member {String} submittedSource
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['submittedSource'] = undefined;
/**
 * A boolean flag indicating whether the customer is a change of tenancy.(Request Parameter)
 * @member {Boolean} changeOfTenancyFl
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['changeOfTenancyFl'] = undefined;
/**
 * @member {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress} supplyAddress
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['supplyAddress'] = undefined;
/**
 * @member {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct} electricityProduct
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['electricityProduct'] = undefined;
/**
 * @member {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct} gasProduct
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['gasProduct'] = undefined;
/**
 * @member {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate} gasEstimate
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['gasEstimate'] = undefined;
/**
 * @member {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate} electricityEstimate
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.prototype['electricityEstimate'] = undefined;
var _default = EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody;
exports["default"] = _default;