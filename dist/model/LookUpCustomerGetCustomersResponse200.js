"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _LookUpCustomerGetCustomersResponse200CompanyAddress = _interopRequireDefault(require("./LookUpCustomerGetCustomersResponse200CompanyAddress"));

var _LookUpCustomerGetCustomersResponse200Links = _interopRequireDefault(require("./LookUpCustomerGetCustomersResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The LookUpCustomerGetCustomersResponse200 model module.
 * @module model/LookUpCustomerGetCustomersResponse200
 * @version 1.61.1
 */
var LookUpCustomerGetCustomersResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>LookUpCustomerGetCustomersResponse200</code>.
   * @alias module:model/LookUpCustomerGetCustomersResponse200
   * @param id {Number} Customer ID
   * @param name {String} Customer's full name
   * @param number {String} Customer number
   * @param customerClass {String} Customer class this customer belongs to
   * @param customerType {String} Customer type
   * @param state {String} Customer state. Possible values include `Prospect, Active, Terminated`
   * @param forename {String} Customer's first name
   * @param surname {String} Customer's last name
   * @param bereavementFl {Boolean} Is the Customer's Deceased
   * @param marketingOptOutFl {Boolean} Customer's choice for receiving marketing communications
   * @param links {module:model/LookUpCustomerGetCustomersResponse200Links} 
   */
  function LookUpCustomerGetCustomersResponse200(id, name, number, customerClass, customerType, state, forename, surname, bereavementFl, marketingOptOutFl, links) {
    _classCallCheck(this, LookUpCustomerGetCustomersResponse200);

    LookUpCustomerGetCustomersResponse200.initialize(this, id, name, number, customerClass, customerType, state, forename, surname, bereavementFl, marketingOptOutFl, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(LookUpCustomerGetCustomersResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, name, number, customerClass, customerType, state, forename, surname, bereavementFl, marketingOptOutFl, links) {
      obj['id'] = id;
      obj['name'] = name;
      obj['number'] = number;
      obj['customerClass'] = customerClass;
      obj['customerType'] = customerType;
      obj['state'] = state;
      obj['forename'] = forename;
      obj['surname'] = surname;
      obj['bereavementFl'] = bereavementFl;
      obj['marketingOptOutFl'] = marketingOptOutFl;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>LookUpCustomerGetCustomersResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LookUpCustomerGetCustomersResponse200} obj Optional instance to populate.
     * @return {module:model/LookUpCustomerGetCustomersResponse200} The populated <code>LookUpCustomerGetCustomersResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new LookUpCustomerGetCustomersResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('customerClass')) {
          obj['customerClass'] = _ApiClient["default"].convertToType(data['customerClass'], 'String');
        }

        if (data.hasOwnProperty('customerType')) {
          obj['customerType'] = _ApiClient["default"].convertToType(data['customerType'], 'String');
        }

        if (data.hasOwnProperty('state')) {
          obj['state'] = _ApiClient["default"].convertToType(data['state'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('companyName')) {
          obj['companyName'] = _ApiClient["default"].convertToType(data['companyName'], 'String');
        }

        if (data.hasOwnProperty('companyNumber')) {
          obj['companyNumber'] = _ApiClient["default"].convertToType(data['companyNumber'], 'String');
        }

        if (data.hasOwnProperty('companyAddress')) {
          obj['companyAddress'] = _LookUpCustomerGetCustomersResponse200CompanyAddress["default"].constructFromObject(data['companyAddress']);
        }

        if (data.hasOwnProperty('bereavementFl')) {
          obj['bereavementFl'] = _ApiClient["default"].convertToType(data['bereavementFl'], 'Boolean');
        }

        if (data.hasOwnProperty('marketingOptOutFl')) {
          obj['marketingOptOutFl'] = _ApiClient["default"].convertToType(data['marketingOptOutFl'], 'Boolean');
        }

        if (data.hasOwnProperty('creditScore')) {
          obj['creditScore'] = _ApiClient["default"].convertToType(data['creditScore'], 'Number');
        }

        if (data.hasOwnProperty('taxExemptReason')) {
          obj['taxExemptReason'] = _ApiClient["default"].convertToType(data['taxExemptReason'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _LookUpCustomerGetCustomersResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return LookUpCustomerGetCustomersResponse200;
}();
/**
 * Customer ID
 * @member {Number} id
 */


LookUpCustomerGetCustomersResponse200.prototype['id'] = undefined;
/**
 * Customer's full name
 * @member {String} name
 */

LookUpCustomerGetCustomersResponse200.prototype['name'] = undefined;
/**
 * Customer number
 * @member {String} number
 */

LookUpCustomerGetCustomersResponse200.prototype['number'] = undefined;
/**
 * Customer class this customer belongs to
 * @member {String} customerClass
 */

LookUpCustomerGetCustomersResponse200.prototype['customerClass'] = undefined;
/**
 * Customer type
 * @member {String} customerType
 */

LookUpCustomerGetCustomersResponse200.prototype['customerType'] = undefined;
/**
 * Customer state. Possible values include `Prospect, Active, Terminated`
 * @member {String} state
 */

LookUpCustomerGetCustomersResponse200.prototype['state'] = undefined;
/**
 * Customer's first name
 * @member {String} forename
 */

LookUpCustomerGetCustomersResponse200.prototype['forename'] = undefined;
/**
 * Customer's last name
 * @member {String} surname
 */

LookUpCustomerGetCustomersResponse200.prototype['surname'] = undefined;
/**
 * Customer's company name
 * @member {String} companyName
 */

LookUpCustomerGetCustomersResponse200.prototype['companyName'] = undefined;
/**
 * Customer's company number
 * @member {String} companyNumber
 */

LookUpCustomerGetCustomersResponse200.prototype['companyNumber'] = undefined;
/**
 * @member {module:model/LookUpCustomerGetCustomersResponse200CompanyAddress} companyAddress
 */

LookUpCustomerGetCustomersResponse200.prototype['companyAddress'] = undefined;
/**
 * Is the Customer's Deceased
 * @member {Boolean} bereavementFl
 */

LookUpCustomerGetCustomersResponse200.prototype['bereavementFl'] = undefined;
/**
 * Customer's choice for receiving marketing communications
 * @member {Boolean} marketingOptOutFl
 */

LookUpCustomerGetCustomersResponse200.prototype['marketingOptOutFl'] = undefined;
/**
 * Customer's credit score
 * @member {Number} creditScore
 */

LookUpCustomerGetCustomersResponse200.prototype['creditScore'] = undefined;
/**
 * Customer's tax exempt reason if applicable
 * @member {String} taxExemptReason
 */

LookUpCustomerGetCustomersResponse200.prototype['taxExemptReason'] = undefined;
/**
 * @member {module:model/LookUpCustomerGetCustomersResponse200Links} links
 */

LookUpCustomerGetCustomersResponse200.prototype['links'] = undefined;
var _default = LookUpCustomerGetCustomersResponse200;
exports["default"] = _default;