"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links = _interopRequireDefault(require("./GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links"));

var _GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions = _interopRequireDefault(require("./GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200 model module.
 * @module model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200
 * @version 1.61.1
 */
var GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200</code>.
   * @alias module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200
   * @param id {Number} The id of payment plan
   * @param createdDttm {Date} Date and time when the payment plan was created within Junifer
   * @param collectType {String} the way to collect payment
   * @param currency {String} Currency of the payment plan
   * @param paymentPlanTransactions {Array.<module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions>} PaymentPlanTransactions Array
   * @param links {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links} 
   */
  function GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200(id, createdDttm, collectType, currency, paymentPlanTransactions, links) {
    _classCallCheck(this, GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200);

    GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200.initialize(this, id, createdDttm, collectType, currency, paymentPlanTransactions, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, createdDttm, collectType, currency, paymentPlanTransactions, links) {
      obj['id'] = id;
      obj['createdDttm'] = createdDttm;
      obj['collectType'] = collectType;
      obj['currency'] = currency;
      obj['paymentPlanTransactions'] = paymentPlanTransactions;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200} The populated <code>GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('collectType')) {
          obj['collectType'] = _ApiClient["default"].convertToType(data['collectType'], 'String');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('paymentPlanTransactions')) {
          obj['paymentPlanTransactions'] = _ApiClient["default"].convertToType(data['paymentPlanTransactions'], [_GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions["default"]]);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200;
}();
/**
 * The id of payment plan
 * @member {Number} id
 */


GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200.prototype['id'] = undefined;
/**
 * Date and time when the payment plan was created within Junifer
 * @member {Date} createdDttm
 */

GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200.prototype['createdDttm'] = undefined;
/**
 * the way to collect payment
 * @member {String} collectType
 */

GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200.prototype['collectType'] = undefined;
/**
 * Currency of the payment plan
 * @member {String} currency
 */

GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200.prototype['currency'] = undefined;
/**
 * PaymentPlanTransactions Array
 * @member {Array.<module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions>} paymentPlanTransactions
 */

GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200.prototype['paymentPlanTransactions'] = undefined;
/**
 * @member {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links} links
 */

GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200.prototype['links'] = undefined;
var _default = GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200;
exports["default"] = _default;