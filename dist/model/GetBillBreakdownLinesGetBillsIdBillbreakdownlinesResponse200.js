"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200 model module.
 * @module model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200
 * @version 1.61.1
 */
var GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200</code>.
   * @alias module:model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200
   * @param id {Number} ID of Bill Breakdown Line
   * @param description {String} Description of the Bill Breakdown Line
   * @param currency {String} Currency ISO code
   * @param billCurrencyAmount {Number} Bill Breakdown amount
   * @param salesTaxRate {Number} Sales Tax rate for the Bill Breakdown
   * @param code {String} Code for the Bill breakdown Line
   * @param linkage {String} Identifier for linked Asset or Property's address
   */
  function GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200(id, description, currency, billCurrencyAmount, salesTaxRate, code, linkage) {
    _classCallCheck(this, GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200);

    GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.initialize(this, id, description, currency, billCurrencyAmount, salesTaxRate, code, linkage);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, description, currency, billCurrencyAmount, salesTaxRate, code, linkage) {
      obj['id'] = id;
      obj['description'] = description;
      obj['currency'] = currency;
      obj['billCurrencyAmount'] = billCurrencyAmount;
      obj['salesTaxRate'] = salesTaxRate;
      obj['code'] = code;
      obj['linkage'] = linkage;
    }
    /**
     * Constructs a <code>GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200} obj Optional instance to populate.
     * @return {module:model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200} The populated <code>GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('billCurrencyAmount')) {
          obj['billCurrencyAmount'] = _ApiClient["default"].convertToType(data['billCurrencyAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTaxRate')) {
          obj['salesTaxRate'] = _ApiClient["default"].convertToType(data['salesTaxRate'], 'Number');
        }

        if (data.hasOwnProperty('code')) {
          obj['code'] = _ApiClient["default"].convertToType(data['code'], 'String');
        }

        if (data.hasOwnProperty('linkage')) {
          obj['linkage'] = _ApiClient["default"].convertToType(data['linkage'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200;
}();
/**
 * ID of Bill Breakdown Line
 * @member {Number} id
 */


GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.prototype['id'] = undefined;
/**
 * Description of the Bill Breakdown Line
 * @member {String} description
 */

GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.prototype['description'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.prototype['currency'] = undefined;
/**
 * Bill Breakdown amount
 * @member {Number} billCurrencyAmount
 */

GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.prototype['billCurrencyAmount'] = undefined;
/**
 * Sales Tax rate for the Bill Breakdown
 * @member {Number} salesTaxRate
 */

GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.prototype['salesTaxRate'] = undefined;
/**
 * Code for the Bill breakdown Line
 * @member {String} code
 */

GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.prototype['code'] = undefined;
/**
 * Identifier for linked Asset or Property's address
 * @member {String} linkage
 */

GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.prototype['linkage'] = undefined;
var _default = GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200;
exports["default"] = _default;