"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateProspectPutCustomersIdCreateprospectResponse200 model module.
 * @module model/CreateProspectPutCustomersIdCreateprospectResponse200
 * @version 1.61.1
 */
var CreateProspectPutCustomersIdCreateprospectResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProspectPutCustomersIdCreateprospectResponse200</code>.
   * @alias module:model/CreateProspectPutCustomersIdCreateprospectResponse200
   * @param prospectId {Number} Newly created prospect ID
   */
  function CreateProspectPutCustomersIdCreateprospectResponse200(prospectId) {
    _classCallCheck(this, CreateProspectPutCustomersIdCreateprospectResponse200);

    CreateProspectPutCustomersIdCreateprospectResponse200.initialize(this, prospectId);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProspectPutCustomersIdCreateprospectResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, prospectId) {
      obj['prospectId'] = prospectId;
    }
    /**
     * Constructs a <code>CreateProspectPutCustomersIdCreateprospectResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProspectPutCustomersIdCreateprospectResponse200} obj Optional instance to populate.
     * @return {module:model/CreateProspectPutCustomersIdCreateprospectResponse200} The populated <code>CreateProspectPutCustomersIdCreateprospectResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProspectPutCustomersIdCreateprospectResponse200();

        if (data.hasOwnProperty('prospectId')) {
          obj['prospectId'] = _ApiClient["default"].convertToType(data['prospectId'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return CreateProspectPutCustomersIdCreateprospectResponse200;
}();
/**
 * Newly created prospect ID
 * @member {Number} prospectId
 */


CreateProspectPutCustomersIdCreateprospectResponse200.prototype['prospectId'] = undefined;
var _default = CreateProspectPutCustomersIdCreateprospectResponse200;
exports["default"] = _default;