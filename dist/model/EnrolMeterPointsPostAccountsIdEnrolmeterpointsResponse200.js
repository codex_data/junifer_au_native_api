"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200 model module.
 * @module model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200
 * @version 1.61.1
 */
var EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200</code>.
   * @alias module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200
   * @param id {Number} Meterpoint id
   * @param identifier {String} Meterpoint identifier. For meterpoints of type `MPAN` this will be MPAN core, for `MPRN` - MPRN identifier
   * @param type {String} Meterpoint type
   * @param supplyStartDate {Date} Supply start date or not present if not relevant
   * @param changeOfTenancyFl {Boolean} Change of tenancy flag
   * @param ukGspGroup {String} Meter point GSP group
   * @param operationType {String} Meter point operation type
   * @param meterPointServiceType {String} Meter point service type
   * @param supplyStatus {String} Meter point supply status i.e. 'Registered', 'NotSupplied'
   * @param links {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links} 
   */
  function EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200(id, identifier, type, supplyStartDate, changeOfTenancyFl, ukGspGroup, operationType, meterPointServiceType, supplyStatus, links) {
    _classCallCheck(this, EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200);

    EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.initialize(this, id, identifier, type, supplyStartDate, changeOfTenancyFl, ukGspGroup, operationType, meterPointServiceType, supplyStatus, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, identifier, type, supplyStartDate, changeOfTenancyFl, ukGspGroup, operationType, meterPointServiceType, supplyStatus, links) {
      obj['id'] = id;
      obj['identifier'] = identifier;
      obj['type'] = type;
      obj['supplyStartDate'] = supplyStartDate;
      obj['changeOfTenancyFl'] = changeOfTenancyFl;
      obj['ukGspGroup'] = ukGspGroup;
      obj['operationType'] = operationType;
      obj['meterPointServiceType'] = meterPointServiceType;
      obj['supplyStatus'] = supplyStatus;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200} obj Optional instance to populate.
     * @return {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200} The populated <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('identifier')) {
          obj['identifier'] = _ApiClient["default"].convertToType(data['identifier'], 'String');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('supplyStartDate')) {
          obj['supplyStartDate'] = _ApiClient["default"].convertToType(data['supplyStartDate'], 'Date');
        }

        if (data.hasOwnProperty('changeOfTenancyFl')) {
          obj['changeOfTenancyFl'] = _ApiClient["default"].convertToType(data['changeOfTenancyFl'], 'Boolean');
        }

        if (data.hasOwnProperty('ukProfileClass')) {
          obj['ukProfileClass'] = _ApiClient["default"].convertToType(data['ukProfileClass'], 'Number');
        }

        if (data.hasOwnProperty('ukGspGroup')) {
          obj['ukGspGroup'] = _ApiClient["default"].convertToType(data['ukGspGroup'], 'String');
        }

        if (data.hasOwnProperty('operationType')) {
          obj['operationType'] = _ApiClient["default"].convertToType(data['operationType'], 'String');
        }

        if (data.hasOwnProperty('meterPointServiceType')) {
          obj['meterPointServiceType'] = _ApiClient["default"].convertToType(data['meterPointServiceType'], 'String');
        }

        if (data.hasOwnProperty('readingFrequencyCode')) {
          obj['readingFrequencyCode'] = _ApiClient["default"].convertToType(data['readingFrequencyCode'], 'String');
        }

        if (data.hasOwnProperty('mopMarketParticipant')) {
          obj['mopMarketParticipant'] = _ApiClient["default"].convertToType(data['mopMarketParticipant'], 'String');
        }

        if (data.hasOwnProperty('mamMarketParticipant')) {
          obj['mamMarketParticipant'] = _ApiClient["default"].convertToType(data['mamMarketParticipant'], 'String');
        }

        if (data.hasOwnProperty('DNO')) {
          obj['DNO'] = _ApiClient["default"].convertToType(data['DNO'], 'String');
        }

        if (data.hasOwnProperty('measurementType')) {
          obj['measurementType'] = _ApiClient["default"].convertToType(data['measurementType'], 'String');
        }

        if (data.hasOwnProperty('supplyStatus')) {
          obj['supplyStatus'] = _ApiClient["default"].convertToType(data['supplyStatus'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200;
}();
/**
 * Meterpoint id
 * @member {Number} id
 */


EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['id'] = undefined;
/**
 * Meterpoint identifier. For meterpoints of type `MPAN` this will be MPAN core, for `MPRN` - MPRN identifier
 * @member {String} identifier
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['identifier'] = undefined;
/**
 * Meterpoint type
 * @member {String} type
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['type'] = undefined;
/**
 * Supply start date or not present if not relevant
 * @member {Date} supplyStartDate
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['supplyStartDate'] = undefined;
/**
 * Change of tenancy flag
 * @member {Boolean} changeOfTenancyFl
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['changeOfTenancyFl'] = undefined;
/**
 * UK profile class of the `MPAN`. This will be one of the values in the \"UK Profile Class\" ref table
 * @member {Number} ukProfileClass
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['ukProfileClass'] = undefined;
/**
 * Meter point GSP group
 * @member {String} ukGspGroup
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['ukGspGroup'] = undefined;
/**
 * Meter point operation type
 * @member {String} operationType
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['operationType'] = undefined;
/**
 * Meter point service type
 * @member {String} meterPointServiceType
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['meterPointServiceType'] = undefined;
/**
 * The reading frequency code, only displayed if the meterpoint is an `MPRN`
 * @member {String} readingFrequencyCode
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['readingFrequencyCode'] = undefined;
/**
 * The MOP Market Participant, only displayed if the meterpoint is an `MPAN`.
 * @member {String} mopMarketParticipant
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['mopMarketParticipant'] = undefined;
/**
 * The MAM Market Participant, only displayed if the meterpoint is an `MPRN`.
 * @member {String} mamMarketParticipant
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['mamMarketParticipant'] = undefined;
/**
 * Meter point distribution network operator, only displayed if the meterpoint is an `MPAN`.
 * @member {String} DNO
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['DNO'] = undefined;
/**
 * If the MeterPoint is of type `MPAN` then the measurement type will be shown. This will be Import or Export
 * @member {String} measurementType
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['measurementType'] = undefined;
/**
 * Meter point supply status i.e. 'Registered', 'NotSupplied'
 * @member {String} supplyStatus
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['supplyStatus'] = undefined;
/**
 * @member {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links} links
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.prototype['links'] = undefined;
var _default = EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200;
exports["default"] = _default;