"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillRequestGetBillrequestsIdResponse404 model module.
 * @module model/GetBillRequestGetBillrequestsIdResponse404
 * @version 1.61.1
 */
var GetBillRequestGetBillrequestsIdResponse404 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillRequestGetBillrequestsIdResponse404</code>.
   * @alias module:model/GetBillRequestGetBillrequestsIdResponse404
   * @param errorCode {module:model/GetBillRequestGetBillrequestsIdResponse404.ErrorCodeEnum} Field: * `NotFound` - Could not find 'BillRequest' with Id 'billRequestId'
   * @param errorSeverity {String} The error severity
   */
  function GetBillRequestGetBillrequestsIdResponse404(errorCode, errorSeverity) {
    _classCallCheck(this, GetBillRequestGetBillrequestsIdResponse404);

    GetBillRequestGetBillrequestsIdResponse404.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillRequestGetBillrequestsIdResponse404, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>GetBillRequestGetBillrequestsIdResponse404</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillRequestGetBillrequestsIdResponse404} obj Optional instance to populate.
     * @return {module:model/GetBillRequestGetBillrequestsIdResponse404} The populated <code>GetBillRequestGetBillrequestsIdResponse404</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillRequestGetBillrequestsIdResponse404();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetBillRequestGetBillrequestsIdResponse404;
}();
/**
 * Field: * `NotFound` - Could not find 'BillRequest' with Id 'billRequestId'
 * @member {module:model/GetBillRequestGetBillrequestsIdResponse404.ErrorCodeEnum} errorCode
 */


GetBillRequestGetBillrequestsIdResponse404.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

GetBillRequestGetBillrequestsIdResponse404.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

GetBillRequestGetBillrequestsIdResponse404.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

GetBillRequestGetBillrequestsIdResponse404['ErrorCodeEnum'] = {
  /**
   * value: "NotFound"
   * @const
   */
  "NotFound": "NotFound"
};
var _default = GetBillRequestGetBillrequestsIdResponse404;
exports["default"] = _default;