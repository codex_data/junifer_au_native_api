"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200 model module.
 * @module model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200
 * @version 1.61.1
 */
var GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200</code>.
   * @alias module:model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200
   */
  function GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200() {
    _classCallCheck(this, GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200);

    GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200} obj Optional instance to populate.
     * @return {module:model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200} The populated <code>GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200();

        if (data.hasOwnProperty('nmi')) {
          obj['nmi'] = _ApiClient["default"].convertToType(data['nmi'], 'String');
        }

        if (data.hasOwnProperty('jurisdictionCode')) {
          obj['jurisdictionCode'] = _ApiClient["default"].convertToType(data['jurisdictionCode'], 'String');
        }

        if (data.hasOwnProperty('customerClassificationCode')) {
          obj['customerClassificationCode'] = _ApiClient["default"].convertToType(data['customerClassificationCode'], 'String');
        }

        if (data.hasOwnProperty('customerThresholdCode')) {
          obj['customerThresholdCode'] = _ApiClient["default"].convertToType(data['customerThresholdCode'], 'String');
        }

        if (data.hasOwnProperty('suburbOrPlaceOrLocality')) {
          obj['suburbOrPlaceOrLocality'] = _ApiClient["default"].convertToType(data['suburbOrPlaceOrLocality'], 'String');
        }

        if (data.hasOwnProperty('stateOrTerritory')) {
          obj['stateOrTerritory'] = _ApiClient["default"].convertToType(data['stateOrTerritory'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('deliveryPointIdentifier')) {
          obj['deliveryPointIdentifier'] = _ApiClient["default"].convertToType(data['deliveryPointIdentifier'], 'Number');
        }

        if (data.hasOwnProperty('streetName')) {
          obj['streetName'] = _ApiClient["default"].convertToType(data['streetName'], 'String');
        }

        if (data.hasOwnProperty('streetSuffix')) {
          obj['streetSuffix'] = _ApiClient["default"].convertToType(data['streetSuffix'], 'String');
        }

        if (data.hasOwnProperty('streetType')) {
          obj['streetType'] = _ApiClient["default"].convertToType(data['streetType'], 'String');
        }

        if (data.hasOwnProperty('buildingOrPropertyName')) {
          obj['buildingOrPropertyName'] = _ApiClient["default"].convertToType(data['buildingOrPropertyName'], 'String');
        }

        if (data.hasOwnProperty('buildingOrPropertyName2')) {
          obj['buildingOrPropertyName2'] = _ApiClient["default"].convertToType(data['buildingOrPropertyName2'], 'String');
        }

        if (data.hasOwnProperty('flatOrUnitNumber')) {
          obj['flatOrUnitNumber'] = _ApiClient["default"].convertToType(data['flatOrUnitNumber'], 'String');
        }

        if (data.hasOwnProperty('flatOrUnitType')) {
          obj['flatOrUnitType'] = _ApiClient["default"].convertToType(data['flatOrUnitType'], 'String');
        }

        if (data.hasOwnProperty('floorOrLevelNumber')) {
          obj['floorOrLevelNumber'] = _ApiClient["default"].convertToType(data['floorOrLevelNumber'], 'Number');
        }

        if (data.hasOwnProperty('floorOrLevelType')) {
          obj['floorOrLevelType'] = _ApiClient["default"].convertToType(data['floorOrLevelType'], 'String');
        }

        if (data.hasOwnProperty('houseNumber')) {
          obj['houseNumber'] = _ApiClient["default"].convertToType(data['houseNumber'], 'Number');
        }

        if (data.hasOwnProperty('houseNumberSuffix')) {
          obj['houseNumberSuffix'] = _ApiClient["default"].convertToType(data['houseNumberSuffix'], 'String');
        }

        if (data.hasOwnProperty('houseNumber2')) {
          obj['houseNumber2'] = _ApiClient["default"].convertToType(data['houseNumber2'], 'Number');
        }

        if (data.hasOwnProperty('houseNumber2Suffix')) {
          obj['houseNumber2Suffix'] = _ApiClient["default"].convertToType(data['houseNumber2Suffix'], 'String');
        }

        if (data.hasOwnProperty('locationDescriptor')) {
          obj['locationDescriptor'] = _ApiClient["default"].convertToType(data['locationDescriptor'], 'String');
        }

        if (data.hasOwnProperty('lotNumber')) {
          obj['lotNumber'] = _ApiClient["default"].convertToType(data['lotNumber'], 'String');
        }

        if (data.hasOwnProperty('postalDeliveryNumberPrefix')) {
          obj['postalDeliveryNumberPrefix'] = _ApiClient["default"].convertToType(data['postalDeliveryNumberPrefix'], 'String');
        }

        if (data.hasOwnProperty('postalDeliveryNumberSuffix')) {
          obj['postalDeliveryNumberSuffix'] = _ApiClient["default"].convertToType(data['postalDeliveryNumberSuffix'], 'String');
        }

        if (data.hasOwnProperty('postalDeliveryNumberValue')) {
          obj['postalDeliveryNumberValue'] = _ApiClient["default"].convertToType(data['postalDeliveryNumberValue'], 'Number');
        }

        if (data.hasOwnProperty('postalDeliveryType')) {
          obj['postalDeliveryType'] = _ApiClient["default"].convertToType(data['postalDeliveryType'], 'String');
        }

        if (data.hasOwnProperty('addressLine1')) {
          obj['addressLine1'] = _ApiClient["default"].convertToType(data['addressLine1'], 'String');
        }

        if (data.hasOwnProperty('addressLine2')) {
          obj['addressLine2'] = _ApiClient["default"].convertToType(data['addressLine2'], 'String');
        }

        if (data.hasOwnProperty('addressLine3')) {
          obj['addressLine3'] = _ApiClient["default"].convertToType(data['addressLine3'], 'String');
        }

        if (data.hasOwnProperty('role')) {
          obj['role'] = _ApiClient["default"].convertToType(data['role'], 'String');
        }

        if (data.hasOwnProperty('party')) {
          obj['party'] = _ApiClient["default"].convertToType(data['party'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200;
}();
/**
 * Identifier of a NMI identified from the supplied details
 * @member {String} nmi
 */


GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['nmi'] = undefined;
/**
 * Jurisdiction code
 * @member {String} jurisdictionCode
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['jurisdictionCode'] = undefined;
/**
 * Customer classification code
 * @member {String} customerClassificationCode
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['customerClassificationCode'] = undefined;
/**
 * Customer threshold code
 * @member {String} customerThresholdCode
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['customerThresholdCode'] = undefined;
/**
 * Suburb for the address the meter is located at
 * @member {String} suburbOrPlaceOrLocality
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['suburbOrPlaceOrLocality'] = undefined;
/**
 * State for the address the meter is located at
 * @member {String} stateOrTerritory
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['stateOrTerritory'] = undefined;
/**
 * Postcode of the address the meter is located at
 * @member {String} postcode
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['postcode'] = undefined;
/**
 * 
 * @member {Number} deliveryPointIdentifier
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['deliveryPointIdentifier'] = undefined;
/**
 * Street name for the address the meter is located at
 * @member {String} streetName
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['streetName'] = undefined;
/**
 * Street suffix for the address the meter is located at
 * @member {String} streetSuffix
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['streetSuffix'] = undefined;
/**
 * Street type for the address the meter is located at
 * @member {String} streetType
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['streetType'] = undefined;
/**
 * Property name for the address the meter is located at
 * @member {String} buildingOrPropertyName
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['buildingOrPropertyName'] = undefined;
/**
 * Property name for the address the meter is located at
 * @member {String} buildingOrPropertyName2
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['buildingOrPropertyName2'] = undefined;
/**
 * Unit number for the address the meter is located at
 * @member {String} flatOrUnitNumber
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['flatOrUnitNumber'] = undefined;
/**
 * Unit type for the address the meter is located at
 * @member {String} flatOrUnitType
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['flatOrUnitType'] = undefined;
/**
 * Floor number for the address the meter is located at
 * @member {Number} floorOrLevelNumber
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['floorOrLevelNumber'] = undefined;
/**
 * Floor type for the address the meter is located at
 * @member {String} floorOrLevelType
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['floorOrLevelType'] = undefined;
/**
 * House number for the address the meter is located at
 * @member {Number} houseNumber
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['houseNumber'] = undefined;
/**
 * House suffix for the address the meter is located at
 * @member {String} houseNumberSuffix
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['houseNumberSuffix'] = undefined;
/**
 * House number for the address the meter is located at
 * @member {Number} houseNumber2
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['houseNumber2'] = undefined;
/**
 * House suffix for the address the meter is located at
 * @member {String} houseNumber2Suffix
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['houseNumber2Suffix'] = undefined;
/**
 * Location descriptor for the address the meter is located at
 * @member {String} locationDescriptor
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['locationDescriptor'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {String} lotNumber
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['lotNumber'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {String} postalDeliveryNumberPrefix
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['postalDeliveryNumberPrefix'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {String} postalDeliveryNumberSuffix
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['postalDeliveryNumberSuffix'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {Number} postalDeliveryNumberValue
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['postalDeliveryNumberValue'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {String} postalDeliveryType
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['postalDeliveryType'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {String} addressLine1
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['addressLine1'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {String} addressLine2
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['addressLine2'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {String} addressLine3
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['addressLine3'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {String} role
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['role'] = undefined;
/**
 * Lot number for the address the meter is located at
 * @member {String} party
 */

GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.prototype['party'] = undefined;
var _default = GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200;
exports["default"] = _default;