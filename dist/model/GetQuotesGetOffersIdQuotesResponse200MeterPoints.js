"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks = _interopRequireDefault(require("./GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks"));

var _GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices = _interopRequireDefault(require("./GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetQuotesGetOffersIdQuotesResponse200MeterPoints model module.
 * @module model/GetQuotesGetOffersIdQuotesResponse200MeterPoints
 * @version 1.61.1
 */
var GetQuotesGetOffersIdQuotesResponse200MeterPoints = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetQuotesGetOffersIdQuotesResponse200MeterPoints</code>.
   * Array of meter points on this quote
   * @alias module:model/GetQuotesGetOffersIdQuotesResponse200MeterPoints
   * @param id {Number} Meter point ID
   * @param identifier {String} Meter point identifier
   * @param type {String} Type of meter point
   * @param links {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks} 
   */
  function GetQuotesGetOffersIdQuotesResponse200MeterPoints(id, identifier, type, links) {
    _classCallCheck(this, GetQuotesGetOffersIdQuotesResponse200MeterPoints);

    GetQuotesGetOffersIdQuotesResponse200MeterPoints.initialize(this, id, identifier, type, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetQuotesGetOffersIdQuotesResponse200MeterPoints, null, [{
    key: "initialize",
    value: function initialize(obj, id, identifier, type, links) {
      obj['id'] = id;
      obj['identifier'] = identifier;
      obj['type'] = type;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetQuotesGetOffersIdQuotesResponse200MeterPoints</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPoints} obj Optional instance to populate.
     * @return {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPoints} The populated <code>GetQuotesGetOffersIdQuotesResponse200MeterPoints</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetQuotesGetOffersIdQuotesResponse200MeterPoints();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('identifier')) {
          obj['identifier'] = _ApiClient["default"].convertToType(data['identifier'], 'String');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('prices')) {
          obj['prices'] = _GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices["default"].constructFromObject(data['prices']);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetQuotesGetOffersIdQuotesResponse200MeterPoints;
}();
/**
 * Meter point ID
 * @member {Number} id
 */


GetQuotesGetOffersIdQuotesResponse200MeterPoints.prototype['id'] = undefined;
/**
 * Meter point identifier
 * @member {String} identifier
 */

GetQuotesGetOffersIdQuotesResponse200MeterPoints.prototype['identifier'] = undefined;
/**
 * Type of meter point
 * @member {String} type
 */

GetQuotesGetOffersIdQuotesResponse200MeterPoints.prototype['type'] = undefined;
/**
 * @member {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices} prices
 */

GetQuotesGetOffersIdQuotesResponse200MeterPoints.prototype['prices'] = undefined;
/**
 * @member {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks} links
 */

GetQuotesGetOffersIdQuotesResponse200MeterPoints.prototype['links'] = undefined;
var _default = GetQuotesGetOffersIdQuotesResponse200MeterPoints;
exports["default"] = _default;