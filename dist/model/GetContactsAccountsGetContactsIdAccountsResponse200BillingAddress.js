"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress model module.
 * @module model/GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress
 * @version 1.61.1
 */
var GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress</code>.
   * Account&#39;s billing address
   * @alias module:model/GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress
   */
  function GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress() {
    _classCallCheck(this, GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress);

    GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress} obj Optional instance to populate.
     * @return {module:model/GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress} The populated <code>GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress();

        if (data.hasOwnProperty('addressType')) {
          obj['addressType'] = _ApiClient["default"].convertToType(data['addressType'], 'String');
        }

        if (data.hasOwnProperty('careOf')) {
          obj['careOf'] = _ApiClient["default"].convertToType(data['careOf'], 'String');
        }

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('country')) {
          obj['country'] = _ApiClient["default"].convertToType(data['country'], 'String');
        }

        if (data.hasOwnProperty('countryCode')) {
          obj['countryCode'] = _ApiClient["default"].convertToType(data['countryCode'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress;
}();
/**
 * Internally used address type which defines number of lines and format used etc.
 * @member {String} addressType
 */


GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['addressType'] = undefined;
/**
 * The 'Care of' line of the address
 * @member {String} careOf
 */

GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['careOf'] = undefined;
/**
 * Address 1
 * @member {String} address1
 */

GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['address1'] = undefined;
/**
 * Address 2
 * @member {String} address2
 */

GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['address2'] = undefined;
/**
 * Address 3
 * @member {String} address3
 */

GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['address3'] = undefined;
/**
 * Address 4
 * @member {String} address4
 */

GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['address4'] = undefined;
/**
 * Address 5
 * @member {String} address5
 */

GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['address5'] = undefined;
/**
 * Post code
 * @member {String} postcode
 */

GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['postcode'] = undefined;
/**
 * Country name
 * @member {String} country
 */

GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['country'] = undefined;
/**
 * Country code
 * @member {String} countryCode
 */

GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.prototype['countryCode'] = undefined;
var _default = GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress;
exports["default"] = _default;