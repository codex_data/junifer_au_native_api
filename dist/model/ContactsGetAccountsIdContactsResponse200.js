"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _ContactsGetAccountsIdContactsResponse200Address = _interopRequireDefault(require("./ContactsGetAccountsIdContactsResponse200Address"));

var _ContactsGetAccountsIdContactsResponse200Links = _interopRequireDefault(require("./ContactsGetAccountsIdContactsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The ContactsGetAccountsIdContactsResponse200 model module.
 * @module model/ContactsGetAccountsIdContactsResponse200
 * @version 1.61.1
 */
var ContactsGetAccountsIdContactsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ContactsGetAccountsIdContactsResponse200</code>.
   * @alias module:model/ContactsGetAccountsIdContactsResponse200
   * @param id {Number} Contact Id
   * @param contactType {String} Contact Type
   * @param primary {Boolean} Whether the contact is the primary
   * @param surname {String} Account's contact surname
   * @param email {String} Account's contact email
   * @param accountContactId {Number} Account contact Id
   * @param receivePost {Boolean} Contacts communication postal delivery preference.
   * @param receiveEmail {Boolean} Contacts communication email delivery preference.
   * @param receiveSMS {Boolean} Contacts communication SMS delivery preference.
   * @param cancelled {Boolean} True if this contact is no longer associated with the customer
   * @param fromDttm {Date} Contacts from date time
   * @param links {module:model/ContactsGetAccountsIdContactsResponse200Links} 
   */
  function ContactsGetAccountsIdContactsResponse200(id, contactType, primary, surname, email, accountContactId, receivePost, receiveEmail, receiveSMS, cancelled, fromDttm, links) {
    _classCallCheck(this, ContactsGetAccountsIdContactsResponse200);

    ContactsGetAccountsIdContactsResponse200.initialize(this, id, contactType, primary, surname, email, accountContactId, receivePost, receiveEmail, receiveSMS, cancelled, fromDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ContactsGetAccountsIdContactsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, contactType, primary, surname, email, accountContactId, receivePost, receiveEmail, receiveSMS, cancelled, fromDttm, links) {
      obj['id'] = id;
      obj['contactType'] = contactType;
      obj['primary'] = primary;
      obj['surname'] = surname;
      obj['email'] = email;
      obj['accountContactId'] = accountContactId;
      obj['receivePost'] = receivePost;
      obj['receiveEmail'] = receiveEmail;
      obj['receiveSMS'] = receiveSMS;
      obj['cancelled'] = cancelled;
      obj['fromDttm'] = fromDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>ContactsGetAccountsIdContactsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ContactsGetAccountsIdContactsResponse200} obj Optional instance to populate.
     * @return {module:model/ContactsGetAccountsIdContactsResponse200} The populated <code>ContactsGetAccountsIdContactsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ContactsGetAccountsIdContactsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('contactType')) {
          obj['contactType'] = _ApiClient["default"].convertToType(data['contactType'], 'String');
        }

        if (data.hasOwnProperty('primary')) {
          obj['primary'] = _ApiClient["default"].convertToType(data['primary'], 'Boolean');
        }

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('initials')) {
          obj['initials'] = _ApiClient["default"].convertToType(data['initials'], 'String');
        }

        if (data.hasOwnProperty('jobTitle')) {
          obj['jobTitle'] = _ApiClient["default"].convertToType(data['jobTitle'], 'String');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _ContactsGetAccountsIdContactsResponse200Address["default"].constructFromObject(data['address']);
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('phoneNumber1')) {
          obj['phoneNumber1'] = _ApiClient["default"].convertToType(data['phoneNumber1'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber2')) {
          obj['phoneNumber2'] = _ApiClient["default"].convertToType(data['phoneNumber2'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber3')) {
          obj['phoneNumber3'] = _ApiClient["default"].convertToType(data['phoneNumber3'], 'String');
        }

        if (data.hasOwnProperty('accountContactId')) {
          obj['accountContactId'] = _ApiClient["default"].convertToType(data['accountContactId'], 'Number');
        }

        if (data.hasOwnProperty('billDelivery')) {
          obj['billDelivery'] = _ApiClient["default"].convertToType(data['billDelivery'], 'String');
        }

        if (data.hasOwnProperty('receivePost')) {
          obj['receivePost'] = _ApiClient["default"].convertToType(data['receivePost'], 'Boolean');
        }

        if (data.hasOwnProperty('receiveEmail')) {
          obj['receiveEmail'] = _ApiClient["default"].convertToType(data['receiveEmail'], 'Boolean');
        }

        if (data.hasOwnProperty('receiveSMS')) {
          obj['receiveSMS'] = _ApiClient["default"].convertToType(data['receiveSMS'], 'Boolean');
        }

        if (data.hasOwnProperty('cancelled')) {
          obj['cancelled'] = _ApiClient["default"].convertToType(data['cancelled'], 'Boolean');
        }

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _ContactsGetAccountsIdContactsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return ContactsGetAccountsIdContactsResponse200;
}();
/**
 * Contact Id
 * @member {Number} id
 */


ContactsGetAccountsIdContactsResponse200.prototype['id'] = undefined;
/**
 * Contact Type
 * @member {String} contactType
 */

ContactsGetAccountsIdContactsResponse200.prototype['contactType'] = undefined;
/**
 * Whether the contact is the primary
 * @member {Boolean} primary
 */

ContactsGetAccountsIdContactsResponse200.prototype['primary'] = undefined;
/**
 * Account's contact title
 * @member {String} title
 */

ContactsGetAccountsIdContactsResponse200.prototype['title'] = undefined;
/**
 * Account's contact forename
 * @member {String} forename
 */

ContactsGetAccountsIdContactsResponse200.prototype['forename'] = undefined;
/**
 * Account's contact surname
 * @member {String} surname
 */

ContactsGetAccountsIdContactsResponse200.prototype['surname'] = undefined;
/**
 * Account's contact initials
 * @member {String} initials
 */

ContactsGetAccountsIdContactsResponse200.prototype['initials'] = undefined;
/**
 * Account's contact jobTitle
 * @member {String} jobTitle
 */

ContactsGetAccountsIdContactsResponse200.prototype['jobTitle'] = undefined;
/**
 * @member {module:model/ContactsGetAccountsIdContactsResponse200Address} address
 */

ContactsGetAccountsIdContactsResponse200.prototype['address'] = undefined;
/**
 * Account's contact email
 * @member {String} email
 */

ContactsGetAccountsIdContactsResponse200.prototype['email'] = undefined;
/**
 * Account's date of birth
 * @member {Date} dateOfBirth
 */

ContactsGetAccountsIdContactsResponse200.prototype['dateOfBirth'] = undefined;
/**
 * Account's primary contact phone number
 * @member {String} phoneNumber1
 */

ContactsGetAccountsIdContactsResponse200.prototype['phoneNumber1'] = undefined;
/**
 * Account's secondary contact phone number
 * @member {String} phoneNumber2
 */

ContactsGetAccountsIdContactsResponse200.prototype['phoneNumber2'] = undefined;
/**
 * Account's third contact phone number
 * @member {String} phoneNumber3
 */

ContactsGetAccountsIdContactsResponse200.prototype['phoneNumber3'] = undefined;
/**
 * Account contact Id
 * @member {Number} accountContactId
 */

ContactsGetAccountsIdContactsResponse200.prototype['accountContactId'] = undefined;
/**
 * Contacts Bill Delivery Preference. Can be `Email`, `Mail`, `Both`
 * @member {String} billDelivery
 */

ContactsGetAccountsIdContactsResponse200.prototype['billDelivery'] = undefined;
/**
 * Contacts communication postal delivery preference.
 * @member {Boolean} receivePost
 */

ContactsGetAccountsIdContactsResponse200.prototype['receivePost'] = undefined;
/**
 * Contacts communication email delivery preference.
 * @member {Boolean} receiveEmail
 */

ContactsGetAccountsIdContactsResponse200.prototype['receiveEmail'] = undefined;
/**
 * Contacts communication SMS delivery preference.
 * @member {Boolean} receiveSMS
 */

ContactsGetAccountsIdContactsResponse200.prototype['receiveSMS'] = undefined;
/**
 * True if this contact is no longer associated with the customer
 * @member {Boolean} cancelled
 */

ContactsGetAccountsIdContactsResponse200.prototype['cancelled'] = undefined;
/**
 * Contacts from date time
 * @member {Date} fromDttm
 */

ContactsGetAccountsIdContactsResponse200.prototype['fromDttm'] = undefined;
/**
 * Contacts to date time
 * @member {Date} toDttm
 */

ContactsGetAccountsIdContactsResponse200.prototype['toDttm'] = undefined;
/**
 * @member {module:model/ContactsGetAccountsIdContactsResponse200Links} links
 */

ContactsGetAccountsIdContactsResponse200.prototype['links'] = undefined;
var _default = ContactsGetAccountsIdContactsResponse200;
exports["default"] = _default;