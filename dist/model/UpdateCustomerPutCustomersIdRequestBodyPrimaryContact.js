"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateCustomerPutCustomersIdRequestBodyPrimaryContact model module.
 * @module model/UpdateCustomerPutCustomersIdRequestBodyPrimaryContact
 * @version 1.61.1
 */
var UpdateCustomerPutCustomersIdRequestBodyPrimaryContact = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateCustomerPutCustomersIdRequestBodyPrimaryContact</code>.
   * Customer&#39;s primary contact (This field is ignored)
   * @alias module:model/UpdateCustomerPutCustomersIdRequestBodyPrimaryContact
   * @param surname {String} Customer's primary contact surname (This field is ignored)
   * @param email {String} Customer's primary contact email (This field is ignored)
   */
  function UpdateCustomerPutCustomersIdRequestBodyPrimaryContact(surname, email) {
    _classCallCheck(this, UpdateCustomerPutCustomersIdRequestBodyPrimaryContact);

    UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.initialize(this, surname, email);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateCustomerPutCustomersIdRequestBodyPrimaryContact, null, [{
    key: "initialize",
    value: function initialize(obj, surname, email) {
      obj['surname'] = surname;
      obj['email'] = email;
    }
    /**
     * Constructs a <code>UpdateCustomerPutCustomersIdRequestBodyPrimaryContact</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateCustomerPutCustomersIdRequestBodyPrimaryContact} obj Optional instance to populate.
     * @return {module:model/UpdateCustomerPutCustomersIdRequestBodyPrimaryContact} The populated <code>UpdateCustomerPutCustomersIdRequestBodyPrimaryContact</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateCustomerPutCustomersIdRequestBodyPrimaryContact();

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber1')) {
          obj['phoneNumber1'] = _ApiClient["default"].convertToType(data['phoneNumber1'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber2')) {
          obj['phoneNumber2'] = _ApiClient["default"].convertToType(data['phoneNumber2'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber3')) {
          obj['phoneNumber3'] = _ApiClient["default"].convertToType(data['phoneNumber3'], 'String');
        }
      }

      return obj;
    }
  }]);

  return UpdateCustomerPutCustomersIdRequestBodyPrimaryContact;
}();
/**
 * Customer's primary contact title (This field is ignored)
 * @member {String} title
 */


UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.prototype['title'] = undefined;
/**
 * Customer's primary contact forename (This field is ignored)
 * @member {String} forename
 */

UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.prototype['forename'] = undefined;
/**
 * Customer's primary contact surname (This field is ignored)
 * @member {String} surname
 */

UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.prototype['surname'] = undefined;
/**
 * Customer's primary contact email (This field is ignored)
 * @member {String} email
 */

UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.prototype['email'] = undefined;
/**
 * Customer's date of birth (This field is ignored)
 * @member {String} dateOfBirth
 */

UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.prototype['dateOfBirth'] = undefined;
/**
 * Customer's primary contact phone number (This field is ignored)
 * @member {String} phoneNumber1
 */

UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.prototype['phoneNumber1'] = undefined;
/**
 * Customer's secondary contact phone number (This field is ignored)
 * @member {String} phoneNumber2
 */

UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.prototype['phoneNumber2'] = undefined;
/**
 * Customer's third contact phone number (This field is ignored)
 * @member {String} phoneNumber3
 */

UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.prototype['phoneNumber3'] = undefined;
var _default = UpdateCustomerPutCustomersIdRequestBodyPrimaryContact;
exports["default"] = _default;