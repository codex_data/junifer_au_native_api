"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPropertyGetPropertysIdResponse200Address model module.
 * @module model/GetPropertyGetPropertysIdResponse200Address
 * @version 1.61.1
 */
var GetPropertyGetPropertysIdResponse200Address = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPropertyGetPropertysIdResponse200Address</code>.
   * Internally used address type which defines number of lines and format used etc.
   * @alias module:model/GetPropertyGetPropertysIdResponse200Address
   * @param address1 {String} Property address line 1
   * @param address2 {String} Property address line 2
   * @param address3 {String} Property address line 3
   * @param address4 {String} Property address line 4
   * @param address5 {String} Property address line 5
   * @param postcode {String} Property address post code
   * @param country {String} Property address country name
   */
  function GetPropertyGetPropertysIdResponse200Address(address1, address2, address3, address4, address5, postcode, country) {
    _classCallCheck(this, GetPropertyGetPropertysIdResponse200Address);

    GetPropertyGetPropertysIdResponse200Address.initialize(this, address1, address2, address3, address4, address5, postcode, country);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPropertyGetPropertysIdResponse200Address, null, [{
    key: "initialize",
    value: function initialize(obj, address1, address2, address3, address4, address5, postcode, country) {
      obj['address1'] = address1;
      obj['address2'] = address2;
      obj['address3'] = address3;
      obj['address4'] = address4;
      obj['address5'] = address5;
      obj['postcode'] = postcode;
      obj['country'] = country;
    }
    /**
     * Constructs a <code>GetPropertyGetPropertysIdResponse200Address</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPropertyGetPropertysIdResponse200Address} obj Optional instance to populate.
     * @return {module:model/GetPropertyGetPropertysIdResponse200Address} The populated <code>GetPropertyGetPropertysIdResponse200Address</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPropertyGetPropertysIdResponse200Address();

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('countryCode')) {
          obj['countryCode'] = _ApiClient["default"].convertToType(data['countryCode'], 'String');
        }

        if (data.hasOwnProperty('country')) {
          obj['country'] = _ApiClient["default"].convertToType(data['country'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetPropertyGetPropertysIdResponse200Address;
}();
/**
 * Property address line 1
 * @member {String} address1
 */


GetPropertyGetPropertysIdResponse200Address.prototype['address1'] = undefined;
/**
 * Property address line 2
 * @member {String} address2
 */

GetPropertyGetPropertysIdResponse200Address.prototype['address2'] = undefined;
/**
 * Property address line 3
 * @member {String} address3
 */

GetPropertyGetPropertysIdResponse200Address.prototype['address3'] = undefined;
/**
 * Property address line 4
 * @member {String} address4
 */

GetPropertyGetPropertysIdResponse200Address.prototype['address4'] = undefined;
/**
 * Property address line 5
 * @member {String} address5
 */

GetPropertyGetPropertysIdResponse200Address.prototype['address5'] = undefined;
/**
 * Property address post code
 * @member {String} postcode
 */

GetPropertyGetPropertysIdResponse200Address.prototype['postcode'] = undefined;
/**
 * Property address country code
 * @member {String} countryCode
 */

GetPropertyGetPropertysIdResponse200Address.prototype['countryCode'] = undefined;
/**
 * Property address country name
 * @member {String} country
 */

GetPropertyGetPropertysIdResponse200Address.prototype['country'] = undefined;
var _default = GetPropertyGetPropertysIdResponse200Address;
exports["default"] = _default;