"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress = _interopRequireDefault(require("./GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress"));

var _GetCustomerAccountsGetCustomersIdAccountsResponse200Links = _interopRequireDefault(require("./GetCustomerAccountsGetCustomersIdAccountsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCustomerAccountsGetCustomersIdAccountsResponse200 model module.
 * @module model/GetCustomerAccountsGetCustomersIdAccountsResponse200
 * @version 1.61.1
 */
var GetCustomerAccountsGetCustomersIdAccountsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCustomerAccountsGetCustomersIdAccountsResponse200</code>.
   * @alias module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200
   * @param id {Number} Account ID
   * @param type {String} Account type code as defined in the Account Type ref table in Junifer. This can vary per install so please consult the ref table for possible values
   * @param name {String} Account name
   * @param number {String} Account number
   * @param currency {String} Currency ISO code
   * @param fromDt {Date} Account start date
   * @param createdDttm {Date} Account creation date and time
   * @param links {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200Links} 
   */
  function GetCustomerAccountsGetCustomersIdAccountsResponse200(id, type, name, number, currency, fromDt, createdDttm, links) {
    _classCallCheck(this, GetCustomerAccountsGetCustomersIdAccountsResponse200);

    GetCustomerAccountsGetCustomersIdAccountsResponse200.initialize(this, id, type, name, number, currency, fromDt, createdDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCustomerAccountsGetCustomersIdAccountsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, type, name, number, currency, fromDt, createdDttm, links) {
      obj['id'] = id;
      obj['type'] = type;
      obj['name'] = name;
      obj['number'] = number;
      obj['currency'] = currency;
      obj['fromDt'] = fromDt;
      obj['createdDttm'] = createdDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetCustomerAccountsGetCustomersIdAccountsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200} obj Optional instance to populate.
     * @return {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200} The populated <code>GetCustomerAccountsGetCustomersIdAccountsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCustomerAccountsGetCustomersIdAccountsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('closedDttm')) {
          obj['closedDttm'] = _ApiClient["default"].convertToType(data['closedDttm'], 'Date');
        }

        if (data.hasOwnProperty('cancelled')) {
          obj['cancelled'] = _ApiClient["default"].convertToType(data['cancelled'], 'Boolean');
        }

        if (data.hasOwnProperty('cancelledDttm')) {
          obj['cancelledDttm'] = _ApiClient["default"].convertToType(data['cancelledDttm'], 'Date');
        }

        if (data.hasOwnProperty('balance')) {
          obj['balance'] = _ApiClient["default"].convertToType(data['balance'], 'Number');
        }

        if (data.hasOwnProperty('billingAddress')) {
          obj['billingAddress'] = _GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress["default"].constructFromObject(data['billingAddress']);
        }

        if (data.hasOwnProperty('billDelivery')) {
          obj['billDelivery'] = _ApiClient["default"].convertToType(data['billDelivery'], 'String');
        }

        if (data.hasOwnProperty('paymentMethodType')) {
          obj['paymentMethodType'] = _ApiClient["default"].convertToType(data['paymentMethodType'], 'String');
        }

        if (data.hasOwnProperty('accountClass')) {
          obj['accountClass'] = _ApiClient["default"].convertToType(data['accountClass'], 'String');
        }

        if (data.hasOwnProperty('billingCycle')) {
          obj['billingCycle'] = _ApiClient["default"].convertToType(data['billingCycle'], 'String');
        }

        if (data.hasOwnProperty('parentAccountId')) {
          obj['parentAccountId'] = _ApiClient["default"].convertToType(data['parentAccountId'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetCustomerAccountsGetCustomersIdAccountsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetCustomerAccountsGetCustomersIdAccountsResponse200;
}();
/**
 * Account ID
 * @member {Number} id
 */


GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['id'] = undefined;
/**
 * Account type code as defined in the Account Type ref table in Junifer. This can vary per install so please consult the ref table for possible values
 * @member {String} type
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['type'] = undefined;
/**
 * Account name
 * @member {String} name
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['name'] = undefined;
/**
 * Account number
 * @member {String} number
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['number'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['currency'] = undefined;
/**
 * Account start date
 * @member {Date} fromDt
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['fromDt'] = undefined;
/**
 * Account end date
 * @member {Date} toDt
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['toDt'] = undefined;
/**
 * Account creation date and time
 * @member {Date} createdDttm
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['createdDttm'] = undefined;
/**
 * Account close date and time
 * @member {Date} closedDttm
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['closedDttm'] = undefined;
/**
 * A flag indicating whether the account has been cancelled
 * @member {Boolean} cancelled
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['cancelled'] = undefined;
/**
 * Account cancelled date and time
 * @member {Date} cancelledDttm
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['cancelledDttm'] = undefined;
/**
 * Account's balance (only if AR is enabled)
 * @member {Number} balance
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['balance'] = undefined;
/**
 * @member {module:model/GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress} billingAddress
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['billingAddress'] = undefined;
/**
 * Method of bill delivery \"None\", \"Mail\", \"Email\", \"Both\"
 * @member {String} billDelivery
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['billDelivery'] = undefined;
/**
 * Account's payment method type, i.e. Direct Debit
 * @member {String} paymentMethodType
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['paymentMethodType'] = undefined;
/**
 * Account's class type \"Credit\", \"Statement\", \"Cost Statement, \"Prepay\"
 * @member {String} accountClass
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['accountClass'] = undefined;
/**
 * Account's billing cycle i.e. \"Monthly - Anniversary\", \"Annually - Anniversary\",
 * @member {String} billingCycle
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['billingCycle'] = undefined;
/**
 * Parent account id of the provided account.
 * @member {Number} parentAccountId
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['parentAccountId'] = undefined;
/**
 * @member {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200Links} links
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200.prototype['links'] = undefined;
var _default = GetCustomerAccountsGetCustomersIdAccountsResponse200;
exports["default"] = _default;