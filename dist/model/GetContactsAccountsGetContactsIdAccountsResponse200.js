"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress = _interopRequireDefault(require("./GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress"));

var _GetContactsAccountsGetContactsIdAccountsResponse200Links = _interopRequireDefault(require("./GetContactsAccountsGetContactsIdAccountsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetContactsAccountsGetContactsIdAccountsResponse200 model module.
 * @module model/GetContactsAccountsGetContactsIdAccountsResponse200
 * @version 1.61.1
 */
var GetContactsAccountsGetContactsIdAccountsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetContactsAccountsGetContactsIdAccountsResponse200</code>.
   * @alias module:model/GetContactsAccountsGetContactsIdAccountsResponse200
   * @param id {Number} Account ID
   * @param type {String} Account type code as defined in the Account Type ref table in Junifer. This can vary per install so please consult the ref table for possible values
   * @param name {String} Account name
   * @param number {String} Account number
   * @param currency {String} Currency ISO code
   * @param fromDt {Date} Account start date
   * @param createdDttm {Date} Account creation date and time
   * @param links {module:model/GetContactsAccountsGetContactsIdAccountsResponse200Links} 
   */
  function GetContactsAccountsGetContactsIdAccountsResponse200(id, type, name, number, currency, fromDt, createdDttm, links) {
    _classCallCheck(this, GetContactsAccountsGetContactsIdAccountsResponse200);

    GetContactsAccountsGetContactsIdAccountsResponse200.initialize(this, id, type, name, number, currency, fromDt, createdDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetContactsAccountsGetContactsIdAccountsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, type, name, number, currency, fromDt, createdDttm, links) {
      obj['id'] = id;
      obj['type'] = type;
      obj['name'] = name;
      obj['number'] = number;
      obj['currency'] = currency;
      obj['fromDt'] = fromDt;
      obj['createdDttm'] = createdDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetContactsAccountsGetContactsIdAccountsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetContactsAccountsGetContactsIdAccountsResponse200} obj Optional instance to populate.
     * @return {module:model/GetContactsAccountsGetContactsIdAccountsResponse200} The populated <code>GetContactsAccountsGetContactsIdAccountsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetContactsAccountsGetContactsIdAccountsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('closedDttm')) {
          obj['closedDttm'] = _ApiClient["default"].convertToType(data['closedDttm'], 'Date');
        }

        if (data.hasOwnProperty('balance')) {
          obj['balance'] = _ApiClient["default"].convertToType(data['balance'], 'Number');
        }

        if (data.hasOwnProperty('billingAddress')) {
          obj['billingAddress'] = _GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress["default"].constructFromObject(data['billingAddress']);
        }

        if (data.hasOwnProperty('billDelivery')) {
          obj['billDelivery'] = _ApiClient["default"].convertToType(data['billDelivery'], 'String');
        }

        if (data.hasOwnProperty('paymentMethodType')) {
          obj['paymentMethodType'] = _ApiClient["default"].convertToType(data['paymentMethodType'], 'String');
        }

        if (data.hasOwnProperty('accountClass')) {
          obj['accountClass'] = _ApiClient["default"].convertToType(data['accountClass'], 'String');
        }

        if (data.hasOwnProperty('billingCycle')) {
          obj['billingCycle'] = _ApiClient["default"].convertToType(data['billingCycle'], 'String');
        }

        if (data.hasOwnProperty('parentAccountId')) {
          obj['parentAccountId'] = _ApiClient["default"].convertToType(data['parentAccountId'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetContactsAccountsGetContactsIdAccountsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetContactsAccountsGetContactsIdAccountsResponse200;
}();
/**
 * Account ID
 * @member {Number} id
 */


GetContactsAccountsGetContactsIdAccountsResponse200.prototype['id'] = undefined;
/**
 * Account type code as defined in the Account Type ref table in Junifer. This can vary per install so please consult the ref table for possible values
 * @member {String} type
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['type'] = undefined;
/**
 * Account name
 * @member {String} name
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['name'] = undefined;
/**
 * Account number
 * @member {String} number
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['number'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['currency'] = undefined;
/**
 * Account start date
 * @member {Date} fromDt
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['fromDt'] = undefined;
/**
 * Account end date
 * @member {Date} toDt
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['toDt'] = undefined;
/**
 * Account creation date and time
 * @member {Date} createdDttm
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['createdDttm'] = undefined;
/**
 * Account close date and time
 * @member {Date} closedDttm
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['closedDttm'] = undefined;
/**
 * Account's balance (only if AR is enabled)
 * @member {Number} balance
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['balance'] = undefined;
/**
 * @member {module:model/GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress} billingAddress
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['billingAddress'] = undefined;
/**
 * Method of bill delivery \"None\", \"Mail\", \"Email\", \"Both\"
 * @member {String} billDelivery
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['billDelivery'] = undefined;
/**
 * Account's payment method type, i.e. Direct Debit
 * @member {String} paymentMethodType
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['paymentMethodType'] = undefined;
/**
 * Account's class type \"Credit\", \"Statement\", \"Cost Statement, \"Prepay\"
 * @member {String} accountClass
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['accountClass'] = undefined;
/**
 * Account's billing cycle i.e. \"Monthly - Anniversary\", \"Annually - Anniversary\",
 * @member {String} billingCycle
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['billingCycle'] = undefined;
/**
 * Parent account id of the provided account.
 * @member {Number} parentAccountId
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['parentAccountId'] = undefined;
/**
 * @member {module:model/GetContactsAccountsGetContactsIdAccountsResponse200Links} links
 */

GetContactsAccountsGetContactsIdAccountsResponse200.prototype['links'] = undefined;
var _default = GetContactsAccountsGetContactsIdAccountsResponse200;
exports["default"] = _default;