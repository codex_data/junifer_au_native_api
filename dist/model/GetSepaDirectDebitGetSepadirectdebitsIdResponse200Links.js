"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links model module.
 * @module model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links
 * @version 1.61.1
 */
var GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links</code>.
   * Helpful links
   * @alias module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links
   * @param self {String} Link to this SEPA Direct Debit
   * @param paymentMethod {String} Link to this SEPA Direct Debit payment method
   */
  function GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links(self, paymentMethod) {
    _classCallCheck(this, GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links);

    GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links.initialize(this, self, paymentMethod);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, paymentMethod) {
      obj['self'] = self;
      obj['paymentMethod'] = paymentMethod;
    }
    /**
     * Constructs a <code>GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links} The populated <code>GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('paymentMethod')) {
          obj['paymentMethod'] = _ApiClient["default"].convertToType(data['paymentMethod'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links;
}();
/**
 * Link to this SEPA Direct Debit
 * @member {String} self
 */


GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links.prototype['self'] = undefined;
/**
 * Link to this SEPA Direct Debit payment method
 * @member {String} paymentMethod
 */

GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links.prototype['paymentMethod'] = undefined;
var _default = GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links;
exports["default"] = _default;