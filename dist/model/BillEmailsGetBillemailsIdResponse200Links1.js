"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The BillEmailsGetBillemailsIdResponse200Links1 model module.
 * @module model/BillEmailsGetBillemailsIdResponse200Links1
 * @version 1.61.1
 */
var BillEmailsGetBillemailsIdResponse200Links1 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>BillEmailsGetBillemailsIdResponse200Links1</code>.
   * Links to related resources
   * @alias module:model/BillEmailsGetBillemailsIdResponse200Links1
   * @param self {String} Link to this email
   * @param bill {String} Link to the bill this email belongs to
   */
  function BillEmailsGetBillemailsIdResponse200Links1(self, bill) {
    _classCallCheck(this, BillEmailsGetBillemailsIdResponse200Links1);

    BillEmailsGetBillemailsIdResponse200Links1.initialize(this, self, bill);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(BillEmailsGetBillemailsIdResponse200Links1, null, [{
    key: "initialize",
    value: function initialize(obj, self, bill) {
      obj['self'] = self;
      obj['bill'] = bill;
    }
    /**
     * Constructs a <code>BillEmailsGetBillemailsIdResponse200Links1</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/BillEmailsGetBillemailsIdResponse200Links1} obj Optional instance to populate.
     * @return {module:model/BillEmailsGetBillemailsIdResponse200Links1} The populated <code>BillEmailsGetBillemailsIdResponse200Links1</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new BillEmailsGetBillemailsIdResponse200Links1();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('bill')) {
          obj['bill'] = _ApiClient["default"].convertToType(data['bill'], 'String');
        }
      }

      return obj;
    }
  }]);

  return BillEmailsGetBillemailsIdResponse200Links1;
}();
/**
 * Link to this email
 * @member {String} self
 */


BillEmailsGetBillemailsIdResponse200Links1.prototype['self'] = undefined;
/**
 * Link to the bill this email belongs to
 * @member {String} bill
 */

BillEmailsGetBillemailsIdResponse200Links1.prototype['bill'] = undefined;
var _default = BillEmailsGetBillemailsIdResponse200Links1;
exports["default"] = _default;