"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate model module.
 * @module model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate
 * @version 1.61.1
 */
var EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate</code>.
   * Estimated gas spend/consumption values provided by the customer. Can be provided only for gas or dual-fuel enrolments when gas product is supplied
   * @alias module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate
   * @param period {String} The period for the estimated gas values. Must be `Month` or `Year`(Request Parameter)
   */
  function EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate(period) {
    _classCallCheck(this, EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate);

    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.initialize(this, period);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate, null, [{
    key: "initialize",
    value: function initialize(obj, period) {
      obj['period'] = period;
    }
    /**
     * Constructs a <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate} obj Optional instance to populate.
     * @return {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate} The populated <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate();

        if (data.hasOwnProperty('estimatedCost')) {
          obj['estimatedCost'] = _ApiClient["default"].convertToType(data['estimatedCost'], 'Number');
        }

        if (data.hasOwnProperty('estimatedSaving')) {
          obj['estimatedSaving'] = _ApiClient["default"].convertToType(data['estimatedSaving'], 'Number');
        }

        if (data.hasOwnProperty('consumption')) {
          obj['consumption'] = _ApiClient["default"].convertToType(data['consumption'], 'Number');
        }

        if (data.hasOwnProperty('previousCost')) {
          obj['previousCost'] = _ApiClient["default"].convertToType(data['previousCost'], 'Number');
        }

        if (data.hasOwnProperty('period')) {
          obj['period'] = _ApiClient["default"].convertToType(data['period'], 'String');
        }
      }

      return obj;
    }
  }]);

  return EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate;
}();
/**
 * Estimated cost of the new Gas Tariff
 * @member {Number} estimatedCost
 */


EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.prototype['estimatedCost'] = undefined;
/**
 * Estimated saving of the new Gas Tariff
 * @member {Number} estimatedSaving
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.prototype['estimatedSaving'] = undefined;
/**
 * Customer-provided estimation of their current gas consumption
 * @member {Number} consumption
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.prototype['consumption'] = undefined;
/**
 * Customer-provided estimation of their current gas spend
 * @member {Number} previousCost
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.prototype['previousCost'] = undefined;
/**
 * The period for the estimated gas values. Must be `Month` or `Year`(Request Parameter)
 * @member {String} period
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.prototype['period'] = undefined;
var _default = EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate;
exports["default"] = _default;