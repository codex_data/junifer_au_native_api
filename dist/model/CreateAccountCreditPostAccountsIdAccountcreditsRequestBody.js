"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountCreditPostAccountsIdAccountcreditsRequestBody model module.
 * @module model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody
 * @version 1.61.1
 */
var CreateAccountCreditPostAccountsIdAccountcreditsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountCreditPostAccountsIdAccountcreditsRequestBody</code>.
   * @alias module:model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody
   * @param grossAmount {Number} Gross amount of account credit
   * @param salesTaxName {String} Name of linked sales tax of account credit. Providing `\"None\"` will apply no sales tax to the account credit.
   * @param accountCreditReasonName {String} Name of linked account credit reason for account credit
   */
  function CreateAccountCreditPostAccountsIdAccountcreditsRequestBody(grossAmount, salesTaxName, accountCreditReasonName) {
    _classCallCheck(this, CreateAccountCreditPostAccountsIdAccountcreditsRequestBody);

    CreateAccountCreditPostAccountsIdAccountcreditsRequestBody.initialize(this, grossAmount, salesTaxName, accountCreditReasonName);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountCreditPostAccountsIdAccountcreditsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, grossAmount, salesTaxName, accountCreditReasonName) {
      obj['grossAmount'] = grossAmount;
      obj['salesTaxName'] = salesTaxName;
      obj['accountCreditReasonName'] = accountCreditReasonName;
    }
    /**
     * Constructs a <code>CreateAccountCreditPostAccountsIdAccountcreditsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody} obj Optional instance to populate.
     * @return {module:model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody} The populated <code>CreateAccountCreditPostAccountsIdAccountcreditsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountCreditPostAccountsIdAccountcreditsRequestBody();

        if (data.hasOwnProperty('grossAmount')) {
          obj['grossAmount'] = _ApiClient["default"].convertToType(data['grossAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTaxName')) {
          obj['salesTaxName'] = _ApiClient["default"].convertToType(data['salesTaxName'], 'String');
        }

        if (data.hasOwnProperty('accountCreditReasonName')) {
          obj['accountCreditReasonName'] = _ApiClient["default"].convertToType(data['accountCreditReasonName'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountCreditPostAccountsIdAccountcreditsRequestBody;
}();
/**
 * Gross amount of account credit
 * @member {Number} grossAmount
 */


CreateAccountCreditPostAccountsIdAccountcreditsRequestBody.prototype['grossAmount'] = undefined;
/**
 * Name of linked sales tax of account credit. Providing `\"None\"` will apply no sales tax to the account credit.
 * @member {String} salesTaxName
 */

CreateAccountCreditPostAccountsIdAccountcreditsRequestBody.prototype['salesTaxName'] = undefined;
/**
 * Name of linked account credit reason for account credit
 * @member {String} accountCreditReasonName
 */

CreateAccountCreditPostAccountsIdAccountcreditsRequestBody.prototype['accountCreditReasonName'] = undefined;
/**
 * Text identifier for the account debit
 * @member {String} reference
 */

CreateAccountCreditPostAccountsIdAccountcreditsRequestBody.prototype['reference'] = undefined;
/**
 * Description of the account transaction linked to the account credit
 * @member {String} description
 */

CreateAccountCreditPostAccountsIdAccountcreditsRequestBody.prototype['description'] = undefined;
var _default = CreateAccountCreditPostAccountsIdAccountcreditsRequestBody;
exports["default"] = _default;