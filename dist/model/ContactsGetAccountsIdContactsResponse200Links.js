"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The ContactsGetAccountsIdContactsResponse200Links model module.
 * @module model/ContactsGetAccountsIdContactsResponse200Links
 * @version 1.61.1
 */
var ContactsGetAccountsIdContactsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ContactsGetAccountsIdContactsResponse200Links</code>.
   * Links to related resources
   * @alias module:model/ContactsGetAccountsIdContactsResponse200Links
   * @param self {String} Link to this contact
   */
  function ContactsGetAccountsIdContactsResponse200Links(self) {
    _classCallCheck(this, ContactsGetAccountsIdContactsResponse200Links);

    ContactsGetAccountsIdContactsResponse200Links.initialize(this, self);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ContactsGetAccountsIdContactsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self) {
      obj['self'] = self;
    }
    /**
     * Constructs a <code>ContactsGetAccountsIdContactsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ContactsGetAccountsIdContactsResponse200Links} obj Optional instance to populate.
     * @return {module:model/ContactsGetAccountsIdContactsResponse200Links} The populated <code>ContactsGetAccountsIdContactsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ContactsGetAccountsIdContactsResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }
      }

      return obj;
    }
  }]);

  return ContactsGetAccountsIdContactsResponse200Links;
}();
/**
 * Link to this contact
 * @member {String} self
 */


ContactsGetAccountsIdContactsResponse200Links.prototype['self'] = undefined;
var _default = ContactsGetAccountsIdContactsResponse200Links;
exports["default"] = _default;