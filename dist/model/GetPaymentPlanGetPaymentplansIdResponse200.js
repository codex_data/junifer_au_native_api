"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions = _interopRequireDefault(require("./GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions"));

var _GetPaymentPlanGetPaymentplansIdResponse200Links = _interopRequireDefault(require("./GetPaymentPlanGetPaymentplansIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPaymentPlanGetPaymentplansIdResponse200 model module.
 * @module model/GetPaymentPlanGetPaymentplansIdResponse200
 * @version 1.61.1
 */
var GetPaymentPlanGetPaymentplansIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPaymentPlanGetPaymentplansIdResponse200</code>.
   * @alias module:model/GetPaymentPlanGetPaymentplansIdResponse200
   * @param id {Number} The ID of the payment plan
   * @param status {String} indicates the status of the payment plan
   * @param collectType {String} indicates how payments were collected
   * @param currency {String} The currency that payments will use
   * @param paymentPlanTransactions {Array.<module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions>} PaymentPlanTransactions Object
   * @param links {module:model/GetPaymentPlanGetPaymentplansIdResponse200Links} 
   */
  function GetPaymentPlanGetPaymentplansIdResponse200(id, status, collectType, currency, paymentPlanTransactions, links) {
    _classCallCheck(this, GetPaymentPlanGetPaymentplansIdResponse200);

    GetPaymentPlanGetPaymentplansIdResponse200.initialize(this, id, status, collectType, currency, paymentPlanTransactions, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPaymentPlanGetPaymentplansIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, status, collectType, currency, paymentPlanTransactions, links) {
      obj['id'] = id;
      obj['status'] = status;
      obj['collectType'] = collectType;
      obj['currency'] = currency;
      obj['paymentPlanTransactions'] = paymentPlanTransactions;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetPaymentPlanGetPaymentplansIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPaymentPlanGetPaymentplansIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetPaymentPlanGetPaymentplansIdResponse200} The populated <code>GetPaymentPlanGetPaymentplansIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPaymentPlanGetPaymentplansIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('collectType')) {
          obj['collectType'] = _ApiClient["default"].convertToType(data['collectType'], 'String');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('paymentPlanTransactions')) {
          obj['paymentPlanTransactions'] = _ApiClient["default"].convertToType(data['paymentPlanTransactions'], [_GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions["default"]]);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetPaymentPlanGetPaymentplansIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetPaymentPlanGetPaymentplansIdResponse200;
}();
/**
 * The ID of the payment plan
 * @member {Number} id
 */


GetPaymentPlanGetPaymentplansIdResponse200.prototype['id'] = undefined;
/**
 * indicates the status of the payment plan
 * @member {String} status
 */

GetPaymentPlanGetPaymentplansIdResponse200.prototype['status'] = undefined;
/**
 * DateTime when the payment plan was created
 * @member {Date} createdDttm
 */

GetPaymentPlanGetPaymentplansIdResponse200.prototype['createdDttm'] = undefined;
/**
 * indicates how payments were collected
 * @member {String} collectType
 */

GetPaymentPlanGetPaymentplansIdResponse200.prototype['collectType'] = undefined;
/**
 * The currency that payments will use
 * @member {String} currency
 */

GetPaymentPlanGetPaymentplansIdResponse200.prototype['currency'] = undefined;
/**
 * PaymentPlanTransactions Object
 * @member {Array.<module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions>} paymentPlanTransactions
 */

GetPaymentPlanGetPaymentplansIdResponse200.prototype['paymentPlanTransactions'] = undefined;
/**
 * @member {module:model/GetPaymentPlanGetPaymentplansIdResponse200Links} links
 */

GetPaymentPlanGetPaymentplansIdResponse200.prototype['links'] = undefined;
var _default = GetPaymentPlanGetPaymentplansIdResponse200;
exports["default"] = _default;