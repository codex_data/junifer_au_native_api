"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200 model module.
 * @module model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200
 * @version 1.61.1
 */
var GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200</code>.
   * @alias module:model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200
   * @param allPayReference {Number} AllPay reference number
   */
  function GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200(allPayReference) {
    _classCallCheck(this, GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200);

    GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200.initialize(this, allPayReference);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, allPayReference) {
      obj['allPayReference'] = allPayReference;
    }
    /**
     * Constructs a <code>GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200} obj Optional instance to populate.
     * @return {module:model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200} The populated <code>GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200();

        if (data.hasOwnProperty('allPayReference')) {
          obj['allPayReference'] = _ApiClient["default"].convertToType(data['allPayReference'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200;
}();
/**
 * AllPay reference number
 * @member {Number} allPayReference
 */


GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200.prototype['allPayReference'] = undefined;
var _default = GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200;
exports["default"] = _default;