"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The NewSepaDirectDebitPostSepadirectdebitsRequestBody model module.
 * @module model/NewSepaDirectDebitPostSepadirectdebitsRequestBody
 * @version 1.61.1
 */
var NewSepaDirectDebitPostSepadirectdebitsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>NewSepaDirectDebitPostSepadirectdebitsRequestBody</code>.
   * @alias module:model/NewSepaDirectDebitPostSepadirectdebitsRequestBody
   * @param accountName {String} Account name
   * @param iban {String} IBAN with no spaces
   */
  function NewSepaDirectDebitPostSepadirectdebitsRequestBody(accountName, iban) {
    _classCallCheck(this, NewSepaDirectDebitPostSepadirectdebitsRequestBody);

    NewSepaDirectDebitPostSepadirectdebitsRequestBody.initialize(this, accountName, iban);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(NewSepaDirectDebitPostSepadirectdebitsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, accountName, iban) {
      obj['accountName'] = accountName;
      obj['iban'] = iban;
    }
    /**
     * Constructs a <code>NewSepaDirectDebitPostSepadirectdebitsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/NewSepaDirectDebitPostSepadirectdebitsRequestBody} obj Optional instance to populate.
     * @return {module:model/NewSepaDirectDebitPostSepadirectdebitsRequestBody} The populated <code>NewSepaDirectDebitPostSepadirectdebitsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new NewSepaDirectDebitPostSepadirectdebitsRequestBody();

        if (data.hasOwnProperty('accountName')) {
          obj['accountName'] = _ApiClient["default"].convertToType(data['accountName'], 'String');
        }

        if (data.hasOwnProperty('iban')) {
          obj['iban'] = _ApiClient["default"].convertToType(data['iban'], 'String');
        }
      }

      return obj;
    }
  }]);

  return NewSepaDirectDebitPostSepadirectdebitsRequestBody;
}();
/**
 * Account name
 * @member {String} accountName
 */


NewSepaDirectDebitPostSepadirectdebitsRequestBody.prototype['accountName'] = undefined;
/**
 * IBAN with no spaces
 * @member {String} iban
 */

NewSepaDirectDebitPostSepadirectdebitsRequestBody.prototype['iban'] = undefined;
var _default = NewSepaDirectDebitPostSepadirectdebitsRequestBody;
exports["default"] = _default;