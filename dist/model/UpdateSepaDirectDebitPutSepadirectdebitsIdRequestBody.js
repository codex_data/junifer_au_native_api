"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody model module.
 * @module model/UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody
 * @version 1.61.1
 */
var UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody</code>.
   * @alias module:model/UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody
   * @param accountName {String} Bank account name
   * @param iban {String} IBAN
   * @param mandateReference {String} Mandate Reference
   */
  function UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody(accountName, iban, mandateReference) {
    _classCallCheck(this, UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody);

    UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody.initialize(this, accountName, iban, mandateReference);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, accountName, iban, mandateReference) {
      obj['accountName'] = accountName;
      obj['iban'] = iban;
      obj['mandateReference'] = mandateReference;
    }
    /**
     * Constructs a <code>UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody} obj Optional instance to populate.
     * @return {module:model/UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody} The populated <code>UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody();

        if (data.hasOwnProperty('accountName')) {
          obj['accountName'] = _ApiClient["default"].convertToType(data['accountName'], 'String');
        }

        if (data.hasOwnProperty('iban')) {
          obj['iban'] = _ApiClient["default"].convertToType(data['iban'], 'String');
        }

        if (data.hasOwnProperty('mandateReference')) {
          obj['mandateReference'] = _ApiClient["default"].convertToType(data['mandateReference'], 'String');
        }

        if (data.hasOwnProperty('authorisedDttm')) {
          obj['authorisedDttm'] = _ApiClient["default"].convertToType(data['authorisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('terminatedDttm')) {
          obj['terminatedDttm'] = _ApiClient["default"].convertToType(data['terminatedDttm'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody;
}();
/**
 * Bank account name
 * @member {String} accountName
 */


UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody.prototype['accountName'] = undefined;
/**
 * IBAN
 * @member {String} iban
 */

UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody.prototype['iban'] = undefined;
/**
 * Mandate Reference
 * @member {String} mandateReference
 */

UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody.prototype['mandateReference'] = undefined;
/**
 * Date when the mandate was authorised
 * @member {Date} authorisedDttm
 */

UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody.prototype['authorisedDttm'] = undefined;
/**
 * Date when the mandate was terminated
 * @member {Date} terminatedDttm
 */

UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody.prototype['terminatedDttm'] = undefined;
var _default = UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody;
exports["default"] = _default;