"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountPaymentsGetAccountsIdPaymentsResponse200Links = _interopRequireDefault(require("./GetAccountPaymentsGetAccountsIdPaymentsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPaymentGetPaymentsIdResponse200 model module.
 * @module model/GetPaymentGetPaymentsIdResponse200
 * @version 1.61.1
 */
var GetPaymentGetPaymentsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPaymentGetPaymentsIdResponse200</code>.
   * @alias module:model/GetPaymentGetPaymentsIdResponse200
   * @param id {Number} The ID of the payment
   * @param amount {Number} The amount of the payment
   * @param currency {String} Currency ISO code
   * @param paymentMethod {String} The payment method
   * @param createdDttm {Date} The date when the payment was created
   * @param requestStatus {String} The status of the associated payment request. Can be one of following: `Pending, Requesting, Successful, Cancelled, FailedPendingRetry, FailedRequestingAgain`
   * @param createdBy {String} The source of the payment. Can be one of the following: `API User, SYSTEM User`
   * @param links {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links} 
   */
  function GetPaymentGetPaymentsIdResponse200(id, amount, currency, paymentMethod, createdDttm, requestStatus, createdBy, links) {
    _classCallCheck(this, GetPaymentGetPaymentsIdResponse200);

    GetPaymentGetPaymentsIdResponse200.initialize(this, id, amount, currency, paymentMethod, createdDttm, requestStatus, createdBy, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPaymentGetPaymentsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, amount, currency, paymentMethod, createdDttm, requestStatus, createdBy, links) {
      obj['id'] = id;
      obj['amount'] = amount;
      obj['currency'] = currency;
      obj['paymentMethod'] = paymentMethod;
      obj['createdDttm'] = createdDttm;
      obj['requestStatus'] = requestStatus;
      obj['createdBy'] = createdBy;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetPaymentGetPaymentsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPaymentGetPaymentsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetPaymentGetPaymentsIdResponse200} The populated <code>GetPaymentGetPaymentsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPaymentGetPaymentsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('apiIdempotencyKey')) {
          obj['apiIdempotencyKey'] = _ApiClient["default"].convertToType(data['apiIdempotencyKey'], 'String');
        }

        if (data.hasOwnProperty('paymentMethod')) {
          obj['paymentMethod'] = _ApiClient["default"].convertToType(data['paymentMethod'], 'String');
        }

        if (data.hasOwnProperty('acceptedDttm')) {
          obj['acceptedDttm'] = _ApiClient["default"].convertToType(data['acceptedDttm'], 'Date');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('cancelledDttm')) {
          obj['cancelledDttm'] = _ApiClient["default"].convertToType(data['cancelledDttm'], 'Date');
        }

        if (data.hasOwnProperty('postedDt')) {
          obj['postedDt'] = _ApiClient["default"].convertToType(data['postedDt'], 'Date');
        }

        if (data.hasOwnProperty('requestStatus')) {
          obj['requestStatus'] = _ApiClient["default"].convertToType(data['requestStatus'], 'String');
        }

        if (data.hasOwnProperty('createdBy')) {
          obj['createdBy'] = _ApiClient["default"].convertToType(data['createdBy'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountPaymentsGetAccountsIdPaymentsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetPaymentGetPaymentsIdResponse200;
}();
/**
 * The ID of the payment
 * @member {Number} id
 */


GetPaymentGetPaymentsIdResponse200.prototype['id'] = undefined;
/**
 * The amount of the payment
 * @member {Number} amount
 */

GetPaymentGetPaymentsIdResponse200.prototype['amount'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

GetPaymentGetPaymentsIdResponse200.prototype['currency'] = undefined;
/**
 * Key in UUID format (to prevent duplicate payment requests) if specified
 * @member {String} apiIdempotencyKey
 */

GetPaymentGetPaymentsIdResponse200.prototype['apiIdempotencyKey'] = undefined;
/**
 * The payment method
 * @member {String} paymentMethod
 */

GetPaymentGetPaymentsIdResponse200.prototype['paymentMethod'] = undefined;
/**
 * The date when the payment has been accepted
 * @member {Date} acceptedDttm
 */

GetPaymentGetPaymentsIdResponse200.prototype['acceptedDttm'] = undefined;
/**
 * The date when the payment was created
 * @member {Date} createdDttm
 */

GetPaymentGetPaymentsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * The date when the payment was cancelled
 * @member {Date} cancelledDttm
 */

GetPaymentGetPaymentsIdResponse200.prototype['cancelledDttm'] = undefined;
/**
 * The date when the payment has been posted
 * @member {Date} postedDt
 */

GetPaymentGetPaymentsIdResponse200.prototype['postedDt'] = undefined;
/**
 * The status of the associated payment request. Can be one of following: `Pending, Requesting, Successful, Cancelled, FailedPendingRetry, FailedRequestingAgain`
 * @member {String} requestStatus
 */

GetPaymentGetPaymentsIdResponse200.prototype['requestStatus'] = undefined;
/**
 * The source of the payment. Can be one of the following: `API User, SYSTEM User`
 * @member {String} createdBy
 */

GetPaymentGetPaymentsIdResponse200.prototype['createdBy'] = undefined;
/**
 * @member {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links} links
 */

GetPaymentGetPaymentsIdResponse200.prototype['links'] = undefined;
var _default = GetPaymentGetPaymentsIdResponse200;
exports["default"] = _default;