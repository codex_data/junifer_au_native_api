"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo model module.
 * @module model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo
 * @version 1.61.1
 */
var EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo</code>.
   * Direct Debit information if the customer selected to pay by Direct Debit
   * @alias module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo
   * @param bankAccountSortCode {String} Customer's bank's sort code
   * @param bankAccountNumber {String} Customer's bank's account number
   * @param bankAccountName {String} Customer's bank's account name
   */
  function EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo(bankAccountSortCode, bankAccountNumber, bankAccountName) {
    _classCallCheck(this, EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo);

    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.initialize(this, bankAccountSortCode, bankAccountNumber, bankAccountName);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo, null, [{
    key: "initialize",
    value: function initialize(obj, bankAccountSortCode, bankAccountNumber, bankAccountName) {
      obj['bankAccountSortCode'] = bankAccountSortCode;
      obj['bankAccountNumber'] = bankAccountNumber;
      obj['bankAccountName'] = bankAccountName;
    }
    /**
     * Constructs a <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo} obj Optional instance to populate.
     * @return {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo} The populated <code>EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo();

        if (data.hasOwnProperty('bankAccountSortCode')) {
          obj['bankAccountSortCode'] = _ApiClient["default"].convertToType(data['bankAccountSortCode'], 'String');
        }

        if (data.hasOwnProperty('bankAccountNumber')) {
          obj['bankAccountNumber'] = _ApiClient["default"].convertToType(data['bankAccountNumber'], 'String');
        }

        if (data.hasOwnProperty('bankAccountName')) {
          obj['bankAccountName'] = _ApiClient["default"].convertToType(data['bankAccountName'], 'String');
        }

        if (data.hasOwnProperty('regularPaymentAmount')) {
          obj['regularPaymentAmount'] = _ApiClient["default"].convertToType(data['regularPaymentAmount'], 'Number');
        }

        if (data.hasOwnProperty('regularPaymentDayOfMonth')) {
          obj['regularPaymentDayOfMonth'] = _ApiClient["default"].convertToType(data['regularPaymentDayOfMonth'], 'Number');
        }

        if (data.hasOwnProperty('seasonalPaymentFl')) {
          obj['seasonalPaymentFl'] = _ApiClient["default"].convertToType(data['seasonalPaymentFl'], 'Boolean');
        }
      }

      return obj;
    }
  }]);

  return EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo;
}();
/**
 * Customer's bank's sort code
 * @member {String} bankAccountSortCode
 */


EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.prototype['bankAccountSortCode'] = undefined;
/**
 * Customer's bank's account number
 * @member {String} bankAccountNumber
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.prototype['bankAccountNumber'] = undefined;
/**
 * Customer's bank's account name
 * @member {String} bankAccountName
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.prototype['bankAccountName'] = undefined;
/**
 * The amount in pounds which customer selected to pay by Regular (Fixed) Direct Debit
 * @member {Number} regularPaymentAmount
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.prototype['regularPaymentAmount'] = undefined;
/**
 * Day of the month when the Regular (Fixed) Direct Debit amount should be taken from the customer's bank account. Must be a value between 1-28 inclusive.
 * @member {Number} regularPaymentDayOfMonth
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.prototype['regularPaymentDayOfMonth'] = undefined;
/**
 * A boolean flag indicating whether the customer wants to pay a variable amount during the high and low seasons, defined by the active Payment Schedule Seasonal Definition(Request Parameter)
 * @member {Boolean} seasonalPaymentFl
 */

EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.prototype['seasonalPaymentFl'] = undefined;
var _default = EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo;
exports["default"] = _default;