"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetCreditGetCreditsIdResponse200Links = _interopRequireDefault(require("./GetCreditGetCreditsIdResponse200Links1"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCreditGetCreditsIdResponse200CreditFiles model module.
 * @module model/GetCreditGetCreditsIdResponse200CreditFiles
 * @version 1.61.1
 */
var GetCreditGetCreditsIdResponse200CreditFiles = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCreditGetCreditsIdResponse200CreditFiles</code>.
   * @alias module:model/GetCreditGetCreditsIdResponse200CreditFiles
   * @param id {Number} Credit file ID
   * @param display {String} Credit file type
   * @param viewable {Boolean} Flag indicating whether the credit file can be viewed
   * @param links {module:model/GetCreditGetCreditsIdResponse200Links1} 
   */
  function GetCreditGetCreditsIdResponse200CreditFiles(id, display, viewable, links) {
    _classCallCheck(this, GetCreditGetCreditsIdResponse200CreditFiles);

    GetCreditGetCreditsIdResponse200CreditFiles.initialize(this, id, display, viewable, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCreditGetCreditsIdResponse200CreditFiles, null, [{
    key: "initialize",
    value: function initialize(obj, id, display, viewable, links) {
      obj['id'] = id;
      obj['display'] = display;
      obj['viewable'] = viewable;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetCreditGetCreditsIdResponse200CreditFiles</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCreditGetCreditsIdResponse200CreditFiles} obj Optional instance to populate.
     * @return {module:model/GetCreditGetCreditsIdResponse200CreditFiles} The populated <code>GetCreditGetCreditsIdResponse200CreditFiles</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCreditGetCreditsIdResponse200CreditFiles();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('display')) {
          obj['display'] = _ApiClient["default"].convertToType(data['display'], 'String');
        }

        if (data.hasOwnProperty('viewable')) {
          obj['viewable'] = _ApiClient["default"].convertToType(data['viewable'], 'Boolean');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetCreditGetCreditsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetCreditGetCreditsIdResponse200CreditFiles;
}();
/**
 * Credit file ID
 * @member {Number} id
 */


GetCreditGetCreditsIdResponse200CreditFiles.prototype['id'] = undefined;
/**
 * Credit file type
 * @member {String} display
 */

GetCreditGetCreditsIdResponse200CreditFiles.prototype['display'] = undefined;
/**
 * Flag indicating whether the credit file can be viewed
 * @member {Boolean} viewable
 */

GetCreditGetCreditsIdResponse200CreditFiles.prototype['viewable'] = undefined;
/**
 * @member {module:model/GetCreditGetCreditsIdResponse200Links1} links
 */

GetCreditGetCreditsIdResponse200CreditFiles.prototype['links'] = undefined;
var _default = GetCreditGetCreditsIdResponse200CreditFiles;
exports["default"] = _default;