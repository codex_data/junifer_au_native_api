"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _BillEmailsGetBillemailsIdResponse200Files = _interopRequireDefault(require("./BillEmailsGetBillemailsIdResponse200Files"));

var _BillEmailsGetBillemailsIdResponse200Links = _interopRequireDefault(require("./BillEmailsGetBillemailsIdResponse200Links1"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The BillEmailsGetBillemailsIdResponse200 model module.
 * @module model/BillEmailsGetBillemailsIdResponse200
 * @version 1.61.1
 */
var BillEmailsGetBillemailsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>BillEmailsGetBillemailsIdResponse200</code>.
   * @alias module:model/BillEmailsGetBillemailsIdResponse200
   * @param id {Number} Bill ID (not email id)
   * @param subject {String} Email subject
   * @param createdDttm {Date} Email creation date
   * @param body {String} Email body
   * @param files {Array.<module:model/BillEmailsGetBillemailsIdResponse200Files>} Email attachments
   * @param links {module:model/BillEmailsGetBillemailsIdResponse200Links1} 
   */
  function BillEmailsGetBillemailsIdResponse200(id, subject, createdDttm, body, files, links) {
    _classCallCheck(this, BillEmailsGetBillemailsIdResponse200);

    BillEmailsGetBillemailsIdResponse200.initialize(this, id, subject, createdDttm, body, files, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(BillEmailsGetBillemailsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, subject, createdDttm, body, files, links) {
      obj['id'] = id;
      obj['subject'] = subject;
      obj['createdDttm'] = createdDttm;
      obj['body'] = body;
      obj['files'] = files;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>BillEmailsGetBillemailsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/BillEmailsGetBillemailsIdResponse200} obj Optional instance to populate.
     * @return {module:model/BillEmailsGetBillemailsIdResponse200} The populated <code>BillEmailsGetBillemailsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new BillEmailsGetBillemailsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('deliveryStatus')) {
          obj['deliveryStatus'] = _ApiClient["default"].convertToType(data['deliveryStatus'], 'String');
        }

        if (data.hasOwnProperty('subject')) {
          obj['subject'] = _ApiClient["default"].convertToType(data['subject'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('body')) {
          obj['body'] = _ApiClient["default"].convertToType(data['body'], 'String');
        }

        if (data.hasOwnProperty('files')) {
          obj['files'] = _ApiClient["default"].convertToType(data['files'], [_BillEmailsGetBillemailsIdResponse200Files["default"]]);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _BillEmailsGetBillemailsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return BillEmailsGetBillemailsIdResponse200;
}();
/**
 * Bill ID (not email id)
 * @member {Number} id
 */


BillEmailsGetBillemailsIdResponse200.prototype['id'] = undefined;
/**
 * Communication email delivery status. The status will be one of the following: \"Draft\", \"Sending\", \"Sent\", \"Failed\", \"Retrying\", \"Cancelled\".
 * @member {String} deliveryStatus
 */

BillEmailsGetBillemailsIdResponse200.prototype['deliveryStatus'] = undefined;
/**
 * Email subject
 * @member {String} subject
 */

BillEmailsGetBillemailsIdResponse200.prototype['subject'] = undefined;
/**
 * Email creation date
 * @member {Date} createdDttm
 */

BillEmailsGetBillemailsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * Email body
 * @member {String} body
 */

BillEmailsGetBillemailsIdResponse200.prototype['body'] = undefined;
/**
 * Email attachments
 * @member {Array.<module:model/BillEmailsGetBillemailsIdResponse200Files>} files
 */

BillEmailsGetBillemailsIdResponse200.prototype['files'] = undefined;
/**
 * @member {module:model/BillEmailsGetBillemailsIdResponse200Links1} links
 */

BillEmailsGetBillemailsIdResponse200.prototype['links'] = undefined;
var _default = BillEmailsGetBillemailsIdResponse200;
exports["default"] = _default;