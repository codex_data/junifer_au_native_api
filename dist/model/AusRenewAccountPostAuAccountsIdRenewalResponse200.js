"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AusRenewAccountPostAuAccountsIdRenewalResponse200Results = _interopRequireDefault(require("./AusRenewAccountPostAuAccountsIdRenewalResponse200Results"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusRenewAccountPostAuAccountsIdRenewalResponse200 model module.
 * @module model/AusRenewAccountPostAuAccountsIdRenewalResponse200
 * @version 1.61.1
 */
var AusRenewAccountPostAuAccountsIdRenewalResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusRenewAccountPostAuAccountsIdRenewalResponse200</code>.
   * @alias module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200
   * @param results {Array.<module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200Results>} Array with ids of new agreements created
   */
  function AusRenewAccountPostAuAccountsIdRenewalResponse200(results) {
    _classCallCheck(this, AusRenewAccountPostAuAccountsIdRenewalResponse200);

    AusRenewAccountPostAuAccountsIdRenewalResponse200.initialize(this, results);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusRenewAccountPostAuAccountsIdRenewalResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, results) {
      obj['results'] = results;
    }
    /**
     * Constructs a <code>AusRenewAccountPostAuAccountsIdRenewalResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200} obj Optional instance to populate.
     * @return {module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200} The populated <code>AusRenewAccountPostAuAccountsIdRenewalResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusRenewAccountPostAuAccountsIdRenewalResponse200();

        if (data.hasOwnProperty('results')) {
          obj['results'] = _ApiClient["default"].convertToType(data['results'], [_AusRenewAccountPostAuAccountsIdRenewalResponse200Results["default"]]);
        }
      }

      return obj;
    }
  }]);

  return AusRenewAccountPostAuAccountsIdRenewalResponse200;
}();
/**
 * Array with ids of new agreements created
 * @member {Array.<module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200Results>} results
 */


AusRenewAccountPostAuAccountsIdRenewalResponse200.prototype['results'] = undefined;
var _default = AusRenewAccountPostAuAccountsIdRenewalResponse200;
exports["default"] = _default;