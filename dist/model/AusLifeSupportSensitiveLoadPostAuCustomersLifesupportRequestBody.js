"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody model module.
 * @module model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody
 * @version 1.61.1
 */
var AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody</code>.
   * @alias module:model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody
   * @param nmi {String} Identifier of the NMI including checksum
   * @param vulnerabilityType {String} Life Support or Sensitive Load
   * @param lifeSupportEquipment {String} AU Market allowable values for Life Support equipment type i.e. \"Oxygen Concentrator\", \"Intermittent Peritoneal Dialysis Machine\", \"Kidney Dialysis Machine\", \"Chronic Positive Airways Pressure Respirator\", \"Crigler Najjar Syndrome Phototherapy Equipment\", \"Ventilator For Life Support\", \"Other\"
   * @param lifeSupportStatus {String} AU Market allowable values for Life Support status i.e. \"Registered - No Medical Confirmation\", \"Registered - Medical Confirmation\", \"Deregistered - No Medical Confirmation\", \"Deregistered - Customer Advice\", \"Deregistered - No Customer Response\", \"None\"
   * @param contactTitle {String} Title of the person who is the contact for the management of Life Support requirements. i.e. \"Ms\", \"Mrs\", \"Mr\", \"Dr\", \"Prof\"
   * @param contactFirstName {String} First name of the person who is the contact for the management of Life Support requirements.
   * @param contactLastName {String} Last name of the person who is the contact for the management of Life Support requirements.
   * @param phoneNumber {String} Phone number is required when preferred contact method is via Phone
   * @param preferredContactMethod {String} AU Market allowable values for Contact Method i.e. \"Email Address\", \"Phone\", \"Postal Address\", \"Site Address\"
   */
  function AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody(nmi, vulnerabilityType, lifeSupportEquipment, lifeSupportStatus, contactTitle, contactFirstName, contactLastName, phoneNumber, preferredContactMethod) {
    _classCallCheck(this, AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody);

    AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.initialize(this, nmi, vulnerabilityType, lifeSupportEquipment, lifeSupportStatus, contactTitle, contactFirstName, contactLastName, phoneNumber, preferredContactMethod);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, nmi, vulnerabilityType, lifeSupportEquipment, lifeSupportStatus, contactTitle, contactFirstName, contactLastName, phoneNumber, preferredContactMethod) {
      obj['nmi'] = nmi;
      obj['vulnerabilityType'] = vulnerabilityType;
      obj['lifeSupportEquipment'] = lifeSupportEquipment;
      obj['lifeSupportStatus'] = lifeSupportStatus;
      obj['contactTitle'] = contactTitle;
      obj['contactFirstName'] = contactFirstName;
      obj['contactLastName'] = contactLastName;
      obj['phoneNumber'] = phoneNumber;
      obj['preferredContactMethod'] = preferredContactMethod;
    }
    /**
     * Constructs a <code>AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody} obj Optional instance to populate.
     * @return {module:model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody} The populated <code>AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody();

        if (data.hasOwnProperty('nmi')) {
          obj['nmi'] = _ApiClient["default"].convertToType(data['nmi'], 'String');
        }

        if (data.hasOwnProperty('vulnerabilityType')) {
          obj['vulnerabilityType'] = _ApiClient["default"].convertToType(data['vulnerabilityType'], 'String');
        }

        if (data.hasOwnProperty('lifeSupportEquipment')) {
          obj['lifeSupportEquipment'] = _ApiClient["default"].convertToType(data['lifeSupportEquipment'], 'String');
        }

        if (data.hasOwnProperty('lifeSupportStatus')) {
          obj['lifeSupportStatus'] = _ApiClient["default"].convertToType(data['lifeSupportStatus'], 'String');
        }

        if (data.hasOwnProperty('contactTitle')) {
          obj['contactTitle'] = _ApiClient["default"].convertToType(data['contactTitle'], 'String');
        }

        if (data.hasOwnProperty('contactFirstName')) {
          obj['contactFirstName'] = _ApiClient["default"].convertToType(data['contactFirstName'], 'String');
        }

        if (data.hasOwnProperty('contactLastName')) {
          obj['contactLastName'] = _ApiClient["default"].convertToType(data['contactLastName'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber')) {
          obj['phoneNumber'] = _ApiClient["default"].convertToType(data['phoneNumber'], 'String');
        }

        if (data.hasOwnProperty('alternatePhoneNumber')) {
          obj['alternatePhoneNumber'] = _ApiClient["default"].convertToType(data['alternatePhoneNumber'], 'String');
        }

        if (data.hasOwnProperty('emailAddress')) {
          obj['emailAddress'] = _ApiClient["default"].convertToType(data['emailAddress'], 'String');
        }

        if (data.hasOwnProperty('preferredContactMethod')) {
          obj['preferredContactMethod'] = _ApiClient["default"].convertToType(data['preferredContactMethod'], 'String');
        }

        if (data.hasOwnProperty('specialNotes')) {
          obj['specialNotes'] = _ApiClient["default"].convertToType(data['specialNotes'], 'String');
        }
      }

      return obj;
    }
  }]);

  return AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody;
}();
/**
 * Identifier of the NMI including checksum
 * @member {String} nmi
 */


AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['nmi'] = undefined;
/**
 * Life Support or Sensitive Load
 * @member {String} vulnerabilityType
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['vulnerabilityType'] = undefined;
/**
 * AU Market allowable values for Life Support equipment type i.e. \"Oxygen Concentrator\", \"Intermittent Peritoneal Dialysis Machine\", \"Kidney Dialysis Machine\", \"Chronic Positive Airways Pressure Respirator\", \"Crigler Najjar Syndrome Phototherapy Equipment\", \"Ventilator For Life Support\", \"Other\"
 * @member {String} lifeSupportEquipment
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['lifeSupportEquipment'] = undefined;
/**
 * AU Market allowable values for Life Support status i.e. \"Registered - No Medical Confirmation\", \"Registered - Medical Confirmation\", \"Deregistered - No Medical Confirmation\", \"Deregistered - Customer Advice\", \"Deregistered - No Customer Response\", \"None\"
 * @member {String} lifeSupportStatus
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['lifeSupportStatus'] = undefined;
/**
 * Title of the person who is the contact for the management of Life Support requirements. i.e. \"Ms\", \"Mrs\", \"Mr\", \"Dr\", \"Prof\"
 * @member {String} contactTitle
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['contactTitle'] = undefined;
/**
 * First name of the person who is the contact for the management of Life Support requirements.
 * @member {String} contactFirstName
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['contactFirstName'] = undefined;
/**
 * Last name of the person who is the contact for the management of Life Support requirements.
 * @member {String} contactLastName
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['contactLastName'] = undefined;
/**
 * Phone number is required when preferred contact method is via Phone
 * @member {String} phoneNumber
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['phoneNumber'] = undefined;
/**
 * Alternative contact number
 * @member {String} alternatePhoneNumber
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['alternatePhoneNumber'] = undefined;
/**
 * Email address is required when preferred contact method is via Email Address
 * @member {String} emailAddress
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['emailAddress'] = undefined;
/**
 * AU Market allowable values for Contact Method i.e. \"Email Address\", \"Phone\", \"Postal Address\", \"Site Address\"
 * @member {String} preferredContactMethod
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['preferredContactMethod'] = undefined;
/**
 * Additional notes for Sensitive Load or Life Support. Mandatory if Life Support equipment is 'Other' to provide details
 * @member {String} specialNotes
 */

AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.prototype['specialNotes'] = undefined;
var _default = AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody;
exports["default"] = _default;