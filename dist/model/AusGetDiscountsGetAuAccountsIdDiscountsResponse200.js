"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusGetDiscountsGetAuAccountsIdDiscountsResponse200 model module.
 * @module model/AusGetDiscountsGetAuAccountsIdDiscountsResponse200
 * @version 1.61.1
 */
var AusGetDiscountsGetAuAccountsIdDiscountsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusGetDiscountsGetAuAccountsIdDiscountsResponse200</code>.
   * @alias module:model/AusGetDiscountsGetAuAccountsIdDiscountsResponse200
   * @param id {Number} The ID of the DiscountDfn
   * @param name {String} The discount name
   * @param fromDt {Date} The from date of the account discount
   * @param toDt {Date} The to date of the account discount (not returned for open-ended discounts)
   * @param billingEntityId {Number} The ID of the billing entity
   * @param currency {String} The ISO code of the currency
   */
  function AusGetDiscountsGetAuAccountsIdDiscountsResponse200(id, name, fromDt, toDt, billingEntityId, currency) {
    _classCallCheck(this, AusGetDiscountsGetAuAccountsIdDiscountsResponse200);

    AusGetDiscountsGetAuAccountsIdDiscountsResponse200.initialize(this, id, name, fromDt, toDt, billingEntityId, currency);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusGetDiscountsGetAuAccountsIdDiscountsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, name, fromDt, toDt, billingEntityId, currency) {
      obj['id'] = id;
      obj['name'] = name;
      obj['fromDt'] = fromDt;
      obj['toDt'] = toDt;
      obj['billingEntityId'] = billingEntityId;
      obj['currency'] = currency;
    }
    /**
     * Constructs a <code>AusGetDiscountsGetAuAccountsIdDiscountsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusGetDiscountsGetAuAccountsIdDiscountsResponse200} obj Optional instance to populate.
     * @return {module:model/AusGetDiscountsGetAuAccountsIdDiscountsResponse200} The populated <code>AusGetDiscountsGetAuAccountsIdDiscountsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusGetDiscountsGetAuAccountsIdDiscountsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }

        if (data.hasOwnProperty('billingEntityId')) {
          obj['billingEntityId'] = _ApiClient["default"].convertToType(data['billingEntityId'], 'Number');
        }

        if (data.hasOwnProperty('billingEntityCode')) {
          obj['billingEntityCode'] = _ApiClient["default"].convertToType(data['billingEntityCode'], 'String');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }
      }

      return obj;
    }
  }]);

  return AusGetDiscountsGetAuAccountsIdDiscountsResponse200;
}();
/**
 * The ID of the DiscountDfn
 * @member {Number} id
 */


AusGetDiscountsGetAuAccountsIdDiscountsResponse200.prototype['id'] = undefined;
/**
 * The discount name
 * @member {String} name
 */

AusGetDiscountsGetAuAccountsIdDiscountsResponse200.prototype['name'] = undefined;
/**
 * The from date of the account discount
 * @member {Date} fromDt
 */

AusGetDiscountsGetAuAccountsIdDiscountsResponse200.prototype['fromDt'] = undefined;
/**
 * The to date of the account discount (not returned for open-ended discounts)
 * @member {Date} toDt
 */

AusGetDiscountsGetAuAccountsIdDiscountsResponse200.prototype['toDt'] = undefined;
/**
 * The ID of the billing entity
 * @member {Number} billingEntityId
 */

AusGetDiscountsGetAuAccountsIdDiscountsResponse200.prototype['billingEntityId'] = undefined;
/**
 * The code of the billing entity
 * @member {String} billingEntityCode
 */

AusGetDiscountsGetAuAccountsIdDiscountsResponse200.prototype['billingEntityCode'] = undefined;
/**
 * The ISO code of the currency
 * @member {String} currency
 */

AusGetDiscountsGetAuAccountsIdDiscountsResponse200.prototype['currency'] = undefined;
var _default = AusGetDiscountsGetAuAccountsIdDiscountsResponse200;
exports["default"] = _default;