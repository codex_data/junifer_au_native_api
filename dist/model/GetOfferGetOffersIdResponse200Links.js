"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetOfferGetOffersIdResponse200Links model module.
 * @module model/GetOfferGetOffersIdResponse200Links
 * @version 1.61.1
 */
var GetOfferGetOffersIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetOfferGetOffersIdResponse200Links</code>.
   * Links to any related content associated with the offer.
   * @alias module:model/GetOfferGetOffersIdResponse200Links
   * @param self {String} Link to the offer itself.
   * @param prospect {String} The associated prospect for the offer.
   * @param broker {String} The associated broker for the offer.
   */
  function GetOfferGetOffersIdResponse200Links(self, prospect, broker) {
    _classCallCheck(this, GetOfferGetOffersIdResponse200Links);

    GetOfferGetOffersIdResponse200Links.initialize(this, self, prospect, broker);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetOfferGetOffersIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, prospect, broker) {
      obj['self'] = self;
      obj['prospect'] = prospect;
      obj['broker'] = broker;
    }
    /**
     * Constructs a <code>GetOfferGetOffersIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetOfferGetOffersIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetOfferGetOffersIdResponse200Links} The populated <code>GetOfferGetOffersIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetOfferGetOffersIdResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('prospect')) {
          obj['prospect'] = _ApiClient["default"].convertToType(data['prospect'], 'String');
        }

        if (data.hasOwnProperty('broker')) {
          obj['broker'] = _ApiClient["default"].convertToType(data['broker'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetOfferGetOffersIdResponse200Links;
}();
/**
 * Link to the offer itself.
 * @member {String} self
 */


GetOfferGetOffersIdResponse200Links.prototype['self'] = undefined;
/**
 * The associated prospect for the offer.
 * @member {String} prospect
 */

GetOfferGetOffersIdResponse200Links.prototype['prospect'] = undefined;
/**
 * The associated broker for the offer.
 * @member {String} broker
 */

GetOfferGetOffersIdResponse200Links.prototype['broker'] = undefined;
var _default = GetOfferGetOffersIdResponse200Links;
exports["default"] = _default;