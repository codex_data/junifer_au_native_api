"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The RenewAccountPostAccountsIdRenewalResponse200 model module.
 * @module model/RenewAccountPostAccountsIdRenewalResponse200
 * @version 1.61.1
 */
var RenewAccountPostAccountsIdRenewalResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>RenewAccountPostAccountsIdRenewalResponse200</code>.
   * @alias module:model/RenewAccountPostAccountsIdRenewalResponse200
   * @param accountNumber {String} Account Number
   * @param elecProductBundleId {String} ID of the newly generated electricity product bundle
   * @param gasProductBundleId {String} ID of the newly generated gas product bundle
   */
  function RenewAccountPostAccountsIdRenewalResponse200(accountNumber, elecProductBundleId, gasProductBundleId) {
    _classCallCheck(this, RenewAccountPostAccountsIdRenewalResponse200);

    RenewAccountPostAccountsIdRenewalResponse200.initialize(this, accountNumber, elecProductBundleId, gasProductBundleId);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(RenewAccountPostAccountsIdRenewalResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, accountNumber, elecProductBundleId, gasProductBundleId) {
      obj['accountNumber'] = accountNumber;
      obj['elecProductBundleId'] = elecProductBundleId;
      obj['gasProductBundleId'] = gasProductBundleId;
    }
    /**
     * Constructs a <code>RenewAccountPostAccountsIdRenewalResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RenewAccountPostAccountsIdRenewalResponse200} obj Optional instance to populate.
     * @return {module:model/RenewAccountPostAccountsIdRenewalResponse200} The populated <code>RenewAccountPostAccountsIdRenewalResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new RenewAccountPostAccountsIdRenewalResponse200();

        if (data.hasOwnProperty('accountNumber')) {
          obj['accountNumber'] = _ApiClient["default"].convertToType(data['accountNumber'], 'String');
        }

        if (data.hasOwnProperty('elecProductBundleId')) {
          obj['elecProductBundleId'] = _ApiClient["default"].convertToType(data['elecProductBundleId'], 'String');
        }

        if (data.hasOwnProperty('gasProductBundleId')) {
          obj['gasProductBundleId'] = _ApiClient["default"].convertToType(data['gasProductBundleId'], 'String');
        }
      }

      return obj;
    }
  }]);

  return RenewAccountPostAccountsIdRenewalResponse200;
}();
/**
 * Account Number
 * @member {String} accountNumber
 */


RenewAccountPostAccountsIdRenewalResponse200.prototype['accountNumber'] = undefined;
/**
 * ID of the newly generated electricity product bundle
 * @member {String} elecProductBundleId
 */

RenewAccountPostAccountsIdRenewalResponse200.prototype['elecProductBundleId'] = undefined;
/**
 * ID of the newly generated gas product bundle
 * @member {String} gasProductBundleId
 */

RenewAccountPostAccountsIdRenewalResponse200.prototype['gasProductBundleId'] = undefined;
var _default = RenewAccountPostAccountsIdRenewalResponse200;
exports["default"] = _default;