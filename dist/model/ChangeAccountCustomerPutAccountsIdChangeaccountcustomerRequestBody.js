"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody model module.
 * @module model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody
 * @version 1.61.1
 */
var ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody</code>.
   * @alias module:model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody
   * @param id {Number} The ID of customer to which this account will be linked to
   */
  function ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody(id) {
    _classCallCheck(this, ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody);

    ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody.initialize(this, id);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, id) {
      obj['id'] = id;
    }
    /**
     * Constructs a <code>ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody} obj Optional instance to populate.
     * @return {module:model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody} The populated <code>ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody;
}();
/**
 * The ID of customer to which this account will be linked to
 * @member {Number} id
 */


ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody.prototype['id'] = undefined;
var _default = ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody;
exports["default"] = _default;