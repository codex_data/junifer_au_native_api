"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress = _interopRequireDefault(require("./EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts model module.
 * @module model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts
 * @version 1.61.1
 */
var EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts</code>.
   * @alias module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts
   * @param surname {String} Contact's second name
   * @param billingMethod {String} A way for the contact to receive communications. Can be one of the following: `Both, Paper, eBilling`
   * @param primaryContact {Boolean} A boolean flag indicating whether this contact is the primary for this customer
   */
  function EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts(surname, billingMethod, primaryContact) {
    _classCallCheck(this, EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts);

    EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.initialize(this, surname, billingMethod, primaryContact);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts, null, [{
    key: "initialize",
    value: function initialize(obj, surname, billingMethod, primaryContact) {
      obj['surname'] = surname;
      obj['billingMethod'] = billingMethod;
      obj['primaryContact'] = primaryContact;
    }
    /**
     * Constructs a <code>EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts} obj Optional instance to populate.
     * @return {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts} The populated <code>EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts();

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('billingMethod')) {
          obj['billingMethod'] = _ApiClient["default"].convertToType(data['billingMethod'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('phone1')) {
          obj['phone1'] = _ApiClient["default"].convertToType(data['phone1'], 'String');
        }

        if (data.hasOwnProperty('phone2')) {
          obj['phone2'] = _ApiClient["default"].convertToType(data['phone2'], 'String');
        }

        if (data.hasOwnProperty('phone3')) {
          obj['phone3'] = _ApiClient["default"].convertToType(data['phone3'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('primaryContact')) {
          obj['primaryContact'] = _ApiClient["default"].convertToType(data['primaryContact'], 'Boolean');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress["default"].constructFromObject(data['address']);
        }
      }

      return obj;
    }
  }]);

  return EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts;
}();
/**
 * Contact title. Can be `Dr, Miss, Mr, Mrs, Ms, Prof`
 * @member {String} title
 */


EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['title'] = undefined;
/**
 * Contact's first name
 * @member {String} forename
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['forename'] = undefined;
/**
 * Contact's second name
 * @member {String} surname
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['surname'] = undefined;
/**
 * A way for the contact to receive communications. Can be one of the following: `Both, Paper, eBilling`
 * @member {String} billingMethod
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['billingMethod'] = undefined;
/**
 * Contact's email address to which communications will be sent in the case where `billingMethod` is `Both` or `eBilling`
 * @member {String} email
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['email'] = undefined;
/**
 * Contact's primary contact telephone number Can be changed from an optional to mandatory property in `Customer enrolment Properties -> Phone 1 Entry Mandatory on Enrolment`.
 * @member {String} phone1
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['phone1'] = undefined;
/**
 * Contact's second contact telephone number
 * @member {String} phone2
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['phone2'] = undefined;
/**
 * Contact's third contact telephone number
 * @member {String} phone3
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['phone3'] = undefined;
/**
 * Contact's date of birth
 * @member {Date} dateOfBirth
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['dateOfBirth'] = undefined;
/**
 * A boolean flag indicating whether this contact is the primary for this customer
 * @member {Boolean} primaryContact
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['primaryContact'] = undefined;
/**
 * @member {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress} address
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.prototype['address'] = undefined;
var _default = EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts;
exports["default"] = _default;