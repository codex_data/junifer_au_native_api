"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links model module.
 * @module model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links
 * @version 1.61.1
 */
var GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links
   * @param self {String} Link referring to this payment method
   * @param account {String} Link to the account to which this payment method belongs
   */
  function GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links(self, account) {
    _classCallCheck(this, GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links);

    GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links.initialize(this, self, account);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, account) {
      obj['self'] = self;
      obj['account'] = account;
    }
    /**
     * Constructs a <code>GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links} The populated <code>GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('directDebit')) {
          obj['directDebit'] = _ApiClient["default"].convertToType(data['directDebit'], 'String');
        }

        if (data.hasOwnProperty('goCardlessDirectDebit')) {
          obj['goCardlessDirectDebit'] = _ApiClient["default"].convertToType(data['goCardlessDirectDebit'], 'String');
        }

        if (data.hasOwnProperty('stripePaymentCard')) {
          obj['stripePaymentCard'] = _ApiClient["default"].convertToType(data['stripePaymentCard'], 'String');
        }

        if (data.hasOwnProperty('account')) {
          obj['account'] = _ApiClient["default"].convertToType(data['account'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links;
}();
/**
 * Link referring to this payment method
 * @member {String} self
 */


GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links.prototype['self'] = undefined;
/**
 * Link to direct debit if the `paymentMethodType` is \"Direct Debit\"
 * @member {String} directDebit
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links.prototype['directDebit'] = undefined;
/**
 * Link to Go Cardless direct debit if the `paymentMethodType` is \"Go Cardless Direct Debit\"
 * @member {String} goCardlessDirectDebit
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links.prototype['goCardlessDirectDebit'] = undefined;
/**
 * Link to Stripe payment card if the `paymentMethodType` is \"Stripe Payment Card\"
 * @member {String} stripePaymentCard
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links.prototype['stripePaymentCard'] = undefined;
/**
 * Link to the account to which this payment method belongs
 * @member {String} account
 */

GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links.prototype['account'] = undefined;
var _default = GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links;
exports["default"] = _default;