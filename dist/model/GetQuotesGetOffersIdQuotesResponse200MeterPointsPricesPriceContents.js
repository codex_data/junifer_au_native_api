"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents model module.
 * @module model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents
 * @version 1.61.1
 */
var GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents</code>.
   * Class dependent rate information. For instance arecurring charge contains a single rate and rateUnit pair and a supply class contains a rates arraycontaining rate, rateName and rateUnit groups
   * @alias module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents
   */
  function GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents() {
    _classCallCheck(this, GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents);

    GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents} obj Optional instance to populate.
     * @return {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents} The populated <code>GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents();

        if (data.hasOwnProperty('rate')) {
          obj['rate'] = _ApiClient["default"].convertToType(data['rate'], Object);
        }

        if (data.hasOwnProperty('rateName')) {
          obj['rateName'] = _ApiClient["default"].convertToType(data['rateName'], Object);
        }

        if (data.hasOwnProperty('rateUnit')) {
          obj['rateUnit'] = _ApiClient["default"].convertToType(data['rateUnit'], Object);
        }
      }

      return obj;
    }
  }]);

  return GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents;
}();
/**
 * Value of this class dependent rate
 * @member {Object} rate
 */


GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents.prototype['rate'] = undefined;
/**
 * Name of this class dependent rate
 * @member {Object} rateName
 */

GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents.prototype['rateName'] = undefined;
/**
 * Unit of this class dependent rate
 * @member {Object} rateUnit
 */

GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents.prototype['rateUnit'] = undefined;
var _default = GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents;
exports["default"] = _default;