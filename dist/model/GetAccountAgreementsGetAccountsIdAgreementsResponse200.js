"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType = _interopRequireDefault(require("./GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType"));

var _GetAccountAgreementsGetAccountsIdAgreementsResponse200Links = _interopRequireDefault(require("./GetAccountAgreementsGetAccountsIdAgreementsResponse200Links"));

var _GetAccountAgreementsGetAccountsIdAgreementsResponse200Products = _interopRequireDefault(require("./GetAccountAgreementsGetAccountsIdAgreementsResponse200Products"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountAgreementsGetAccountsIdAgreementsResponse200 model module.
 * @module model/GetAccountAgreementsGetAccountsIdAgreementsResponse200
 * @version 1.61.1
 */
var GetAccountAgreementsGetAccountsIdAgreementsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200</code>.
   * @alias module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200
   * @param id {Number} The ID of the agreement
   * @param number {String} Agreement number
   * @param fromDt {Date} The start date of the agreement
   * @param orderedDt {Date} Agreement creation date
   * @param links {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links} 
   * @param products {Array.<module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Products>} Products present in the agreement
   */
  function GetAccountAgreementsGetAccountsIdAgreementsResponse200(id, number, fromDt, orderedDt, links, products) {
    _classCallCheck(this, GetAccountAgreementsGetAccountsIdAgreementsResponse200);

    GetAccountAgreementsGetAccountsIdAgreementsResponse200.initialize(this, id, number, fromDt, orderedDt, links, products);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountAgreementsGetAccountsIdAgreementsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, number, fromDt, orderedDt, links, products) {
      obj['id'] = id;
      obj['number'] = number;
      obj['fromDt'] = fromDt;
      obj['orderedDt'] = orderedDt;
      obj['links'] = links;
      obj['products'] = products;
    }
    /**
     * Constructs a <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200} The populated <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountAgreementsGetAccountsIdAgreementsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('orderedDt')) {
          obj['orderedDt'] = _ApiClient["default"].convertToType(data['orderedDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }

        if (data.hasOwnProperty('cancelled')) {
          obj['cancelled'] = _ApiClient["default"].convertToType(data['cancelled'], 'Boolean');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountAgreementsGetAccountsIdAgreementsResponse200Links["default"].constructFromObject(data['links']);
        }

        if (data.hasOwnProperty('products')) {
          obj['products'] = _ApiClient["default"].convertToType(data['products'], [_GetAccountAgreementsGetAccountsIdAgreementsResponse200Products["default"]]);
        }

        if (data.hasOwnProperty('agreementType')) {
          obj['agreementType'] = _GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType["default"].constructFromObject(data['agreementType']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountAgreementsGetAccountsIdAgreementsResponse200;
}();
/**
 * The ID of the agreement
 * @member {Number} id
 */


GetAccountAgreementsGetAccountsIdAgreementsResponse200.prototype['id'] = undefined;
/**
 * Agreement number
 * @member {String} number
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200.prototype['number'] = undefined;
/**
 * The start date of the agreement
 * @member {Date} fromDt
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200.prototype['fromDt'] = undefined;
/**
 * Agreement creation date
 * @member {Date} orderedDt
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200.prototype['orderedDt'] = undefined;
/**
 * Agreement end date (exclusive)
 * @member {Date} toDt
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200.prototype['toDt'] = undefined;
/**
 * A flag indicating whether this agreement has been cancelled
 * @member {Boolean} cancelled
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200.prototype['cancelled'] = undefined;
/**
 * @member {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links} links
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200.prototype['links'] = undefined;
/**
 * Products present in the agreement
 * @member {Array.<module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Products>} products
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200.prototype['products'] = undefined;
/**
 * @member {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType} agreementType
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200.prototype['agreementType'] = undefined;
var _default = GetAccountAgreementsGetAccountsIdAgreementsResponse200;
exports["default"] = _default;