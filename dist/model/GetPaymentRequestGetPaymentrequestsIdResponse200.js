"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetPaymentRequestGetPaymentrequestsIdResponse200Links = _interopRequireDefault(require("./GetPaymentRequestGetPaymentrequestsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPaymentRequestGetPaymentrequestsIdResponse200 model module.
 * @module model/GetPaymentRequestGetPaymentrequestsIdResponse200
 * @version 1.61.1
 */
var GetPaymentRequestGetPaymentrequestsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPaymentRequestGetPaymentrequestsIdResponse200</code>.
   * @alias module:model/GetPaymentRequestGetPaymentrequestsIdResponse200
   * @param id {Number} Payment request ID
   * @param currency {String} Currency ISO code
   * @param amount {Number} Amount of money to be paid or repaid
   * @param status {String} Current state of the payment
   * @param paymentMethodType {String} Payment method type
   * @param createdDttm {Date} Date and time when the payment request was created
   * @param links {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200Links} 
   */
  function GetPaymentRequestGetPaymentrequestsIdResponse200(id, currency, amount, status, paymentMethodType, createdDttm, links) {
    _classCallCheck(this, GetPaymentRequestGetPaymentrequestsIdResponse200);

    GetPaymentRequestGetPaymentrequestsIdResponse200.initialize(this, id, currency, amount, status, paymentMethodType, createdDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPaymentRequestGetPaymentrequestsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, currency, amount, status, paymentMethodType, createdDttm, links) {
      obj['id'] = id;
      obj['currency'] = currency;
      obj['amount'] = amount;
      obj['status'] = status;
      obj['paymentMethodType'] = paymentMethodType;
      obj['createdDttm'] = createdDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetPaymentRequestGetPaymentrequestsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200} The populated <code>GetPaymentRequestGetPaymentrequestsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPaymentRequestGetPaymentrequestsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('paymentMethodType')) {
          obj['paymentMethodType'] = _ApiClient["default"].convertToType(data['paymentMethodType'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetPaymentRequestGetPaymentrequestsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetPaymentRequestGetPaymentrequestsIdResponse200;
}();
/**
 * Payment request ID
 * @member {Number} id
 */


GetPaymentRequestGetPaymentrequestsIdResponse200.prototype['id'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

GetPaymentRequestGetPaymentrequestsIdResponse200.prototype['currency'] = undefined;
/**
 * Amount of money to be paid or repaid
 * @member {Number} amount
 */

GetPaymentRequestGetPaymentrequestsIdResponse200.prototype['amount'] = undefined;
/**
 * Current state of the payment
 * @member {String} status
 */

GetPaymentRequestGetPaymentrequestsIdResponse200.prototype['status'] = undefined;
/**
 * Payment method type
 * @member {String} paymentMethodType
 */

GetPaymentRequestGetPaymentrequestsIdResponse200.prototype['paymentMethodType'] = undefined;
/**
 * Date and time when the payment request was created
 * @member {Date} createdDttm
 */

GetPaymentRequestGetPaymentrequestsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * Payment description
 * @member {String} description
 */

GetPaymentRequestGetPaymentrequestsIdResponse200.prototype['description'] = undefined;
/**
 * @member {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200Links} links
 */

GetPaymentRequestGetPaymentrequestsIdResponse200.prototype['links'] = undefined;
var _default = GetPaymentRequestGetPaymentrequestsIdResponse200;
exports["default"] = _default;