"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountTicketPostAccountsIdTicketsResponse200Links model module.
 * @module model/CreateAccountTicketPostAccountsIdTicketsResponse200Links
 * @version 1.61.1
 */
var CreateAccountTicketPostAccountsIdTicketsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountTicketPostAccountsIdTicketsResponse200Links</code>.
   * Links to related resources
   * @alias module:model/CreateAccountTicketPostAccountsIdTicketsResponse200Links
   */
  function CreateAccountTicketPostAccountsIdTicketsResponse200Links() {
    _classCallCheck(this, CreateAccountTicketPostAccountsIdTicketsResponse200Links);

    CreateAccountTicketPostAccountsIdTicketsResponse200Links.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountTicketPostAccountsIdTicketsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>CreateAccountTicketPostAccountsIdTicketsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200Links} obj Optional instance to populate.
     * @return {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200Links} The populated <code>CreateAccountTicketPostAccountsIdTicketsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountTicketPostAccountsIdTicketsResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('parentTicket')) {
          obj['parentTicket'] = _ApiClient["default"].convertToType(data['parentTicket'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountTicketPostAccountsIdTicketsResponse200Links;
}();
/**
 * Link referring to this ticket
 * @member {String} self
 */


CreateAccountTicketPostAccountsIdTicketsResponse200Links.prototype['self'] = undefined;
/**
 * Link to the parent ticket of the ticket.
 * @member {String} parentTicket
 */

CreateAccountTicketPostAccountsIdTicketsResponse200Links.prototype['parentTicket'] = undefined;
var _default = CreateAccountTicketPostAccountsIdTicketsResponse200Links;
exports["default"] = _default;