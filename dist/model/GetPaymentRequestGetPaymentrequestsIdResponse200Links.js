"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPaymentRequestGetPaymentrequestsIdResponse200Links model module.
 * @module model/GetPaymentRequestGetPaymentrequestsIdResponse200Links
 * @version 1.61.1
 */
var GetPaymentRequestGetPaymentrequestsIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPaymentRequestGetPaymentrequestsIdResponse200Links</code>.
   * Links to related sources
   * @alias module:model/GetPaymentRequestGetPaymentrequestsIdResponse200Links
   * @param account {String} Link to the account
   */
  function GetPaymentRequestGetPaymentrequestsIdResponse200Links(account) {
    _classCallCheck(this, GetPaymentRequestGetPaymentrequestsIdResponse200Links);

    GetPaymentRequestGetPaymentrequestsIdResponse200Links.initialize(this, account);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPaymentRequestGetPaymentrequestsIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, account) {
      obj['account'] = account;
    }
    /**
     * Constructs a <code>GetPaymentRequestGetPaymentrequestsIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200Links} The populated <code>GetPaymentRequestGetPaymentrequestsIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPaymentRequestGetPaymentrequestsIdResponse200Links();

        if (data.hasOwnProperty('account')) {
          obj['account'] = _ApiClient["default"].convertToType(data['account'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetPaymentRequestGetPaymentrequestsIdResponse200Links;
}();
/**
 * Link to the account
 * @member {String} account
 */


GetPaymentRequestGetPaymentrequestsIdResponse200Links.prototype['account'] = undefined;
var _default = GetPaymentRequestGetPaymentrequestsIdResponse200Links;
exports["default"] = _default;