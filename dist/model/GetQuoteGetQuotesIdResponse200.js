"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetQuoteGetQuotesIdResponse200BrokerMargins = _interopRequireDefault(require("./GetQuoteGetQuotesIdResponse200BrokerMargins"));

var _GetQuoteGetQuotesIdResponse200QuoteFiles = _interopRequireDefault(require("./GetQuoteGetQuotesIdResponse200QuoteFiles"));

var _GetQuotesGetOffersIdQuotesResponse200Links = _interopRequireDefault(require("./GetQuotesGetOffersIdQuotesResponse200Links"));

var _GetQuotesGetOffersIdQuotesResponse200MeterPoints = _interopRequireDefault(require("./GetQuotesGetOffersIdQuotesResponse200MeterPoints"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetQuoteGetQuotesIdResponse200 model module.
 * @module model/GetQuoteGetQuotesIdResponse200
 * @version 1.61.1
 */
var GetQuoteGetQuotesIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetQuoteGetQuotesIdResponse200</code>.
   * @alias module:model/GetQuoteGetQuotesIdResponse200
   * @param id {Number} Quote ID
   * @param number {String} Quote number
   * @param quoteType {String} Quote type
   * @param description {String} Description of the quote
   * @param createdDttm {Date} Date and time when the quote was created
   * @param createdUser {String} User who created the quote
   * @param archived {Boolean} Indicates whether the quote is archived
   * @param contractSpend {Number} Total gross value for this quote
   * @param links {module:model/GetQuotesGetOffersIdQuotesResponse200Links} 
   */
  function GetQuoteGetQuotesIdResponse200(id, number, quoteType, description, createdDttm, createdUser, archived, contractSpend, links) {
    _classCallCheck(this, GetQuoteGetQuotesIdResponse200);

    GetQuoteGetQuotesIdResponse200.initialize(this, id, number, quoteType, description, createdDttm, createdUser, archived, contractSpend, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetQuoteGetQuotesIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, number, quoteType, description, createdDttm, createdUser, archived, contractSpend, links) {
      obj['id'] = id;
      obj['number'] = number;
      obj['quoteType'] = quoteType;
      obj['description'] = description;
      obj['createdDttm'] = createdDttm;
      obj['createdUser'] = createdUser;
      obj['archived'] = archived;
      obj['contractSpend'] = contractSpend;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetQuoteGetQuotesIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetQuoteGetQuotesIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetQuoteGetQuotesIdResponse200} The populated <code>GetQuoteGetQuotesIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetQuoteGetQuotesIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('quoteType')) {
          obj['quoteType'] = _ApiClient["default"].convertToType(data['quoteType'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('createdUser')) {
          obj['createdUser'] = _ApiClient["default"].convertToType(data['createdUser'], 'String');
        }

        if (data.hasOwnProperty('archived')) {
          obj['archived'] = _ApiClient["default"].convertToType(data['archived'], 'Boolean');
        }

        if (data.hasOwnProperty('contractSpend')) {
          obj['contractSpend'] = _ApiClient["default"].convertToType(data['contractSpend'], 'Number');
        }

        if (data.hasOwnProperty('brokerMargins')) {
          obj['brokerMargins'] = _GetQuoteGetQuotesIdResponse200BrokerMargins["default"].constructFromObject(data['brokerMargins']);
        }

        if (data.hasOwnProperty('meterPoints')) {
          obj['meterPoints'] = _GetQuotesGetOffersIdQuotesResponse200MeterPoints["default"].constructFromObject(data['meterPoints']);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetQuotesGetOffersIdQuotesResponse200Links["default"].constructFromObject(data['links']);
        }

        if (data.hasOwnProperty('quoteFiles')) {
          obj['quoteFiles'] = _GetQuoteGetQuotesIdResponse200QuoteFiles["default"].constructFromObject(data['quoteFiles']);
        }
      }

      return obj;
    }
  }]);

  return GetQuoteGetQuotesIdResponse200;
}();
/**
 * Quote ID
 * @member {Number} id
 */


GetQuoteGetQuotesIdResponse200.prototype['id'] = undefined;
/**
 * Quote number
 * @member {String} number
 */

GetQuoteGetQuotesIdResponse200.prototype['number'] = undefined;
/**
 * Quote type
 * @member {String} quoteType
 */

GetQuoteGetQuotesIdResponse200.prototype['quoteType'] = undefined;
/**
 * Description of the quote
 * @member {String} description
 */

GetQuoteGetQuotesIdResponse200.prototype['description'] = undefined;
/**
 * Date and time when the quote was created
 * @member {Date} createdDttm
 */

GetQuoteGetQuotesIdResponse200.prototype['createdDttm'] = undefined;
/**
 * User who created the quote
 * @member {String} createdUser
 */

GetQuoteGetQuotesIdResponse200.prototype['createdUser'] = undefined;
/**
 * Indicates whether the quote is archived
 * @member {Boolean} archived
 */

GetQuoteGetQuotesIdResponse200.prototype['archived'] = undefined;
/**
 * Total gross value for this quote
 * @member {Number} contractSpend
 */

GetQuoteGetQuotesIdResponse200.prototype['contractSpend'] = undefined;
/**
 * @member {module:model/GetQuoteGetQuotesIdResponse200BrokerMargins} brokerMargins
 */

GetQuoteGetQuotesIdResponse200.prototype['brokerMargins'] = undefined;
/**
 * @member {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPoints} meterPoints
 */

GetQuoteGetQuotesIdResponse200.prototype['meterPoints'] = undefined;
/**
 * @member {module:model/GetQuotesGetOffersIdQuotesResponse200Links} links
 */

GetQuoteGetQuotesIdResponse200.prototype['links'] = undefined;
/**
 * @member {module:model/GetQuoteGetQuotesIdResponse200QuoteFiles} quoteFiles
 */

GetQuoteGetQuotesIdResponse200.prototype['quoteFiles'] = undefined;
var _default = GetQuoteGetQuotesIdResponse200;
exports["default"] = _default;