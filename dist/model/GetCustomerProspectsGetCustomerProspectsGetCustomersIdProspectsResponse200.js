"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links = _interopRequireDefault(require("./GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200 model module.
 * @module model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200
 * @version 1.61.1
 */
var GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200</code>.
   * @alias module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200
   * @param id {Number} Prospect ID
   * @param number {String} Prospect number
   * @param links {module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links} 
   */
  function GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200(id, number, links) {
    _classCallCheck(this, GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200);

    GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200.initialize(this, id, number, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, number, links) {
      obj['id'] = id;
      obj['number'] = number;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200} obj Optional instance to populate.
     * @return {module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200} The populated <code>GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('companyName')) {
          obj['companyName'] = _ApiClient["default"].convertToType(data['companyName'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200;
}();
/**
 * Prospect ID
 * @member {Number} id
 */


GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200.prototype['id'] = undefined;
/**
 * Prospect number
 * @member {String} number
 */

GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200.prototype['number'] = undefined;
/**
 * Name of the prospected company
 * @member {String} companyName
 */

GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200.prototype['companyName'] = undefined;
/**
 * @member {module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links} links
 */

GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200.prototype['links'] = undefined;
var _default = GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200;
exports["default"] = _default;