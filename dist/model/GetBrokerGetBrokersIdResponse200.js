"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBrokerGetBrokersIdResponse200Links = _interopRequireDefault(require("./GetBrokerGetBrokersIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBrokerGetBrokersIdResponse200 model module.
 * @module model/GetBrokerGetBrokersIdResponse200
 * @version 1.61.1
 */
var GetBrokerGetBrokersIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBrokerGetBrokersIdResponse200</code>.
   * @alias module:model/GetBrokerGetBrokersIdResponse200
   * @param id {Number} Broker's Id
   * @param code {String} Broker's unique code
   * @param name {String} Broker's Name
   * @param links {module:model/GetBrokerGetBrokersIdResponse200Links} 
   */
  function GetBrokerGetBrokersIdResponse200(id, code, name, links) {
    _classCallCheck(this, GetBrokerGetBrokersIdResponse200);

    GetBrokerGetBrokersIdResponse200.initialize(this, id, code, name, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBrokerGetBrokersIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, code, name, links) {
      obj['id'] = id;
      obj['code'] = code;
      obj['name'] = name;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetBrokerGetBrokersIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBrokerGetBrokersIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetBrokerGetBrokersIdResponse200} The populated <code>GetBrokerGetBrokersIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBrokerGetBrokersIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('code')) {
          obj['code'] = _ApiClient["default"].convertToType(data['code'], 'String');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetBrokerGetBrokersIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetBrokerGetBrokersIdResponse200;
}();
/**
 * Broker's Id
 * @member {Number} id
 */


GetBrokerGetBrokersIdResponse200.prototype['id'] = undefined;
/**
 * Broker's unique code
 * @member {String} code
 */

GetBrokerGetBrokersIdResponse200.prototype['code'] = undefined;
/**
 * Broker's Name
 * @member {String} name
 */

GetBrokerGetBrokersIdResponse200.prototype['name'] = undefined;
/**
 * Broker's email address
 * @member {String} email
 */

GetBrokerGetBrokersIdResponse200.prototype['email'] = undefined;
/**
 * @member {module:model/GetBrokerGetBrokersIdResponse200Links} links
 */

GetBrokerGetBrokersIdResponse200.prototype['links'] = undefined;
var _default = GetBrokerGetBrokersIdResponse200;
exports["default"] = _default;