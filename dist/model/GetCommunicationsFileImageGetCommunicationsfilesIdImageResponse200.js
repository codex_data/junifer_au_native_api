"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200 model module.
 * @module model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200
 * @version 1.61.1
 */
var GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200</code>.
   * @alias module:model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200
   * @param content {String} will be sent back as application/octet-stream
   */
  function GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200(content) {
    _classCallCheck(this, GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200);

    GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200.initialize(this, content);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, content) {
      obj['Content'] = content;
    }
    /**
     * Constructs a <code>GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200} obj Optional instance to populate.
     * @return {module:model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200} The populated <code>GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200();

        if (data.hasOwnProperty('Content')) {
          obj['Content'] = _ApiClient["default"].convertToType(data['Content'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200;
}();
/**
 * will be sent back as application/octet-stream
 * @member {String} Content
 */


GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200.prototype['Content'] = undefined;
var _default = GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200;
exports["default"] = _default;