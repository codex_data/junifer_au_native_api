"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountPropertysGetAccountsIdPropertysResponse200Address = _interopRequireDefault(require("./GetAccountPropertysGetAccountsIdPropertysResponse200Address"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountPropertysGetAccountsIdPropertysResponse200 model module.
 * @module model/GetAccountPropertysGetAccountsIdPropertysResponse200
 * @version 1.61.1
 */
var GetAccountPropertysGetAccountsIdPropertysResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountPropertysGetAccountsIdPropertysResponse200</code>.
   * @alias module:model/GetAccountPropertysGetAccountsIdPropertysResponse200
   * @param id {Number} The ID of the property
   * @param propertyType {String} The type of the property. This will be one of the values in the \"Property Type\" ref table.
   * @param address {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200Address} 
   * @param country {String} Property address country
   * @param countryCode {String} Property address country code
   */
  function GetAccountPropertysGetAccountsIdPropertysResponse200(id, propertyType, address, country, countryCode) {
    _classCallCheck(this, GetAccountPropertysGetAccountsIdPropertysResponse200);

    GetAccountPropertysGetAccountsIdPropertysResponse200.initialize(this, id, propertyType, address, country, countryCode);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountPropertysGetAccountsIdPropertysResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, propertyType, address, country, countryCode) {
      obj['id'] = id;
      obj['propertyType'] = propertyType;
      obj['address'] = address;
      obj['country'] = country;
      obj['countryCode'] = countryCode;
    }
    /**
     * Constructs a <code>GetAccountPropertysGetAccountsIdPropertysResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200} The populated <code>GetAccountPropertysGetAccountsIdPropertysResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountPropertysGetAccountsIdPropertysResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('propertyType')) {
          obj['propertyType'] = _ApiClient["default"].convertToType(data['propertyType'], 'String');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _GetAccountPropertysGetAccountsIdPropertysResponse200Address["default"].constructFromObject(data['address']);
        }

        if (data.hasOwnProperty('country')) {
          obj['country'] = _ApiClient["default"].convertToType(data['country'], 'String');
        }

        if (data.hasOwnProperty('countryCode')) {
          obj['countryCode'] = _ApiClient["default"].convertToType(data['countryCode'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountPropertysGetAccountsIdPropertysResponse200;
}();
/**
 * The ID of the property
 * @member {Number} id
 */


GetAccountPropertysGetAccountsIdPropertysResponse200.prototype['id'] = undefined;
/**
 * The type of the property. This will be one of the values in the \"Property Type\" ref table.
 * @member {String} propertyType
 */

GetAccountPropertysGetAccountsIdPropertysResponse200.prototype['propertyType'] = undefined;
/**
 * @member {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200Address} address
 */

GetAccountPropertysGetAccountsIdPropertysResponse200.prototype['address'] = undefined;
/**
 * Property address country
 * @member {String} country
 */

GetAccountPropertysGetAccountsIdPropertysResponse200.prototype['country'] = undefined;
/**
 * Property address country code
 * @member {String} countryCode
 */

GetAccountPropertysGetAccountsIdPropertysResponse200.prototype['countryCode'] = undefined;
var _default = GetAccountPropertysGetAccountsIdPropertysResponse200;
exports["default"] = _default;