"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBillRequestGetBillrequestsIdResponse200Links = _interopRequireDefault(require("./GetBillRequestGetBillrequestsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillRequestGetBillrequestsIdResponse200 model module.
 * @module model/GetBillRequestGetBillrequestsIdResponse200
 * @version 1.61.1
 */
var GetBillRequestGetBillrequestsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillRequestGetBillrequestsIdResponse200</code>.
   * @alias module:model/GetBillRequestGetBillrequestsIdResponse200
   * @param id {Number} Id of bill request
   * @param billPeriodFromDttm {Date} From date time of bill period linked to bill request
   * @param billPeriodToDttm {Date} To date time of bill period linked to bill request
   * @param statusCode {String} The status code of the bill request
   * @param typeCode {String} The type code of the bill request
   * @param taskCreatedDttm {Date} The created date time of task linked to bill request
   * @param taskUserTblUsername {String} The username of the UserTbl id of the task linked to bill request
   * @param testFl {Boolean} Flag indicating whether the bill request is for a test bill
   * @param links {module:model/GetBillRequestGetBillrequestsIdResponse200Links} 
   */
  function GetBillRequestGetBillrequestsIdResponse200(id, billPeriodFromDttm, billPeriodToDttm, statusCode, typeCode, taskCreatedDttm, taskUserTblUsername, testFl, links) {
    _classCallCheck(this, GetBillRequestGetBillrequestsIdResponse200);

    GetBillRequestGetBillrequestsIdResponse200.initialize(this, id, billPeriodFromDttm, billPeriodToDttm, statusCode, typeCode, taskCreatedDttm, taskUserTblUsername, testFl, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillRequestGetBillrequestsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, billPeriodFromDttm, billPeriodToDttm, statusCode, typeCode, taskCreatedDttm, taskUserTblUsername, testFl, links) {
      obj['id'] = id;
      obj['billPeriodFromDttm'] = billPeriodFromDttm;
      obj['billPeriodToDttm'] = billPeriodToDttm;
      obj['statusCode'] = statusCode;
      obj['typeCode'] = typeCode;
      obj['taskCreatedDttm'] = taskCreatedDttm;
      obj['taskUserTblUsername'] = taskUserTblUsername;
      obj['testFl'] = testFl;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetBillRequestGetBillrequestsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillRequestGetBillrequestsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetBillRequestGetBillrequestsIdResponse200} The populated <code>GetBillRequestGetBillrequestsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillRequestGetBillrequestsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('billId')) {
          obj['billId'] = _ApiClient["default"].convertToType(data['billId'], 'Number');
        }

        if (data.hasOwnProperty('billPeriodFromDttm')) {
          obj['billPeriodFromDttm'] = _ApiClient["default"].convertToType(data['billPeriodFromDttm'], 'Date');
        }

        if (data.hasOwnProperty('billPeriodToDttm')) {
          obj['billPeriodToDttm'] = _ApiClient["default"].convertToType(data['billPeriodToDttm'], 'Date');
        }

        if (data.hasOwnProperty('statusCode')) {
          obj['statusCode'] = _ApiClient["default"].convertToType(data['statusCode'], 'String');
        }

        if (data.hasOwnProperty('typeCode')) {
          obj['typeCode'] = _ApiClient["default"].convertToType(data['typeCode'], 'String');
        }

        if (data.hasOwnProperty('taskCreatedDttm')) {
          obj['taskCreatedDttm'] = _ApiClient["default"].convertToType(data['taskCreatedDttm'], 'Date');
        }

        if (data.hasOwnProperty('taskStartDttm')) {
          obj['taskStartDttm'] = _ApiClient["default"].convertToType(data['taskStartDttm'], 'Date');
        }

        if (data.hasOwnProperty('taskFinishDttm')) {
          obj['taskFinishDttm'] = _ApiClient["default"].convertToType(data['taskFinishDttm'], 'Date');
        }

        if (data.hasOwnProperty('taskUserTblUsername')) {
          obj['taskUserTblUsername'] = _ApiClient["default"].convertToType(data['taskUserTblUsername'], 'String');
        }

        if (data.hasOwnProperty('serverExceptionMessage')) {
          obj['serverExceptionMessage'] = _ApiClient["default"].convertToType(data['serverExceptionMessage'], 'String');
        }

        if (data.hasOwnProperty('testFl')) {
          obj['testFl'] = _ApiClient["default"].convertToType(data['testFl'], 'Boolean');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetBillRequestGetBillrequestsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetBillRequestGetBillrequestsIdResponse200;
}();
/**
 * Id of bill request
 * @member {Number} id
 */


GetBillRequestGetBillrequestsIdResponse200.prototype['id'] = undefined;
/**
 * The id of bill linked to bill request (if the associated bill has been generated)
 * @member {Number} billId
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['billId'] = undefined;
/**
 * From date time of bill period linked to bill request
 * @member {Date} billPeriodFromDttm
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['billPeriodFromDttm'] = undefined;
/**
 * To date time of bill period linked to bill request
 * @member {Date} billPeriodToDttm
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['billPeriodToDttm'] = undefined;
/**
 * The status code of the bill request
 * @member {String} statusCode
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['statusCode'] = undefined;
/**
 * The type code of the bill request
 * @member {String} typeCode
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['typeCode'] = undefined;
/**
 * The created date time of task linked to bill request
 * @member {Date} taskCreatedDttm
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['taskCreatedDttm'] = undefined;
/**
 * The start date time of task linked to bill request
 * @member {Date} taskStartDttm
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['taskStartDttm'] = undefined;
/**
 * The finish date time of task linked to bill request
 * @member {Date} taskFinishDttm
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['taskFinishDttm'] = undefined;
/**
 * The username of the UserTbl id of the task linked to bill request
 * @member {String} taskUserTblUsername
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['taskUserTblUsername'] = undefined;
/**
 * Server exception message of bill request if it failed
 * @member {String} serverExceptionMessage
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['serverExceptionMessage'] = undefined;
/**
 * Flag indicating whether the bill request is for a test bill
 * @member {Boolean} testFl
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['testFl'] = undefined;
/**
 * @member {module:model/GetBillRequestGetBillrequestsIdResponse200Links} links
 */

GetBillRequestGetBillrequestsIdResponse200.prototype['links'] = undefined;
var _default = GetBillRequestGetBillrequestsIdResponse200;
exports["default"] = _default;