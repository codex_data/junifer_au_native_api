"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links = _interopRequireDefault(require("./GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200 model module.
 * @module model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200
 * @version 1.61.1
 */
var CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200</code>.
   * @alias module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200
   * @param id {Number} The ID of the payment schedule period
   * @param fromDt {Date} Date when the payment schedule period becomes active
   * @param frequencyMultiple {Number} Frequency multiplier
   * @param frequency {String} The frequency of payments
   * @param frequencyAlignmentDt {Date} Payment schedule alignment/pivot date
   * @param nextPaymentDt {Date} Date when the next payment is schedule to be collected
   * @param amount {Number} The amount each payment collection will try to collect. Important: If a seasonal definition is present for the payment schedulethen the amount returned will be adjusted accordingly (if the payment at nextPaymentDt is in the high season then the returned amount will be the base amount increased by highPercentage, if the payment is in the low seasonthen the returned amount will be the base amount decreased by lowPercentage).
   * @param seasonalPaymentFl {Boolean} if the payment amount varies by season
   * @param createdDttm {Date} Date and time when this payment schedule period was created
   * @param links {module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links} 
   */
  function CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200(id, fromDt, frequencyMultiple, frequency, frequencyAlignmentDt, nextPaymentDt, amount, seasonalPaymentFl, createdDttm, links) {
    _classCallCheck(this, CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200);

    CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.initialize(this, id, fromDt, frequencyMultiple, frequency, frequencyAlignmentDt, nextPaymentDt, amount, seasonalPaymentFl, createdDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, fromDt, frequencyMultiple, frequency, frequencyAlignmentDt, nextPaymentDt, amount, seasonalPaymentFl, createdDttm, links) {
      obj['id'] = id;
      obj['fromDt'] = fromDt;
      obj['frequencyMultiple'] = frequencyMultiple;
      obj['frequency'] = frequency;
      obj['frequencyAlignmentDt'] = frequencyAlignmentDt;
      obj['nextPaymentDt'] = nextPaymentDt;
      obj['amount'] = amount;
      obj['seasonalPaymentFl'] = seasonalPaymentFl;
      obj['createdDttm'] = createdDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200} obj Optional instance to populate.
     * @return {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200} The populated <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }

        if (data.hasOwnProperty('frequencyMultiple')) {
          obj['frequencyMultiple'] = _ApiClient["default"].convertToType(data['frequencyMultiple'], 'Number');
        }

        if (data.hasOwnProperty('frequency')) {
          obj['frequency'] = _ApiClient["default"].convertToType(data['frequency'], 'String');
        }

        if (data.hasOwnProperty('frequencyAlignmentDt')) {
          obj['frequencyAlignmentDt'] = _ApiClient["default"].convertToType(data['frequencyAlignmentDt'], 'Date');
        }

        if (data.hasOwnProperty('nextPaymentDt')) {
          obj['nextPaymentDt'] = _ApiClient["default"].convertToType(data['nextPaymentDt'], 'Date');
        }

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }

        if (data.hasOwnProperty('seasonalPaymentFl')) {
          obj['seasonalPaymentFl'] = _ApiClient["default"].convertToType(data['seasonalPaymentFl'], 'Boolean');
        }

        if (data.hasOwnProperty('highPercentage')) {
          obj['highPercentage'] = _ApiClient["default"].convertToType(data['highPercentage'], 'Number');
        }

        if (data.hasOwnProperty('lowPercentage')) {
          obj['lowPercentage'] = _ApiClient["default"].convertToType(data['lowPercentage'], 'Number');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200;
}();
/**
 * The ID of the payment schedule period
 * @member {Number} id
 */


CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['id'] = undefined;
/**
 * Date when the payment schedule period becomes active
 * @member {Date} fromDt
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['fromDt'] = undefined;
/**
 * Date when the payment schedule period terminates
 * @member {Date} toDt
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['toDt'] = undefined;
/**
 * Frequency multiplier
 * @member {Number} frequencyMultiple
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['frequencyMultiple'] = undefined;
/**
 * The frequency of payments
 * @member {String} frequency
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['frequency'] = undefined;
/**
 * Payment schedule alignment/pivot date
 * @member {Date} frequencyAlignmentDt
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['frequencyAlignmentDt'] = undefined;
/**
 * Date when the next payment is schedule to be collected
 * @member {Date} nextPaymentDt
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['nextPaymentDt'] = undefined;
/**
 * The amount each payment collection will try to collect. Important: If a seasonal definition is present for the payment schedulethen the amount returned will be adjusted accordingly (if the payment at nextPaymentDt is in the high season then the returned amount will be the base amount increased by highPercentage, if the payment is in the low seasonthen the returned amount will be the base amount decreased by lowPercentage).
 * @member {Number} amount
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['amount'] = undefined;
/**
 * if the payment amount varies by season
 * @member {Boolean} seasonalPaymentFl
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['seasonalPaymentFl'] = undefined;
/**
 * The percentage increase of direct debit amount during the high usage months (specified in the seasonal definition associated with the payment schedule)
 * @member {Number} highPercentage
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['highPercentage'] = undefined;
/**
 * The percentage decrease of direct debit amount during the low usage months (specified in the seasonal definition associated with the payment schedule)
 * @member {Number} lowPercentage
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['lowPercentage'] = undefined;
/**
 * Date and time when this payment schedule period was created
 * @member {Date} createdDttm
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['createdDttm'] = undefined;
/**
 * @member {module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links} links
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.prototype['links'] = undefined;
var _default = CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200;
exports["default"] = _default;