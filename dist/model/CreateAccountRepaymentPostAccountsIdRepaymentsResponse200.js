"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links = _interopRequireDefault(require("./CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountRepaymentPostAccountsIdRepaymentsResponse200 model module.
 * @module model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200
 * @version 1.61.1
 */
var CreateAccountRepaymentPostAccountsIdRepaymentsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountRepaymentPostAccountsIdRepaymentsResponse200</code>.
   * @alias module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200
   * @param id {Number} The id of repayment's payment request
   * @param currency {String} Currency ISO code
   * @param amount {Number} The amount of money to be repaid
   * @param status {String} The state in which the repayment was created. If `Pending` then no further action is needed. If `PendingAuthorisation`, then the amount supplied was higher or lower than the limit for repayments and another user will need to Authorise the repayment before it can proceed
   * @param paymentMethodType {String} The repayment's payment method
   * @param createdDttm {Date} Date and time when the repayment was created
   * @param links {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links} 
   */
  function CreateAccountRepaymentPostAccountsIdRepaymentsResponse200(id, currency, amount, status, paymentMethodType, createdDttm, links) {
    _classCallCheck(this, CreateAccountRepaymentPostAccountsIdRepaymentsResponse200);

    CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.initialize(this, id, currency, amount, status, paymentMethodType, createdDttm, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountRepaymentPostAccountsIdRepaymentsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, currency, amount, status, paymentMethodType, createdDttm, links) {
      obj['id'] = id;
      obj['currency'] = currency;
      obj['amount'] = amount;
      obj['status'] = status;
      obj['paymentMethodType'] = paymentMethodType;
      obj['createdDttm'] = createdDttm;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>CreateAccountRepaymentPostAccountsIdRepaymentsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200} obj Optional instance to populate.
     * @return {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200} The populated <code>CreateAccountRepaymentPostAccountsIdRepaymentsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountRepaymentPostAccountsIdRepaymentsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('paymentMethodType')) {
          obj['paymentMethodType'] = _ApiClient["default"].convertToType(data['paymentMethodType'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return CreateAccountRepaymentPostAccountsIdRepaymentsResponse200;
}();
/**
 * The id of repayment's payment request
 * @member {Number} id
 */


CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.prototype['id'] = undefined;
/**
 * Currency ISO code
 * @member {String} currency
 */

CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.prototype['currency'] = undefined;
/**
 * The amount of money to be repaid
 * @member {Number} amount
 */

CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.prototype['amount'] = undefined;
/**
 * The state in which the repayment was created. If `Pending` then no further action is needed. If `PendingAuthorisation`, then the amount supplied was higher or lower than the limit for repayments and another user will need to Authorise the repayment before it can proceed
 * @member {String} status
 */

CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.prototype['status'] = undefined;
/**
 * The repayment's payment method
 * @member {String} paymentMethodType
 */

CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.prototype['paymentMethodType'] = undefined;
/**
 * Date and time when the repayment was created
 * @member {Date} createdDttm
 */

CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.prototype['createdDttm'] = undefined;
/**
 * Repayment description
 * @member {String} description
 */

CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.prototype['description'] = undefined;
/**
 * @member {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links} links
 */

CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.prototype['links'] = undefined;
var _default = CreateAccountRepaymentPostAccountsIdRepaymentsResponse200;
exports["default"] = _default;