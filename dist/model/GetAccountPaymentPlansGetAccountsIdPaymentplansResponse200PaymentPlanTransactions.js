"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions model module.
 * @module model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions
 * @version 1.61.1
 */
var GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions</code>.
   * @alias module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions
   * @param id {Number} PaymentPlanTransaction ID
   * @param accountTransactionId {Number} Associated Account Transaction ID
   */
  function GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions(id, accountTransactionId) {
    _classCallCheck(this, GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions);

    GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions.initialize(this, id, accountTransactionId);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions, null, [{
    key: "initialize",
    value: function initialize(obj, id, accountTransactionId) {
      obj['id'] = id;
      obj['accountTransactionId'] = accountTransactionId;
    }
    /**
     * Constructs a <code>GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions} obj Optional instance to populate.
     * @return {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions} The populated <code>GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('accountTransactionId')) {
          obj['accountTransactionId'] = _ApiClient["default"].convertToType(data['accountTransactionId'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions;
}();
/**
 * PaymentPlanTransaction ID
 * @member {Number} id
 */


GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions.prototype['id'] = undefined;
/**
 * Associated Account Transaction ID
 * @member {Number} accountTransactionId
 */

GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions.prototype['accountTransactionId'] = undefined;
var _default = GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions;
exports["default"] = _default;