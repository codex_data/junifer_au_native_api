"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans model module.
 * @module model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans
 * @version 1.61.1
 */
var EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans</code>.
   * @alias module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans
   * @param mpanCore {String} MPAN core of the MPAN
   */
  function EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans(mpanCore) {
    _classCallCheck(this, EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans);

    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans.initialize(this, mpanCore);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans, null, [{
    key: "initialize",
    value: function initialize(obj, mpanCore) {
      obj['mpanCore'] = mpanCore;
    }
    /**
     * Constructs a <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans} obj Optional instance to populate.
     * @return {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans} The populated <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans();

        if (data.hasOwnProperty('mpanCore')) {
          obj['mpanCore'] = _ApiClient["default"].convertToType(data['mpanCore'], 'String');
        }

        if (data.hasOwnProperty('measurementType')) {
          obj['measurementType'] = _ApiClient["default"].convertToType(data['measurementType'], 'String');
        }
      }

      return obj;
    }
  }]);

  return EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans;
}();
/**
 * MPAN core of the MPAN
 * @member {String} mpanCore
 */


EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans.prototype['mpanCore'] = undefined;
/**
 * Measurement type of the MPAN. Accepted values `Import, Export`. Defaults to Import(Request Parameter)
 * @member {String} measurementType
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans.prototype['measurementType'] = undefined;
var _default = EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans;
exports["default"] = _default;