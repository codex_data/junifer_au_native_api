"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateProspectPostProspectsRequestBodyCustomerCompanyAddress model module.
 * @module model/CreateProspectPostProspectsRequestBodyCustomerCompanyAddress
 * @version 1.61.1
 */
var CreateProspectPostProspectsRequestBodyCustomerCompanyAddress = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProspectPostProspectsRequestBodyCustomerCompanyAddress</code>.
   * Company registered address
   * @alias module:model/CreateProspectPostProspectsRequestBodyCustomerCompanyAddress
   */
  function CreateProspectPostProspectsRequestBodyCustomerCompanyAddress() {
    _classCallCheck(this, CreateProspectPostProspectsRequestBodyCustomerCompanyAddress);

    CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProspectPostProspectsRequestBodyCustomerCompanyAddress, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>CreateProspectPostProspectsRequestBodyCustomerCompanyAddress</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProspectPostProspectsRequestBodyCustomerCompanyAddress} obj Optional instance to populate.
     * @return {module:model/CreateProspectPostProspectsRequestBodyCustomerCompanyAddress} The populated <code>CreateProspectPostProspectsRequestBodyCustomerCompanyAddress</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProspectPostProspectsRequestBodyCustomerCompanyAddress();

        if (data.hasOwnProperty('addressType')) {
          obj['addressType'] = _ApiClient["default"].convertToType(data['addressType'], 'String');
        }

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('country')) {
          obj['country'] = _ApiClient["default"].convertToType(data['country'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateProspectPostProspectsRequestBodyCustomerCompanyAddress;
}();
/**
 * Company registered address type - defaults to standard address type
 * @member {String} addressType
 */


CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.prototype['addressType'] = undefined;
/**
 * Company registered address line 1
 * @member {String} address1
 */

CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.prototype['address1'] = undefined;
/**
 * Company registered address line 2
 * @member {String} address2
 */

CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.prototype['address2'] = undefined;
/**
 * Company registered address line 3
 * @member {String} address3
 */

CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.prototype['address3'] = undefined;
/**
 * Company registered address line 4
 * @member {String} address4
 */

CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.prototype['address4'] = undefined;
/**
 * Company registered address line 5
 * @member {String} address5
 */

CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.prototype['address5'] = undefined;
/**
 * Company registered address postcode
 * @member {String} postcode
 */

CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.prototype['postcode'] = undefined;
/**
 * Company registered address country - see the Country ref table for valid values
 * @member {String} country
 */

CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.prototype['country'] = undefined;
var _default = CreateProspectPostProspectsRequestBodyCustomerCompanyAddress;
exports["default"] = _default;