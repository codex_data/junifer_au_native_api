"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetOfferGetOffersIdResponse200Links = _interopRequireDefault(require("./GetOfferGetOffersIdResponse200Links"));

var _GetOfferGetOffersIdResponse200MeterPoints = _interopRequireDefault(require("./GetOfferGetOffersIdResponse200MeterPoints"));

var _GetOfferGetOffersIdResponse200PaymentParams = _interopRequireDefault(require("./GetOfferGetOffersIdResponse200PaymentParams"));

var _GetOfferGetOffersIdResponse200Quotes = _interopRequireDefault(require("./GetOfferGetOffersIdResponse200Quotes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetOfferGetOffersIdResponse200 model module.
 * @module model/GetOfferGetOffersIdResponse200
 * @version 1.61.1
 */
var GetOfferGetOffersIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetOfferGetOffersIdResponse200</code>.
   * @alias module:model/GetOfferGetOffersIdResponse200
   * @param id {Number} The offer's id.
   * @param _class {String} The class of the offer. Examples include `Gas`.
   * @param number {String} The offer's number.
   * @param fromDttm {Date} The from date for the offer.
   * @param toDttm {Date} The to date for the offer.
   * @param createdDttm {Date} Offer's creation date and time.
   * @param productBundle {String} The offer's associated product bundle.
   * @param status {String} The offer status. Status codes include `Cancelled, Accepted, Approved, Offered, Priced, Pricing Failed, Pricing, New`.
   * @param createdByUser {String} The user who created the offer.
   * @param quotes {module:model/GetOfferGetOffersIdResponse200Quotes} 
   * @param meterPoints {module:model/GetOfferGetOffersIdResponse200MeterPoints} 
   * @param links {module:model/GetOfferGetOffersIdResponse200Links} 
   */
  function GetOfferGetOffersIdResponse200(id, _class, number, fromDttm, toDttm, createdDttm, productBundle, status, createdByUser, quotes, meterPoints, links) {
    _classCallCheck(this, GetOfferGetOffersIdResponse200);

    GetOfferGetOffersIdResponse200.initialize(this, id, _class, number, fromDttm, toDttm, createdDttm, productBundle, status, createdByUser, quotes, meterPoints, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetOfferGetOffersIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, _class, number, fromDttm, toDttm, createdDttm, productBundle, status, createdByUser, quotes, meterPoints, links) {
      obj['id'] = id;
      obj['class'] = _class;
      obj['number'] = number;
      obj['fromDttm'] = fromDttm;
      obj['toDttm'] = toDttm;
      obj['createdDttm'] = createdDttm;
      obj['productBundle'] = productBundle;
      obj['status'] = status;
      obj['createdByUser'] = createdByUser;
      obj['quotes'] = quotes;
      obj['meterPoints'] = meterPoints;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetOfferGetOffersIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetOfferGetOffersIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetOfferGetOffersIdResponse200} The populated <code>GetOfferGetOffersIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetOfferGetOffersIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('class')) {
          obj['class'] = _ApiClient["default"].convertToType(data['class'], 'String');
        }

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('productBundle')) {
          obj['productBundle'] = _ApiClient["default"].convertToType(data['productBundle'], 'String');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('cancelledByUser')) {
          obj['cancelledByUser'] = _ApiClient["default"].convertToType(data['cancelledByUser'], 'String');
        }

        if (data.hasOwnProperty('cancelledDttm')) {
          obj['cancelledDttm'] = _ApiClient["default"].convertToType(data['cancelledDttm'], 'Date');
        }

        if (data.hasOwnProperty('cancelledReason')) {
          obj['cancelledReason'] = _ApiClient["default"].convertToType(data['cancelledReason'], 'String');
        }

        if (data.hasOwnProperty('acceptedDttm')) {
          obj['acceptedDttm'] = _ApiClient["default"].convertToType(data['acceptedDttm'], 'Date');
        }

        if (data.hasOwnProperty('pricedDttm')) {
          obj['pricedDttm'] = _ApiClient["default"].convertToType(data['pricedDttm'], 'Date');
        }

        if (data.hasOwnProperty('createdByUser')) {
          obj['createdByUser'] = _ApiClient["default"].convertToType(data['createdByUser'], 'String');
        }

        if (data.hasOwnProperty('paymentParams')) {
          obj['paymentParams'] = _GetOfferGetOffersIdResponse200PaymentParams["default"].constructFromObject(data['paymentParams']);
        }

        if (data.hasOwnProperty('quotes')) {
          obj['quotes'] = _GetOfferGetOffersIdResponse200Quotes["default"].constructFromObject(data['quotes']);
        }

        if (data.hasOwnProperty('meterPoints')) {
          obj['meterPoints'] = _GetOfferGetOffersIdResponse200MeterPoints["default"].constructFromObject(data['meterPoints']);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetOfferGetOffersIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetOfferGetOffersIdResponse200;
}();
/**
 * The offer's id.
 * @member {Number} id
 */


GetOfferGetOffersIdResponse200.prototype['id'] = undefined;
/**
 * The class of the offer. Examples include `Gas`.
 * @member {String} class
 */

GetOfferGetOffersIdResponse200.prototype['class'] = undefined;
/**
 * The offer's number.
 * @member {String} number
 */

GetOfferGetOffersIdResponse200.prototype['number'] = undefined;
/**
 * The from date for the offer.
 * @member {Date} fromDttm
 */

GetOfferGetOffersIdResponse200.prototype['fromDttm'] = undefined;
/**
 * The to date for the offer.
 * @member {Date} toDttm
 */

GetOfferGetOffersIdResponse200.prototype['toDttm'] = undefined;
/**
 * Offer's creation date and time.
 * @member {Date} createdDttm
 */

GetOfferGetOffersIdResponse200.prototype['createdDttm'] = undefined;
/**
 * The offer's associated product bundle.
 * @member {String} productBundle
 */

GetOfferGetOffersIdResponse200.prototype['productBundle'] = undefined;
/**
 * The offer status. Status codes include `Cancelled, Accepted, Approved, Offered, Priced, Pricing Failed, Pricing, New`.
 * @member {String} status
 */

GetOfferGetOffersIdResponse200.prototype['status'] = undefined;
/**
 * The user who cancelled the offer. Note: This is only shown if the offer has been cancelled.
 * @member {String} cancelledByUser
 */

GetOfferGetOffersIdResponse200.prototype['cancelledByUser'] = undefined;
/**
 * The date and time in which the offer was cancelled on. Note: This is only shown when the offer has been cancelled.
 * @member {Date} cancelledDttm
 */

GetOfferGetOffersIdResponse200.prototype['cancelledDttm'] = undefined;
/**
 * The reason for cancelling the offer. Note: This is only show when thew offer has been cancelled.
 * @member {String} cancelledReason
 */

GetOfferGetOffersIdResponse200.prototype['cancelledReason'] = undefined;
/**
 * The day and time the offer was accepted at. Note: This is only shown when the offer has been accepted.
 * @member {Date} acceptedDttm
 */

GetOfferGetOffersIdResponse200.prototype['acceptedDttm'] = undefined;
/**
 * The date and time the offer was priced at. Note: This is only shown when the status of the offer is either of the following ` Accepted, Approved, Offered, Priced
 * @member {Date} pricedDttm
 */

GetOfferGetOffersIdResponse200.prototype['pricedDttm'] = undefined;
/**
 * The user who created the offer.
 * @member {String} createdByUser
 */

GetOfferGetOffersIdResponse200.prototype['createdByUser'] = undefined;
/**
 * @member {module:model/GetOfferGetOffersIdResponse200PaymentParams} paymentParams
 */

GetOfferGetOffersIdResponse200.prototype['paymentParams'] = undefined;
/**
 * @member {module:model/GetOfferGetOffersIdResponse200Quotes} quotes
 */

GetOfferGetOffersIdResponse200.prototype['quotes'] = undefined;
/**
 * @member {module:model/GetOfferGetOffersIdResponse200MeterPoints} meterPoints
 */

GetOfferGetOffersIdResponse200.prototype['meterPoints'] = undefined;
/**
 * @member {module:model/GetOfferGetOffersIdResponse200Links} links
 */

GetOfferGetOffersIdResponse200.prototype['links'] = undefined;
var _default = GetOfferGetOffersIdResponse200;
exports["default"] = _default;