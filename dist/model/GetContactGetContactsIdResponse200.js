"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetContactGetContactsIdResponse200Address = _interopRequireDefault(require("./GetContactGetContactsIdResponse200Address"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetContactGetContactsIdResponse200 model module.
 * @module model/GetContactGetContactsIdResponse200
 * @version 1.61.1
 */
var GetContactGetContactsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetContactGetContactsIdResponse200</code>.
   * @alias module:model/GetContactGetContactsIdResponse200
   * @param id {Number} Contact Id
   * @param contactType {String} Contact Type
   * @param surname {String} Contact surname
   * @param email {String} Contact email
   */
  function GetContactGetContactsIdResponse200(id, contactType, surname, email) {
    _classCallCheck(this, GetContactGetContactsIdResponse200);

    GetContactGetContactsIdResponse200.initialize(this, id, contactType, surname, email);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetContactGetContactsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, contactType, surname, email) {
      obj['id'] = id;
      obj['contactType'] = contactType;
      obj['surname'] = surname;
      obj['email'] = email;
    }
    /**
     * Constructs a <code>GetContactGetContactsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetContactGetContactsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetContactGetContactsIdResponse200} The populated <code>GetContactGetContactsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetContactGetContactsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('contactType')) {
          obj['contactType'] = _ApiClient["default"].convertToType(data['contactType'], 'String');
        }

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('initials')) {
          obj['initials'] = _ApiClient["default"].convertToType(data['initials'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('jobTitle')) {
          obj['jobTitle'] = _ApiClient["default"].convertToType(data['jobTitle'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('phoneNumber1')) {
          obj['phoneNumber1'] = _ApiClient["default"].convertToType(data['phoneNumber1'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber2')) {
          obj['phoneNumber2'] = _ApiClient["default"].convertToType(data['phoneNumber2'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber3')) {
          obj['phoneNumber3'] = _ApiClient["default"].convertToType(data['phoneNumber3'], 'String');
        }

        if (data.hasOwnProperty('socialMedia1')) {
          obj['socialMedia1'] = _ApiClient["default"].convertToType(data['socialMedia1'], 'String');
        }

        if (data.hasOwnProperty('socialMedia2')) {
          obj['socialMedia2'] = _ApiClient["default"].convertToType(data['socialMedia2'], 'String');
        }

        if (data.hasOwnProperty('socialMedia3')) {
          obj['socialMedia3'] = _ApiClient["default"].convertToType(data['socialMedia3'], 'String');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _GetContactGetContactsIdResponse200Address["default"].constructFromObject(data['address']);
        }
      }

      return obj;
    }
  }]);

  return GetContactGetContactsIdResponse200;
}();
/**
 * Contact Id
 * @member {Number} id
 */


GetContactGetContactsIdResponse200.prototype['id'] = undefined;
/**
 * Contact Type
 * @member {String} contactType
 */

GetContactGetContactsIdResponse200.prototype['contactType'] = undefined;
/**
 * Contact title
 * @member {String} title
 */

GetContactGetContactsIdResponse200.prototype['title'] = undefined;
/**
 * Contact forename
 * @member {String} forename
 */

GetContactGetContactsIdResponse200.prototype['forename'] = undefined;
/**
 * Contact initials
 * @member {String} initials
 */

GetContactGetContactsIdResponse200.prototype['initials'] = undefined;
/**
 * Contact surname
 * @member {String} surname
 */

GetContactGetContactsIdResponse200.prototype['surname'] = undefined;
/**
 * Contact job title
 * @member {String} jobTitle
 */

GetContactGetContactsIdResponse200.prototype['jobTitle'] = undefined;
/**
 * Contact email
 * @member {String} email
 */

GetContactGetContactsIdResponse200.prototype['email'] = undefined;
/**
 * Contact's date of birth
 * @member {Date} dateOfBirth
 */

GetContactGetContactsIdResponse200.prototype['dateOfBirth'] = undefined;
/**
 * Primary contact phone number
 * @member {String} phoneNumber1
 */

GetContactGetContactsIdResponse200.prototype['phoneNumber1'] = undefined;
/**
 * Secondary contact phone number
 * @member {String} phoneNumber2
 */

GetContactGetContactsIdResponse200.prototype['phoneNumber2'] = undefined;
/**
 * Third contact phone number
 * @member {String} phoneNumber3
 */

GetContactGetContactsIdResponse200.prototype['phoneNumber3'] = undefined;
/**
 * Contact social media type 1
 * @member {String} socialMedia1
 */

GetContactGetContactsIdResponse200.prototype['socialMedia1'] = undefined;
/**
 * Contact social media type 2
 * @member {String} socialMedia2
 */

GetContactGetContactsIdResponse200.prototype['socialMedia2'] = undefined;
/**
 * Contact social media type 3
 * @member {String} socialMedia3
 */

GetContactGetContactsIdResponse200.prototype['socialMedia3'] = undefined;
/**
 * @member {module:model/GetContactGetContactsIdResponse200Address} address
 */

GetContactGetContactsIdResponse200.prototype['address'] = undefined;
var _default = GetContactGetContactsIdResponse200;
exports["default"] = _default;