"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateTicketPutTicketsIdRequestBody model module.
 * @module model/UpdateTicketPutTicketsIdRequestBody
 * @version 1.61.1
 */
var UpdateTicketPutTicketsIdRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateTicketPutTicketsIdRequestBody</code>.
   * @alias module:model/UpdateTicketPutTicketsIdRequestBody
   * @param ticketStepCode {String} Ticket step code to move ticket to
   * @param summary {String} Brief summary for the ticket (overwrites existing Summary)
   * @param description {String} Ticket description
   */
  function UpdateTicketPutTicketsIdRequestBody(ticketStepCode, summary, description) {
    _classCallCheck(this, UpdateTicketPutTicketsIdRequestBody);

    UpdateTicketPutTicketsIdRequestBody.initialize(this, ticketStepCode, summary, description);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateTicketPutTicketsIdRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, ticketStepCode, summary, description) {
      obj['ticketStepCode'] = ticketStepCode;
      obj['summary'] = summary;
      obj['description'] = description;
    }
    /**
     * Constructs a <code>UpdateTicketPutTicketsIdRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateTicketPutTicketsIdRequestBody} obj Optional instance to populate.
     * @return {module:model/UpdateTicketPutTicketsIdRequestBody} The populated <code>UpdateTicketPutTicketsIdRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateTicketPutTicketsIdRequestBody();

        if (data.hasOwnProperty('ticketStepCode')) {
          obj['ticketStepCode'] = _ApiClient["default"].convertToType(data['ticketStepCode'], 'String');
        }

        if (data.hasOwnProperty('summary')) {
          obj['summary'] = _ApiClient["default"].convertToType(data['summary'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('parentTicketId')) {
          obj['parentTicketId'] = _ApiClient["default"].convertToType(data['parentTicketId'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return UpdateTicketPutTicketsIdRequestBody;
}();
/**
 * Ticket step code to move ticket to
 * @member {String} ticketStepCode
 */


UpdateTicketPutTicketsIdRequestBody.prototype['ticketStepCode'] = undefined;
/**
 * Brief summary for the ticket (overwrites existing Summary)
 * @member {String} summary
 */

UpdateTicketPutTicketsIdRequestBody.prototype['summary'] = undefined;
/**
 * Ticket description
 * @member {String} description
 */

UpdateTicketPutTicketsIdRequestBody.prototype['description'] = undefined;
/**
 * Optionally associate a ticket with a parent
 * @member {Number} parentTicketId
 */

UpdateTicketPutTicketsIdRequestBody.prototype['parentTicketId'] = undefined;
var _default = UpdateTicketPutTicketsIdRequestBody;
exports["default"] = _default;