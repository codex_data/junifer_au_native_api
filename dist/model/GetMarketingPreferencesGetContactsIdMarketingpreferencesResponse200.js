"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail = _interopRequireDefault(require("./GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber = _interopRequireDefault(require("./GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2 = _interopRequireDefault(require("./GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3 = _interopRequireDefault(require("./GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost = _interopRequireDefault(require("./GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms = _interopRequireDefault(require("./GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia = _interopRequireDefault(require("./GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2 = _interopRequireDefault(require("./GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2"));

var _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3 = _interopRequireDefault(require("./GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200 model module.
 * @module model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200
 * @version 1.61.1
 */
var GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200</code>.
   * @alias module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200
   * @param marketingMaterials {Array.<Object>} Array of marketing material string names (see MarketingMaterial ref table for possible values)
   */
  function GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200(marketingMaterials) {
    _classCallCheck(this, GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200);

    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.initialize(this, marketingMaterials);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, marketingMaterials) {
      obj['marketingMaterials'] = marketingMaterials;
    }
    /**
     * Constructs a <code>GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200} obj Optional instance to populate.
     * @return {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200} The populated <code>GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200();

        if (data.hasOwnProperty('marketingMaterials')) {
          obj['marketingMaterials'] = _ApiClient["default"].convertToType(data['marketingMaterials'], [Object]);
        }

        if (data.hasOwnProperty('marketByEmail')) {
          obj['marketByEmail'] = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail["default"].constructFromObject(data['marketByEmail']);
        }

        if (data.hasOwnProperty('marketByNumber1')) {
          obj['marketByNumber1'] = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber["default"].constructFromObject(data['marketByNumber1']);
        }

        if (data.hasOwnProperty('marketByNumber2')) {
          obj['marketByNumber2'] = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2["default"].constructFromObject(data['marketByNumber2']);
        }

        if (data.hasOwnProperty('marketByNumber3')) {
          obj['marketByNumber3'] = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3["default"].constructFromObject(data['marketByNumber3']);
        }

        if (data.hasOwnProperty('marketBySms')) {
          obj['marketBySms'] = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms["default"].constructFromObject(data['marketBySms']);
        }

        if (data.hasOwnProperty('marketByPost')) {
          obj['marketByPost'] = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost["default"].constructFromObject(data['marketByPost']);
        }

        if (data.hasOwnProperty('marketBySocialMedia1')) {
          obj['marketBySocialMedia1'] = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia["default"].constructFromObject(data['marketBySocialMedia1']);
        }

        if (data.hasOwnProperty('marketBySocialMedia2')) {
          obj['marketBySocialMedia2'] = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2["default"].constructFromObject(data['marketBySocialMedia2']);
        }

        if (data.hasOwnProperty('marketBySocialMedia3')) {
          obj['marketBySocialMedia3'] = _GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3["default"].constructFromObject(data['marketBySocialMedia3']);
        }
      }

      return obj;
    }
  }]);

  return GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200;
}();
/**
 * Array of marketing material string names (see MarketingMaterial ref table for possible values)
 * @member {Array.<Object>} marketingMaterials
 */


GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketingMaterials'] = undefined;
/**
 * @member {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail} marketByEmail
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketByEmail'] = undefined;
/**
 * @member {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1} marketByNumber1
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketByNumber1'] = undefined;
/**
 * @member {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2} marketByNumber2
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketByNumber2'] = undefined;
/**
 * @member {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3} marketByNumber3
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketByNumber3'] = undefined;
/**
 * @member {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms} marketBySms
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketBySms'] = undefined;
/**
 * @member {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost} marketByPost
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketByPost'] = undefined;
/**
 * @member {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1} marketBySocialMedia1
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketBySocialMedia1'] = undefined;
/**
 * @member {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2} marketBySocialMedia2
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketBySocialMedia2'] = undefined;
/**
 * @member {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3} marketBySocialMedia3
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.prototype['marketBySocialMedia3'] = undefined;
var _default = GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200;
exports["default"] = _default;