"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400 model module.
 * @module model/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400
 * @version 1.61.1
 */
var SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400</code>.
   * @alias module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400
   * @param errorCode {module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400.ErrorCodeEnum} Field: * `BadRequest` - Invalid reading validation errors have been found * `InternalError` - Unknown error while processing readingDttm 'readingsDttm' * `InvalidQuality` - The specified quality x is not valid * `InvalidSource` - The specified source x is not valid * `MeterNotAttachedToMeterPoint` - The meter x is not attached to the meter point at date/time y * `MeterMissingEACAQ` - The meter x does not have y for all its registers at date/time z * `MeterMissingEUC` - The meterpoint x does not have an EUC at date/time y * `ReadingMismatch` - EventDttm, Sequence Type, Source and Quality must match for each register under a meter * `MeterHasNoRegisters` - The meter x has no registers configured * `RequiredRegisterIdentifierMissing` - The meter x has multiple registers but no register identifier was specified * `InvalidRegisterIdentifier` - The meter x does not have a register with identifier y * `RegisterNotConnectedToMeterPoint` - The meter is connected to the meter point but the specified register is not connected to the meter point at the date/time x
   * @param errorSeverity {String} The error severity
   */
  function SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400(errorCode, errorSeverity) {
    _classCallCheck(this, SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400);

    SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400} obj Optional instance to populate.
     * @return {module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400} The populated <code>SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400;
}();
/**
 * Field: * `BadRequest` - Invalid reading validation errors have been found * `InternalError` - Unknown error while processing readingDttm 'readingsDttm' * `InvalidQuality` - The specified quality x is not valid * `InvalidSource` - The specified source x is not valid * `MeterNotAttachedToMeterPoint` - The meter x is not attached to the meter point at date/time y * `MeterMissingEACAQ` - The meter x does not have y for all its registers at date/time z * `MeterMissingEUC` - The meterpoint x does not have an EUC at date/time y * `ReadingMismatch` - EventDttm, Sequence Type, Source and Quality must match for each register under a meter * `MeterHasNoRegisters` - The meter x has no registers configured * `RequiredRegisterIdentifierMissing` - The meter x has multiple registers but no register identifier was specified * `InvalidRegisterIdentifier` - The meter x does not have a register with identifier y * `RegisterNotConnectedToMeterPoint` - The meter is connected to the meter point but the specified register is not connected to the meter point at the date/time x
 * @member {module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400.ErrorCodeEnum} errorCode
 */


SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400['ErrorCodeEnum'] = {
  /**
   * value: "BadRequest"
   * @const
   */
  "BadRequest": "BadRequest",

  /**
   * value: "InternalError"
   * @const
   */
  "InternalError": "InternalError",

  /**
   * value: "InvalidQuality"
   * @const
   */
  "InvalidQuality": "InvalidQuality",

  /**
   * value: "InvalidSource"
   * @const
   */
  "InvalidSource": "InvalidSource",

  /**
   * value: "MeterNotAttachedToMeterPoint"
   * @const
   */
  "MeterNotAttachedToMeterPoint": "MeterNotAttachedToMeterPoint",

  /**
   * value: "MeterMissingEACAQ"
   * @const
   */
  "MeterMissingEACAQ": "MeterMissingEACAQ",

  /**
   * value: "MeterMissingEUC"
   * @const
   */
  "MeterMissingEUC": "MeterMissingEUC",

  /**
   * value: "ReadingMismatch"
   * @const
   */
  "ReadingMismatch": "ReadingMismatch",

  /**
   * value: "MeterHasNoRegisters"
   * @const
   */
  "MeterHasNoRegisters": "MeterHasNoRegisters",

  /**
   * value: "RequiredRegisterIdentifierMissing"
   * @const
   */
  "RequiredRegisterIdentifierMissing": "RequiredRegisterIdentifierMissing",

  /**
   * value: "InvalidRegisterIdentifier"
   * @const
   */
  "InvalidRegisterIdentifier": "InvalidRegisterIdentifier",

  /**
   * value: "RegisterNotConnectedToMeterPoint"
   * @const
   */
  "RegisterNotConnectedToMeterPoint": "RegisterNotConnectedToMeterPoint"
};
var _default = SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400;
exports["default"] = _default;