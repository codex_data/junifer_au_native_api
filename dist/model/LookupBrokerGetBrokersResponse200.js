"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _LookupBrokerGetBrokersResponse200Links = _interopRequireDefault(require("./LookupBrokerGetBrokersResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The LookupBrokerGetBrokersResponse200 model module.
 * @module model/LookupBrokerGetBrokersResponse200
 * @version 1.61.1
 */
var LookupBrokerGetBrokersResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>LookupBrokerGetBrokersResponse200</code>.
   * @alias module:model/LookupBrokerGetBrokersResponse200
   * @param id {Number} Broker's Id
   * @param code {String} Broker's unique code
   * @param name {String} Broker's Name
   * @param links {module:model/LookupBrokerGetBrokersResponse200Links} 
   */
  function LookupBrokerGetBrokersResponse200(id, code, name, links) {
    _classCallCheck(this, LookupBrokerGetBrokersResponse200);

    LookupBrokerGetBrokersResponse200.initialize(this, id, code, name, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(LookupBrokerGetBrokersResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, code, name, links) {
      obj['id'] = id;
      obj['code'] = code;
      obj['name'] = name;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>LookupBrokerGetBrokersResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LookupBrokerGetBrokersResponse200} obj Optional instance to populate.
     * @return {module:model/LookupBrokerGetBrokersResponse200} The populated <code>LookupBrokerGetBrokersResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new LookupBrokerGetBrokersResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('code')) {
          obj['code'] = _ApiClient["default"].convertToType(data['code'], 'String');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _LookupBrokerGetBrokersResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return LookupBrokerGetBrokersResponse200;
}();
/**
 * Broker's Id
 * @member {Number} id
 */


LookupBrokerGetBrokersResponse200.prototype['id'] = undefined;
/**
 * Broker's unique code
 * @member {String} code
 */

LookupBrokerGetBrokersResponse200.prototype['code'] = undefined;
/**
 * Broker's Name
 * @member {String} name
 */

LookupBrokerGetBrokersResponse200.prototype['name'] = undefined;
/**
 * Broker's email address
 * @member {String} email
 */

LookupBrokerGetBrokersResponse200.prototype['email'] = undefined;
/**
 * @member {module:model/LookupBrokerGetBrokersResponse200Links} links
 */

LookupBrokerGetBrokersResponse200.prototype['links'] = undefined;
var _default = LookupBrokerGetBrokersResponse200;
exports["default"] = _default;