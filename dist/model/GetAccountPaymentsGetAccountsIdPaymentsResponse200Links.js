"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountPaymentsGetAccountsIdPaymentsResponse200Links model module.
 * @module model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links
 * @version 1.61.1
 */
var GetAccountPaymentsGetAccountsIdPaymentsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountPaymentsGetAccountsIdPaymentsResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links
   * @param self {String} Link referring to this payment
   * @param account {String} Link to the account to which the payment is linked to
   */
  function GetAccountPaymentsGetAccountsIdPaymentsResponse200Links(self, account) {
    _classCallCheck(this, GetAccountPaymentsGetAccountsIdPaymentsResponse200Links);

    GetAccountPaymentsGetAccountsIdPaymentsResponse200Links.initialize(this, self, account);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountPaymentsGetAccountsIdPaymentsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, account) {
      obj['self'] = self;
      obj['account'] = account;
    }
    /**
     * Constructs a <code>GetAccountPaymentsGetAccountsIdPaymentsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links} The populated <code>GetAccountPaymentsGetAccountsIdPaymentsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountPaymentsGetAccountsIdPaymentsResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('account')) {
          obj['account'] = _ApiClient["default"].convertToType(data['account'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountPaymentsGetAccountsIdPaymentsResponse200Links;
}();
/**
 * Link referring to this payment
 * @member {String} self
 */


GetAccountPaymentsGetAccountsIdPaymentsResponse200Links.prototype['self'] = undefined;
/**
 * Link to the account to which the payment is linked to
 * @member {String} account
 */

GetAccountPaymentsGetAccountsIdPaymentsResponse200Links.prototype['account'] = undefined;
var _default = GetAccountPaymentsGetAccountsIdPaymentsResponse200Links;
exports["default"] = _default;