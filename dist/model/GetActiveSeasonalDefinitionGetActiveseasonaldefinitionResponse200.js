"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months = _interopRequireDefault(require("./GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200 model module.
 * @module model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200
 * @version 1.61.1
 */
var GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200</code>.
   * @alias module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200
   * @param lowPercentage {Number} The percentage decrease of direct debit amount during the low usage months
   * @param highPercentage {Number} The percentage increase of direct debit amount during the high usage months
   * @param createdDttm {Date} Date when the seasonal definition was created
   * @param activeDttm {Date} Date when the seasonal definition became the active definition
   * @param months {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months} 
   */
  function GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200(lowPercentage, highPercentage, createdDttm, activeDttm, months) {
    _classCallCheck(this, GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200);

    GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200.initialize(this, lowPercentage, highPercentage, createdDttm, activeDttm, months);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, lowPercentage, highPercentage, createdDttm, activeDttm, months) {
      obj['lowPercentage'] = lowPercentage;
      obj['highPercentage'] = highPercentage;
      obj['createdDttm'] = createdDttm;
      obj['activeDttm'] = activeDttm;
      obj['months'] = months;
    }
    /**
     * Constructs a <code>GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200} obj Optional instance to populate.
     * @return {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200} The populated <code>GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200();

        if (data.hasOwnProperty('lowPercentage')) {
          obj['lowPercentage'] = _ApiClient["default"].convertToType(data['lowPercentage'], 'Number');
        }

        if (data.hasOwnProperty('highPercentage')) {
          obj['highPercentage'] = _ApiClient["default"].convertToType(data['highPercentage'], 'Number');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('activeDttm')) {
          obj['activeDttm'] = _ApiClient["default"].convertToType(data['activeDttm'], 'Date');
        }

        if (data.hasOwnProperty('months')) {
          obj['months'] = _GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months["default"].constructFromObject(data['months']);
        }
      }

      return obj;
    }
  }]);

  return GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200;
}();
/**
 * The percentage decrease of direct debit amount during the low usage months
 * @member {Number} lowPercentage
 */


GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200.prototype['lowPercentage'] = undefined;
/**
 * The percentage increase of direct debit amount during the high usage months
 * @member {Number} highPercentage
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200.prototype['highPercentage'] = undefined;
/**
 * Date when the seasonal definition was created
 * @member {Date} createdDttm
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200.prototype['createdDttm'] = undefined;
/**
 * Date when the seasonal definition became the active definition
 * @member {Date} activeDttm
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200.prototype['activeDttm'] = undefined;
/**
 * @member {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months} months
 */

GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200.prototype['months'] = undefined;
var _default = GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200;
exports["default"] = _default;