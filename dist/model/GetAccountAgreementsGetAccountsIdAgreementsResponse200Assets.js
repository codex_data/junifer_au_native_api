"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountAgreementsGetAccountsIdAgreementsResponse200Links = _interopRequireDefault(require("./GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets model module.
 * @module model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets
 * @version 1.61.1
 */
var GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets</code>.
   * @alias module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets
   * @param id {Number} The ID of the asset
   * @param links {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1} 
   */
  function GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets(id, links) {
    _classCallCheck(this, GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets);

    GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets.initialize(this, id, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets, null, [{
    key: "initialize",
    value: function initialize(obj, id, links) {
      obj['id'] = id;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets} obj Optional instance to populate.
     * @return {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets} The populated <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountAgreementsGetAccountsIdAgreementsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets;
}();
/**
 * The ID of the asset
 * @member {Number} id
 */


GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets.prototype['id'] = undefined;
/**
 * @member {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1} links
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets.prototype['links'] = undefined;
var _default = GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets;
exports["default"] = _default;