"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountDebitPostAccountsIdAccountdebitsRequestBody model module.
 * @module model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody
 * @version 1.61.1
 */
var CreateAccountDebitPostAccountsIdAccountdebitsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountDebitPostAccountsIdAccountdebitsRequestBody</code>.
   * @alias module:model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody
   * @param grossAmount {Number} Gross amount of account debit
   * @param salesTaxName {String} Name of linked sales tax of account debit. Providing `\"None\"` will apply no sales tax to the account debit.
   * @param accountDebitReasonName {String} Name of linked account debit reason for account debit
   */
  function CreateAccountDebitPostAccountsIdAccountdebitsRequestBody(grossAmount, salesTaxName, accountDebitReasonName) {
    _classCallCheck(this, CreateAccountDebitPostAccountsIdAccountdebitsRequestBody);

    CreateAccountDebitPostAccountsIdAccountdebitsRequestBody.initialize(this, grossAmount, salesTaxName, accountDebitReasonName);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountDebitPostAccountsIdAccountdebitsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, grossAmount, salesTaxName, accountDebitReasonName) {
      obj['grossAmount'] = grossAmount;
      obj['salesTaxName'] = salesTaxName;
      obj['accountDebitReasonName'] = accountDebitReasonName;
    }
    /**
     * Constructs a <code>CreateAccountDebitPostAccountsIdAccountdebitsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody} obj Optional instance to populate.
     * @return {module:model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody} The populated <code>CreateAccountDebitPostAccountsIdAccountdebitsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountDebitPostAccountsIdAccountdebitsRequestBody();

        if (data.hasOwnProperty('grossAmount')) {
          obj['grossAmount'] = _ApiClient["default"].convertToType(data['grossAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTaxName')) {
          obj['salesTaxName'] = _ApiClient["default"].convertToType(data['salesTaxName'], 'String');
        }

        if (data.hasOwnProperty('accountDebitReasonName')) {
          obj['accountDebitReasonName'] = _ApiClient["default"].convertToType(data['accountDebitReasonName'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountDebitPostAccountsIdAccountdebitsRequestBody;
}();
/**
 * Gross amount of account debit
 * @member {Number} grossAmount
 */


CreateAccountDebitPostAccountsIdAccountdebitsRequestBody.prototype['grossAmount'] = undefined;
/**
 * Name of linked sales tax of account debit. Providing `\"None\"` will apply no sales tax to the account debit.
 * @member {String} salesTaxName
 */

CreateAccountDebitPostAccountsIdAccountdebitsRequestBody.prototype['salesTaxName'] = undefined;
/**
 * Name of linked account debit reason for account debit
 * @member {String} accountDebitReasonName
 */

CreateAccountDebitPostAccountsIdAccountdebitsRequestBody.prototype['accountDebitReasonName'] = undefined;
/**
 * Text identifier for the account debit
 * @member {String} reference
 */

CreateAccountDebitPostAccountsIdAccountdebitsRequestBody.prototype['reference'] = undefined;
/**
 * Description of the account transaction linked to the account debit
 * @member {String} description
 */

CreateAccountDebitPostAccountsIdAccountdebitsRequestBody.prototype['description'] = undefined;
var _default = CreateAccountDebitPostAccountsIdAccountdebitsRequestBody;
exports["default"] = _default;