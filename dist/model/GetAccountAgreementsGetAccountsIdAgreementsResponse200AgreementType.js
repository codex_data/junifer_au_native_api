"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType model module.
 * @module model/GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType
 * @version 1.61.1
 */
var GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType</code>.
   * The agreement type of the agreement
   * @alias module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType
   */
  function GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType() {
    _classCallCheck(this, GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType);

    GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType} obj Optional instance to populate.
     * @return {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType} The populated <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType();

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('duration')) {
          obj['duration'] = _ApiClient["default"].convertToType(data['duration'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType;
}();
/**
 * name of the agreement type
 * @member {String} name
 */


GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType.prototype['name'] = undefined;
/**
 * the duration of the agreement
 * @member {Number} duration
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType.prototype['duration'] = undefined;
var _default = GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType;
exports["default"] = _default;