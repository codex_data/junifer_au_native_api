"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The TariffInformationGetAccountsIdTariffinformationResponse200TIL model module.
 * @module model/TariffInformationGetAccountsIdTariffinformationResponse200TIL
 * @version 1.61.1
 */
var TariffInformationGetAccountsIdTariffinformationResponse200TIL = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>TariffInformationGetAccountsIdTariffinformationResponse200TIL</code>.
   * @alias module:model/TariffInformationGetAccountsIdTariffinformationResponse200TIL
   */
  function TariffInformationGetAccountsIdTariffinformationResponse200TIL() {
    _classCallCheck(this, TariffInformationGetAccountsIdTariffinformationResponse200TIL);

    TariffInformationGetAccountsIdTariffinformationResponse200TIL.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(TariffInformationGetAccountsIdTariffinformationResponse200TIL, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>TariffInformationGetAccountsIdTariffinformationResponse200TIL</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/TariffInformationGetAccountsIdTariffinformationResponse200TIL} obj Optional instance to populate.
     * @return {module:model/TariffInformationGetAccountsIdTariffinformationResponse200TIL} The populated <code>TariffInformationGetAccountsIdTariffinformationResponse200TIL</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new TariffInformationGetAccountsIdTariffinformationResponse200TIL();

        if (data.hasOwnProperty('itemName')) {
          obj['itemName'] = _ApiClient["default"].convertToType(data['itemName'], 'String');
        }

        if (data.hasOwnProperty('itemValue')) {
          obj['itemValue'] = _ApiClient["default"].convertToType(data['itemValue'], 'String');
        }

        if (data.hasOwnProperty('exclVAT')) {
          obj['exclVAT'] = _ApiClient["default"].convertToType(data['exclVAT'], 'String');
        }

        if (data.hasOwnProperty('inclVAT')) {
          obj['inclVAT'] = _ApiClient["default"].convertToType(data['inclVAT'], 'String');
        }
      }

      return obj;
    }
  }]);

  return TariffInformationGetAccountsIdTariffinformationResponse200TIL;
}();
/**
 * The TIL's item name eg \"Unit Rate\"
 * @member {String} itemName
 */


TariffInformationGetAccountsIdTariffinformationResponse200TIL.prototype['itemName'] = undefined;
/**
 * The TIL's item value eg \"Fixed Rate\"
 * @member {String} itemValue
 */

TariffInformationGetAccountsIdTariffinformationResponse200TIL.prototype['itemValue'] = undefined;
/**
 * The price value excluding VAT.
 * @member {String} exclVAT
 */

TariffInformationGetAccountsIdTariffinformationResponse200TIL.prototype['exclVAT'] = undefined;
/**
 * The price value including VAT.
 * @member {String} inclVAT
 */

TariffInformationGetAccountsIdTariffinformationResponse200TIL.prototype['inclVAT'] = undefined;
var _default = TariffInformationGetAccountsIdTariffinformationResponse200TIL;
exports["default"] = _default;