"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateProspectPutCustomersIdCreateprospectRequestBody model module.
 * @module model/CreateProspectPutCustomersIdCreateprospectRequestBody
 * @version 1.61.1
 */
var CreateProspectPutCustomersIdCreateprospectRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProspectPutCustomersIdCreateprospectRequestBody</code>.
   * @alias module:model/CreateProspectPutCustomersIdCreateprospectRequestBody
   * @param brokerId {Number} Broker ID
   */
  function CreateProspectPutCustomersIdCreateprospectRequestBody(brokerId) {
    _classCallCheck(this, CreateProspectPutCustomersIdCreateprospectRequestBody);

    CreateProspectPutCustomersIdCreateprospectRequestBody.initialize(this, brokerId);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProspectPutCustomersIdCreateprospectRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, brokerId) {
      obj['brokerId'] = brokerId;
    }
    /**
     * Constructs a <code>CreateProspectPutCustomersIdCreateprospectRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProspectPutCustomersIdCreateprospectRequestBody} obj Optional instance to populate.
     * @return {module:model/CreateProspectPutCustomersIdCreateprospectRequestBody} The populated <code>CreateProspectPutCustomersIdCreateprospectRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProspectPutCustomersIdCreateprospectRequestBody();

        if (data.hasOwnProperty('brokerId')) {
          obj['brokerId'] = _ApiClient["default"].convertToType(data['brokerId'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return CreateProspectPutCustomersIdCreateprospectRequestBody;
}();
/**
 * Broker ID
 * @member {Number} brokerId
 */


CreateProspectPutCustomersIdCreateprospectRequestBody.prototype['brokerId'] = undefined;
var _default = CreateProspectPutCustomersIdCreateprospectRequestBody;
exports["default"] = _default;