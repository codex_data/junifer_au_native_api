"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateProspectPostProspectsRequestBodyCustomerPrimaryContact model module.
 * @module model/CreateProspectPostProspectsRequestBodyCustomerPrimaryContact
 * @version 1.61.1
 */
var CreateProspectPostProspectsRequestBodyCustomerPrimaryContact = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProspectPostProspectsRequestBodyCustomerPrimaryContact</code>.
   * Primary contact
   * @alias module:model/CreateProspectPostProspectsRequestBodyCustomerPrimaryContact
   * @param contactType {String} Primary contact type name - see the Contact Type ref table for valid values
   */
  function CreateProspectPostProspectsRequestBodyCustomerPrimaryContact(contactType) {
    _classCallCheck(this, CreateProspectPostProspectsRequestBodyCustomerPrimaryContact);

    CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.initialize(this, contactType);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProspectPostProspectsRequestBodyCustomerPrimaryContact, null, [{
    key: "initialize",
    value: function initialize(obj, contactType) {
      obj['contactType'] = contactType;
    }
    /**
     * Constructs a <code>CreateProspectPostProspectsRequestBodyCustomerPrimaryContact</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProspectPostProspectsRequestBodyCustomerPrimaryContact} obj Optional instance to populate.
     * @return {module:model/CreateProspectPostProspectsRequestBodyCustomerPrimaryContact} The populated <code>CreateProspectPostProspectsRequestBodyCustomerPrimaryContact</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProspectPostProspectsRequestBodyCustomerPrimaryContact();

        if (data.hasOwnProperty('contactType')) {
          obj['contactType'] = _ApiClient["default"].convertToType(data['contactType'], 'String');
        }

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('phoneNumber1')) {
          obj['phoneNumber1'] = _ApiClient["default"].convertToType(data['phoneNumber1'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber2')) {
          obj['phoneNumber2'] = _ApiClient["default"].convertToType(data['phoneNumber2'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber3')) {
          obj['phoneNumber3'] = _ApiClient["default"].convertToType(data['phoneNumber3'], 'String');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _ApiClient["default"].convertToType(data['address'], Object);
        }

        if (data.hasOwnProperty('addressType')) {
          obj['addressType'] = _ApiClient["default"].convertToType(data['addressType'], 'String');
        }

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }

        if (data.hasOwnProperty('country')) {
          obj['country'] = _ApiClient["default"].convertToType(data['country'], 'String');
        }

        if (data.hasOwnProperty('careOf')) {
          obj['careOf'] = _ApiClient["default"].convertToType(data['careOf'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateProspectPostProspectsRequestBodyCustomerPrimaryContact;
}();
/**
 * Primary contact type name - see the Contact Type ref table for valid values
 * @member {String} contactType
 */


CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['contactType'] = undefined;
/**
 * Primary contact title - see the Title ref table for valid values
 * @member {String} title
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['title'] = undefined;
/**
 * Primary contact forename
 * @member {String} forename
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['forename'] = undefined;
/**
 * Primary contact surname
 * @member {String} surname
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['surname'] = undefined;
/**
 * Primary contact email
 * @member {String} email
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['email'] = undefined;
/**
 * Primary contact date of birth
 * @member {Date} dateOfBirth
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['dateOfBirth'] = undefined;
/**
 * Primary contact phone number 1
 * @member {String} phoneNumber1
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['phoneNumber1'] = undefined;
/**
 * Primary contact phone number 2
 * @member {String} phoneNumber2
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['phoneNumber2'] = undefined;
/**
 * Primary contact phone number 3
 * @member {String} phoneNumber3
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['phoneNumber3'] = undefined;
/**
 * Primary contact address
 * @member {Object} address
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['address'] = undefined;
/**
 * Primary contact address type - defaults to standard address type
 * @member {String} addressType
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['addressType'] = undefined;
/**
 * Primary contact address line 1
 * @member {String} address1
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['address1'] = undefined;
/**
 * Primary contact address line 2
 * @member {String} address2
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['address2'] = undefined;
/**
 * Primary contact address line 3
 * @member {String} address3
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['address3'] = undefined;
/**
 * Primary contact address line 4
 * @member {String} address4
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['address4'] = undefined;
/**
 * Primary contact address line 5
 * @member {String} address5
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['address5'] = undefined;
/**
 * Primary contact postcode
 * @member {String} postcode
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['postcode'] = undefined;
/**
 * Primary contact address country - see the Country ref table for valid values
 * @member {String} country
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['country'] = undefined;
/**
 * Primary contact address care of
 * @member {String} careOf
 */

CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.prototype['careOf'] = undefined;
var _default = CreateProspectPostProspectsRequestBodyCustomerPrimaryContact;
exports["default"] = _default;