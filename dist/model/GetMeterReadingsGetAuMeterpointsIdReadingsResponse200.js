"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetMeterReadingsGetAuMeterpointsIdReadingsResponse200 model module.
 * @module model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200
 * @version 1.61.1
 */
var GetMeterReadingsGetAuMeterpointsIdReadingsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetMeterReadingsGetAuMeterpointsIdReadingsResponse200</code>.
   * @alias module:model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200
   * @param id {Number} UtilityTimeSeriesEvent id, unless the meter read is type of `Pending` then this is the pendingMeterRead id.
   * @param readingDttm {Date} The date and time of the meter reading
   * @param status {String} The meter reading status
   * @param sequenceType {String} The sequence type of the meter reading
   * @param source {String} The source of the meter reading
   * @param quality {String} The quality of the meter reading
   */
  function GetMeterReadingsGetAuMeterpointsIdReadingsResponse200(id, readingDttm, status, sequenceType, source, quality) {
    _classCallCheck(this, GetMeterReadingsGetAuMeterpointsIdReadingsResponse200);

    GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.initialize(this, id, readingDttm, status, sequenceType, source, quality);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetMeterReadingsGetAuMeterpointsIdReadingsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, readingDttm, status, sequenceType, source, quality) {
      obj['id'] = id;
      obj['readingDttm'] = readingDttm;
      obj['status'] = status;
      obj['sequenceType'] = sequenceType;
      obj['source'] = source;
      obj['quality'] = quality;
    }
    /**
     * Constructs a <code>GetMeterReadingsGetAuMeterpointsIdReadingsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200} obj Optional instance to populate.
     * @return {module:model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200} The populated <code>GetMeterReadingsGetAuMeterpointsIdReadingsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetMeterReadingsGetAuMeterpointsIdReadingsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('readingDttm')) {
          obj['readingDttm'] = _ApiClient["default"].convertToType(data['readingDttm'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('sequenceType')) {
          obj['sequenceType'] = _ApiClient["default"].convertToType(data['sequenceType'], 'String');
        }

        if (data.hasOwnProperty('source')) {
          obj['source'] = _ApiClient["default"].convertToType(data['source'], 'String');
        }

        if (data.hasOwnProperty('quality')) {
          obj['quality'] = _ApiClient["default"].convertToType(data['quality'], 'String');
        }

        if (data.hasOwnProperty('cumulative')) {
          obj['cumulative'] = _ApiClient["default"].convertToType(data['cumulative'], 'Number');
        }

        if (data.hasOwnProperty('consumption')) {
          obj['consumption'] = _ApiClient["default"].convertToType(data['consumption'], 'Number');
        }

        if (data.hasOwnProperty('unit')) {
          obj['unit'] = _ApiClient["default"].convertToType(data['unit'], 'String');
        }

        if (data.hasOwnProperty('receivedDttm')) {
          obj['receivedDttm'] = _ApiClient["default"].convertToType(data['receivedDttm'], 'Date');
        }

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('meter')) {
          obj['meter'] = _ApiClient["default"].convertToType(data['meter'], 'String');
        }

        if (data.hasOwnProperty('meterId')) {
          obj['meterId'] = _ApiClient["default"].convertToType(data['meterId'], 'Number');
        }

        if (data.hasOwnProperty('register')) {
          obj['register'] = _ApiClient["default"].convertToType(data['register'], 'String');
        }

        if (data.hasOwnProperty('meterRegisterId')) {
          obj['meterRegisterId'] = _ApiClient["default"].convertToType(data['meterRegisterId'], 'Number');
        }

        if (data.hasOwnProperty('meterMeasurementType')) {
          obj['meterMeasurementType'] = _ApiClient["default"].convertToType(data['meterMeasurementType'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetMeterReadingsGetAuMeterpointsIdReadingsResponse200;
}();
/**
 * UtilityTimeSeriesEvent id, unless the meter read is type of `Pending` then this is the pendingMeterRead id.
 * @member {Number} id
 */


GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['id'] = undefined;
/**
 * The date and time of the meter reading
 * @member {Date} readingDttm
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['readingDttm'] = undefined;
/**
 * The meter reading status
 * @member {String} status
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['status'] = undefined;
/**
 * The sequence type of the meter reading
 * @member {String} sequenceType
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['sequenceType'] = undefined;
/**
 * The source of the meter reading
 * @member {String} source
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['source'] = undefined;
/**
 * The quality of the meter reading
 * @member {String} quality
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['quality'] = undefined;
/**
 * The cumulative value of this meter reading
 * @member {Number} cumulative
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['cumulative'] = undefined;
/**
 * Consumption registered in this meter reading. Accepted readings only
 * @member {Number} consumption
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['consumption'] = undefined;
/**
 * The metric unit of the meter reading. Accepted readings only
 * @member {String} unit
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['unit'] = undefined;
/**
 * Received Dttm the date of when the system receives a read
 * @member {Date} receivedDttm
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['receivedDttm'] = undefined;
/**
 * From Dttm the date of the last read.
 * @member {Date} fromDttm
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['fromDttm'] = undefined;
/**
 * Meter identifier. Accepted readings only
 * @member {String} meter
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['meter'] = undefined;
/**
 * Meter id.
 * @member {Number} meterId
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['meterId'] = undefined;
/**
 * Meter register identifier to which the meter reading belongs. Accepted readings only
 * @member {String} register
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['register'] = undefined;
/**
 * Meter Register id
 * @member {Number} meterRegisterId
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['meterRegisterId'] = undefined;
/**
 * Meter measurement type assigned to the meter register
 * @member {String} meterMeasurementType
 */

GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.prototype['meterMeasurementType'] = undefined;
var _default = GetMeterReadingsGetAuMeterpointsIdReadingsResponse200;
exports["default"] = _default;