"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links = _interopRequireDefault(require("./GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files model module.
 * @module model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files
 * @version 1.61.1
 */
var GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files</code>.
   * @alias module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files
   * @param id {Number} the id of the file
   * @param links {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links} 
   */
  function GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files(id, links) {
    _classCallCheck(this, GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files);

    GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files.initialize(this, id, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files, null, [{
    key: "initialize",
    value: function initialize(obj, id, links) {
      obj['id'] = id;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files} obj Optional instance to populate.
     * @return {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files} The populated <code>GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files;
}();
/**
 * the id of the file
 * @member {Number} id
 */


GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files.prototype['id'] = undefined;
/**
 * @member {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links} links
 */

GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files.prototype['links'] = undefined;
var _default = GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files;
exports["default"] = _default;