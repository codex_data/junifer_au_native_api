"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody model module.
 * @module model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody
 * @version 1.61.1
 */
var CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody</code>.
   * @alias module:model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody
   * @param amount {Number} How much money you wish to repay
   */
  function CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody(amount) {
    _classCallCheck(this, CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody);

    CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody.initialize(this, amount);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, amount) {
      obj['amount'] = amount;
    }
    /**
     * Constructs a <code>CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody} obj Optional instance to populate.
     * @return {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody} The populated <code>CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody();

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody;
}();
/**
 * How much money you wish to repay
 * @member {Number} amount
 */


CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody.prototype['amount'] = undefined;
/**
 * Repayment description
 * @member {String} description
 */

CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody.prototype['description'] = undefined;
var _default = CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody;
exports["default"] = _default;