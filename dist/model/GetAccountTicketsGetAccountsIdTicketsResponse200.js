"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateAccountTicketPostAccountsIdTicketsResponse200Links = _interopRequireDefault(require("./CreateAccountTicketPostAccountsIdTicketsResponse200Links"));

var _CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities = _interopRequireDefault(require("./CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountTicketsGetAccountsIdTicketsResponse200 model module.
 * @module model/GetAccountTicketsGetAccountsIdTicketsResponse200
 * @version 1.61.1
 */
var GetAccountTicketsGetAccountsIdTicketsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountTicketsGetAccountsIdTicketsResponse200</code>.
   * @alias module:model/GetAccountTicketsGetAccountsIdTicketsResponse200
   * @param id {Number} Ticket Identifier
   * @param ticketDefinitionCode {String} Ticket definition code
   * @param ticketDefinitionName {String} Ticket definition name
   * @param ticketPriority {String} Ticket priority
   * @param ticketStepCode {String} Ticket step code
   * @param ticketStepName {String} Ticket step name
   * @param status {String} Ticket status. Can be one of following: `Open, Closed, Cancelled, Error`
   * @param keyIdentifier {String} Ticket key identifier
   * @param summary {String} Ticket summary
   * @param createdDttm {Date} Ticket creation date and time
   * @param ticketEntities {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities} 
   * @param links {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200Links} 
   */
  function GetAccountTicketsGetAccountsIdTicketsResponse200(id, ticketDefinitionCode, ticketDefinitionName, ticketPriority, ticketStepCode, ticketStepName, status, keyIdentifier, summary, createdDttm, ticketEntities, links) {
    _classCallCheck(this, GetAccountTicketsGetAccountsIdTicketsResponse200);

    GetAccountTicketsGetAccountsIdTicketsResponse200.initialize(this, id, ticketDefinitionCode, ticketDefinitionName, ticketPriority, ticketStepCode, ticketStepName, status, keyIdentifier, summary, createdDttm, ticketEntities, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountTicketsGetAccountsIdTicketsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, ticketDefinitionCode, ticketDefinitionName, ticketPriority, ticketStepCode, ticketStepName, status, keyIdentifier, summary, createdDttm, ticketEntities, links) {
      obj['id'] = id;
      obj['ticketDefinitionCode'] = ticketDefinitionCode;
      obj['ticketDefinitionName'] = ticketDefinitionName;
      obj['ticketPriority'] = ticketPriority;
      obj['ticketStepCode'] = ticketStepCode;
      obj['ticketStepName'] = ticketStepName;
      obj['status'] = status;
      obj['keyIdentifier'] = keyIdentifier;
      obj['summary'] = summary;
      obj['createdDttm'] = createdDttm;
      obj['ticketEntities'] = ticketEntities;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountTicketsGetAccountsIdTicketsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountTicketsGetAccountsIdTicketsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountTicketsGetAccountsIdTicketsResponse200} The populated <code>GetAccountTicketsGetAccountsIdTicketsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountTicketsGetAccountsIdTicketsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('parentTicketId')) {
          obj['parentTicketId'] = _ApiClient["default"].convertToType(data['parentTicketId'], 'Number');
        }

        if (data.hasOwnProperty('ticketDefinitionCode')) {
          obj['ticketDefinitionCode'] = _ApiClient["default"].convertToType(data['ticketDefinitionCode'], 'String');
        }

        if (data.hasOwnProperty('ticketDefinitionName')) {
          obj['ticketDefinitionName'] = _ApiClient["default"].convertToType(data['ticketDefinitionName'], 'String');
        }

        if (data.hasOwnProperty('ticketPriority')) {
          obj['ticketPriority'] = _ApiClient["default"].convertToType(data['ticketPriority'], 'String');
        }

        if (data.hasOwnProperty('ticketStepCode')) {
          obj['ticketStepCode'] = _ApiClient["default"].convertToType(data['ticketStepCode'], 'String');
        }

        if (data.hasOwnProperty('ticketStepName')) {
          obj['ticketStepName'] = _ApiClient["default"].convertToType(data['ticketStepName'], 'String');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('keyIdentifier')) {
          obj['keyIdentifier'] = _ApiClient["default"].convertToType(data['keyIdentifier'], 'String');
        }

        if (data.hasOwnProperty('summary')) {
          obj['summary'] = _ApiClient["default"].convertToType(data['summary'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('dueDttm')) {
          obj['dueDttm'] = _ApiClient["default"].convertToType(data['dueDttm'], 'Date');
        }

        if (data.hasOwnProperty('cancelDttm')) {
          obj['cancelDttm'] = _ApiClient["default"].convertToType(data['cancelDttm'], 'Date');
        }

        if (data.hasOwnProperty('ticketCancelReason')) {
          obj['ticketCancelReason'] = _ApiClient["default"].convertToType(data['ticketCancelReason'], 'String');
        }

        if (data.hasOwnProperty('assignedUserTeam')) {
          obj['assignedUserTeam'] = _ApiClient["default"].convertToType(data['assignedUserTeam'], 'String');
        }

        if (data.hasOwnProperty('stepStartDttm')) {
          obj['stepStartDttm'] = _ApiClient["default"].convertToType(data['stepStartDttm'], 'Date');
        }

        if (data.hasOwnProperty('stepScheduleDttm')) {
          obj['stepScheduleDttm'] = _ApiClient["default"].convertToType(data['stepScheduleDttm'], 'Date');
        }

        if (data.hasOwnProperty('stepDueDttm')) {
          obj['stepDueDttm'] = _ApiClient["default"].convertToType(data['stepDueDttm'], 'Date');
        }

        if (data.hasOwnProperty('ticketEntities')) {
          obj['ticketEntities'] = _CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities["default"].constructFromObject(data['ticketEntities']);
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _CreateAccountTicketPostAccountsIdTicketsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountTicketsGetAccountsIdTicketsResponse200;
}();
/**
 * Ticket Identifier
 * @member {Number} id
 */


GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['id'] = undefined;
/**
 * The ID of the parent ticket
 * @member {Number} parentTicketId
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['parentTicketId'] = undefined;
/**
 * Ticket definition code
 * @member {String} ticketDefinitionCode
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['ticketDefinitionCode'] = undefined;
/**
 * Ticket definition name
 * @member {String} ticketDefinitionName
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['ticketDefinitionName'] = undefined;
/**
 * Ticket priority
 * @member {String} ticketPriority
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['ticketPriority'] = undefined;
/**
 * Ticket step code
 * @member {String} ticketStepCode
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['ticketStepCode'] = undefined;
/**
 * Ticket step name
 * @member {String} ticketStepName
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['ticketStepName'] = undefined;
/**
 * Ticket status. Can be one of following: `Open, Closed, Cancelled, Error`
 * @member {String} status
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['status'] = undefined;
/**
 * Ticket key identifier
 * @member {String} keyIdentifier
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['keyIdentifier'] = undefined;
/**
 * Ticket summary
 * @member {String} summary
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['summary'] = undefined;
/**
 * Ticket description
 * @member {String} description
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['description'] = undefined;
/**
 * Ticket creation date and time
 * @member {Date} createdDttm
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['createdDttm'] = undefined;
/**
 * Ticket due date and time
 * @member {Date} dueDttm
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['dueDttm'] = undefined;
/**
 * Date and time when ticket was cancelled
 * @member {Date} cancelDttm
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['cancelDttm'] = undefined;
/**
 * The reason ticket was cancelled
 * @member {String} ticketCancelReason
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['ticketCancelReason'] = undefined;
/**
 * Team assigned to deal with this ticket
 * @member {String} assignedUserTeam
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['assignedUserTeam'] = undefined;
/**
 * Ticket step start date and time
 * @member {Date} stepStartDttm
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['stepStartDttm'] = undefined;
/**
 * Ticket step scheduled date and time
 * @member {Date} stepScheduleDttm
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['stepScheduleDttm'] = undefined;
/**
 * Ticket step due date and time
 * @member {Date} stepDueDttm
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['stepDueDttm'] = undefined;
/**
 * @member {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities} ticketEntities
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['ticketEntities'] = undefined;
/**
 * @member {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200Links} links
 */

GetAccountTicketsGetAccountsIdTicketsResponse200.prototype['links'] = undefined;
var _default = GetAccountTicketsGetAccountsIdTicketsResponse200;
exports["default"] = _default;