"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404 model module.
 * @module model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404
 * @version 1.61.1
 */
var CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404</code>.
   * @alias module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404
   * @param errorCode {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404.ErrorCodeEnum} Field: * `AccountNotFound` - Account with such 'accountid' does not exist
   * @param errorSeverity {String} The error severity
   */
  function CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404(errorCode, errorSeverity) {
    _classCallCheck(this, CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404);

    CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404} obj Optional instance to populate.
     * @return {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404} The populated <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404;
}();
/**
 * Field: * `AccountNotFound` - Account with such 'accountid' does not exist
 * @member {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404.ErrorCodeEnum} errorCode
 */


CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404['ErrorCodeEnum'] = {
  /**
   * value: "AccountNotFound"
   * @const
   */
  "AccountNotFound": "AccountNotFound"
};
var _default = CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404;
exports["default"] = _default;