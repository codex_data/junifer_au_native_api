"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The NewBpointDirectDebitPostAuBpointdirectdebitsResponse200 model module.
 * @module model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200
 * @version 1.61.1
 */
var NewBpointDirectDebitPostAuBpointdirectdebitsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>NewBpointDirectDebitPostAuBpointdirectdebitsResponse200</code>.
   * @alias module:model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200
   * @param id {Number} BPOINT Direct Debit object id
   * @param accountName {String} Account name
   * @param mandateReference {String} Mandate Reference
   * @param paymentType {String} Payment type. Can be either `BankAccount` or `CreditCard`
   * @param status {String} Current status of the Direct Debit
   */
  function NewBpointDirectDebitPostAuBpointdirectdebitsResponse200(id, accountName, mandateReference, paymentType, status) {
    _classCallCheck(this, NewBpointDirectDebitPostAuBpointdirectdebitsResponse200);

    NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.initialize(this, id, accountName, mandateReference, paymentType, status);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(NewBpointDirectDebitPostAuBpointdirectdebitsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, accountName, mandateReference, paymentType, status) {
      obj['id'] = id;
      obj['accountName'] = accountName;
      obj['mandateReference'] = mandateReference;
      obj['paymentType'] = paymentType;
      obj['status'] = status;
    }
    /**
     * Constructs a <code>NewBpointDirectDebitPostAuBpointdirectdebitsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200} obj Optional instance to populate.
     * @return {module:model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200} The populated <code>NewBpointDirectDebitPostAuBpointdirectdebitsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new NewBpointDirectDebitPostAuBpointdirectdebitsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('accountName')) {
          obj['accountName'] = _ApiClient["default"].convertToType(data['accountName'], 'String');
        }

        if (data.hasOwnProperty('mandateReference')) {
          obj['mandateReference'] = _ApiClient["default"].convertToType(data['mandateReference'], 'String');
        }

        if (data.hasOwnProperty('paymentType')) {
          obj['paymentType'] = _ApiClient["default"].convertToType(data['paymentType'], 'String');
        }

        if (data.hasOwnProperty('authorisedDttm')) {
          obj['authorisedDttm'] = _ApiClient["default"].convertToType(data['authorisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('terminatedDttm')) {
          obj['terminatedDttm'] = _ApiClient["default"].convertToType(data['terminatedDttm'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }
      }

      return obj;
    }
  }]);

  return NewBpointDirectDebitPostAuBpointdirectdebitsResponse200;
}();
/**
 * BPOINT Direct Debit object id
 * @member {Number} id
 */


NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.prototype['id'] = undefined;
/**
 * Account name
 * @member {String} accountName
 */

NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.prototype['accountName'] = undefined;
/**
 * Mandate Reference
 * @member {String} mandateReference
 */

NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.prototype['mandateReference'] = undefined;
/**
 * Payment type. Can be either `BankAccount` or `CreditCard`
 * @member {String} paymentType
 */

NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.prototype['paymentType'] = undefined;
/**
 * Date when the Direct Debit was authorised
 * @member {Date} authorisedDttm
 */

NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.prototype['authorisedDttm'] = undefined;
/**
 * Date when the Direct Debit was terminated
 * @member {Date} terminatedDttm
 */

NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.prototype['terminatedDttm'] = undefined;
/**
 * Current status of the Direct Debit
 * @member {String} status
 */

NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.prototype['status'] = undefined;
var _default = NewBpointDirectDebitPostAuBpointdirectdebitsResponse200;
exports["default"] = _default;