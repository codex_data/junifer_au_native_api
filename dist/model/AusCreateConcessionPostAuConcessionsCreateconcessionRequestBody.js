"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody model module.
 * @module model/AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody
 * @version 1.61.1
 */
var AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody</code>.
   * @alias module:model/AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody
   * @param accountId {Number} Account ID
   * @param cardNumber {String} Card number
   * @param contactId {Number} Contact ID
   * @param fromDt {Date} From date
   */
  function AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody(accountId, cardNumber, contactId, fromDt) {
    _classCallCheck(this, AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody);

    AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.initialize(this, accountId, cardNumber, contactId, fromDt);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, accountId, cardNumber, contactId, fromDt) {
      obj['accountId'] = accountId;
      obj['cardNumber'] = cardNumber;
      obj['contactId'] = contactId;
      obj['fromDt'] = fromDt;
    }
    /**
     * Constructs a <code>AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody} obj Optional instance to populate.
     * @return {module:model/AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody} The populated <code>AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody();

        if (data.hasOwnProperty('accountId')) {
          obj['accountId'] = _ApiClient["default"].convertToType(data['accountId'], 'Number');
        }

        if (data.hasOwnProperty('concessionDfnId')) {
          obj['concessionDfnId'] = _ApiClient["default"].convertToType(data['concessionDfnId'], 'Number');
        }

        if (data.hasOwnProperty('concessionDfnName')) {
          obj['concessionDfnName'] = _ApiClient["default"].convertToType(data['concessionDfnName'], 'String');
        }

        if (data.hasOwnProperty('concessionCardTypeId')) {
          obj['concessionCardTypeId'] = _ApiClient["default"].convertToType(data['concessionCardTypeId'], 'Number');
        }

        if (data.hasOwnProperty('concessionCardTypeName')) {
          obj['concessionCardTypeName'] = _ApiClient["default"].convertToType(data['concessionCardTypeName'], 'String');
        }

        if (data.hasOwnProperty('cardNumber')) {
          obj['cardNumber'] = _ApiClient["default"].convertToType(data['cardNumber'], 'String');
        }

        if (data.hasOwnProperty('applicantName')) {
          obj['applicantName'] = _ApiClient["default"].convertToType(data['applicantName'], 'String');
        }

        if (data.hasOwnProperty('contactId')) {
          obj['contactId'] = _ApiClient["default"].convertToType(data['contactId'], 'Number');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('endDt')) {
          obj['endDt'] = _ApiClient["default"].convertToType(data['endDt'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody;
}();
/**
 * Account ID
 * @member {Number} accountId
 */


AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['accountId'] = undefined;
/**
 * Concession definition ID (required if no name given)
 * @member {Number} concessionDfnId
 */

AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['concessionDfnId'] = undefined;
/**
 * Concession definition name (required if no ID given)
 * @member {String} concessionDfnName
 */

AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['concessionDfnName'] = undefined;
/**
 * Concession card type ID (required if no name given)
 * @member {Number} concessionCardTypeId
 */

AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['concessionCardTypeId'] = undefined;
/**
 * Concession card type name (required if no ID given)
 * @member {String} concessionCardTypeName
 */

AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['concessionCardTypeName'] = undefined;
/**
 * Card number
 * @member {String} cardNumber
 */

AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['cardNumber'] = undefined;
/**
 * Applicant name
 * @member {String} applicantName
 */

AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['applicantName'] = undefined;
/**
 * Contact ID
 * @member {Number} contactId
 */

AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['contactId'] = undefined;
/**
 * From date
 * @member {Date} fromDt
 */

AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['fromDt'] = undefined;
/**
 * End date
 * @member {Date} endDt
 */

AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.prototype['endDt'] = undefined;
var _default = AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody;
exports["default"] = _default;