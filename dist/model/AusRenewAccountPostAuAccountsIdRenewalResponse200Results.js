"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusRenewAccountPostAuAccountsIdRenewalResponse200Results model module.
 * @module model/AusRenewAccountPostAuAccountsIdRenewalResponse200Results
 * @version 1.61.1
 */
var AusRenewAccountPostAuAccountsIdRenewalResponse200Results = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusRenewAccountPostAuAccountsIdRenewalResponse200Results</code>.
   * @alias module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200Results
   * @param accountNumber {String} Account Number
   * @param elecProductBundleId {String} ID of the newly generated electricity product bundle
   */
  function AusRenewAccountPostAuAccountsIdRenewalResponse200Results(accountNumber, elecProductBundleId) {
    _classCallCheck(this, AusRenewAccountPostAuAccountsIdRenewalResponse200Results);

    AusRenewAccountPostAuAccountsIdRenewalResponse200Results.initialize(this, accountNumber, elecProductBundleId);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusRenewAccountPostAuAccountsIdRenewalResponse200Results, null, [{
    key: "initialize",
    value: function initialize(obj, accountNumber, elecProductBundleId) {
      obj['accountNumber'] = accountNumber;
      obj['elecProductBundleId'] = elecProductBundleId;
    }
    /**
     * Constructs a <code>AusRenewAccountPostAuAccountsIdRenewalResponse200Results</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200Results} obj Optional instance to populate.
     * @return {module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200Results} The populated <code>AusRenewAccountPostAuAccountsIdRenewalResponse200Results</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusRenewAccountPostAuAccountsIdRenewalResponse200Results();

        if (data.hasOwnProperty('accountNumber')) {
          obj['accountNumber'] = _ApiClient["default"].convertToType(data['accountNumber'], 'String');
        }

        if (data.hasOwnProperty('elecProductBundleId')) {
          obj['elecProductBundleId'] = _ApiClient["default"].convertToType(data['elecProductBundleId'], 'String');
        }
      }

      return obj;
    }
  }]);

  return AusRenewAccountPostAuAccountsIdRenewalResponse200Results;
}();
/**
 * Account Number
 * @member {String} accountNumber
 */


AusRenewAccountPostAuAccountsIdRenewalResponse200Results.prototype['accountNumber'] = undefined;
/**
 * ID of the newly generated electricity product bundle
 * @member {String} elecProductBundleId
 */

AusRenewAccountPostAuAccountsIdRenewalResponse200Results.prototype['elecProductBundleId'] = undefined;
var _default = AusRenewAccountPostAuAccountsIdRenewalResponse200Results;
exports["default"] = _default;