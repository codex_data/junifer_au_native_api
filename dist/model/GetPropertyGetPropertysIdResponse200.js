"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetPropertyGetPropertysIdResponse200Address = _interopRequireDefault(require("./GetPropertyGetPropertysIdResponse200Address"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPropertyGetPropertysIdResponse200 model module.
 * @module model/GetPropertyGetPropertysIdResponse200
 * @version 1.61.1
 */
var GetPropertyGetPropertysIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPropertyGetPropertysIdResponse200</code>.
   * @alias module:model/GetPropertyGetPropertysIdResponse200
   * @param id {Number} The ID of the property
   * @param propertyType {String} The type of the property. This will be one of the values in the \"Property Type\" ref table.
   * @param address {module:model/GetPropertyGetPropertysIdResponse200Address} 
   * @param addressType {String} The type of the address
   */
  function GetPropertyGetPropertysIdResponse200(id, propertyType, address, addressType) {
    _classCallCheck(this, GetPropertyGetPropertysIdResponse200);

    GetPropertyGetPropertysIdResponse200.initialize(this, id, propertyType, address, addressType);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPropertyGetPropertysIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, propertyType, address, addressType) {
      obj['id'] = id;
      obj['propertyType'] = propertyType;
      obj['address'] = address;
      obj['addressType'] = addressType;
    }
    /**
     * Constructs a <code>GetPropertyGetPropertysIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPropertyGetPropertysIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetPropertyGetPropertysIdResponse200} The populated <code>GetPropertyGetPropertysIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPropertyGetPropertysIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('propertyType')) {
          obj['propertyType'] = _ApiClient["default"].convertToType(data['propertyType'], 'String');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _GetPropertyGetPropertysIdResponse200Address["default"].constructFromObject(data['address']);
        }

        if (data.hasOwnProperty('addressType')) {
          obj['addressType'] = _ApiClient["default"].convertToType(data['addressType'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetPropertyGetPropertysIdResponse200;
}();
/**
 * The ID of the property
 * @member {Number} id
 */


GetPropertyGetPropertysIdResponse200.prototype['id'] = undefined;
/**
 * The type of the property. This will be one of the values in the \"Property Type\" ref table.
 * @member {String} propertyType
 */

GetPropertyGetPropertysIdResponse200.prototype['propertyType'] = undefined;
/**
 * @member {module:model/GetPropertyGetPropertysIdResponse200Address} address
 */

GetPropertyGetPropertysIdResponse200.prototype['address'] = undefined;
/**
 * The type of the address
 * @member {String} addressType
 */

GetPropertyGetPropertysIdResponse200.prototype['addressType'] = undefined;
var _default = GetPropertyGetPropertysIdResponse200;
exports["default"] = _default;