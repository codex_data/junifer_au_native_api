"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements model module.
 * @module model/AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements
 * @version 1.61.1
 */
var AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements</code>.
   * @alias module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements
   * @param electricityProductCode {String} Product code for new electricity agreement
   * @param electricitySupplyProductSubType {String} Product sub type for new electricity agreement
   * @param electricityStartDate {String} Start date of new electricity agreement
   */
  function AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements(electricityProductCode, electricitySupplyProductSubType, electricityStartDate) {
    _classCallCheck(this, AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements);

    AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements.initialize(this, electricityProductCode, electricitySupplyProductSubType, electricityStartDate);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements, null, [{
    key: "initialize",
    value: function initialize(obj, electricityProductCode, electricitySupplyProductSubType, electricityStartDate) {
      obj['electricityProductCode'] = electricityProductCode;
      obj['electricitySupplyProductSubType'] = electricitySupplyProductSubType;
      obj['electricityStartDate'] = electricityStartDate;
    }
    /**
     * Constructs a <code>AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements} obj Optional instance to populate.
     * @return {module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements} The populated <code>AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements();

        if (data.hasOwnProperty('electricityProductCode')) {
          obj['electricityProductCode'] = _ApiClient["default"].convertToType(data['electricityProductCode'], 'String');
        }

        if (data.hasOwnProperty('electricitySupplyProductSubType')) {
          obj['electricitySupplyProductSubType'] = _ApiClient["default"].convertToType(data['electricitySupplyProductSubType'], 'String');
        }

        if (data.hasOwnProperty('electricityStartDate')) {
          obj['electricityStartDate'] = _ApiClient["default"].convertToType(data['electricityStartDate'], 'String');
        }

        if (data.hasOwnProperty('agreementIdToRenew')) {
          obj['agreementIdToRenew'] = _ApiClient["default"].convertToType(data['agreementIdToRenew'], 'Number');
        }

        if (data.hasOwnProperty('agreementNumberToRenew')) {
          obj['agreementNumberToRenew'] = _ApiClient["default"].convertToType(data['agreementNumberToRenew'], 'String');
        }

        if (data.hasOwnProperty('meterPointIdentifier')) {
          obj['meterPointIdentifier'] = _ApiClient["default"].convertToType(data['meterPointIdentifier'], 'String');
        }
      }

      return obj;
    }
  }]);

  return AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements;
}();
/**
 * Product code for new electricity agreement
 * @member {String} electricityProductCode
 */


AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements.prototype['electricityProductCode'] = undefined;
/**
 * Product sub type for new electricity agreement
 * @member {String} electricitySupplyProductSubType
 */

AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements.prototype['electricitySupplyProductSubType'] = undefined;
/**
 * Start date of new electricity agreement
 * @member {String} electricityStartDate
 */

AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements.prototype['electricityStartDate'] = undefined;
/**
 * Id of the existing agreement to be replaced. If neither `agreementIdToRenew` nor `agreementNumberToRenew` are provided, a new agreement will be created alongside existing agreements instead of replacing an existing agreement.
 * @member {Number} agreementIdToRenew
 */

AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements.prototype['agreementIdToRenew'] = undefined;
/**
 * Number of the existing agreement to be replaced. If neither `agreementIdToRenew` nor `agreementNumberToRenew` are provided, a new agreement will be created alongside existing agreements instead of replacing an existing agreement.
 * @member {String} agreementNumberToRenew
 */

AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements.prototype['agreementNumberToRenew'] = undefined;
/**
 * Meter point identifier if creating a new asset-linked agreement.
 * @member {String} meterPointIdentifier
 */

AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements.prototype['meterPointIdentifier'] = undefined;
var _default = AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements;
exports["default"] = _default;