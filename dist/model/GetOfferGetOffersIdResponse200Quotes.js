"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetOfferGetOffersIdResponse200QuotesLinks = _interopRequireDefault(require("./GetOfferGetOffersIdResponse200QuotesLinks"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetOfferGetOffersIdResponse200Quotes model module.
 * @module model/GetOfferGetOffersIdResponse200Quotes
 * @version 1.61.1
 */
var GetOfferGetOffersIdResponse200Quotes = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetOfferGetOffersIdResponse200Quotes</code>.
   * The quote(s) that the offer is associated with.
   * @alias module:model/GetOfferGetOffersIdResponse200Quotes
   * @param number {String} The quotes number.
   * @param createdDttm {Date} The creation date and time for the quote.
   * @param status {String} The status of the quote.
   * @param links {module:model/GetOfferGetOffersIdResponse200QuotesLinks} 
   */
  function GetOfferGetOffersIdResponse200Quotes(number, createdDttm, status, links) {
    _classCallCheck(this, GetOfferGetOffersIdResponse200Quotes);

    GetOfferGetOffersIdResponse200Quotes.initialize(this, number, createdDttm, status, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetOfferGetOffersIdResponse200Quotes, null, [{
    key: "initialize",
    value: function initialize(obj, number, createdDttm, status, links) {
      obj['number'] = number;
      obj['createdDttm'] = createdDttm;
      obj['status'] = status;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetOfferGetOffersIdResponse200Quotes</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetOfferGetOffersIdResponse200Quotes} obj Optional instance to populate.
     * @return {module:model/GetOfferGetOffersIdResponse200Quotes} The populated <code>GetOfferGetOffersIdResponse200Quotes</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetOfferGetOffersIdResponse200Quotes();

        if (data.hasOwnProperty('number')) {
          obj['number'] = _ApiClient["default"].convertToType(data['number'], 'String');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetOfferGetOffersIdResponse200QuotesLinks["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetOfferGetOffersIdResponse200Quotes;
}();
/**
 * The quotes number.
 * @member {String} number
 */


GetOfferGetOffersIdResponse200Quotes.prototype['number'] = undefined;
/**
 * The creation date and time for the quote.
 * @member {Date} createdDttm
 */

GetOfferGetOffersIdResponse200Quotes.prototype['createdDttm'] = undefined;
/**
 * The status of the quote.
 * @member {String} status
 */

GetOfferGetOffersIdResponse200Quotes.prototype['status'] = undefined;
/**
 * @member {module:model/GetOfferGetOffersIdResponse200QuotesLinks} links
 */

GetOfferGetOffersIdResponse200Quotes.prototype['links'] = undefined;
var _default = GetOfferGetOffersIdResponse200Quotes;
exports["default"] = _default;