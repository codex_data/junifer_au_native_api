"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateAccountContactPutAccountsIdContactsContactidResponse400 model module.
 * @module model/UpdateAccountContactPutAccountsIdContactsContactidResponse400
 * @version 1.61.1
 */
var UpdateAccountContactPutAccountsIdContactsContactidResponse400 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateAccountContactPutAccountsIdContactsContactidResponse400</code>.
   * @alias module:model/UpdateAccountContactPutAccountsIdContactsContactidResponse400
   * @param errorCode {module:model/UpdateAccountContactPutAccountsIdContactsContactidResponse400.ErrorCodeEnum} Field: * `RequiredParameterMissing` - The supplied `surname` parameter contains no value * `UnrecognisedTitle` - The supplied 'title' parameter `title` is not recognised
   * @param errorSeverity {String} The error severity
   */
  function UpdateAccountContactPutAccountsIdContactsContactidResponse400(errorCode, errorSeverity) {
    _classCallCheck(this, UpdateAccountContactPutAccountsIdContactsContactidResponse400);

    UpdateAccountContactPutAccountsIdContactsContactidResponse400.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateAccountContactPutAccountsIdContactsContactidResponse400, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>UpdateAccountContactPutAccountsIdContactsContactidResponse400</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateAccountContactPutAccountsIdContactsContactidResponse400} obj Optional instance to populate.
     * @return {module:model/UpdateAccountContactPutAccountsIdContactsContactidResponse400} The populated <code>UpdateAccountContactPutAccountsIdContactsContactidResponse400</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateAccountContactPutAccountsIdContactsContactidResponse400();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return UpdateAccountContactPutAccountsIdContactsContactidResponse400;
}();
/**
 * Field: * `RequiredParameterMissing` - The supplied `surname` parameter contains no value * `UnrecognisedTitle` - The supplied 'title' parameter `title` is not recognised
 * @member {module:model/UpdateAccountContactPutAccountsIdContactsContactidResponse400.ErrorCodeEnum} errorCode
 */


UpdateAccountContactPutAccountsIdContactsContactidResponse400.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

UpdateAccountContactPutAccountsIdContactsContactidResponse400.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

UpdateAccountContactPutAccountsIdContactsContactidResponse400.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

UpdateAccountContactPutAccountsIdContactsContactidResponse400['ErrorCodeEnum'] = {
  /**
   * value: "RequiredParameterMissing"
   * @const
   */
  "RequiredParameterMissing": "RequiredParameterMissing",

  /**
   * value: "UnrecognisedTitle"
   * @const
   */
  "UnrecognisedTitle": "UnrecognisedTitle"
};
var _default = UpdateAccountContactPutAccountsIdContactsContactidResponse400;
exports["default"] = _default;