"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links model module.
 * @module model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links
 * @version 1.61.1
 */
var GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links</code>.
   * Links to related sources
   * @alias module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links
   * @param paymentSchedulePeriod {String} Link to the payment schedule period that the payment schedule item is associated with
   */
  function GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links(paymentSchedulePeriod) {
    _classCallCheck(this, GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links);

    GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links.initialize(this, paymentSchedulePeriod);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, paymentSchedulePeriod) {
      obj['paymentSchedulePeriod'] = paymentSchedulePeriod;
    }
    /**
     * Constructs a <code>GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links} The populated <code>GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links();

        if (data.hasOwnProperty('paymentSchedulePeriod')) {
          obj['paymentSchedulePeriod'] = _ApiClient["default"].convertToType(data['paymentSchedulePeriod'], 'String');
        }

        if (data.hasOwnProperty('paymentRequest')) {
          obj['paymentRequest'] = _ApiClient["default"].convertToType(data['paymentRequest'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links;
}();
/**
 * Link to the payment schedule period that the payment schedule item is associated with
 * @member {String} paymentSchedulePeriod
 */


GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links.prototype['paymentSchedulePeriod'] = undefined;
/**
 * Link to the payment request that the payment schedule item is associated with
 * @member {String} paymentRequest
 */

GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links.prototype['paymentRequest'] = undefined;
var _default = GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links;
exports["default"] = _default;