"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct"));

var _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress = _interopRequireDefault(require("./EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress"));

var _EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts = _interopRequireDefault(require("./EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolCustomerPostCustomersEnrolcustomerRequestBody model module.
 * @module model/EnrolCustomerPostCustomersEnrolcustomerRequestBody
 * @version 1.61.1
 */
var EnrolCustomerPostCustomersEnrolcustomerRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolCustomerPostCustomersEnrolcustomerRequestBody</code>.
   * @alias module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBody
   * @param senderReference {String} Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected
   * @param reference {String} Unique reference string identifying this particular enrolment
   * @param marketingOptOutFl {Boolean} A boolean flag indicating whether the customer would like to opt out of any marketing communications
   * @param contacts {Array.<module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts>} Customer contacts' detail's array. One of the contacts must be primary contact and must be present in the request.
   * @param supplyAddress {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress} 
   * @param electricityProduct {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct} 
   * @param gasProduct {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct} 
   */
  function EnrolCustomerPostCustomersEnrolcustomerRequestBody(senderReference, reference, marketingOptOutFl, contacts, supplyAddress, electricityProduct, gasProduct) {
    _classCallCheck(this, EnrolCustomerPostCustomersEnrolcustomerRequestBody);

    EnrolCustomerPostCustomersEnrolcustomerRequestBody.initialize(this, senderReference, reference, marketingOptOutFl, contacts, supplyAddress, electricityProduct, gasProduct);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolCustomerPostCustomersEnrolcustomerRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, senderReference, reference, marketingOptOutFl, contacts, supplyAddress, electricityProduct, gasProduct) {
      obj['senderReference'] = senderReference;
      obj['reference'] = reference;
      obj['marketingOptOutFl'] = marketingOptOutFl;
      obj['contacts'] = contacts;
      obj['supplyAddress'] = supplyAddress;
      obj['electricityProduct'] = electricityProduct;
      obj['gasProduct'] = gasProduct;
    }
    /**
     * Constructs a <code>EnrolCustomerPostCustomersEnrolcustomerRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBody} obj Optional instance to populate.
     * @return {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBody} The populated <code>EnrolCustomerPostCustomersEnrolcustomerRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolCustomerPostCustomersEnrolcustomerRequestBody();

        if (data.hasOwnProperty('senderReference')) {
          obj['senderReference'] = _ApiClient["default"].convertToType(data['senderReference'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('billingEntityCode')) {
          obj['billingEntityCode'] = _ApiClient["default"].convertToType(data['billingEntityCode'], 'String');
        }

        if (data.hasOwnProperty('marketingOptOutFl')) {
          obj['marketingOptOutFl'] = _ApiClient["default"].convertToType(data['marketingOptOutFl'], 'Boolean');
        }

        if (data.hasOwnProperty('submittedSource')) {
          obj['submittedSource'] = _ApiClient["default"].convertToType(data['submittedSource'], 'String');
        }

        if (data.hasOwnProperty('changeOfTenancyFl')) {
          obj['changeOfTenancyFl'] = _ApiClient["default"].convertToType(data['changeOfTenancyFl'], 'Boolean');
        }

        if (data.hasOwnProperty('contacts')) {
          obj['contacts'] = _ApiClient["default"].convertToType(data['contacts'], [_EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts["default"]]);
        }

        if (data.hasOwnProperty('supplyAddress')) {
          obj['supplyAddress'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress["default"].constructFromObject(data['supplyAddress']);
        }

        if (data.hasOwnProperty('directDebitInfo')) {
          obj['directDebitInfo'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo["default"].constructFromObject(data['directDebitInfo']);
        }

        if (data.hasOwnProperty('electricityProduct')) {
          obj['electricityProduct'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct["default"].constructFromObject(data['electricityProduct']);
        }

        if (data.hasOwnProperty('gasProduct')) {
          obj['gasProduct'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct["default"].constructFromObject(data['gasProduct']);
        }

        if (data.hasOwnProperty('gasEstimate')) {
          obj['gasEstimate'] = _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate["default"].constructFromObject(data['gasEstimate']);
        }

        if (data.hasOwnProperty('electricityEstimate')) {
          obj['electricityEstimate'] = _EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate["default"].constructFromObject(data['electricityEstimate']);
        }
      }

      return obj;
    }
  }]);

  return EnrolCustomerPostCustomersEnrolcustomerRequestBody;
}();
/**
 * Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected
 * @member {String} senderReference
 */


EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['senderReference'] = undefined;
/**
 * Unique reference string identifying this particular enrolment
 * @member {String} reference
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['reference'] = undefined;
/**
 * Code of the billing entity to which the customer must be attached
 * @member {String} billingEntityCode
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['billingEntityCode'] = undefined;
/**
 * A boolean flag indicating whether the customer would like to opt out of any marketing communications
 * @member {Boolean} marketingOptOutFl
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['marketingOptOutFl'] = undefined;
/**
 * The source of the enrolment. This overrides the default source (WebService) if set
 * @member {String} submittedSource
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['submittedSource'] = undefined;
/**
 * A boolean flag indicating whether the customer is a change of tenancy.
 * @member {Boolean} changeOfTenancyFl
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['changeOfTenancyFl'] = undefined;
/**
 * Customer contacts' detail's array. One of the contacts must be primary contact and must be present in the request.
 * @member {Array.<module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts>} contacts
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['contacts'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress} supplyAddress
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['supplyAddress'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo} directDebitInfo
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['directDebitInfo'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct} electricityProduct
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['electricityProduct'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct} gasProduct
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['gasProduct'] = undefined;
/**
 * @member {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate} gasEstimate
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['gasEstimate'] = undefined;
/**
 * @member {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate} electricityEstimate
 */

EnrolCustomerPostCustomersEnrolcustomerRequestBody.prototype['electricityEstimate'] = undefined;
var _default = EnrolCustomerPostCustomersEnrolcustomerRequestBody;
exports["default"] = _default;