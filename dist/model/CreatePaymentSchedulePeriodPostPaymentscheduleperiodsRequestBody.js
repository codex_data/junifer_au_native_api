"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody model module.
 * @module model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody
 * @version 1.61.1
 */
var CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody</code>.
   * @alias module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody
   * @param fromDt {Date} Date when the payment schedule period should start
   * @param frequency {String} The name of the frequency indicating when a payment should be collected, e.g. `Monthly, Weekly, Daily`
   * @param frequencyMultiple {Number} Frequency multiplier
   * @param frequencyAlignmentDt {Date} Date to align/pivot the payments
   * @param seasonalPaymentFl {Boolean} Flag indicating if the created payment schedule should be seasonal
   * @param amount {Number} Decimal amount which should customer should be charged (2 dp)
   */
  function CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody(fromDt, frequency, frequencyMultiple, frequencyAlignmentDt, seasonalPaymentFl, amount) {
    _classCallCheck(this, CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody);

    CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.initialize(this, fromDt, frequency, frequencyMultiple, frequencyAlignmentDt, seasonalPaymentFl, amount);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, fromDt, frequency, frequencyMultiple, frequencyAlignmentDt, seasonalPaymentFl, amount) {
      obj['fromDt'] = fromDt;
      obj['frequency'] = frequency;
      obj['frequencyMultiple'] = frequencyMultiple;
      obj['frequencyAlignmentDt'] = frequencyAlignmentDt;
      obj['seasonalPaymentFl'] = seasonalPaymentFl;
      obj['amount'] = amount;
    }
    /**
     * Constructs a <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody} obj Optional instance to populate.
     * @return {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody} The populated <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody();

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }

        if (data.hasOwnProperty('frequency')) {
          obj['frequency'] = _ApiClient["default"].convertToType(data['frequency'], 'String');
        }

        if (data.hasOwnProperty('frequencyMultiple')) {
          obj['frequencyMultiple'] = _ApiClient["default"].convertToType(data['frequencyMultiple'], 'Number');
        }

        if (data.hasOwnProperty('frequencyAlignmentDt')) {
          obj['frequencyAlignmentDt'] = _ApiClient["default"].convertToType(data['frequencyAlignmentDt'], 'Date');
        }

        if (data.hasOwnProperty('seasonalPaymentFl')) {
          obj['seasonalPaymentFl'] = _ApiClient["default"].convertToType(data['seasonalPaymentFl'], 'Boolean');
        }

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody;
}();
/**
 * Date when the payment schedule period should start
 * @member {Date} fromDt
 */


CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.prototype['fromDt'] = undefined;
/**
 * Date when the payment schedule period should terminate
 * @member {Date} toDt
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.prototype['toDt'] = undefined;
/**
 * The name of the frequency indicating when a payment should be collected, e.g. `Monthly, Weekly, Daily`
 * @member {String} frequency
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.prototype['frequency'] = undefined;
/**
 * Frequency multiplier
 * @member {Number} frequencyMultiple
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.prototype['frequencyMultiple'] = undefined;
/**
 * Date to align/pivot the payments
 * @member {Date} frequencyAlignmentDt
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.prototype['frequencyAlignmentDt'] = undefined;
/**
 * Flag indicating if the created payment schedule should be seasonal
 * @member {Boolean} seasonalPaymentFl
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.prototype['seasonalPaymentFl'] = undefined;
/**
 * Decimal amount which should customer should be charged (2 dp)
 * @member {Number} amount
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.prototype['amount'] = undefined;
var _default = CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody;
exports["default"] = _default;