"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _BillEmailsGetBillemailsIdResponse200Links = _interopRequireDefault(require("./BillEmailsGetBillemailsIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The BillEmailsGetBillemailsIdResponse200Files model module.
 * @module model/BillEmailsGetBillemailsIdResponse200Files
 * @version 1.61.1
 */
var BillEmailsGetBillemailsIdResponse200Files = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>BillEmailsGetBillemailsIdResponse200Files</code>.
   * @alias module:model/BillEmailsGetBillemailsIdResponse200Files
   * @param id {Number} Attachment ID
   * @param links {module:model/BillEmailsGetBillemailsIdResponse200Links} 
   */
  function BillEmailsGetBillemailsIdResponse200Files(id, links) {
    _classCallCheck(this, BillEmailsGetBillemailsIdResponse200Files);

    BillEmailsGetBillemailsIdResponse200Files.initialize(this, id, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(BillEmailsGetBillemailsIdResponse200Files, null, [{
    key: "initialize",
    value: function initialize(obj, id, links) {
      obj['id'] = id;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>BillEmailsGetBillemailsIdResponse200Files</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/BillEmailsGetBillemailsIdResponse200Files} obj Optional instance to populate.
     * @return {module:model/BillEmailsGetBillemailsIdResponse200Files} The populated <code>BillEmailsGetBillemailsIdResponse200Files</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new BillEmailsGetBillemailsIdResponse200Files();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _BillEmailsGetBillemailsIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return BillEmailsGetBillemailsIdResponse200Files;
}();
/**
 * Attachment ID
 * @member {Number} id
 */


BillEmailsGetBillemailsIdResponse200Files.prototype['id'] = undefined;
/**
 * @member {module:model/BillEmailsGetBillemailsIdResponse200Links} links
 */

BillEmailsGetBillemailsIdResponse200Files.prototype['links'] = undefined;
var _default = BillEmailsGetBillemailsIdResponse200Files;
exports["default"] = _default;