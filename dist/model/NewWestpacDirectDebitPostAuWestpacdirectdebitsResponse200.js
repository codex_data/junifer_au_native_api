"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200 model module.
 * @module model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200
 * @version 1.61.1
 */
var NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200</code>.
   * @alias module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200
   * @param id {Number} Westpac Direct Debit object id
   * @param accountName {String} Account name
   * @param paymentType {String} Payment type. Can be either `BankAccount` or `CreditCard`
   * @param status {String} Current status of the Direct Debit
   */
  function NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200(id, accountName, paymentType, status) {
    _classCallCheck(this, NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200);

    NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.initialize(this, id, accountName, paymentType, status);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, accountName, paymentType, status) {
      obj['id'] = id;
      obj['accountName'] = accountName;
      obj['paymentType'] = paymentType;
      obj['status'] = status;
    }
    /**
     * Constructs a <code>NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200} obj Optional instance to populate.
     * @return {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200} The populated <code>NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('accountName')) {
          obj['accountName'] = _ApiClient["default"].convertToType(data['accountName'], 'String');
        }

        if (data.hasOwnProperty('mandateReference')) {
          obj['mandateReference'] = _ApiClient["default"].convertToType(data['mandateReference'], 'String');
        }

        if (data.hasOwnProperty('bankAccountNumber')) {
          obj['bankAccountNumber'] = _ApiClient["default"].convertToType(data['bankAccountNumber'], 'String');
        }

        if (data.hasOwnProperty('bsbNumber')) {
          obj['bsbNumber'] = _ApiClient["default"].convertToType(data['bsbNumber'], 'String');
        }

        if (data.hasOwnProperty('customerNumber')) {
          obj['customerNumber'] = _ApiClient["default"].convertToType(data['customerNumber'], 'String');
        }

        if (data.hasOwnProperty('paymentType')) {
          obj['paymentType'] = _ApiClient["default"].convertToType(data['paymentType'], 'String');
        }

        if (data.hasOwnProperty('cardType')) {
          obj['cardType'] = _ApiClient["default"].convertToType(data['cardType'], 'String');
        }

        if (data.hasOwnProperty('authorisedDttm')) {
          obj['authorisedDttm'] = _ApiClient["default"].convertToType(data['authorisedDttm'], 'Date');
        }

        if (data.hasOwnProperty('terminatedDttm')) {
          obj['terminatedDttm'] = _ApiClient["default"].convertToType(data['terminatedDttm'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }
      }

      return obj;
    }
  }]);

  return NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200;
}();
/**
 * Westpac Direct Debit object id
 * @member {Number} id
 */


NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['id'] = undefined;
/**
 * Account name
 * @member {String} accountName
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['accountName'] = undefined;
/**
 * Mandate Reference
 * @member {String} mandateReference
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['mandateReference'] = undefined;
/**
 * Bank account number
 * @member {String} bankAccountNumber
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['bankAccountNumber'] = undefined;
/**
 * BSB number
 * @member {String} bsbNumber
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['bsbNumber'] = undefined;
/**
 * Customer's unique ID
 * @member {String} customerNumber
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['customerNumber'] = undefined;
/**
 * Payment type. Can be either `BankAccount` or `CreditCard`
 * @member {String} paymentType
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['paymentType'] = undefined;
/**
 * Credit card type.
 * @member {String} cardType
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['cardType'] = undefined;
/**
 * Date when the Direct Debit was authorised
 * @member {Date} authorisedDttm
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['authorisedDttm'] = undefined;
/**
 * Date when the Direct Debit was terminated
 * @member {Date} terminatedDttm
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['terminatedDttm'] = undefined;
/**
 * Current status of the Direct Debit
 * @member {String} status
 */

NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.prototype['status'] = undefined;
var _default = NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200;
exports["default"] = _default;