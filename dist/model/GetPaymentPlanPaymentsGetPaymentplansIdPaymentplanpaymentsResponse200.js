"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200 model module.
 * @module model/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200
 * @version 1.61.1
 */
var GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200</code>.
   * @alias module:model/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200
   * @param id {Number} PaymentPlan payment ID
   * @param scheduledDt {Date} Date when the payment is due to collect
   * @param status {String} The status of payments
   * @param scheduledAmount {Number} the amount to collect for the payment
   * @param balance {Number} The balance of the corresonding payment, i.e. the paidAmount - scheduledAmount
   */
  function GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200(id, scheduledDt, status, scheduledAmount, balance) {
    _classCallCheck(this, GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200);

    GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200.initialize(this, id, scheduledDt, status, scheduledAmount, balance);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, scheduledDt, status, scheduledAmount, balance) {
      obj['id'] = id;
      obj['scheduledDt'] = scheduledDt;
      obj['status'] = status;
      obj['scheduledAmount'] = scheduledAmount;
      obj['balance'] = balance;
    }
    /**
     * Constructs a <code>GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200} obj Optional instance to populate.
     * @return {module:model/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200} The populated <code>GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('scheduledDt')) {
          obj['scheduledDt'] = _ApiClient["default"].convertToType(data['scheduledDt'], 'Date');
        }

        if (data.hasOwnProperty('status')) {
          obj['status'] = _ApiClient["default"].convertToType(data['status'], 'String');
        }

        if (data.hasOwnProperty('scheduledAmount')) {
          obj['scheduledAmount'] = _ApiClient["default"].convertToType(data['scheduledAmount'], 'Number');
        }

        if (data.hasOwnProperty('balance')) {
          obj['balance'] = _ApiClient["default"].convertToType(data['balance'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200;
}();
/**
 * PaymentPlan payment ID
 * @member {Number} id
 */


GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200.prototype['id'] = undefined;
/**
 * Date when the payment is due to collect
 * @member {Date} scheduledDt
 */

GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200.prototype['scheduledDt'] = undefined;
/**
 * The status of payments
 * @member {String} status
 */

GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200.prototype['status'] = undefined;
/**
 * the amount to collect for the payment
 * @member {Number} scheduledAmount
 */

GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200.prototype['scheduledAmount'] = undefined;
/**
 * The balance of the corresonding payment, i.e. the paidAmount - scheduledAmount
 * @member {Number} balance
 */

GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200.prototype['balance'] = undefined;
var _default = GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200;
exports["default"] = _default;