"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusCreateConcessionPostAuConcessionsCreateconcessionResponse200 model module.
 * @module model/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200
 * @version 1.61.1
 */
var AusCreateConcessionPostAuConcessionsCreateconcessionResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusCreateConcessionPostAuConcessionsCreateconcessionResponse200</code>.
   * @alias module:model/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200
   * @param concessionId {Number} Newly created concession ID
   */
  function AusCreateConcessionPostAuConcessionsCreateconcessionResponse200(concessionId) {
    _classCallCheck(this, AusCreateConcessionPostAuConcessionsCreateconcessionResponse200);

    AusCreateConcessionPostAuConcessionsCreateconcessionResponse200.initialize(this, concessionId);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusCreateConcessionPostAuConcessionsCreateconcessionResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, concessionId) {
      obj['concessionId'] = concessionId;
    }
    /**
     * Constructs a <code>AusCreateConcessionPostAuConcessionsCreateconcessionResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200} obj Optional instance to populate.
     * @return {module:model/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200} The populated <code>AusCreateConcessionPostAuConcessionsCreateconcessionResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusCreateConcessionPostAuConcessionsCreateconcessionResponse200();

        if (data.hasOwnProperty('concessionId')) {
          obj['concessionId'] = _ApiClient["default"].convertToType(data['concessionId'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return AusCreateConcessionPostAuConcessionsCreateconcessionResponse200;
}();
/**
 * Newly created concession ID
 * @member {Number} concessionId
 */


AusCreateConcessionPostAuConcessionsCreateconcessionResponse200.prototype['concessionId'] = undefined;
var _default = AusCreateConcessionPostAuConcessionsCreateconcessionResponse200;
exports["default"] = _default;