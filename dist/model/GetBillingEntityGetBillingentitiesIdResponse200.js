"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetBillingEntityGetBillingentitiesIdResponse200Links = _interopRequireDefault(require("./GetBillingEntityGetBillingentitiesIdResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillingEntityGetBillingentitiesIdResponse200 model module.
 * @module model/GetBillingEntityGetBillingentitiesIdResponse200
 * @version 1.61.1
 */
var GetBillingEntityGetBillingentitiesIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillingEntityGetBillingentitiesIdResponse200</code>.
   * @alias module:model/GetBillingEntityGetBillingentitiesIdResponse200
   * @param id {Number} Billing entity ID
   * @param name {String} Display name
   * @param code {String} Identification code. Used in other API calls for product lookup, enrolment, etc.
   * @param creditNotesForBbfBillingFl {String} Generate Credit Notes For Brought Forward Billing Accounts flag
   * @param customerFl {String} Whether the 'Customer' permission is enabled
   * @param productFl {String} Whether the 'Product' permission is enabled
   * @param tariffFl {String} Whether the 'Tariff' permission is enabled
   * @param prospectFl {String} Whether the 'Prospect' permission is enabled
   * @param reference {String} Internal reference
   * @param links {module:model/GetBillingEntityGetBillingentitiesIdResponse200Links} 
   */
  function GetBillingEntityGetBillingentitiesIdResponse200(id, name, code, creditNotesForBbfBillingFl, customerFl, productFl, tariffFl, prospectFl, reference, links) {
    _classCallCheck(this, GetBillingEntityGetBillingentitiesIdResponse200);

    GetBillingEntityGetBillingentitiesIdResponse200.initialize(this, id, name, code, creditNotesForBbfBillingFl, customerFl, productFl, tariffFl, prospectFl, reference, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillingEntityGetBillingentitiesIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, name, code, creditNotesForBbfBillingFl, customerFl, productFl, tariffFl, prospectFl, reference, links) {
      obj['id'] = id;
      obj['name'] = name;
      obj['code'] = code;
      obj['creditNotesForBbfBillingFl'] = creditNotesForBbfBillingFl;
      obj['customerFl'] = customerFl;
      obj['productFl'] = productFl;
      obj['tariffFl'] = tariffFl;
      obj['prospectFl'] = prospectFl;
      obj['reference'] = reference;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetBillingEntityGetBillingentitiesIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillingEntityGetBillingentitiesIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetBillingEntityGetBillingentitiesIdResponse200} The populated <code>GetBillingEntityGetBillingentitiesIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillingEntityGetBillingentitiesIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('parentBillingEntity')) {
          obj['parentBillingEntity'] = _ApiClient["default"].convertToType(data['parentBillingEntity'], 'String');
        }

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('code')) {
          obj['code'] = _ApiClient["default"].convertToType(data['code'], 'String');
        }

        if (data.hasOwnProperty('currency')) {
          obj['currency'] = _ApiClient["default"].convertToType(data['currency'], 'String');
        }

        if (data.hasOwnProperty('dunningSelectorComponent')) {
          obj['dunningSelectorComponent'] = _ApiClient["default"].convertToType(data['dunningSelectorComponent'], 'String');
        }

        if (data.hasOwnProperty('numberGeneratorComponent')) {
          obj['numberGeneratorComponent'] = _ApiClient["default"].convertToType(data['numberGeneratorComponent'], 'String');
        }

        if (data.hasOwnProperty('txnAllocatorComponent')) {
          obj['txnAllocatorComponent'] = _ApiClient["default"].convertToType(data['txnAllocatorComponent'], 'String');
        }

        if (data.hasOwnProperty('mailServer')) {
          obj['mailServer'] = _ApiClient["default"].convertToType(data['mailServer'], 'String');
        }

        if (data.hasOwnProperty('creditNotesForBbfBillingFl')) {
          obj['creditNotesForBbfBillingFl'] = _ApiClient["default"].convertToType(data['creditNotesForBbfBillingFl'], 'String');
        }

        if (data.hasOwnProperty('customerFl')) {
          obj['customerFl'] = _ApiClient["default"].convertToType(data['customerFl'], 'String');
        }

        if (data.hasOwnProperty('productFl')) {
          obj['productFl'] = _ApiClient["default"].convertToType(data['productFl'], 'String');
        }

        if (data.hasOwnProperty('tariffFl')) {
          obj['tariffFl'] = _ApiClient["default"].convertToType(data['tariffFl'], 'String');
        }

        if (data.hasOwnProperty('prospectFl')) {
          obj['prospectFl'] = _ApiClient["default"].convertToType(data['prospectFl'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetBillingEntityGetBillingentitiesIdResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetBillingEntityGetBillingentitiesIdResponse200;
}();
/**
 * Billing entity ID
 * @member {Number} id
 */


GetBillingEntityGetBillingentitiesIdResponse200.prototype['id'] = undefined;
/**
 * Parent billing entity
 * @member {String} parentBillingEntity
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['parentBillingEntity'] = undefined;
/**
 * Display name
 * @member {String} name
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['name'] = undefined;
/**
 * Identification code. Used in other API calls for product lookup, enrolment, etc.
 * @member {String} code
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['code'] = undefined;
/**
 * Default currency
 * @member {String} currency
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['currency'] = undefined;
/**
 * Dunning package selector
 * @member {String} dunningSelectorComponent
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['dunningSelectorComponent'] = undefined;
/**
 * Customer/account number generator
 * @member {String} numberGeneratorComponent
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['numberGeneratorComponent'] = undefined;
/**
 * Transaction allocator
 * @member {String} txnAllocatorComponent
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['txnAllocatorComponent'] = undefined;
/**
 * Mail Server. An absent field means it's inherited from the parent billing entity
 * @member {String} mailServer
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['mailServer'] = undefined;
/**
 * Generate Credit Notes For Brought Forward Billing Accounts flag
 * @member {String} creditNotesForBbfBillingFl
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['creditNotesForBbfBillingFl'] = undefined;
/**
 * Whether the 'Customer' permission is enabled
 * @member {String} customerFl
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['customerFl'] = undefined;
/**
 * Whether the 'Product' permission is enabled
 * @member {String} productFl
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['productFl'] = undefined;
/**
 * Whether the 'Tariff' permission is enabled
 * @member {String} tariffFl
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['tariffFl'] = undefined;
/**
 * Whether the 'Prospect' permission is enabled
 * @member {String} prospectFl
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['prospectFl'] = undefined;
/**
 * Internal reference
 * @member {String} reference
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['reference'] = undefined;
/**
 * @member {module:model/GetBillingEntityGetBillingentitiesIdResponse200Links} links
 */

GetBillingEntityGetBillingentitiesIdResponse200.prototype['links'] = undefined;
var _default = GetBillingEntityGetBillingentitiesIdResponse200;
exports["default"] = _default;