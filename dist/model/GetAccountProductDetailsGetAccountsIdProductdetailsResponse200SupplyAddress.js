"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress model module.
 * @module model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress
 * @version 1.61.1
 */
var GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress</code>.
   * Supply Address
   * @alias module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress
   * @param address1 {String} Address 1
   * @param address2 {String} Address 2
   * @param address3 {String} Address 3
   * @param address4 {String} Address 4
   * @param address5 {String} Address 5
   * @param postcode {String} Post code
   */
  function GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress(address1, address2, address3, address4, address5, postcode) {
    _classCallCheck(this, GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress);

    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress.initialize(this, address1, address2, address3, address4, address5, postcode);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress, null, [{
    key: "initialize",
    value: function initialize(obj, address1, address2, address3, address4, address5, postcode) {
      obj['address1'] = address1;
      obj['address2'] = address2;
      obj['address3'] = address3;
      obj['address4'] = address4;
      obj['address5'] = address5;
      obj['postcode'] = postcode;
    }
    /**
     * Constructs a <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress} obj Optional instance to populate.
     * @return {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress} The populated <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress();

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress;
}();
/**
 * Address 1
 * @member {String} address1
 */


GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress.prototype['address1'] = undefined;
/**
 * Address 2
 * @member {String} address2
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress.prototype['address2'] = undefined;
/**
 * Address 3
 * @member {String} address3
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress.prototype['address3'] = undefined;
/**
 * Address 4
 * @member {String} address4
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress.prototype['address4'] = undefined;
/**
 * Address 5
 * @member {String} address5
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress.prototype['address5'] = undefined;
/**
 * Post code
 * @member {String} postcode
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress.prototype['postcode'] = undefined;
var _default = GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress;
exports["default"] = _default;