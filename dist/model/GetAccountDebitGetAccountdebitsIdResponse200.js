"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountDebitGetAccountdebitsIdResponse200 model module.
 * @module model/GetAccountDebitGetAccountdebitsIdResponse200
 * @version 1.61.1
 */
var GetAccountDebitGetAccountdebitsIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountDebitGetAccountdebitsIdResponse200</code>.
   * @alias module:model/GetAccountDebitGetAccountdebitsIdResponse200
   * @param id {Number} Account debit Identifier
   * @param createdDttm {Date} Date and time when the debit was created
   * @param acceptedDttm {Date} Date and time when the debit was accepted
   * @param grossAmount {Number} Gross amount of account debit
   * @param netAmount {Number} Net amount of account debit
   * @param salesTaxAmount {Number} Amount of sales tax for this account debit
   * @param reason {String} Name of linked account debit reason
   */
  function GetAccountDebitGetAccountdebitsIdResponse200(id, createdDttm, acceptedDttm, grossAmount, netAmount, salesTaxAmount, reason) {
    _classCallCheck(this, GetAccountDebitGetAccountdebitsIdResponse200);

    GetAccountDebitGetAccountdebitsIdResponse200.initialize(this, id, createdDttm, acceptedDttm, grossAmount, netAmount, salesTaxAmount, reason);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountDebitGetAccountdebitsIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, createdDttm, acceptedDttm, grossAmount, netAmount, salesTaxAmount, reason) {
      obj['id'] = id;
      obj['createdDttm'] = createdDttm;
      obj['acceptedDttm'] = acceptedDttm;
      obj['grossAmount'] = grossAmount;
      obj['netAmount'] = netAmount;
      obj['salesTaxAmount'] = salesTaxAmount;
      obj['reason'] = reason;
    }
    /**
     * Constructs a <code>GetAccountDebitGetAccountdebitsIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountDebitGetAccountdebitsIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountDebitGetAccountdebitsIdResponse200} The populated <code>GetAccountDebitGetAccountdebitsIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountDebitGetAccountdebitsIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('createdDttm')) {
          obj['createdDttm'] = _ApiClient["default"].convertToType(data['createdDttm'], 'Date');
        }

        if (data.hasOwnProperty('acceptedDttm')) {
          obj['acceptedDttm'] = _ApiClient["default"].convertToType(data['acceptedDttm'], 'Date');
        }

        if (data.hasOwnProperty('grossAmount')) {
          obj['grossAmount'] = _ApiClient["default"].convertToType(data['grossAmount'], 'Number');
        }

        if (data.hasOwnProperty('netAmount')) {
          obj['netAmount'] = _ApiClient["default"].convertToType(data['netAmount'], 'Number');
        }

        if (data.hasOwnProperty('salesTax')) {
          obj['salesTax'] = _ApiClient["default"].convertToType(data['salesTax'], 'String');
        }

        if (data.hasOwnProperty('salesTaxAmount')) {
          obj['salesTaxAmount'] = _ApiClient["default"].convertToType(data['salesTaxAmount'], 'Number');
        }

        if (data.hasOwnProperty('reason')) {
          obj['reason'] = _ApiClient["default"].convertToType(data['reason'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('cancelledDttm')) {
          obj['cancelledDttm'] = _ApiClient["default"].convertToType(data['cancelledDttm'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return GetAccountDebitGetAccountdebitsIdResponse200;
}();
/**
 * Account debit Identifier
 * @member {Number} id
 */


GetAccountDebitGetAccountdebitsIdResponse200.prototype['id'] = undefined;
/**
 * Date and time when the debit was created
 * @member {Date} createdDttm
 */

GetAccountDebitGetAccountdebitsIdResponse200.prototype['createdDttm'] = undefined;
/**
 * Date and time when the debit was accepted
 * @member {Date} acceptedDttm
 */

GetAccountDebitGetAccountdebitsIdResponse200.prototype['acceptedDttm'] = undefined;
/**
 * Gross amount of account debit
 * @member {Number} grossAmount
 */

GetAccountDebitGetAccountdebitsIdResponse200.prototype['grossAmount'] = undefined;
/**
 * Net amount of account debit
 * @member {Number} netAmount
 */

GetAccountDebitGetAccountdebitsIdResponse200.prototype['netAmount'] = undefined;
/**
 * Name of linked sales tax to this account debit
 * @member {String} salesTax
 */

GetAccountDebitGetAccountdebitsIdResponse200.prototype['salesTax'] = undefined;
/**
 * Amount of sales tax for this account debit
 * @member {Number} salesTaxAmount
 */

GetAccountDebitGetAccountdebitsIdResponse200.prototype['salesTaxAmount'] = undefined;
/**
 * Name of linked account debit reason
 * @member {String} reason
 */

GetAccountDebitGetAccountdebitsIdResponse200.prototype['reason'] = undefined;
/**
 * Custom text reference for account debit
 * @member {String} reference
 */

GetAccountDebitGetAccountdebitsIdResponse200.prototype['reference'] = undefined;
/**
 * Date and time when the account debit was cancelled
 * @member {Date} cancelledDttm
 */

GetAccountDebitGetAccountdebitsIdResponse200.prototype['cancelledDttm'] = undefined;
var _default = GetAccountDebitGetAccountdebitsIdResponse200;
exports["default"] = _default;