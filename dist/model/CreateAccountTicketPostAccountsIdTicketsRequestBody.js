"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateAccountTicketPostAccountsIdTicketsRequestBody model module.
 * @module model/CreateAccountTicketPostAccountsIdTicketsRequestBody
 * @version 1.61.1
 */
var CreateAccountTicketPostAccountsIdTicketsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateAccountTicketPostAccountsIdTicketsRequestBody</code>.
   * @alias module:model/CreateAccountTicketPostAccountsIdTicketsRequestBody
   * @param ticketDefinitionCode {String} Ticket definition code
   * @param ticketStepCode {String} Ticket step code to use initially
   * @param summary {String} Brief summary for the created ticket
   */
  function CreateAccountTicketPostAccountsIdTicketsRequestBody(ticketDefinitionCode, ticketStepCode, summary) {
    _classCallCheck(this, CreateAccountTicketPostAccountsIdTicketsRequestBody);

    CreateAccountTicketPostAccountsIdTicketsRequestBody.initialize(this, ticketDefinitionCode, ticketStepCode, summary);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateAccountTicketPostAccountsIdTicketsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, ticketDefinitionCode, ticketStepCode, summary) {
      obj['ticketDefinitionCode'] = ticketDefinitionCode;
      obj['ticketStepCode'] = ticketStepCode;
      obj['summary'] = summary;
    }
    /**
     * Constructs a <code>CreateAccountTicketPostAccountsIdTicketsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateAccountTicketPostAccountsIdTicketsRequestBody} obj Optional instance to populate.
     * @return {module:model/CreateAccountTicketPostAccountsIdTicketsRequestBody} The populated <code>CreateAccountTicketPostAccountsIdTicketsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateAccountTicketPostAccountsIdTicketsRequestBody();

        if (data.hasOwnProperty('ticketDefinitionCode')) {
          obj['ticketDefinitionCode'] = _ApiClient["default"].convertToType(data['ticketDefinitionCode'], 'String');
        }

        if (data.hasOwnProperty('ticketStepCode')) {
          obj['ticketStepCode'] = _ApiClient["default"].convertToType(data['ticketStepCode'], 'String');
        }

        if (data.hasOwnProperty('summary')) {
          obj['summary'] = _ApiClient["default"].convertToType(data['summary'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('parentTicketId')) {
          obj['parentTicketId'] = _ApiClient["default"].convertToType(data['parentTicketId'], 'Number');
        }

        if (data.hasOwnProperty('assignedUserTeam')) {
          obj['assignedUserTeam'] = _ApiClient["default"].convertToType(data['assignedUserTeam'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateAccountTicketPostAccountsIdTicketsRequestBody;
}();
/**
 * Ticket definition code
 * @member {String} ticketDefinitionCode
 */


CreateAccountTicketPostAccountsIdTicketsRequestBody.prototype['ticketDefinitionCode'] = undefined;
/**
 * Ticket step code to use initially
 * @member {String} ticketStepCode
 */

CreateAccountTicketPostAccountsIdTicketsRequestBody.prototype['ticketStepCode'] = undefined;
/**
 * Brief summary for the created ticket
 * @member {String} summary
 */

CreateAccountTicketPostAccountsIdTicketsRequestBody.prototype['summary'] = undefined;
/**
 * Ticket description
 * @member {String} description
 */

CreateAccountTicketPostAccountsIdTicketsRequestBody.prototype['description'] = undefined;
/**
 * Associate a ticket with a parent
 * @member {Number} parentTicketId
 */

CreateAccountTicketPostAccountsIdTicketsRequestBody.prototype['parentTicketId'] = undefined;
/**
 * Assign a team
 * @member {String} assignedUserTeam
 */

CreateAccountTicketPostAccountsIdTicketsRequestBody.prototype['assignedUserTeam'] = undefined;
var _default = CreateAccountTicketPostAccountsIdTicketsRequestBody;
exports["default"] = _default;