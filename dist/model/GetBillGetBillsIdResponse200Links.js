"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetBillGetBillsIdResponse200Links model module.
 * @module model/GetBillGetBillsIdResponse200Links
 * @version 1.61.1
 */
var GetBillGetBillsIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetBillGetBillsIdResponse200Links</code>.
   * Links to related sources
   * @alias module:model/GetBillGetBillsIdResponse200Links
   * @param self {String} Link referring to this bill
   * @param account {String} Link to the billed account
   * @param billBreakdownLines {String} Link to the associated billBreakdownLines
   */
  function GetBillGetBillsIdResponse200Links(self, account, billBreakdownLines) {
    _classCallCheck(this, GetBillGetBillsIdResponse200Links);

    GetBillGetBillsIdResponse200Links.initialize(this, self, account, billBreakdownLines);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetBillGetBillsIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, account, billBreakdownLines) {
      obj['self'] = self;
      obj['account'] = account;
      obj['billBreakdownLines'] = billBreakdownLines;
    }
    /**
     * Constructs a <code>GetBillGetBillsIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetBillGetBillsIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetBillGetBillsIdResponse200Links} The populated <code>GetBillGetBillsIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetBillGetBillsIdResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('account')) {
          obj['account'] = _ApiClient["default"].convertToType(data['account'], 'String');
        }

        if (data.hasOwnProperty('billBreakdownLines')) {
          obj['billBreakdownLines'] = _ApiClient["default"].convertToType(data['billBreakdownLines'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetBillGetBillsIdResponse200Links;
}();
/**
 * Link referring to this bill
 * @member {String} self
 */


GetBillGetBillsIdResponse200Links.prototype['self'] = undefined;
/**
 * Link to the billed account
 * @member {String} account
 */

GetBillGetBillsIdResponse200Links.prototype['account'] = undefined;
/**
 * Link to the associated billBreakdownLines
 * @member {String} billBreakdownLines
 */

GetBillGetBillsIdResponse200Links.prototype['billBreakdownLines'] = undefined;
var _default = GetBillGetBillsIdResponse200Links;
exports["default"] = _default;