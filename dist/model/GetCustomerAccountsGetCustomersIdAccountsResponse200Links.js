"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCustomerAccountsGetCustomersIdAccountsResponse200Links model module.
 * @module model/GetCustomerAccountsGetCustomersIdAccountsResponse200Links
 * @version 1.61.1
 */
var GetCustomerAccountsGetCustomersIdAccountsResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCustomerAccountsGetCustomersIdAccountsResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200Links
   * @param self {String} Link referring to this account
   * @param customer {String} Link to this account's customer record
   * @param contacts {String} Link to this account's contacts
   * @param bills {String} Link to account's bills
   * @param payments {String} Link to account's payments
   * @param paymentMethods {String} Link to account's payment methods
   * @param agreements {String} Link to account's agreements
   * @param paymentSchedulePeriods {String} Link to account's payment schedule periods
   * @param paymentSchedulesSuggestedPaymentAmount {String} Suggested payment amount for the payment schedule
   * @param tickets {String} Link to account's tickets
   * @param productDetails {String} Link to account's product details
   */
  function GetCustomerAccountsGetCustomersIdAccountsResponse200Links(self, customer, contacts, bills, payments, paymentMethods, agreements, paymentSchedulePeriods, paymentSchedulesSuggestedPaymentAmount, tickets, productDetails) {
    _classCallCheck(this, GetCustomerAccountsGetCustomersIdAccountsResponse200Links);

    GetCustomerAccountsGetCustomersIdAccountsResponse200Links.initialize(this, self, customer, contacts, bills, payments, paymentMethods, agreements, paymentSchedulePeriods, paymentSchedulesSuggestedPaymentAmount, tickets, productDetails);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCustomerAccountsGetCustomersIdAccountsResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, customer, contacts, bills, payments, paymentMethods, agreements, paymentSchedulePeriods, paymentSchedulesSuggestedPaymentAmount, tickets, productDetails) {
      obj['self'] = self;
      obj['customer'] = customer;
      obj['contacts'] = contacts;
      obj['bills'] = bills;
      obj['payments'] = payments;
      obj['paymentMethods'] = paymentMethods;
      obj['agreements'] = agreements;
      obj['paymentSchedulePeriods'] = paymentSchedulePeriods;
      obj['paymentSchedules/suggestedPaymentAmount'] = paymentSchedulesSuggestedPaymentAmount;
      obj['tickets'] = tickets;
      obj['productDetails'] = productDetails;
    }
    /**
     * Constructs a <code>GetCustomerAccountsGetCustomersIdAccountsResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200Links} The populated <code>GetCustomerAccountsGetCustomersIdAccountsResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCustomerAccountsGetCustomersIdAccountsResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('customer')) {
          obj['customer'] = _ApiClient["default"].convertToType(data['customer'], 'String');
        }

        if (data.hasOwnProperty('contacts')) {
          obj['contacts'] = _ApiClient["default"].convertToType(data['contacts'], 'String');
        }

        if (data.hasOwnProperty('bills')) {
          obj['bills'] = _ApiClient["default"].convertToType(data['bills'], 'String');
        }

        if (data.hasOwnProperty('payments')) {
          obj['payments'] = _ApiClient["default"].convertToType(data['payments'], 'String');
        }

        if (data.hasOwnProperty('paymentMethods')) {
          obj['paymentMethods'] = _ApiClient["default"].convertToType(data['paymentMethods'], 'String');
        }

        if (data.hasOwnProperty('agreements')) {
          obj['agreements'] = _ApiClient["default"].convertToType(data['agreements'], 'String');
        }

        if (data.hasOwnProperty('paymentSchedulePeriods')) {
          obj['paymentSchedulePeriods'] = _ApiClient["default"].convertToType(data['paymentSchedulePeriods'], 'String');
        }

        if (data.hasOwnProperty('paymentSchedules/suggestedPaymentAmount')) {
          obj['paymentSchedules/suggestedPaymentAmount'] = _ApiClient["default"].convertToType(data['paymentSchedules/suggestedPaymentAmount'], 'String');
        }

        if (data.hasOwnProperty('tickets')) {
          obj['tickets'] = _ApiClient["default"].convertToType(data['tickets'], 'String');
        }

        if (data.hasOwnProperty('productDetails')) {
          obj['productDetails'] = _ApiClient["default"].convertToType(data['productDetails'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetCustomerAccountsGetCustomersIdAccountsResponse200Links;
}();
/**
 * Link referring to this account
 * @member {String} self
 */


GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['self'] = undefined;
/**
 * Link to this account's customer record
 * @member {String} customer
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['customer'] = undefined;
/**
 * Link to this account's contacts
 * @member {String} contacts
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['contacts'] = undefined;
/**
 * Link to account's bills
 * @member {String} bills
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['bills'] = undefined;
/**
 * Link to account's payments
 * @member {String} payments
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['payments'] = undefined;
/**
 * Link to account's payment methods
 * @member {String} paymentMethods
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['paymentMethods'] = undefined;
/**
 * Link to account's agreements
 * @member {String} agreements
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['agreements'] = undefined;
/**
 * Link to account's payment schedule periods
 * @member {String} paymentSchedulePeriods
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['paymentSchedulePeriods'] = undefined;
/**
 * Suggested payment amount for the payment schedule
 * @member {String} paymentSchedules/suggestedPaymentAmount
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['paymentSchedules/suggestedPaymentAmount'] = undefined;
/**
 * Link to account's tickets
 * @member {String} tickets
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['tickets'] = undefined;
/**
 * Link to account's product details
 * @member {String} productDetails
 */

GetCustomerAccountsGetCustomersIdAccountsResponse200Links.prototype['productDetails'] = undefined;
var _default = GetCustomerAccountsGetCustomersIdAccountsResponse200Links;
exports["default"] = _default;