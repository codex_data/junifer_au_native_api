"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links = _interopRequireDefault(require("./GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200 model module.
 * @module model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200
 * @version 1.61.1
 */
var GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200</code>.
   * @alias module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200
   * @param frequency {String} The frequency indicating when a payment should be collected
   * @param frequencyMultiple {Number} Frequency multiplier
   * @param suggestedPaymentAmount {Number} Suggested payment amount for the selected account
   * @param links {module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links} 
   */
  function GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200(frequency, frequencyMultiple, suggestedPaymentAmount, links) {
    _classCallCheck(this, GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200);

    GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200.initialize(this, frequency, frequencyMultiple, suggestedPaymentAmount, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, frequency, frequencyMultiple, suggestedPaymentAmount, links) {
      obj['frequency'] = frequency;
      obj['frequencyMultiple'] = frequencyMultiple;
      obj['suggestedPaymentAmount'] = suggestedPaymentAmount;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200} The populated <code>GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200();

        if (data.hasOwnProperty('frequency')) {
          obj['frequency'] = _ApiClient["default"].convertToType(data['frequency'], 'String');
        }

        if (data.hasOwnProperty('frequencyMultiple')) {
          obj['frequencyMultiple'] = _ApiClient["default"].convertToType(data['frequencyMultiple'], 'Number');
        }

        if (data.hasOwnProperty('suggestedPaymentAmount')) {
          obj['suggestedPaymentAmount'] = _ApiClient["default"].convertToType(data['suggestedPaymentAmount'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200;
}();
/**
 * The frequency indicating when a payment should be collected
 * @member {String} frequency
 */


GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200.prototype['frequency'] = undefined;
/**
 * Frequency multiplier
 * @member {Number} frequencyMultiple
 */

GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200.prototype['frequencyMultiple'] = undefined;
/**
 * Suggested payment amount for the selected account
 * @member {Number} suggestedPaymentAmount
 */

GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200.prototype['suggestedPaymentAmount'] = undefined;
/**
 * @member {module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links} links
 */

GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200.prototype['links'] = undefined;
var _default = GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200;
exports["default"] = _default;