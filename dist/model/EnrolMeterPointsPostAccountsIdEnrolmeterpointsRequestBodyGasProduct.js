"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct model module.
 * @module model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct
 * @version 1.61.1
 */
var EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct</code>.
   * Gas product the customer chose
   * @alias module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct
   * @param productCode {String} Gas product code (reference in Junifer)
   * @param mprns {Array.<module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns>} List of MPRNs to associate with a new gas agreement
   */
  function EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct(productCode, mprns) {
    _classCallCheck(this, EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct);

    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct.initialize(this, productCode, mprns);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct, null, [{
    key: "initialize",
    value: function initialize(obj, productCode, mprns) {
      obj['productCode'] = productCode;
      obj['mprns'] = mprns;
    }
    /**
     * Constructs a <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct} obj Optional instance to populate.
     * @return {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct} The populated <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct();

        if (data.hasOwnProperty('previousSupplier')) {
          obj['previousSupplier'] = _ApiClient["default"].convertToType(data['previousSupplier'], 'String');
        }

        if (data.hasOwnProperty('previousTariff')) {
          obj['previousTariff'] = _ApiClient["default"].convertToType(data['previousTariff'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('productCode')) {
          obj['productCode'] = _ApiClient["default"].convertToType(data['productCode'], 'String');
        }

        if (data.hasOwnProperty('startDate')) {
          obj['startDate'] = _ApiClient["default"].convertToType(data['startDate'], 'Date');
        }

        if (data.hasOwnProperty('mprns')) {
          obj['mprns'] = _ApiClient["default"].convertToType(data['mprns'], [_EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns["default"]]);
        }
      }

      return obj;
    }
  }]);

  return EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct;
}();
/**
 * Previous supplier to the mprn
 * @member {String} previousSupplier
 */


EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct.prototype['previousSupplier'] = undefined;
/**
 * Previous Tariff of the mprn
 * @member {String} previousTariff
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct.prototype['previousTariff'] = undefined;
/**
 * External reference code
 * @member {String} reference
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct.prototype['reference'] = undefined;
/**
 * Gas product code (reference in Junifer)
 * @member {String} productCode
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct.prototype['productCode'] = undefined;
/**
 * Set the new Agreement start date and Registration date for a newly enrolled meterpoint
 * @member {Date} startDate
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct.prototype['startDate'] = undefined;
/**
 * List of MPRNs to associate with a new gas agreement
 * @member {Array.<module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns>} mprns
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct.prototype['mprns'] = undefined;
var _default = EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct;
exports["default"] = _default;