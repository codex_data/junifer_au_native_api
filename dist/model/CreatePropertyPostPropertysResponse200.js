"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreatePropertyPostPropertysResponse200 model module.
 * @module model/CreatePropertyPostPropertysResponse200
 * @version 1.61.1
 */
var CreatePropertyPostPropertysResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreatePropertyPostPropertysResponse200</code>.
   * @alias module:model/CreatePropertyPostPropertysResponse200
   * @param propertyId {Number} Newly created property ID
   */
  function CreatePropertyPostPropertysResponse200(propertyId) {
    _classCallCheck(this, CreatePropertyPostPropertysResponse200);

    CreatePropertyPostPropertysResponse200.initialize(this, propertyId);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreatePropertyPostPropertysResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, propertyId) {
      obj['propertyId'] = propertyId;
    }
    /**
     * Constructs a <code>CreatePropertyPostPropertysResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreatePropertyPostPropertysResponse200} obj Optional instance to populate.
     * @return {module:model/CreatePropertyPostPropertysResponse200} The populated <code>CreatePropertyPostPropertysResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreatePropertyPostPropertysResponse200();

        if (data.hasOwnProperty('propertyId')) {
          obj['propertyId'] = _ApiClient["default"].convertToType(data['propertyId'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return CreatePropertyPostPropertysResponse200;
}();
/**
 * Newly created property ID
 * @member {Number} propertyId
 */


CreatePropertyPostPropertysResponse200.prototype['propertyId'] = undefined;
var _default = CreatePropertyPostPropertysResponse200;
exports["default"] = _default;