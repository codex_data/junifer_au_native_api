"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail model module.
 * @module model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail
 * @version 1.61.1
 */
var GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail</code>.
   * Current consent settings for marketing via email
   * @alias module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail
   * @param consentGiven {Boolean} Consent has been given (true or false)
   */
  function GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail(consentGiven) {
    _classCallCheck(this, GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail);

    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail.initialize(this, consentGiven);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail, null, [{
    key: "initialize",
    value: function initialize(obj, consentGiven) {
      obj['consentGiven'] = consentGiven;
    }
    /**
     * Constructs a <code>GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail} obj Optional instance to populate.
     * @return {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail} The populated <code>GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail();

        if (data.hasOwnProperty('consentGiven')) {
          obj['consentGiven'] = _ApiClient["default"].convertToType(data['consentGiven'], 'Boolean');
        }

        if (data.hasOwnProperty('methodOfConsent')) {
          obj['methodOfConsent'] = _ApiClient["default"].convertToType(data['methodOfConsent'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail;
}();
/**
 * Consent has been given (true or false)
 * @member {Boolean} consentGiven
 */


GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail.prototype['consentGiven'] = undefined;
/**
 * If consent given, how it was given (see MethodOfConsent ref table for possible values)
 * @member {String} methodOfConsent
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail.prototype['methodOfConsent'] = undefined;
var _default = GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail;
exports["default"] = _default;