"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401 model module.
 * @module model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401
 * @version 1.61.1
 */
var CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401</code>.
   * @alias module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401
   * @param errorCode {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401.ErrorCodeEnum} Field: * `AmountNotAuthorised` - \"AmountNotAuthorised\"
   * @param errorSeverity {String} The error severity
   */
  function CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401(errorCode, errorSeverity) {
    _classCallCheck(this, CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401);

    CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401} obj Optional instance to populate.
     * @return {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401} The populated <code>CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401;
}();
/**
 * Field: * `AmountNotAuthorised` - \"AmountNotAuthorised\"
 * @member {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401.ErrorCodeEnum} errorCode
 */


CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401['ErrorCodeEnum'] = {
  /**
   * value: "AmountNotAuthorised"
   * @const
   */
  "AmountNotAuthorised": "AmountNotAuthorised"
};
var _default = CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401;
exports["default"] = _default;