"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCustomerGetCustomersIdResponse200Links model module.
 * @module model/GetCustomerGetCustomersIdResponse200Links
 * @version 1.61.1
 */
var GetCustomerGetCustomersIdResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCustomerGetCustomersIdResponse200Links</code>.
   * Links to related resources
   * @alias module:model/GetCustomerGetCustomersIdResponse200Links
   * @param self {String} Link referring to this customer
   * @param accounts {String} Link to customer's accounts
   * @param securityQuestions {String} Link to customers security questions and answers
   * @param billingEntities {String} Link to customer's billing entities
   * @param prospects {String} Link to account's product details
   */
  function GetCustomerGetCustomersIdResponse200Links(self, accounts, securityQuestions, billingEntities, prospects) {
    _classCallCheck(this, GetCustomerGetCustomersIdResponse200Links);

    GetCustomerGetCustomersIdResponse200Links.initialize(this, self, accounts, securityQuestions, billingEntities, prospects);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCustomerGetCustomersIdResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, accounts, securityQuestions, billingEntities, prospects) {
      obj['self'] = self;
      obj['accounts'] = accounts;
      obj['securityQuestions'] = securityQuestions;
      obj['billingEntities'] = billingEntities;
      obj['prospects'] = prospects;
    }
    /**
     * Constructs a <code>GetCustomerGetCustomersIdResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCustomerGetCustomersIdResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetCustomerGetCustomersIdResponse200Links} The populated <code>GetCustomerGetCustomersIdResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCustomerGetCustomersIdResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('accounts')) {
          obj['accounts'] = _ApiClient["default"].convertToType(data['accounts'], 'String');
        }

        if (data.hasOwnProperty('securityQuestions')) {
          obj['securityQuestions'] = _ApiClient["default"].convertToType(data['securityQuestions'], 'String');
        }

        if (data.hasOwnProperty('billingEntities')) {
          obj['billingEntities'] = _ApiClient["default"].convertToType(data['billingEntities'], 'String');
        }

        if (data.hasOwnProperty('prospects')) {
          obj['prospects'] = _ApiClient["default"].convertToType(data['prospects'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetCustomerGetCustomersIdResponse200Links;
}();
/**
 * Link referring to this customer
 * @member {String} self
 */


GetCustomerGetCustomersIdResponse200Links.prototype['self'] = undefined;
/**
 * Link to customer's accounts
 * @member {String} accounts
 */

GetCustomerGetCustomersIdResponse200Links.prototype['accounts'] = undefined;
/**
 * Link to customers security questions and answers
 * @member {String} securityQuestions
 */

GetCustomerGetCustomersIdResponse200Links.prototype['securityQuestions'] = undefined;
/**
 * Link to customer's billing entities
 * @member {String} billingEntities
 */

GetCustomerGetCustomersIdResponse200Links.prototype['billingEntities'] = undefined;
/**
 * Link to account's product details
 * @member {String} prospects
 */

GetCustomerGetCustomersIdResponse200Links.prototype['prospects'] = undefined;
var _default = GetCustomerGetCustomersIdResponse200Links;
exports["default"] = _default;