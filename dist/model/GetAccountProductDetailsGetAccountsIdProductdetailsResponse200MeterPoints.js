"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints model module.
 * @module model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints
 * @version 1.61.1
 */
var GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints</code>.
   * @alias module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints
   * @param id {String} The id for the meter point
   * @param identifier {String} Meter point identifier
   * @param type {String} The type of the meter point (e.g MPAN, MPRN...)
   */
  function GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints(id, identifier, type) {
    _classCallCheck(this, GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints);

    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints.initialize(this, id, identifier, type);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints, null, [{
    key: "initialize",
    value: function initialize(obj, id, identifier, type) {
      obj['id'] = id;
      obj['identifier'] = identifier;
      obj['type'] = type;
    }
    /**
     * Constructs a <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints} obj Optional instance to populate.
     * @return {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints} The populated <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'String');
        }

        if (data.hasOwnProperty('identifier')) {
          obj['identifier'] = _ApiClient["default"].convertToType(data['identifier'], 'String');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints;
}();
/**
 * The id for the meter point
 * @member {String} id
 */


GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints.prototype['id'] = undefined;
/**
 * Meter point identifier
 * @member {String} identifier
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints.prototype['identifier'] = undefined;
/**
 * The type of the meter point (e.g MPAN, MPRN...)
 * @member {String} type
 */

GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints.prototype['type'] = undefined;
var _default = GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints;
exports["default"] = _default;