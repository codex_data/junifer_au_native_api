"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences = _interopRequireDefault(require("./UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody model module.
 * @module model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody
 * @version 1.61.1
 */
var UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody</code>.
   * @alias module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody
   * @param methodOfConsent {String} How the contact's marketing preferences were requested (\"Phone\", \"Email\", \"Letter\", or \"Portal\")
   */
  function UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody(methodOfConsent) {
    _classCallCheck(this, UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody);

    UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody.initialize(this, methodOfConsent);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, methodOfConsent) {
      obj['methodOfConsent'] = methodOfConsent;
    }
    /**
     * Constructs a <code>UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody} obj Optional instance to populate.
     * @return {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody} The populated <code>UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody();

        if (data.hasOwnProperty('marketingMaterials')) {
          obj['marketingMaterials'] = _ApiClient["default"].convertToType(data['marketingMaterials'], [Object]);
        }

        if (data.hasOwnProperty('marketingPreferences')) {
          obj['marketingPreferences'] = _UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences["default"].constructFromObject(data['marketingPreferences']);
        }

        if (data.hasOwnProperty('methodOfConsent')) {
          obj['methodOfConsent'] = _ApiClient["default"].convertToType(data['methodOfConsent'], 'String');
        }
      }

      return obj;
    }
  }]);

  return UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody;
}();
/**
 * Array of marketing material names (see MarketingMaterial ref table for possible values)
 * @member {Array.<Object>} marketingMaterials
 */


UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody.prototype['marketingMaterials'] = undefined;
/**
 * @member {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences} marketingPreferences
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody.prototype['marketingPreferences'] = undefined;
/**
 * How the contact's marketing preferences were requested (\"Phone\", \"Email\", \"Letter\", or \"Portal\")
 * @member {String} methodOfConsent
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody.prototype['methodOfConsent'] = undefined;
var _default = UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody;
exports["default"] = _default;