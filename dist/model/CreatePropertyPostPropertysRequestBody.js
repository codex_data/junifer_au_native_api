"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreatePropertyPostPropertysRequestBody model module.
 * @module model/CreatePropertyPostPropertysRequestBody
 * @version 1.61.1
 */
var CreatePropertyPostPropertysRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreatePropertyPostPropertysRequestBody</code>.
   * @alias module:model/CreatePropertyPostPropertysRequestBody
   */
  function CreatePropertyPostPropertysRequestBody() {
    _classCallCheck(this, CreatePropertyPostPropertysRequestBody);

    CreatePropertyPostPropertysRequestBody.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreatePropertyPostPropertysRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>CreatePropertyPostPropertysRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreatePropertyPostPropertysRequestBody} obj Optional instance to populate.
     * @return {module:model/CreatePropertyPostPropertysRequestBody} The populated <code>CreatePropertyPostPropertysRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreatePropertyPostPropertysRequestBody();

        if (data.hasOwnProperty('parentId')) {
          obj['parentId'] = _ApiClient["default"].convertToType(data['parentId'], 'Number');
        }

        if (data.hasOwnProperty('rootPropertyId')) {
          obj['rootPropertyId'] = _ApiClient["default"].convertToType(data['rootPropertyId'], 'Number');
        }

        if (data.hasOwnProperty('identifier')) {
          obj['identifier'] = _ApiClient["default"].convertToType(data['identifier'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('propertyType')) {
          obj['propertyType'] = _ApiClient["default"].convertToType(data['propertyType'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreatePropertyPostPropertysRequestBody;
}();
/**
 * ID of parent property
 * @member {Number} parentId
 */


CreatePropertyPostPropertysRequestBody.prototype['parentId'] = undefined;
/**
 * ID of the root property
 * @member {Number} rootPropertyId
 */

CreatePropertyPostPropertysRequestBody.prototype['rootPropertyId'] = undefined;
/**
 * Identifier of the property
 * @member {String} identifier
 */

CreatePropertyPostPropertysRequestBody.prototype['identifier'] = undefined;
/**
 * Property Reference
 * @member {String} reference
 */

CreatePropertyPostPropertysRequestBody.prototype['reference'] = undefined;
/**
 * The type of the property. This will be one of the values in the \"Property Type\" ref table.
 * @member {String} propertyType
 */

CreatePropertyPostPropertysRequestBody.prototype['propertyType'] = undefined;
var _default = CreatePropertyPostPropertysRequestBody;
exports["default"] = _default;