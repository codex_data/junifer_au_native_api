"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody model module.
 * @module model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody
 * @version 1.61.1
 */
var UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody</code>.
   * @alias module:model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody
   * @param surname {String} Primary contact's surname
   */
  function UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody(surname) {
    _classCallCheck(this, UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody);

    UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.initialize(this, surname);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, surname) {
      obj['surname'] = surname;
    }
    /**
     * Constructs a <code>UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody} obj Optional instance to populate.
     * @return {module:model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody} The populated <code>UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody();

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('phoneNumber1')) {
          obj['phoneNumber1'] = _ApiClient["default"].convertToType(data['phoneNumber1'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber2')) {
          obj['phoneNumber2'] = _ApiClient["default"].convertToType(data['phoneNumber2'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber3')) {
          obj['phoneNumber3'] = _ApiClient["default"].convertToType(data['phoneNumber3'], 'String');
        }

        if (data.hasOwnProperty('careOfField')) {
          obj['careOfField'] = _ApiClient["default"].convertToType(data['careOfField'], 'String');
        }
      }

      return obj;
    }
  }]);

  return UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody;
}();
/**
 * Primary Contact's title. Can be `Dr, Miss, Mr, Mrs, Ms, Prof`
 * @member {String} title
 */


UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.prototype['title'] = undefined;
/**
 * Primary contact's first name
 * @member {String} forename
 */

UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.prototype['forename'] = undefined;
/**
 * Primary contact's surname
 * @member {String} surname
 */

UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.prototype['surname'] = undefined;
/**
 * Email address
 * @member {String} email
 */

UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.prototype['email'] = undefined;
/**
 * Primary contact's date of birth
 * @member {Date} dateOfBirth
 */

UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.prototype['dateOfBirth'] = undefined;
/**
 * Phone number 1
 * @member {String} phoneNumber1
 */

UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.prototype['phoneNumber1'] = undefined;
/**
 * Phone number 2
 * @member {String} phoneNumber2
 */

UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.prototype['phoneNumber2'] = undefined;
/**
 * Phone number 3
 * @member {String} phoneNumber3
 */

UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.prototype['phoneNumber3'] = undefined;
/**
 * care of name
 * @member {String} careOfField
 */

UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.prototype['careOfField'] = undefined;
var _default = UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody;
exports["default"] = _default;