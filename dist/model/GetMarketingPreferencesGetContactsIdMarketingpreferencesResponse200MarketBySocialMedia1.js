"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1 model module.
 * @module model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1
 * @version 1.61.1
 */
var GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1</code>.
   * Current consent settings for marketing via social media type 1
   * @alias module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1
   * @param label {String} The social media type 1 label name for the contact type (see ContactType ref table for possible values)
   * @param consentGiven {Boolean} Consent has been given (true or false)
   */
  function GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1(label, consentGiven) {
    _classCallCheck(this, GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1);

    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1.initialize(this, label, consentGiven);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1, null, [{
    key: "initialize",
    value: function initialize(obj, label, consentGiven) {
      obj['label'] = label;
      obj['consentGiven'] = consentGiven;
    }
    /**
     * Constructs a <code>GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1} obj Optional instance to populate.
     * @return {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1} The populated <code>GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1();

        if (data.hasOwnProperty('label')) {
          obj['label'] = _ApiClient["default"].convertToType(data['label'], 'String');
        }

        if (data.hasOwnProperty('consentGiven')) {
          obj['consentGiven'] = _ApiClient["default"].convertToType(data['consentGiven'], 'Boolean');
        }

        if (data.hasOwnProperty('methodOfConsent')) {
          obj['methodOfConsent'] = _ApiClient["default"].convertToType(data['methodOfConsent'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1;
}();
/**
 * The social media type 1 label name for the contact type (see ContactType ref table for possible values)
 * @member {String} label
 */


GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1.prototype['label'] = undefined;
/**
 * Consent has been given (true or false)
 * @member {Boolean} consentGiven
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1.prototype['consentGiven'] = undefined;
/**
 * If consent given, how it was given (see MethodOfConsent ref table for possible values)
 * @member {String} methodOfConsent
 */

GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1.prototype['methodOfConsent'] = undefined;
var _default = GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1;
exports["default"] = _default;