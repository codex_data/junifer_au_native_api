"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers model module.
 * @module model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers
 * @version 1.61.1
 */
var GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers</code>.
   * @alias module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers
   * @param id {Number} Meter register ID
   * @param identifier {String} Meter register identifier
   * @param digits {Number} The amount of digits meter register has
   * @param decimalPlaces {Number} The amount of decimal places meter register has
   */
  function GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers(id, identifier, digits, decimalPlaces) {
    _classCallCheck(this, GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers);

    GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers.initialize(this, id, identifier, digits, decimalPlaces);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers, null, [{
    key: "initialize",
    value: function initialize(obj, id, identifier, digits, decimalPlaces) {
      obj['id'] = id;
      obj['identifier'] = identifier;
      obj['digits'] = digits;
      obj['decimalPlaces'] = decimalPlaces;
    }
    /**
     * Constructs a <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers} obj Optional instance to populate.
     * @return {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers} The populated <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('identifier')) {
          obj['identifier'] = _ApiClient["default"].convertToType(data['identifier'], 'String');
        }

        if (data.hasOwnProperty('digits')) {
          obj['digits'] = _ApiClient["default"].convertToType(data['digits'], 'Number');
        }

        if (data.hasOwnProperty('decimalPlaces')) {
          obj['decimalPlaces'] = _ApiClient["default"].convertToType(data['decimalPlaces'], 'Number');
        }

        if (data.hasOwnProperty('registerType')) {
          obj['registerType'] = _ApiClient["default"].convertToType(data['registerType'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers;
}();
/**
 * Meter register ID
 * @member {Number} id
 */


GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers.prototype['id'] = undefined;
/**
 * Meter register identifier
 * @member {String} identifier
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers.prototype['identifier'] = undefined;
/**
 * The amount of digits meter register has
 * @member {Number} digits
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers.prototype['digits'] = undefined;
/**
 * The amount of decimal places meter register has
 * @member {Number} decimalPlaces
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers.prototype['decimalPlaces'] = undefined;
/**
 * Meter register type
 * @member {String} registerType
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers.prototype['registerType'] = undefined;
var _default = GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers;
exports["default"] = _default;