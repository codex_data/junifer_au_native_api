"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType = _interopRequireDefault(require("./GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200 model module.
 * @module model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200
 * @version 1.61.1
 */
var GetAccountProductDetailsGetAccountsIdProductdetailsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200</code>.
   * @alias module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200
   * @param productType {Array.<module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType>} Product types. Can be one of the following: `Electricity, Gas`
   */
  function GetAccountProductDetailsGetAccountsIdProductdetailsResponse200(productType) {
    _classCallCheck(this, GetAccountProductDetailsGetAccountsIdProductdetailsResponse200);

    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200.initialize(this, productType);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountProductDetailsGetAccountsIdProductdetailsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, productType) {
      obj['productType'] = productType;
    }
    /**
     * Constructs a <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200} obj Optional instance to populate.
     * @return {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200} The populated <code>GetAccountProductDetailsGetAccountsIdProductdetailsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountProductDetailsGetAccountsIdProductdetailsResponse200();

        if (data.hasOwnProperty('productType')) {
          obj['productType'] = _ApiClient["default"].convertToType(data['productType'], [_GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType["default"]]);
        }
      }

      return obj;
    }
  }]);

  return GetAccountProductDetailsGetAccountsIdProductdetailsResponse200;
}();
/**
 * Product types. Can be one of the following: `Electricity, Gas`
 * @member {Array.<module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType>} productType
 */


GetAccountProductDetailsGetAccountsIdProductdetailsResponse200.prototype['productType'] = undefined;
var _default = GetAccountProductDetailsGetAccountsIdProductdetailsResponse200;
exports["default"] = _default;