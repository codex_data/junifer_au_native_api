"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links = _interopRequireDefault(require("./GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links"));

var _GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters = _interopRequireDefault(require("./GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetMeterStructureGetAuMeterpointsMeterstructureResponse200 model module.
 * @module model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200
 * @version 1.61.1
 */
var GetMeterStructureGetAuMeterpointsMeterstructureResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200</code>.
   * @alias module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200
   * @param id {Number} Meterpoint id
   * @param identifier {String} Meterpoint identifier. For meterpoints of type `NMI` this will be NMI Identifier
   * @param type {String} Meterpoint type
   * @param links {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links} 
   * @param meters {Array.<module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters>} Array with meters attached to this property
   */
  function GetMeterStructureGetAuMeterpointsMeterstructureResponse200(id, identifier, type, links, meters) {
    _classCallCheck(this, GetMeterStructureGetAuMeterpointsMeterstructureResponse200);

    GetMeterStructureGetAuMeterpointsMeterstructureResponse200.initialize(this, id, identifier, type, links, meters);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetMeterStructureGetAuMeterpointsMeterstructureResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, identifier, type, links, meters) {
      obj['id'] = id;
      obj['identifier'] = identifier;
      obj['type'] = type;
      obj['links'] = links;
      obj['meters'] = meters;
    }
    /**
     * Constructs a <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200} obj Optional instance to populate.
     * @return {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200} The populated <code>GetMeterStructureGetAuMeterpointsMeterstructureResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetMeterStructureGetAuMeterpointsMeterstructureResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('identifier')) {
          obj['identifier'] = _ApiClient["default"].convertToType(data['identifier'], 'String');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links["default"].constructFromObject(data['links']);
        }

        if (data.hasOwnProperty('meters')) {
          obj['meters'] = _ApiClient["default"].convertToType(data['meters'], [_GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters["default"]]);
        }
      }

      return obj;
    }
  }]);

  return GetMeterStructureGetAuMeterpointsMeterstructureResponse200;
}();
/**
 * Meterpoint id
 * @member {Number} id
 */


GetMeterStructureGetAuMeterpointsMeterstructureResponse200.prototype['id'] = undefined;
/**
 * Meterpoint identifier. For meterpoints of type `NMI` this will be NMI Identifier
 * @member {String} identifier
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200.prototype['identifier'] = undefined;
/**
 * Meterpoint type
 * @member {String} type
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200.prototype['type'] = undefined;
/**
 * @member {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links} links
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200.prototype['links'] = undefined;
/**
 * Array with meters attached to this property
 * @member {Array.<module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters>} meters
 */

GetMeterStructureGetAuMeterpointsMeterstructureResponse200.prototype['meters'] = undefined;
var _default = GetMeterStructureGetAuMeterpointsMeterstructureResponse200;
exports["default"] = _default;