"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody model module.
 * @module model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody
 * @version 1.61.1
 */
var UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody</code>.
   * @alias module:model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody
   * @param surname {String} Primary contact's surname
   */
  function UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody(surname) {
    _classCallCheck(this, UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody);

    UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.initialize(this, surname);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, surname) {
      obj['surname'] = surname;
    }
    /**
     * Constructs a <code>UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody} obj Optional instance to populate.
     * @return {module:model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody} The populated <code>UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody();

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('forename')) {
          obj['forename'] = _ApiClient["default"].convertToType(data['forename'], 'String');
        }

        if (data.hasOwnProperty('surname')) {
          obj['surname'] = _ApiClient["default"].convertToType(data['surname'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('dateOfBirth')) {
          obj['dateOfBirth'] = _ApiClient["default"].convertToType(data['dateOfBirth'], 'Date');
        }

        if (data.hasOwnProperty('phoneNumber1')) {
          obj['phoneNumber1'] = _ApiClient["default"].convertToType(data['phoneNumber1'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber2')) {
          obj['phoneNumber2'] = _ApiClient["default"].convertToType(data['phoneNumber2'], 'String');
        }

        if (data.hasOwnProperty('phoneNumber3')) {
          obj['phoneNumber3'] = _ApiClient["default"].convertToType(data['phoneNumber3'], 'String');
        }

        if (data.hasOwnProperty('careOfField')) {
          obj['careOfField'] = _ApiClient["default"].convertToType(data['careOfField'], 'String');
        }

        if (data.hasOwnProperty('billDelivery')) {
          obj['billDelivery'] = _ApiClient["default"].convertToType(data['billDelivery'], 'String');
        }

        if (data.hasOwnProperty('receivePost')) {
          obj['receivePost'] = _ApiClient["default"].convertToType(data['receivePost'], 'Boolean');
        }

        if (data.hasOwnProperty('receiveEmail')) {
          obj['receiveEmail'] = _ApiClient["default"].convertToType(data['receiveEmail'], 'Boolean');
        }

        if (data.hasOwnProperty('receiveSMS')) {
          obj['receiveSMS'] = _ApiClient["default"].convertToType(data['receiveSMS'], 'Boolean');
        }
      }

      return obj;
    }
  }]);

  return UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody;
}();
/**
 * Primary Contact's title. Can be `Dr, Miss, Mr, Mrs, Ms, Prof`
 * @member {String} title
 */


UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['title'] = undefined;
/**
 * Primary contact's first name
 * @member {String} forename
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['forename'] = undefined;
/**
 * Primary contact's surname
 * @member {String} surname
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['surname'] = undefined;
/**
 * Email address
 * @member {String} email
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['email'] = undefined;
/**
 * Primary contact's date of birth
 * @member {Date} dateOfBirth
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['dateOfBirth'] = undefined;
/**
 * Phone number 1
 * @member {String} phoneNumber1
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['phoneNumber1'] = undefined;
/**
 * Phone number 2
 * @member {String} phoneNumber2
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['phoneNumber2'] = undefined;
/**
 * Phone number 3
 * @member {String} phoneNumber3
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['phoneNumber3'] = undefined;
/**
 * care of name
 * @member {String} careOfField
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['careOfField'] = undefined;
/**
 * Contacts Bill Delivery Preference. Can be `Email`, `Mail`, `Both`, `None`
 * @member {String} billDelivery
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['billDelivery'] = undefined;
/**
 * Contacts communication postal delivery preference.
 * @member {Boolean} receivePost
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['receivePost'] = undefined;
/**
 * Contacts communication email delivery preference.
 * @member {Boolean} receiveEmail
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['receiveEmail'] = undefined;
/**
 * Contacts communication SMS delivery preference.
 * @member {Boolean} receiveSMS
 */

UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.prototype['receiveSMS'] = undefined;
var _default = UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody;
exports["default"] = _default;