"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The StoreCardPostStripepaymentcardsResponse400 model module.
 * @module model/StoreCardPostStripepaymentcardsResponse400
 * @version 1.61.1
 */
var StoreCardPostStripepaymentcardsResponse400 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>StoreCardPostStripepaymentcardsResponse400</code>.
   * @alias module:model/StoreCardPostStripepaymentcardsResponse400
   * @param errorCode {module:model/StoreCardPostStripepaymentcardsResponse400.ErrorCodeEnum} Field: * `MissingLast4` - \"The required 'last4' field is missing\" * `AccountNotFound` - \"Account with such 'account.id' does not exist\" * `MissingCustomer` - \"The required 'customer' field is missing\" * `MissingAccountId` - \"The required 'account.id' field is missing\" * `MissingExpMonth` - \"The required 'expMonth' field is missing\" * `MissingExpYear` - \"The required 'expYear' field is missing\" * `ProvidedExpMonth` - \"The 'expMonth' field has been provided with a Payment Method ID\" * `ProvidedLast4` - \"The 'last4' field has been provided with a Payment Method ID\" * `ProvidedExpYear` - \"The 'expYear' field has been provided with a Payment Method ID\"
   * @param errorSeverity {String} The error severity
   */
  function StoreCardPostStripepaymentcardsResponse400(errorCode, errorSeverity) {
    _classCallCheck(this, StoreCardPostStripepaymentcardsResponse400);

    StoreCardPostStripepaymentcardsResponse400.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(StoreCardPostStripepaymentcardsResponse400, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>StoreCardPostStripepaymentcardsResponse400</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/StoreCardPostStripepaymentcardsResponse400} obj Optional instance to populate.
     * @return {module:model/StoreCardPostStripepaymentcardsResponse400} The populated <code>StoreCardPostStripepaymentcardsResponse400</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new StoreCardPostStripepaymentcardsResponse400();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return StoreCardPostStripepaymentcardsResponse400;
}();
/**
 * Field: * `MissingLast4` - \"The required 'last4' field is missing\" * `AccountNotFound` - \"Account with such 'account.id' does not exist\" * `MissingCustomer` - \"The required 'customer' field is missing\" * `MissingAccountId` - \"The required 'account.id' field is missing\" * `MissingExpMonth` - \"The required 'expMonth' field is missing\" * `MissingExpYear` - \"The required 'expYear' field is missing\" * `ProvidedExpMonth` - \"The 'expMonth' field has been provided with a Payment Method ID\" * `ProvidedLast4` - \"The 'last4' field has been provided with a Payment Method ID\" * `ProvidedExpYear` - \"The 'expYear' field has been provided with a Payment Method ID\"
 * @member {module:model/StoreCardPostStripepaymentcardsResponse400.ErrorCodeEnum} errorCode
 */


StoreCardPostStripepaymentcardsResponse400.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

StoreCardPostStripepaymentcardsResponse400.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

StoreCardPostStripepaymentcardsResponse400.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

StoreCardPostStripepaymentcardsResponse400['ErrorCodeEnum'] = {
  /**
   * value: "MissingLast4"
   * @const
   */
  "MissingLast4": "MissingLast4",

  /**
   * value: "AccountNotFound"
   * @const
   */
  "AccountNotFound": "AccountNotFound",

  /**
   * value: "MissingCustomer"
   * @const
   */
  "MissingCustomer": "MissingCustomer",

  /**
   * value: "MissingAccountId"
   * @const
   */
  "MissingAccountId": "MissingAccountId",

  /**
   * value: "MissingExpMonth"
   * @const
   */
  "MissingExpMonth": "MissingExpMonth",

  /**
   * value: "MissingExpYear"
   * @const
   */
  "MissingExpYear": "MissingExpYear",

  /**
   * value: "ProvidedExpMonth"
   * @const
   */
  "ProvidedExpMonth": "ProvidedExpMonth",

  /**
   * value: "ProvidedLast4"
   * @const
   */
  "ProvidedLast4": "ProvidedLast4",

  /**
   * value: "ProvidedExpYear"
   * @const
   */
  "ProvidedExpYear": "ProvidedExpYear"
};
var _default = StoreCardPostStripepaymentcardsResponse400;
exports["default"] = _default;