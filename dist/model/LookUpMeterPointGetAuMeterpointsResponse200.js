"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links = _interopRequireDefault(require("./GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The LookUpMeterPointGetAuMeterpointsResponse200 model module.
 * @module model/LookUpMeterPointGetAuMeterpointsResponse200
 * @version 1.61.1
 */
var LookUpMeterPointGetAuMeterpointsResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>LookUpMeterPointGetAuMeterpointsResponse200</code>.
   * @alias module:model/LookUpMeterPointGetAuMeterpointsResponse200
   * @param id {Number} Meterpoint id
   * @param identifier {String} Meterpoint identifier. For meterpoints of type `NMI` this will be NMI identifier.
   * @param type {String} Meterpoint type
   * @param meterPointServiceType {String} Indicates the service type for a meter point, and whether that service type accommodates automated readings
   * @param links {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links} 
   */
  function LookUpMeterPointGetAuMeterpointsResponse200(id, identifier, type, meterPointServiceType, links) {
    _classCallCheck(this, LookUpMeterPointGetAuMeterpointsResponse200);

    LookUpMeterPointGetAuMeterpointsResponse200.initialize(this, id, identifier, type, meterPointServiceType, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(LookUpMeterPointGetAuMeterpointsResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, identifier, type, meterPointServiceType, links) {
      obj['id'] = id;
      obj['identifier'] = identifier;
      obj['type'] = type;
      obj['meterPointServiceType'] = meterPointServiceType;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>LookUpMeterPointGetAuMeterpointsResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LookUpMeterPointGetAuMeterpointsResponse200} obj Optional instance to populate.
     * @return {module:model/LookUpMeterPointGetAuMeterpointsResponse200} The populated <code>LookUpMeterPointGetAuMeterpointsResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new LookUpMeterPointGetAuMeterpointsResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('identifier')) {
          obj['identifier'] = _ApiClient["default"].convertToType(data['identifier'], 'String');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('meterPointServiceType')) {
          obj['meterPointServiceType'] = _ApiClient["default"].convertToType(data['meterPointServiceType'], 'String');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return LookUpMeterPointGetAuMeterpointsResponse200;
}();
/**
 * Meterpoint id
 * @member {Number} id
 */


LookUpMeterPointGetAuMeterpointsResponse200.prototype['id'] = undefined;
/**
 * Meterpoint identifier. For meterpoints of type `NMI` this will be NMI identifier.
 * @member {String} identifier
 */

LookUpMeterPointGetAuMeterpointsResponse200.prototype['identifier'] = undefined;
/**
 * Meterpoint type
 * @member {String} type
 */

LookUpMeterPointGetAuMeterpointsResponse200.prototype['type'] = undefined;
/**
 * Indicates the service type for a meter point, and whether that service type accommodates automated readings
 * @member {String} meterPointServiceType
 */

LookUpMeterPointGetAuMeterpointsResponse200.prototype['meterPointServiceType'] = undefined;
/**
 * @member {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links} links
 */

LookUpMeterPointGetAuMeterpointsResponse200.prototype['links'] = undefined;
var _default = LookUpMeterPointGetAuMeterpointsResponse200;
exports["default"] = _default;