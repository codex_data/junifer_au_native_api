"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets = _interopRequireDefault(require("./GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountAgreementsGetAccountsIdAgreementsResponse200Products model module.
 * @module model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Products
 * @version 1.61.1
 */
var GetAccountAgreementsGetAccountsIdAgreementsResponse200Products = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200Products</code>.
   * @alias module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Products
   * @param id {Number} The ID of the product
   * @param type {String} The type of the product
   * @param internalName {String} The name of the product used internally
   * @param displayName {String} The name of the product shown to customers on communications and webportals. This can change at any time
   * @param reference {String} The product's reference code
   * @param assets {Array.<module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets>} An array of assets associated with the product. The array can be empty if an agreement is asset independent, e.g., an one-off charge.
   */
  function GetAccountAgreementsGetAccountsIdAgreementsResponse200Products(id, type, internalName, displayName, reference, assets) {
    _classCallCheck(this, GetAccountAgreementsGetAccountsIdAgreementsResponse200Products);

    GetAccountAgreementsGetAccountsIdAgreementsResponse200Products.initialize(this, id, type, internalName, displayName, reference, assets);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountAgreementsGetAccountsIdAgreementsResponse200Products, null, [{
    key: "initialize",
    value: function initialize(obj, id, type, internalName, displayName, reference, assets) {
      obj['id'] = id;
      obj['type'] = type;
      obj['internalName'] = internalName;
      obj['displayName'] = displayName;
      obj['reference'] = reference;
      obj['assets'] = assets;
    }
    /**
     * Constructs a <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200Products</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Products} obj Optional instance to populate.
     * @return {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Products} The populated <code>GetAccountAgreementsGetAccountsIdAgreementsResponse200Products</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountAgreementsGetAccountsIdAgreementsResponse200Products();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('internalName')) {
          obj['internalName'] = _ApiClient["default"].convertToType(data['internalName'], 'String');
        }

        if (data.hasOwnProperty('displayName')) {
          obj['displayName'] = _ApiClient["default"].convertToType(data['displayName'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('assets')) {
          obj['assets'] = _ApiClient["default"].convertToType(data['assets'], [_GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets["default"]]);
        }
      }

      return obj;
    }
  }]);

  return GetAccountAgreementsGetAccountsIdAgreementsResponse200Products;
}();
/**
 * The ID of the product
 * @member {Number} id
 */


GetAccountAgreementsGetAccountsIdAgreementsResponse200Products.prototype['id'] = undefined;
/**
 * The type of the product
 * @member {String} type
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200Products.prototype['type'] = undefined;
/**
 * The name of the product used internally
 * @member {String} internalName
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200Products.prototype['internalName'] = undefined;
/**
 * The name of the product shown to customers on communications and webportals. This can change at any time
 * @member {String} displayName
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200Products.prototype['displayName'] = undefined;
/**
 * The product's reference code
 * @member {String} reference
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200Products.prototype['reference'] = undefined;
/**
 * An array of assets associated with the product. The array can be empty if an agreement is asset independent, e.g., an one-off charge.
 * @member {Array.<module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets>} assets
 */

GetAccountAgreementsGetAccountsIdAgreementsResponse200Products.prototype['assets'] = undefined;
var _default = GetAccountAgreementsGetAccountsIdAgreementsResponse200Products;
exports["default"] = _default;