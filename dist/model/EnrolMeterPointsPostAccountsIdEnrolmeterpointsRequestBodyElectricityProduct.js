"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans = _interopRequireDefault(require("./EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct model module.
 * @module model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct
 * @version 1.61.1
 */
var EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct</code>.
   * Electricity product the customer chose
   * @alias module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct
   * @param productCode {String} Electricity product code (reference in Junifer)
   * @param mpans {Array.<module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans>} List of MPANs to associate with a new electricity agreement
   */
  function EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct(productCode, mpans) {
    _classCallCheck(this, EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct);

    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.initialize(this, productCode, mpans);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct, null, [{
    key: "initialize",
    value: function initialize(obj, productCode, mpans) {
      obj['productCode'] = productCode;
      obj['mpans'] = mpans;
    }
    /**
     * Constructs a <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct} obj Optional instance to populate.
     * @return {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct} The populated <code>EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct();

        if (data.hasOwnProperty('previousSupplier')) {
          obj['previousSupplier'] = _ApiClient["default"].convertToType(data['previousSupplier'], 'String');
        }

        if (data.hasOwnProperty('previousTariff')) {
          obj['previousTariff'] = _ApiClient["default"].convertToType(data['previousTariff'], 'String');
        }

        if (data.hasOwnProperty('reference')) {
          obj['reference'] = _ApiClient["default"].convertToType(data['reference'], 'String');
        }

        if (data.hasOwnProperty('newConnectionFl')) {
          obj['newConnectionFl'] = _ApiClient["default"].convertToType(data['newConnectionFl'], 'Boolean');
        }

        if (data.hasOwnProperty('productCode')) {
          obj['productCode'] = _ApiClient["default"].convertToType(data['productCode'], 'String');
        }

        if (data.hasOwnProperty('startDate')) {
          obj['startDate'] = _ApiClient["default"].convertToType(data['startDate'], 'Date');
        }

        if (data.hasOwnProperty('mpans')) {
          obj['mpans'] = _ApiClient["default"].convertToType(data['mpans'], [_EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans["default"]]);
        }
      }

      return obj;
    }
  }]);

  return EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct;
}();
/**
 * Previous supplier to the mpan
 * @member {String} previousSupplier
 */


EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.prototype['previousSupplier'] = undefined;
/**
 * Previous Tariff of the mpan
 * @member {String} previousTariff
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.prototype['previousTariff'] = undefined;
/**
 * External reference code
 * @member {String} reference
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.prototype['reference'] = undefined;
/**
 * Is this a new connection requiring a new meter to be fitted? If true then an X0003 will be sent with the AREGI if using utiliserve to handle flows.
 * @member {Boolean} newConnectionFl
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.prototype['newConnectionFl'] = undefined;
/**
 * Electricity product code (reference in Junifer)
 * @member {String} productCode
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.prototype['productCode'] = undefined;
/**
 * Set the new Agreement start date and Registration date for a newly enrolled meterpoint
 * @member {Date} startDate
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.prototype['startDate'] = undefined;
/**
 * List of MPANs to associate with a new electricity agreement
 * @member {Array.<module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans>} mpans
 */

EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.prototype['mpans'] = undefined;
var _default = EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct;
exports["default"] = _default;