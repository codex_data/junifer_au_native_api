"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetOfferGetOffersIdResponse200PaymentParams model module.
 * @module model/GetOfferGetOffersIdResponse200PaymentParams
 * @version 1.61.1
 */
var GetOfferGetOffersIdResponse200PaymentParams = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetOfferGetOffersIdResponse200PaymentParams</code>.
   * The payment parameters that are associated with the offer.
   * @alias module:model/GetOfferGetOffersIdResponse200PaymentParams
   */
  function GetOfferGetOffersIdResponse200PaymentParams() {
    _classCallCheck(this, GetOfferGetOffersIdResponse200PaymentParams);

    GetOfferGetOffersIdResponse200PaymentParams.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetOfferGetOffersIdResponse200PaymentParams, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>GetOfferGetOffersIdResponse200PaymentParams</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetOfferGetOffersIdResponse200PaymentParams} obj Optional instance to populate.
     * @return {module:model/GetOfferGetOffersIdResponse200PaymentParams} The populated <code>GetOfferGetOffersIdResponse200PaymentParams</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetOfferGetOffersIdResponse200PaymentParams();

        if (data.hasOwnProperty('paymentTerm')) {
          obj['paymentTerm'] = _ApiClient["default"].convertToType(data['paymentTerm'], 'String');
        }

        if (data.hasOwnProperty('paymentMethodType')) {
          obj['paymentMethodType'] = _ApiClient["default"].convertToType(data['paymentMethodType'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetOfferGetOffersIdResponse200PaymentParams;
}();
/**
 * The offers payment term. Examples include `Standard - 30 days`.
 * @member {String} paymentTerm
 */


GetOfferGetOffersIdResponse200PaymentParams.prototype['paymentTerm'] = undefined;
/**
 * The offers payment method type. Examples include `Direct Debit`.
 * @member {String} paymentMethodType
 */

GetOfferGetOffersIdResponse200PaymentParams.prototype['paymentMethodType'] = undefined;
var _default = GetOfferGetOffersIdResponse200PaymentParams;
exports["default"] = _default;