"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails model module.
 * @module model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails
 * @version 1.61.1
 */
var GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails</code>.
   * @alias module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails
   * @param fromDttm {Date} debt recover period from datetime
   * @param toDttm {Date} debt recover period to datetime
   * @param amount {Number} debt recover amount
   */
  function GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails(fromDttm, toDttm, amount) {
    _classCallCheck(this, GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails);

    GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails.initialize(this, fromDttm, toDttm, amount);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails, null, [{
    key: "initialize",
    value: function initialize(obj, fromDttm, toDttm, amount) {
      obj['fromDttm'] = fromDttm;
      obj['toDttm'] = toDttm;
      obj['amount'] = amount;
    }
    /**
     * Constructs a <code>GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails} obj Optional instance to populate.
     * @return {module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails} The populated <code>GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails();

        if (data.hasOwnProperty('fromDttm')) {
          obj['fromDttm'] = _ApiClient["default"].convertToType(data['fromDttm'], 'Date');
        }

        if (data.hasOwnProperty('toDttm')) {
          obj['toDttm'] = _ApiClient["default"].convertToType(data['toDttm'], 'Date');
        }

        if (data.hasOwnProperty('amount')) {
          obj['amount'] = _ApiClient["default"].convertToType(data['amount'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails;
}();
/**
 * debt recover period from datetime
 * @member {Date} fromDttm
 */


GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails.prototype['fromDttm'] = undefined;
/**
 * debt recover period to datetime
 * @member {Date} toDttm
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails.prototype['toDttm'] = undefined;
/**
 * debt recover amount
 * @member {Number} amount
 */

GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails.prototype['amount'] = undefined;
var _default = GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails;
exports["default"] = _default;