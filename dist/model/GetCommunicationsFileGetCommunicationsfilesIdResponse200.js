"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links = _interopRequireDefault(require("./GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetCommunicationsFileGetCommunicationsfilesIdResponse200 model module.
 * @module model/GetCommunicationsFileGetCommunicationsfilesIdResponse200
 * @version 1.61.1
 */
var GetCommunicationsFileGetCommunicationsfilesIdResponse200 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetCommunicationsFileGetCommunicationsfilesIdResponse200</code>.
   * @alias module:model/GetCommunicationsFileGetCommunicationsfilesIdResponse200
   * @param id {Number} Communication id
   * @param links {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links} 
   */
  function GetCommunicationsFileGetCommunicationsfilesIdResponse200(id, links) {
    _classCallCheck(this, GetCommunicationsFileGetCommunicationsfilesIdResponse200);

    GetCommunicationsFileGetCommunicationsfilesIdResponse200.initialize(this, id, links);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetCommunicationsFileGetCommunicationsfilesIdResponse200, null, [{
    key: "initialize",
    value: function initialize(obj, id, links) {
      obj['id'] = id;
      obj['links'] = links;
    }
    /**
     * Constructs a <code>GetCommunicationsFileGetCommunicationsfilesIdResponse200</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetCommunicationsFileGetCommunicationsfilesIdResponse200} obj Optional instance to populate.
     * @return {module:model/GetCommunicationsFileGetCommunicationsfilesIdResponse200} The populated <code>GetCommunicationsFileGetCommunicationsfilesIdResponse200</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetCommunicationsFileGetCommunicationsfilesIdResponse200();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('links')) {
          obj['links'] = _GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links["default"].constructFromObject(data['links']);
        }
      }

      return obj;
    }
  }]);

  return GetCommunicationsFileGetCommunicationsfilesIdResponse200;
}();
/**
 * Communication id
 * @member {Number} id
 */


GetCommunicationsFileGetCommunicationsfilesIdResponse200.prototype['id'] = undefined;
/**
 * @member {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links} links
 */

GetCommunicationsFileGetCommunicationsfilesIdResponse200.prototype['links'] = undefined;
var _default = GetCommunicationsFileGetCommunicationsfilesIdResponse200;
exports["default"] = _default;