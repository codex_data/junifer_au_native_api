"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400 model module.
 * @module model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400
 * @version 1.61.1
 */
var CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400 = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400</code>.
   * @alias module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400
   * @param errorCode {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400.ErrorCodeEnum} Field: * `RequiredParameterMissing` - The required 'accountid' field is missing * `PaymentSchedulePeriodAlreadyExists` - Could not create payment schedule period as it would overlap with existing payment schedule period id 'paymentSchedulePeriodgetId' * `InvalidFrequency` - Unrecognised value in 'frequency' field * `InvalidDates` - toDt must be after fromDt * `InvalidAmount` - The payment amount is negative * `AccountCancelled` - Account is cancelled * `AccountTerminated` - Payment schedule not permitted for terminated accounts * `NonBroughtForwardBilling` - Payment schedule not permitted for non-brought forward billing accounts * `NotAPayPoint` - Account is not a pay point * `NoActiveSeasonalDefinition` - There is currently no active seasonal definition
   * @param errorSeverity {String} The error severity
   */
  function CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400(errorCode, errorSeverity) {
    _classCallCheck(this, CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400);

    CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400.initialize(this, errorCode, errorSeverity);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400, null, [{
    key: "initialize",
    value: function initialize(obj, errorCode, errorSeverity) {
      obj['errorCode'] = errorCode;
      obj['errorSeverity'] = errorSeverity;
    }
    /**
     * Constructs a <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400} obj Optional instance to populate.
     * @return {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400} The populated <code>CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400();

        if (data.hasOwnProperty('errorCode')) {
          obj['errorCode'] = _ApiClient["default"].convertToType(data['errorCode'], 'String');
        }

        if (data.hasOwnProperty('errorDescription')) {
          obj['errorDescription'] = _ApiClient["default"].convertToType(data['errorDescription'], 'String');
        }

        if (data.hasOwnProperty('errorSeverity')) {
          obj['errorSeverity'] = _ApiClient["default"].convertToType(data['errorSeverity'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400;
}();
/**
 * Field: * `RequiredParameterMissing` - The required 'accountid' field is missing * `PaymentSchedulePeriodAlreadyExists` - Could not create payment schedule period as it would overlap with existing payment schedule period id 'paymentSchedulePeriodgetId' * `InvalidFrequency` - Unrecognised value in 'frequency' field * `InvalidDates` - toDt must be after fromDt * `InvalidAmount` - The payment amount is negative * `AccountCancelled` - Account is cancelled * `AccountTerminated` - Payment schedule not permitted for terminated accounts * `NonBroughtForwardBilling` - Payment schedule not permitted for non-brought forward billing accounts * `NotAPayPoint` - Account is not a pay point * `NoActiveSeasonalDefinition` - There is currently no active seasonal definition
 * @member {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400.ErrorCodeEnum} errorCode
 */


CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400.prototype['errorCode'] = undefined;
/**
 * The error description
 * @member {String} errorDescription
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400.prototype['errorDescription'] = undefined;
/**
 * The error severity
 * @member {String} errorSeverity
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400.prototype['errorSeverity'] = undefined;
/**
 * Allowed values for the <code>errorCode</code> property.
 * @enum {String}
 * @readonly
 */

CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400['ErrorCodeEnum'] = {
  /**
   * value: "RequiredParameterMissing"
   * @const
   */
  "RequiredParameterMissing": "RequiredParameterMissing",

  /**
   * value: "PaymentSchedulePeriodAlreadyExists"
   * @const
   */
  "PaymentSchedulePeriodAlreadyExists": "PaymentSchedulePeriodAlreadyExists",

  /**
   * value: "InvalidFrequency"
   * @const
   */
  "InvalidFrequency": "InvalidFrequency",

  /**
   * value: "InvalidDates"
   * @const
   */
  "InvalidDates": "InvalidDates",

  /**
   * value: "InvalidAmount"
   * @const
   */
  "InvalidAmount": "InvalidAmount",

  /**
   * value: "AccountCancelled"
   * @const
   */
  "AccountCancelled": "AccountCancelled",

  /**
   * value: "AccountTerminated"
   * @const
   */
  "AccountTerminated": "AccountTerminated",

  /**
   * value: "NonBroughtForwardBilling"
   * @const
   */
  "NonBroughtForwardBilling": "NonBroughtForwardBilling",

  /**
   * value: "NotAPayPoint"
   * @const
   */
  "NotAPayPoint": "NotAPayPoint",

  /**
   * value: "NoActiveSeasonalDefinition"
   * @const
   */
  "NoActiveSeasonalDefinition": "NoActiveSeasonalDefinition"
};
var _default = CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400;
exports["default"] = _default;