"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody model module.
 * @module model/SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody
 * @version 1.61.1
 */
var SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody</code>.
   * @alias module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody
   * @param meterIdentifier {String} Meter identifier
   * @param registerIdentifier {String} Meter register to which the reading belongs to. Optional if meter has only one register
   * @param readingDttm {Date} Date and time of the reading
   * @param sequenceType {String} The type of the meter reading. Can be one of the following: `Normal,First,Last`
   * @param source {String} The source of the meter reading. Depends on the configuration of the Junifer installation
   * @param quality {String} The quality of the meter reading.Depends on the configuration of the Junifer installation
   * @param cumulative {Number} The cumulative meter reading being submitted
   */
  function SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody(meterIdentifier, registerIdentifier, readingDttm, sequenceType, source, quality, cumulative) {
    _classCallCheck(this, SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody);

    SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.initialize(this, meterIdentifier, registerIdentifier, readingDttm, sequenceType, source, quality, cumulative);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, meterIdentifier, registerIdentifier, readingDttm, sequenceType, source, quality, cumulative) {
      obj['meterIdentifier'] = meterIdentifier;
      obj['registerIdentifier'] = registerIdentifier;
      obj['readingDttm'] = readingDttm;
      obj['sequenceType'] = sequenceType;
      obj['source'] = source;
      obj['quality'] = quality;
      obj['cumulative'] = cumulative;
    }
    /**
     * Constructs a <code>SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody} obj Optional instance to populate.
     * @return {module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody} The populated <code>SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody();

        if (data.hasOwnProperty('ignoreWarnings')) {
          obj['ignoreWarnings'] = _ApiClient["default"].convertToType(data['ignoreWarnings'], 'Boolean');
        }

        if (data.hasOwnProperty('submissionMode')) {
          obj['submissionMode'] = _ApiClient["default"].convertToType(data['submissionMode'], 'String');
        }

        if (data.hasOwnProperty('validateOnly')) {
          obj['validateOnly'] = _ApiClient["default"].convertToType(data['validateOnly'], 'Boolean');
        }

        if (data.hasOwnProperty('meterIdentifier')) {
          obj['meterIdentifier'] = _ApiClient["default"].convertToType(data['meterIdentifier'], 'String');
        }

        if (data.hasOwnProperty('registerIdentifier')) {
          obj['registerIdentifier'] = _ApiClient["default"].convertToType(data['registerIdentifier'], 'String');
        }

        if (data.hasOwnProperty('readingDttm')) {
          obj['readingDttm'] = _ApiClient["default"].convertToType(data['readingDttm'], 'Date');
        }

        if (data.hasOwnProperty('sequenceType')) {
          obj['sequenceType'] = _ApiClient["default"].convertToType(data['sequenceType'], 'String');
        }

        if (data.hasOwnProperty('source')) {
          obj['source'] = _ApiClient["default"].convertToType(data['source'], 'String');
        }

        if (data.hasOwnProperty('quality')) {
          obj['quality'] = _ApiClient["default"].convertToType(data['quality'], 'String');
        }

        if (data.hasOwnProperty('cumulative')) {
          obj['cumulative'] = _ApiClient["default"].convertToType(data['cumulative'], 'Number');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }
      }

      return obj;
    }
  }]);

  return SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody;
}();
/**
 * If true, don't fail the call when non-critical validation checks fail, such as null or negative consumption, or consumption outside the thresholds defined in the validation properties.Defaults to false
 * @member {Boolean} ignoreWarnings
 */


SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['ignoreWarnings'] = undefined;
/**
 * Supports different behaviour upon submission of reads. Default behaviour is `Submit`, can also be `Replace` which will create new reads or replace any duplicates.
 * @member {String} submissionMode
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['submissionMode'] = undefined;
/**
 * If true, the read will go through all the validation checks as a normal read would, but it will not be submitted to Junifer.
 * @member {Boolean} validateOnly
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['validateOnly'] = undefined;
/**
 * Meter identifier
 * @member {String} meterIdentifier
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['meterIdentifier'] = undefined;
/**
 * Meter register to which the reading belongs to. Optional if meter has only one register
 * @member {String} registerIdentifier
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['registerIdentifier'] = undefined;
/**
 * Date and time of the reading
 * @member {Date} readingDttm
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['readingDttm'] = undefined;
/**
 * The type of the meter reading. Can be one of the following: `Normal,First,Last`
 * @member {String} sequenceType
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['sequenceType'] = undefined;
/**
 * The source of the meter reading. Depends on the configuration of the Junifer installation
 * @member {String} source
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['source'] = undefined;
/**
 * The quality of the meter reading.Depends on the configuration of the Junifer installation
 * @member {String} quality
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['quality'] = undefined;
/**
 * The cumulative meter reading being submitted
 * @member {Number} cumulative
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['cumulative'] = undefined;
/**
 * Where the read is submitted from.
 * @member {String} description
 */

SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.prototype['description'] = undefined;
var _default = SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody;
exports["default"] = _default;