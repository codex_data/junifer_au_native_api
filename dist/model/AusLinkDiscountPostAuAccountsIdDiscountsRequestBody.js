"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The AusLinkDiscountPostAuAccountsIdDiscountsRequestBody model module.
 * @module model/AusLinkDiscountPostAuAccountsIdDiscountsRequestBody
 * @version 1.61.1
 */
var AusLinkDiscountPostAuAccountsIdDiscountsRequestBody = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AusLinkDiscountPostAuAccountsIdDiscountsRequestBody</code>.
   * @alias module:model/AusLinkDiscountPostAuAccountsIdDiscountsRequestBody
   * @param discountDfnId {String} account discount definition ID (required if no name given)
   * @param discountDfnName {String} account discount definition name (required if no ID given)
   * @param fromDt {Date} from date of the account discount
   */
  function AusLinkDiscountPostAuAccountsIdDiscountsRequestBody(discountDfnId, discountDfnName, fromDt) {
    _classCallCheck(this, AusLinkDiscountPostAuAccountsIdDiscountsRequestBody);

    AusLinkDiscountPostAuAccountsIdDiscountsRequestBody.initialize(this, discountDfnId, discountDfnName, fromDt);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AusLinkDiscountPostAuAccountsIdDiscountsRequestBody, null, [{
    key: "initialize",
    value: function initialize(obj, discountDfnId, discountDfnName, fromDt) {
      obj['discountDfnId'] = discountDfnId;
      obj['discountDfnName'] = discountDfnName;
      obj['fromDt'] = fromDt;
    }
    /**
     * Constructs a <code>AusLinkDiscountPostAuAccountsIdDiscountsRequestBody</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusLinkDiscountPostAuAccountsIdDiscountsRequestBody} obj Optional instance to populate.
     * @return {module:model/AusLinkDiscountPostAuAccountsIdDiscountsRequestBody} The populated <code>AusLinkDiscountPostAuAccountsIdDiscountsRequestBody</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AusLinkDiscountPostAuAccountsIdDiscountsRequestBody();

        if (data.hasOwnProperty('discountDfnId')) {
          obj['discountDfnId'] = _ApiClient["default"].convertToType(data['discountDfnId'], 'String');
        }

        if (data.hasOwnProperty('discountDfnName')) {
          obj['discountDfnName'] = _ApiClient["default"].convertToType(data['discountDfnName'], 'String');
        }

        if (data.hasOwnProperty('fromDt')) {
          obj['fromDt'] = _ApiClient["default"].convertToType(data['fromDt'], 'Date');
        }

        if (data.hasOwnProperty('toDt')) {
          obj['toDt'] = _ApiClient["default"].convertToType(data['toDt'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return AusLinkDiscountPostAuAccountsIdDiscountsRequestBody;
}();
/**
 * account discount definition ID (required if no name given)
 * @member {String} discountDfnId
 */


AusLinkDiscountPostAuAccountsIdDiscountsRequestBody.prototype['discountDfnId'] = undefined;
/**
 * account discount definition name (required if no ID given)
 * @member {String} discountDfnName
 */

AusLinkDiscountPostAuAccountsIdDiscountsRequestBody.prototype['discountDfnName'] = undefined;
/**
 * from date of the account discount
 * @member {Date} fromDt
 */

AusLinkDiscountPostAuAccountsIdDiscountsRequestBody.prototype['fromDt'] = undefined;
/**
 * to date of the account discount (open-ended if not given)
 * @member {Date} toDt
 */

AusLinkDiscountPostAuAccountsIdDiscountsRequestBody.prototype['toDt'] = undefined;
var _default = AusLinkDiscountPostAuAccountsIdDiscountsRequestBody;
exports["default"] = _default;