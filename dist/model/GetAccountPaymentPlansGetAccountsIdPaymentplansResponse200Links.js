"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links model module.
 * @module model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links
 * @version 1.61.1
 */
var GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links</code>.
   * Links to related sources
   * @alias module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links
   * @param self {String} Link referring to this payment plan
   * @param paymentMethod {String} Link to account's payment method
   * @param account {String} Link to the account that the payment plan belongs to
   * @param paymentPlanPayments {String} Link to account's payment plan payments
   */
  function GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links(self, paymentMethod, account, paymentPlanPayments) {
    _classCallCheck(this, GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links);

    GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links.initialize(this, self, paymentMethod, account, paymentPlanPayments);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links, null, [{
    key: "initialize",
    value: function initialize(obj, self, paymentMethod, account, paymentPlanPayments) {
      obj['self'] = self;
      obj['paymentMethod'] = paymentMethod;
      obj['account'] = account;
      obj['paymentPlanPayments'] = paymentPlanPayments;
    }
    /**
     * Constructs a <code>GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links} obj Optional instance to populate.
     * @return {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links} The populated <code>GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links();

        if (data.hasOwnProperty('self')) {
          obj['self'] = _ApiClient["default"].convertToType(data['self'], 'String');
        }

        if (data.hasOwnProperty('paymentMethod')) {
          obj['paymentMethod'] = _ApiClient["default"].convertToType(data['paymentMethod'], 'String');
        }

        if (data.hasOwnProperty('account')) {
          obj['account'] = _ApiClient["default"].convertToType(data['account'], 'String');
        }

        if (data.hasOwnProperty('paymentPlanPayments')) {
          obj['paymentPlanPayments'] = _ApiClient["default"].convertToType(data['paymentPlanPayments'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links;
}();
/**
 * Link referring to this payment plan
 * @member {String} self
 */


GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links.prototype['self'] = undefined;
/**
 * Link to account's payment method
 * @member {String} paymentMethod
 */

GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links.prototype['paymentMethod'] = undefined;
/**
 * Link to the account that the payment plan belongs to
 * @member {String} account
 */

GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links.prototype['account'] = undefined;
/**
 * Link to account's payment plan payments
 * @member {String} paymentPlanPayments
 */

GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links.prototype['paymentPlanPayments'] = undefined;
var _default = GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links;
exports["default"] = _default;