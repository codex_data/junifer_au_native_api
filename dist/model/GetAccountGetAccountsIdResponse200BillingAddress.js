"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The GetAccountGetAccountsIdResponse200BillingAddress model module.
 * @module model/GetAccountGetAccountsIdResponse200BillingAddress
 * @version 1.61.1
 */
var GetAccountGetAccountsIdResponse200BillingAddress = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>GetAccountGetAccountsIdResponse200BillingAddress</code>.
   * Account&#39;s billing address
   * @alias module:model/GetAccountGetAccountsIdResponse200BillingAddress
   */
  function GetAccountGetAccountsIdResponse200BillingAddress() {
    _classCallCheck(this, GetAccountGetAccountsIdResponse200BillingAddress);

    GetAccountGetAccountsIdResponse200BillingAddress.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(GetAccountGetAccountsIdResponse200BillingAddress, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>GetAccountGetAccountsIdResponse200BillingAddress</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/GetAccountGetAccountsIdResponse200BillingAddress} obj Optional instance to populate.
     * @return {module:model/GetAccountGetAccountsIdResponse200BillingAddress} The populated <code>GetAccountGetAccountsIdResponse200BillingAddress</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new GetAccountGetAccountsIdResponse200BillingAddress();

        if (data.hasOwnProperty('careOf')) {
          obj['careOf'] = _ApiClient["default"].convertToType(data['careOf'], 'String');
        }

        if (data.hasOwnProperty('address1')) {
          obj['address1'] = _ApiClient["default"].convertToType(data['address1'], 'String');
        }

        if (data.hasOwnProperty('address2')) {
          obj['address2'] = _ApiClient["default"].convertToType(data['address2'], 'String');
        }

        if (data.hasOwnProperty('address3')) {
          obj['address3'] = _ApiClient["default"].convertToType(data['address3'], 'String');
        }

        if (data.hasOwnProperty('address4')) {
          obj['address4'] = _ApiClient["default"].convertToType(data['address4'], 'String');
        }

        if (data.hasOwnProperty('address5')) {
          obj['address5'] = _ApiClient["default"].convertToType(data['address5'], 'String');
        }

        if (data.hasOwnProperty('postcode')) {
          obj['postcode'] = _ApiClient["default"].convertToType(data['postcode'], 'String');
        }
      }

      return obj;
    }
  }]);

  return GetAccountGetAccountsIdResponse200BillingAddress;
}();
/**
 * The 'Care of' line of the address
 * @member {String} careOf
 */


GetAccountGetAccountsIdResponse200BillingAddress.prototype['careOf'] = undefined;
/**
 * Address 1
 * @member {String} address1
 */

GetAccountGetAccountsIdResponse200BillingAddress.prototype['address1'] = undefined;
/**
 * Address 2
 * @member {String} address2
 */

GetAccountGetAccountsIdResponse200BillingAddress.prototype['address2'] = undefined;
/**
 * Address 3
 * @member {String} address3
 */

GetAccountGetAccountsIdResponse200BillingAddress.prototype['address3'] = undefined;
/**
 * Address 4
 * @member {String} address4
 */

GetAccountGetAccountsIdResponse200BillingAddress.prototype['address4'] = undefined;
/**
 * Address 5
 * @member {String} address5
 */

GetAccountGetAccountsIdResponse200BillingAddress.prototype['address5'] = undefined;
/**
 * Post code
 * @member {String} postcode
 */

GetAccountGetAccountsIdResponse200BillingAddress.prototype['postcode'] = undefined;
var _default = GetAccountGetAccountsIdResponse200BillingAddress;
exports["default"] = _default;