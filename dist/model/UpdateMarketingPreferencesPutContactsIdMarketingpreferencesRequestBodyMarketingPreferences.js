"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * The UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences model module.
 * @module model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences
 * @version 1.61.1
 */
var UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences</code>.
   * Marketing preference channels
   * @alias module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences
   */
  function UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences() {
    _classCallCheck(this, UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences);

    UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences} obj Optional instance to populate.
     * @return {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences} The populated <code>UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences();

        if (data.hasOwnProperty('marketByEmail')) {
          obj['marketByEmail'] = _ApiClient["default"].convertToType(data['marketByEmail'], 'Boolean');
        }

        if (data.hasOwnProperty('marketByNumber1')) {
          obj['marketByNumber1'] = _ApiClient["default"].convertToType(data['marketByNumber1'], 'Boolean');
        }

        if (data.hasOwnProperty('marketByNumber2')) {
          obj['marketByNumber2'] = _ApiClient["default"].convertToType(data['marketByNumber2'], 'Boolean');
        }

        if (data.hasOwnProperty('marketByNumber3')) {
          obj['marketByNumber3'] = _ApiClient["default"].convertToType(data['marketByNumber3'], 'Boolean');
        }

        if (data.hasOwnProperty('marketBySms')) {
          obj['marketBySms'] = _ApiClient["default"].convertToType(data['marketBySms'], 'Boolean');
        }

        if (data.hasOwnProperty('marketByPost')) {
          obj['marketByPost'] = _ApiClient["default"].convertToType(data['marketByPost'], 'Boolean');
        }

        if (data.hasOwnProperty('marketBySocialMedia1')) {
          obj['marketBySocialMedia1'] = _ApiClient["default"].convertToType(data['marketBySocialMedia1'], 'Boolean');
        }

        if (data.hasOwnProperty('marketBySocialMedia2')) {
          obj['marketBySocialMedia2'] = _ApiClient["default"].convertToType(data['marketBySocialMedia2'], 'Boolean');
        }

        if (data.hasOwnProperty('marketBySocialMedia3')) {
          obj['marketBySocialMedia3'] = _ApiClient["default"].convertToType(data['marketBySocialMedia3'], 'Boolean');
        }
      }

      return obj;
    }
  }]);

  return UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences;
}();
/**
 * Consent has been given to receive marketing via email (true or false)
 * @member {Boolean} marketByEmail
 */


UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.prototype['marketByEmail'] = undefined;
/**
 * Consent has been given to receive marketing via phone number type 1 (true or false)
 * @member {Boolean} marketByNumber1
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.prototype['marketByNumber1'] = undefined;
/**
 * Consent has been given to receive marketing via phone number type 2 (true or false)
 * @member {Boolean} marketByNumber2
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.prototype['marketByNumber2'] = undefined;
/**
 * Consent has been given to receive marketing via phone number type 3 (true or false)
 * @member {Boolean} marketByNumber3
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.prototype['marketByNumber3'] = undefined;
/**
 * Consent has been given to receive marketing via SMS (true or false)
 * @member {Boolean} marketBySms
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.prototype['marketBySms'] = undefined;
/**
 * Consent has been given to receive marketing via post (true or false)
 * @member {Boolean} marketByPost
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.prototype['marketByPost'] = undefined;
/**
 * Consent has been given to receive marketing via social media type 1 (true or false)
 * @member {Boolean} marketBySocialMedia1
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.prototype['marketBySocialMedia1'] = undefined;
/**
 * Consent has been given to receive marketing via social media type 2 (true or false)
 * @member {Boolean} marketBySocialMedia2
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.prototype['marketBySocialMedia2'] = undefined;
/**
 * Consent has been given to receive marketing via social media type 3 (true or false)
 * @member {Boolean} marketBySocialMedia3
 */

UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.prototype['marketBySocialMedia3'] = undefined;
var _default = UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences;
exports["default"] = _default;