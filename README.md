# junifer_au_native_api

JuniferAuNativeApi - JavaScript client for junifer_au_native_api
Junifer Native Web API provided by the Gentrack Cloud Integration Services

# How It Works

For how to prepare your application to call Junifer Native Web API, please refer to:
* [Read me first](https://portal.integration.gentrack.cloud/api/index.html#section/Read-me-first)
* [Sovereignty Regions](https://portal.integration.gentrack.cloud/api/index.html#section/Sovereignty-Regions)
* [Preparing your request](https://portal.integration.gentrack.cloud/api/index.html#section/Preparing-your-request)
* [Authentication](https://portal.integration.gentrack.cloud/api/index.html#section/Authentication)
* [Responses](https://portal.integration.gentrack.cloud/api/index.html#section/Responses)

## Sample of requesting contact details
The following is an example of calling get contact details API in UK region:

```bash
curl -X GET \\
    https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010 \\
    -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJZCI6IjA2NDYxODczLTFlOWMtNGVkMy1iZWZkLTU3NGY5ZmEwYjExYyIsIm9yZ0lkIj
oiQzAwMDAiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJpYXQiOjE1MzA1ODkwODcsImV4cCI6MTUzMDU5MjY4NywiaXNzIjoiaHR0cHM6Ly9hcGkuZ2VudHJhY2suaW8vIn0.WUAFYWTTEAy
Q0Bt4YXu-mxCXd-Y9ehwdZcYvcGNnyLTZH_hiJjtXWWfsx69M606pvCP6lLMT7MfK-F3E4rLO6KAlGsuz4A_7oVWQf4QNeR178GnwgmqQRw5OnLxwPbPrRa9nODngwGq8dcWejhmEYU6i
w02bvYdQBHnnsc3Kpyzw7Wdv_3jnBS4TPYS20muQOgG6KxRp9hLJM7ERLoAbsULwqdPOV8eUJJhGrq1NDuH_lA83YRDZmCWEzw96tSm3hb7y88kXs-4OvamnO1m5wFPBx69VximlS4Ltr
3ztqU2s3fHoj0OJLIafge9JvTgvuB6noHfs1uSRaahvstGJAA'
```

The sample of response is as follows.

```json
{
   \"id\": 1010,
   \"contactType\": \"Business\",
   \"title\": \"Mr\",
   \"forename\": \"Tony\",
   \"surname\": \"Soprano\",
   \"email\": \"bigtone@didntseeanything.com\",
   \"dateOfBirth\": \"1959-08-24\",
   \"phoneNumber1\": \"44626478370\",
   \"address\":{
     \"address1\": \"633 Stag Trail Road\",
     \"address2\": \"North Caldwell\",
     \"address3\": \"New Jersey\",
     \"postcode\": \"NE18 0PY\"
   },
   \"links\": {
     \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010\",
     \"accounts\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010/accounts\"
   }
}
```

# General Rules/Patterns

## Navigation
A links section is returned in most requests.

This section contains how to access the entity itself and, more importantly, how to access items related to the entity.

For example take the following response:
```json
{
  \"id\": 1,
  \"name\": \"Mr Joe Bloggs\",
  \"number\": \"00000001\",
  \"surname\": \"Bloggs\",
  \"links\": {
    \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\"
  },
  \"accounts\": [
    {
      \"id\": 7,
      \"name\": \"Invoice\",
      \"number\": \"00000001\",
      \"currency\": \"GBP\",
      \"fromDt\": \"2013-05-01\",
      \"createdDttm\": \"2013-05-08T13:36:34.000\",
      \"balance\": 0,
      \"billingAddress\": {
        \"address1\": \"11 Tuppy Street\"
      },
      \"links\": {
        \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7\",
        \"customer\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\",
        \"bills\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/bills\",
        \"payments\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/payments\"
      }
    }
  ]
}
```

The `self` URL in the `links` section can be used to access the entities listed in the response.
In the above example there are self fields for the customer itself, and the accounts under this customer.
There are other links under the account.

For example the above example contains links for the following:

- `bills` – List of Bills for the Account
- `payments` – List of Payments for the Account
- `customer` – Link back to the Account’s Customer

## About IDs

The id in the API requests (JSON payload and URL) and responses identifies the resource within the API.
It has no business meaning or purpose, therefore shouldn't be exposed to the end user in any way.

As the IDs of the resources aren't sequential or aligned with any other resource IDs, it is highly recommended using the links section from the responses to navigate to other related resources.
Given the example above, the customer with ID 1 links to the account with ID 7.
The links section will populate the correct IDs in the URLs so none of the assumptions about related resource IDs need to be made by the implementer.

# Accessing accounts by account number
Throughout this API, anywhere an account is accessed by its `id` (i.e. the URL includes `/accounts/:id`), it can also be accessed by its account number by replacing `/accounts/:id` with `accounts/accountNumber/:num`.

For example: the Create Account Credit endpoint, available at `/accounts/:id/accountCredits`, can also be accessed at `/accounts/accountNumber/:num/accountCredits`.

# Standard data types in JSON
The standard data types are as follows:

- String – Quoted as per JSON standard
- Number – Unquoted as per JSON standard
- Decimal – Unquoted as per JSON standard
- Date – In the standard ISO format \"yyyy-MM-dd\"
- DateTime – In the standard ISO format \"yyyy-MM-ddThh:mm:ss.SSS\" (no time zone component)
- Boolean – Unquoted 'true' and 'false' are the only valid values

# Ordering of data
Response representations are not ordered unless specified on a specific resource.
Therefore, clients should be implemented to use the name of fields/objects as the key for accessing data.

For example in JSON objects are comprised of key/value pairs and clients should be implemented to search on the ‘key’ value rather than looking for the nth key/value pair in the object.

# Changes of data
Response representations may change without warning in the following ways.

- Additional key/value pairs may be added to an existing object
- Additional sub-objects may be added to an existing object

These changes are considered non-breaking.

Therefore, clients should be implemented to consume only the data they need and ignore any other information.

# Versioning
The API is usually changed in each release of Junifer. Navigate to different versions by clicking the dropdown menu in the top right of this web page.

# Pagination
Some REST APIs that provide a list of results support pagination.

Pagination allows API users to query potential large data sets but only receive a sub-set of the results on the initial query. Subsequent queries can use the response of a prior query to retrieve the next or previous set of results.

Pagination functionality and behaviour will be documented on each API that requires it.

## Pagination on existing APIs
Existing APIs may have pagination support added. In the future this pagination will become mandatory.

In order to facilitate API users planning and executing upgrades against the existing APIs, pagination is optional during a transition period. API users can enable pagination on these APIs by specifying the `pagination` query parameter. This parameter only needs to be present, the value is not required or checked. If this query parameter is specified, then pagination will be enabled. Otherwise, the existing non-paginated functionality, that returns all results, will be used.

API users must plan and execute upgrades to their existing applications to use the pagination support during the transition period. At the end of the transition period pagination will become mandatory on these APIs.

For new APIs that support pagination, it will be mandatory with the initial release of the API.

## Ordering
The ordering that the API provides will be documented for each API.

An ordering is always required to ensure moving between pages is consistent.

Some APIs may define an ordering on a value or values in the response where appropriate. For example results may be ordered by a date/time property of the results.

Other APIs may not define an explicit ordering but will guarantee the order is consistent for multiple API calls.

## Limiting results of each page
The `desiredResults` query parameter can be used to specify the maximum number of results that may be returned.

The APIs treat this as a hint so API callers should expect to sometimes receive a different number of results. APIs will endeavour to ensure no more than `desiredResults` are returned but for small values of `desiredResults` (less than 10) this may not be possible. This is due to how pages of data are structured inside the product. The API will prefer to return some data rather than no data where it is not possible to return less than `desiredResults`

If `desiredResults` is not specified the default maximum value for the API will be used. Unless documented differently, this value is 100.

If `desiredResults` is specified higher than the default maximum for the API a 400 error response will be received.

## Cursors
The pagination provided by the APIs is cursor-based.

In the JSON response additional metadata will be provided when pagination is present.

- `after` cursor points to the start of the results that have been returned
- `before` cursor points to the end of the results that have been returned

These cursors can be used to make subsequent API calls. The API URL should be unchanged except for setting the `before` or `after` query parameter. If both parameters are specified, then an error 400 response will be received.

Cursor tokens must only be used for pagination purposes. API users should not infer any business meaning or logic from cursor representations. These representations may change without warning when Junifer is upgraded.

When scrolling forward the `after` cursor will be absent from the response when there are no more results after the current set.

When scrolling backwards the `before` cursor will be absent from the response when there are no more results before the current set.

In addition, a paginated response will include a `next` and `previous` URLs for convenience.

## Best practices
The following best practices must be adhered to by users of the pagination APIs:

- Don't store cursors or next and previous URLs for long. Cursors can quickly become invalid if items are added or deleted to the data set
- If an invalid cursor is used, then a 404 response will be returned in this situation. API users should design a policy to handle this situation
- API users should handle empty data sets. In limited cases a pagination query using a previously valid cursor may return no results

## Example

This example is illustrative only.

A small `desiredResults` parameter has been used to make the example concise. API users should generally choose larger values, subject to the maximum value.

The following API call would retrieve the *first* set of results.

### Request

```bash
https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?desiredResults=3 \\
    -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA'
```

### Response
```json
{
  \"results\": [
    {
      \"snapshotDateTime\": \"2019-04-20T00:00:00.000\",
      \"creditBalance\": 109,
      \"emergencyCreditBalance\": 0,
      \"accumulatedDebtRegister\": 0,
      \"timeDebtRegister1\": 0,
      \"timeDebtRegister2\": 0,
      \"paymentDebtRegister\": 0
    },
    {
      \"snapshotDateTime\": \"2019-04-19T00:00:00.000\",
      \"creditBalance\": 108,
      \"emergencyCreditBalance\": 0,
      \"accumulatedDebtRegister\": 0,
      \"timeDebtRegister1\": 0,
      \"timeDebtRegister2\": 0,
      \"paymentDebtRegister\": 0
    },
    {
      \"snapshotDateTime\": \"2019-04-18T00:00:00.000\",
      \"creditBalance\": 107,
      \"emergencyCreditBalance\": 0,
      \"accumulatedDebtRegister\": 0,
      \"timeDebtRegister1\": 0,
      \"timeDebtRegister2\": 0,
      \"paymentDebtRegister\": 0
    }
  ],
  \"pagination\": {
    \"cursors\": {
      \"after\": \"107\"
    },
    \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107\"
  }
}
```

The `results` section contains the payload of the API.

The `pagination` section contains details needed to retrieve additional results.

API users can choose to use the `next` URL directly or use the `after` cursor token to construct the URL themselves.

The API call to query the next set of results would be

### Request

```bash
https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107 \\
    -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA'
```

### Response
```json
{
  \"results\": [
    {
      \"snapshotDateTime\": \"2019-04-17T00:00:00.000\",
      \"creditBalance\": 106,
      \"emergencyCreditBalance\": 0,
      \"accumulatedDebtRegister\": 0,
      \"timeDebtRegister1\": 0,
      \"timeDebtRegister2\": 0,
      \"paymentDebtRegister\": 0
    },
    {
      \"snapshotDateTime\": \"2019-04-16T00:00:00.000\",
      \"creditBalance\": 105,
      \"emergencyCreditBalance\": 0,
      \"accumulatedDebtRegister\": 0,
      \"timeDebtRegister1\": 0,
      \"timeDebtRegister2\": 0,
      \"paymentDebtRegister\": 0
    },
    {
      \"snapshotDateTime\": \"2019-04-15T00:00:00.000\",
      \"creditBalance\": 104,
      \"emergencyCreditBalance\": 0,
      \"accumulatedDebtRegister\": 0,
      \"timeDebtRegister1\": 0,
      \"timeDebtRegister2\": 0,
      \"paymentDebtRegister\": 0
    }
  ],
  \"pagination\": {
    \"cursors\": {
      \"before\": \"106\",
      \"after\": \"104\"
    },
    \"previous\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&before=106&desiredResults=3\",
    \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=104\"
  }
}
```

This time the response contains both `before` and `after` tokens and `previous` and `next` URLs. The `before` token or `previous` link can be used to navigate backwards in the results.

This SDK is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.61.1
- Package version: 1.61.1
- Build package: org.openapitools.codegen.languages.JavascriptClientCodegen

## Installation

### For [Node.js](https://nodejs.org/)

#### npm

To publish the library as a [npm](https://www.npmjs.com/), please follow the procedure in ["Publishing npm packages"](https://docs.npmjs.com/getting-started/publishing-npm-packages).

Then install it via:

```shell
npm install junifer_au_native_api --save
```

Finally, you need to build the module:

```shell
npm run build
```

##### Local development

To use the library locally without publishing to a remote npm registry, first install the dependencies by changing into the directory containing `package.json` (and this README). Let's call this `JAVASCRIPT_CLIENT_DIR`. Then run:

```shell
npm install
```

Next, [link](https://docs.npmjs.com/cli/link) it globally in npm with the following, also from `JAVASCRIPT_CLIENT_DIR`:

```shell
npm link
```

To use the link you just defined in your project, switch to the directory you want to use your junifer_au_native_api from, and run:

```shell
npm link /path/to/<JAVASCRIPT_CLIENT_DIR>
```

Finally, you need to build the module:

```shell
npm run build
```

#### git

If the library is hosted at a git repository, e.g.https://github.com/GIT_USER_ID/GIT_REPO_ID
then install it via:

```shell
    npm install GIT_USER_ID/GIT_REPO_ID --save
```

### For browser

The library also works in the browser environment via npm and [browserify](http://browserify.org/). After following
the above steps with Node.js and installing browserify with `npm install -g browserify`,
perform the following (assuming *main.js* is your entry file):

```shell
browserify main.js > bundle.js
```

Then include *bundle.js* in the HTML pages.

### Webpack Configuration

Using Webpack you may encounter the following error: "Module not found: Error:
Cannot resolve module", most certainly you should disable AMD loader. Add/merge
the following section to your webpack config:

```javascript
module: {
  rules: [
    {
      parser: {
        amd: false
      }
    }
  ]
}
```

## Getting Started

Please follow the [installation](#installation) instruction and execute the following JS code:

```javascript
var JuniferAuNativeApi = require('junifer_au_native_api');

var defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
var tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix['Authorization'] = "Token"

var api = new JuniferAuNativeApi.AccountCreditsApi()
var id = 3.4; // {Number} Account credit ID number
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
api.juniferAccountCreditsIdDelete(id, callback);

```

## Documentation for API Endpoints

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*JuniferAuNativeApi.AccountCreditsApi* | [**juniferAccountCreditsIdDelete**](docs/AccountCreditsApi.md#juniferAccountCreditsIdDelete) | **DELETE** /junifer/accountCredits/{id} | Cancel account credit
*JuniferAuNativeApi.AccountCreditsApi* | [**juniferAccountCreditsIdGet**](docs/AccountCreditsApi.md#juniferAccountCreditsIdGet) | **GET** /junifer/accountCredits/{id} | Get Account Credit
*JuniferAuNativeApi.AccountDebitsApi* | [**juniferAccountDebitsIdDelete**](docs/AccountDebitsApi.md#juniferAccountDebitsIdDelete) | **DELETE** /junifer/accountDebits/{id} | Cancel account debit
*JuniferAuNativeApi.AccountDebitsApi* | [**juniferAccountDebitsIdGet**](docs/AccountDebitsApi.md#juniferAccountDebitsIdGet) | **GET** /junifer/accountDebits/{id} | Get Account Debit
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsAccountNumberNumGet**](docs/AccountsApi.md#juniferAccountsAccountNumberNumGet) | **GET** /junifer/accounts/accountNumber/{num} | Get account by account number
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsGet**](docs/AccountsApi.md#juniferAccountsGet) | **GET** /junifer/accounts | Account lookup
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdAccountCreditsGet**](docs/AccountsApi.md#juniferAccountsIdAccountCreditsGet) | **GET** /junifer/accounts/{id}/accountCredits | Get account credits
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdAccountCreditsPost**](docs/AccountsApi.md#juniferAccountsIdAccountCreditsPost) | **POST** /junifer/accounts/{id}/accountCredits | Create account credit
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdAccountDebitsGet**](docs/AccountsApi.md#juniferAccountsIdAccountDebitsGet) | **GET** /junifer/accounts/{id}/accountDebits | Get account debits
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdAccountDebitsPost**](docs/AccountsApi.md#juniferAccountsIdAccountDebitsPost) | **POST** /junifer/accounts/{id}/accountDebits | Create account debit
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdAccountReviewPeriodsGet**](docs/AccountsApi.md#juniferAccountsIdAccountReviewPeriodsGet) | **GET** /junifer/accounts/{id}/accountReviewPeriods | Get account&#39;s review periods
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdAccountReviewPeriodsPost**](docs/AccountsApi.md#juniferAccountsIdAccountReviewPeriodsPost) | **POST** /junifer/accounts/{id}/accountReviewPeriods | Create account review period
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdAgreementsGet**](docs/AccountsApi.md#juniferAccountsIdAgreementsGet) | **GET** /junifer/accounts/{id}/agreements | Get account&#39;s agreements
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdAllPayReferenceGet**](docs/AccountsApi.md#juniferAccountsIdAllPayReferenceGet) | **GET** /junifer/accounts/{id}/allPayReference | Get account&#39;s AllPay Reference Number
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdBillRequestsGet**](docs/AccountsApi.md#juniferAccountsIdBillRequestsGet) | **GET** /junifer/accounts/{id}/billRequests | Get account&#39;s bill requests
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdBillsGet**](docs/AccountsApi.md#juniferAccountsIdBillsGet) | **GET** /junifer/accounts/{id}/bills | Get account&#39;s bills
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdCancelAccountReviewPeriodDelete**](docs/AccountsApi.md#juniferAccountsIdCancelAccountReviewPeriodDelete) | **DELETE** /junifer/accounts/{id}/cancelAccountReviewPeriod | Cancel account review period
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdChangeAccountCustomerPut**](docs/AccountsApi.md#juniferAccountsIdChangeAccountCustomerPut) | **PUT** /junifer/accounts/{id}/changeAccountCustomer | Change the customer linked to an account
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdCommercialProductDetailsGet**](docs/AccountsApi.md#juniferAccountsIdCommercialProductDetailsGet) | **GET** /junifer/accounts/{id}/commercial/productDetails | Get account&#39;s product details for commercial customer
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdCommunicationsGet**](docs/AccountsApi.md#juniferAccountsIdCommunicationsGet) | **GET** /junifer/accounts/{id}/communications | Get account&#39;s communications
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdContactsContactIdPut**](docs/AccountsApi.md#juniferAccountsIdContactsContactIdPut) | **PUT** /junifer/accounts/{id}/contacts/{contactId} | Update account&#39;s contact details
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdContactsGet**](docs/AccountsApi.md#juniferAccountsIdContactsGet) | **GET** /junifer/accounts/{id}/contacts | Contacts
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdCreditsGet**](docs/AccountsApi.md#juniferAccountsIdCreditsGet) | **GET** /junifer/accounts/{id}/credits | Get account&#39;s credits
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdEnrolMeterPointsPost**](docs/AccountsApi.md#juniferAccountsIdEnrolMeterPointsPost) | **POST** /junifer/accounts/{id}/enrolMeterPoints | Enrol additional Meter Points to existing account/site
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdGet**](docs/AccountsApi.md#juniferAccountsIdGet) | **GET** /junifer/accounts/{id} | Get account
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdHotBillPost**](docs/AccountsApi.md#juniferAccountsIdHotBillPost) | **POST** /junifer/accounts/{id}/hotBill | Hot bill
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdNotePost**](docs/AccountsApi.md#juniferAccountsIdNotePost) | **POST** /junifer/accounts/{id}/note | Create account note
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdPaymentMethodsGet**](docs/AccountsApi.md#juniferAccountsIdPaymentMethodsGet) | **GET** /junifer/accounts/{id}/paymentMethods | Get account&#39;s payment methods
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdPaymentPlansGet**](docs/AccountsApi.md#juniferAccountsIdPaymentPlansGet) | **GET** /junifer/accounts/{id}/paymentPlans | Get account&#39;s payment plans
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdPaymentPost**](docs/AccountsApi.md#juniferAccountsIdPaymentPost) | **POST** /junifer/accounts/{id}/payment | Create Payment Transaction
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdPaymentSchedulePeriodsGet**](docs/AccountsApi.md#juniferAccountsIdPaymentSchedulePeriodsGet) | **GET** /junifer/accounts/{id}/paymentSchedulePeriods | Get account&#39;s payment schedule periods
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet**](docs/AccountsApi.md#juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet) | **GET** /junifer/accounts/{id}/paymentSchedules/suggestedPaymentAmount | Get account&#39;s suggested payment amount
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdPaymentsGet**](docs/AccountsApi.md#juniferAccountsIdPaymentsGet) | **GET** /junifer/accounts/{id}/payments | Get account&#39;s payments
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdPrimaryContactPut**](docs/AccountsApi.md#juniferAccountsIdPrimaryContactPut) | **PUT** /junifer/accounts/{id}/primaryContact | Update Accounts&#39;s primary contact details.
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdProductDetailsGet**](docs/AccountsApi.md#juniferAccountsIdProductDetailsGet) | **GET** /junifer/accounts/{id}/productDetails | Get account&#39;s product details
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdPropertysGet**](docs/AccountsApi.md#juniferAccountsIdPropertysGet) | **GET** /junifer/accounts/{id}/propertys | Get account&#39;s properties
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdRenewalPost**](docs/AccountsApi.md#juniferAccountsIdRenewalPost) | **POST** /junifer/accounts/{id}/renewal | Renew account
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdRepaymentsPost**](docs/AccountsApi.md#juniferAccountsIdRepaymentsPost) | **POST** /junifer/accounts/{id}/repayments | Create account repayment
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdTariffInformationGet**](docs/AccountsApi.md#juniferAccountsIdTariffInformationGet) | **GET** /junifer/accounts/{id}/tariffInformation | Get account&#39;s tariff Information
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdTicketsGet**](docs/AccountsApi.md#juniferAccountsIdTicketsGet) | **GET** /junifer/accounts/{id}/tickets | Get account&#39;s tickets
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdTicketsPost**](docs/AccountsApi.md#juniferAccountsIdTicketsPost) | **POST** /junifer/accounts/{id}/tickets | Create account ticket
*JuniferAuNativeApi.AccountsApi* | [**juniferAccountsIdTransactionsGet**](docs/AccountsApi.md#juniferAccountsIdTransactionsGet) | **GET** /junifer/accounts/{id}/transactions | Get account&#39;s transactions
*JuniferAuNativeApi.AccountsAustraliaApi* | [**juniferAuAccountsIdDiscountsGet**](docs/AccountsAustraliaApi.md#juniferAuAccountsIdDiscountsGet) | **GET** /junifer/au/accounts/{id}/discounts | Get discounts linked to the account specified by the ID
*JuniferAuNativeApi.AccountsAustraliaApi* | [**juniferAuAccountsIdDiscountsPost**](docs/AccountsAustraliaApi.md#juniferAuAccountsIdDiscountsPost) | **POST** /junifer/au/accounts/{id}/discounts | Links a discount to the account specified by the ID
*JuniferAuNativeApi.AccountsAustraliaApi* | [**juniferAuAccountsIdRenewalPost**](docs/AccountsAustraliaApi.md#juniferAuAccountsIdRenewalPost) | **POST** /junifer/au/accounts/{id}/renewal | Renew account with one or more new agreements
*JuniferAuNativeApi.AlertsApi* | [**juniferAlertsIdGet**](docs/AlertsApi.md#juniferAlertsIdGet) | **GET** /junifer/alerts/{id} | Get alert
*JuniferAuNativeApi.BillPeriodsApi* | [**juniferBillPeriodsIdGet**](docs/BillPeriodsApi.md#juniferBillPeriodsIdGet) | **GET** /junifer/billPeriods/{id} | Get Bill Period
*JuniferAuNativeApi.BillRequestsApi* | [**juniferBillRequestsIdGet**](docs/BillRequestsApi.md#juniferBillRequestsIdGet) | **GET** /junifer/billRequests/{id} | Get bill request
*JuniferAuNativeApi.BillRequestsApi* | [**juniferBillRequestsIdRestartFailedPost**](docs/BillRequestsApi.md#juniferBillRequestsIdRestartFailedPost) | **POST** /junifer/billRequests/{id}/restartFailed | Restart failed bill request
*JuniferAuNativeApi.BillingEntitiesApi* | [**juniferBillingEntitiesIdGet**](docs/BillingEntitiesApi.md#juniferBillingEntitiesIdGet) | **GET** /junifer/billingEntities/{id} | Get billing entity
*JuniferAuNativeApi.BillsApi* | [**juniferBillEmailsIdGet**](docs/BillsApi.md#juniferBillEmailsIdGet) | **GET** /junifer/billEmails/{id} | Get bill email
*JuniferAuNativeApi.BillsApi* | [**juniferBillFilesIdGet**](docs/BillsApi.md#juniferBillFilesIdGet) | **GET** /junifer/billFiles/{id} | Get bill file
*JuniferAuNativeApi.BillsApi* | [**juniferBillFilesIdImageGet**](docs/BillsApi.md#juniferBillFilesIdImageGet) | **GET** /junifer/billFiles/{id}/image | Get bill file image
*JuniferAuNativeApi.BillsApi* | [**juniferBillsIdAcceptDraftPost**](docs/BillsApi.md#juniferBillsIdAcceptDraftPost) | **POST** /junifer/bills/{id}/acceptDraft | Accept Draft bill
*JuniferAuNativeApi.BillsApi* | [**juniferBillsIdBillBreakdownLinesGet**](docs/BillsApi.md#juniferBillsIdBillBreakdownLinesGet) | **GET** /junifer/bills/{id}/billBreakdownLines | Get bill&#39;s breakdown lines
*JuniferAuNativeApi.BillsApi* | [**juniferBillsIdGet**](docs/BillsApi.md#juniferBillsIdGet) | **GET** /junifer/bills/{id} | Get bill
*JuniferAuNativeApi.BillsApi* | [**juniferBillsIdReversionPost**](docs/BillsApi.md#juniferBillsIdReversionPost) | **POST** /junifer/bills/{id}/reversion | Reversion bill
*JuniferAuNativeApi.CommunicationsApi* | [**juniferCommunicationsEmailPost**](docs/CommunicationsApi.md#juniferCommunicationsEmailPost) | **POST** /junifer/communications/email | Send email
*JuniferAuNativeApi.CommunicationsApi* | [**juniferCommunicationsEmailsIdGet**](docs/CommunicationsApi.md#juniferCommunicationsEmailsIdGet) | **GET** /junifer/communicationsEmails/{id} | Get communication email
*JuniferAuNativeApi.CommunicationsApi* | [**juniferCommunicationsFilesIdGet**](docs/CommunicationsApi.md#juniferCommunicationsFilesIdGet) | **GET** /junifer/communicationsFiles/{id} | Get communication file
*JuniferAuNativeApi.CommunicationsApi* | [**juniferCommunicationsFilesIdImageGet**](docs/CommunicationsApi.md#juniferCommunicationsFilesIdImageGet) | **GET** /junifer/communicationsFiles/{id}/image | Get communication file image
*JuniferAuNativeApi.CommunicationsApi* | [**juniferCommunicationsIdGet**](docs/CommunicationsApi.md#juniferCommunicationsIdGet) | **GET** /junifer/communications/{id} | Get Communication
*JuniferAuNativeApi.ConcessionsAustraliaApi* | [**juniferAuConcessionsCreateConcessionPost**](docs/ConcessionsAustraliaApi.md#juniferAuConcessionsCreateConcessionPost) | **POST** /junifer/au/concessions/createConcession | Create concession
*JuniferAuNativeApi.ContactsApi* | [**juniferContactsIdAccountsGet**](docs/ContactsApi.md#juniferContactsIdAccountsGet) | **GET** /junifer/contacts/{id}/accounts | Get accounts
*JuniferAuNativeApi.ContactsApi* | [**juniferContactsIdGet**](docs/ContactsApi.md#juniferContactsIdGet) | **GET** /junifer/contacts/{id} | Get contact
*JuniferAuNativeApi.ContactsApi* | [**juniferContactsIdMarketingPreferencesGet**](docs/ContactsApi.md#juniferContactsIdMarketingPreferencesGet) | **GET** /junifer/contacts/{id}/marketingPreferences | Get a contact&#39;s marketing preferences
*JuniferAuNativeApi.ContactsApi* | [**juniferContactsIdMarketingPreferencesPut**](docs/ContactsApi.md#juniferContactsIdMarketingPreferencesPut) | **PUT** /junifer/contacts/{id}/marketingPreferences | Update a contact&#39;s marketing preferences
*JuniferAuNativeApi.CreditsApi* | [**juniferCreditFilesIdGet**](docs/CreditsApi.md#juniferCreditFilesIdGet) | **GET** /junifer/creditFiles/{id} | Get credit file
*JuniferAuNativeApi.CreditsApi* | [**juniferCreditFilesIdImageGet**](docs/CreditsApi.md#juniferCreditFilesIdImageGet) | **GET** /junifer/creditFiles/{id}/image | Get credit file image
*JuniferAuNativeApi.CreditsApi* | [**juniferCreditsIdCreditBreakdownLinesGet**](docs/CreditsApi.md#juniferCreditsIdCreditBreakdownLinesGet) | **GET** /junifer/credits/{id}/creditBreakdownLines | Get credit&#39;s breakdown lines
*JuniferAuNativeApi.CreditsApi* | [**juniferCreditsIdGet**](docs/CreditsApi.md#juniferCreditsIdGet) | **GET** /junifer/credits/{id} | Get Credit
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferAuBpointDirectDebitsIdDelete**](docs/CustomerPaymentsApi.md#juniferAuBpointDirectDebitsIdDelete) | **DELETE** /junifer/au/bpointDirectDebits/{id} | Cancel BPOINT Direct Debit Instruction
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferAuBpointDirectDebitsIdGet**](docs/CustomerPaymentsApi.md#juniferAuBpointDirectDebitsIdGet) | **GET** /junifer/au/bpointDirectDebits/{id} | Existing BPOINT Direct Debit Instruction
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferAuBpointDirectDebitsPost**](docs/CustomerPaymentsApi.md#juniferAuBpointDirectDebitsPost) | **POST** /junifer/au/bpointDirectDebits | New BPOINT Direct Debit
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferAuPayPalDirectDebitsIdDelete**](docs/CustomerPaymentsApi.md#juniferAuPayPalDirectDebitsIdDelete) | **DELETE** /junifer/au/payPalDirectDebits/{id} | Cancel PayPal Direct Debit Instruction
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferAuPayPalDirectDebitsIdGet**](docs/CustomerPaymentsApi.md#juniferAuPayPalDirectDebitsIdGet) | **GET** /junifer/au/payPalDirectDebits/{id} | Existing PayPal Direct Debit Instruction
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferAuPayPalDirectDebitsPost**](docs/CustomerPaymentsApi.md#juniferAuPayPalDirectDebitsPost) | **POST** /junifer/au/payPalDirectDebits | New PayPal Direct Debit
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferAuWestpacDirectDebitsIdDelete**](docs/CustomerPaymentsApi.md#juniferAuWestpacDirectDebitsIdDelete) | **DELETE** /junifer/au/westpacDirectDebits/{id} | Cancel Westpac Direct Debit Instruction
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferAuWestpacDirectDebitsIdGet**](docs/CustomerPaymentsApi.md#juniferAuWestpacDirectDebitsIdGet) | **GET** /junifer/au/westpacDirectDebits/{id} | Existing Westpac Direct Debit Instruction
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferAuWestpacDirectDebitsPost**](docs/CustomerPaymentsApi.md#juniferAuWestpacDirectDebitsPost) | **POST** /junifer/au/westpacDirectDebits | New Westpac Direct Debit
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferSepaDirectDebitsIdDelete**](docs/CustomerPaymentsApi.md#juniferSepaDirectDebitsIdDelete) | **DELETE** /junifer/sepaDirectDebits/{id} | Cancel SEPA Direct Debit Instruction
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferSepaDirectDebitsIdGet**](docs/CustomerPaymentsApi.md#juniferSepaDirectDebitsIdGet) | **GET** /junifer/sepaDirectDebits/{id} | Existing SEPA Direct Debit Instruction
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferSepaDirectDebitsIdPut**](docs/CustomerPaymentsApi.md#juniferSepaDirectDebitsIdPut) | **PUT** /junifer/sepaDirectDebits/{id} | Update SEPA Direct Debit Mandate
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferSepaDirectDebitsPost**](docs/CustomerPaymentsApi.md#juniferSepaDirectDebitsPost) | **POST** /junifer/sepaDirectDebits | New SEPA Direct Debit Mandate
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferStripePaymentCardsIdDelete**](docs/CustomerPaymentsApi.md#juniferStripePaymentCardsIdDelete) | **DELETE** /junifer/stripePaymentCards/{id} | Cancel Stripe Payment Card
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferStripePaymentCardsIdGet**](docs/CustomerPaymentsApi.md#juniferStripePaymentCardsIdGet) | **GET** /junifer/stripePaymentCards/{id} | Existing Stripe Payment Card Instruction
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferStripePaymentCardsIdPaymentsPost**](docs/CustomerPaymentsApi.md#juniferStripePaymentCardsIdPaymentsPost) | **POST** /junifer/stripePaymentCards/{id}/payments | Create Stripe Card Payment Collection
*JuniferAuNativeApi.CustomerPaymentsApi* | [**juniferStripePaymentCardsPost**](docs/CustomerPaymentsApi.md#juniferStripePaymentCardsPost) | **POST** /junifer/stripePaymentCards | Store Stripe Payment Card
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersEnrolCustomerPost**](docs/CustomersApi.md#juniferCustomersEnrolCustomerPost) | **POST** /junifer/customers/enrolCustomer | Enrol customer
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersGet**](docs/CustomersApi.md#juniferCustomersGet) | **GET** /junifer/customers | Customer lookup
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdAccountsGet**](docs/CustomersApi.md#juniferCustomersIdAccountsGet) | **GET** /junifer/customers/{id}/accounts | Get customer&#39;s accounts
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdBillingEntitiesGet**](docs/CustomersApi.md#juniferCustomersIdBillingEntitiesGet) | **GET** /junifer/customers/{id}/billingEntities | Get customer&#39;s billing entities
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdConsentsGet**](docs/CustomersApi.md#juniferCustomersIdConsentsGet) | **GET** /junifer/customers/{id}/consents | Get customer&#39;s consents
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdConsentsPut**](docs/CustomersApi.md#juniferCustomersIdConsentsPut) | **PUT** /junifer/customers/{id}/consents | Update customer&#39;s consents
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdContactsContactIdPut**](docs/CustomersApi.md#juniferCustomersIdContactsContactIdPut) | **PUT** /junifer/customers/{id}/contacts/{contactId} | Update customer&#39;s contact details
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdContactsGet**](docs/CustomersApi.md#juniferCustomersIdContactsGet) | **GET** /junifer/customers/{id}/contacts | Contacts
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdCreateProspectPut**](docs/CustomersApi.md#juniferCustomersIdCreateProspectPut) | **PUT** /junifer/customers/{id}/createProspect | Create Prospect
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdEnrolAdditionalAccountPost**](docs/CustomersApi.md#juniferCustomersIdEnrolAdditionalAccountPost) | **POST** /junifer/customers/{id}/enrolAdditionalAccount | Enrol a new account to an existing customer
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdGet**](docs/CustomersApi.md#juniferCustomersIdGet) | **GET** /junifer/customers/{id} | Get customer
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdPrimaryContactPut**](docs/CustomersApi.md#juniferCustomersIdPrimaryContactPut) | **PUT** /junifer/customers/{id}/primaryContact | Update customer&#39;s primary contact details
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdProspectsGet**](docs/CustomersApi.md#juniferCustomersIdProspectsGet) | **GET** /junifer/customers/{id}/prospects | Get customer&#39;s Prospects
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdPut**](docs/CustomersApi.md#juniferCustomersIdPut) | **PUT** /junifer/customers/{id} | Update customer
*JuniferAuNativeApi.CustomersApi* | [**juniferCustomersIdUpdateBillingAddressPut**](docs/CustomersApi.md#juniferCustomersIdUpdateBillingAddressPut) | **PUT** /junifer/customers/{id}/updateBillingAddress | Update customer&#39;s Billing Address.
*JuniferAuNativeApi.CustomersAustraliaApi* | [**juniferAuCustomersEnrolBusinessCustomerPost**](docs/CustomersAustraliaApi.md#juniferAuCustomersEnrolBusinessCustomerPost) | **POST** /junifer/au/customers/enrolBusinessCustomer | Enrol business customer
*JuniferAuNativeApi.CustomersAustraliaApi* | [**juniferAuCustomersEnrolCustomerPost**](docs/CustomersAustraliaApi.md#juniferAuCustomersEnrolCustomerPost) | **POST** /junifer/au/customers/enrolCustomer | Enrol customer
*JuniferAuNativeApi.CustomersAustraliaApi* | [**juniferAuCustomersLifeSupportPost**](docs/CustomersAustraliaApi.md#juniferAuCustomersLifeSupportPost) | **POST** /junifer/au/customers/lifeSupport | Add life support and sensitive load details
*JuniferAuNativeApi.CustomersAustraliaApi* | [**juniferAuCustomersSiteAccessPost**](docs/CustomersAustraliaApi.md#juniferAuCustomersSiteAccessPost) | **POST** /junifer/au/customers/siteAccess | Add site access and hazard details
*JuniferAuNativeApi.FinancialsApi* | [**juniferActiveSeasonalDefinitionGet**](docs/FinancialsApi.md#juniferActiveSeasonalDefinitionGet) | **GET** /junifer/activeSeasonalDefinition | Get active seasonal definition
*JuniferAuNativeApi.FinancialsApi* | [**juniferPaymentMethodsIdGet**](docs/FinancialsApi.md#juniferPaymentMethodsIdGet) | **GET** /junifer/paymentMethods/{id} | Get payment method
*JuniferAuNativeApi.FinancialsApi* | [**juniferPaymentPlansIdGet**](docs/FinancialsApi.md#juniferPaymentPlansIdGet) | **GET** /junifer/paymentPlans/{id} | Get payment plan
*JuniferAuNativeApi.FinancialsApi* | [**juniferPaymentRequestsIdGet**](docs/FinancialsApi.md#juniferPaymentRequestsIdGet) | **GET** /junifer/paymentRequests/{id} | Get payment request
*JuniferAuNativeApi.FinancialsApi* | [**juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet**](docs/FinancialsApi.md#juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet) | **GET** /junifer/paymentSchedulePeriods/calculateNextPaymentDate | Calculate next payment schedule collection date
*JuniferAuNativeApi.FinancialsApi* | [**juniferPaymentSchedulePeriodsIdGet**](docs/FinancialsApi.md#juniferPaymentSchedulePeriodsIdGet) | **GET** /junifer/paymentSchedulePeriods/{id} | Get payment schedule period
*JuniferAuNativeApi.FinancialsApi* | [**juniferPaymentSchedulePeriodsIdPut**](docs/FinancialsApi.md#juniferPaymentSchedulePeriodsIdPut) | **PUT** /junifer/paymentSchedulePeriods/{id} | Stop payment schedule period
*JuniferAuNativeApi.FinancialsApi* | [**juniferPaymentSchedulePeriodsPaymentScheduleItemsGet**](docs/FinancialsApi.md#juniferPaymentSchedulePeriodsPaymentScheduleItemsGet) | **GET** /junifer/paymentSchedulePeriods/paymentScheduleItems | Get account&#39;s payment schedule items
*JuniferAuNativeApi.FinancialsApi* | [**juniferPaymentSchedulePeriodsPost**](docs/FinancialsApi.md#juniferPaymentSchedulePeriodsPost) | **POST** /junifer/paymentSchedulePeriods | Create new payment schedule period
*JuniferAuNativeApi.FinancialsApi* | [**juniferPaymentsIdGet**](docs/FinancialsApi.md#juniferPaymentsIdGet) | **GET** /junifer/payments/{id} | Get payment
*JuniferAuNativeApi.MarketingCampaignSourcesApi* | [**juniferMarketingCampaignSourcesGet**](docs/MarketingCampaignSourcesApi.md#juniferMarketingCampaignSourcesGet) | **GET** /junifer/marketingCampaignSources | List Marketing Campaign Sources
*JuniferAuNativeApi.MeterpointsAustraliaApi* | [**juniferAuMeterPointsGet**](docs/MeterpointsAustraliaApi.md#juniferAuMeterPointsGet) | **GET** /junifer/au/meterPoints | MeterPoint lookup
*JuniferAuNativeApi.MeterpointsAustraliaApi* | [**juniferAuMeterPointsIdMeterStructureGet**](docs/MeterpointsAustraliaApi.md#juniferAuMeterPointsIdMeterStructureGet) | **GET** /junifer/au/meterPoints/{id}/meterStructure | Get meterpoint structure
*JuniferAuNativeApi.MeterpointsAustraliaApi* | [**juniferAuMeterPointsIdReadingsGet**](docs/MeterpointsAustraliaApi.md#juniferAuMeterPointsIdReadingsGet) | **GET** /junifer/au/meterPoints/{id}/readings | Get meter readings
*JuniferAuNativeApi.MeterpointsAustraliaApi* | [**juniferAuMeterPointsIdReadingsPost**](docs/MeterpointsAustraliaApi.md#juniferAuMeterPointsIdReadingsPost) | **POST** /junifer/au/meterPoints/{id}/readings | Submit meter readings
*JuniferAuNativeApi.MeterpointsAustraliaApi* | [**juniferAuMeterPointsMeterStructureGet**](docs/MeterpointsAustraliaApi.md#juniferAuMeterPointsMeterStructureGet) | **GET** /junifer/au/meterPoints/meterStructure | Get meterpoint structure
*JuniferAuNativeApi.NmisApi* | [**juniferAuNmisStandingDataForNMIGet**](docs/NmisApi.md#juniferAuNmisStandingDataForNMIGet) | **GET** /junifer/au/nmis/standingDataForNMI | Get NMIs including standing data
*JuniferAuNativeApi.OffersApi* | [**juniferOffersIdGet**](docs/OffersApi.md#juniferOffersIdGet) | **GET** /junifer/offers/{id} | Get Offer
*JuniferAuNativeApi.OffersApi* | [**juniferOffersIdQuotesGet**](docs/OffersApi.md#juniferOffersIdQuotesGet) | **GET** /junifer/offers/{id}/quotes | Get quotes
*JuniferAuNativeApi.PaymentPlansApi* | [**juniferPaymentPlansIdPaymentPlanPaymentsGet**](docs/PaymentPlansApi.md#juniferPaymentPlansIdPaymentPlanPaymentsGet) | **GET** /junifer/paymentPlans/{id}/paymentPlanPayments | Get paymentPlan&#39;s payments
*JuniferAuNativeApi.ProductsApi* | [**juniferAuProductsTariffsForNMIGet**](docs/ProductsApi.md#juniferAuProductsTariffsForNMIGet) | **GET** /junifer/au/products/tariffsForNMI | Get products for nmi
*JuniferAuNativeApi.PropertiesApi* | [**juniferPropertysIdGet**](docs/PropertiesApi.md#juniferPropertysIdGet) | **GET** /junifer/propertys/{id} | Get property
*JuniferAuNativeApi.PropertiesApi* | [**juniferPropertysIdMeterPointsGet**](docs/PropertiesApi.md#juniferPropertysIdMeterPointsGet) | **GET** /junifer/propertys/{id}/meterPoints | Get meter points
*JuniferAuNativeApi.PropertiesApi* | [**juniferPropertysPost**](docs/PropertiesApi.md#juniferPropertysPost) | **POST** /junifer/propertys | 
*JuniferAuNativeApi.ProspectsApi* | [**juniferBrokerLinkagesIdGet**](docs/ProspectsApi.md#juniferBrokerLinkagesIdGet) | **GET** /junifer/brokerLinkages/{id} | Get Broker Linkage
*JuniferAuNativeApi.ProspectsApi* | [**juniferBrokersGet**](docs/ProspectsApi.md#juniferBrokersGet) | **GET** /junifer/brokers | Lookup broker
*JuniferAuNativeApi.ProspectsApi* | [**juniferBrokersIdBrokerLinkagesGet**](docs/ProspectsApi.md#juniferBrokersIdBrokerLinkagesGet) | **GET** /junifer/brokers/{id}/brokerLinkages | Get BrokerLinkages for Broker
*JuniferAuNativeApi.ProspectsApi* | [**juniferBrokersIdGet**](docs/ProspectsApi.md#juniferBrokersIdGet) | **GET** /junifer/brokers/{id} | Get Broker
*JuniferAuNativeApi.ProspectsApi* | [**juniferProspectsIdAddPropertiesPost**](docs/ProspectsApi.md#juniferProspectsIdAddPropertiesPost) | **POST** /junifer/prospects/{id}/add_properties | Add Properties
*JuniferAuNativeApi.ProspectsApi* | [**juniferProspectsIdGet**](docs/ProspectsApi.md#juniferProspectsIdGet) | **GET** /junifer/prospects/{id} | Get prospect
*JuniferAuNativeApi.ProspectsApi* | [**juniferProspectsIdOffersGet**](docs/ProspectsApi.md#juniferProspectsIdOffersGet) | **GET** /junifer/prospects/{id}/offers | Get prospect offers
*JuniferAuNativeApi.ProspectsApi* | [**juniferProspectsIdPropertysGet**](docs/ProspectsApi.md#juniferProspectsIdPropertysGet) | **GET** /junifer/prospects/{id}/propertys | Get prospect&#39;s properties
*JuniferAuNativeApi.ProspectsApi* | [**juniferProspectsPost**](docs/ProspectsApi.md#juniferProspectsPost) | **POST** /junifer/prospects | Create Prospect
*JuniferAuNativeApi.QuoteFilesApi* | [**juniferQuoteFilesIdGet**](docs/QuoteFilesApi.md#juniferQuoteFilesIdGet) | **GET** /junifer/quoteFiles/{id} | Get quote file
*JuniferAuNativeApi.QuoteFilesApi* | [**juniferQuoteFilesIdImageGet**](docs/QuoteFilesApi.md#juniferQuoteFilesIdImageGet) | **GET** /junifer/quoteFiles/{id}/image | Get quote file image
*JuniferAuNativeApi.QuotesApi* | [**juniferQuotesIdAcceptQuotePost**](docs/QuotesApi.md#juniferQuotesIdAcceptQuotePost) | **POST** /junifer/quotes/{id}/acceptQuote | 
*JuniferAuNativeApi.QuotesApi* | [**juniferQuotesIdGet**](docs/QuotesApi.md#juniferQuotesIdGet) | **GET** /junifer/quotes/{id} | Get quote
*JuniferAuNativeApi.TicketsApi* | [**juniferTicketsIdCancelTicketPost**](docs/TicketsApi.md#juniferTicketsIdCancelTicketPost) | **POST** /junifer/tickets/{id}/cancelTicket | Cancel Ticket
*JuniferAuNativeApi.TicketsApi* | [**juniferTicketsIdGet**](docs/TicketsApi.md#juniferTicketsIdGet) | **GET** /junifer/tickets/{id} | Get ticket
*JuniferAuNativeApi.TicketsApi* | [**juniferTicketsIdPut**](docs/TicketsApi.md#juniferTicketsIdPut) | **PUT** /junifer/tickets/{id} | Update Ticket


## Documentation for Models

 - [JuniferAuNativeApi.AcceptDraftBillPostBillsIdAcceptdraftResponse404](docs/AcceptDraftBillPostBillsIdAcceptdraftResponse404.md)
 - [JuniferAuNativeApi.AcceptQuotePostQuotesIdAcceptquoteResponse400](docs/AcceptQuotePostQuotesIdAcceptquoteResponse400.md)
 - [JuniferAuNativeApi.AcceptQuotePostQuotesIdAcceptquoteResponse404](docs/AcceptQuotePostQuotesIdAcceptquoteResponse404.md)
 - [JuniferAuNativeApi.AddPropertiesPostProspectsIdAddPropertiesResponse400](docs/AddPropertiesPostProspectsIdAddPropertiesResponse400.md)
 - [JuniferAuNativeApi.AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody](docs/AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.md)
 - [JuniferAuNativeApi.AusCreateConcessionPostAuConcessionsCreateconcessionResponse200](docs/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200.md)
 - [JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody](docs/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody.md)
 - [JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress](docs/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.md)
 - [JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts](docs/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.md)
 - [JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct](docs/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct.md)
 - [JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints](docs/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints.md)
 - [JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts](docs/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts.md)
 - [JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress](docs/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.md)
 - [JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200](docs/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200.md)
 - [JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse400](docs/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse400.md)
 - [JuniferAuNativeApi.AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody](docs/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.md)
 - [JuniferAuNativeApi.AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct](docs/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct.md)
 - [JuniferAuNativeApi.AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200](docs/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200.md)
 - [JuniferAuNativeApi.AusEnrolCustomerPostAuCustomersEnrolcustomerResponse400](docs/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse400.md)
 - [JuniferAuNativeApi.AusGetDiscountsGetAuAccountsIdDiscountsResponse200](docs/AusGetDiscountsGetAuAccountsIdDiscountsResponse200.md)
 - [JuniferAuNativeApi.AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody](docs/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.md)
 - [JuniferAuNativeApi.AusLifeSupportSensitiveLoadPostAuCustomersLifesupportResponse400](docs/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportResponse400.md)
 - [JuniferAuNativeApi.AusLinkDiscountPostAuAccountsIdDiscountsRequestBody](docs/AusLinkDiscountPostAuAccountsIdDiscountsRequestBody.md)
 - [JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalRequestBody](docs/AusRenewAccountPostAuAccountsIdRenewalRequestBody.md)
 - [JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements](docs/AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements.md)
 - [JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalResponse200](docs/AusRenewAccountPostAuAccountsIdRenewalResponse200.md)
 - [JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalResponse200Results](docs/AusRenewAccountPostAuAccountsIdRenewalResponse200Results.md)
 - [JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalResponse400](docs/AusRenewAccountPostAuAccountsIdRenewalResponse400.md)
 - [JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalResponse404](docs/AusRenewAccountPostAuAccountsIdRenewalResponse404.md)
 - [JuniferAuNativeApi.AusSiteAccessPostAuCustomersSiteaccessRequestBody](docs/AusSiteAccessPostAuCustomersSiteaccessRequestBody.md)
 - [JuniferAuNativeApi.AusSiteAccessPostAuCustomersSiteaccessResponse400](docs/AusSiteAccessPostAuCustomersSiteaccessResponse400.md)
 - [JuniferAuNativeApi.BillEmailsGetBillemailsIdResponse200](docs/BillEmailsGetBillemailsIdResponse200.md)
 - [JuniferAuNativeApi.BillEmailsGetBillemailsIdResponse200Files](docs/BillEmailsGetBillemailsIdResponse200Files.md)
 - [JuniferAuNativeApi.BillEmailsGetBillemailsIdResponse200Links](docs/BillEmailsGetBillemailsIdResponse200Links.md)
 - [JuniferAuNativeApi.BillEmailsGetBillemailsIdResponse200Links1](docs/BillEmailsGetBillemailsIdResponse200Links1.md)
 - [JuniferAuNativeApi.BillEmailsGetBillemailsIdResponse404](docs/BillEmailsGetBillemailsIdResponse404.md)
 - [JuniferAuNativeApi.CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400](docs/CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400.md)
 - [JuniferAuNativeApi.CancelAccountCreditDeleteAccountcreditsIdResponse400](docs/CancelAccountCreditDeleteAccountcreditsIdResponse400.md)
 - [JuniferAuNativeApi.CancelAccountDebitDeleteAccountdebitsIdResponse400](docs/CancelAccountDebitDeleteAccountdebitsIdResponse400.md)
 - [JuniferAuNativeApi.CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400](docs/CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400.md)
 - [JuniferAuNativeApi.CancelTicketPostTicketsIdCancelticketResponse400](docs/CancelTicketPostTicketsIdCancelticketResponse400.md)
 - [JuniferAuNativeApi.ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody](docs/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody.md)
 - [JuniferAuNativeApi.ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400](docs/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400.md)
 - [JuniferAuNativeApi.ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404](docs/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404.md)
 - [JuniferAuNativeApi.CommunicationsEmailsGetCommunicationsemailsIdResponse200](docs/CommunicationsEmailsGetCommunicationsemailsIdResponse200.md)
 - [JuniferAuNativeApi.CommunicationsEmailsGetCommunicationsemailsIdResponse200Links](docs/CommunicationsEmailsGetCommunicationsemailsIdResponse200Links.md)
 - [JuniferAuNativeApi.CommunicationsEmailsGetCommunicationsemailsIdResponse404](docs/CommunicationsEmailsGetCommunicationsemailsIdResponse404.md)
 - [JuniferAuNativeApi.ContactsGetAccountsIdContactsResponse200](docs/ContactsGetAccountsIdContactsResponse200.md)
 - [JuniferAuNativeApi.ContactsGetAccountsIdContactsResponse200Address](docs/ContactsGetAccountsIdContactsResponse200Address.md)
 - [JuniferAuNativeApi.ContactsGetAccountsIdContactsResponse200Links](docs/ContactsGetAccountsIdContactsResponse200Links.md)
 - [JuniferAuNativeApi.ContactsGetAccountsIdContactsResponse400](docs/ContactsGetAccountsIdContactsResponse400.md)
 - [JuniferAuNativeApi.ContactsGetCustomersIdContactsResponse200](docs/ContactsGetCustomersIdContactsResponse200.md)
 - [JuniferAuNativeApi.ContactsGetCustomersIdContactsResponse400](docs/ContactsGetCustomersIdContactsResponse400.md)
 - [JuniferAuNativeApi.CreateAccountCreditPostAccountsIdAccountcreditsRequestBody](docs/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody.md)
 - [JuniferAuNativeApi.CreateAccountCreditPostAccountsIdAccountcreditsResponse400](docs/CreateAccountCreditPostAccountsIdAccountcreditsResponse400.md)
 - [JuniferAuNativeApi.CreateAccountCreditPostAccountsIdAccountcreditsResponse404](docs/CreateAccountCreditPostAccountsIdAccountcreditsResponse404.md)
 - [JuniferAuNativeApi.CreateAccountDebitPostAccountsIdAccountdebitsRequestBody](docs/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody.md)
 - [JuniferAuNativeApi.CreateAccountDebitPostAccountsIdAccountdebitsResponse400](docs/CreateAccountDebitPostAccountsIdAccountdebitsResponse400.md)
 - [JuniferAuNativeApi.CreateAccountDebitPostAccountsIdAccountdebitsResponse404](docs/CreateAccountDebitPostAccountsIdAccountdebitsResponse404.md)
 - [JuniferAuNativeApi.CreateAccountNotePostAccountsIdNoteRequestBody](docs/CreateAccountNotePostAccountsIdNoteRequestBody.md)
 - [JuniferAuNativeApi.CreateAccountNotePostAccountsIdNoteResponse200](docs/CreateAccountNotePostAccountsIdNoteResponse200.md)
 - [JuniferAuNativeApi.CreateAccountNotePostAccountsIdNoteResponse400](docs/CreateAccountNotePostAccountsIdNoteResponse400.md)
 - [JuniferAuNativeApi.CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody](docs/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody.md)
 - [JuniferAuNativeApi.CreateAccountRepaymentPostAccountsIdRepaymentsResponse200](docs/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.md)
 - [JuniferAuNativeApi.CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links](docs/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links.md)
 - [JuniferAuNativeApi.CreateAccountRepaymentPostAccountsIdRepaymentsResponse400](docs/CreateAccountRepaymentPostAccountsIdRepaymentsResponse400.md)
 - [JuniferAuNativeApi.CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody](docs/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.md)
 - [JuniferAuNativeApi.CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200](docs/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.md)
 - [JuniferAuNativeApi.CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400](docs/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400.md)
 - [JuniferAuNativeApi.CreateAccountTicketPostAccountsIdTicketsRequestBody](docs/CreateAccountTicketPostAccountsIdTicketsRequestBody.md)
 - [JuniferAuNativeApi.CreateAccountTicketPostAccountsIdTicketsResponse200](docs/CreateAccountTicketPostAccountsIdTicketsResponse200.md)
 - [JuniferAuNativeApi.CreateAccountTicketPostAccountsIdTicketsResponse200Links](docs/CreateAccountTicketPostAccountsIdTicketsResponse200Links.md)
 - [JuniferAuNativeApi.CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities](docs/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.md)
 - [JuniferAuNativeApi.CreateAccountTicketPostAccountsIdTicketsResponse400](docs/CreateAccountTicketPostAccountsIdTicketsResponse400.md)
 - [JuniferAuNativeApi.CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody](docs/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.md)
 - [JuniferAuNativeApi.CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200](docs/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.md)
 - [JuniferAuNativeApi.CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400](docs/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400.md)
 - [JuniferAuNativeApi.CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404](docs/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404.md)
 - [JuniferAuNativeApi.CreatePropertyPostPropertysRequestBody](docs/CreatePropertyPostPropertysRequestBody.md)
 - [JuniferAuNativeApi.CreatePropertyPostPropertysResponse200](docs/CreatePropertyPostPropertysResponse200.md)
 - [JuniferAuNativeApi.CreatePropertyPostPropertysResponse400](docs/CreatePropertyPostPropertysResponse400.md)
 - [JuniferAuNativeApi.CreateProspectPostProspectsRequestBody](docs/CreateProspectPostProspectsRequestBody.md)
 - [JuniferAuNativeApi.CreateProspectPostProspectsRequestBodyCustomer](docs/CreateProspectPostProspectsRequestBodyCustomer.md)
 - [JuniferAuNativeApi.CreateProspectPostProspectsRequestBodyCustomerCompanyAddress](docs/CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.md)
 - [JuniferAuNativeApi.CreateProspectPostProspectsRequestBodyCustomerPrimaryContact](docs/CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.md)
 - [JuniferAuNativeApi.CreateProspectPostProspectsResponse200](docs/CreateProspectPostProspectsResponse200.md)
 - [JuniferAuNativeApi.CreateProspectPostProspectsResponse400](docs/CreateProspectPostProspectsResponse400.md)
 - [JuniferAuNativeApi.CreateProspectPutCustomersIdCreateprospectRequestBody](docs/CreateProspectPutCustomersIdCreateprospectRequestBody.md)
 - [JuniferAuNativeApi.CreateProspectPutCustomersIdCreateprospectResponse200](docs/CreateProspectPutCustomersIdCreateprospectResponse200.md)
 - [JuniferAuNativeApi.CreateProspectPutCustomersIdCreateprospectResponse400](docs/CreateProspectPutCustomersIdCreateprospectResponse400.md)
 - [JuniferAuNativeApi.CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200](docs/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200.md)
 - [JuniferAuNativeApi.CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse400](docs/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse400.md)
 - [JuniferAuNativeApi.CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401](docs/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401.md)
 - [JuniferAuNativeApi.EmailPostCommunicationsEmailRequestBody](docs/EmailPostCommunicationsEmailRequestBody.md)
 - [JuniferAuNativeApi.EmailPostCommunicationsEmailResponse400](docs/EmailPostCommunicationsEmailResponse400.md)
 - [JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody](docs/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.md)
 - [JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo](docs/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.md)
 - [JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate](docs/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate.md)
 - [JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct](docs/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct.md)
 - [JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct](docs/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.md)
 - [JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns](docs/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns.md)
 - [JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress](docs/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.md)
 - [JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200](docs/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200.md)
 - [JuniferAuNativeApi.EnrolCustomerPostCustomersEnrolcustomerRequestBody](docs/EnrolCustomerPostCustomersEnrolcustomerRequestBody.md)
 - [JuniferAuNativeApi.EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress](docs/EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress.md)
 - [JuniferAuNativeApi.EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts](docs/EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.md)
 - [JuniferAuNativeApi.EnrolCustomerPostCustomersEnrolcustomerResponse200](docs/EnrolCustomerPostCustomersEnrolcustomerResponse200.md)
 - [JuniferAuNativeApi.EnrolCustomerPostCustomersEnrolcustomerResponse400](docs/EnrolCustomerPostCustomersEnrolcustomerResponse400.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links.md)
 - [JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400](docs/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400.md)
 - [JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200](docs/GetAccountAgreementsGetAccountsIdAgreementsResponse200.md)
 - [JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType](docs/GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType.md)
 - [JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets](docs/GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets.md)
 - [JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200Links](docs/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1](docs/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1.md)
 - [JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200Products](docs/GetAccountAgreementsGetAccountsIdAgreementsResponse200Products.md)
 - [JuniferAuNativeApi.GetAccountBillRequestsGetAccountsIdBillrequestsResponse200](docs/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.md)
 - [JuniferAuNativeApi.GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links](docs/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountBillRequestsGetAccountsIdBillrequestsResponse404](docs/GetAccountBillRequestsGetAccountsIdBillrequestsResponse404.md)
 - [JuniferAuNativeApi.GetAccountBillsGetAccountsIdBillsResponse200](docs/GetAccountBillsGetAccountsIdBillsResponse200.md)
 - [JuniferAuNativeApi.GetAccountBillsGetAccountsIdBillsResponse200Links](docs/GetAccountBillsGetAccountsIdBillsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountByNumberGetAccountsAccountnumberNumResponse400](docs/GetAccountByNumberGetAccountsAccountnumberNumResponse400.md)
 - [JuniferAuNativeApi.GetAccountByNumberGetAccountsAccountnumberNumResponse404](docs/GetAccountByNumberGetAccountsAccountnumberNumResponse404.md)
 - [JuniferAuNativeApi.GetAccountCommunicationsGetAccountsIdCommunicationsResponse200](docs/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.md)
 - [JuniferAuNativeApi.GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files](docs/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files.md)
 - [JuniferAuNativeApi.GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links](docs/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountCommunicationsGetAccountsIdCommunicationsResponse400](docs/GetAccountCommunicationsGetAccountsIdCommunicationsResponse400.md)
 - [JuniferAuNativeApi.GetAccountCreditGetAccountcreditsIdResponse200](docs/GetAccountCreditGetAccountcreditsIdResponse200.md)
 - [JuniferAuNativeApi.GetAccountCreditNotesGetAccountsIdCreditsResponse200](docs/GetAccountCreditNotesGetAccountsIdCreditsResponse200.md)
 - [JuniferAuNativeApi.GetAccountCreditNotesGetAccountsIdCreditsResponse200Links](docs/GetAccountCreditNotesGetAccountsIdCreditsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountCreditsGetAccountsIdAccountcreditsResponse200](docs/GetAccountCreditsGetAccountsIdAccountcreditsResponse200.md)
 - [JuniferAuNativeApi.GetAccountDebitGetAccountdebitsIdResponse200](docs/GetAccountDebitGetAccountdebitsIdResponse200.md)
 - [JuniferAuNativeApi.GetAccountDebitsGetAccountsIdAccountdebitsResponse200](docs/GetAccountDebitsGetAccountsIdAccountdebitsResponse200.md)
 - [JuniferAuNativeApi.GetAccountGetAccountsIdResponse200](docs/GetAccountGetAccountsIdResponse200.md)
 - [JuniferAuNativeApi.GetAccountGetAccountsIdResponse200BillingAddress](docs/GetAccountGetAccountsIdResponse200BillingAddress.md)
 - [JuniferAuNativeApi.GetAccountGetAccountsIdResponse200Links](docs/GetAccountGetAccountsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountGetAccountsIdResponse404](docs/GetAccountGetAccountsIdResponse404.md)
 - [JuniferAuNativeApi.GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200](docs/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.md)
 - [JuniferAuNativeApi.GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links](docs/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200](docs/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200.md)
 - [JuniferAuNativeApi.GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links](docs/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions](docs/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions.md)
 - [JuniferAuNativeApi.GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200](docs/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200.md)
 - [JuniferAuNativeApi.GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200DebtRecoveryDetails](docs/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200DebtRecoveryDetails.md)
 - [JuniferAuNativeApi.GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links](docs/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountPaymentsGetAccountsIdPaymentsResponse200](docs/GetAccountPaymentsGetAccountsIdPaymentsResponse200.md)
 - [JuniferAuNativeApi.GetAccountPaymentsGetAccountsIdPaymentsResponse200Links](docs/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountPaymentsGetAccountsIdPaymentsResponse400](docs/GetAccountPaymentsGetAccountsIdPaymentsResponse400.md)
 - [JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200](docs/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200.md)
 - [JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints](docs/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints.md)
 - [JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType](docs/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.md)
 - [JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress](docs/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress.md)
 - [JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates](docs/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates.md)
 - [JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse400](docs/GetAccountProductDetailsGetAccountsIdProductdetailsResponse400.md)
 - [JuniferAuNativeApi.GetAccountPropertysGetAccountsIdPropertysResponse200](docs/GetAccountPropertysGetAccountsIdPropertysResponse200.md)
 - [JuniferAuNativeApi.GetAccountPropertysGetAccountsIdPropertysResponse200Address](docs/GetAccountPropertysGetAccountsIdPropertysResponse200Address.md)
 - [JuniferAuNativeApi.GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200](docs/GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200.md)
 - [JuniferAuNativeApi.GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200](docs/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200.md)
 - [JuniferAuNativeApi.GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links](docs/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400](docs/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400.md)
 - [JuniferAuNativeApi.GetAccountTicketsGetAccountsIdTicketsResponse200](docs/GetAccountTicketsGetAccountsIdTicketsResponse200.md)
 - [JuniferAuNativeApi.GetAccountTicketsGetAccountsIdTicketsResponse400](docs/GetAccountTicketsGetAccountsIdTicketsResponse400.md)
 - [JuniferAuNativeApi.GetAccountTransactionsGetAccountsIdTransactionsResponse200](docs/GetAccountTransactionsGetAccountsIdTransactionsResponse200.md)
 - [JuniferAuNativeApi.GetAccountTransactionsGetAccountsIdTransactionsResponse200Links](docs/GetAccountTransactionsGetAccountsIdTransactionsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountTransactionsGetAccountsIdTransactionsResponse400](docs/GetAccountTransactionsGetAccountsIdTransactionsResponse400.md)
 - [JuniferAuNativeApi.GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200](docs/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200.md)
 - [JuniferAuNativeApi.GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links](docs/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links.md)
 - [JuniferAuNativeApi.GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400](docs/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400.md)
 - [JuniferAuNativeApi.GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404](docs/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404.md)
 - [JuniferAuNativeApi.GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200](docs/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200.md)
 - [JuniferAuNativeApi.GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months](docs/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.md)
 - [JuniferAuNativeApi.GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400](docs/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400.md)
 - [JuniferAuNativeApi.GetAlertGetAlertsIdResponse200](docs/GetAlertGetAlertsIdResponse200.md)
 - [JuniferAuNativeApi.GetAlertGetAlertsIdResponse200Links](docs/GetAlertGetAlertsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetAlertGetAlertsIdResponse404](docs/GetAlertGetAlertsIdResponse404.md)
 - [JuniferAuNativeApi.GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200](docs/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200.md)
 - [JuniferAuNativeApi.GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400](docs/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400.md)
 - [JuniferAuNativeApi.GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200](docs/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.md)
 - [JuniferAuNativeApi.GetBillFileGetBillfilesIdResponse200](docs/GetBillFileGetBillfilesIdResponse200.md)
 - [JuniferAuNativeApi.GetBillFileGetBillfilesIdResponse200Links](docs/GetBillFileGetBillfilesIdResponse200Links.md)
 - [JuniferAuNativeApi.GetBillFileGetBillfilesIdResponse404](docs/GetBillFileGetBillfilesIdResponse404.md)
 - [JuniferAuNativeApi.GetBillFileImageGetBillfilesIdImageResponse200](docs/GetBillFileImageGetBillfilesIdImageResponse200.md)
 - [JuniferAuNativeApi.GetBillFileImageGetBillfilesIdImageResponse410](docs/GetBillFileImageGetBillfilesIdImageResponse410.md)
 - [JuniferAuNativeApi.GetBillGetBillsIdResponse200](docs/GetBillGetBillsIdResponse200.md)
 - [JuniferAuNativeApi.GetBillGetBillsIdResponse200BillFiles](docs/GetBillGetBillsIdResponse200BillFiles.md)
 - [JuniferAuNativeApi.GetBillGetBillsIdResponse200Links](docs/GetBillGetBillsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetBillGetBillsIdResponse200Links1](docs/GetBillGetBillsIdResponse200Links1.md)
 - [JuniferAuNativeApi.GetBillGetBillsIdResponse404](docs/GetBillGetBillsIdResponse404.md)
 - [JuniferAuNativeApi.GetBillPeriodGetBillperiodsIdResponse200](docs/GetBillPeriodGetBillperiodsIdResponse200.md)
 - [JuniferAuNativeApi.GetBillPeriodGetBillperiodsIdResponse404](docs/GetBillPeriodGetBillperiodsIdResponse404.md)
 - [JuniferAuNativeApi.GetBillRequestGetBillrequestsIdResponse200](docs/GetBillRequestGetBillrequestsIdResponse200.md)
 - [JuniferAuNativeApi.GetBillRequestGetBillrequestsIdResponse200Links](docs/GetBillRequestGetBillrequestsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetBillRequestGetBillrequestsIdResponse404](docs/GetBillRequestGetBillrequestsIdResponse404.md)
 - [JuniferAuNativeApi.GetBillingEntityGetBillingentitiesIdResponse200](docs/GetBillingEntityGetBillingentitiesIdResponse200.md)
 - [JuniferAuNativeApi.GetBillingEntityGetBillingentitiesIdResponse200Links](docs/GetBillingEntityGetBillingentitiesIdResponse200Links.md)
 - [JuniferAuNativeApi.GetBillingEntityGetBillingentitiesIdResponse404](docs/GetBillingEntityGetBillingentitiesIdResponse404.md)
 - [JuniferAuNativeApi.GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200](docs/GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200.md)
 - [JuniferAuNativeApi.GetBrokerGetBrokersIdResponse200](docs/GetBrokerGetBrokersIdResponse200.md)
 - [JuniferAuNativeApi.GetBrokerGetBrokersIdResponse200Links](docs/GetBrokerGetBrokersIdResponse200Links.md)
 - [JuniferAuNativeApi.GetBrokerGetBrokersIdResponse404](docs/GetBrokerGetBrokersIdResponse404.md)
 - [JuniferAuNativeApi.GetBrokerLinkageGetBrokerlinkagesIdResponse200](docs/GetBrokerLinkageGetBrokerlinkagesIdResponse200.md)
 - [JuniferAuNativeApi.GetBrokerLinkageGetBrokerlinkagesIdResponse200Links](docs/GetBrokerLinkageGetBrokerlinkagesIdResponse200Links.md)
 - [JuniferAuNativeApi.GetBrokerLinkageGetBrokerlinkagesIdResponse404](docs/GetBrokerLinkageGetBrokerlinkagesIdResponse404.md)
 - [JuniferAuNativeApi.GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200](docs/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.md)
 - [JuniferAuNativeApi.GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links](docs/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links.md)
 - [JuniferAuNativeApi.GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse404](docs/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse404.md)
 - [JuniferAuNativeApi.GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200](docs/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200.md)
 - [JuniferAuNativeApi.GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404](docs/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404.md)
 - [JuniferAuNativeApi.GetCommunicationsFileGetCommunicationsfilesIdResponse200](docs/GetCommunicationsFileGetCommunicationsfilesIdResponse200.md)
 - [JuniferAuNativeApi.GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200](docs/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200.md)
 - [JuniferAuNativeApi.GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse410](docs/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse410.md)
 - [JuniferAuNativeApi.GetCommunicationsGetCommunicationsIdResponse200](docs/GetCommunicationsGetCommunicationsIdResponse200.md)
 - [JuniferAuNativeApi.GetCommunicationsGetCommunicationsIdResponse200Links](docs/GetCommunicationsGetCommunicationsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetCommunicationsGetCommunicationsIdResponse404](docs/GetCommunicationsGetCommunicationsIdResponse404.md)
 - [JuniferAuNativeApi.GetContactGetContactsIdResponse200](docs/GetContactGetContactsIdResponse200.md)
 - [JuniferAuNativeApi.GetContactGetContactsIdResponse200Address](docs/GetContactGetContactsIdResponse200Address.md)
 - [JuniferAuNativeApi.GetContactGetContactsIdResponse400](docs/GetContactGetContactsIdResponse400.md)
 - [JuniferAuNativeApi.GetContactsAccountsGetContactsIdAccountsResponse200](docs/GetContactsAccountsGetContactsIdAccountsResponse200.md)
 - [JuniferAuNativeApi.GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress](docs/GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.md)
 - [JuniferAuNativeApi.GetContactsAccountsGetContactsIdAccountsResponse200Links](docs/GetContactsAccountsGetContactsIdAccountsResponse200Links.md)
 - [JuniferAuNativeApi.GetContactsAccountsGetContactsIdAccountsResponse400](docs/GetContactsAccountsGetContactsIdAccountsResponse400.md)
 - [JuniferAuNativeApi.GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200](docs/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200.md)
 - [JuniferAuNativeApi.GetCreditFileGetCreditfilesIdResponse200](docs/GetCreditFileGetCreditfilesIdResponse200.md)
 - [JuniferAuNativeApi.GetCreditFileGetCreditfilesIdResponse200Links](docs/GetCreditFileGetCreditfilesIdResponse200Links.md)
 - [JuniferAuNativeApi.GetCreditFileGetCreditfilesIdResponse404](docs/GetCreditFileGetCreditfilesIdResponse404.md)
 - [JuniferAuNativeApi.GetCreditFileImageGetCreditfilesIdImageResponse200](docs/GetCreditFileImageGetCreditfilesIdImageResponse200.md)
 - [JuniferAuNativeApi.GetCreditFileImageGetCreditfilesIdImageResponse410](docs/GetCreditFileImageGetCreditfilesIdImageResponse410.md)
 - [JuniferAuNativeApi.GetCreditGetCreditsIdResponse200](docs/GetCreditGetCreditsIdResponse200.md)
 - [JuniferAuNativeApi.GetCreditGetCreditsIdResponse200CreditFiles](docs/GetCreditGetCreditsIdResponse200CreditFiles.md)
 - [JuniferAuNativeApi.GetCreditGetCreditsIdResponse200Links](docs/GetCreditGetCreditsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetCreditGetCreditsIdResponse200Links1](docs/GetCreditGetCreditsIdResponse200Links1.md)
 - [JuniferAuNativeApi.GetCreditGetCreditsIdResponse404](docs/GetCreditGetCreditsIdResponse404.md)
 - [JuniferAuNativeApi.GetCustomerAccountsGetCustomersIdAccountsResponse200](docs/GetCustomerAccountsGetCustomersIdAccountsResponse200.md)
 - [JuniferAuNativeApi.GetCustomerAccountsGetCustomersIdAccountsResponse200Links](docs/GetCustomerAccountsGetCustomersIdAccountsResponse200Links.md)
 - [JuniferAuNativeApi.GetCustomerAccountsGetCustomersIdAccountsResponse400](docs/GetCustomerAccountsGetCustomersIdAccountsResponse400.md)
 - [JuniferAuNativeApi.GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200](docs/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200.md)
 - [JuniferAuNativeApi.GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400](docs/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400.md)
 - [JuniferAuNativeApi.GetCustomerConsentsGetCustomersIdConsentsResponse200](docs/GetCustomerConsentsGetCustomersIdConsentsResponse200.md)
 - [JuniferAuNativeApi.GetCustomerConsentsGetCustomersIdConsentsResponse400](docs/GetCustomerConsentsGetCustomersIdConsentsResponse400.md)
 - [JuniferAuNativeApi.GetCustomerGetCustomersIdResponse200](docs/GetCustomerGetCustomersIdResponse200.md)
 - [JuniferAuNativeApi.GetCustomerGetCustomersIdResponse200CompanyAddress](docs/GetCustomerGetCustomersIdResponse200CompanyAddress.md)
 - [JuniferAuNativeApi.GetCustomerGetCustomersIdResponse200Links](docs/GetCustomerGetCustomersIdResponse200Links.md)
 - [JuniferAuNativeApi.GetCustomerGetCustomersIdResponse200PrimaryContact](docs/GetCustomerGetCustomersIdResponse200PrimaryContact.md)
 - [JuniferAuNativeApi.GetCustomerGetCustomersIdResponse200PrimaryContactAddress](docs/GetCustomerGetCustomersIdResponse200PrimaryContactAddress.md)
 - [JuniferAuNativeApi.GetCustomerGetCustomersIdResponse400](docs/GetCustomerGetCustomersIdResponse400.md)
 - [JuniferAuNativeApi.GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200](docs/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200.md)
 - [JuniferAuNativeApi.GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links](docs/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links.md)
 - [JuniferAuNativeApi.GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400](docs/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3.md)
 - [JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse404](docs/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse404.md)
 - [JuniferAuNativeApi.GetMeterPointsGetPropertysIdMeterpointsResponse200](docs/GetMeterPointsGetPropertysIdMeterpointsResponse200.md)
 - [JuniferAuNativeApi.GetMeterPointsGetPropertysIdMeterpointsResponse404](docs/GetMeterPointsGetPropertysIdMeterpointsResponse404.md)
 - [JuniferAuNativeApi.GetMeterReadingsGetAuMeterpointsIdReadingsResponse200](docs/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.md)
 - [JuniferAuNativeApi.GetMeterReadingsGetAuMeterpointsIdReadingsResponse400](docs/GetMeterReadingsGetAuMeterpointsIdReadingsResponse400.md)
 - [JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200](docs/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200.md)
 - [JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsIdMeterstructureResponse400](docs/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse400.md)
 - [JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsMeterstructureResponse200](docs/GetMeterStructureGetAuMeterpointsMeterstructureResponse200.md)
 - [JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links](docs/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links.md)
 - [JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters](docs/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters.md)
 - [JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers](docs/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers.md)
 - [JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsMeterstructureResponse400](docs/GetMeterStructureGetAuMeterpointsMeterstructureResponse400.md)
 - [JuniferAuNativeApi.GetOfferGetOffersIdResponse200](docs/GetOfferGetOffersIdResponse200.md)
 - [JuniferAuNativeApi.GetOfferGetOffersIdResponse200Links](docs/GetOfferGetOffersIdResponse200Links.md)
 - [JuniferAuNativeApi.GetOfferGetOffersIdResponse200MeterPoints](docs/GetOfferGetOffersIdResponse200MeterPoints.md)
 - [JuniferAuNativeApi.GetOfferGetOffersIdResponse200MeterPointsLinks](docs/GetOfferGetOffersIdResponse200MeterPointsLinks.md)
 - [JuniferAuNativeApi.GetOfferGetOffersIdResponse200PaymentParams](docs/GetOfferGetOffersIdResponse200PaymentParams.md)
 - [JuniferAuNativeApi.GetOfferGetOffersIdResponse200Quotes](docs/GetOfferGetOffersIdResponse200Quotes.md)
 - [JuniferAuNativeApi.GetOfferGetOffersIdResponse200QuotesLinks](docs/GetOfferGetOffersIdResponse200QuotesLinks.md)
 - [JuniferAuNativeApi.GetOfferGetOffersIdResponse404](docs/GetOfferGetOffersIdResponse404.md)
 - [JuniferAuNativeApi.GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200](docs/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200.md)
 - [JuniferAuNativeApi.GetPaymentGetPaymentsIdResponse200](docs/GetPaymentGetPaymentsIdResponse200.md)
 - [JuniferAuNativeApi.GetPaymentGetPaymentsIdResponse404](docs/GetPaymentGetPaymentsIdResponse404.md)
 - [JuniferAuNativeApi.GetPaymentMethodGetPaymentmethodsIdResponse200](docs/GetPaymentMethodGetPaymentmethodsIdResponse200.md)
 - [JuniferAuNativeApi.GetPaymentMethodGetPaymentmethodsIdResponse404](docs/GetPaymentMethodGetPaymentmethodsIdResponse404.md)
 - [JuniferAuNativeApi.GetPaymentPlanGetPaymentplansIdResponse200](docs/GetPaymentPlanGetPaymentplansIdResponse200.md)
 - [JuniferAuNativeApi.GetPaymentPlanGetPaymentplansIdResponse200Links](docs/GetPaymentPlanGetPaymentplansIdResponse200Links.md)
 - [JuniferAuNativeApi.GetPaymentPlanGetPaymentplansIdResponse404](docs/GetPaymentPlanGetPaymentplansIdResponse404.md)
 - [JuniferAuNativeApi.GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200](docs/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200.md)
 - [JuniferAuNativeApi.GetPaymentRequestGetPaymentrequestsIdResponse200](docs/GetPaymentRequestGetPaymentrequestsIdResponse200.md)
 - [JuniferAuNativeApi.GetPaymentRequestGetPaymentrequestsIdResponse200Links](docs/GetPaymentRequestGetPaymentrequestsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetPaymentRequestGetPaymentrequestsIdResponse404](docs/GetPaymentRequestGetPaymentrequestsIdResponse404.md)
 - [JuniferAuNativeApi.GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200](docs/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.md)
 - [JuniferAuNativeApi.GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails](docs/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails.md)
 - [JuniferAuNativeApi.GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404](docs/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404.md)
 - [JuniferAuNativeApi.GetProductsForNmiGetAuProductsTariffsfornmiResponse200](docs/GetProductsForNmiGetAuProductsTariffsfornmiResponse200.md)
 - [JuniferAuNativeApi.GetProductsForNmiGetAuProductsTariffsfornmiResponse400](docs/GetProductsForNmiGetAuProductsTariffsfornmiResponse400.md)
 - [JuniferAuNativeApi.GetPropertyGetPropertysIdResponse200](docs/GetPropertyGetPropertysIdResponse200.md)
 - [JuniferAuNativeApi.GetPropertyGetPropertysIdResponse200Address](docs/GetPropertyGetPropertysIdResponse200Address.md)
 - [JuniferAuNativeApi.GetPropertyGetPropertysIdResponse404](docs/GetPropertyGetPropertysIdResponse404.md)
 - [JuniferAuNativeApi.GetProspectGetProspectsIdResponse200](docs/GetProspectGetProspectsIdResponse200.md)
 - [JuniferAuNativeApi.GetProspectGetProspectsIdResponse200Links](docs/GetProspectGetProspectsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetProspectGetProspectsIdResponse404](docs/GetProspectGetProspectsIdResponse404.md)
 - [JuniferAuNativeApi.GetProspectOffersGetProspectsIdOffersResponse200](docs/GetProspectOffersGetProspectsIdOffersResponse200.md)
 - [JuniferAuNativeApi.GetProspectOffersGetProspectsIdOffersResponse200Links](docs/GetProspectOffersGetProspectsIdOffersResponse200Links.md)
 - [JuniferAuNativeApi.GetProspectOffersGetProspectsIdOffersResponse404](docs/GetProspectOffersGetProspectsIdOffersResponse404.md)
 - [JuniferAuNativeApi.GetProspectPropertiesGetProspectsIdPropertysResponse200](docs/GetProspectPropertiesGetProspectsIdPropertysResponse200.md)
 - [JuniferAuNativeApi.GetProspectPropertiesGetProspectsIdPropertysResponse200Links](docs/GetProspectPropertiesGetProspectsIdPropertysResponse200Links.md)
 - [JuniferAuNativeApi.GetProspectPropertiesGetProspectsIdPropertysResponse404](docs/GetProspectPropertiesGetProspectsIdPropertysResponse404.md)
 - [JuniferAuNativeApi.GetQuoteFileGetQuotefilesIdResponse200](docs/GetQuoteFileGetQuotefilesIdResponse200.md)
 - [JuniferAuNativeApi.GetQuoteFileGetQuotefilesIdResponse200Links](docs/GetQuoteFileGetQuotefilesIdResponse200Links.md)
 - [JuniferAuNativeApi.GetQuoteFileGetQuotefilesIdResponse404](docs/GetQuoteFileGetQuotefilesIdResponse404.md)
 - [JuniferAuNativeApi.GetQuoteFileImageGetQuotefilesIdImageResponse200](docs/GetQuoteFileImageGetQuotefilesIdImageResponse200.md)
 - [JuniferAuNativeApi.GetQuoteGetQuotesIdResponse200](docs/GetQuoteGetQuotesIdResponse200.md)
 - [JuniferAuNativeApi.GetQuoteGetQuotesIdResponse200BrokerMargins](docs/GetQuoteGetQuotesIdResponse200BrokerMargins.md)
 - [JuniferAuNativeApi.GetQuoteGetQuotesIdResponse200QuoteFiles](docs/GetQuoteGetQuotesIdResponse200QuoteFiles.md)
 - [JuniferAuNativeApi.GetQuoteGetQuotesIdResponse200QuoteFilesLinks](docs/GetQuoteGetQuotesIdResponse200QuoteFilesLinks.md)
 - [JuniferAuNativeApi.GetQuoteGetQuotesIdResponse404](docs/GetQuoteGetQuotesIdResponse404.md)
 - [JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200](docs/GetQuotesGetOffersIdQuotesResponse200.md)
 - [JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200Links](docs/GetQuotesGetOffersIdQuotesResponse200Links.md)
 - [JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200MeterPoints](docs/GetQuotesGetOffersIdQuotesResponse200MeterPoints.md)
 - [JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks](docs/GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks.md)
 - [JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices](docs/GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices.md)
 - [JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents](docs/GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents.md)
 - [JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse404](docs/GetQuotesGetOffersIdQuotesResponse404.md)
 - [JuniferAuNativeApi.GetSepaDirectDebitGetSepadirectdebitsIdResponse200](docs/GetSepaDirectDebitGetSepadirectdebitsIdResponse200.md)
 - [JuniferAuNativeApi.GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links](docs/GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200](docs/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.md)
 - [JuniferAuNativeApi.GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse400](docs/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse400.md)
 - [JuniferAuNativeApi.GetStripePaymentCardGetStripepaymentcardsIdResponse200](docs/GetStripePaymentCardGetStripepaymentcardsIdResponse200.md)
 - [JuniferAuNativeApi.GetStripePaymentCardGetStripepaymentcardsIdResponse200Links](docs/GetStripePaymentCardGetStripepaymentcardsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetStripePaymentCardGetStripepaymentcardsIdResponse404](docs/GetStripePaymentCardGetStripepaymentcardsIdResponse404.md)
 - [JuniferAuNativeApi.GetTicketGetTicketsIdResponse200](docs/GetTicketGetTicketsIdResponse200.md)
 - [JuniferAuNativeApi.GetTicketGetTicketsIdResponse200Links](docs/GetTicketGetTicketsIdResponse200Links.md)
 - [JuniferAuNativeApi.GetTicketGetTicketsIdResponse400](docs/GetTicketGetTicketsIdResponse400.md)
 - [JuniferAuNativeApi.GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200](docs/GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200.md)
 - [JuniferAuNativeApi.HotbillPostAccountsIdHotbillResponse400](docs/HotbillPostAccountsIdHotbillResponse400.md)
 - [JuniferAuNativeApi.ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200](docs/ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200.md)
 - [JuniferAuNativeApi.LookUpAccountGetAccountsResponse200](docs/LookUpAccountGetAccountsResponse200.md)
 - [JuniferAuNativeApi.LookUpAccountGetAccountsResponse200BillingAddress](docs/LookUpAccountGetAccountsResponse200BillingAddress.md)
 - [JuniferAuNativeApi.LookUpAccountGetAccountsResponse200Links](docs/LookUpAccountGetAccountsResponse200Links.md)
 - [JuniferAuNativeApi.LookUpAccountGetAccountsResponse400](docs/LookUpAccountGetAccountsResponse400.md)
 - [JuniferAuNativeApi.LookUpCustomerGetCustomersResponse200](docs/LookUpCustomerGetCustomersResponse200.md)
 - [JuniferAuNativeApi.LookUpCustomerGetCustomersResponse200CompanyAddress](docs/LookUpCustomerGetCustomersResponse200CompanyAddress.md)
 - [JuniferAuNativeApi.LookUpCustomerGetCustomersResponse200Links](docs/LookUpCustomerGetCustomersResponse200Links.md)
 - [JuniferAuNativeApi.LookUpCustomerGetCustomersResponse400](docs/LookUpCustomerGetCustomersResponse400.md)
 - [JuniferAuNativeApi.LookUpMeterPointGetAuMeterpointsResponse200](docs/LookUpMeterPointGetAuMeterpointsResponse200.md)
 - [JuniferAuNativeApi.LookUpMeterPointGetAuMeterpointsResponse400](docs/LookUpMeterPointGetAuMeterpointsResponse400.md)
 - [JuniferAuNativeApi.LookUpTicketsGetTicketsResponse200](docs/LookUpTicketsGetTicketsResponse200.md)
 - [JuniferAuNativeApi.LookUpTicketsGetTicketsResponse400](docs/LookUpTicketsGetTicketsResponse400.md)
 - [JuniferAuNativeApi.LookupBrokerGetBrokersResponse200](docs/LookupBrokerGetBrokersResponse200.md)
 - [JuniferAuNativeApi.LookupBrokerGetBrokersResponse200Links](docs/LookupBrokerGetBrokersResponse200Links.md)
 - [JuniferAuNativeApi.LookupBrokerGetBrokersResponse400](docs/LookupBrokerGetBrokersResponse400.md)
 - [JuniferAuNativeApi.LookupBrokerGetBrokersResponse404](docs/LookupBrokerGetBrokersResponse404.md)
 - [JuniferAuNativeApi.NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody](docs/NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody.md)
 - [JuniferAuNativeApi.NewBpointDirectDebitPostAuBpointdirectdebitsResponse200](docs/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.md)
 - [JuniferAuNativeApi.NewBpointDirectDebitPostAuBpointdirectdebitsResponse400](docs/NewBpointDirectDebitPostAuBpointdirectdebitsResponse400.md)
 - [JuniferAuNativeApi.NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody](docs/NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody.md)
 - [JuniferAuNativeApi.NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200](docs/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200.md)
 - [JuniferAuNativeApi.NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse400](docs/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse400.md)
 - [JuniferAuNativeApi.NewSepaDirectDebitPostSepadirectdebitsRequestBody](docs/NewSepaDirectDebitPostSepadirectdebitsRequestBody.md)
 - [JuniferAuNativeApi.NewSepaDirectDebitPostSepadirectdebitsResponse200](docs/NewSepaDirectDebitPostSepadirectdebitsResponse200.md)
 - [JuniferAuNativeApi.NewSepaDirectDebitPostSepadirectdebitsResponse200Links](docs/NewSepaDirectDebitPostSepadirectdebitsResponse200Links.md)
 - [JuniferAuNativeApi.NewSepaDirectDebitPostSepadirectdebitsResponse400](docs/NewSepaDirectDebitPostSepadirectdebitsResponse400.md)
 - [JuniferAuNativeApi.NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody](docs/NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.md)
 - [JuniferAuNativeApi.NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200](docs/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.md)
 - [JuniferAuNativeApi.NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400](docs/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400.md)
 - [JuniferAuNativeApi.PaymentPostAccountsIdPaymentRequestBody](docs/PaymentPostAccountsIdPaymentRequestBody.md)
 - [JuniferAuNativeApi.PaymentPostAccountsIdPaymentResponse200](docs/PaymentPostAccountsIdPaymentResponse200.md)
 - [JuniferAuNativeApi.PaymentPostAccountsIdPaymentResponse400](docs/PaymentPostAccountsIdPaymentResponse400.md)
 - [JuniferAuNativeApi.RenewAccountPostAccountsIdRenewalRequestBody](docs/RenewAccountPostAccountsIdRenewalRequestBody.md)
 - [JuniferAuNativeApi.RenewAccountPostAccountsIdRenewalResponse200](docs/RenewAccountPostAccountsIdRenewalResponse200.md)
 - [JuniferAuNativeApi.RenewAccountPostAccountsIdRenewalResponse400](docs/RenewAccountPostAccountsIdRenewalResponse400.md)
 - [JuniferAuNativeApi.RestartFailedBillRequestPostBillrequestsIdRestartfailedResponse404](docs/RestartFailedBillRequestPostBillrequestsIdRestartfailedResponse404.md)
 - [JuniferAuNativeApi.ReversionBillPostBillsIdReversionResponse200](docs/ReversionBillPostBillsIdReversionResponse200.md)
 - [JuniferAuNativeApi.ReversionBillPostBillsIdReversionResponse200Links](docs/ReversionBillPostBillsIdReversionResponse200Links.md)
 - [JuniferAuNativeApi.ReversionBillPostBillsIdReversionResponse404](docs/ReversionBillPostBillsIdReversionResponse404.md)
 - [JuniferAuNativeApi.StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody](docs/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody.md)
 - [JuniferAuNativeApi.StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400](docs/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400.md)
 - [JuniferAuNativeApi.StoreCardPostStripepaymentcardsRequestBody](docs/StoreCardPostStripepaymentcardsRequestBody.md)
 - [JuniferAuNativeApi.StoreCardPostStripepaymentcardsResponse400](docs/StoreCardPostStripepaymentcardsResponse400.md)
 - [JuniferAuNativeApi.SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody](docs/SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.md)
 - [JuniferAuNativeApi.SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400](docs/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400.md)
 - [JuniferAuNativeApi.TariffInformationGetAccountsIdTariffinformationResponse200](docs/TariffInformationGetAccountsIdTariffinformationResponse200.md)
 - [JuniferAuNativeApi.TariffInformationGetAccountsIdTariffinformationResponse200TIL](docs/TariffInformationGetAccountsIdTariffinformationResponse200TIL.md)
 - [JuniferAuNativeApi.TariffInformationGetAccountsIdTariffinformationResponse400](docs/TariffInformationGetAccountsIdTariffinformationResponse400.md)
 - [JuniferAuNativeApi.UpdateAccountContactPutAccountsIdContactsContactidRequestBody](docs/UpdateAccountContactPutAccountsIdContactsContactidRequestBody.md)
 - [JuniferAuNativeApi.UpdateAccountContactPutAccountsIdContactsContactidResponse400](docs/UpdateAccountContactPutAccountsIdContactsContactidResponse400.md)
 - [JuniferAuNativeApi.UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody](docs/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.md)
 - [JuniferAuNativeApi.UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400](docs/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400.md)
 - [JuniferAuNativeApi.UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody](docs/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.md)
 - [JuniferAuNativeApi.UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400](docs/UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400.md)
 - [JuniferAuNativeApi.UpdateCustomerConsentsPutCustomersIdConsentsRequestBody](docs/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody.md)
 - [JuniferAuNativeApi.UpdateCustomerConsentsPutCustomersIdConsentsResponse400](docs/UpdateCustomerConsentsPutCustomersIdConsentsResponse400.md)
 - [JuniferAuNativeApi.UpdateCustomerContactPutCustomersIdContactsContactidRequestBody](docs/UpdateCustomerContactPutCustomersIdContactsContactidRequestBody.md)
 - [JuniferAuNativeApi.UpdateCustomerContactPutCustomersIdContactsContactidResponse400](docs/UpdateCustomerContactPutCustomersIdContactsContactidResponse400.md)
 - [JuniferAuNativeApi.UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody](docs/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.md)
 - [JuniferAuNativeApi.UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400](docs/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400.md)
 - [JuniferAuNativeApi.UpdateCustomerPutCustomersIdRequestBody](docs/UpdateCustomerPutCustomersIdRequestBody.md)
 - [JuniferAuNativeApi.UpdateCustomerPutCustomersIdRequestBodyPrimaryContact](docs/UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.md)
 - [JuniferAuNativeApi.UpdateCustomerPutCustomersIdResponse400](docs/UpdateCustomerPutCustomersIdResponse400.md)
 - [JuniferAuNativeApi.UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody](docs/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody.md)
 - [JuniferAuNativeApi.UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences](docs/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.md)
 - [JuniferAuNativeApi.UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse400](docs/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse400.md)
 - [JuniferAuNativeApi.UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse404](docs/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse404.md)
 - [JuniferAuNativeApi.UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody](docs/UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody.md)
 - [JuniferAuNativeApi.UpdateSepaDirectDebitPutSepadirectdebitsIdResponse400](docs/UpdateSepaDirectDebitPutSepadirectdebitsIdResponse400.md)
 - [JuniferAuNativeApi.UpdateTicketPutTicketsIdRequestBody](docs/UpdateTicketPutTicketsIdRequestBody.md)
 - [JuniferAuNativeApi.UpdateTicketPutTicketsIdResponse400](docs/UpdateTicketPutTicketsIdResponse400.md)


## Documentation for Authorization



### tokenAuth


- **Type**: API key
- **API key parameter name**: Authorization
- **Location**: HTTP header

