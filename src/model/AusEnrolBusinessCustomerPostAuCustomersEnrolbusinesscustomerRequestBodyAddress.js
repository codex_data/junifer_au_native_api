/**
 * Junifer AU Native API
 * Junifer Native Web API provided by the Gentrack Cloud Integration Services  # How It Works  For how to prepare your application to call Junifer Native Web API, please refer to: * [Read me first](https://portal.integration.gentrack.cloud/api/index.html#section/Read-me-first) * [Sovereignty Regions](https://portal.integration.gentrack.cloud/api/index.html#section/Sovereignty-Regions) * [Preparing your request](https://portal.integration.gentrack.cloud/api/index.html#section/Preparing-your-request) * [Authentication](https://portal.integration.gentrack.cloud/api/index.html#section/Authentication) * [Responses](https://portal.integration.gentrack.cloud/api/index.html#section/Responses)  ## Sample of requesting contact details The following is an example of calling get contact details API in UK region:  ```bash curl -X GET \\     https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJZCI6IjA2NDYxODczLTFlOWMtNGVkMy1iZWZkLTU3NGY5ZmEwYjExYyIsIm9yZ0lkIj oiQzAwMDAiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJpYXQiOjE1MzA1ODkwODcsImV4cCI6MTUzMDU5MjY4NywiaXNzIjoiaHR0cHM6Ly9hcGkuZ2VudHJhY2suaW8vIn0.WUAFYWTTEAy Q0Bt4YXu-mxCXd-Y9ehwdZcYvcGNnyLTZH_hiJjtXWWfsx69M606pvCP6lLMT7MfK-F3E4rLO6KAlGsuz4A_7oVWQf4QNeR178GnwgmqQRw5OnLxwPbPrRa9nODngwGq8dcWejhmEYU6i w02bvYdQBHnnsc3Kpyzw7Wdv_3jnBS4TPYS20muQOgG6KxRp9hLJM7ERLoAbsULwqdPOV8eUJJhGrq1NDuH_lA83YRDZmCWEzw96tSm3hb7y88kXs-4OvamnO1m5wFPBx69VximlS4Ltr 3ztqU2s3fHoj0OJLIafge9JvTgvuB6noHfs1uSRaahvstGJAA' ```  The sample of response is as follows.  ```json {    \"id\": 1010,    \"contactType\": \"Business\",    \"title\": \"Mr\",    \"forename\": \"Tony\",    \"surname\": \"Soprano\",    \"email\": \"bigtone@didntseeanything.com\",    \"dateOfBirth\": \"1959-08-24\",    \"phoneNumber1\": \"44626478370\",    \"address\":{      \"address1\": \"633 Stag Trail Road\",      \"address2\": \"North Caldwell\",      \"address3\": \"New Jersey\",      \"postcode\": \"NE18 0PY\"    },    \"links\": {      \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010\",      \"accounts\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010/accounts\"    } } ```  # General Rules/Patterns  ## Navigation A links section is returned in most requests.  This section contains how to access the entity itself and, more importantly, how to access items related to the entity.  For example take the following response: ```json {   \"id\": 1,   \"name\": \"Mr Joe Bloggs\",   \"number\": \"00000001\",   \"surname\": \"Bloggs\",   \"links\": {     \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\"   },   \"accounts\": [     {       \"id\": 7,       \"name\": \"Invoice\",       \"number\": \"00000001\",       \"currency\": \"GBP\",       \"fromDt\": \"2013-05-01\",       \"createdDttm\": \"2013-05-08T13:36:34.000\",       \"balance\": 0,       \"billingAddress\": {         \"address1\": \"11 Tuppy Street\"       },       \"links\": {         \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7\",         \"customer\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\",         \"bills\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/bills\",         \"payments\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/payments\"       }     }   ] } ```  The `self` URL in the `links` section can be used to access the entities listed in the response. In the above example there are self fields for the customer itself, and the accounts under this customer. There are other links under the account.  For example the above example contains links for the following:  - `bills` – List of Bills for the Account - `payments` – List of Payments for the Account - `customer` – Link back to the Account’s Customer  ## About IDs  The id in the API requests (JSON payload and URL) and responses identifies the resource within the API. It has no business meaning or purpose, therefore shouldn't be exposed to the end user in any way.  As the IDs of the resources aren't sequential or aligned with any other resource IDs, it is highly recommended using the links section from the responses to navigate to other related resources. Given the example above, the customer with ID 1 links to the account with ID 7. The links section will populate the correct IDs in the URLs so none of the assumptions about related resource IDs need to be made by the implementer.  # Accessing accounts by account number Throughout this API, anywhere an account is accessed by its `id` (i.e. the URL includes `/accounts/:id`), it can also be accessed by its account number by replacing `/accounts/:id` with `accounts/accountNumber/:num`.  For example: the Create Account Credit endpoint, available at `/accounts/:id/accountCredits`, can also be accessed at `/accounts/accountNumber/:num/accountCredits`.  # Standard data types in JSON The standard data types are as follows:  - String – Quoted as per JSON standard - Number – Unquoted as per JSON standard - Decimal – Unquoted as per JSON standard - Date – In the standard ISO format \"yyyy-MM-dd\" - DateTime – In the standard ISO format \"yyyy-MM-ddThh:mm:ss.SSS\" (no time zone component) - Boolean – Unquoted 'true' and 'false' are the only valid values  # Ordering of data Response representations are not ordered unless specified on a specific resource. Therefore, clients should be implemented to use the name of fields/objects as the key for accessing data.  For example in JSON objects are comprised of key/value pairs and clients should be implemented to search on the ‘key’ value rather than looking for the nth key/value pair in the object.  # Changes of data Response representations may change without warning in the following ways.  - Additional key/value pairs may be added to an existing object - Additional sub-objects may be added to an existing object  These changes are considered non-breaking.  Therefore, clients should be implemented to consume only the data they need and ignore any other information.  # Versioning The API is usually changed in each release of Junifer. Navigate to different versions by clicking the dropdown menu in the top right of this web page.  # Pagination Some REST APIs that provide a list of results support pagination.  Pagination allows API users to query potential large data sets but only receive a sub-set of the results on the initial query. Subsequent queries can use the response of a prior query to retrieve the next or previous set of results.  Pagination functionality and behaviour will be documented on each API that requires it.  ## Pagination on existing APIs Existing APIs may have pagination support added. In the future this pagination will become mandatory.  In order to facilitate API users planning and executing upgrades against the existing APIs, pagination is optional during a transition period. API users can enable pagination on these APIs by specifying the `pagination` query parameter. This parameter only needs to be present, the value is not required or checked. If this query parameter is specified, then pagination will be enabled. Otherwise, the existing non-paginated functionality, that returns all results, will be used.  API users must plan and execute upgrades to their existing applications to use the pagination support during the transition period. At the end of the transition period pagination will become mandatory on these APIs.  For new APIs that support pagination, it will be mandatory with the initial release of the API.  ## Ordering The ordering that the API provides will be documented for each API.  An ordering is always required to ensure moving between pages is consistent.  Some APIs may define an ordering on a value or values in the response where appropriate. For example results may be ordered by a date/time property of the results.  Other APIs may not define an explicit ordering but will guarantee the order is consistent for multiple API calls.  ## Limiting results of each page The `desiredResults` query parameter can be used to specify the maximum number of results that may be returned.  The APIs treat this as a hint so API callers should expect to sometimes receive a different number of results. APIs will endeavour to ensure no more than `desiredResults` are returned but for small values of `desiredResults` (less than 10) this may not be possible. This is due to how pages of data are structured inside the product. The API will prefer to return some data rather than no data where it is not possible to return less than `desiredResults`  If `desiredResults` is not specified the default maximum value for the API will be used. Unless documented differently, this value is 100.  If `desiredResults` is specified higher than the default maximum for the API a 400 error response will be received.  ## Cursors The pagination provided by the APIs is cursor-based.  In the JSON response additional metadata will be provided when pagination is present.  - `after` cursor points to the start of the results that have been returned - `before` cursor points to the end of the results that have been returned  These cursors can be used to make subsequent API calls. The API URL should be unchanged except for setting the `before` or `after` query parameter. If both parameters are specified, then an error 400 response will be received.  Cursor tokens must only be used for pagination purposes. API users should not infer any business meaning or logic from cursor representations. These representations may change without warning when Junifer is upgraded.  When scrolling forward the `after` cursor will be absent from the response when there are no more results after the current set.  When scrolling backwards the `before` cursor will be absent from the response when there are no more results before the current set.  In addition, a paginated response will include a `next` and `previous` URLs for convenience.  ## Best practices The following best practices must be adhered to by users of the pagination APIs:  - Don't store cursors or next and previous URLs for long. Cursors can quickly become invalid if items are added or deleted to the data set - If an invalid cursor is used, then a 404 response will be returned in this situation. API users should design a policy to handle this situation - API users should handle empty data sets. In limited cases a pagination query using a previously valid cursor may return no results  ## Example  This example is illustrative only.  A small `desiredResults` parameter has been used to make the example concise. API users should generally choose larger values, subject to the maximum value.  The following API call would retrieve the *first* set of results.  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?desiredResults=3 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-20T00:00:00.000\",       \"creditBalance\": 109,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-19T00:00:00.000\",       \"creditBalance\": 108,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-18T00:00:00.000\",       \"creditBalance\": 107,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"after\": \"107\"     },     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107\"   } } ```  The `results` section contains the payload of the API.  The `pagination` section contains details needed to retrieve additional results.  API users can choose to use the `next` URL directly or use the `after` cursor token to construct the URL themselves.  The API call to query the next set of results would be  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-17T00:00:00.000\",       \"creditBalance\": 106,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-16T00:00:00.000\",       \"creditBalance\": 105,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-15T00:00:00.000\",       \"creditBalance\": 104,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"before\": \"106\",       \"after\": \"104\"     },     \"previous\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&before=106&desiredResults=3\",     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=104\"   } } ```  This time the response contains both `before` and `after` tokens and `previous` and `next` URLs. The `before` token or `previous` link can be used to navigate backwards in the results. 
 *
 * The version of the OpenAPI document: 1.61.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress model module.
 * @module model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress
 * @version 1.61.1
 */
class AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress {
    /**
     * Constructs a new <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress</code>.
     * Contact&#39;s address to which communications will be sent in the case where &#x60;billingMethod&#x60; is &#x60;Both&#x60; or &#x60;Paper&#x60;. Mandatory for primary contact.
     * @alias module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress
     * @param countryCode {String} An ISO country code. AU is the the only acceptable value!
     */
    constructor(countryCode) { 
        
        AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.initialize(this, countryCode);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, countryCode) { 
        obj['countryCode'] = countryCode;
    }

    /**
     * Constructs a <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress} obj Optional instance to populate.
     * @return {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress} The populated <code>AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress();

            if (data.hasOwnProperty('')) {
                obj[''] = ApiClient.convertToType(data[''], 'String');
            }
            if (data.hasOwnProperty('buildingOrPropertyName')) {
                obj['buildingOrPropertyName'] = ApiClient.convertToType(data['buildingOrPropertyName'], 'String');
            }
            if (data.hasOwnProperty('buildingOrPropertyName2')) {
                obj['buildingOrPropertyName2'] = ApiClient.convertToType(data['buildingOrPropertyName2'], 'String');
            }
            if (data.hasOwnProperty('lotNumber')) {
                obj['lotNumber'] = ApiClient.convertToType(data['lotNumber'], 'String');
            }
            if (data.hasOwnProperty('flatOrUnitType')) {
                obj['flatOrUnitType'] = ApiClient.convertToType(data['flatOrUnitType'], 'String');
            }
            if (data.hasOwnProperty('flatOrUnitNumber')) {
                obj['flatOrUnitNumber'] = ApiClient.convertToType(data['flatOrUnitNumber'], 'String');
            }
            if (data.hasOwnProperty('floorOrLevelType')) {
                obj['floorOrLevelType'] = ApiClient.convertToType(data['floorOrLevelType'], 'String');
            }
            if (data.hasOwnProperty('floorOrLevelNumber')) {
                obj['floorOrLevelNumber'] = ApiClient.convertToType(data['floorOrLevelNumber'], 'String');
            }
            if (data.hasOwnProperty('houseNumber')) {
                obj['houseNumber'] = ApiClient.convertToType(data['houseNumber'], 'String');
            }
            if (data.hasOwnProperty('houseNumberSuffix')) {
                obj['houseNumberSuffix'] = ApiClient.convertToType(data['houseNumberSuffix'], 'String');
            }
            if (data.hasOwnProperty('houseNumber2')) {
                obj['houseNumber2'] = ApiClient.convertToType(data['houseNumber2'], 'String');
            }
            if (data.hasOwnProperty('houseNumberSuffix2')) {
                obj['houseNumberSuffix2'] = ApiClient.convertToType(data['houseNumberSuffix2'], 'String');
            }
            if (data.hasOwnProperty('streetName')) {
                obj['streetName'] = ApiClient.convertToType(data['streetName'], 'String');
            }
            if (data.hasOwnProperty('streetType')) {
                obj['streetType'] = ApiClient.convertToType(data['streetType'], 'String');
            }
            if (data.hasOwnProperty('streetSuffix')) {
                obj['streetSuffix'] = ApiClient.convertToType(data['streetSuffix'], 'String');
            }
            if (data.hasOwnProperty('suburbOrPlaceOrLocality')) {
                obj['suburbOrPlaceOrLocality'] = ApiClient.convertToType(data['suburbOrPlaceOrLocality'], 'String');
            }
            if (data.hasOwnProperty('locationDescriptor')) {
                obj['locationDescriptor'] = ApiClient.convertToType(data['locationDescriptor'], 'String');
            }
            if (data.hasOwnProperty('stateOrTerritory')) {
                obj['stateOrTerritory'] = ApiClient.convertToType(data['stateOrTerritory'], 'String');
            }
            if (data.hasOwnProperty('deliveryPointIdentifier')) {
                obj['deliveryPointIdentifier'] = ApiClient.convertToType(data['deliveryPointIdentifier'], 'String');
            }
            if (data.hasOwnProperty('postalDeliveryType')) {
                obj['postalDeliveryType'] = ApiClient.convertToType(data['postalDeliveryType'], 'String');
            }
            if (data.hasOwnProperty('postalDeliveryNumberPrefix')) {
                obj['postalDeliveryNumberPrefix'] = ApiClient.convertToType(data['postalDeliveryNumberPrefix'], 'String');
            }
            if (data.hasOwnProperty('postalDeliveryNumberValue')) {
                obj['postalDeliveryNumberValue'] = ApiClient.convertToType(data['postalDeliveryNumberValue'], 'String');
            }
            if (data.hasOwnProperty('postalDeliveryNumberSuffix')) {
                obj['postalDeliveryNumberSuffix'] = ApiClient.convertToType(data['postalDeliveryNumberSuffix'], 'String');
            }
            if (data.hasOwnProperty('postcode')) {
                obj['postcode'] = ApiClient.convertToType(data['postcode'], 'String');
            }
            if (data.hasOwnProperty('countryCode')) {
                obj['countryCode'] = ApiClient.convertToType(data['countryCode'], 'String');
            }
        }
        return obj;
    }


}

/**
 * careOf The 'Care of' line for the address
 * @member {String} 
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype[''] = undefined;

/**
 * A free text description of the full name used to identify the physical building or property as part of its location.
 * @member {String} buildingOrPropertyName
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['buildingOrPropertyName'] = undefined;

/**
 * The overflow from above
 * @member {String} buildingOrPropertyName2
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['buildingOrPropertyName2'] = undefined;

/**
 * The lot reference number allocated to an address prior to street numbering.
 * @member {String} lotNumber
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['lotNumber'] = undefined;

/**
 * Specification of the type of flat or unit which is a separately identifiable portion within a building/complex.
 * @member {String} flatOrUnitType
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['flatOrUnitType'] = undefined;

/**
 * Specification of the number of the flat or unit which is a separately identifiable portion within a building/complex.
 * @member {String} flatOrUnitNumber
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['flatOrUnitNumber'] = undefined;

/**
 * Floor Type is used to identify the floor or level of a multi-storey building/complex..
 * @member {String} floorOrLevelType
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['floorOrLevelType'] = undefined;

/**
 * Floor Number is used to identify the floor or level of a multi-storey building/complex.
 * @member {String} floorOrLevelNumber
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['floorOrLevelNumber'] = undefined;

/**
 * The numeric reference of a house or property.
 * @member {String} houseNumber
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['houseNumber'] = undefined;

/**
 * The numeric reference of a house or property.
 * @member {String} houseNumberSuffix
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['houseNumberSuffix'] = undefined;

/**
 * The numeric reference of a house or property.
 * @member {String} houseNumber2
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['houseNumber2'] = undefined;

/**
 * The numeric reference of a house or property.
 * @member {String} houseNumberSuffix2
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['houseNumberSuffix2'] = undefined;

/**
 * Records the thoroughfare name.
 * @member {String} streetName
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['streetName'] = undefined;

/**
 * Records the street type abbreviation.
 * @member {String} streetType
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['streetType'] = undefined;

/**
 * Records street suffixes.
 * @member {String} streetSuffix
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['streetSuffix'] = undefined;

/**
 * The full name of the general locality containing the specific address
 * @member {String} suburbOrPlaceOrLocality
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['suburbOrPlaceOrLocality'] = undefined;

/**
 * A general field to capture various references to address locations alongside another physical location.
 * @member {String} locationDescriptor
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['locationDescriptor'] = undefined;

/**
 * Defined State or Territory abbreviation.
 * @member {String} stateOrTerritory
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['stateOrTerritory'] = undefined;

/**
 * Postal delivery point identifier.
 * @member {String} deliveryPointIdentifier
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['deliveryPointIdentifier'] = undefined;

/**
 * Postal delivery type.
 * @member {String} postalDeliveryType
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['postalDeliveryType'] = undefined;

/**
 * Postal delivery number prefix.
 * @member {String} postalDeliveryNumberPrefix
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['postalDeliveryNumberPrefix'] = undefined;

/**
 * Postal delivery number.
 * @member {String} postalDeliveryNumberValue
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['postalDeliveryNumberValue'] = undefined;

/**
 * Postal delivery number suffix.
 * @member {String} postalDeliveryNumberSuffix
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['postalDeliveryNumberSuffix'] = undefined;

/**
 * Post code
 * @member {String} postcode
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['postcode'] = undefined;

/**
 * An ISO country code. AU is the the only acceptable value!
 * @member {String} countryCode
 */
AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.prototype['countryCode'] = undefined;






export default AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress;

