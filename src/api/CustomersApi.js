/**
 * Junifer AU Native API
 * Junifer Native Web API provided by the Gentrack Cloud Integration Services  # How It Works  For how to prepare your application to call Junifer Native Web API, please refer to: * [Read me first](https://portal.integration.gentrack.cloud/api/index.html#section/Read-me-first) * [Sovereignty Regions](https://portal.integration.gentrack.cloud/api/index.html#section/Sovereignty-Regions) * [Preparing your request](https://portal.integration.gentrack.cloud/api/index.html#section/Preparing-your-request) * [Authentication](https://portal.integration.gentrack.cloud/api/index.html#section/Authentication) * [Responses](https://portal.integration.gentrack.cloud/api/index.html#section/Responses)  ## Sample of requesting contact details The following is an example of calling get contact details API in UK region:  ```bash curl -X GET \\     https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJZCI6IjA2NDYxODczLTFlOWMtNGVkMy1iZWZkLTU3NGY5ZmEwYjExYyIsIm9yZ0lkIj oiQzAwMDAiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJpYXQiOjE1MzA1ODkwODcsImV4cCI6MTUzMDU5MjY4NywiaXNzIjoiaHR0cHM6Ly9hcGkuZ2VudHJhY2suaW8vIn0.WUAFYWTTEAy Q0Bt4YXu-mxCXd-Y9ehwdZcYvcGNnyLTZH_hiJjtXWWfsx69M606pvCP6lLMT7MfK-F3E4rLO6KAlGsuz4A_7oVWQf4QNeR178GnwgmqQRw5OnLxwPbPrRa9nODngwGq8dcWejhmEYU6i w02bvYdQBHnnsc3Kpyzw7Wdv_3jnBS4TPYS20muQOgG6KxRp9hLJM7ERLoAbsULwqdPOV8eUJJhGrq1NDuH_lA83YRDZmCWEzw96tSm3hb7y88kXs-4OvamnO1m5wFPBx69VximlS4Ltr 3ztqU2s3fHoj0OJLIafge9JvTgvuB6noHfs1uSRaahvstGJAA' ```  The sample of response is as follows.  ```json {    \"id\": 1010,    \"contactType\": \"Business\",    \"title\": \"Mr\",    \"forename\": \"Tony\",    \"surname\": \"Soprano\",    \"email\": \"bigtone@didntseeanything.com\",    \"dateOfBirth\": \"1959-08-24\",    \"phoneNumber1\": \"44626478370\",    \"address\":{      \"address1\": \"633 Stag Trail Road\",      \"address2\": \"North Caldwell\",      \"address3\": \"New Jersey\",      \"postcode\": \"NE18 0PY\"    },    \"links\": {      \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010\",      \"accounts\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010/accounts\"    } } ```  # General Rules/Patterns  ## Navigation A links section is returned in most requests.  This section contains how to access the entity itself and, more importantly, how to access items related to the entity.  For example take the following response: ```json {   \"id\": 1,   \"name\": \"Mr Joe Bloggs\",   \"number\": \"00000001\",   \"surname\": \"Bloggs\",   \"links\": {     \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\"   },   \"accounts\": [     {       \"id\": 7,       \"name\": \"Invoice\",       \"number\": \"00000001\",       \"currency\": \"GBP\",       \"fromDt\": \"2013-05-01\",       \"createdDttm\": \"2013-05-08T13:36:34.000\",       \"balance\": 0,       \"billingAddress\": {         \"address1\": \"11 Tuppy Street\"       },       \"links\": {         \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7\",         \"customer\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\",         \"bills\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/bills\",         \"payments\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/payments\"       }     }   ] } ```  The `self` URL in the `links` section can be used to access the entities listed in the response. In the above example there are self fields for the customer itself, and the accounts under this customer. There are other links under the account.  For example the above example contains links for the following:  - `bills` – List of Bills for the Account - `payments` – List of Payments for the Account - `customer` – Link back to the Account’s Customer  ## About IDs  The id in the API requests (JSON payload and URL) and responses identifies the resource within the API. It has no business meaning or purpose, therefore shouldn't be exposed to the end user in any way.  As the IDs of the resources aren't sequential or aligned with any other resource IDs, it is highly recommended using the links section from the responses to navigate to other related resources. Given the example above, the customer with ID 1 links to the account with ID 7. The links section will populate the correct IDs in the URLs so none of the assumptions about related resource IDs need to be made by the implementer.  # Accessing accounts by account number Throughout this API, anywhere an account is accessed by its `id` (i.e. the URL includes `/accounts/:id`), it can also be accessed by its account number by replacing `/accounts/:id` with `accounts/accountNumber/:num`.  For example: the Create Account Credit endpoint, available at `/accounts/:id/accountCredits`, can also be accessed at `/accounts/accountNumber/:num/accountCredits`.  # Standard data types in JSON The standard data types are as follows:  - String – Quoted as per JSON standard - Number – Unquoted as per JSON standard - Decimal – Unquoted as per JSON standard - Date – In the standard ISO format \"yyyy-MM-dd\" - DateTime – In the standard ISO format \"yyyy-MM-ddThh:mm:ss.SSS\" (no time zone component) - Boolean – Unquoted 'true' and 'false' are the only valid values  # Ordering of data Response representations are not ordered unless specified on a specific resource. Therefore, clients should be implemented to use the name of fields/objects as the key for accessing data.  For example in JSON objects are comprised of key/value pairs and clients should be implemented to search on the ‘key’ value rather than looking for the nth key/value pair in the object.  # Changes of data Response representations may change without warning in the following ways.  - Additional key/value pairs may be added to an existing object - Additional sub-objects may be added to an existing object  These changes are considered non-breaking.  Therefore, clients should be implemented to consume only the data they need and ignore any other information.  # Versioning The API is usually changed in each release of Junifer. Navigate to different versions by clicking the dropdown menu in the top right of this web page.  # Pagination Some REST APIs that provide a list of results support pagination.  Pagination allows API users to query potential large data sets but only receive a sub-set of the results on the initial query. Subsequent queries can use the response of a prior query to retrieve the next or previous set of results.  Pagination functionality and behaviour will be documented on each API that requires it.  ## Pagination on existing APIs Existing APIs may have pagination support added. In the future this pagination will become mandatory.  In order to facilitate API users planning and executing upgrades against the existing APIs, pagination is optional during a transition period. API users can enable pagination on these APIs by specifying the `pagination` query parameter. This parameter only needs to be present, the value is not required or checked. If this query parameter is specified, then pagination will be enabled. Otherwise, the existing non-paginated functionality, that returns all results, will be used.  API users must plan and execute upgrades to their existing applications to use the pagination support during the transition period. At the end of the transition period pagination will become mandatory on these APIs.  For new APIs that support pagination, it will be mandatory with the initial release of the API.  ## Ordering The ordering that the API provides will be documented for each API.  An ordering is always required to ensure moving between pages is consistent.  Some APIs may define an ordering on a value or values in the response where appropriate. For example results may be ordered by a date/time property of the results.  Other APIs may not define an explicit ordering but will guarantee the order is consistent for multiple API calls.  ## Limiting results of each page The `desiredResults` query parameter can be used to specify the maximum number of results that may be returned.  The APIs treat this as a hint so API callers should expect to sometimes receive a different number of results. APIs will endeavour to ensure no more than `desiredResults` are returned but for small values of `desiredResults` (less than 10) this may not be possible. This is due to how pages of data are structured inside the product. The API will prefer to return some data rather than no data where it is not possible to return less than `desiredResults`  If `desiredResults` is not specified the default maximum value for the API will be used. Unless documented differently, this value is 100.  If `desiredResults` is specified higher than the default maximum for the API a 400 error response will be received.  ## Cursors The pagination provided by the APIs is cursor-based.  In the JSON response additional metadata will be provided when pagination is present.  - `after` cursor points to the start of the results that have been returned - `before` cursor points to the end of the results that have been returned  These cursors can be used to make subsequent API calls. The API URL should be unchanged except for setting the `before` or `after` query parameter. If both parameters are specified, then an error 400 response will be received.  Cursor tokens must only be used for pagination purposes. API users should not infer any business meaning or logic from cursor representations. These representations may change without warning when Junifer is upgraded.  When scrolling forward the `after` cursor will be absent from the response when there are no more results after the current set.  When scrolling backwards the `before` cursor will be absent from the response when there are no more results before the current set.  In addition, a paginated response will include a `next` and `previous` URLs for convenience.  ## Best practices The following best practices must be adhered to by users of the pagination APIs:  - Don't store cursors or next and previous URLs for long. Cursors can quickly become invalid if items are added or deleted to the data set - If an invalid cursor is used, then a 404 response will be returned in this situation. API users should design a policy to handle this situation - API users should handle empty data sets. In limited cases a pagination query using a previously valid cursor may return no results  ## Example  This example is illustrative only.  A small `desiredResults` parameter has been used to make the example concise. API users should generally choose larger values, subject to the maximum value.  The following API call would retrieve the *first* set of results.  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?desiredResults=3 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-20T00:00:00.000\",       \"creditBalance\": 109,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-19T00:00:00.000\",       \"creditBalance\": 108,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-18T00:00:00.000\",       \"creditBalance\": 107,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"after\": \"107\"     },     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107\"   } } ```  The `results` section contains the payload of the API.  The `pagination` section contains details needed to retrieve additional results.  API users can choose to use the `next` URL directly or use the `after` cursor token to construct the URL themselves.  The API call to query the next set of results would be  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-17T00:00:00.000\",       \"creditBalance\": 106,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-16T00:00:00.000\",       \"creditBalance\": 105,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-15T00:00:00.000\",       \"creditBalance\": 104,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"before\": \"106\",       \"after\": \"104\"     },     \"previous\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&before=106&desiredResults=3\",     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=104\"   } } ```  This time the response contains both `before` and `after` tokens and `previous` and `next` URLs. The `before` token or `previous` link can be used to navigate backwards in the results. 
 *
 * The version of the OpenAPI document: 1.61.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import ContactsGetCustomersIdContactsResponse200 from '../model/ContactsGetCustomersIdContactsResponse200';
import ContactsGetCustomersIdContactsResponse400 from '../model/ContactsGetCustomersIdContactsResponse400';
import CreateProspectPutCustomersIdCreateprospectRequestBody from '../model/CreateProspectPutCustomersIdCreateprospectRequestBody';
import CreateProspectPutCustomersIdCreateprospectResponse200 from '../model/CreateProspectPutCustomersIdCreateprospectResponse200';
import CreateProspectPutCustomersIdCreateprospectResponse400 from '../model/CreateProspectPutCustomersIdCreateprospectResponse400';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody from '../model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200 from '../model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200';
import EnrolCustomerPostCustomersEnrolcustomerRequestBody from '../model/EnrolCustomerPostCustomersEnrolcustomerRequestBody';
import EnrolCustomerPostCustomersEnrolcustomerResponse200 from '../model/EnrolCustomerPostCustomersEnrolcustomerResponse200';
import EnrolCustomerPostCustomersEnrolcustomerResponse400 from '../model/EnrolCustomerPostCustomersEnrolcustomerResponse400';
import GetCustomerAccountsGetCustomersIdAccountsResponse200 from '../model/GetCustomerAccountsGetCustomersIdAccountsResponse200';
import GetCustomerAccountsGetCustomersIdAccountsResponse400 from '../model/GetCustomerAccountsGetCustomersIdAccountsResponse400';
import GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200 from '../model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200';
import GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400 from '../model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400';
import GetCustomerConsentsGetCustomersIdConsentsResponse200 from '../model/GetCustomerConsentsGetCustomersIdConsentsResponse200';
import GetCustomerConsentsGetCustomersIdConsentsResponse400 from '../model/GetCustomerConsentsGetCustomersIdConsentsResponse400';
import GetCustomerGetCustomersIdResponse200 from '../model/GetCustomerGetCustomersIdResponse200';
import GetCustomerGetCustomersIdResponse400 from '../model/GetCustomerGetCustomersIdResponse400';
import GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200 from '../model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200';
import GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400 from '../model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400';
import LookUpCustomerGetCustomersResponse200 from '../model/LookUpCustomerGetCustomersResponse200';
import LookUpCustomerGetCustomersResponse400 from '../model/LookUpCustomerGetCustomersResponse400';
import UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody from '../model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody';
import UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400 from '../model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400';
import UpdateCustomerConsentsPutCustomersIdConsentsRequestBody from '../model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody';
import UpdateCustomerConsentsPutCustomersIdConsentsResponse400 from '../model/UpdateCustomerConsentsPutCustomersIdConsentsResponse400';
import UpdateCustomerContactPutCustomersIdContactsContactidRequestBody from '../model/UpdateCustomerContactPutCustomersIdContactsContactidRequestBody';
import UpdateCustomerContactPutCustomersIdContactsContactidResponse400 from '../model/UpdateCustomerContactPutCustomersIdContactsContactidResponse400';
import UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody from '../model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody';
import UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400 from '../model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400';
import UpdateCustomerPutCustomersIdRequestBody from '../model/UpdateCustomerPutCustomersIdRequestBody';
import UpdateCustomerPutCustomersIdResponse400 from '../model/UpdateCustomerPutCustomersIdResponse400';

/**
* Customers service.
* @module api/CustomersApi
* @version 1.61.1
*/
export default class CustomersApi {

    /**
    * Constructs a new CustomersApi. 
    * @alias module:api/CustomersApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the juniferCustomersEnrolCustomerPost operation.
     * @callback module:api/CustomersApi~juniferCustomersEnrolCustomerPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/EnrolCustomerPostCustomersEnrolcustomerResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Enrol customer
     * Enrols customer for electricity and/or gas product. This is a Great Britain specific API.
     * @param {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersEnrolCustomerPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/EnrolCustomerPostCustomersEnrolcustomerResponse200}
     */
    juniferCustomersEnrolCustomerPost(requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersEnrolCustomerPost");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = EnrolCustomerPostCustomersEnrolcustomerResponse200;
      return this.apiClient.callApi(
        '/junifer/customers/enrolCustomer', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersGet operation.
     * @callback module:api/CustomersApi~juniferCustomersGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LookUpCustomerGetCustomersResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Customer lookup
     * Searches for a customer by the specified customer number. If no customer can be found an empty `results` array is returned.
     * @param {Number} customerNumber Customer number
     * @param {module:api/CustomersApi~juniferCustomersGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/LookUpCustomerGetCustomersResponse200}
     */
    juniferCustomersGet(customerNumber, callback) {
      let postBody = null;
      // verify the required parameter 'customerNumber' is set
      if (customerNumber === undefined || customerNumber === null) {
        throw new Error("Missing the required parameter 'customerNumber' when calling juniferCustomersGet");
      }

      let pathParams = {
      };
      let queryParams = {
        'customer_number': customerNumber
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = LookUpCustomerGetCustomersResponse200;
      return this.apiClient.callApi(
        '/junifer/customers', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdAccountsGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdAccountsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer's accounts
     * List customer accounts
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdAccountsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200}
     */
    juniferCustomersIdAccountsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdAccountsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetCustomerAccountsGetCustomersIdAccountsResponse200;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/accounts', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdBillingEntitiesGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdBillingEntitiesGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer's billing entities
     * Get customer's billing entities
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdBillingEntitiesGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200}
     */
    juniferCustomersIdBillingEntitiesGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdBillingEntitiesGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/billingEntities', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdConsentsGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdConsentsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerConsentsGetCustomersIdConsentsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer's consents
     * Get customer's consents for DCC meters i.e. meter reading frequency. If no consents can be found an empty `results` array is returned
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdConsentsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerConsentsGetCustomersIdConsentsResponse200}
     */
    juniferCustomersIdConsentsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdConsentsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetCustomerConsentsGetCustomersIdConsentsResponse200;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/consents', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdConsentsPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdConsentsPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer's consents
     * Updates customer's consents for DCC meters i.e. meter reading frequency.
     * @param {Number} id Customer ID
     * @param {module:model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdConsentsPutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferCustomersIdConsentsPut(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdConsentsPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdConsentsPut");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/consents', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdContactsContactIdPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdContactsContactIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer's contact details
     * Updates a customer's given contact details. Correct usage is to get the `contact` object from the `get customer contact` call then modify the `contact` object and return it using this `Update Customer's Contact` call. Our database is then updated to match the incoming `contact` object. All null fields in that incoming `contact` object are set to null in our database.
     * @param {Number} id Customer ID
     * @param {Number} contactId Contact ID
     * @param {module:model/UpdateCustomerContactPutCustomersIdContactsContactidRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdContactsContactIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferCustomersIdContactsContactIdPut(id, contactId, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdContactsContactIdPut");
      }
      // verify the required parameter 'contactId' is set
      if (contactId === undefined || contactId === null) {
        throw new Error("Missing the required parameter 'contactId' when calling juniferCustomersIdContactsContactIdPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdContactsContactIdPut");
      }

      let pathParams = {
        'id': id,
        'contactId': contactId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/contacts/{contactId}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdContactsGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdContactsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ContactsGetCustomersIdContactsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Contacts
     * Get all contacts for given customer. If no contacts can be found an empty `results` array is returned.
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdContactsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ContactsGetCustomersIdContactsResponse200}
     */
    juniferCustomersIdContactsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdContactsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = ContactsGetCustomersIdContactsResponse200;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/contacts', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdCreateProspectPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdCreateProspectPutCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateProspectPutCustomersIdCreateprospectResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create Prospect
     * Create new prospect for existing customer
     * @param {Number} id Customer ID
     * @param {module:model/CreateProspectPutCustomersIdCreateprospectRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdCreateProspectPutCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateProspectPutCustomersIdCreateprospectResponse200}
     */
    juniferCustomersIdCreateProspectPut(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdCreateProspectPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdCreateProspectPut");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = CreateProspectPutCustomersIdCreateprospectResponse200;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/createProspect', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdEnrolAdditionalAccountPost operation.
     * @callback module:api/CustomersApi~juniferCustomersIdEnrolAdditionalAccountPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Enrol a new account to an existing customer
     * Creates a new account for existing customer and enrols it for electricity and/or gas product
     * @param {Number} id ID of additional account to enrol
     * @param {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdEnrolAdditionalAccountPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200}
     */
    juniferCustomersIdEnrolAdditionalAccountPost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdEnrolAdditionalAccountPost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdEnrolAdditionalAccountPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/enrolAdditionalAccount', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerGetCustomersIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer
     * Get customer by its ID
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerGetCustomersIdResponse200}
     */
    juniferCustomersIdGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetCustomerGetCustomersIdResponse200;
      return this.apiClient.callApi(
        '/junifer/customers/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdPrimaryContactPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdPrimaryContactPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer's primary contact details
     * Updates customer's primary contact details. Correct usage is to get the `primaryContact` object from the `get customer` call then modify the `primaryContact` object and return it using this `Update Customer's Primary Contact` call. Our database is then updated to match the incoming `primaryContact` object. All null fields in that incoming `primaryContact` object are set to null in our database.
     * @param {Number} id Customer ID
     * @param {module:model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdPrimaryContactPutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferCustomersIdPrimaryContactPut(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdPrimaryContactPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdPrimaryContactPut");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/primaryContact', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdProspectsGet operation.
     * @callback module:api/CustomersApi~juniferCustomersIdProspectsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get customer's Prospects
     * Gets prospects for a customer
     * @param {Number} id Customer ID
     * @param {module:api/CustomersApi~juniferCustomersIdProspectsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200}
     */
    juniferCustomersIdProspectsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdProspectsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/prospects', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer
     * Updates customer record in Junifer. Only `title`, `forename`, `surname` and `marketingOptOutFl` will be updated. The rest of the fields in the request are ignored.
     * @param {Number} id Customer ID
     * @param {module:model/UpdateCustomerPutCustomersIdRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferCustomersIdPut(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdPut");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/customers/{id}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferCustomersIdUpdateBillingAddressPut operation.
     * @callback module:api/CustomersApi~juniferCustomersIdUpdateBillingAddressPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update customer's Billing Address.
     * This Api call will change a customers billing address to what is supplied in the send address, if a postcode is sent it must be a valid UK postcode and if a Country name is sent it must be one that is saved into junifer already.Although postcode, Address lines and Country parameters are marked as optional, atleast one of them must be supplied to the call.
     * @param {Number} id Customer ID
     * @param {module:model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody} requestBody Request body
     * @param {module:api/CustomersApi~juniferCustomersIdUpdateBillingAddressPutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferCustomersIdUpdateBillingAddressPut(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferCustomersIdUpdateBillingAddressPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferCustomersIdUpdateBillingAddressPut");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/customers/{id}/updateBillingAddress', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
