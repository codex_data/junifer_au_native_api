/**
 * Junifer AU Native API
 * Junifer Native Web API provided by the Gentrack Cloud Integration Services  # How It Works  For how to prepare your application to call Junifer Native Web API, please refer to: * [Read me first](https://portal.integration.gentrack.cloud/api/index.html#section/Read-me-first) * [Sovereignty Regions](https://portal.integration.gentrack.cloud/api/index.html#section/Sovereignty-Regions) * [Preparing your request](https://portal.integration.gentrack.cloud/api/index.html#section/Preparing-your-request) * [Authentication](https://portal.integration.gentrack.cloud/api/index.html#section/Authentication) * [Responses](https://portal.integration.gentrack.cloud/api/index.html#section/Responses)  ## Sample of requesting contact details The following is an example of calling get contact details API in UK region:  ```bash curl -X GET \\     https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJZCI6IjA2NDYxODczLTFlOWMtNGVkMy1iZWZkLTU3NGY5ZmEwYjExYyIsIm9yZ0lkIj oiQzAwMDAiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJpYXQiOjE1MzA1ODkwODcsImV4cCI6MTUzMDU5MjY4NywiaXNzIjoiaHR0cHM6Ly9hcGkuZ2VudHJhY2suaW8vIn0.WUAFYWTTEAy Q0Bt4YXu-mxCXd-Y9ehwdZcYvcGNnyLTZH_hiJjtXWWfsx69M606pvCP6lLMT7MfK-F3E4rLO6KAlGsuz4A_7oVWQf4QNeR178GnwgmqQRw5OnLxwPbPrRa9nODngwGq8dcWejhmEYU6i w02bvYdQBHnnsc3Kpyzw7Wdv_3jnBS4TPYS20muQOgG6KxRp9hLJM7ERLoAbsULwqdPOV8eUJJhGrq1NDuH_lA83YRDZmCWEzw96tSm3hb7y88kXs-4OvamnO1m5wFPBx69VximlS4Ltr 3ztqU2s3fHoj0OJLIafge9JvTgvuB6noHfs1uSRaahvstGJAA' ```  The sample of response is as follows.  ```json {    \"id\": 1010,    \"contactType\": \"Business\",    \"title\": \"Mr\",    \"forename\": \"Tony\",    \"surname\": \"Soprano\",    \"email\": \"bigtone@didntseeanything.com\",    \"dateOfBirth\": \"1959-08-24\",    \"phoneNumber1\": \"44626478370\",    \"address\":{      \"address1\": \"633 Stag Trail Road\",      \"address2\": \"North Caldwell\",      \"address3\": \"New Jersey\",      \"postcode\": \"NE18 0PY\"    },    \"links\": {      \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010\",      \"accounts\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010/accounts\"    } } ```  # General Rules/Patterns  ## Navigation A links section is returned in most requests.  This section contains how to access the entity itself and, more importantly, how to access items related to the entity.  For example take the following response: ```json {   \"id\": 1,   \"name\": \"Mr Joe Bloggs\",   \"number\": \"00000001\",   \"surname\": \"Bloggs\",   \"links\": {     \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\"   },   \"accounts\": [     {       \"id\": 7,       \"name\": \"Invoice\",       \"number\": \"00000001\",       \"currency\": \"GBP\",       \"fromDt\": \"2013-05-01\",       \"createdDttm\": \"2013-05-08T13:36:34.000\",       \"balance\": 0,       \"billingAddress\": {         \"address1\": \"11 Tuppy Street\"       },       \"links\": {         \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7\",         \"customer\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\",         \"bills\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/bills\",         \"payments\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/payments\"       }     }   ] } ```  The `self` URL in the `links` section can be used to access the entities listed in the response. In the above example there are self fields for the customer itself, and the accounts under this customer. There are other links under the account.  For example the above example contains links for the following:  - `bills` – List of Bills for the Account - `payments` – List of Payments for the Account - `customer` – Link back to the Account’s Customer  ## About IDs  The id in the API requests (JSON payload and URL) and responses identifies the resource within the API. It has no business meaning or purpose, therefore shouldn't be exposed to the end user in any way.  As the IDs of the resources aren't sequential or aligned with any other resource IDs, it is highly recommended using the links section from the responses to navigate to other related resources. Given the example above, the customer with ID 1 links to the account with ID 7. The links section will populate the correct IDs in the URLs so none of the assumptions about related resource IDs need to be made by the implementer.  # Accessing accounts by account number Throughout this API, anywhere an account is accessed by its `id` (i.e. the URL includes `/accounts/:id`), it can also be accessed by its account number by replacing `/accounts/:id` with `accounts/accountNumber/:num`.  For example: the Create Account Credit endpoint, available at `/accounts/:id/accountCredits`, can also be accessed at `/accounts/accountNumber/:num/accountCredits`.  # Standard data types in JSON The standard data types are as follows:  - String – Quoted as per JSON standard - Number – Unquoted as per JSON standard - Decimal – Unquoted as per JSON standard - Date – In the standard ISO format \"yyyy-MM-dd\" - DateTime – In the standard ISO format \"yyyy-MM-ddThh:mm:ss.SSS\" (no time zone component) - Boolean – Unquoted 'true' and 'false' are the only valid values  # Ordering of data Response representations are not ordered unless specified on a specific resource. Therefore, clients should be implemented to use the name of fields/objects as the key for accessing data.  For example in JSON objects are comprised of key/value pairs and clients should be implemented to search on the ‘key’ value rather than looking for the nth key/value pair in the object.  # Changes of data Response representations may change without warning in the following ways.  - Additional key/value pairs may be added to an existing object - Additional sub-objects may be added to an existing object  These changes are considered non-breaking.  Therefore, clients should be implemented to consume only the data they need and ignore any other information.  # Versioning The API is usually changed in each release of Junifer. Navigate to different versions by clicking the dropdown menu in the top right of this web page.  # Pagination Some REST APIs that provide a list of results support pagination.  Pagination allows API users to query potential large data sets but only receive a sub-set of the results on the initial query. Subsequent queries can use the response of a prior query to retrieve the next or previous set of results.  Pagination functionality and behaviour will be documented on each API that requires it.  ## Pagination on existing APIs Existing APIs may have pagination support added. In the future this pagination will become mandatory.  In order to facilitate API users planning and executing upgrades against the existing APIs, pagination is optional during a transition period. API users can enable pagination on these APIs by specifying the `pagination` query parameter. This parameter only needs to be present, the value is not required or checked. If this query parameter is specified, then pagination will be enabled. Otherwise, the existing non-paginated functionality, that returns all results, will be used.  API users must plan and execute upgrades to their existing applications to use the pagination support during the transition period. At the end of the transition period pagination will become mandatory on these APIs.  For new APIs that support pagination, it will be mandatory with the initial release of the API.  ## Ordering The ordering that the API provides will be documented for each API.  An ordering is always required to ensure moving between pages is consistent.  Some APIs may define an ordering on a value or values in the response where appropriate. For example results may be ordered by a date/time property of the results.  Other APIs may not define an explicit ordering but will guarantee the order is consistent for multiple API calls.  ## Limiting results of each page The `desiredResults` query parameter can be used to specify the maximum number of results that may be returned.  The APIs treat this as a hint so API callers should expect to sometimes receive a different number of results. APIs will endeavour to ensure no more than `desiredResults` are returned but for small values of `desiredResults` (less than 10) this may not be possible. This is due to how pages of data are structured inside the product. The API will prefer to return some data rather than no data where it is not possible to return less than `desiredResults`  If `desiredResults` is not specified the default maximum value for the API will be used. Unless documented differently, this value is 100.  If `desiredResults` is specified higher than the default maximum for the API a 400 error response will be received.  ## Cursors The pagination provided by the APIs is cursor-based.  In the JSON response additional metadata will be provided when pagination is present.  - `after` cursor points to the start of the results that have been returned - `before` cursor points to the end of the results that have been returned  These cursors can be used to make subsequent API calls. The API URL should be unchanged except for setting the `before` or `after` query parameter. If both parameters are specified, then an error 400 response will be received.  Cursor tokens must only be used for pagination purposes. API users should not infer any business meaning or logic from cursor representations. These representations may change without warning when Junifer is upgraded.  When scrolling forward the `after` cursor will be absent from the response when there are no more results after the current set.  When scrolling backwards the `before` cursor will be absent from the response when there are no more results before the current set.  In addition, a paginated response will include a `next` and `previous` URLs for convenience.  ## Best practices The following best practices must be adhered to by users of the pagination APIs:  - Don't store cursors or next and previous URLs for long. Cursors can quickly become invalid if items are added or deleted to the data set - If an invalid cursor is used, then a 404 response will be returned in this situation. API users should design a policy to handle this situation - API users should handle empty data sets. In limited cases a pagination query using a previously valid cursor may return no results  ## Example  This example is illustrative only.  A small `desiredResults` parameter has been used to make the example concise. API users should generally choose larger values, subject to the maximum value.  The following API call would retrieve the *first* set of results.  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?desiredResults=3 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-20T00:00:00.000\",       \"creditBalance\": 109,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-19T00:00:00.000\",       \"creditBalance\": 108,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-18T00:00:00.000\",       \"creditBalance\": 107,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"after\": \"107\"     },     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107\"   } } ```  The `results` section contains the payload of the API.  The `pagination` section contains details needed to retrieve additional results.  API users can choose to use the `next` URL directly or use the `after` cursor token to construct the URL themselves.  The API call to query the next set of results would be  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-17T00:00:00.000\",       \"creditBalance\": 106,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-16T00:00:00.000\",       \"creditBalance\": 105,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-15T00:00:00.000\",       \"creditBalance\": 104,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"before\": \"106\",       \"after\": \"104\"     },     \"previous\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&before=106&desiredResults=3\",     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=104\"   } } ```  This time the response contains both `before` and `after` tokens and `previous` and `next` URLs. The `before` token or `previous` link can be used to navigate backwards in the results. 
 *
 * The version of the OpenAPI document: 1.61.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400 from '../model/CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400';
import CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody from '../model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody';
import CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200 from '../model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200';
import CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400 from '../model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400';
import CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404 from '../model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404';
import GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200 from '../model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200';
import GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400 from '../model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400';
import GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404 from '../model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404';
import GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200 from '../model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200';
import GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400 from '../model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400';
import GetPaymentGetPaymentsIdResponse200 from '../model/GetPaymentGetPaymentsIdResponse200';
import GetPaymentGetPaymentsIdResponse404 from '../model/GetPaymentGetPaymentsIdResponse404';
import GetPaymentMethodGetPaymentmethodsIdResponse200 from '../model/GetPaymentMethodGetPaymentmethodsIdResponse200';
import GetPaymentMethodGetPaymentmethodsIdResponse404 from '../model/GetPaymentMethodGetPaymentmethodsIdResponse404';
import GetPaymentPlanGetPaymentplansIdResponse200 from '../model/GetPaymentPlanGetPaymentplansIdResponse200';
import GetPaymentPlanGetPaymentplansIdResponse404 from '../model/GetPaymentPlanGetPaymentplansIdResponse404';
import GetPaymentRequestGetPaymentrequestsIdResponse200 from '../model/GetPaymentRequestGetPaymentrequestsIdResponse200';
import GetPaymentRequestGetPaymentrequestsIdResponse404 from '../model/GetPaymentRequestGetPaymentrequestsIdResponse404';
import GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200 from '../model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200';
import GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404 from '../model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404';
import StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody from '../model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody';
import StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400 from '../model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400';

/**
* Financials service.
* @module api/FinancialsApi
* @version 1.61.1
*/
export default class FinancialsApi {

    /**
    * Constructs a new FinancialsApi. 
    * @alias module:api/FinancialsApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the juniferActiveSeasonalDefinitionGet operation.
     * @callback module:api/FinancialsApi~juniferActiveSeasonalDefinitionGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get active seasonal definition
     * Gets the currently active seasonal definition for seasonally-variable Direct Debit payments
     * @param {module:api/FinancialsApi~juniferActiveSeasonalDefinitionGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200}
     */
    juniferActiveSeasonalDefinitionGet(callback) {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200;
      return this.apiClient.callApi(
        '/junifer/activeSeasonalDefinition', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferPaymentMethodsIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentMethodsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentMethodGetPaymentmethodsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment method
     * Get payment method by its ID
     * @param {Number} id Payment method ID
     * @param {module:api/FinancialsApi~juniferPaymentMethodsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentMethodGetPaymentmethodsIdResponse200}
     */
    juniferPaymentMethodsIdGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentMethodsIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetPaymentMethodGetPaymentmethodsIdResponse200;
      return this.apiClient.callApi(
        '/junifer/paymentMethods/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferPaymentPlansIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentPlansIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentPlanGetPaymentplansIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment plan
     * Get payment plan by its ID
     * @param {Number} id Payment plan ID
     * @param {module:api/FinancialsApi~juniferPaymentPlansIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentPlanGetPaymentplansIdResponse200}
     */
    juniferPaymentPlansIdGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentPlansIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetPaymentPlanGetPaymentplansIdResponse200;
      return this.apiClient.callApi(
        '/junifer/paymentPlans/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferPaymentRequestsIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentRequestsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment request
     * Get payment request by its ID
     * @param {Number} id Payment plan ID
     * @param {module:api/FinancialsApi~juniferPaymentRequestsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentRequestGetPaymentrequestsIdResponse200}
     */
    juniferPaymentRequestsIdGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentRequestsIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetPaymentRequestGetPaymentrequestsIdResponse200;
      return this.apiClient.callApi(
        '/junifer/paymentRequests/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsCalculateNextPaymentDateGetCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Calculate next payment schedule collection date
     * Calculates and returns next payment schedule date based on the parameters supplied
     * @param {String} frequency The name of the frequency indicating when a payment should be collected, e.g. `Monthly, Weekly, Daily`
     * @param {Number} multiple Frequency multiplier
     * @param {String} startDttm Payment schedule period start date
     * @param {String} alignmentDttm Alignment date
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsCalculateNextPaymentDateGetCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet(frequency, multiple, startDttm, alignmentDttm, callback) {
      let postBody = null;
      // verify the required parameter 'frequency' is set
      if (frequency === undefined || frequency === null) {
        throw new Error("Missing the required parameter 'frequency' when calling juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet");
      }
      // verify the required parameter 'multiple' is set
      if (multiple === undefined || multiple === null) {
        throw new Error("Missing the required parameter 'multiple' when calling juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet");
      }
      // verify the required parameter 'startDttm' is set
      if (startDttm === undefined || startDttm === null) {
        throw new Error("Missing the required parameter 'startDttm' when calling juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet");
      }
      // verify the required parameter 'alignmentDttm' is set
      if (alignmentDttm === undefined || alignmentDttm === null) {
        throw new Error("Missing the required parameter 'alignmentDttm' when calling juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet");
      }

      let pathParams = {
      };
      let queryParams = {
        'frequency': frequency,
        'multiple': multiple,
        'startDttm': startDttm,
        'alignmentDttm': alignmentDttm
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/paymentSchedulePeriods/calculateNextPaymentDate', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment schedule period
     * Get payment schedule period by its ID
     * @param {Number} id Payment schedule period ID
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200}
     */
    juniferPaymentSchedulePeriodsIdGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentSchedulePeriodsIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200;
      return this.apiClient.callApi(
        '/junifer/paymentSchedulePeriods/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsIdPut operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Stop payment schedule period
     * Stops payment schedule period. Exactly the same JSON structure can be passed as retrieved in Get payment schedule period, but with `toDt` field added. This method will set `PaymentSchedulePeriod.StoppedDttm` to Now.
     * @param {Number} id Payment schedule ID
     * @param {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody} requestBody Request body
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferPaymentSchedulePeriodsIdPut(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentSchedulePeriodsIdPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferPaymentSchedulePeriodsIdPut");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/paymentSchedulePeriods/{id}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsPaymentScheduleItemsGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsPaymentScheduleItemsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payment schedule items
     * Get payment schedule items for a given account ID. If no payment schedules can be found an empty `results` array is returned.
     * @param {Number} accountId Account ID
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsPaymentScheduleItemsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200}
     */
    juniferPaymentSchedulePeriodsPaymentScheduleItemsGet(accountId, callback) {
      let postBody = null;
      // verify the required parameter 'accountId' is set
      if (accountId === undefined || accountId === null) {
        throw new Error("Missing the required parameter 'accountId' when calling juniferPaymentSchedulePeriodsPaymentScheduleItemsGet");
      }

      let pathParams = {
      };
      let queryParams = {
        'accountId': accountId
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200;
      return this.apiClient.callApi(
        '/junifer/paymentSchedulePeriods/paymentScheduleItems', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferPaymentSchedulePeriodsPost operation.
     * @callback module:api/FinancialsApi~juniferPaymentSchedulePeriodsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create new payment schedule period
     * Creates a new payment schedule period.  Will raise a ticket, if one is specified in the property Payment Schedule Period Properties > API Payment Schedule Period Change Ticket.
     * @param {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody} requestBody Request body
     * @param {module:api/FinancialsApi~juniferPaymentSchedulePeriodsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200}
     */
    juniferPaymentSchedulePeriodsPost(requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferPaymentSchedulePeriodsPost");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200;
      return this.apiClient.callApi(
        '/junifer/paymentSchedulePeriods', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferPaymentsIdGet operation.
     * @callback module:api/FinancialsApi~juniferPaymentsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetPaymentGetPaymentsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get payment
     * Gets payment by its ID
     * @param {Number} id Payment ID
     * @param {module:api/FinancialsApi~juniferPaymentsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetPaymentGetPaymentsIdResponse200}
     */
    juniferPaymentsIdGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferPaymentsIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetPaymentGetPaymentsIdResponse200;
      return this.apiClient.callApi(
        '/junifer/payments/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
