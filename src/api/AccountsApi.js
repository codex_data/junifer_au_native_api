/**
 * Junifer AU Native API
 * Junifer Native Web API provided by the Gentrack Cloud Integration Services  # How It Works  For how to prepare your application to call Junifer Native Web API, please refer to: * [Read me first](https://portal.integration.gentrack.cloud/api/index.html#section/Read-me-first) * [Sovereignty Regions](https://portal.integration.gentrack.cloud/api/index.html#section/Sovereignty-Regions) * [Preparing your request](https://portal.integration.gentrack.cloud/api/index.html#section/Preparing-your-request) * [Authentication](https://portal.integration.gentrack.cloud/api/index.html#section/Authentication) * [Responses](https://portal.integration.gentrack.cloud/api/index.html#section/Responses)  ## Sample of requesting contact details The following is an example of calling get contact details API in UK region:  ```bash curl -X GET \\     https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJZCI6IjA2NDYxODczLTFlOWMtNGVkMy1iZWZkLTU3NGY5ZmEwYjExYyIsIm9yZ0lkIj oiQzAwMDAiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJpYXQiOjE1MzA1ODkwODcsImV4cCI6MTUzMDU5MjY4NywiaXNzIjoiaHR0cHM6Ly9hcGkuZ2VudHJhY2suaW8vIn0.WUAFYWTTEAy Q0Bt4YXu-mxCXd-Y9ehwdZcYvcGNnyLTZH_hiJjtXWWfsx69M606pvCP6lLMT7MfK-F3E4rLO6KAlGsuz4A_7oVWQf4QNeR178GnwgmqQRw5OnLxwPbPrRa9nODngwGq8dcWejhmEYU6i w02bvYdQBHnnsc3Kpyzw7Wdv_3jnBS4TPYS20muQOgG6KxRp9hLJM7ERLoAbsULwqdPOV8eUJJhGrq1NDuH_lA83YRDZmCWEzw96tSm3hb7y88kXs-4OvamnO1m5wFPBx69VximlS4Ltr 3ztqU2s3fHoj0OJLIafge9JvTgvuB6noHfs1uSRaahvstGJAA' ```  The sample of response is as follows.  ```json {    \"id\": 1010,    \"contactType\": \"Business\",    \"title\": \"Mr\",    \"forename\": \"Tony\",    \"surname\": \"Soprano\",    \"email\": \"bigtone@didntseeanything.com\",    \"dateOfBirth\": \"1959-08-24\",    \"phoneNumber1\": \"44626478370\",    \"address\":{      \"address1\": \"633 Stag Trail Road\",      \"address2\": \"North Caldwell\",      \"address3\": \"New Jersey\",      \"postcode\": \"NE18 0PY\"    },    \"links\": {      \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010\",      \"accounts\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010/accounts\"    } } ```  # General Rules/Patterns  ## Navigation A links section is returned in most requests.  This section contains how to access the entity itself and, more importantly, how to access items related to the entity.  For example take the following response: ```json {   \"id\": 1,   \"name\": \"Mr Joe Bloggs\",   \"number\": \"00000001\",   \"surname\": \"Bloggs\",   \"links\": {     \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\"   },   \"accounts\": [     {       \"id\": 7,       \"name\": \"Invoice\",       \"number\": \"00000001\",       \"currency\": \"GBP\",       \"fromDt\": \"2013-05-01\",       \"createdDttm\": \"2013-05-08T13:36:34.000\",       \"balance\": 0,       \"billingAddress\": {         \"address1\": \"11 Tuppy Street\"       },       \"links\": {         \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7\",         \"customer\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\",         \"bills\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/bills\",         \"payments\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/payments\"       }     }   ] } ```  The `self` URL in the `links` section can be used to access the entities listed in the response. In the above example there are self fields for the customer itself, and the accounts under this customer. There are other links under the account.  For example the above example contains links for the following:  - `bills` – List of Bills for the Account - `payments` – List of Payments for the Account - `customer` – Link back to the Account’s Customer  ## About IDs  The id in the API requests (JSON payload and URL) and responses identifies the resource within the API. It has no business meaning or purpose, therefore shouldn't be exposed to the end user in any way.  As the IDs of the resources aren't sequential or aligned with any other resource IDs, it is highly recommended using the links section from the responses to navigate to other related resources. Given the example above, the customer with ID 1 links to the account with ID 7. The links section will populate the correct IDs in the URLs so none of the assumptions about related resource IDs need to be made by the implementer.  # Accessing accounts by account number Throughout this API, anywhere an account is accessed by its `id` (i.e. the URL includes `/accounts/:id`), it can also be accessed by its account number by replacing `/accounts/:id` with `accounts/accountNumber/:num`.  For example: the Create Account Credit endpoint, available at `/accounts/:id/accountCredits`, can also be accessed at `/accounts/accountNumber/:num/accountCredits`.  # Standard data types in JSON The standard data types are as follows:  - String – Quoted as per JSON standard - Number – Unquoted as per JSON standard - Decimal – Unquoted as per JSON standard - Date – In the standard ISO format \"yyyy-MM-dd\" - DateTime – In the standard ISO format \"yyyy-MM-ddThh:mm:ss.SSS\" (no time zone component) - Boolean – Unquoted 'true' and 'false' are the only valid values  # Ordering of data Response representations are not ordered unless specified on a specific resource. Therefore, clients should be implemented to use the name of fields/objects as the key for accessing data.  For example in JSON objects are comprised of key/value pairs and clients should be implemented to search on the ‘key’ value rather than looking for the nth key/value pair in the object.  # Changes of data Response representations may change without warning in the following ways.  - Additional key/value pairs may be added to an existing object - Additional sub-objects may be added to an existing object  These changes are considered non-breaking.  Therefore, clients should be implemented to consume only the data they need and ignore any other information.  # Versioning The API is usually changed in each release of Junifer. Navigate to different versions by clicking the dropdown menu in the top right of this web page.  # Pagination Some REST APIs that provide a list of results support pagination.  Pagination allows API users to query potential large data sets but only receive a sub-set of the results on the initial query. Subsequent queries can use the response of a prior query to retrieve the next or previous set of results.  Pagination functionality and behaviour will be documented on each API that requires it.  ## Pagination on existing APIs Existing APIs may have pagination support added. In the future this pagination will become mandatory.  In order to facilitate API users planning and executing upgrades against the existing APIs, pagination is optional during a transition period. API users can enable pagination on these APIs by specifying the `pagination` query parameter. This parameter only needs to be present, the value is not required or checked. If this query parameter is specified, then pagination will be enabled. Otherwise, the existing non-paginated functionality, that returns all results, will be used.  API users must plan and execute upgrades to their existing applications to use the pagination support during the transition period. At the end of the transition period pagination will become mandatory on these APIs.  For new APIs that support pagination, it will be mandatory with the initial release of the API.  ## Ordering The ordering that the API provides will be documented for each API.  An ordering is always required to ensure moving between pages is consistent.  Some APIs may define an ordering on a value or values in the response where appropriate. For example results may be ordered by a date/time property of the results.  Other APIs may not define an explicit ordering but will guarantee the order is consistent for multiple API calls.  ## Limiting results of each page The `desiredResults` query parameter can be used to specify the maximum number of results that may be returned.  The APIs treat this as a hint so API callers should expect to sometimes receive a different number of results. APIs will endeavour to ensure no more than `desiredResults` are returned but for small values of `desiredResults` (less than 10) this may not be possible. This is due to how pages of data are structured inside the product. The API will prefer to return some data rather than no data where it is not possible to return less than `desiredResults`  If `desiredResults` is not specified the default maximum value for the API will be used. Unless documented differently, this value is 100.  If `desiredResults` is specified higher than the default maximum for the API a 400 error response will be received.  ## Cursors The pagination provided by the APIs is cursor-based.  In the JSON response additional metadata will be provided when pagination is present.  - `after` cursor points to the start of the results that have been returned - `before` cursor points to the end of the results that have been returned  These cursors can be used to make subsequent API calls. The API URL should be unchanged except for setting the `before` or `after` query parameter. If both parameters are specified, then an error 400 response will be received.  Cursor tokens must only be used for pagination purposes. API users should not infer any business meaning or logic from cursor representations. These representations may change without warning when Junifer is upgraded.  When scrolling forward the `after` cursor will be absent from the response when there are no more results after the current set.  When scrolling backwards the `before` cursor will be absent from the response when there are no more results before the current set.  In addition, a paginated response will include a `next` and `previous` URLs for convenience.  ## Best practices The following best practices must be adhered to by users of the pagination APIs:  - Don't store cursors or next and previous URLs for long. Cursors can quickly become invalid if items are added or deleted to the data set - If an invalid cursor is used, then a 404 response will be returned in this situation. API users should design a policy to handle this situation - API users should handle empty data sets. In limited cases a pagination query using a previously valid cursor may return no results  ## Example  This example is illustrative only.  A small `desiredResults` parameter has been used to make the example concise. API users should generally choose larger values, subject to the maximum value.  The following API call would retrieve the *first* set of results.  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?desiredResults=3 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-20T00:00:00.000\",       \"creditBalance\": 109,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-19T00:00:00.000\",       \"creditBalance\": 108,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-18T00:00:00.000\",       \"creditBalance\": 107,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"after\": \"107\"     },     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107\"   } } ```  The `results` section contains the payload of the API.  The `pagination` section contains details needed to retrieve additional results.  API users can choose to use the `next` URL directly or use the `after` cursor token to construct the URL themselves.  The API call to query the next set of results would be  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-17T00:00:00.000\",       \"creditBalance\": 106,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-16T00:00:00.000\",       \"creditBalance\": 105,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-15T00:00:00.000\",       \"creditBalance\": 104,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"before\": \"106\",       \"after\": \"104\"     },     \"previous\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&before=106&desiredResults=3\",     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=104\"   } } ```  This time the response contains both `before` and `after` tokens and `previous` and `next` URLs. The `before` token or `previous` link can be used to navigate backwards in the results. 
 *
 * The version of the OpenAPI document: 1.61.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400 from '../model/CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400';
import ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody from '../model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody';
import ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400 from '../model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400';
import ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404 from '../model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404';
import ContactsGetAccountsIdContactsResponse200 from '../model/ContactsGetAccountsIdContactsResponse200';
import ContactsGetAccountsIdContactsResponse400 from '../model/ContactsGetAccountsIdContactsResponse400';
import CreateAccountCreditPostAccountsIdAccountcreditsRequestBody from '../model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody';
import CreateAccountCreditPostAccountsIdAccountcreditsResponse400 from '../model/CreateAccountCreditPostAccountsIdAccountcreditsResponse400';
import CreateAccountCreditPostAccountsIdAccountcreditsResponse404 from '../model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404';
import CreateAccountDebitPostAccountsIdAccountdebitsRequestBody from '../model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody';
import CreateAccountDebitPostAccountsIdAccountdebitsResponse400 from '../model/CreateAccountDebitPostAccountsIdAccountdebitsResponse400';
import CreateAccountDebitPostAccountsIdAccountdebitsResponse404 from '../model/CreateAccountDebitPostAccountsIdAccountdebitsResponse404';
import CreateAccountNotePostAccountsIdNoteRequestBody from '../model/CreateAccountNotePostAccountsIdNoteRequestBody';
import CreateAccountNotePostAccountsIdNoteResponse200 from '../model/CreateAccountNotePostAccountsIdNoteResponse200';
import CreateAccountNotePostAccountsIdNoteResponse400 from '../model/CreateAccountNotePostAccountsIdNoteResponse400';
import CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody from '../model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody';
import CreateAccountRepaymentPostAccountsIdRepaymentsResponse200 from '../model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200';
import CreateAccountRepaymentPostAccountsIdRepaymentsResponse400 from '../model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse400';
import CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody from '../model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody';
import CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200 from '../model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200';
import CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400 from '../model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400';
import CreateAccountTicketPostAccountsIdTicketsRequestBody from '../model/CreateAccountTicketPostAccountsIdTicketsRequestBody';
import CreateAccountTicketPostAccountsIdTicketsResponse200 from '../model/CreateAccountTicketPostAccountsIdTicketsResponse200';
import CreateAccountTicketPostAccountsIdTicketsResponse400 from '../model/CreateAccountTicketPostAccountsIdTicketsResponse400';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody from '../model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200 from '../model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400 from '../model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400';
import GetAccountAgreementsGetAccountsIdAgreementsResponse200 from '../model/GetAccountAgreementsGetAccountsIdAgreementsResponse200';
import GetAccountBillRequestsGetAccountsIdBillrequestsResponse200 from '../model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200';
import GetAccountBillRequestsGetAccountsIdBillrequestsResponse404 from '../model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse404';
import GetAccountBillsGetAccountsIdBillsResponse200 from '../model/GetAccountBillsGetAccountsIdBillsResponse200';
import GetAccountByNumberGetAccountsAccountnumberNumResponse400 from '../model/GetAccountByNumberGetAccountsAccountnumberNumResponse400';
import GetAccountByNumberGetAccountsAccountnumberNumResponse404 from '../model/GetAccountByNumberGetAccountsAccountnumberNumResponse404';
import GetAccountCommunicationsGetAccountsIdCommunicationsResponse200 from '../model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200';
import GetAccountCommunicationsGetAccountsIdCommunicationsResponse400 from '../model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse400';
import GetAccountCreditNotesGetAccountsIdCreditsResponse200 from '../model/GetAccountCreditNotesGetAccountsIdCreditsResponse200';
import GetAccountCreditsGetAccountsIdAccountcreditsResponse200 from '../model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200';
import GetAccountDebitsGetAccountsIdAccountdebitsResponse200 from '../model/GetAccountDebitsGetAccountsIdAccountdebitsResponse200';
import GetAccountGetAccountsIdResponse200 from '../model/GetAccountGetAccountsIdResponse200';
import GetAccountGetAccountsIdResponse404 from '../model/GetAccountGetAccountsIdResponse404';
import GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200 from '../model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200';
import GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200 from '../model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200';
import GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200 from '../model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200';
import GetAccountPaymentsGetAccountsIdPaymentsResponse200 from '../model/GetAccountPaymentsGetAccountsIdPaymentsResponse200';
import GetAccountPaymentsGetAccountsIdPaymentsResponse400 from '../model/GetAccountPaymentsGetAccountsIdPaymentsResponse400';
import GetAccountProductDetailsGetAccountsIdProductdetailsResponse200 from '../model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200';
import GetAccountProductDetailsGetAccountsIdProductdetailsResponse400 from '../model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse400';
import GetAccountPropertysGetAccountsIdPropertysResponse200 from '../model/GetAccountPropertysGetAccountsIdPropertysResponse200';
import GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200 from '../model/GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200';
import GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200 from '../model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200';
import GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400 from '../model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400';
import GetAccountTicketsGetAccountsIdTicketsResponse200 from '../model/GetAccountTicketsGetAccountsIdTicketsResponse200';
import GetAccountTicketsGetAccountsIdTicketsResponse400 from '../model/GetAccountTicketsGetAccountsIdTicketsResponse400';
import GetAccountTransactionsGetAccountsIdTransactionsResponse200 from '../model/GetAccountTransactionsGetAccountsIdTransactionsResponse200';
import GetAccountTransactionsGetAccountsIdTransactionsResponse400 from '../model/GetAccountTransactionsGetAccountsIdTransactionsResponse400';
import GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200 from '../model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200';
import GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400 from '../model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400';
import GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200 from '../model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200';
import GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404 from '../model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404';
import HotbillPostAccountsIdHotbillResponse400 from '../model/HotbillPostAccountsIdHotbillResponse400';
import LookUpAccountGetAccountsResponse200 from '../model/LookUpAccountGetAccountsResponse200';
import LookUpAccountGetAccountsResponse400 from '../model/LookUpAccountGetAccountsResponse400';
import PaymentPostAccountsIdPaymentRequestBody from '../model/PaymentPostAccountsIdPaymentRequestBody';
import PaymentPostAccountsIdPaymentResponse200 from '../model/PaymentPostAccountsIdPaymentResponse200';
import PaymentPostAccountsIdPaymentResponse400 from '../model/PaymentPostAccountsIdPaymentResponse400';
import RenewAccountPostAccountsIdRenewalRequestBody from '../model/RenewAccountPostAccountsIdRenewalRequestBody';
import RenewAccountPostAccountsIdRenewalResponse200 from '../model/RenewAccountPostAccountsIdRenewalResponse200';
import RenewAccountPostAccountsIdRenewalResponse400 from '../model/RenewAccountPostAccountsIdRenewalResponse400';
import TariffInformationGetAccountsIdTariffinformationResponse200 from '../model/TariffInformationGetAccountsIdTariffinformationResponse200';
import TariffInformationGetAccountsIdTariffinformationResponse400 from '../model/TariffInformationGetAccountsIdTariffinformationResponse400';
import UpdateAccountContactPutAccountsIdContactsContactidRequestBody from '../model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody';
import UpdateAccountContactPutAccountsIdContactsContactidResponse400 from '../model/UpdateAccountContactPutAccountsIdContactsContactidResponse400';
import UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody from '../model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody';
import UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400 from '../model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400';

/**
* Accounts service.
* @module api/AccountsApi
* @version 1.61.1
*/
export default class AccountsApi {

    /**
    * Constructs a new AccountsApi. 
    * @alias module:api/AccountsApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the juniferAccountsAccountNumberNumGet operation.
     * @callback module:api/AccountsApi~juniferAccountsAccountNumberNumGetCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account by account number
     * Get an account by its account number (i.e. the customer-facing identifier).The success response is identical to the Get Account API.
     * @param {String} num Account Number
     * @param {module:api/AccountsApi~juniferAccountsAccountNumberNumGetCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferAccountsAccountNumberNumGet(num, callback) {
      let postBody = null;
      // verify the required parameter 'num' is set
      if (num === undefined || num === null) {
        throw new Error("Missing the required parameter 'num' when calling juniferAccountsAccountNumberNumGet");
      }

      let pathParams = {
        'num': num
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/accounts/accountNumber/{num}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LookUpAccountGetAccountsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Account lookup
     * Searches for an account by one or more of the specified search terms. If no account can be found an empty `results` array is returned. The query parameters are :account_number, :surname, :billingPostcode, :email. The lookup can be performed using either of them, but at least one must be provided. If more than one are specified, the account contact details are cross-checked by looking if the provided search values number belong to the same account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.number number
     * @param {String} opts.surname surname
     * @param {String} opts.telephone Telephone number in any of the telephone fields in account primary contact. Right search allowed (i.e without international/area codes)d. If this parameter is present in the request it must be non-empty.
     * @param {String} opts.billingPostcode Billing postcode (no spaces)
     * @param {String} opts.email email address in account primary contact record
     * @param {module:api/AccountsApi~juniferAccountsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/LookUpAccountGetAccountsResponse200}
     */
    juniferAccountsGet(opts, callback) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'number': opts['number'],
        'surname': opts['surname'],
        'telephone': opts['telephone'],
        'billingPostcode': opts['billingPostcode'],
        'email': opts['email']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = LookUpAccountGetAccountsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdAccountCreditsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountCreditsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account credits
     * Get account credits for an account
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAccountCreditsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200}
     */
    juniferAccountsIdAccountCreditsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountCreditsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountCreditsGetAccountsIdAccountcreditsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/accountCredits', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdAccountCreditsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountCreditsPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account credit
     * Create account credit for account. Authentication must be active
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdAccountCreditsPostCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferAccountsIdAccountCreditsPost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountCreditsPost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdAccountCreditsPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/accountCredits', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdAccountDebitsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountDebitsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountDebitsGetAccountsIdAccountdebitsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account debits
     * Get account debits for an account
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAccountDebitsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountDebitsGetAccountsIdAccountdebitsResponse200}
     */
    juniferAccountsIdAccountDebitsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountDebitsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountDebitsGetAccountsIdAccountdebitsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/accountDebits', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdAccountDebitsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountDebitsPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account debit
     * Create account debit for account. Authentication must be active
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdAccountDebitsPostCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferAccountsIdAccountDebitsPost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountDebitsPost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdAccountDebitsPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/accountDebits', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdAccountReviewPeriodsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountReviewPeriodsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's review periods
     * Gets account's review periods which belong to the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAccountReviewPeriodsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200}
     */
    juniferAccountsIdAccountReviewPeriodsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountReviewPeriodsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/accountReviewPeriods', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdAccountReviewPeriodsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAccountReviewPeriodsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account review period
     * Creates a new account review period for the account specified by the Id
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdAccountReviewPeriodsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200}
     */
    juniferAccountsIdAccountReviewPeriodsPost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAccountReviewPeriodsPost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdAccountReviewPeriodsPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/accountReviewPeriods', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdAgreementsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAgreementsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's agreements
     * Get agreements associated to the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAgreementsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200}
     */
    juniferAccountsIdAgreementsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAgreementsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountAgreementsGetAccountsIdAgreementsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/agreements', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdAllPayReferenceGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdAllPayReferenceGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's AllPay Reference Number
     * Get the AllPay reference number
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdAllPayReferenceGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200}
     */
    juniferAccountsIdAllPayReferenceGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdAllPayReferenceGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/allPayReference', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdBillRequestsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdBillRequestsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's bill requests
     * Gets bill requests which belong to the account specified by the ID. If no bill requests can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.statusCode Status Code of retrieved bill requests. Can contain the following: `Scheduled, Completed, Failed, Superseded, Retrying`. If not provided then this filter won't be applied
     * @param {String} opts.typeCode Type Code of retrieved bill requests. Can contain the following: `Normal, Hot, NewVersion`. If not provided then this filter won't be applied
     * @param {module:api/AccountsApi~juniferAccountsIdBillRequestsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200}
     */
    juniferAccountsIdBillRequestsGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdBillRequestsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'statusCode': opts['statusCode'],
        'typeCode': opts['typeCode']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountBillRequestsGetAccountsIdBillrequestsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/billRequests', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdBillsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdBillsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountBillsGetAccountsIdBillsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's bills
     * Gets bills which belong to the account specified by the ID. If no bills can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.status Status of retrieved bills. Can contain the following: `All, Accepted, Draft`. Defaults to `Accepted` if not provided.
     * @param {module:api/AccountsApi~juniferAccountsIdBillsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountBillsGetAccountsIdBillsResponse200}
     */
    juniferAccountsIdBillsGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdBillsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'status': opts['status']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountBillsGetAccountsIdBillsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/bills', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdCancelAccountReviewPeriodDelete operation.
     * @callback module:api/AccountsApi~juniferAccountsIdCancelAccountReviewPeriodDeleteCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Cancel account review period
     * Cancel an existing account review period for an account
     * @param {Number} id Account ID.
     * @param {Number} accountReviewPeriodId Account Review Period ID
     * @param {module:api/AccountsApi~juniferAccountsIdCancelAccountReviewPeriodDeleteCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferAccountsIdCancelAccountReviewPeriodDelete(id, accountReviewPeriodId, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdCancelAccountReviewPeriodDelete");
      }
      // verify the required parameter 'accountReviewPeriodId' is set
      if (accountReviewPeriodId === undefined || accountReviewPeriodId === null) {
        throw new Error("Missing the required parameter 'accountReviewPeriodId' when calling juniferAccountsIdCancelAccountReviewPeriodDelete");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'accountReviewPeriodId': accountReviewPeriodId
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/cancelAccountReviewPeriod', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdChangeAccountCustomerPut operation.
     * @callback module:api/AccountsApi~juniferAccountsIdChangeAccountCustomerPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Change the customer linked to an account
     * Change the customer linked to the account to the customer specified by the id
     * @param {Number} id Account ID
     * @param {module:model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdChangeAccountCustomerPutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferAccountsIdChangeAccountCustomerPut(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdChangeAccountCustomerPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdChangeAccountCustomerPut");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/changeAccountCustomer', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdCommercialProductDetailsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdCommercialProductDetailsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's product details for commercial customer
     * Get the details for products associated to the commercial account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdCommercialProductDetailsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200}
     */
    juniferAccountsIdCommercialProductDetailsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdCommercialProductDetailsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/commercial/productDetails', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdCommunicationsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdCommunicationsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's communications
     * Get communications associated to the account specified by the ID
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.fromDate Date from
     * @param {String} opts.toDate Date to
     * @param {Boolean} opts.testFlag true will include test communications
     * @param {Array.<String>} opts.status Array of statuses strings used to filter the search of the communications. Can contain the following: `Pending, Successful, SuccessfulWithWarnings, NoDeliveries, Failed` and if left blank will default to `Successful, SuccessfulWithWarnings`
     * @param {module:api/AccountsApi~juniferAccountsIdCommunicationsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200}
     */
    juniferAccountsIdCommunicationsGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdCommunicationsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'from_date': opts['fromDate'],
        'to_date': opts['toDate'],
        'test_flag': opts['testFlag'],
        'status': this.apiClient.buildCollectionParam(opts['status'], 'csv')
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountCommunicationsGetAccountsIdCommunicationsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/communications', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdContactsContactIdPut operation.
     * @callback module:api/AccountsApi~juniferAccountsIdContactsContactIdPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update account's contact details
     * Updates a account's given contact details. Correct usage is to get the `contact` object from the accounts `contacts` call then modify the `contact` object and return it using this `Update Account's Contact` call. Our database is then updated to match the incoming `contact` object. All null fields in that incoming `contact` object are set to null in our database.  Note that setting billDelivery to \"None\" could mean that no one will be notified when a bill is generated. Other billing activity will continue as normal. Please use with caution for Residential contacts.
     * @param {Number} id Account ID
     * @param {Number} contactId Contact ID
     * @param {module:model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdContactsContactIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferAccountsIdContactsContactIdPut(id, contactId, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdContactsContactIdPut");
      }
      // verify the required parameter 'contactId' is set
      if (contactId === undefined || contactId === null) {
        throw new Error("Missing the required parameter 'contactId' when calling juniferAccountsIdContactsContactIdPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdContactsContactIdPut");
      }

      let pathParams = {
        'id': id,
        'contactId': contactId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/contacts/{contactId}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdContactsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdContactsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ContactsGetAccountsIdContactsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Contacts
     * Get all contacts for given account. If no contacts can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdContactsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ContactsGetAccountsIdContactsResponse200}
     */
    juniferAccountsIdContactsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdContactsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = ContactsGetAccountsIdContactsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/contacts', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdCreditsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdCreditsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's credits
     * Gets credits which belong to the account specified by the ID. If no credits can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.status Status of retrieved credits. Can contain the following: `All,Draft, Authorised, Rejected`. Defaults to `Accepted` if not provided.
     * @param {module:api/AccountsApi~juniferAccountsIdCreditsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200}
     */
    juniferAccountsIdCreditsGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdCreditsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'status': opts['status']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountCreditNotesGetAccountsIdCreditsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/credits', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdEnrolMeterPointsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdEnrolMeterPointsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Enrol additional Meter Points to existing account/site
     * Enrols new Meter Point(s) to an existing account/site for electricity and/or gas product. Returns a list of the newly-enrolled Meter Points.
     * @param {Number} id Account ID
     * @param {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdEnrolMeterPointsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200}
     */
    juniferAccountsIdEnrolMeterPointsPost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdEnrolMeterPointsPost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdEnrolMeterPointsPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/enrolMeterPoints', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountGetAccountsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account
     * Gets account by its ID (not account number!)
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountGetAccountsIdResponse200}
     */
    juniferAccountsIdGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountGetAccountsIdResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdHotBillPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdHotBillPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Hot bill
     * Create a hot bill for a given Account. This will consolidate all open bill periods that precede today's date. Authentication must be active.
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdHotBillPostCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferAccountsIdHotBillPost(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdHotBillPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/hotBill', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdNotePost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdNotePostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateAccountNotePostAccountsIdNoteResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account note
     * Creates a new note for the account specified by the Id
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountNotePostAccountsIdNoteRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdNotePostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateAccountNotePostAccountsIdNoteResponse200}
     */
    juniferAccountsIdNotePost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdNotePost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdNotePost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = CreateAccountNotePostAccountsIdNoteResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/note', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentMethodsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentMethodsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payment methods
     * Get payment methods registered for the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentMethodsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200}
     */
    juniferAccountsIdPaymentMethodsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentMethodsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/paymentMethods', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentPlansGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentPlansGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payment plans
     * Gets payment plans which belong to the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentPlansGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200}
     */
    juniferAccountsIdPaymentPlansGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentPlansGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/paymentPlans', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/PaymentPostAccountsIdPaymentResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create Payment Transaction
     * Creates a transaction for payments that occurs outside of Junifer. No payment collection will be triggered. Requires authentication.
     * @param {Number} id Account ID
     * @param {module:model/PaymentPostAccountsIdPaymentRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/PaymentPostAccountsIdPaymentResponse200}
     */
    juniferAccountsIdPaymentPost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentPost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdPaymentPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = PaymentPostAccountsIdPaymentResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/payment', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentSchedulePeriodsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentSchedulePeriodsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payment schedule periods
     * Get account's payment schedule periods
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.nextPaymentOnlyFl If `true` returns only the payment period that will be active when the next payment is collected, this may be the current payment period. If not provided then defaults to `false`.
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentSchedulePeriodsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200}
     */
    juniferAccountsIdPaymentSchedulePeriodsGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentSchedulePeriodsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'nextPaymentOnlyFl': opts['nextPaymentOnlyFl']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/paymentSchedulePeriods', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's suggested payment amount
     * Get account's suggested payment amount given the payment schedule review calculation strategy
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {String} opts.frequency The name of the frequency indicating when a payment should be collected, e.g. `Monthly, Weekly`. Defaults to `Monthly`
     * @param {Number} opts.frequencyMultiple Frequency multiplier. Defaults to 1
     * @param {Boolean} opts.ignoreWarnings If set to true, consumption estimation warnings will be ignored. If false, the API will return an error if there are consumption estimation warnings. Defaults to false
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200}
     */
    juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'frequency': opts['frequency'],
        'frequencyMultiple': opts['frequencyMultiple'],
        'ignoreWarnings': opts['ignoreWarnings']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/paymentSchedules/suggestedPaymentAmount', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdPaymentsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPaymentsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's payments
     * Get payments associated to the account specified by the ID with the corresponding transaction status.
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {Array.<String>} opts.status Array of transaction status strings used to filter the search of our payments.  Can contain the following: `Pending, Accepted, Rejected, Authorising` and if left blank will default to using `Accepted`.
     * @param {Boolean} opts.cancelFl Returns cancelled transactions if `true`. If not provided then defaults to `false`
     * @param {module:api/AccountsApi~juniferAccountsIdPaymentsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200}
     */
    juniferAccountsIdPaymentsGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPaymentsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'status': this.apiClient.buildCollectionParam(opts['status'], 'csv'),
        'cancelFl': opts['cancelFl']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountPaymentsGetAccountsIdPaymentsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/payments', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdPrimaryContactPut operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPrimaryContactPutCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update Accounts's primary contact details.
     * Updates Accounts's primary contact details. Correct usage is to get the `primaryContact` object from the `get account` call then modify the `primaryContact` information in it using this Update Account's Contact call. Our database is then updated to match the incoming `primaryContact` object. All null fields in that incoming `primaryContact` object are set to null in our database. Missing params are treated as nulls.  Note that setting billDelivery to \"None\" could mean that no one will be notified when a bill is generated. Other billing activity will continue as normal. Please use with caution for Residential contacts.
     * @param {Number} id Account ID
     * @param {module:model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdPrimaryContactPutCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferAccountsIdPrimaryContactPut(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPrimaryContactPut");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdPrimaryContactPut");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/primaryContact', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdProductDetailsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdProductDetailsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's product details
     * Get the details for products associated to the account specified by the ID
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdProductDetailsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200}
     */
    juniferAccountsIdProductDetailsGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdProductDetailsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountProductDetailsGetAccountsIdProductdetailsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/productDetails', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdPropertysGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdPropertysGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's properties
     * Get account's properties. If no properties can be found an empty `results` array is returned.
     * @param {Number} id Account ID
     * @param {module:api/AccountsApi~juniferAccountsIdPropertysGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountPropertysGetAccountsIdPropertysResponse200}
     */
    juniferAccountsIdPropertysGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdPropertysGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountPropertysGetAccountsIdPropertysResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/propertys', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdRenewalPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdRenewalPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/RenewAccountPostAccountsIdRenewalResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Renew account
     * Renews an account with a new agreement based on the additional details supplied. Either electricity product details or gas product details need to be supplied for this to work.  Will raise a ticket, if one is specified in the property Agreement Creation Properties > API Agreement Setup Ticket.  Will trigger change of meter mode if new or existing agreements are SMART PAYG and meter contracted to the account is in DCC mode
     * @param {Number} id Account ID
     * @param {module:model/RenewAccountPostAccountsIdRenewalRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdRenewalPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/RenewAccountPostAccountsIdRenewalResponse200}
     */
    juniferAccountsIdRenewalPost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdRenewalPost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdRenewalPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = RenewAccountPostAccountsIdRenewalResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/renewal', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdRepaymentsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdRepaymentsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account repayment
     * Create a refund for an existing account payment. The account must have a valid default payment method set. This call is only usable when authentication is active
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdRepaymentsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200}
     */
    juniferAccountsIdRepaymentsPost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdRepaymentsPost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdRepaymentsPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = CreateAccountRepaymentPostAccountsIdRepaymentsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/repayments', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdTariffInformationGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdTariffInformationGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/TariffInformationGetAccountsIdTariffinformationResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's tariff Information
     * Get tariff information associated with this account at the comparison date, including tariffs that are active on the day and tariffs that end on the day.
     * @param {Number} id Account ID
     * @param {Boolean} ignoreWarnings If set to true, consumption estimation warnings will be ignored. If false, the API will return an error if there are consumption estimation warnings. Defaults to false
     * @param {Object} opts Optional parameters
     * @param {String} opts.comparisonDatetime Date to get information for. Default to today if not specified.
     * @param {module:api/AccountsApi~juniferAccountsIdTariffInformationGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/TariffInformationGetAccountsIdTariffinformationResponse200}
     */
    juniferAccountsIdTariffInformationGet(id, ignoreWarnings, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdTariffInformationGet");
      }
      // verify the required parameter 'ignoreWarnings' is set
      if (ignoreWarnings === undefined || ignoreWarnings === null) {
        throw new Error("Missing the required parameter 'ignoreWarnings' when calling juniferAccountsIdTariffInformationGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'comparison_datetime': opts['comparisonDatetime'],
        'ignoreWarnings': ignoreWarnings
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = TariffInformationGetAccountsIdTariffinformationResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/tariffInformation', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdTicketsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdTicketsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountTicketsGetAccountsIdTicketsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's tickets
     * Get tickets associated with this account
     * @param {Number} id Account ID
     * @param {Object} opts Optional parameters
     * @param {Array.<String>} opts.status Array of status which is used to filter the search of our ticket status. Can contain the following: `Open, Error, Closed, Cancelled` and if left blank will default to display all the tickets.
     * @param {Boolean} opts.includeIndirect if false, only tickets directly linked to the Account will be returned. If true, will also return tickets that are indirectly linked to the account via Bill, AccountTransaction, DunningInst, Asset, Customer, ProductPropertyAsset and Property.
     * @param {module:api/AccountsApi~juniferAccountsIdTicketsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountTicketsGetAccountsIdTicketsResponse200}
     */
    juniferAccountsIdTicketsGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdTicketsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'status': this.apiClient.buildCollectionParam(opts['status'], 'csv'),
        'includeIndirect': opts['includeIndirect']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountTicketsGetAccountsIdTicketsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/tickets', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdTicketsPost operation.
     * @callback module:api/AccountsApi~juniferAccountsIdTicketsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create account ticket
     * Creates a new ticket for the account specified by the ID  Contact Gentrack before using this request. It can have serious unexpected consequences if not used correctly.
     * @param {Number} id Account ID
     * @param {module:model/CreateAccountTicketPostAccountsIdTicketsRequestBody} requestBody Request body
     * @param {module:api/AccountsApi~juniferAccountsIdTicketsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CreateAccountTicketPostAccountsIdTicketsResponse200}
     */
    juniferAccountsIdTicketsPost(id, requestBody, callback) {
      let postBody = requestBody;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdTicketsPost");
      }
      // verify the required parameter 'requestBody' is set
      if (requestBody === undefined || requestBody === null) {
        throw new Error("Missing the required parameter 'requestBody' when calling juniferAccountsIdTicketsPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = CreateAccountTicketPostAccountsIdTicketsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/tickets', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferAccountsIdTransactionsGet operation.
     * @callback module:api/AccountsApi~juniferAccountsIdTransactionsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get account's transactions
     * Gets accepted transactions for the account specified by the ID
     * @param {Number} id AccountID
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.cancelFl Returns cancelled transactions if `true`. If not provided then defaults to `false`
     * @param {Array.<String>} opts.status Array of transaction status strings used to filter the search of account transactions.  Can contain the following: `All, Pending, Accepted, Rejected, Authorising` and if left blank will default to using `Accepted`.
     * @param {String} opts.createdDt Date that specifies which transactions select from that date onwards
     * @param {module:api/AccountsApi~juniferAccountsIdTransactionsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200}
     */
    juniferAccountsIdTransactionsGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferAccountsIdTransactionsGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'cancelFl': opts['cancelFl'],
        'status': this.apiClient.buildCollectionParam(opts['status'], 'csv'),
        'created_dt': opts['createdDt']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetAccountTransactionsGetAccountsIdTransactionsResponse200;
      return this.apiClient.callApi(
        '/junifer/accounts/{id}/transactions', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
