/**
 * Junifer AU Native API
 * Junifer Native Web API provided by the Gentrack Cloud Integration Services  # How It Works  For how to prepare your application to call Junifer Native Web API, please refer to: * [Read me first](https://portal.integration.gentrack.cloud/api/index.html#section/Read-me-first) * [Sovereignty Regions](https://portal.integration.gentrack.cloud/api/index.html#section/Sovereignty-Regions) * [Preparing your request](https://portal.integration.gentrack.cloud/api/index.html#section/Preparing-your-request) * [Authentication](https://portal.integration.gentrack.cloud/api/index.html#section/Authentication) * [Responses](https://portal.integration.gentrack.cloud/api/index.html#section/Responses)  ## Sample of requesting contact details The following is an example of calling get contact details API in UK region:  ```bash curl -X GET \\     https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJZCI6IjA2NDYxODczLTFlOWMtNGVkMy1iZWZkLTU3NGY5ZmEwYjExYyIsIm9yZ0lkIj oiQzAwMDAiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJpYXQiOjE1MzA1ODkwODcsImV4cCI6MTUzMDU5MjY4NywiaXNzIjoiaHR0cHM6Ly9hcGkuZ2VudHJhY2suaW8vIn0.WUAFYWTTEAy Q0Bt4YXu-mxCXd-Y9ehwdZcYvcGNnyLTZH_hiJjtXWWfsx69M606pvCP6lLMT7MfK-F3E4rLO6KAlGsuz4A_7oVWQf4QNeR178GnwgmqQRw5OnLxwPbPrRa9nODngwGq8dcWejhmEYU6i w02bvYdQBHnnsc3Kpyzw7Wdv_3jnBS4TPYS20muQOgG6KxRp9hLJM7ERLoAbsULwqdPOV8eUJJhGrq1NDuH_lA83YRDZmCWEzw96tSm3hb7y88kXs-4OvamnO1m5wFPBx69VximlS4Ltr 3ztqU2s3fHoj0OJLIafge9JvTgvuB6noHfs1uSRaahvstGJAA' ```  The sample of response is as follows.  ```json {    \"id\": 1010,    \"contactType\": \"Business\",    \"title\": \"Mr\",    \"forename\": \"Tony\",    \"surname\": \"Soprano\",    \"email\": \"bigtone@didntseeanything.com\",    \"dateOfBirth\": \"1959-08-24\",    \"phoneNumber1\": \"44626478370\",    \"address\":{      \"address1\": \"633 Stag Trail Road\",      \"address2\": \"North Caldwell\",      \"address3\": \"New Jersey\",      \"postcode\": \"NE18 0PY\"    },    \"links\": {      \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010\",      \"accounts\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010/accounts\"    } } ```  # General Rules/Patterns  ## Navigation A links section is returned in most requests.  This section contains how to access the entity itself and, more importantly, how to access items related to the entity.  For example take the following response: ```json {   \"id\": 1,   \"name\": \"Mr Joe Bloggs\",   \"number\": \"00000001\",   \"surname\": \"Bloggs\",   \"links\": {     \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\"   },   \"accounts\": [     {       \"id\": 7,       \"name\": \"Invoice\",       \"number\": \"00000001\",       \"currency\": \"GBP\",       \"fromDt\": \"2013-05-01\",       \"createdDttm\": \"2013-05-08T13:36:34.000\",       \"balance\": 0,       \"billingAddress\": {         \"address1\": \"11 Tuppy Street\"       },       \"links\": {         \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7\",         \"customer\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\",         \"bills\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/bills\",         \"payments\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/payments\"       }     }   ] } ```  The `self` URL in the `links` section can be used to access the entities listed in the response. In the above example there are self fields for the customer itself, and the accounts under this customer. There are other links under the account.  For example the above example contains links for the following:  - `bills` – List of Bills for the Account - `payments` – List of Payments for the Account - `customer` – Link back to the Account’s Customer  ## About IDs  The id in the API requests (JSON payload and URL) and responses identifies the resource within the API. It has no business meaning or purpose, therefore shouldn't be exposed to the end user in any way.  As the IDs of the resources aren't sequential or aligned with any other resource IDs, it is highly recommended using the links section from the responses to navigate to other related resources. Given the example above, the customer with ID 1 links to the account with ID 7. The links section will populate the correct IDs in the URLs so none of the assumptions about related resource IDs need to be made by the implementer.  # Accessing accounts by account number Throughout this API, anywhere an account is accessed by its `id` (i.e. the URL includes `/accounts/:id`), it can also be accessed by its account number by replacing `/accounts/:id` with `accounts/accountNumber/:num`.  For example: the Create Account Credit endpoint, available at `/accounts/:id/accountCredits`, can also be accessed at `/accounts/accountNumber/:num/accountCredits`.  # Standard data types in JSON The standard data types are as follows:  - String – Quoted as per JSON standard - Number – Unquoted as per JSON standard - Decimal – Unquoted as per JSON standard - Date – In the standard ISO format \"yyyy-MM-dd\" - DateTime – In the standard ISO format \"yyyy-MM-ddThh:mm:ss.SSS\" (no time zone component) - Boolean – Unquoted 'true' and 'false' are the only valid values  # Ordering of data Response representations are not ordered unless specified on a specific resource. Therefore, clients should be implemented to use the name of fields/objects as the key for accessing data.  For example in JSON objects are comprised of key/value pairs and clients should be implemented to search on the ‘key’ value rather than looking for the nth key/value pair in the object.  # Changes of data Response representations may change without warning in the following ways.  - Additional key/value pairs may be added to an existing object - Additional sub-objects may be added to an existing object  These changes are considered non-breaking.  Therefore, clients should be implemented to consume only the data they need and ignore any other information.  # Versioning The API is usually changed in each release of Junifer. Navigate to different versions by clicking the dropdown menu in the top right of this web page.  # Pagination Some REST APIs that provide a list of results support pagination.  Pagination allows API users to query potential large data sets but only receive a sub-set of the results on the initial query. Subsequent queries can use the response of a prior query to retrieve the next or previous set of results.  Pagination functionality and behaviour will be documented on each API that requires it.  ## Pagination on existing APIs Existing APIs may have pagination support added. In the future this pagination will become mandatory.  In order to facilitate API users planning and executing upgrades against the existing APIs, pagination is optional during a transition period. API users can enable pagination on these APIs by specifying the `pagination` query parameter. This parameter only needs to be present, the value is not required or checked. If this query parameter is specified, then pagination will be enabled. Otherwise, the existing non-paginated functionality, that returns all results, will be used.  API users must plan and execute upgrades to their existing applications to use the pagination support during the transition period. At the end of the transition period pagination will become mandatory on these APIs.  For new APIs that support pagination, it will be mandatory with the initial release of the API.  ## Ordering The ordering that the API provides will be documented for each API.  An ordering is always required to ensure moving between pages is consistent.  Some APIs may define an ordering on a value or values in the response where appropriate. For example results may be ordered by a date/time property of the results.  Other APIs may not define an explicit ordering but will guarantee the order is consistent for multiple API calls.  ## Limiting results of each page The `desiredResults` query parameter can be used to specify the maximum number of results that may be returned.  The APIs treat this as a hint so API callers should expect to sometimes receive a different number of results. APIs will endeavour to ensure no more than `desiredResults` are returned but for small values of `desiredResults` (less than 10) this may not be possible. This is due to how pages of data are structured inside the product. The API will prefer to return some data rather than no data where it is not possible to return less than `desiredResults`  If `desiredResults` is not specified the default maximum value for the API will be used. Unless documented differently, this value is 100.  If `desiredResults` is specified higher than the default maximum for the API a 400 error response will be received.  ## Cursors The pagination provided by the APIs is cursor-based.  In the JSON response additional metadata will be provided when pagination is present.  - `after` cursor points to the start of the results that have been returned - `before` cursor points to the end of the results that have been returned  These cursors can be used to make subsequent API calls. The API URL should be unchanged except for setting the `before` or `after` query parameter. If both parameters are specified, then an error 400 response will be received.  Cursor tokens must only be used for pagination purposes. API users should not infer any business meaning or logic from cursor representations. These representations may change without warning when Junifer is upgraded.  When scrolling forward the `after` cursor will be absent from the response when there are no more results after the current set.  When scrolling backwards the `before` cursor will be absent from the response when there are no more results before the current set.  In addition, a paginated response will include a `next` and `previous` URLs for convenience.  ## Best practices The following best practices must be adhered to by users of the pagination APIs:  - Don't store cursors or next and previous URLs for long. Cursors can quickly become invalid if items are added or deleted to the data set - If an invalid cursor is used, then a 404 response will be returned in this situation. API users should design a policy to handle this situation - API users should handle empty data sets. In limited cases a pagination query using a previously valid cursor may return no results  ## Example  This example is illustrative only.  A small `desiredResults` parameter has been used to make the example concise. API users should generally choose larger values, subject to the maximum value.  The following API call would retrieve the *first* set of results.  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?desiredResults=3 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-20T00:00:00.000\",       \"creditBalance\": 109,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-19T00:00:00.000\",       \"creditBalance\": 108,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-18T00:00:00.000\",       \"creditBalance\": 107,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"after\": \"107\"     },     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107\"   } } ```  The `results` section contains the payload of the API.  The `pagination` section contains details needed to retrieve additional results.  API users can choose to use the `next` URL directly or use the `after` cursor token to construct the URL themselves.  The API call to query the next set of results would be  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-17T00:00:00.000\",       \"creditBalance\": 106,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-16T00:00:00.000\",       \"creditBalance\": 105,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-15T00:00:00.000\",       \"creditBalance\": 104,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"before\": \"106\",       \"after\": \"104\"     },     \"previous\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&before=106&desiredResults=3\",     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=104\"   } } ```  This time the response contains both `before` and `after` tokens and `previous` and `next` URLs. The `before` token or `previous` link can be used to navigate backwards in the results. 
 *
 * The version of the OpenAPI document: 1.61.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import AcceptDraftBillPostBillsIdAcceptdraftResponse404 from '../model/AcceptDraftBillPostBillsIdAcceptdraftResponse404';
import BillEmailsGetBillemailsIdResponse200 from '../model/BillEmailsGetBillemailsIdResponse200';
import BillEmailsGetBillemailsIdResponse404 from '../model/BillEmailsGetBillemailsIdResponse404';
import GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200 from '../model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200';
import GetBillFileGetBillfilesIdResponse200 from '../model/GetBillFileGetBillfilesIdResponse200';
import GetBillFileGetBillfilesIdResponse404 from '../model/GetBillFileGetBillfilesIdResponse404';
import GetBillFileImageGetBillfilesIdImageResponse200 from '../model/GetBillFileImageGetBillfilesIdImageResponse200';
import GetBillFileImageGetBillfilesIdImageResponse410 from '../model/GetBillFileImageGetBillfilesIdImageResponse410';
import GetBillGetBillsIdResponse200 from '../model/GetBillGetBillsIdResponse200';
import GetBillGetBillsIdResponse404 from '../model/GetBillGetBillsIdResponse404';
import ReversionBillPostBillsIdReversionResponse200 from '../model/ReversionBillPostBillsIdReversionResponse200';
import ReversionBillPostBillsIdReversionResponse404 from '../model/ReversionBillPostBillsIdReversionResponse404';

/**
* Bills service.
* @module api/BillsApi
* @version 1.61.1
*/
export default class BillsApi {

    /**
    * Constructs a new BillsApi. 
    * @alias module:api/BillsApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the juniferBillEmailsIdGet operation.
     * @callback module:api/BillsApi~juniferBillEmailsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/BillEmailsGetBillemailsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get bill email
     * Gets the email associated with a bill
     * @param {Number} id Bill ID (not email id)
     * @param {module:api/BillsApi~juniferBillEmailsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/BillEmailsGetBillemailsIdResponse200}
     */
    juniferBillEmailsIdGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillEmailsIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = BillEmailsGetBillemailsIdResponse200;
      return this.apiClient.callApi(
        '/junifer/billEmails/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferBillFilesIdGet operation.
     * @callback module:api/BillsApi~juniferBillFilesIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBillFileGetBillfilesIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get bill file
     * Get bill file record by its ID.  Note that the ID is of the bill file, NOT the bill. To get the correct bill files follow the link URL in the response of e.g. Get Bill API, where the correct bill file ID is prepopulated in the URL by the system.
     * @param {Number} id Bill file ID
     * @param {module:api/BillsApi~juniferBillFilesIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBillFileGetBillfilesIdResponse200}
     */
    juniferBillFilesIdGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillFilesIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetBillFileGetBillfilesIdResponse200;
      return this.apiClient.callApi(
        '/junifer/billFiles/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferBillFilesIdImageGet operation.
     * @callback module:api/BillsApi~juniferBillFilesIdImageGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBillFileImageGetBillfilesIdImageResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get bill file image
     * Initiates bill file image (usually PDF file) download if the file is available
     * @param {Number} id Bill file ID
     * @param {module:api/BillsApi~juniferBillFilesIdImageGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBillFileImageGetBillfilesIdImageResponse200}
     */
    juniferBillFilesIdImageGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillFilesIdImageGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetBillFileImageGetBillfilesIdImageResponse200;
      return this.apiClient.callApi(
        '/junifer/billFiles/{id}/image', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferBillsIdAcceptDraftPost operation.
     * @callback module:api/BillsApi~juniferBillsIdAcceptDraftPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Accept Draft bill
     * Accept Draft bill
     * @param {Number} id Bill ID to accept
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.closeAssociatedTicketsFl Flag to close tickets associated to the bill when it is accepted. Defaults to false if not set
     * @param {module:api/BillsApi~juniferBillsIdAcceptDraftPostCallback} callback The callback function, accepting three arguments: error, data, response
     */
    juniferBillsIdAcceptDraftPost(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillsIdAcceptDraftPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'closeAssociatedTicketsFl': opts['closeAssociatedTicketsFl']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/junifer/bills/{id}/acceptDraft', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferBillsIdBillBreakdownLinesGet operation.
     * @callback module:api/BillsApi~juniferBillsIdBillBreakdownLinesGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get bill's breakdown lines
     * Get bill's breakdown lines by its bill ID
     * @param {Number} id Bill ID
     * @param {module:api/BillsApi~juniferBillsIdBillBreakdownLinesGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200}
     */
    juniferBillsIdBillBreakdownLinesGet(id, callback) {
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillsIdBillBreakdownLinesGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200;
      return this.apiClient.callApi(
        '/junifer/bills/{id}/billBreakdownLines', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferBillsIdGet operation.
     * @callback module:api/BillsApi~juniferBillsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/GetBillGetBillsIdResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get bill
     * Get bill record by its ID
     * @param {Number} id Bill ID
     * @param {Object} opts Optional parameters
     * @param {Number} opts.contactId Restrict billFiles to a specific contact
     * @param {module:api/BillsApi~juniferBillsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/GetBillGetBillsIdResponse200}
     */
    juniferBillsIdGet(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillsIdGet");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'contactId': opts['contactId']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = GetBillGetBillsIdResponse200;
      return this.apiClient.callApi(
        '/junifer/bills/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the juniferBillsIdReversionPost operation.
     * @callback module:api/BillsApi~juniferBillsIdReversionPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ReversionBillPostBillsIdReversionResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Reversion bill
     * Create new version of bill
     * @param {Number} id Bill ID
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.consolidateAllClosedBillPeriodsFl Flag to consolidate all Closed bill periods onto this bill period. Defaults to false if not specified
     * @param {Boolean} opts.consolidateAllOpenBillPeriodsFl Flag to consolidate all Open bill periods onto this bill period. Defaults to false if not specified
     * @param {module:api/BillsApi~juniferBillsIdReversionPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ReversionBillPostBillsIdReversionResponse200}
     */
    juniferBillsIdReversionPost(id, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling juniferBillsIdReversionPost");
      }

      let pathParams = {
        'id': id
      };
      let queryParams = {
        'consolidateAllClosedBillPeriodsFl': opts['consolidateAllClosedBillPeriodsFl'],
        'consolidateAllOpenBillPeriodsFl': opts['consolidateAllOpenBillPeriodsFl']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['tokenAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = ReversionBillPostBillsIdReversionResponse200;
      return this.apiClient.callApi(
        '/junifer/bills/{id}/reversion', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
