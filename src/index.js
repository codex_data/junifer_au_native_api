/**
 * Junifer AU Native API
 * Junifer Native Web API provided by the Gentrack Cloud Integration Services  # How It Works  For how to prepare your application to call Junifer Native Web API, please refer to: * [Read me first](https://portal.integration.gentrack.cloud/api/index.html#section/Read-me-first) * [Sovereignty Regions](https://portal.integration.gentrack.cloud/api/index.html#section/Sovereignty-Regions) * [Preparing your request](https://portal.integration.gentrack.cloud/api/index.html#section/Preparing-your-request) * [Authentication](https://portal.integration.gentrack.cloud/api/index.html#section/Authentication) * [Responses](https://portal.integration.gentrack.cloud/api/index.html#section/Responses)  ## Sample of requesting contact details The following is an example of calling get contact details API in UK region:  ```bash curl -X GET \\     https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJZCI6IjA2NDYxODczLTFlOWMtNGVkMy1iZWZkLTU3NGY5ZmEwYjExYyIsIm9yZ0lkIj oiQzAwMDAiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJpYXQiOjE1MzA1ODkwODcsImV4cCI6MTUzMDU5MjY4NywiaXNzIjoiaHR0cHM6Ly9hcGkuZ2VudHJhY2suaW8vIn0.WUAFYWTTEAy Q0Bt4YXu-mxCXd-Y9ehwdZcYvcGNnyLTZH_hiJjtXWWfsx69M606pvCP6lLMT7MfK-F3E4rLO6KAlGsuz4A_7oVWQf4QNeR178GnwgmqQRw5OnLxwPbPrRa9nODngwGq8dcWejhmEYU6i w02bvYdQBHnnsc3Kpyzw7Wdv_3jnBS4TPYS20muQOgG6KxRp9hLJM7ERLoAbsULwqdPOV8eUJJhGrq1NDuH_lA83YRDZmCWEzw96tSm3hb7y88kXs-4OvamnO1m5wFPBx69VximlS4Ltr 3ztqU2s3fHoj0OJLIafge9JvTgvuB6noHfs1uSRaahvstGJAA' ```  The sample of response is as follows.  ```json {    \"id\": 1010,    \"contactType\": \"Business\",    \"title\": \"Mr\",    \"forename\": \"Tony\",    \"surname\": \"Soprano\",    \"email\": \"bigtone@didntseeanything.com\",    \"dateOfBirth\": \"1959-08-24\",    \"phoneNumber1\": \"44626478370\",    \"address\":{      \"address1\": \"633 Stag Trail Road\",      \"address2\": \"North Caldwell\",      \"address3\": \"New Jersey\",      \"postcode\": \"NE18 0PY\"    },    \"links\": {      \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010\",      \"accounts\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/contacts/1010/accounts\"    } } ```  # General Rules/Patterns  ## Navigation A links section is returned in most requests.  This section contains how to access the entity itself and, more importantly, how to access items related to the entity.  For example take the following response: ```json {   \"id\": 1,   \"name\": \"Mr Joe Bloggs\",   \"number\": \"00000001\",   \"surname\": \"Bloggs\",   \"links\": {     \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\"   },   \"accounts\": [     {       \"id\": 7,       \"name\": \"Invoice\",       \"number\": \"00000001\",       \"currency\": \"GBP\",       \"fromDt\": \"2013-05-01\",       \"createdDttm\": \"2013-05-08T13:36:34.000\",       \"balance\": 0,       \"billingAddress\": {         \"address1\": \"11 Tuppy Street\"       },       \"links\": {         \"self\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7\",         \"customer\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/customers/1\",         \"bills\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/bills\",         \"payments\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/accounts/7/payments\"       }     }   ] } ```  The `self` URL in the `links` section can be used to access the entities listed in the response. In the above example there are self fields for the customer itself, and the accounts under this customer. There are other links under the account.  For example the above example contains links for the following:  - `bills` – List of Bills for the Account - `payments` – List of Payments for the Account - `customer` – Link back to the Account’s Customer  ## About IDs  The id in the API requests (JSON payload and URL) and responses identifies the resource within the API. It has no business meaning or purpose, therefore shouldn't be exposed to the end user in any way.  As the IDs of the resources aren't sequential or aligned with any other resource IDs, it is highly recommended using the links section from the responses to navigate to other related resources. Given the example above, the customer with ID 1 links to the account with ID 7. The links section will populate the correct IDs in the URLs so none of the assumptions about related resource IDs need to be made by the implementer.  # Accessing accounts by account number Throughout this API, anywhere an account is accessed by its `id` (i.e. the URL includes `/accounts/:id`), it can also be accessed by its account number by replacing `/accounts/:id` with `accounts/accountNumber/:num`.  For example: the Create Account Credit endpoint, available at `/accounts/:id/accountCredits`, can also be accessed at `/accounts/accountNumber/:num/accountCredits`.  # Standard data types in JSON The standard data types are as follows:  - String – Quoted as per JSON standard - Number – Unquoted as per JSON standard - Decimal – Unquoted as per JSON standard - Date – In the standard ISO format \"yyyy-MM-dd\" - DateTime – In the standard ISO format \"yyyy-MM-ddThh:mm:ss.SSS\" (no time zone component) - Boolean – Unquoted 'true' and 'false' are the only valid values  # Ordering of data Response representations are not ordered unless specified on a specific resource. Therefore, clients should be implemented to use the name of fields/objects as the key for accessing data.  For example in JSON objects are comprised of key/value pairs and clients should be implemented to search on the ‘key’ value rather than looking for the nth key/value pair in the object.  # Changes of data Response representations may change without warning in the following ways.  - Additional key/value pairs may be added to an existing object - Additional sub-objects may be added to an existing object  These changes are considered non-breaking.  Therefore, clients should be implemented to consume only the data they need and ignore any other information.  # Versioning The API is usually changed in each release of Junifer. Navigate to different versions by clicking the dropdown menu in the top right of this web page.  # Pagination Some REST APIs that provide a list of results support pagination.  Pagination allows API users to query potential large data sets but only receive a sub-set of the results on the initial query. Subsequent queries can use the response of a prior query to retrieve the next or previous set of results.  Pagination functionality and behaviour will be documented on each API that requires it.  ## Pagination on existing APIs Existing APIs may have pagination support added. In the future this pagination will become mandatory.  In order to facilitate API users planning and executing upgrades against the existing APIs, pagination is optional during a transition period. API users can enable pagination on these APIs by specifying the `pagination` query parameter. This parameter only needs to be present, the value is not required or checked. If this query parameter is specified, then pagination will be enabled. Otherwise, the existing non-paginated functionality, that returns all results, will be used.  API users must plan and execute upgrades to their existing applications to use the pagination support during the transition period. At the end of the transition period pagination will become mandatory on these APIs.  For new APIs that support pagination, it will be mandatory with the initial release of the API.  ## Ordering The ordering that the API provides will be documented for each API.  An ordering is always required to ensure moving between pages is consistent.  Some APIs may define an ordering on a value or values in the response where appropriate. For example results may be ordered by a date/time property of the results.  Other APIs may not define an explicit ordering but will guarantee the order is consistent for multiple API calls.  ## Limiting results of each page The `desiredResults` query parameter can be used to specify the maximum number of results that may be returned.  The APIs treat this as a hint so API callers should expect to sometimes receive a different number of results. APIs will endeavour to ensure no more than `desiredResults` are returned but for small values of `desiredResults` (less than 10) this may not be possible. This is due to how pages of data are structured inside the product. The API will prefer to return some data rather than no data where it is not possible to return less than `desiredResults`  If `desiredResults` is not specified the default maximum value for the API will be used. Unless documented differently, this value is 100.  If `desiredResults` is specified higher than the default maximum for the API a 400 error response will be received.  ## Cursors The pagination provided by the APIs is cursor-based.  In the JSON response additional metadata will be provided when pagination is present.  - `after` cursor points to the start of the results that have been returned - `before` cursor points to the end of the results that have been returned  These cursors can be used to make subsequent API calls. The API URL should be unchanged except for setting the `before` or `after` query parameter. If both parameters are specified, then an error 400 response will be received.  Cursor tokens must only be used for pagination purposes. API users should not infer any business meaning or logic from cursor representations. These representations may change without warning when Junifer is upgraded.  When scrolling forward the `after` cursor will be absent from the response when there are no more results after the current set.  When scrolling backwards the `before` cursor will be absent from the response when there are no more results before the current set.  In addition, a paginated response will include a `next` and `previous` URLs for convenience.  ## Best practices The following best practices must be adhered to by users of the pagination APIs:  - Don't store cursors or next and previous URLs for long. Cursors can quickly become invalid if items are added or deleted to the data set - If an invalid cursor is used, then a 404 response will be returned in this situation. API users should design a policy to handle this situation - API users should handle empty data sets. In limited cases a pagination query using a previously valid cursor may return no results  ## Example  This example is illustrative only.  A small `desiredResults` parameter has been used to make the example concise. API users should generally choose larger values, subject to the maximum value.  The following API call would retrieve the *first* set of results.  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?desiredResults=3 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-20T00:00:00.000\",       \"creditBalance\": 109,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-19T00:00:00.000\",       \"creditBalance\": 108,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-18T00:00:00.000\",       \"creditBalance\": 107,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"after\": \"107\"     },     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107\"   } } ```  The `results` section contains the payload of the API.  The `pagination` section contains details needed to retrieve additional results.  API users can choose to use the `next` URL directly or use the `after` cursor token to construct the URL themselves.  The API call to query the next set of results would be  ### Request  ```bash https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=107 \\     -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9....aahvstGJAA' ```  ### Response ```json {   \"results\": [     {       \"snapshotDateTime\": \"2019-04-17T00:00:00.000\",       \"creditBalance\": 106,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-16T00:00:00.000\",       \"creditBalance\": 105,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     },     {       \"snapshotDateTime\": \"2019-04-15T00:00:00.000\",       \"creditBalance\": 104,       \"emergencyCreditBalance\": 0,       \"accumulatedDebtRegister\": 0,       \"timeDebtRegister1\": 0,       \"timeDebtRegister2\": 0,       \"paymentDebtRegister\": 0     }   ],   \"pagination\": {     \"cursors\": {       \"before\": \"106\",       \"after\": \"104\"     },     \"previous\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&before=106&desiredResults=3\",     \"next\": \"https://api-uk.integration.gentrack.cloud/v1/junifer/uk/meterPoints/1/payg/balances?pagination=&desiredResults=3&after=104\"   } } ```  This time the response contains both `before` and `after` tokens and `previous` and `next` URLs. The `before` token or `previous` link can be used to navigate backwards in the results. 
 *
 * The version of the OpenAPI document: 1.61.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from './ApiClient';
import AcceptDraftBillPostBillsIdAcceptdraftResponse404 from './model/AcceptDraftBillPostBillsIdAcceptdraftResponse404';
import AcceptQuotePostQuotesIdAcceptquoteResponse400 from './model/AcceptQuotePostQuotesIdAcceptquoteResponse400';
import AcceptQuotePostQuotesIdAcceptquoteResponse404 from './model/AcceptQuotePostQuotesIdAcceptquoteResponse404';
import AddPropertiesPostProspectsIdAddPropertiesResponse400 from './model/AddPropertiesPostProspectsIdAddPropertiesResponse400';
import AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody from './model/AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody';
import AusCreateConcessionPostAuConcessionsCreateconcessionResponse200 from './model/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200';
import AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody from './model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody';
import AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress from './model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress';
import AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts from './model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts';
import AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct from './model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct';
import AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints from './model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints';
import AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts from './model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts';
import AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress from './model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress';
import AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200 from './model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200';
import AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse400 from './model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse400';
import AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody from './model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody';
import AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct from './model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct';
import AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200 from './model/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200';
import AusEnrolCustomerPostAuCustomersEnrolcustomerResponse400 from './model/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse400';
import AusGetDiscountsGetAuAccountsIdDiscountsResponse200 from './model/AusGetDiscountsGetAuAccountsIdDiscountsResponse200';
import AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody from './model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody';
import AusLifeSupportSensitiveLoadPostAuCustomersLifesupportResponse400 from './model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportResponse400';
import AusLinkDiscountPostAuAccountsIdDiscountsRequestBody from './model/AusLinkDiscountPostAuAccountsIdDiscountsRequestBody';
import AusRenewAccountPostAuAccountsIdRenewalRequestBody from './model/AusRenewAccountPostAuAccountsIdRenewalRequestBody';
import AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements from './model/AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements';
import AusRenewAccountPostAuAccountsIdRenewalResponse200 from './model/AusRenewAccountPostAuAccountsIdRenewalResponse200';
import AusRenewAccountPostAuAccountsIdRenewalResponse200Results from './model/AusRenewAccountPostAuAccountsIdRenewalResponse200Results';
import AusRenewAccountPostAuAccountsIdRenewalResponse400 from './model/AusRenewAccountPostAuAccountsIdRenewalResponse400';
import AusRenewAccountPostAuAccountsIdRenewalResponse404 from './model/AusRenewAccountPostAuAccountsIdRenewalResponse404';
import AusSiteAccessPostAuCustomersSiteaccessRequestBody from './model/AusSiteAccessPostAuCustomersSiteaccessRequestBody';
import AusSiteAccessPostAuCustomersSiteaccessResponse400 from './model/AusSiteAccessPostAuCustomersSiteaccessResponse400';
import BillEmailsGetBillemailsIdResponse200 from './model/BillEmailsGetBillemailsIdResponse200';
import BillEmailsGetBillemailsIdResponse200Files from './model/BillEmailsGetBillemailsIdResponse200Files';
import BillEmailsGetBillemailsIdResponse200Links from './model/BillEmailsGetBillemailsIdResponse200Links';
import BillEmailsGetBillemailsIdResponse200Links1 from './model/BillEmailsGetBillemailsIdResponse200Links1';
import BillEmailsGetBillemailsIdResponse404 from './model/BillEmailsGetBillemailsIdResponse404';
import CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400 from './model/CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400';
import CancelAccountCreditDeleteAccountcreditsIdResponse400 from './model/CancelAccountCreditDeleteAccountcreditsIdResponse400';
import CancelAccountDebitDeleteAccountdebitsIdResponse400 from './model/CancelAccountDebitDeleteAccountdebitsIdResponse400';
import CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400 from './model/CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400';
import CancelTicketPostTicketsIdCancelticketResponse400 from './model/CancelTicketPostTicketsIdCancelticketResponse400';
import ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody from './model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody';
import ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400 from './model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400';
import ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404 from './model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404';
import CommunicationsEmailsGetCommunicationsemailsIdResponse200 from './model/CommunicationsEmailsGetCommunicationsemailsIdResponse200';
import CommunicationsEmailsGetCommunicationsemailsIdResponse200Links from './model/CommunicationsEmailsGetCommunicationsemailsIdResponse200Links';
import CommunicationsEmailsGetCommunicationsemailsIdResponse404 from './model/CommunicationsEmailsGetCommunicationsemailsIdResponse404';
import ContactsGetAccountsIdContactsResponse200 from './model/ContactsGetAccountsIdContactsResponse200';
import ContactsGetAccountsIdContactsResponse200Address from './model/ContactsGetAccountsIdContactsResponse200Address';
import ContactsGetAccountsIdContactsResponse200Links from './model/ContactsGetAccountsIdContactsResponse200Links';
import ContactsGetAccountsIdContactsResponse400 from './model/ContactsGetAccountsIdContactsResponse400';
import ContactsGetCustomersIdContactsResponse200 from './model/ContactsGetCustomersIdContactsResponse200';
import ContactsGetCustomersIdContactsResponse400 from './model/ContactsGetCustomersIdContactsResponse400';
import CreateAccountCreditPostAccountsIdAccountcreditsRequestBody from './model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody';
import CreateAccountCreditPostAccountsIdAccountcreditsResponse400 from './model/CreateAccountCreditPostAccountsIdAccountcreditsResponse400';
import CreateAccountCreditPostAccountsIdAccountcreditsResponse404 from './model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404';
import CreateAccountDebitPostAccountsIdAccountdebitsRequestBody from './model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody';
import CreateAccountDebitPostAccountsIdAccountdebitsResponse400 from './model/CreateAccountDebitPostAccountsIdAccountdebitsResponse400';
import CreateAccountDebitPostAccountsIdAccountdebitsResponse404 from './model/CreateAccountDebitPostAccountsIdAccountdebitsResponse404';
import CreateAccountNotePostAccountsIdNoteRequestBody from './model/CreateAccountNotePostAccountsIdNoteRequestBody';
import CreateAccountNotePostAccountsIdNoteResponse200 from './model/CreateAccountNotePostAccountsIdNoteResponse200';
import CreateAccountNotePostAccountsIdNoteResponse400 from './model/CreateAccountNotePostAccountsIdNoteResponse400';
import CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody from './model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody';
import CreateAccountRepaymentPostAccountsIdRepaymentsResponse200 from './model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200';
import CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links from './model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links';
import CreateAccountRepaymentPostAccountsIdRepaymentsResponse400 from './model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse400';
import CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody from './model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody';
import CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200 from './model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200';
import CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400 from './model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400';
import CreateAccountTicketPostAccountsIdTicketsRequestBody from './model/CreateAccountTicketPostAccountsIdTicketsRequestBody';
import CreateAccountTicketPostAccountsIdTicketsResponse200 from './model/CreateAccountTicketPostAccountsIdTicketsResponse200';
import CreateAccountTicketPostAccountsIdTicketsResponse200Links from './model/CreateAccountTicketPostAccountsIdTicketsResponse200Links';
import CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities from './model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities';
import CreateAccountTicketPostAccountsIdTicketsResponse400 from './model/CreateAccountTicketPostAccountsIdTicketsResponse400';
import CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody from './model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody';
import CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200 from './model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200';
import CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400 from './model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400';
import CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404 from './model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404';
import CreatePropertyPostPropertysRequestBody from './model/CreatePropertyPostPropertysRequestBody';
import CreatePropertyPostPropertysResponse200 from './model/CreatePropertyPostPropertysResponse200';
import CreatePropertyPostPropertysResponse400 from './model/CreatePropertyPostPropertysResponse400';
import CreateProspectPostProspectsRequestBody from './model/CreateProspectPostProspectsRequestBody';
import CreateProspectPostProspectsRequestBodyCustomer from './model/CreateProspectPostProspectsRequestBodyCustomer';
import CreateProspectPostProspectsRequestBodyCustomerCompanyAddress from './model/CreateProspectPostProspectsRequestBodyCustomerCompanyAddress';
import CreateProspectPostProspectsRequestBodyCustomerPrimaryContact from './model/CreateProspectPostProspectsRequestBodyCustomerPrimaryContact';
import CreateProspectPostProspectsResponse200 from './model/CreateProspectPostProspectsResponse200';
import CreateProspectPostProspectsResponse400 from './model/CreateProspectPostProspectsResponse400';
import CreateProspectPutCustomersIdCreateprospectRequestBody from './model/CreateProspectPutCustomersIdCreateprospectRequestBody';
import CreateProspectPutCustomersIdCreateprospectResponse200 from './model/CreateProspectPutCustomersIdCreateprospectResponse200';
import CreateProspectPutCustomersIdCreateprospectResponse400 from './model/CreateProspectPutCustomersIdCreateprospectResponse400';
import CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200 from './model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200';
import CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse400 from './model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse400';
import CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401 from './model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401';
import EmailPostCommunicationsEmailRequestBody from './model/EmailPostCommunicationsEmailRequestBody';
import EmailPostCommunicationsEmailResponse400 from './model/EmailPostCommunicationsEmailResponse400';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody from './model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo from './model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate from './model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct from './model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct from './model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns from './model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress from './model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress';
import EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200 from './model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200';
import EnrolCustomerPostCustomersEnrolcustomerRequestBody from './model/EnrolCustomerPostCustomersEnrolcustomerRequestBody';
import EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress from './model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress';
import EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts from './model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts';
import EnrolCustomerPostCustomersEnrolcustomerResponse200 from './model/EnrolCustomerPostCustomersEnrolcustomerResponse200';
import EnrolCustomerPostCustomersEnrolcustomerResponse400 from './model/EnrolCustomerPostCustomersEnrolcustomerResponse400';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200 from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links';
import EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400 from './model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400';
import GetAccountAgreementsGetAccountsIdAgreementsResponse200 from './model/GetAccountAgreementsGetAccountsIdAgreementsResponse200';
import GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType from './model/GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType';
import GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets from './model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets';
import GetAccountAgreementsGetAccountsIdAgreementsResponse200Links from './model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links';
import GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1 from './model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1';
import GetAccountAgreementsGetAccountsIdAgreementsResponse200Products from './model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Products';
import GetAccountBillRequestsGetAccountsIdBillrequestsResponse200 from './model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200';
import GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links from './model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links';
import GetAccountBillRequestsGetAccountsIdBillrequestsResponse404 from './model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse404';
import GetAccountBillsGetAccountsIdBillsResponse200 from './model/GetAccountBillsGetAccountsIdBillsResponse200';
import GetAccountBillsGetAccountsIdBillsResponse200Links from './model/GetAccountBillsGetAccountsIdBillsResponse200Links';
import GetAccountByNumberGetAccountsAccountnumberNumResponse400 from './model/GetAccountByNumberGetAccountsAccountnumberNumResponse400';
import GetAccountByNumberGetAccountsAccountnumberNumResponse404 from './model/GetAccountByNumberGetAccountsAccountnumberNumResponse404';
import GetAccountCommunicationsGetAccountsIdCommunicationsResponse200 from './model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200';
import GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files from './model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files';
import GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links from './model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links';
import GetAccountCommunicationsGetAccountsIdCommunicationsResponse400 from './model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse400';
import GetAccountCreditGetAccountcreditsIdResponse200 from './model/GetAccountCreditGetAccountcreditsIdResponse200';
import GetAccountCreditNotesGetAccountsIdCreditsResponse200 from './model/GetAccountCreditNotesGetAccountsIdCreditsResponse200';
import GetAccountCreditNotesGetAccountsIdCreditsResponse200Links from './model/GetAccountCreditNotesGetAccountsIdCreditsResponse200Links';
import GetAccountCreditsGetAccountsIdAccountcreditsResponse200 from './model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200';
import GetAccountDebitGetAccountdebitsIdResponse200 from './model/GetAccountDebitGetAccountdebitsIdResponse200';
import GetAccountDebitsGetAccountsIdAccountdebitsResponse200 from './model/GetAccountDebitsGetAccountsIdAccountdebitsResponse200';
import GetAccountGetAccountsIdResponse200 from './model/GetAccountGetAccountsIdResponse200';
import GetAccountGetAccountsIdResponse200BillingAddress from './model/GetAccountGetAccountsIdResponse200BillingAddress';
import GetAccountGetAccountsIdResponse200Links from './model/GetAccountGetAccountsIdResponse200Links';
import GetAccountGetAccountsIdResponse404 from './model/GetAccountGetAccountsIdResponse404';
import GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200 from './model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200';
import GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links from './model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links';
import GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200 from './model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200';
import GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links from './model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links';
import GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions from './model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions';
import GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200 from './model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200';
import GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200DebtRecoveryDetails from './model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200DebtRecoveryDetails';
import GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links from './model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links';
import GetAccountPaymentsGetAccountsIdPaymentsResponse200 from './model/GetAccountPaymentsGetAccountsIdPaymentsResponse200';
import GetAccountPaymentsGetAccountsIdPaymentsResponse200Links from './model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links';
import GetAccountPaymentsGetAccountsIdPaymentsResponse400 from './model/GetAccountPaymentsGetAccountsIdPaymentsResponse400';
import GetAccountProductDetailsGetAccountsIdProductdetailsResponse200 from './model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200';
import GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints from './model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints';
import GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType from './model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType';
import GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress from './model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress';
import GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates from './model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates';
import GetAccountProductDetailsGetAccountsIdProductdetailsResponse400 from './model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse400';
import GetAccountPropertysGetAccountsIdPropertysResponse200 from './model/GetAccountPropertysGetAccountsIdPropertysResponse200';
import GetAccountPropertysGetAccountsIdPropertysResponse200Address from './model/GetAccountPropertysGetAccountsIdPropertysResponse200Address';
import GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200 from './model/GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200';
import GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200 from './model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200';
import GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links from './model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links';
import GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400 from './model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400';
import GetAccountTicketsGetAccountsIdTicketsResponse200 from './model/GetAccountTicketsGetAccountsIdTicketsResponse200';
import GetAccountTicketsGetAccountsIdTicketsResponse400 from './model/GetAccountTicketsGetAccountsIdTicketsResponse400';
import GetAccountTransactionsGetAccountsIdTransactionsResponse200 from './model/GetAccountTransactionsGetAccountsIdTransactionsResponse200';
import GetAccountTransactionsGetAccountsIdTransactionsResponse200Links from './model/GetAccountTransactionsGetAccountsIdTransactionsResponse200Links';
import GetAccountTransactionsGetAccountsIdTransactionsResponse400 from './model/GetAccountTransactionsGetAccountsIdTransactionsResponse400';
import GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200 from './model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200';
import GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links from './model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links';
import GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400 from './model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400';
import GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404 from './model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404';
import GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200 from './model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200';
import GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months from './model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months';
import GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400 from './model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400';
import GetAlertGetAlertsIdResponse200 from './model/GetAlertGetAlertsIdResponse200';
import GetAlertGetAlertsIdResponse200Links from './model/GetAlertGetAlertsIdResponse200Links';
import GetAlertGetAlertsIdResponse404 from './model/GetAlertGetAlertsIdResponse404';
import GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200 from './model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200';
import GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400 from './model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400';
import GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200 from './model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200';
import GetBillFileGetBillfilesIdResponse200 from './model/GetBillFileGetBillfilesIdResponse200';
import GetBillFileGetBillfilesIdResponse200Links from './model/GetBillFileGetBillfilesIdResponse200Links';
import GetBillFileGetBillfilesIdResponse404 from './model/GetBillFileGetBillfilesIdResponse404';
import GetBillFileImageGetBillfilesIdImageResponse200 from './model/GetBillFileImageGetBillfilesIdImageResponse200';
import GetBillFileImageGetBillfilesIdImageResponse410 from './model/GetBillFileImageGetBillfilesIdImageResponse410';
import GetBillGetBillsIdResponse200 from './model/GetBillGetBillsIdResponse200';
import GetBillGetBillsIdResponse200BillFiles from './model/GetBillGetBillsIdResponse200BillFiles';
import GetBillGetBillsIdResponse200Links from './model/GetBillGetBillsIdResponse200Links';
import GetBillGetBillsIdResponse200Links1 from './model/GetBillGetBillsIdResponse200Links1';
import GetBillGetBillsIdResponse404 from './model/GetBillGetBillsIdResponse404';
import GetBillPeriodGetBillperiodsIdResponse200 from './model/GetBillPeriodGetBillperiodsIdResponse200';
import GetBillPeriodGetBillperiodsIdResponse404 from './model/GetBillPeriodGetBillperiodsIdResponse404';
import GetBillRequestGetBillrequestsIdResponse200 from './model/GetBillRequestGetBillrequestsIdResponse200';
import GetBillRequestGetBillrequestsIdResponse200Links from './model/GetBillRequestGetBillrequestsIdResponse200Links';
import GetBillRequestGetBillrequestsIdResponse404 from './model/GetBillRequestGetBillrequestsIdResponse404';
import GetBillingEntityGetBillingentitiesIdResponse200 from './model/GetBillingEntityGetBillingentitiesIdResponse200';
import GetBillingEntityGetBillingentitiesIdResponse200Links from './model/GetBillingEntityGetBillingentitiesIdResponse200Links';
import GetBillingEntityGetBillingentitiesIdResponse404 from './model/GetBillingEntityGetBillingentitiesIdResponse404';
import GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200 from './model/GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200';
import GetBrokerGetBrokersIdResponse200 from './model/GetBrokerGetBrokersIdResponse200';
import GetBrokerGetBrokersIdResponse200Links from './model/GetBrokerGetBrokersIdResponse200Links';
import GetBrokerGetBrokersIdResponse404 from './model/GetBrokerGetBrokersIdResponse404';
import GetBrokerLinkageGetBrokerlinkagesIdResponse200 from './model/GetBrokerLinkageGetBrokerlinkagesIdResponse200';
import GetBrokerLinkageGetBrokerlinkagesIdResponse200Links from './model/GetBrokerLinkageGetBrokerlinkagesIdResponse200Links';
import GetBrokerLinkageGetBrokerlinkagesIdResponse404 from './model/GetBrokerLinkageGetBrokerlinkagesIdResponse404';
import GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200 from './model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200';
import GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links from './model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links';
import GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse404 from './model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse404';
import GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200 from './model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200';
import GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404 from './model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404';
import GetCommunicationsFileGetCommunicationsfilesIdResponse200 from './model/GetCommunicationsFileGetCommunicationsfilesIdResponse200';
import GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200 from './model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200';
import GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse410 from './model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse410';
import GetCommunicationsGetCommunicationsIdResponse200 from './model/GetCommunicationsGetCommunicationsIdResponse200';
import GetCommunicationsGetCommunicationsIdResponse200Links from './model/GetCommunicationsGetCommunicationsIdResponse200Links';
import GetCommunicationsGetCommunicationsIdResponse404 from './model/GetCommunicationsGetCommunicationsIdResponse404';
import GetContactGetContactsIdResponse200 from './model/GetContactGetContactsIdResponse200';
import GetContactGetContactsIdResponse200Address from './model/GetContactGetContactsIdResponse200Address';
import GetContactGetContactsIdResponse400 from './model/GetContactGetContactsIdResponse400';
import GetContactsAccountsGetContactsIdAccountsResponse200 from './model/GetContactsAccountsGetContactsIdAccountsResponse200';
import GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress from './model/GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress';
import GetContactsAccountsGetContactsIdAccountsResponse200Links from './model/GetContactsAccountsGetContactsIdAccountsResponse200Links';
import GetContactsAccountsGetContactsIdAccountsResponse400 from './model/GetContactsAccountsGetContactsIdAccountsResponse400';
import GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200 from './model/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200';
import GetCreditFileGetCreditfilesIdResponse200 from './model/GetCreditFileGetCreditfilesIdResponse200';
import GetCreditFileGetCreditfilesIdResponse200Links from './model/GetCreditFileGetCreditfilesIdResponse200Links';
import GetCreditFileGetCreditfilesIdResponse404 from './model/GetCreditFileGetCreditfilesIdResponse404';
import GetCreditFileImageGetCreditfilesIdImageResponse200 from './model/GetCreditFileImageGetCreditfilesIdImageResponse200';
import GetCreditFileImageGetCreditfilesIdImageResponse410 from './model/GetCreditFileImageGetCreditfilesIdImageResponse410';
import GetCreditGetCreditsIdResponse200 from './model/GetCreditGetCreditsIdResponse200';
import GetCreditGetCreditsIdResponse200CreditFiles from './model/GetCreditGetCreditsIdResponse200CreditFiles';
import GetCreditGetCreditsIdResponse200Links from './model/GetCreditGetCreditsIdResponse200Links';
import GetCreditGetCreditsIdResponse200Links1 from './model/GetCreditGetCreditsIdResponse200Links1';
import GetCreditGetCreditsIdResponse404 from './model/GetCreditGetCreditsIdResponse404';
import GetCustomerAccountsGetCustomersIdAccountsResponse200 from './model/GetCustomerAccountsGetCustomersIdAccountsResponse200';
import GetCustomerAccountsGetCustomersIdAccountsResponse200Links from './model/GetCustomerAccountsGetCustomersIdAccountsResponse200Links';
import GetCustomerAccountsGetCustomersIdAccountsResponse400 from './model/GetCustomerAccountsGetCustomersIdAccountsResponse400';
import GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200 from './model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200';
import GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400 from './model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400';
import GetCustomerConsentsGetCustomersIdConsentsResponse200 from './model/GetCustomerConsentsGetCustomersIdConsentsResponse200';
import GetCustomerConsentsGetCustomersIdConsentsResponse400 from './model/GetCustomerConsentsGetCustomersIdConsentsResponse400';
import GetCustomerGetCustomersIdResponse200 from './model/GetCustomerGetCustomersIdResponse200';
import GetCustomerGetCustomersIdResponse200CompanyAddress from './model/GetCustomerGetCustomersIdResponse200CompanyAddress';
import GetCustomerGetCustomersIdResponse200Links from './model/GetCustomerGetCustomersIdResponse200Links';
import GetCustomerGetCustomersIdResponse200PrimaryContact from './model/GetCustomerGetCustomersIdResponse200PrimaryContact';
import GetCustomerGetCustomersIdResponse200PrimaryContactAddress from './model/GetCustomerGetCustomersIdResponse200PrimaryContactAddress';
import GetCustomerGetCustomersIdResponse400 from './model/GetCustomerGetCustomersIdResponse400';
import GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200 from './model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200';
import GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links from './model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links';
import GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400 from './model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200 from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1 from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2 from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3 from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1 from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2 from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3 from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3';
import GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse404 from './model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse404';
import GetMeterPointsGetPropertysIdMeterpointsResponse200 from './model/GetMeterPointsGetPropertysIdMeterpointsResponse200';
import GetMeterPointsGetPropertysIdMeterpointsResponse404 from './model/GetMeterPointsGetPropertysIdMeterpointsResponse404';
import GetMeterReadingsGetAuMeterpointsIdReadingsResponse200 from './model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200';
import GetMeterReadingsGetAuMeterpointsIdReadingsResponse400 from './model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse400';
import GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200 from './model/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200';
import GetMeterStructureGetAuMeterpointsIdMeterstructureResponse400 from './model/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse400';
import GetMeterStructureGetAuMeterpointsMeterstructureResponse200 from './model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200';
import GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links from './model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links';
import GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters from './model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters';
import GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers from './model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers';
import GetMeterStructureGetAuMeterpointsMeterstructureResponse400 from './model/GetMeterStructureGetAuMeterpointsMeterstructureResponse400';
import GetOfferGetOffersIdResponse200 from './model/GetOfferGetOffersIdResponse200';
import GetOfferGetOffersIdResponse200Links from './model/GetOfferGetOffersIdResponse200Links';
import GetOfferGetOffersIdResponse200MeterPoints from './model/GetOfferGetOffersIdResponse200MeterPoints';
import GetOfferGetOffersIdResponse200MeterPointsLinks from './model/GetOfferGetOffersIdResponse200MeterPointsLinks';
import GetOfferGetOffersIdResponse200PaymentParams from './model/GetOfferGetOffersIdResponse200PaymentParams';
import GetOfferGetOffersIdResponse200Quotes from './model/GetOfferGetOffersIdResponse200Quotes';
import GetOfferGetOffersIdResponse200QuotesLinks from './model/GetOfferGetOffersIdResponse200QuotesLinks';
import GetOfferGetOffersIdResponse404 from './model/GetOfferGetOffersIdResponse404';
import GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200 from './model/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200';
import GetPaymentGetPaymentsIdResponse200 from './model/GetPaymentGetPaymentsIdResponse200';
import GetPaymentGetPaymentsIdResponse404 from './model/GetPaymentGetPaymentsIdResponse404';
import GetPaymentMethodGetPaymentmethodsIdResponse200 from './model/GetPaymentMethodGetPaymentmethodsIdResponse200';
import GetPaymentMethodGetPaymentmethodsIdResponse404 from './model/GetPaymentMethodGetPaymentmethodsIdResponse404';
import GetPaymentPlanGetPaymentplansIdResponse200 from './model/GetPaymentPlanGetPaymentplansIdResponse200';
import GetPaymentPlanGetPaymentplansIdResponse200Links from './model/GetPaymentPlanGetPaymentplansIdResponse200Links';
import GetPaymentPlanGetPaymentplansIdResponse404 from './model/GetPaymentPlanGetPaymentplansIdResponse404';
import GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200 from './model/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200';
import GetPaymentRequestGetPaymentrequestsIdResponse200 from './model/GetPaymentRequestGetPaymentrequestsIdResponse200';
import GetPaymentRequestGetPaymentrequestsIdResponse200Links from './model/GetPaymentRequestGetPaymentrequestsIdResponse200Links';
import GetPaymentRequestGetPaymentrequestsIdResponse404 from './model/GetPaymentRequestGetPaymentrequestsIdResponse404';
import GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200 from './model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200';
import GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails from './model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails';
import GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404 from './model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404';
import GetProductsForNmiGetAuProductsTariffsfornmiResponse200 from './model/GetProductsForNmiGetAuProductsTariffsfornmiResponse200';
import GetProductsForNmiGetAuProductsTariffsfornmiResponse400 from './model/GetProductsForNmiGetAuProductsTariffsfornmiResponse400';
import GetPropertyGetPropertysIdResponse200 from './model/GetPropertyGetPropertysIdResponse200';
import GetPropertyGetPropertysIdResponse200Address from './model/GetPropertyGetPropertysIdResponse200Address';
import GetPropertyGetPropertysIdResponse404 from './model/GetPropertyGetPropertysIdResponse404';
import GetProspectGetProspectsIdResponse200 from './model/GetProspectGetProspectsIdResponse200';
import GetProspectGetProspectsIdResponse200Links from './model/GetProspectGetProspectsIdResponse200Links';
import GetProspectGetProspectsIdResponse404 from './model/GetProspectGetProspectsIdResponse404';
import GetProspectOffersGetProspectsIdOffersResponse200 from './model/GetProspectOffersGetProspectsIdOffersResponse200';
import GetProspectOffersGetProspectsIdOffersResponse200Links from './model/GetProspectOffersGetProspectsIdOffersResponse200Links';
import GetProspectOffersGetProspectsIdOffersResponse404 from './model/GetProspectOffersGetProspectsIdOffersResponse404';
import GetProspectPropertiesGetProspectsIdPropertysResponse200 from './model/GetProspectPropertiesGetProspectsIdPropertysResponse200';
import GetProspectPropertiesGetProspectsIdPropertysResponse200Links from './model/GetProspectPropertiesGetProspectsIdPropertysResponse200Links';
import GetProspectPropertiesGetProspectsIdPropertysResponse404 from './model/GetProspectPropertiesGetProspectsIdPropertysResponse404';
import GetQuoteFileGetQuotefilesIdResponse200 from './model/GetQuoteFileGetQuotefilesIdResponse200';
import GetQuoteFileGetQuotefilesIdResponse200Links from './model/GetQuoteFileGetQuotefilesIdResponse200Links';
import GetQuoteFileGetQuotefilesIdResponse404 from './model/GetQuoteFileGetQuotefilesIdResponse404';
import GetQuoteFileImageGetQuotefilesIdImageResponse200 from './model/GetQuoteFileImageGetQuotefilesIdImageResponse200';
import GetQuoteGetQuotesIdResponse200 from './model/GetQuoteGetQuotesIdResponse200';
import GetQuoteGetQuotesIdResponse200BrokerMargins from './model/GetQuoteGetQuotesIdResponse200BrokerMargins';
import GetQuoteGetQuotesIdResponse200QuoteFiles from './model/GetQuoteGetQuotesIdResponse200QuoteFiles';
import GetQuoteGetQuotesIdResponse200QuoteFilesLinks from './model/GetQuoteGetQuotesIdResponse200QuoteFilesLinks';
import GetQuoteGetQuotesIdResponse404 from './model/GetQuoteGetQuotesIdResponse404';
import GetQuotesGetOffersIdQuotesResponse200 from './model/GetQuotesGetOffersIdQuotesResponse200';
import GetQuotesGetOffersIdQuotesResponse200Links from './model/GetQuotesGetOffersIdQuotesResponse200Links';
import GetQuotesGetOffersIdQuotesResponse200MeterPoints from './model/GetQuotesGetOffersIdQuotesResponse200MeterPoints';
import GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks from './model/GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks';
import GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices from './model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices';
import GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents from './model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents';
import GetQuotesGetOffersIdQuotesResponse404 from './model/GetQuotesGetOffersIdQuotesResponse404';
import GetSepaDirectDebitGetSepadirectdebitsIdResponse200 from './model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200';
import GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links from './model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links';
import GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200 from './model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200';
import GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse400 from './model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse400';
import GetStripePaymentCardGetStripepaymentcardsIdResponse200 from './model/GetStripePaymentCardGetStripepaymentcardsIdResponse200';
import GetStripePaymentCardGetStripepaymentcardsIdResponse200Links from './model/GetStripePaymentCardGetStripepaymentcardsIdResponse200Links';
import GetStripePaymentCardGetStripepaymentcardsIdResponse404 from './model/GetStripePaymentCardGetStripepaymentcardsIdResponse404';
import GetTicketGetTicketsIdResponse200 from './model/GetTicketGetTicketsIdResponse200';
import GetTicketGetTicketsIdResponse200Links from './model/GetTicketGetTicketsIdResponse200Links';
import GetTicketGetTicketsIdResponse400 from './model/GetTicketGetTicketsIdResponse400';
import GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200 from './model/GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200';
import HotbillPostAccountsIdHotbillResponse400 from './model/HotbillPostAccountsIdHotbillResponse400';
import ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200 from './model/ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200';
import LookUpAccountGetAccountsResponse200 from './model/LookUpAccountGetAccountsResponse200';
import LookUpAccountGetAccountsResponse200BillingAddress from './model/LookUpAccountGetAccountsResponse200BillingAddress';
import LookUpAccountGetAccountsResponse200Links from './model/LookUpAccountGetAccountsResponse200Links';
import LookUpAccountGetAccountsResponse400 from './model/LookUpAccountGetAccountsResponse400';
import LookUpCustomerGetCustomersResponse200 from './model/LookUpCustomerGetCustomersResponse200';
import LookUpCustomerGetCustomersResponse200CompanyAddress from './model/LookUpCustomerGetCustomersResponse200CompanyAddress';
import LookUpCustomerGetCustomersResponse200Links from './model/LookUpCustomerGetCustomersResponse200Links';
import LookUpCustomerGetCustomersResponse400 from './model/LookUpCustomerGetCustomersResponse400';
import LookUpMeterPointGetAuMeterpointsResponse200 from './model/LookUpMeterPointGetAuMeterpointsResponse200';
import LookUpMeterPointGetAuMeterpointsResponse400 from './model/LookUpMeterPointGetAuMeterpointsResponse400';
import LookUpTicketsGetTicketsResponse200 from './model/LookUpTicketsGetTicketsResponse200';
import LookUpTicketsGetTicketsResponse400 from './model/LookUpTicketsGetTicketsResponse400';
import LookupBrokerGetBrokersResponse200 from './model/LookupBrokerGetBrokersResponse200';
import LookupBrokerGetBrokersResponse200Links from './model/LookupBrokerGetBrokersResponse200Links';
import LookupBrokerGetBrokersResponse400 from './model/LookupBrokerGetBrokersResponse400';
import LookupBrokerGetBrokersResponse404 from './model/LookupBrokerGetBrokersResponse404';
import NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody from './model/NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody';
import NewBpointDirectDebitPostAuBpointdirectdebitsResponse200 from './model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200';
import NewBpointDirectDebitPostAuBpointdirectdebitsResponse400 from './model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse400';
import NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody from './model/NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody';
import NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200 from './model/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200';
import NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse400 from './model/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse400';
import NewSepaDirectDebitPostSepadirectdebitsRequestBody from './model/NewSepaDirectDebitPostSepadirectdebitsRequestBody';
import NewSepaDirectDebitPostSepadirectdebitsResponse200 from './model/NewSepaDirectDebitPostSepadirectdebitsResponse200';
import NewSepaDirectDebitPostSepadirectdebitsResponse200Links from './model/NewSepaDirectDebitPostSepadirectdebitsResponse200Links';
import NewSepaDirectDebitPostSepadirectdebitsResponse400 from './model/NewSepaDirectDebitPostSepadirectdebitsResponse400';
import NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody from './model/NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody';
import NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200 from './model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200';
import NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400 from './model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400';
import PaymentPostAccountsIdPaymentRequestBody from './model/PaymentPostAccountsIdPaymentRequestBody';
import PaymentPostAccountsIdPaymentResponse200 from './model/PaymentPostAccountsIdPaymentResponse200';
import PaymentPostAccountsIdPaymentResponse400 from './model/PaymentPostAccountsIdPaymentResponse400';
import RenewAccountPostAccountsIdRenewalRequestBody from './model/RenewAccountPostAccountsIdRenewalRequestBody';
import RenewAccountPostAccountsIdRenewalResponse200 from './model/RenewAccountPostAccountsIdRenewalResponse200';
import RenewAccountPostAccountsIdRenewalResponse400 from './model/RenewAccountPostAccountsIdRenewalResponse400';
import RestartFailedBillRequestPostBillrequestsIdRestartfailedResponse404 from './model/RestartFailedBillRequestPostBillrequestsIdRestartfailedResponse404';
import ReversionBillPostBillsIdReversionResponse200 from './model/ReversionBillPostBillsIdReversionResponse200';
import ReversionBillPostBillsIdReversionResponse200Links from './model/ReversionBillPostBillsIdReversionResponse200Links';
import ReversionBillPostBillsIdReversionResponse404 from './model/ReversionBillPostBillsIdReversionResponse404';
import StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody from './model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody';
import StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400 from './model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400';
import StoreCardPostStripepaymentcardsRequestBody from './model/StoreCardPostStripepaymentcardsRequestBody';
import StoreCardPostStripepaymentcardsResponse400 from './model/StoreCardPostStripepaymentcardsResponse400';
import SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody from './model/SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody';
import SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400 from './model/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400';
import TariffInformationGetAccountsIdTariffinformationResponse200 from './model/TariffInformationGetAccountsIdTariffinformationResponse200';
import TariffInformationGetAccountsIdTariffinformationResponse200TIL from './model/TariffInformationGetAccountsIdTariffinformationResponse200TIL';
import TariffInformationGetAccountsIdTariffinformationResponse400 from './model/TariffInformationGetAccountsIdTariffinformationResponse400';
import UpdateAccountContactPutAccountsIdContactsContactidRequestBody from './model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody';
import UpdateAccountContactPutAccountsIdContactsContactidResponse400 from './model/UpdateAccountContactPutAccountsIdContactsContactidResponse400';
import UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody from './model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody';
import UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400 from './model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400';
import UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody from './model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody';
import UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400 from './model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400';
import UpdateCustomerConsentsPutCustomersIdConsentsRequestBody from './model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody';
import UpdateCustomerConsentsPutCustomersIdConsentsResponse400 from './model/UpdateCustomerConsentsPutCustomersIdConsentsResponse400';
import UpdateCustomerContactPutCustomersIdContactsContactidRequestBody from './model/UpdateCustomerContactPutCustomersIdContactsContactidRequestBody';
import UpdateCustomerContactPutCustomersIdContactsContactidResponse400 from './model/UpdateCustomerContactPutCustomersIdContactsContactidResponse400';
import UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody from './model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody';
import UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400 from './model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400';
import UpdateCustomerPutCustomersIdRequestBody from './model/UpdateCustomerPutCustomersIdRequestBody';
import UpdateCustomerPutCustomersIdRequestBodyPrimaryContact from './model/UpdateCustomerPutCustomersIdRequestBodyPrimaryContact';
import UpdateCustomerPutCustomersIdResponse400 from './model/UpdateCustomerPutCustomersIdResponse400';
import UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody from './model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody';
import UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences from './model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences';
import UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse400 from './model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse400';
import UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse404 from './model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse404';
import UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody from './model/UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody';
import UpdateSepaDirectDebitPutSepadirectdebitsIdResponse400 from './model/UpdateSepaDirectDebitPutSepadirectdebitsIdResponse400';
import UpdateTicketPutTicketsIdRequestBody from './model/UpdateTicketPutTicketsIdRequestBody';
import UpdateTicketPutTicketsIdResponse400 from './model/UpdateTicketPutTicketsIdResponse400';
import AccountCreditsApi from './api/AccountCreditsApi';
import AccountDebitsApi from './api/AccountDebitsApi';
import AccountsApi from './api/AccountsApi';
import AccountsAustraliaApi from './api/AccountsAustraliaApi';
import AlertsApi from './api/AlertsApi';
import BillPeriodsApi from './api/BillPeriodsApi';
import BillRequestsApi from './api/BillRequestsApi';
import BillingEntitiesApi from './api/BillingEntitiesApi';
import BillsApi from './api/BillsApi';
import CommunicationsApi from './api/CommunicationsApi';
import ConcessionsAustraliaApi from './api/ConcessionsAustraliaApi';
import ContactsApi from './api/ContactsApi';
import CreditsApi from './api/CreditsApi';
import CustomerPaymentsApi from './api/CustomerPaymentsApi';
import CustomersApi from './api/CustomersApi';
import CustomersAustraliaApi from './api/CustomersAustraliaApi';
import FinancialsApi from './api/FinancialsApi';
import MarketingCampaignSourcesApi from './api/MarketingCampaignSourcesApi';
import MeterpointsAustraliaApi from './api/MeterpointsAustraliaApi';
import NmisApi from './api/NmisApi';
import OffersApi from './api/OffersApi';
import PaymentPlansApi from './api/PaymentPlansApi';
import ProductsApi from './api/ProductsApi';
import PropertiesApi from './api/PropertiesApi';
import ProspectsApi from './api/ProspectsApi';
import QuoteFilesApi from './api/QuoteFilesApi';
import QuotesApi from './api/QuotesApi';
import TicketsApi from './api/TicketsApi';


/**
* Junifer_Native_Web_API_provided_by_the_Gentrack_Cloud_Integration_Services_How_It_WorksFor_how_to_prepare_your_application_to_call_Junifer_Native_Web_API_please_refer_to__Read_me_first_https__portal_integration_gentrack_cloud_api_index_htmlsection_Read_me_first__Sovereignty_Regions_https__portal_integration_gentrack_cloud_api_index_htmlsection_Sovereignty_Regions__Preparing_your_request_https__portal_integration_gentrack_cloud_api_index_htmlsection_Preparing_your_request__Authentication_https__portal_integration_gentrack_cloud_api_index_htmlsection_Authentication__Responses_https__portal_integration_gentrack_cloud_api_index_htmlsection_Responses_Sample_of_requesting_contact_detailsThe_following_is_an_example_of_calling_get_contact_details_API_in_UK_regionbashcurl__X_GET______https__api_uk_integration_gentrack_cloud_v1_junifer_contacts_1010_______H_Authorization_Bearer_eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9_eyJhcHBJZCI6IjA2NDYxODczLTFlOWMtNGVkMy1iZWZkLTU3NGY5ZmEwYjExYyIsIm9yZ0lkIjoiQzAwMDAiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJpYXQiOjE1MzA1ODkwODcsImV4cCI6MTUzMDU5MjY4NywiaXNzIjoiaHR0cHM6Ly9hcGkuZ2VudHJhY2suaW8vIn0_WUAFYWTTEAyQ0Bt4YXu_mxCXd_Y9ehwdZcYvcGNnyLTZH_hiJjtXWWfsx69M606pvCP6lLMT7MfK_F3E4rLO6KAlGsuz4A_7oVWQf4QNeR178GnwgmqQRw5OnLxwPbPrRa9nODngwGq8dcWejhmEYU6iw02bvYdQBHnnsc3Kpyzw7Wdv_3jnBS4TPYS20muQOgG6KxRp9hLJM7ERLoAbsULwqdPOV8eUJJhGrq1NDuH_lA83YRDZmCWEzw96tSm3hb7y88kXs_4OvamnO1m5wFPBx69VximlS4Ltr3ztqU2s3fHoj0OJLIafge9JvTgvuB6noHfs1uSRaahvstGJAAThe_sample_of_response_is_as_follows_json___id_1010___contactType_Business___title_Mr___forename_Tony___surname_Soprano___email_bigtonedidntseeanything_com___dateOfBirth_1959_08_24___phoneNumber1_44626478370___address_____address1_633_Stag_Trail_Road_____address2_North_Caldwell_____address3_New_Jersey_____postcode_NE18_0PY______links______self_https__api_uk_integration_gentrack_cloud_v1_junifer_contacts_1010_____accounts_https__api_uk_integration_gentrack_cloud_v1_junifer_contacts_1010_accounts____General_Rules_Patterns_NavigationA_links_section_is_returned_in_most_requests_This_section_contains_how_to_access_the_entity_itself_and_more_importantly_how_to_access_items_related_to_the_entity_For_example_take_the_following_responsejson__id_1__name_Mr_Joe_Bloggs__number_00000001__surname_Bloggs__links_____self_https__api_uk_integration_gentrack_cloud_v1_junifer_customers_1____accounts____________id_7______name_Invoice______number_00000001______currency_GBP______fromDt_2013_05_01______createdDttm_2013_05_08T133634_000______balance_0______billingAddress_________address1_11_Tuppy_Street____________links_________self_https__api_uk_integration_gentrack_cloud_v1_junifer_accounts_7________customer_https__api_uk_integration_gentrack_cloud_v1_junifer_customers_1________bills_https__api_uk_integration_gentrack_cloud_v1_junifer_accounts_7_bills________payments_https__api_uk_integration_gentrack_cloud_v1_junifer_accounts_7_payments____________The_self_URL_in_the_links_section_can_be_used_to_access_the_entities_listed_in_the_response_In_the_above_example_there_are_self_fields_for_the_customer_itself_and_the_accounts_under_this_customer_There_are_other_links_under_the_account_For_example_the_above_example_contains_links_for_the_following__bills__List_of_Bills_for_the_Account__payments__List_of_Payments_for_the_Account__customer__Link_back_to_the_Accounts_Customer_About_IDsThe_id_in_the_API_requests__JSON_payload_and_URL_and_responses_identifies_the_resource_within_the_API_It_has_no_business_meaning_or_purpose_therefore_shouldnt_be_exposed_to_the_end_user_in_any_way_As_the_IDs_of_the_resources_arent_sequential_or_aligned_with_any_other_resource_IDs_it_is_highly_recommended_using_the_links_section_from_the_responses_to_navigate_to_other_related_resources_Given_the_example_above_the_customer_with_ID_1_links_to_the_account_with_ID_7_The_links_section_will_populate_the_correct_IDs_in_the_URLs_so_none_of_the_assumptions_about_related_resource_IDs_need_to_be_made_by_the_implementer__Accessing_accounts_by_account_numberThroughout_this_API_anywhere_an_account_is_accessed_by_its_id__i_e__the_URL_includes__accounts_id_it_can_also_be_accessed_by_its_account_number_by_replacing__accounts_id_with_accounts_accountNumber_num_For_example_the_Create_Account_Credit_endpoint_available_at__accounts_id_accountCredits_can_also_be_accessed_at__accounts_accountNumber_num_accountCredits__Standard_data_types_in_JSONThe_standard_data_types_are_as_follows__String__Quoted_as_per_JSON_standard__Number__Unquoted_as_per_JSON_standard__Decimal__Unquoted_as_per_JSON_standard__Date__In_the_standard_ISO_format_yyyy_MM_dd__DateTime__In_the_standard_ISO_format_yyyy_MM_ddThhmmss_SSS__no_time_zone_component__Boolean__Unquoted_true_and_false_are_the_only_valid_values_Ordering_of_dataResponse_representations_are_not_ordered_unless_specified_on_a_specific_resource_Therefore_clients_should_be_implemented_to_use_the_name_of_fields_objects_as_the_key_for_accessing_data_For_example_in_JSON_objects_are_comprised_of_key_value_pairs_and_clients_should_be_implemented_to_search_on_the_key_value_rather_than_looking_for_the_nth_key_value_pair_in_the_object__Changes_of_dataResponse_representations_may_change_without_warning_in_the_following_ways___Additional_key_value_pairs_may_be_added_to_an_existing_object__Additional_sub_objects_may_be_added_to_an_existing_objectThese_changes_are_considered_non_breaking_Therefore_clients_should_be_implemented_to_consume_only_the_data_they_need_and_ignore_any_other_information__VersioningThe_API_is_usually_changed_in_each_release_of_Junifer__Navigate_to_different_versions_by_clicking_the_dropdown_menu_in_the_top_right_of_this_web_page__PaginationSome_REST_APIs_that_provide_a_list_of_results_support_pagination_Pagination_allows_API_users_to_query_potential_large_data_sets_but_only_receive_a_sub_set_of_the_results_on_the_initial_query__Subsequent_queries_can_use_the_response_of_a_prior_query_to_retrieve_the_next_or_previous_set_of_results_Pagination_functionality_and_behaviour_will_be_documented_on_each_API_that_requires_it__Pagination_on_existing_APIsExisting_APIs_may_have_pagination_support_added__In_the_future_this_pagination_will_become_mandatory_In_order_to_facilitate_API_users_planning_and_executing_upgrades_against_the_existing_APIs_pagination_is_optional_during_a_transition_period__API_users_can_enable_pagination_on_these_APIs_by_specifying_the_pagination_query_parameter__This_parameter_only_needs_to_be_present_the_value_is_not_required_or_checked__If_this_query_parameter_is_specified_then_pagination_will_be_enabled__Otherwise_the_existing_non_paginated_functionality_that_returns_all_results_will_be_used_API_users_must_plan_and_execute_upgrades_to_their_existing_applications_to_use_the_pagination_support_during_the_transition_period__At_the_end_of_the_transition_period_pagination_will_become_mandatory_on_these_APIs_For_new_APIs_that_support_pagination_it_will_be_mandatory_with_the_initial_release_of_the_API__OrderingThe_ordering_that_the_API_provides_will_be_documented_for_each_API_An_ordering_is_always_required_to_ensure_moving_between_pages_is_consistent_Some_APIs_may_define_an_ordering_on_a_value_or_values_in_the_response_where_appropriate__For_example_results_may_be_ordered_by_a_date_time_property_of_the_results_Other_APIs_may_not_define_an_explicit_ordering_but_will_guarantee_the_order_is_consistent_for_multiple_API_calls__Limiting_results_of_each_pageThe_desiredResults_query_parameter_can_be_used_to_specify_the_maximum_number_of_results_that_may_be_returned_The_APIs_treat_this_as_a_hint_so_API_callers_should_expect_to_sometimes_receive_a_different_number_of_results__APIs_will_endeavour_to_ensure_no_more_than_desiredResults_are_returned_but_for_small_values_of_desiredResults__less_than_10_this_may_not_be_possible__This_is_due_to_how_pages_of_data_are_structured_inside_the_product__The_API_will_prefer_to_return_some_data_rather_than_no_data_where_it_is_not_possible_to_return_less_than_desiredResultsIf_desiredResults_is_not_specified_the_default_maximum_value_for_the_API_will_be_used__Unless_documented_differently_this_value_is_100_If_desiredResults_is_specified_higher_than_the_default_maximum_for_the_API_a_400_error_response_will_be_received__CursorsThe_pagination_provided_by_the_APIs_is_cursor_based_In_the_JSON_response_additional_metadata_will_be_provided_when_pagination_is_present___after_cursor_points_to_the_start_of_the_results_that_have_been_returned__before_cursor_points_to_the_end_of_the_results_that_have_been_returnedThese_cursors_can_be_used_to_make_subsequent_API_calls__The_API_URL_should_be_unchanged_except_for_setting_the_before_or_after_query_parameter__If_both_parameters_are_specified_then_an_error_400_response_will_be_received_Cursor_tokens_must_only_be_used_for_pagination_purposes__API_users_should_not_infer_any_business_meaning_or_logic_from_cursor_representations__These_representations_may_change_without_warning_when_Junifer_is_upgraded_When_scrolling_forward_the_after_cursor_will_be_absent_from_the_response_when_there_are_no_more_results_after_the_current_set_When_scrolling_backwards_the_before_cursor_will_be_absent_from_the_response_when_there_are_no_more_results_before_the_current_set_In_addition_a_paginated_response_will_include_a_next_and_previous_URLs_for_convenience__Best_practicesThe_following_best_practices_must_be_adhered_to_by_users_of_the_pagination_APIs__Dont_store_cursors_or_next_and_previous_URLs_for_long__Cursors_can_quickly_become_invalid_if_items_are_added_or_deleted_to_the_data_set__If_an_invalid_cursor_is_used_then_a_404_response_will_be_returned_in_this_situation__API_users_should_design_a_policy_to_handle_this_situation__API_users_should_handle_empty_data_sets__In_limited_cases_a_pagination_query_using_a_previously_valid_cursor_may_return_no_results_ExampleThis_example_is_illustrative_only_A_small_desiredResults_parameter_has_been_used_to_make_the_example_concise__API_users_should_generally_choose_larger_values_subject_to_the_maximum_value_The_following_API_call_would_retrieve_the_first_set_of_results__Requestbashhttps__api_uk_integration_gentrack_cloud_v1_junifer_uk_meterPoints_1_payg_balancesdesiredResults3_______H_Authorization_Bearer_eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9____aahvstGJAA_Responsejson__results____________snapshotDateTime_2019_04_20T000000_000______creditBalance_109______emergencyCreditBalance_0______accumulatedDebtRegister_0______timeDebtRegister1_0______timeDebtRegister2_0______paymentDebtRegister_0______________snapshotDateTime_2019_04_19T000000_000______creditBalance_108______emergencyCreditBalance_0______accumulatedDebtRegister_0______timeDebtRegister1_0______timeDebtRegister2_0______paymentDebtRegister_0______________snapshotDateTime_2019_04_18T000000_000______creditBalance_107______emergencyCreditBalance_0______accumulatedDebtRegister_0______timeDebtRegister1_0______timeDebtRegister2_0______paymentDebtRegister_0________pagination_____cursors_______after_107________next_https__api_uk_integration_gentrack_cloud_v1_junifer_uk_meterPoints_1_payg_balancespaginationdesiredResults3after107__The_results_section_contains_the_payload_of_the_API_The_pagination_section_contains_details_needed_to_retrieve_additional_results_API_users_can_choose_to_use_the_next_URL_directly_or_use_the_after_cursor_token_to_construct_the_URL_themselves_The_API_call_to_query_the_next_set_of_results_would_be_Requestbashhttps__api_uk_integration_gentrack_cloud_v1_junifer_uk_meterPoints_1_payg_balancespaginationdesiredResults3after107_______H_Authorization_Bearer_eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9____aahvstGJAA_Responsejson__results____________snapshotDateTime_2019_04_17T000000_000______creditBalance_106______emergencyCreditBalance_0______accumulatedDebtRegister_0______timeDebtRegister1_0______timeDebtRegister2_0______paymentDebtRegister_0______________snapshotDateTime_2019_04_16T000000_000______creditBalance_105______emergencyCreditBalance_0______accumulatedDebtRegister_0______timeDebtRegister1_0______timeDebtRegister2_0______paymentDebtRegister_0______________snapshotDateTime_2019_04_15T000000_000______creditBalance_104______emergencyCreditBalance_0______accumulatedDebtRegister_0______timeDebtRegister1_0______timeDebtRegister2_0______paymentDebtRegister_0________pagination_____cursors_______before_106______after_104________previous_https__api_uk_integration_gentrack_cloud_v1_junifer_uk_meterPoints_1_payg_balancespaginationbefore106desiredResults3____next_https__api_uk_integration_gentrack_cloud_v1_junifer_uk_meterPoints_1_payg_balancespaginationdesiredResults3after104__This_time_the_response_contains_both_before_and_after_tokens_and_previous_and_next_URLs__The_before_token_or_previous_link_can_be_used_to_navigate_backwards_in_the_results_.<br>
* The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
* <p>
* An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
* <pre>
* var JuniferAuNativeApi = require('index'); // See note below*.
* var xxxSvc = new JuniferAuNativeApi.XxxApi(); // Allocate the API class we're going to use.
* var yyyModel = new JuniferAuNativeApi.Yyy(); // Construct a model instance.
* yyyModel.someProperty = 'someValue';
* ...
* var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
* ...
* </pre>
* <em>*NOTE: For a top-level AMD script, use require(['index'], function(){...})
* and put the application logic within the callback function.</em>
* </p>
* <p>
* A non-AMD browser application (discouraged) might do something like this:
* <pre>
* var xxxSvc = new JuniferAuNativeApi.XxxApi(); // Allocate the API class we're going to use.
* var yyy = new JuniferAuNativeApi.Yyy(); // Construct a model instance.
* yyyModel.someProperty = 'someValue';
* ...
* var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
* ...
* </pre>
* </p>
* @module index
* @version 1.61.1
*/
export {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient,

    /**
     * The AcceptDraftBillPostBillsIdAcceptdraftResponse404 model constructor.
     * @property {module:model/AcceptDraftBillPostBillsIdAcceptdraftResponse404}
     */
    AcceptDraftBillPostBillsIdAcceptdraftResponse404,

    /**
     * The AcceptQuotePostQuotesIdAcceptquoteResponse400 model constructor.
     * @property {module:model/AcceptQuotePostQuotesIdAcceptquoteResponse400}
     */
    AcceptQuotePostQuotesIdAcceptquoteResponse400,

    /**
     * The AcceptQuotePostQuotesIdAcceptquoteResponse404 model constructor.
     * @property {module:model/AcceptQuotePostQuotesIdAcceptquoteResponse404}
     */
    AcceptQuotePostQuotesIdAcceptquoteResponse404,

    /**
     * The AddPropertiesPostProspectsIdAddPropertiesResponse400 model constructor.
     * @property {module:model/AddPropertiesPostProspectsIdAddPropertiesResponse400}
     */
    AddPropertiesPostProspectsIdAddPropertiesResponse400,

    /**
     * The AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody model constructor.
     * @property {module:model/AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody}
     */
    AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody,

    /**
     * The AusCreateConcessionPostAuConcessionsCreateconcessionResponse200 model constructor.
     * @property {module:model/AusCreateConcessionPostAuConcessionsCreateconcessionResponse200}
     */
    AusCreateConcessionPostAuConcessionsCreateconcessionResponse200,

    /**
     * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody model constructor.
     * @property {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody}
     */
    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody,

    /**
     * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress model constructor.
     * @property {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress}
     */
    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress,

    /**
     * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts model constructor.
     * @property {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts}
     */
    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts,

    /**
     * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct model constructor.
     * @property {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct}
     */
    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct,

    /**
     * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints model constructor.
     * @property {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints}
     */
    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints,

    /**
     * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts model constructor.
     * @property {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts}
     */
    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts,

    /**
     * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress model constructor.
     * @property {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress}
     */
    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress,

    /**
     * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200 model constructor.
     * @property {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200}
     */
    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200,

    /**
     * The AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse400 model constructor.
     * @property {module:model/AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse400}
     */
    AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse400,

    /**
     * The AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody model constructor.
     * @property {module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody}
     */
    AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody,

    /**
     * The AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct model constructor.
     * @property {module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct}
     */
    AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct,

    /**
     * The AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200 model constructor.
     * @property {module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200}
     */
    AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200,

    /**
     * The AusEnrolCustomerPostAuCustomersEnrolcustomerResponse400 model constructor.
     * @property {module:model/AusEnrolCustomerPostAuCustomersEnrolcustomerResponse400}
     */
    AusEnrolCustomerPostAuCustomersEnrolcustomerResponse400,

    /**
     * The AusGetDiscountsGetAuAccountsIdDiscountsResponse200 model constructor.
     * @property {module:model/AusGetDiscountsGetAuAccountsIdDiscountsResponse200}
     */
    AusGetDiscountsGetAuAccountsIdDiscountsResponse200,

    /**
     * The AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody model constructor.
     * @property {module:model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody}
     */
    AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody,

    /**
     * The AusLifeSupportSensitiveLoadPostAuCustomersLifesupportResponse400 model constructor.
     * @property {module:model/AusLifeSupportSensitiveLoadPostAuCustomersLifesupportResponse400}
     */
    AusLifeSupportSensitiveLoadPostAuCustomersLifesupportResponse400,

    /**
     * The AusLinkDiscountPostAuAccountsIdDiscountsRequestBody model constructor.
     * @property {module:model/AusLinkDiscountPostAuAccountsIdDiscountsRequestBody}
     */
    AusLinkDiscountPostAuAccountsIdDiscountsRequestBody,

    /**
     * The AusRenewAccountPostAuAccountsIdRenewalRequestBody model constructor.
     * @property {module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBody}
     */
    AusRenewAccountPostAuAccountsIdRenewalRequestBody,

    /**
     * The AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements model constructor.
     * @property {module:model/AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements}
     */
    AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements,

    /**
     * The AusRenewAccountPostAuAccountsIdRenewalResponse200 model constructor.
     * @property {module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200}
     */
    AusRenewAccountPostAuAccountsIdRenewalResponse200,

    /**
     * The AusRenewAccountPostAuAccountsIdRenewalResponse200Results model constructor.
     * @property {module:model/AusRenewAccountPostAuAccountsIdRenewalResponse200Results}
     */
    AusRenewAccountPostAuAccountsIdRenewalResponse200Results,

    /**
     * The AusRenewAccountPostAuAccountsIdRenewalResponse400 model constructor.
     * @property {module:model/AusRenewAccountPostAuAccountsIdRenewalResponse400}
     */
    AusRenewAccountPostAuAccountsIdRenewalResponse400,

    /**
     * The AusRenewAccountPostAuAccountsIdRenewalResponse404 model constructor.
     * @property {module:model/AusRenewAccountPostAuAccountsIdRenewalResponse404}
     */
    AusRenewAccountPostAuAccountsIdRenewalResponse404,

    /**
     * The AusSiteAccessPostAuCustomersSiteaccessRequestBody model constructor.
     * @property {module:model/AusSiteAccessPostAuCustomersSiteaccessRequestBody}
     */
    AusSiteAccessPostAuCustomersSiteaccessRequestBody,

    /**
     * The AusSiteAccessPostAuCustomersSiteaccessResponse400 model constructor.
     * @property {module:model/AusSiteAccessPostAuCustomersSiteaccessResponse400}
     */
    AusSiteAccessPostAuCustomersSiteaccessResponse400,

    /**
     * The BillEmailsGetBillemailsIdResponse200 model constructor.
     * @property {module:model/BillEmailsGetBillemailsIdResponse200}
     */
    BillEmailsGetBillemailsIdResponse200,

    /**
     * The BillEmailsGetBillemailsIdResponse200Files model constructor.
     * @property {module:model/BillEmailsGetBillemailsIdResponse200Files}
     */
    BillEmailsGetBillemailsIdResponse200Files,

    /**
     * The BillEmailsGetBillemailsIdResponse200Links model constructor.
     * @property {module:model/BillEmailsGetBillemailsIdResponse200Links}
     */
    BillEmailsGetBillemailsIdResponse200Links,

    /**
     * The BillEmailsGetBillemailsIdResponse200Links1 model constructor.
     * @property {module:model/BillEmailsGetBillemailsIdResponse200Links1}
     */
    BillEmailsGetBillemailsIdResponse200Links1,

    /**
     * The BillEmailsGetBillemailsIdResponse404 model constructor.
     * @property {module:model/BillEmailsGetBillemailsIdResponse404}
     */
    BillEmailsGetBillemailsIdResponse404,

    /**
     * The CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400 model constructor.
     * @property {module:model/CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400}
     */
    CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400,

    /**
     * The CancelAccountCreditDeleteAccountcreditsIdResponse400 model constructor.
     * @property {module:model/CancelAccountCreditDeleteAccountcreditsIdResponse400}
     */
    CancelAccountCreditDeleteAccountcreditsIdResponse400,

    /**
     * The CancelAccountDebitDeleteAccountdebitsIdResponse400 model constructor.
     * @property {module:model/CancelAccountDebitDeleteAccountdebitsIdResponse400}
     */
    CancelAccountDebitDeleteAccountdebitsIdResponse400,

    /**
     * The CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400 model constructor.
     * @property {module:model/CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400}
     */
    CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400,

    /**
     * The CancelTicketPostTicketsIdCancelticketResponse400 model constructor.
     * @property {module:model/CancelTicketPostTicketsIdCancelticketResponse400}
     */
    CancelTicketPostTicketsIdCancelticketResponse400,

    /**
     * The ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody model constructor.
     * @property {module:model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody}
     */
    ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody,

    /**
     * The ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400 model constructor.
     * @property {module:model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400}
     */
    ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400,

    /**
     * The ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404 model constructor.
     * @property {module:model/ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404}
     */
    ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse404,

    /**
     * The CommunicationsEmailsGetCommunicationsemailsIdResponse200 model constructor.
     * @property {module:model/CommunicationsEmailsGetCommunicationsemailsIdResponse200}
     */
    CommunicationsEmailsGetCommunicationsemailsIdResponse200,

    /**
     * The CommunicationsEmailsGetCommunicationsemailsIdResponse200Links model constructor.
     * @property {module:model/CommunicationsEmailsGetCommunicationsemailsIdResponse200Links}
     */
    CommunicationsEmailsGetCommunicationsemailsIdResponse200Links,

    /**
     * The CommunicationsEmailsGetCommunicationsemailsIdResponse404 model constructor.
     * @property {module:model/CommunicationsEmailsGetCommunicationsemailsIdResponse404}
     */
    CommunicationsEmailsGetCommunicationsemailsIdResponse404,

    /**
     * The ContactsGetAccountsIdContactsResponse200 model constructor.
     * @property {module:model/ContactsGetAccountsIdContactsResponse200}
     */
    ContactsGetAccountsIdContactsResponse200,

    /**
     * The ContactsGetAccountsIdContactsResponse200Address model constructor.
     * @property {module:model/ContactsGetAccountsIdContactsResponse200Address}
     */
    ContactsGetAccountsIdContactsResponse200Address,

    /**
     * The ContactsGetAccountsIdContactsResponse200Links model constructor.
     * @property {module:model/ContactsGetAccountsIdContactsResponse200Links}
     */
    ContactsGetAccountsIdContactsResponse200Links,

    /**
     * The ContactsGetAccountsIdContactsResponse400 model constructor.
     * @property {module:model/ContactsGetAccountsIdContactsResponse400}
     */
    ContactsGetAccountsIdContactsResponse400,

    /**
     * The ContactsGetCustomersIdContactsResponse200 model constructor.
     * @property {module:model/ContactsGetCustomersIdContactsResponse200}
     */
    ContactsGetCustomersIdContactsResponse200,

    /**
     * The ContactsGetCustomersIdContactsResponse400 model constructor.
     * @property {module:model/ContactsGetCustomersIdContactsResponse400}
     */
    ContactsGetCustomersIdContactsResponse400,

    /**
     * The CreateAccountCreditPostAccountsIdAccountcreditsRequestBody model constructor.
     * @property {module:model/CreateAccountCreditPostAccountsIdAccountcreditsRequestBody}
     */
    CreateAccountCreditPostAccountsIdAccountcreditsRequestBody,

    /**
     * The CreateAccountCreditPostAccountsIdAccountcreditsResponse400 model constructor.
     * @property {module:model/CreateAccountCreditPostAccountsIdAccountcreditsResponse400}
     */
    CreateAccountCreditPostAccountsIdAccountcreditsResponse400,

    /**
     * The CreateAccountCreditPostAccountsIdAccountcreditsResponse404 model constructor.
     * @property {module:model/CreateAccountCreditPostAccountsIdAccountcreditsResponse404}
     */
    CreateAccountCreditPostAccountsIdAccountcreditsResponse404,

    /**
     * The CreateAccountDebitPostAccountsIdAccountdebitsRequestBody model constructor.
     * @property {module:model/CreateAccountDebitPostAccountsIdAccountdebitsRequestBody}
     */
    CreateAccountDebitPostAccountsIdAccountdebitsRequestBody,

    /**
     * The CreateAccountDebitPostAccountsIdAccountdebitsResponse400 model constructor.
     * @property {module:model/CreateAccountDebitPostAccountsIdAccountdebitsResponse400}
     */
    CreateAccountDebitPostAccountsIdAccountdebitsResponse400,

    /**
     * The CreateAccountDebitPostAccountsIdAccountdebitsResponse404 model constructor.
     * @property {module:model/CreateAccountDebitPostAccountsIdAccountdebitsResponse404}
     */
    CreateAccountDebitPostAccountsIdAccountdebitsResponse404,

    /**
     * The CreateAccountNotePostAccountsIdNoteRequestBody model constructor.
     * @property {module:model/CreateAccountNotePostAccountsIdNoteRequestBody}
     */
    CreateAccountNotePostAccountsIdNoteRequestBody,

    /**
     * The CreateAccountNotePostAccountsIdNoteResponse200 model constructor.
     * @property {module:model/CreateAccountNotePostAccountsIdNoteResponse200}
     */
    CreateAccountNotePostAccountsIdNoteResponse200,

    /**
     * The CreateAccountNotePostAccountsIdNoteResponse400 model constructor.
     * @property {module:model/CreateAccountNotePostAccountsIdNoteResponse400}
     */
    CreateAccountNotePostAccountsIdNoteResponse400,

    /**
     * The CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody model constructor.
     * @property {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody}
     */
    CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody,

    /**
     * The CreateAccountRepaymentPostAccountsIdRepaymentsResponse200 model constructor.
     * @property {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200}
     */
    CreateAccountRepaymentPostAccountsIdRepaymentsResponse200,

    /**
     * The CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links model constructor.
     * @property {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links}
     */
    CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links,

    /**
     * The CreateAccountRepaymentPostAccountsIdRepaymentsResponse400 model constructor.
     * @property {module:model/CreateAccountRepaymentPostAccountsIdRepaymentsResponse400}
     */
    CreateAccountRepaymentPostAccountsIdRepaymentsResponse400,

    /**
     * The CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody model constructor.
     * @property {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody}
     */
    CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody,

    /**
     * The CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200 model constructor.
     * @property {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200}
     */
    CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200,

    /**
     * The CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400 model constructor.
     * @property {module:model/CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400}
     */
    CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400,

    /**
     * The CreateAccountTicketPostAccountsIdTicketsRequestBody model constructor.
     * @property {module:model/CreateAccountTicketPostAccountsIdTicketsRequestBody}
     */
    CreateAccountTicketPostAccountsIdTicketsRequestBody,

    /**
     * The CreateAccountTicketPostAccountsIdTicketsResponse200 model constructor.
     * @property {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200}
     */
    CreateAccountTicketPostAccountsIdTicketsResponse200,

    /**
     * The CreateAccountTicketPostAccountsIdTicketsResponse200Links model constructor.
     * @property {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200Links}
     */
    CreateAccountTicketPostAccountsIdTicketsResponse200Links,

    /**
     * The CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities model constructor.
     * @property {module:model/CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities}
     */
    CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities,

    /**
     * The CreateAccountTicketPostAccountsIdTicketsResponse400 model constructor.
     * @property {module:model/CreateAccountTicketPostAccountsIdTicketsResponse400}
     */
    CreateAccountTicketPostAccountsIdTicketsResponse400,

    /**
     * The CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody model constructor.
     * @property {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody}
     */
    CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody,

    /**
     * The CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200 model constructor.
     * @property {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200}
     */
    CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200,

    /**
     * The CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400 model constructor.
     * @property {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400}
     */
    CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400,

    /**
     * The CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404 model constructor.
     * @property {module:model/CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404}
     */
    CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404,

    /**
     * The CreatePropertyPostPropertysRequestBody model constructor.
     * @property {module:model/CreatePropertyPostPropertysRequestBody}
     */
    CreatePropertyPostPropertysRequestBody,

    /**
     * The CreatePropertyPostPropertysResponse200 model constructor.
     * @property {module:model/CreatePropertyPostPropertysResponse200}
     */
    CreatePropertyPostPropertysResponse200,

    /**
     * The CreatePropertyPostPropertysResponse400 model constructor.
     * @property {module:model/CreatePropertyPostPropertysResponse400}
     */
    CreatePropertyPostPropertysResponse400,

    /**
     * The CreateProspectPostProspectsRequestBody model constructor.
     * @property {module:model/CreateProspectPostProspectsRequestBody}
     */
    CreateProspectPostProspectsRequestBody,

    /**
     * The CreateProspectPostProspectsRequestBodyCustomer model constructor.
     * @property {module:model/CreateProspectPostProspectsRequestBodyCustomer}
     */
    CreateProspectPostProspectsRequestBodyCustomer,

    /**
     * The CreateProspectPostProspectsRequestBodyCustomerCompanyAddress model constructor.
     * @property {module:model/CreateProspectPostProspectsRequestBodyCustomerCompanyAddress}
     */
    CreateProspectPostProspectsRequestBodyCustomerCompanyAddress,

    /**
     * The CreateProspectPostProspectsRequestBodyCustomerPrimaryContact model constructor.
     * @property {module:model/CreateProspectPostProspectsRequestBodyCustomerPrimaryContact}
     */
    CreateProspectPostProspectsRequestBodyCustomerPrimaryContact,

    /**
     * The CreateProspectPostProspectsResponse200 model constructor.
     * @property {module:model/CreateProspectPostProspectsResponse200}
     */
    CreateProspectPostProspectsResponse200,

    /**
     * The CreateProspectPostProspectsResponse400 model constructor.
     * @property {module:model/CreateProspectPostProspectsResponse400}
     */
    CreateProspectPostProspectsResponse400,

    /**
     * The CreateProspectPutCustomersIdCreateprospectRequestBody model constructor.
     * @property {module:model/CreateProspectPutCustomersIdCreateprospectRequestBody}
     */
    CreateProspectPutCustomersIdCreateprospectRequestBody,

    /**
     * The CreateProspectPutCustomersIdCreateprospectResponse200 model constructor.
     * @property {module:model/CreateProspectPutCustomersIdCreateprospectResponse200}
     */
    CreateProspectPutCustomersIdCreateprospectResponse200,

    /**
     * The CreateProspectPutCustomersIdCreateprospectResponse400 model constructor.
     * @property {module:model/CreateProspectPutCustomersIdCreateprospectResponse400}
     */
    CreateProspectPutCustomersIdCreateprospectResponse400,

    /**
     * The CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200 model constructor.
     * @property {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200}
     */
    CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200,

    /**
     * The CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse400 model constructor.
     * @property {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse400}
     */
    CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse400,

    /**
     * The CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401 model constructor.
     * @property {module:model/CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401}
     */
    CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401,

    /**
     * The EmailPostCommunicationsEmailRequestBody model constructor.
     * @property {module:model/EmailPostCommunicationsEmailRequestBody}
     */
    EmailPostCommunicationsEmailRequestBody,

    /**
     * The EmailPostCommunicationsEmailResponse400 model constructor.
     * @property {module:model/EmailPostCommunicationsEmailResponse400}
     */
    EmailPostCommunicationsEmailResponse400,

    /**
     * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody model constructor.
     * @property {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody}
     */
    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody,

    /**
     * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo model constructor.
     * @property {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo}
     */
    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo,

    /**
     * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate model constructor.
     * @property {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate}
     */
    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate,

    /**
     * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct model constructor.
     * @property {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct}
     */
    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct,

    /**
     * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct model constructor.
     * @property {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct}
     */
    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct,

    /**
     * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns model constructor.
     * @property {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns}
     */
    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns,

    /**
     * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress model constructor.
     * @property {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress}
     */
    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress,

    /**
     * The EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200 model constructor.
     * @property {module:model/EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200}
     */
    EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200,

    /**
     * The EnrolCustomerPostCustomersEnrolcustomerRequestBody model constructor.
     * @property {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBody}
     */
    EnrolCustomerPostCustomersEnrolcustomerRequestBody,

    /**
     * The EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress model constructor.
     * @property {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress}
     */
    EnrolCustomerPostCustomersEnrolcustomerRequestBodyAddress,

    /**
     * The EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts model constructor.
     * @property {module:model/EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts}
     */
    EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts,

    /**
     * The EnrolCustomerPostCustomersEnrolcustomerResponse200 model constructor.
     * @property {module:model/EnrolCustomerPostCustomersEnrolcustomerResponse200}
     */
    EnrolCustomerPostCustomersEnrolcustomerResponse200,

    /**
     * The EnrolCustomerPostCustomersEnrolcustomerResponse400 model constructor.
     * @property {module:model/EnrolCustomerPostCustomersEnrolcustomerResponse400}
     */
    EnrolCustomerPostCustomersEnrolcustomerResponse400,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200 model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links,

    /**
     * The EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400 model constructor.
     * @property {module:model/EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400}
     */
    EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse400,

    /**
     * The GetAccountAgreementsGetAccountsIdAgreementsResponse200 model constructor.
     * @property {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200}
     */
    GetAccountAgreementsGetAccountsIdAgreementsResponse200,

    /**
     * The GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType model constructor.
     * @property {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType}
     */
    GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType,

    /**
     * The GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets model constructor.
     * @property {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets}
     */
    GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets,

    /**
     * The GetAccountAgreementsGetAccountsIdAgreementsResponse200Links model constructor.
     * @property {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links}
     */
    GetAccountAgreementsGetAccountsIdAgreementsResponse200Links,

    /**
     * The GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1 model constructor.
     * @property {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1}
     */
    GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1,

    /**
     * The GetAccountAgreementsGetAccountsIdAgreementsResponse200Products model constructor.
     * @property {module:model/GetAccountAgreementsGetAccountsIdAgreementsResponse200Products}
     */
    GetAccountAgreementsGetAccountsIdAgreementsResponse200Products,

    /**
     * The GetAccountBillRequestsGetAccountsIdBillrequestsResponse200 model constructor.
     * @property {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200}
     */
    GetAccountBillRequestsGetAccountsIdBillrequestsResponse200,

    /**
     * The GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links model constructor.
     * @property {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links}
     */
    GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links,

    /**
     * The GetAccountBillRequestsGetAccountsIdBillrequestsResponse404 model constructor.
     * @property {module:model/GetAccountBillRequestsGetAccountsIdBillrequestsResponse404}
     */
    GetAccountBillRequestsGetAccountsIdBillrequestsResponse404,

    /**
     * The GetAccountBillsGetAccountsIdBillsResponse200 model constructor.
     * @property {module:model/GetAccountBillsGetAccountsIdBillsResponse200}
     */
    GetAccountBillsGetAccountsIdBillsResponse200,

    /**
     * The GetAccountBillsGetAccountsIdBillsResponse200Links model constructor.
     * @property {module:model/GetAccountBillsGetAccountsIdBillsResponse200Links}
     */
    GetAccountBillsGetAccountsIdBillsResponse200Links,

    /**
     * The GetAccountByNumberGetAccountsAccountnumberNumResponse400 model constructor.
     * @property {module:model/GetAccountByNumberGetAccountsAccountnumberNumResponse400}
     */
    GetAccountByNumberGetAccountsAccountnumberNumResponse400,

    /**
     * The GetAccountByNumberGetAccountsAccountnumberNumResponse404 model constructor.
     * @property {module:model/GetAccountByNumberGetAccountsAccountnumberNumResponse404}
     */
    GetAccountByNumberGetAccountsAccountnumberNumResponse404,

    /**
     * The GetAccountCommunicationsGetAccountsIdCommunicationsResponse200 model constructor.
     * @property {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200}
     */
    GetAccountCommunicationsGetAccountsIdCommunicationsResponse200,

    /**
     * The GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files model constructor.
     * @property {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files}
     */
    GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files,

    /**
     * The GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links model constructor.
     * @property {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links}
     */
    GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links,

    /**
     * The GetAccountCommunicationsGetAccountsIdCommunicationsResponse400 model constructor.
     * @property {module:model/GetAccountCommunicationsGetAccountsIdCommunicationsResponse400}
     */
    GetAccountCommunicationsGetAccountsIdCommunicationsResponse400,

    /**
     * The GetAccountCreditGetAccountcreditsIdResponse200 model constructor.
     * @property {module:model/GetAccountCreditGetAccountcreditsIdResponse200}
     */
    GetAccountCreditGetAccountcreditsIdResponse200,

    /**
     * The GetAccountCreditNotesGetAccountsIdCreditsResponse200 model constructor.
     * @property {module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200}
     */
    GetAccountCreditNotesGetAccountsIdCreditsResponse200,

    /**
     * The GetAccountCreditNotesGetAccountsIdCreditsResponse200Links model constructor.
     * @property {module:model/GetAccountCreditNotesGetAccountsIdCreditsResponse200Links}
     */
    GetAccountCreditNotesGetAccountsIdCreditsResponse200Links,

    /**
     * The GetAccountCreditsGetAccountsIdAccountcreditsResponse200 model constructor.
     * @property {module:model/GetAccountCreditsGetAccountsIdAccountcreditsResponse200}
     */
    GetAccountCreditsGetAccountsIdAccountcreditsResponse200,

    /**
     * The GetAccountDebitGetAccountdebitsIdResponse200 model constructor.
     * @property {module:model/GetAccountDebitGetAccountdebitsIdResponse200}
     */
    GetAccountDebitGetAccountdebitsIdResponse200,

    /**
     * The GetAccountDebitsGetAccountsIdAccountdebitsResponse200 model constructor.
     * @property {module:model/GetAccountDebitsGetAccountsIdAccountdebitsResponse200}
     */
    GetAccountDebitsGetAccountsIdAccountdebitsResponse200,

    /**
     * The GetAccountGetAccountsIdResponse200 model constructor.
     * @property {module:model/GetAccountGetAccountsIdResponse200}
     */
    GetAccountGetAccountsIdResponse200,

    /**
     * The GetAccountGetAccountsIdResponse200BillingAddress model constructor.
     * @property {module:model/GetAccountGetAccountsIdResponse200BillingAddress}
     */
    GetAccountGetAccountsIdResponse200BillingAddress,

    /**
     * The GetAccountGetAccountsIdResponse200Links model constructor.
     * @property {module:model/GetAccountGetAccountsIdResponse200Links}
     */
    GetAccountGetAccountsIdResponse200Links,

    /**
     * The GetAccountGetAccountsIdResponse404 model constructor.
     * @property {module:model/GetAccountGetAccountsIdResponse404}
     */
    GetAccountGetAccountsIdResponse404,

    /**
     * The GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200 model constructor.
     * @property {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200}
     */
    GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200,

    /**
     * The GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links model constructor.
     * @property {module:model/GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links}
     */
    GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links,

    /**
     * The GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200 model constructor.
     * @property {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200}
     */
    GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200,

    /**
     * The GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links model constructor.
     * @property {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links}
     */
    GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links,

    /**
     * The GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions model constructor.
     * @property {module:model/GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions}
     */
    GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions,

    /**
     * The GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200 model constructor.
     * @property {module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200}
     */
    GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200,

    /**
     * The GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200DebtRecoveryDetails model constructor.
     * @property {module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200DebtRecoveryDetails}
     */
    GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200DebtRecoveryDetails,

    /**
     * The GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links model constructor.
     * @property {module:model/GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links}
     */
    GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links,

    /**
     * The GetAccountPaymentsGetAccountsIdPaymentsResponse200 model constructor.
     * @property {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200}
     */
    GetAccountPaymentsGetAccountsIdPaymentsResponse200,

    /**
     * The GetAccountPaymentsGetAccountsIdPaymentsResponse200Links model constructor.
     * @property {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse200Links}
     */
    GetAccountPaymentsGetAccountsIdPaymentsResponse200Links,

    /**
     * The GetAccountPaymentsGetAccountsIdPaymentsResponse400 model constructor.
     * @property {module:model/GetAccountPaymentsGetAccountsIdPaymentsResponse400}
     */
    GetAccountPaymentsGetAccountsIdPaymentsResponse400,

    /**
     * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200 model constructor.
     * @property {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200}
     */
    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200,

    /**
     * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints model constructor.
     * @property {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints}
     */
    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints,

    /**
     * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType model constructor.
     * @property {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType}
     */
    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType,

    /**
     * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress model constructor.
     * @property {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress}
     */
    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress,

    /**
     * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates model constructor.
     * @property {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates}
     */
    GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates,

    /**
     * The GetAccountProductDetailsGetAccountsIdProductdetailsResponse400 model constructor.
     * @property {module:model/GetAccountProductDetailsGetAccountsIdProductdetailsResponse400}
     */
    GetAccountProductDetailsGetAccountsIdProductdetailsResponse400,

    /**
     * The GetAccountPropertysGetAccountsIdPropertysResponse200 model constructor.
     * @property {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200}
     */
    GetAccountPropertysGetAccountsIdPropertysResponse200,

    /**
     * The GetAccountPropertysGetAccountsIdPropertysResponse200Address model constructor.
     * @property {module:model/GetAccountPropertysGetAccountsIdPropertysResponse200Address}
     */
    GetAccountPropertysGetAccountsIdPropertysResponse200Address,

    /**
     * The GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200 model constructor.
     * @property {module:model/GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200}
     */
    GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200,

    /**
     * The GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200 model constructor.
     * @property {module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200}
     */
    GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200,

    /**
     * The GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links model constructor.
     * @property {module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links}
     */
    GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links,

    /**
     * The GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400 model constructor.
     * @property {module:model/GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400}
     */
    GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400,

    /**
     * The GetAccountTicketsGetAccountsIdTicketsResponse200 model constructor.
     * @property {module:model/GetAccountTicketsGetAccountsIdTicketsResponse200}
     */
    GetAccountTicketsGetAccountsIdTicketsResponse200,

    /**
     * The GetAccountTicketsGetAccountsIdTicketsResponse400 model constructor.
     * @property {module:model/GetAccountTicketsGetAccountsIdTicketsResponse400}
     */
    GetAccountTicketsGetAccountsIdTicketsResponse400,

    /**
     * The GetAccountTransactionsGetAccountsIdTransactionsResponse200 model constructor.
     * @property {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200}
     */
    GetAccountTransactionsGetAccountsIdTransactionsResponse200,

    /**
     * The GetAccountTransactionsGetAccountsIdTransactionsResponse200Links model constructor.
     * @property {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse200Links}
     */
    GetAccountTransactionsGetAccountsIdTransactionsResponse200Links,

    /**
     * The GetAccountTransactionsGetAccountsIdTransactionsResponse400 model constructor.
     * @property {module:model/GetAccountTransactionsGetAccountsIdTransactionsResponse400}
     */
    GetAccountTransactionsGetAccountsIdTransactionsResponse400,

    /**
     * The GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200 model constructor.
     * @property {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200}
     */
    GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200,

    /**
     * The GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links model constructor.
     * @property {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links}
     */
    GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links,

    /**
     * The GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400 model constructor.
     * @property {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400}
     */
    GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400,

    /**
     * The GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404 model constructor.
     * @property {module:model/GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404}
     */
    GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse404,

    /**
     * The GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200 model constructor.
     * @property {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200}
     */
    GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200,

    /**
     * The GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months model constructor.
     * @property {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months}
     */
    GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months,

    /**
     * The GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400 model constructor.
     * @property {module:model/GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400}
     */
    GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400,

    /**
     * The GetAlertGetAlertsIdResponse200 model constructor.
     * @property {module:model/GetAlertGetAlertsIdResponse200}
     */
    GetAlertGetAlertsIdResponse200,

    /**
     * The GetAlertGetAlertsIdResponse200Links model constructor.
     * @property {module:model/GetAlertGetAlertsIdResponse200Links}
     */
    GetAlertGetAlertsIdResponse200Links,

    /**
     * The GetAlertGetAlertsIdResponse404 model constructor.
     * @property {module:model/GetAlertGetAlertsIdResponse404}
     */
    GetAlertGetAlertsIdResponse404,

    /**
     * The GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200 model constructor.
     * @property {module:model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200}
     */
    GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200,

    /**
     * The GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400 model constructor.
     * @property {module:model/GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400}
     */
    GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400,

    /**
     * The GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200 model constructor.
     * @property {module:model/GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200}
     */
    GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200,

    /**
     * The GetBillFileGetBillfilesIdResponse200 model constructor.
     * @property {module:model/GetBillFileGetBillfilesIdResponse200}
     */
    GetBillFileGetBillfilesIdResponse200,

    /**
     * The GetBillFileGetBillfilesIdResponse200Links model constructor.
     * @property {module:model/GetBillFileGetBillfilesIdResponse200Links}
     */
    GetBillFileGetBillfilesIdResponse200Links,

    /**
     * The GetBillFileGetBillfilesIdResponse404 model constructor.
     * @property {module:model/GetBillFileGetBillfilesIdResponse404}
     */
    GetBillFileGetBillfilesIdResponse404,

    /**
     * The GetBillFileImageGetBillfilesIdImageResponse200 model constructor.
     * @property {module:model/GetBillFileImageGetBillfilesIdImageResponse200}
     */
    GetBillFileImageGetBillfilesIdImageResponse200,

    /**
     * The GetBillFileImageGetBillfilesIdImageResponse410 model constructor.
     * @property {module:model/GetBillFileImageGetBillfilesIdImageResponse410}
     */
    GetBillFileImageGetBillfilesIdImageResponse410,

    /**
     * The GetBillGetBillsIdResponse200 model constructor.
     * @property {module:model/GetBillGetBillsIdResponse200}
     */
    GetBillGetBillsIdResponse200,

    /**
     * The GetBillGetBillsIdResponse200BillFiles model constructor.
     * @property {module:model/GetBillGetBillsIdResponse200BillFiles}
     */
    GetBillGetBillsIdResponse200BillFiles,

    /**
     * The GetBillGetBillsIdResponse200Links model constructor.
     * @property {module:model/GetBillGetBillsIdResponse200Links}
     */
    GetBillGetBillsIdResponse200Links,

    /**
     * The GetBillGetBillsIdResponse200Links1 model constructor.
     * @property {module:model/GetBillGetBillsIdResponse200Links1}
     */
    GetBillGetBillsIdResponse200Links1,

    /**
     * The GetBillGetBillsIdResponse404 model constructor.
     * @property {module:model/GetBillGetBillsIdResponse404}
     */
    GetBillGetBillsIdResponse404,

    /**
     * The GetBillPeriodGetBillperiodsIdResponse200 model constructor.
     * @property {module:model/GetBillPeriodGetBillperiodsIdResponse200}
     */
    GetBillPeriodGetBillperiodsIdResponse200,

    /**
     * The GetBillPeriodGetBillperiodsIdResponse404 model constructor.
     * @property {module:model/GetBillPeriodGetBillperiodsIdResponse404}
     */
    GetBillPeriodGetBillperiodsIdResponse404,

    /**
     * The GetBillRequestGetBillrequestsIdResponse200 model constructor.
     * @property {module:model/GetBillRequestGetBillrequestsIdResponse200}
     */
    GetBillRequestGetBillrequestsIdResponse200,

    /**
     * The GetBillRequestGetBillrequestsIdResponse200Links model constructor.
     * @property {module:model/GetBillRequestGetBillrequestsIdResponse200Links}
     */
    GetBillRequestGetBillrequestsIdResponse200Links,

    /**
     * The GetBillRequestGetBillrequestsIdResponse404 model constructor.
     * @property {module:model/GetBillRequestGetBillrequestsIdResponse404}
     */
    GetBillRequestGetBillrequestsIdResponse404,

    /**
     * The GetBillingEntityGetBillingentitiesIdResponse200 model constructor.
     * @property {module:model/GetBillingEntityGetBillingentitiesIdResponse200}
     */
    GetBillingEntityGetBillingentitiesIdResponse200,

    /**
     * The GetBillingEntityGetBillingentitiesIdResponse200Links model constructor.
     * @property {module:model/GetBillingEntityGetBillingentitiesIdResponse200Links}
     */
    GetBillingEntityGetBillingentitiesIdResponse200Links,

    /**
     * The GetBillingEntityGetBillingentitiesIdResponse404 model constructor.
     * @property {module:model/GetBillingEntityGetBillingentitiesIdResponse404}
     */
    GetBillingEntityGetBillingentitiesIdResponse404,

    /**
     * The GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200 model constructor.
     * @property {module:model/GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200}
     */
    GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200,

    /**
     * The GetBrokerGetBrokersIdResponse200 model constructor.
     * @property {module:model/GetBrokerGetBrokersIdResponse200}
     */
    GetBrokerGetBrokersIdResponse200,

    /**
     * The GetBrokerGetBrokersIdResponse200Links model constructor.
     * @property {module:model/GetBrokerGetBrokersIdResponse200Links}
     */
    GetBrokerGetBrokersIdResponse200Links,

    /**
     * The GetBrokerGetBrokersIdResponse404 model constructor.
     * @property {module:model/GetBrokerGetBrokersIdResponse404}
     */
    GetBrokerGetBrokersIdResponse404,

    /**
     * The GetBrokerLinkageGetBrokerlinkagesIdResponse200 model constructor.
     * @property {module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse200}
     */
    GetBrokerLinkageGetBrokerlinkagesIdResponse200,

    /**
     * The GetBrokerLinkageGetBrokerlinkagesIdResponse200Links model constructor.
     * @property {module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse200Links}
     */
    GetBrokerLinkageGetBrokerlinkagesIdResponse200Links,

    /**
     * The GetBrokerLinkageGetBrokerlinkagesIdResponse404 model constructor.
     * @property {module:model/GetBrokerLinkageGetBrokerlinkagesIdResponse404}
     */
    GetBrokerLinkageGetBrokerlinkagesIdResponse404,

    /**
     * The GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200 model constructor.
     * @property {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200}
     */
    GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200,

    /**
     * The GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links model constructor.
     * @property {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links}
     */
    GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links,

    /**
     * The GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse404 model constructor.
     * @property {module:model/GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse404}
     */
    GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse404,

    /**
     * The GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200 model constructor.
     * @property {module:model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200}
     */
    GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200,

    /**
     * The GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404 model constructor.
     * @property {module:model/GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404}
     */
    GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404,

    /**
     * The GetCommunicationsFileGetCommunicationsfilesIdResponse200 model constructor.
     * @property {module:model/GetCommunicationsFileGetCommunicationsfilesIdResponse200}
     */
    GetCommunicationsFileGetCommunicationsfilesIdResponse200,

    /**
     * The GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200 model constructor.
     * @property {module:model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200}
     */
    GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200,

    /**
     * The GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse410 model constructor.
     * @property {module:model/GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse410}
     */
    GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse410,

    /**
     * The GetCommunicationsGetCommunicationsIdResponse200 model constructor.
     * @property {module:model/GetCommunicationsGetCommunicationsIdResponse200}
     */
    GetCommunicationsGetCommunicationsIdResponse200,

    /**
     * The GetCommunicationsGetCommunicationsIdResponse200Links model constructor.
     * @property {module:model/GetCommunicationsGetCommunicationsIdResponse200Links}
     */
    GetCommunicationsGetCommunicationsIdResponse200Links,

    /**
     * The GetCommunicationsGetCommunicationsIdResponse404 model constructor.
     * @property {module:model/GetCommunicationsGetCommunicationsIdResponse404}
     */
    GetCommunicationsGetCommunicationsIdResponse404,

    /**
     * The GetContactGetContactsIdResponse200 model constructor.
     * @property {module:model/GetContactGetContactsIdResponse200}
     */
    GetContactGetContactsIdResponse200,

    /**
     * The GetContactGetContactsIdResponse200Address model constructor.
     * @property {module:model/GetContactGetContactsIdResponse200Address}
     */
    GetContactGetContactsIdResponse200Address,

    /**
     * The GetContactGetContactsIdResponse400 model constructor.
     * @property {module:model/GetContactGetContactsIdResponse400}
     */
    GetContactGetContactsIdResponse400,

    /**
     * The GetContactsAccountsGetContactsIdAccountsResponse200 model constructor.
     * @property {module:model/GetContactsAccountsGetContactsIdAccountsResponse200}
     */
    GetContactsAccountsGetContactsIdAccountsResponse200,

    /**
     * The GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress model constructor.
     * @property {module:model/GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress}
     */
    GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress,

    /**
     * The GetContactsAccountsGetContactsIdAccountsResponse200Links model constructor.
     * @property {module:model/GetContactsAccountsGetContactsIdAccountsResponse200Links}
     */
    GetContactsAccountsGetContactsIdAccountsResponse200Links,

    /**
     * The GetContactsAccountsGetContactsIdAccountsResponse400 model constructor.
     * @property {module:model/GetContactsAccountsGetContactsIdAccountsResponse400}
     */
    GetContactsAccountsGetContactsIdAccountsResponse400,

    /**
     * The GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200 model constructor.
     * @property {module:model/GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200}
     */
    GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200,

    /**
     * The GetCreditFileGetCreditfilesIdResponse200 model constructor.
     * @property {module:model/GetCreditFileGetCreditfilesIdResponse200}
     */
    GetCreditFileGetCreditfilesIdResponse200,

    /**
     * The GetCreditFileGetCreditfilesIdResponse200Links model constructor.
     * @property {module:model/GetCreditFileGetCreditfilesIdResponse200Links}
     */
    GetCreditFileGetCreditfilesIdResponse200Links,

    /**
     * The GetCreditFileGetCreditfilesIdResponse404 model constructor.
     * @property {module:model/GetCreditFileGetCreditfilesIdResponse404}
     */
    GetCreditFileGetCreditfilesIdResponse404,

    /**
     * The GetCreditFileImageGetCreditfilesIdImageResponse200 model constructor.
     * @property {module:model/GetCreditFileImageGetCreditfilesIdImageResponse200}
     */
    GetCreditFileImageGetCreditfilesIdImageResponse200,

    /**
     * The GetCreditFileImageGetCreditfilesIdImageResponse410 model constructor.
     * @property {module:model/GetCreditFileImageGetCreditfilesIdImageResponse410}
     */
    GetCreditFileImageGetCreditfilesIdImageResponse410,

    /**
     * The GetCreditGetCreditsIdResponse200 model constructor.
     * @property {module:model/GetCreditGetCreditsIdResponse200}
     */
    GetCreditGetCreditsIdResponse200,

    /**
     * The GetCreditGetCreditsIdResponse200CreditFiles model constructor.
     * @property {module:model/GetCreditGetCreditsIdResponse200CreditFiles}
     */
    GetCreditGetCreditsIdResponse200CreditFiles,

    /**
     * The GetCreditGetCreditsIdResponse200Links model constructor.
     * @property {module:model/GetCreditGetCreditsIdResponse200Links}
     */
    GetCreditGetCreditsIdResponse200Links,

    /**
     * The GetCreditGetCreditsIdResponse200Links1 model constructor.
     * @property {module:model/GetCreditGetCreditsIdResponse200Links1}
     */
    GetCreditGetCreditsIdResponse200Links1,

    /**
     * The GetCreditGetCreditsIdResponse404 model constructor.
     * @property {module:model/GetCreditGetCreditsIdResponse404}
     */
    GetCreditGetCreditsIdResponse404,

    /**
     * The GetCustomerAccountsGetCustomersIdAccountsResponse200 model constructor.
     * @property {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200}
     */
    GetCustomerAccountsGetCustomersIdAccountsResponse200,

    /**
     * The GetCustomerAccountsGetCustomersIdAccountsResponse200Links model constructor.
     * @property {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse200Links}
     */
    GetCustomerAccountsGetCustomersIdAccountsResponse200Links,

    /**
     * The GetCustomerAccountsGetCustomersIdAccountsResponse400 model constructor.
     * @property {module:model/GetCustomerAccountsGetCustomersIdAccountsResponse400}
     */
    GetCustomerAccountsGetCustomersIdAccountsResponse400,

    /**
     * The GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200 model constructor.
     * @property {module:model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200}
     */
    GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200,

    /**
     * The GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400 model constructor.
     * @property {module:model/GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400}
     */
    GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse400,

    /**
     * The GetCustomerConsentsGetCustomersIdConsentsResponse200 model constructor.
     * @property {module:model/GetCustomerConsentsGetCustomersIdConsentsResponse200}
     */
    GetCustomerConsentsGetCustomersIdConsentsResponse200,

    /**
     * The GetCustomerConsentsGetCustomersIdConsentsResponse400 model constructor.
     * @property {module:model/GetCustomerConsentsGetCustomersIdConsentsResponse400}
     */
    GetCustomerConsentsGetCustomersIdConsentsResponse400,

    /**
     * The GetCustomerGetCustomersIdResponse200 model constructor.
     * @property {module:model/GetCustomerGetCustomersIdResponse200}
     */
    GetCustomerGetCustomersIdResponse200,

    /**
     * The GetCustomerGetCustomersIdResponse200CompanyAddress model constructor.
     * @property {module:model/GetCustomerGetCustomersIdResponse200CompanyAddress}
     */
    GetCustomerGetCustomersIdResponse200CompanyAddress,

    /**
     * The GetCustomerGetCustomersIdResponse200Links model constructor.
     * @property {module:model/GetCustomerGetCustomersIdResponse200Links}
     */
    GetCustomerGetCustomersIdResponse200Links,

    /**
     * The GetCustomerGetCustomersIdResponse200PrimaryContact model constructor.
     * @property {module:model/GetCustomerGetCustomersIdResponse200PrimaryContact}
     */
    GetCustomerGetCustomersIdResponse200PrimaryContact,

    /**
     * The GetCustomerGetCustomersIdResponse200PrimaryContactAddress model constructor.
     * @property {module:model/GetCustomerGetCustomersIdResponse200PrimaryContactAddress}
     */
    GetCustomerGetCustomersIdResponse200PrimaryContactAddress,

    /**
     * The GetCustomerGetCustomersIdResponse400 model constructor.
     * @property {module:model/GetCustomerGetCustomersIdResponse400}
     */
    GetCustomerGetCustomersIdResponse400,

    /**
     * The GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200 model constructor.
     * @property {module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200}
     */
    GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200,

    /**
     * The GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links model constructor.
     * @property {module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links}
     */
    GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links,

    /**
     * The GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400 model constructor.
     * @property {module:model/GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400}
     */
    GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse400,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200 model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1 model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2 model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3 model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1 model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2 model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3 model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3,

    /**
     * The GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse404 model constructor.
     * @property {module:model/GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse404}
     */
    GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse404,

    /**
     * The GetMeterPointsGetPropertysIdMeterpointsResponse200 model constructor.
     * @property {module:model/GetMeterPointsGetPropertysIdMeterpointsResponse200}
     */
    GetMeterPointsGetPropertysIdMeterpointsResponse200,

    /**
     * The GetMeterPointsGetPropertysIdMeterpointsResponse404 model constructor.
     * @property {module:model/GetMeterPointsGetPropertysIdMeterpointsResponse404}
     */
    GetMeterPointsGetPropertysIdMeterpointsResponse404,

    /**
     * The GetMeterReadingsGetAuMeterpointsIdReadingsResponse200 model constructor.
     * @property {module:model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse200}
     */
    GetMeterReadingsGetAuMeterpointsIdReadingsResponse200,

    /**
     * The GetMeterReadingsGetAuMeterpointsIdReadingsResponse400 model constructor.
     * @property {module:model/GetMeterReadingsGetAuMeterpointsIdReadingsResponse400}
     */
    GetMeterReadingsGetAuMeterpointsIdReadingsResponse400,

    /**
     * The GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200 model constructor.
     * @property {module:model/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200}
     */
    GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200,

    /**
     * The GetMeterStructureGetAuMeterpointsIdMeterstructureResponse400 model constructor.
     * @property {module:model/GetMeterStructureGetAuMeterpointsIdMeterstructureResponse400}
     */
    GetMeterStructureGetAuMeterpointsIdMeterstructureResponse400,

    /**
     * The GetMeterStructureGetAuMeterpointsMeterstructureResponse200 model constructor.
     * @property {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200}
     */
    GetMeterStructureGetAuMeterpointsMeterstructureResponse200,

    /**
     * The GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links model constructor.
     * @property {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links}
     */
    GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links,

    /**
     * The GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters model constructor.
     * @property {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters}
     */
    GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters,

    /**
     * The GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers model constructor.
     * @property {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers}
     */
    GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers,

    /**
     * The GetMeterStructureGetAuMeterpointsMeterstructureResponse400 model constructor.
     * @property {module:model/GetMeterStructureGetAuMeterpointsMeterstructureResponse400}
     */
    GetMeterStructureGetAuMeterpointsMeterstructureResponse400,

    /**
     * The GetOfferGetOffersIdResponse200 model constructor.
     * @property {module:model/GetOfferGetOffersIdResponse200}
     */
    GetOfferGetOffersIdResponse200,

    /**
     * The GetOfferGetOffersIdResponse200Links model constructor.
     * @property {module:model/GetOfferGetOffersIdResponse200Links}
     */
    GetOfferGetOffersIdResponse200Links,

    /**
     * The GetOfferGetOffersIdResponse200MeterPoints model constructor.
     * @property {module:model/GetOfferGetOffersIdResponse200MeterPoints}
     */
    GetOfferGetOffersIdResponse200MeterPoints,

    /**
     * The GetOfferGetOffersIdResponse200MeterPointsLinks model constructor.
     * @property {module:model/GetOfferGetOffersIdResponse200MeterPointsLinks}
     */
    GetOfferGetOffersIdResponse200MeterPointsLinks,

    /**
     * The GetOfferGetOffersIdResponse200PaymentParams model constructor.
     * @property {module:model/GetOfferGetOffersIdResponse200PaymentParams}
     */
    GetOfferGetOffersIdResponse200PaymentParams,

    /**
     * The GetOfferGetOffersIdResponse200Quotes model constructor.
     * @property {module:model/GetOfferGetOffersIdResponse200Quotes}
     */
    GetOfferGetOffersIdResponse200Quotes,

    /**
     * The GetOfferGetOffersIdResponse200QuotesLinks model constructor.
     * @property {module:model/GetOfferGetOffersIdResponse200QuotesLinks}
     */
    GetOfferGetOffersIdResponse200QuotesLinks,

    /**
     * The GetOfferGetOffersIdResponse404 model constructor.
     * @property {module:model/GetOfferGetOffersIdResponse404}
     */
    GetOfferGetOffersIdResponse404,

    /**
     * The GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200 model constructor.
     * @property {module:model/GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200}
     */
    GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200,

    /**
     * The GetPaymentGetPaymentsIdResponse200 model constructor.
     * @property {module:model/GetPaymentGetPaymentsIdResponse200}
     */
    GetPaymentGetPaymentsIdResponse200,

    /**
     * The GetPaymentGetPaymentsIdResponse404 model constructor.
     * @property {module:model/GetPaymentGetPaymentsIdResponse404}
     */
    GetPaymentGetPaymentsIdResponse404,

    /**
     * The GetPaymentMethodGetPaymentmethodsIdResponse200 model constructor.
     * @property {module:model/GetPaymentMethodGetPaymentmethodsIdResponse200}
     */
    GetPaymentMethodGetPaymentmethodsIdResponse200,

    /**
     * The GetPaymentMethodGetPaymentmethodsIdResponse404 model constructor.
     * @property {module:model/GetPaymentMethodGetPaymentmethodsIdResponse404}
     */
    GetPaymentMethodGetPaymentmethodsIdResponse404,

    /**
     * The GetPaymentPlanGetPaymentplansIdResponse200 model constructor.
     * @property {module:model/GetPaymentPlanGetPaymentplansIdResponse200}
     */
    GetPaymentPlanGetPaymentplansIdResponse200,

    /**
     * The GetPaymentPlanGetPaymentplansIdResponse200Links model constructor.
     * @property {module:model/GetPaymentPlanGetPaymentplansIdResponse200Links}
     */
    GetPaymentPlanGetPaymentplansIdResponse200Links,

    /**
     * The GetPaymentPlanGetPaymentplansIdResponse404 model constructor.
     * @property {module:model/GetPaymentPlanGetPaymentplansIdResponse404}
     */
    GetPaymentPlanGetPaymentplansIdResponse404,

    /**
     * The GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200 model constructor.
     * @property {module:model/GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200}
     */
    GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200,

    /**
     * The GetPaymentRequestGetPaymentrequestsIdResponse200 model constructor.
     * @property {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200}
     */
    GetPaymentRequestGetPaymentrequestsIdResponse200,

    /**
     * The GetPaymentRequestGetPaymentrequestsIdResponse200Links model constructor.
     * @property {module:model/GetPaymentRequestGetPaymentrequestsIdResponse200Links}
     */
    GetPaymentRequestGetPaymentrequestsIdResponse200Links,

    /**
     * The GetPaymentRequestGetPaymentrequestsIdResponse404 model constructor.
     * @property {module:model/GetPaymentRequestGetPaymentrequestsIdResponse404}
     */
    GetPaymentRequestGetPaymentrequestsIdResponse404,

    /**
     * The GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200 model constructor.
     * @property {module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200}
     */
    GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200,

    /**
     * The GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails model constructor.
     * @property {module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails}
     */
    GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails,

    /**
     * The GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404 model constructor.
     * @property {module:model/GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404}
     */
    GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse404,

    /**
     * The GetProductsForNmiGetAuProductsTariffsfornmiResponse200 model constructor.
     * @property {module:model/GetProductsForNmiGetAuProductsTariffsfornmiResponse200}
     */
    GetProductsForNmiGetAuProductsTariffsfornmiResponse200,

    /**
     * The GetProductsForNmiGetAuProductsTariffsfornmiResponse400 model constructor.
     * @property {module:model/GetProductsForNmiGetAuProductsTariffsfornmiResponse400}
     */
    GetProductsForNmiGetAuProductsTariffsfornmiResponse400,

    /**
     * The GetPropertyGetPropertysIdResponse200 model constructor.
     * @property {module:model/GetPropertyGetPropertysIdResponse200}
     */
    GetPropertyGetPropertysIdResponse200,

    /**
     * The GetPropertyGetPropertysIdResponse200Address model constructor.
     * @property {module:model/GetPropertyGetPropertysIdResponse200Address}
     */
    GetPropertyGetPropertysIdResponse200Address,

    /**
     * The GetPropertyGetPropertysIdResponse404 model constructor.
     * @property {module:model/GetPropertyGetPropertysIdResponse404}
     */
    GetPropertyGetPropertysIdResponse404,

    /**
     * The GetProspectGetProspectsIdResponse200 model constructor.
     * @property {module:model/GetProspectGetProspectsIdResponse200}
     */
    GetProspectGetProspectsIdResponse200,

    /**
     * The GetProspectGetProspectsIdResponse200Links model constructor.
     * @property {module:model/GetProspectGetProspectsIdResponse200Links}
     */
    GetProspectGetProspectsIdResponse200Links,

    /**
     * The GetProspectGetProspectsIdResponse404 model constructor.
     * @property {module:model/GetProspectGetProspectsIdResponse404}
     */
    GetProspectGetProspectsIdResponse404,

    /**
     * The GetProspectOffersGetProspectsIdOffersResponse200 model constructor.
     * @property {module:model/GetProspectOffersGetProspectsIdOffersResponse200}
     */
    GetProspectOffersGetProspectsIdOffersResponse200,

    /**
     * The GetProspectOffersGetProspectsIdOffersResponse200Links model constructor.
     * @property {module:model/GetProspectOffersGetProspectsIdOffersResponse200Links}
     */
    GetProspectOffersGetProspectsIdOffersResponse200Links,

    /**
     * The GetProspectOffersGetProspectsIdOffersResponse404 model constructor.
     * @property {module:model/GetProspectOffersGetProspectsIdOffersResponse404}
     */
    GetProspectOffersGetProspectsIdOffersResponse404,

    /**
     * The GetProspectPropertiesGetProspectsIdPropertysResponse200 model constructor.
     * @property {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200}
     */
    GetProspectPropertiesGetProspectsIdPropertysResponse200,

    /**
     * The GetProspectPropertiesGetProspectsIdPropertysResponse200Links model constructor.
     * @property {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse200Links}
     */
    GetProspectPropertiesGetProspectsIdPropertysResponse200Links,

    /**
     * The GetProspectPropertiesGetProspectsIdPropertysResponse404 model constructor.
     * @property {module:model/GetProspectPropertiesGetProspectsIdPropertysResponse404}
     */
    GetProspectPropertiesGetProspectsIdPropertysResponse404,

    /**
     * The GetQuoteFileGetQuotefilesIdResponse200 model constructor.
     * @property {module:model/GetQuoteFileGetQuotefilesIdResponse200}
     */
    GetQuoteFileGetQuotefilesIdResponse200,

    /**
     * The GetQuoteFileGetQuotefilesIdResponse200Links model constructor.
     * @property {module:model/GetQuoteFileGetQuotefilesIdResponse200Links}
     */
    GetQuoteFileGetQuotefilesIdResponse200Links,

    /**
     * The GetQuoteFileGetQuotefilesIdResponse404 model constructor.
     * @property {module:model/GetQuoteFileGetQuotefilesIdResponse404}
     */
    GetQuoteFileGetQuotefilesIdResponse404,

    /**
     * The GetQuoteFileImageGetQuotefilesIdImageResponse200 model constructor.
     * @property {module:model/GetQuoteFileImageGetQuotefilesIdImageResponse200}
     */
    GetQuoteFileImageGetQuotefilesIdImageResponse200,

    /**
     * The GetQuoteGetQuotesIdResponse200 model constructor.
     * @property {module:model/GetQuoteGetQuotesIdResponse200}
     */
    GetQuoteGetQuotesIdResponse200,

    /**
     * The GetQuoteGetQuotesIdResponse200BrokerMargins model constructor.
     * @property {module:model/GetQuoteGetQuotesIdResponse200BrokerMargins}
     */
    GetQuoteGetQuotesIdResponse200BrokerMargins,

    /**
     * The GetQuoteGetQuotesIdResponse200QuoteFiles model constructor.
     * @property {module:model/GetQuoteGetQuotesIdResponse200QuoteFiles}
     */
    GetQuoteGetQuotesIdResponse200QuoteFiles,

    /**
     * The GetQuoteGetQuotesIdResponse200QuoteFilesLinks model constructor.
     * @property {module:model/GetQuoteGetQuotesIdResponse200QuoteFilesLinks}
     */
    GetQuoteGetQuotesIdResponse200QuoteFilesLinks,

    /**
     * The GetQuoteGetQuotesIdResponse404 model constructor.
     * @property {module:model/GetQuoteGetQuotesIdResponse404}
     */
    GetQuoteGetQuotesIdResponse404,

    /**
     * The GetQuotesGetOffersIdQuotesResponse200 model constructor.
     * @property {module:model/GetQuotesGetOffersIdQuotesResponse200}
     */
    GetQuotesGetOffersIdQuotesResponse200,

    /**
     * The GetQuotesGetOffersIdQuotesResponse200Links model constructor.
     * @property {module:model/GetQuotesGetOffersIdQuotesResponse200Links}
     */
    GetQuotesGetOffersIdQuotesResponse200Links,

    /**
     * The GetQuotesGetOffersIdQuotesResponse200MeterPoints model constructor.
     * @property {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPoints}
     */
    GetQuotesGetOffersIdQuotesResponse200MeterPoints,

    /**
     * The GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks model constructor.
     * @property {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks}
     */
    GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks,

    /**
     * The GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices model constructor.
     * @property {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices}
     */
    GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices,

    /**
     * The GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents model constructor.
     * @property {module:model/GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents}
     */
    GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents,

    /**
     * The GetQuotesGetOffersIdQuotesResponse404 model constructor.
     * @property {module:model/GetQuotesGetOffersIdQuotesResponse404}
     */
    GetQuotesGetOffersIdQuotesResponse404,

    /**
     * The GetSepaDirectDebitGetSepadirectdebitsIdResponse200 model constructor.
     * @property {module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200}
     */
    GetSepaDirectDebitGetSepadirectdebitsIdResponse200,

    /**
     * The GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links model constructor.
     * @property {module:model/GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links}
     */
    GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links,

    /**
     * The GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200 model constructor.
     * @property {module:model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200}
     */
    GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200,

    /**
     * The GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse400 model constructor.
     * @property {module:model/GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse400}
     */
    GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse400,

    /**
     * The GetStripePaymentCardGetStripepaymentcardsIdResponse200 model constructor.
     * @property {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200}
     */
    GetStripePaymentCardGetStripepaymentcardsIdResponse200,

    /**
     * The GetStripePaymentCardGetStripepaymentcardsIdResponse200Links model constructor.
     * @property {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse200Links}
     */
    GetStripePaymentCardGetStripepaymentcardsIdResponse200Links,

    /**
     * The GetStripePaymentCardGetStripepaymentcardsIdResponse404 model constructor.
     * @property {module:model/GetStripePaymentCardGetStripepaymentcardsIdResponse404}
     */
    GetStripePaymentCardGetStripepaymentcardsIdResponse404,

    /**
     * The GetTicketGetTicketsIdResponse200 model constructor.
     * @property {module:model/GetTicketGetTicketsIdResponse200}
     */
    GetTicketGetTicketsIdResponse200,

    /**
     * The GetTicketGetTicketsIdResponse200Links model constructor.
     * @property {module:model/GetTicketGetTicketsIdResponse200Links}
     */
    GetTicketGetTicketsIdResponse200Links,

    /**
     * The GetTicketGetTicketsIdResponse400 model constructor.
     * @property {module:model/GetTicketGetTicketsIdResponse400}
     */
    GetTicketGetTicketsIdResponse400,

    /**
     * The GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200 model constructor.
     * @property {module:model/GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200}
     */
    GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200,

    /**
     * The HotbillPostAccountsIdHotbillResponse400 model constructor.
     * @property {module:model/HotbillPostAccountsIdHotbillResponse400}
     */
    HotbillPostAccountsIdHotbillResponse400,

    /**
     * The ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200 model constructor.
     * @property {module:model/ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200}
     */
    ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200,

    /**
     * The LookUpAccountGetAccountsResponse200 model constructor.
     * @property {module:model/LookUpAccountGetAccountsResponse200}
     */
    LookUpAccountGetAccountsResponse200,

    /**
     * The LookUpAccountGetAccountsResponse200BillingAddress model constructor.
     * @property {module:model/LookUpAccountGetAccountsResponse200BillingAddress}
     */
    LookUpAccountGetAccountsResponse200BillingAddress,

    /**
     * The LookUpAccountGetAccountsResponse200Links model constructor.
     * @property {module:model/LookUpAccountGetAccountsResponse200Links}
     */
    LookUpAccountGetAccountsResponse200Links,

    /**
     * The LookUpAccountGetAccountsResponse400 model constructor.
     * @property {module:model/LookUpAccountGetAccountsResponse400}
     */
    LookUpAccountGetAccountsResponse400,

    /**
     * The LookUpCustomerGetCustomersResponse200 model constructor.
     * @property {module:model/LookUpCustomerGetCustomersResponse200}
     */
    LookUpCustomerGetCustomersResponse200,

    /**
     * The LookUpCustomerGetCustomersResponse200CompanyAddress model constructor.
     * @property {module:model/LookUpCustomerGetCustomersResponse200CompanyAddress}
     */
    LookUpCustomerGetCustomersResponse200CompanyAddress,

    /**
     * The LookUpCustomerGetCustomersResponse200Links model constructor.
     * @property {module:model/LookUpCustomerGetCustomersResponse200Links}
     */
    LookUpCustomerGetCustomersResponse200Links,

    /**
     * The LookUpCustomerGetCustomersResponse400 model constructor.
     * @property {module:model/LookUpCustomerGetCustomersResponse400}
     */
    LookUpCustomerGetCustomersResponse400,

    /**
     * The LookUpMeterPointGetAuMeterpointsResponse200 model constructor.
     * @property {module:model/LookUpMeterPointGetAuMeterpointsResponse200}
     */
    LookUpMeterPointGetAuMeterpointsResponse200,

    /**
     * The LookUpMeterPointGetAuMeterpointsResponse400 model constructor.
     * @property {module:model/LookUpMeterPointGetAuMeterpointsResponse400}
     */
    LookUpMeterPointGetAuMeterpointsResponse400,

    /**
     * The LookUpTicketsGetTicketsResponse200 model constructor.
     * @property {module:model/LookUpTicketsGetTicketsResponse200}
     */
    LookUpTicketsGetTicketsResponse200,

    /**
     * The LookUpTicketsGetTicketsResponse400 model constructor.
     * @property {module:model/LookUpTicketsGetTicketsResponse400}
     */
    LookUpTicketsGetTicketsResponse400,

    /**
     * The LookupBrokerGetBrokersResponse200 model constructor.
     * @property {module:model/LookupBrokerGetBrokersResponse200}
     */
    LookupBrokerGetBrokersResponse200,

    /**
     * The LookupBrokerGetBrokersResponse200Links model constructor.
     * @property {module:model/LookupBrokerGetBrokersResponse200Links}
     */
    LookupBrokerGetBrokersResponse200Links,

    /**
     * The LookupBrokerGetBrokersResponse400 model constructor.
     * @property {module:model/LookupBrokerGetBrokersResponse400}
     */
    LookupBrokerGetBrokersResponse400,

    /**
     * The LookupBrokerGetBrokersResponse404 model constructor.
     * @property {module:model/LookupBrokerGetBrokersResponse404}
     */
    LookupBrokerGetBrokersResponse404,

    /**
     * The NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody model constructor.
     * @property {module:model/NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody}
     */
    NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody,

    /**
     * The NewBpointDirectDebitPostAuBpointdirectdebitsResponse200 model constructor.
     * @property {module:model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse200}
     */
    NewBpointDirectDebitPostAuBpointdirectdebitsResponse200,

    /**
     * The NewBpointDirectDebitPostAuBpointdirectdebitsResponse400 model constructor.
     * @property {module:model/NewBpointDirectDebitPostAuBpointdirectdebitsResponse400}
     */
    NewBpointDirectDebitPostAuBpointdirectdebitsResponse400,

    /**
     * The NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody model constructor.
     * @property {module:model/NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody}
     */
    NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody,

    /**
     * The NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200 model constructor.
     * @property {module:model/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200}
     */
    NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200,

    /**
     * The NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse400 model constructor.
     * @property {module:model/NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse400}
     */
    NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse400,

    /**
     * The NewSepaDirectDebitPostSepadirectdebitsRequestBody model constructor.
     * @property {module:model/NewSepaDirectDebitPostSepadirectdebitsRequestBody}
     */
    NewSepaDirectDebitPostSepadirectdebitsRequestBody,

    /**
     * The NewSepaDirectDebitPostSepadirectdebitsResponse200 model constructor.
     * @property {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse200}
     */
    NewSepaDirectDebitPostSepadirectdebitsResponse200,

    /**
     * The NewSepaDirectDebitPostSepadirectdebitsResponse200Links model constructor.
     * @property {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse200Links}
     */
    NewSepaDirectDebitPostSepadirectdebitsResponse200Links,

    /**
     * The NewSepaDirectDebitPostSepadirectdebitsResponse400 model constructor.
     * @property {module:model/NewSepaDirectDebitPostSepadirectdebitsResponse400}
     */
    NewSepaDirectDebitPostSepadirectdebitsResponse400,

    /**
     * The NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody model constructor.
     * @property {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody}
     */
    NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody,

    /**
     * The NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200 model constructor.
     * @property {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200}
     */
    NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200,

    /**
     * The NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400 model constructor.
     * @property {module:model/NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400}
     */
    NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400,

    /**
     * The PaymentPostAccountsIdPaymentRequestBody model constructor.
     * @property {module:model/PaymentPostAccountsIdPaymentRequestBody}
     */
    PaymentPostAccountsIdPaymentRequestBody,

    /**
     * The PaymentPostAccountsIdPaymentResponse200 model constructor.
     * @property {module:model/PaymentPostAccountsIdPaymentResponse200}
     */
    PaymentPostAccountsIdPaymentResponse200,

    /**
     * The PaymentPostAccountsIdPaymentResponse400 model constructor.
     * @property {module:model/PaymentPostAccountsIdPaymentResponse400}
     */
    PaymentPostAccountsIdPaymentResponse400,

    /**
     * The RenewAccountPostAccountsIdRenewalRequestBody model constructor.
     * @property {module:model/RenewAccountPostAccountsIdRenewalRequestBody}
     */
    RenewAccountPostAccountsIdRenewalRequestBody,

    /**
     * The RenewAccountPostAccountsIdRenewalResponse200 model constructor.
     * @property {module:model/RenewAccountPostAccountsIdRenewalResponse200}
     */
    RenewAccountPostAccountsIdRenewalResponse200,

    /**
     * The RenewAccountPostAccountsIdRenewalResponse400 model constructor.
     * @property {module:model/RenewAccountPostAccountsIdRenewalResponse400}
     */
    RenewAccountPostAccountsIdRenewalResponse400,

    /**
     * The RestartFailedBillRequestPostBillrequestsIdRestartfailedResponse404 model constructor.
     * @property {module:model/RestartFailedBillRequestPostBillrequestsIdRestartfailedResponse404}
     */
    RestartFailedBillRequestPostBillrequestsIdRestartfailedResponse404,

    /**
     * The ReversionBillPostBillsIdReversionResponse200 model constructor.
     * @property {module:model/ReversionBillPostBillsIdReversionResponse200}
     */
    ReversionBillPostBillsIdReversionResponse200,

    /**
     * The ReversionBillPostBillsIdReversionResponse200Links model constructor.
     * @property {module:model/ReversionBillPostBillsIdReversionResponse200Links}
     */
    ReversionBillPostBillsIdReversionResponse200Links,

    /**
     * The ReversionBillPostBillsIdReversionResponse404 model constructor.
     * @property {module:model/ReversionBillPostBillsIdReversionResponse404}
     */
    ReversionBillPostBillsIdReversionResponse404,

    /**
     * The StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody model constructor.
     * @property {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody}
     */
    StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody,

    /**
     * The StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400 model constructor.
     * @property {module:model/StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400}
     */
    StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400,

    /**
     * The StoreCardPostStripepaymentcardsRequestBody model constructor.
     * @property {module:model/StoreCardPostStripepaymentcardsRequestBody}
     */
    StoreCardPostStripepaymentcardsRequestBody,

    /**
     * The StoreCardPostStripepaymentcardsResponse400 model constructor.
     * @property {module:model/StoreCardPostStripepaymentcardsResponse400}
     */
    StoreCardPostStripepaymentcardsResponse400,

    /**
     * The SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody model constructor.
     * @property {module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody}
     */
    SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody,

    /**
     * The SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400 model constructor.
     * @property {module:model/SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400}
     */
    SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400,

    /**
     * The TariffInformationGetAccountsIdTariffinformationResponse200 model constructor.
     * @property {module:model/TariffInformationGetAccountsIdTariffinformationResponse200}
     */
    TariffInformationGetAccountsIdTariffinformationResponse200,

    /**
     * The TariffInformationGetAccountsIdTariffinformationResponse200TIL model constructor.
     * @property {module:model/TariffInformationGetAccountsIdTariffinformationResponse200TIL}
     */
    TariffInformationGetAccountsIdTariffinformationResponse200TIL,

    /**
     * The TariffInformationGetAccountsIdTariffinformationResponse400 model constructor.
     * @property {module:model/TariffInformationGetAccountsIdTariffinformationResponse400}
     */
    TariffInformationGetAccountsIdTariffinformationResponse400,

    /**
     * The UpdateAccountContactPutAccountsIdContactsContactidRequestBody model constructor.
     * @property {module:model/UpdateAccountContactPutAccountsIdContactsContactidRequestBody}
     */
    UpdateAccountContactPutAccountsIdContactsContactidRequestBody,

    /**
     * The UpdateAccountContactPutAccountsIdContactsContactidResponse400 model constructor.
     * @property {module:model/UpdateAccountContactPutAccountsIdContactsContactidResponse400}
     */
    UpdateAccountContactPutAccountsIdContactsContactidResponse400,

    /**
     * The UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody model constructor.
     * @property {module:model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody}
     */
    UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody,

    /**
     * The UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400 model constructor.
     * @property {module:model/UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400}
     */
    UpdateAccountPrimaryContactPutAccountsIdPrimarycontactResponse400,

    /**
     * The UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody model constructor.
     * @property {module:model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody}
     */
    UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody,

    /**
     * The UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400 model constructor.
     * @property {module:model/UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400}
     */
    UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400,

    /**
     * The UpdateCustomerConsentsPutCustomersIdConsentsRequestBody model constructor.
     * @property {module:model/UpdateCustomerConsentsPutCustomersIdConsentsRequestBody}
     */
    UpdateCustomerConsentsPutCustomersIdConsentsRequestBody,

    /**
     * The UpdateCustomerConsentsPutCustomersIdConsentsResponse400 model constructor.
     * @property {module:model/UpdateCustomerConsentsPutCustomersIdConsentsResponse400}
     */
    UpdateCustomerConsentsPutCustomersIdConsentsResponse400,

    /**
     * The UpdateCustomerContactPutCustomersIdContactsContactidRequestBody model constructor.
     * @property {module:model/UpdateCustomerContactPutCustomersIdContactsContactidRequestBody}
     */
    UpdateCustomerContactPutCustomersIdContactsContactidRequestBody,

    /**
     * The UpdateCustomerContactPutCustomersIdContactsContactidResponse400 model constructor.
     * @property {module:model/UpdateCustomerContactPutCustomersIdContactsContactidResponse400}
     */
    UpdateCustomerContactPutCustomersIdContactsContactidResponse400,

    /**
     * The UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody model constructor.
     * @property {module:model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody}
     */
    UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody,

    /**
     * The UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400 model constructor.
     * @property {module:model/UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400}
     */
    UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactResponse400,

    /**
     * The UpdateCustomerPutCustomersIdRequestBody model constructor.
     * @property {module:model/UpdateCustomerPutCustomersIdRequestBody}
     */
    UpdateCustomerPutCustomersIdRequestBody,

    /**
     * The UpdateCustomerPutCustomersIdRequestBodyPrimaryContact model constructor.
     * @property {module:model/UpdateCustomerPutCustomersIdRequestBodyPrimaryContact}
     */
    UpdateCustomerPutCustomersIdRequestBodyPrimaryContact,

    /**
     * The UpdateCustomerPutCustomersIdResponse400 model constructor.
     * @property {module:model/UpdateCustomerPutCustomersIdResponse400}
     */
    UpdateCustomerPutCustomersIdResponse400,

    /**
     * The UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody model constructor.
     * @property {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody}
     */
    UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody,

    /**
     * The UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences model constructor.
     * @property {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences}
     */
    UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences,

    /**
     * The UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse400 model constructor.
     * @property {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse400}
     */
    UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse400,

    /**
     * The UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse404 model constructor.
     * @property {module:model/UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse404}
     */
    UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse404,

    /**
     * The UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody model constructor.
     * @property {module:model/UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody}
     */
    UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody,

    /**
     * The UpdateSepaDirectDebitPutSepadirectdebitsIdResponse400 model constructor.
     * @property {module:model/UpdateSepaDirectDebitPutSepadirectdebitsIdResponse400}
     */
    UpdateSepaDirectDebitPutSepadirectdebitsIdResponse400,

    /**
     * The UpdateTicketPutTicketsIdRequestBody model constructor.
     * @property {module:model/UpdateTicketPutTicketsIdRequestBody}
     */
    UpdateTicketPutTicketsIdRequestBody,

    /**
     * The UpdateTicketPutTicketsIdResponse400 model constructor.
     * @property {module:model/UpdateTicketPutTicketsIdResponse400}
     */
    UpdateTicketPutTicketsIdResponse400,

    /**
    * The AccountCreditsApi service constructor.
    * @property {module:api/AccountCreditsApi}
    */
    AccountCreditsApi,

    /**
    * The AccountDebitsApi service constructor.
    * @property {module:api/AccountDebitsApi}
    */
    AccountDebitsApi,

    /**
    * The AccountsApi service constructor.
    * @property {module:api/AccountsApi}
    */
    AccountsApi,

    /**
    * The AccountsAustraliaApi service constructor.
    * @property {module:api/AccountsAustraliaApi}
    */
    AccountsAustraliaApi,

    /**
    * The AlertsApi service constructor.
    * @property {module:api/AlertsApi}
    */
    AlertsApi,

    /**
    * The BillPeriodsApi service constructor.
    * @property {module:api/BillPeriodsApi}
    */
    BillPeriodsApi,

    /**
    * The BillRequestsApi service constructor.
    * @property {module:api/BillRequestsApi}
    */
    BillRequestsApi,

    /**
    * The BillingEntitiesApi service constructor.
    * @property {module:api/BillingEntitiesApi}
    */
    BillingEntitiesApi,

    /**
    * The BillsApi service constructor.
    * @property {module:api/BillsApi}
    */
    BillsApi,

    /**
    * The CommunicationsApi service constructor.
    * @property {module:api/CommunicationsApi}
    */
    CommunicationsApi,

    /**
    * The ConcessionsAustraliaApi service constructor.
    * @property {module:api/ConcessionsAustraliaApi}
    */
    ConcessionsAustraliaApi,

    /**
    * The ContactsApi service constructor.
    * @property {module:api/ContactsApi}
    */
    ContactsApi,

    /**
    * The CreditsApi service constructor.
    * @property {module:api/CreditsApi}
    */
    CreditsApi,

    /**
    * The CustomerPaymentsApi service constructor.
    * @property {module:api/CustomerPaymentsApi}
    */
    CustomerPaymentsApi,

    /**
    * The CustomersApi service constructor.
    * @property {module:api/CustomersApi}
    */
    CustomersApi,

    /**
    * The CustomersAustraliaApi service constructor.
    * @property {module:api/CustomersAustraliaApi}
    */
    CustomersAustraliaApi,

    /**
    * The FinancialsApi service constructor.
    * @property {module:api/FinancialsApi}
    */
    FinancialsApi,

    /**
    * The MarketingCampaignSourcesApi service constructor.
    * @property {module:api/MarketingCampaignSourcesApi}
    */
    MarketingCampaignSourcesApi,

    /**
    * The MeterpointsAustraliaApi service constructor.
    * @property {module:api/MeterpointsAustraliaApi}
    */
    MeterpointsAustraliaApi,

    /**
    * The NmisApi service constructor.
    * @property {module:api/NmisApi}
    */
    NmisApi,

    /**
    * The OffersApi service constructor.
    * @property {module:api/OffersApi}
    */
    OffersApi,

    /**
    * The PaymentPlansApi service constructor.
    * @property {module:api/PaymentPlansApi}
    */
    PaymentPlansApi,

    /**
    * The ProductsApi service constructor.
    * @property {module:api/ProductsApi}
    */
    ProductsApi,

    /**
    * The PropertiesApi service constructor.
    * @property {module:api/PropertiesApi}
    */
    PropertiesApi,

    /**
    * The ProspectsApi service constructor.
    * @property {module:api/ProspectsApi}
    */
    ProspectsApi,

    /**
    * The QuoteFilesApi service constructor.
    * @property {module:api/QuoteFilesApi}
    */
    QuoteFilesApi,

    /**
    * The QuotesApi service constructor.
    * @property {module:api/QuotesApi}
    */
    QuotesApi,

    /**
    * The TicketsApi service constructor.
    * @property {module:api/TicketsApi}
    */
    TicketsApi
};
