# JuniferAuNativeApi.GetContactGetContactsIdResponse200Address

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressType** | **String** | Internally used address type which defines number of lines and format used etc. | [optional] 
**address1** | **String** | Contact address line 1 | [optional] 
**address2** | **String** | Contact address line 2 | [optional] 
**address3** | **String** | Contact address line 3 | [optional] 
**address4** | **String** | Contact address line 4 | [optional] 
**address5** | **String** | Contact address line 5 | [optional] 
**postcode** | **String** | Contact address post code | [optional] 
**countryCode** | **String** | Contact address country code | [optional] 
**country** | **String** | Contact address country | [optional] 


