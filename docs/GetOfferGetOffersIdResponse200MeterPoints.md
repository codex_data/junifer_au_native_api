# JuniferAuNativeApi.GetOfferGetOffersIdResponse200MeterPoints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **Number** | MeterPoint identifier. For meterPoints of type &#x60;MPAN&#x60; this will be MPAN core, for &#x60;MPRN&#x60; - MPRN identifier. | 
**type** | **String** | The type of the meterPoint. For gas meterPoints this will be &#x60;MPRN&#x60;, for electricity/power meterPoints this will be &#x60;MPAN&#x60;. | 
**links** | [**GetOfferGetOffersIdResponse200MeterPointsLinks**](GetOfferGetOffersIdResponse200MeterPointsLinks.md) |  | 


