# JuniferAuNativeApi.ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of customer to which this account will be linked to | 


