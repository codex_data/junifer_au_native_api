# JuniferAuNativeApi.GetProspectOffersGetProspectsIdOffersResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Number** | The offer&#39;s number. | 
**fromDttm** | **Date** | The offer&#39;s from date and time. | 
**toDttm** | **Date** | The offer&#39;s to date and time. | 
**links** | [**GetProspectOffersGetProspectsIdOffersResponse200Links**](GetProspectOffersGetProspectsIdOffersResponse200Links.md) |  | 


