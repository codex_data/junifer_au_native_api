# JuniferAuNativeApi.GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200DebtRecoveryDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | DebtRecoveryPeriod id | 
**fromDttm** | **Date** | debt recover period from datetime | 
**toDttm** | **Date** | debt recover period to datetime | 
**stoppedDttm** | **Date** | debt recover period stopped datetime | [optional] 
**amount** | **Number** | debt recover amount | 


