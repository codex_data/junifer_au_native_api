# JuniferAuNativeApi.GetOfferGetOffersIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The offer&#39;s id. | 
**_class** | **String** | The class of the offer. Examples include &#x60;Gas&#x60;. | 
**number** | **String** | The offer&#39;s number. | 
**fromDttm** | **Date** | The from date for the offer. | 
**toDttm** | **Date** | The to date for the offer. | 
**createdDttm** | **Date** | Offer&#39;s creation date and time. | 
**productBundle** | **String** | The offer&#39;s associated product bundle. | 
**status** | **String** | The offer status. Status codes include &#x60;Cancelled, Accepted, Approved, Offered, Priced, Pricing Failed, Pricing, New&#x60;. | 
**cancelledByUser** | **String** | The user who cancelled the offer. Note: This is only shown if the offer has been cancelled. | [optional] 
**cancelledDttm** | **Date** | The date and time in which the offer was cancelled on. Note: This is only shown when the offer has been cancelled. | [optional] 
**cancelledReason** | **String** | The reason for cancelling the offer. Note: This is only show when thew offer has been cancelled. | [optional] 
**acceptedDttm** | **Date** | The day and time the offer was accepted at. Note: This is only shown when the offer has been accepted. | [optional] 
**pricedDttm** | **Date** | The date and time the offer was priced at. Note: This is only shown when the status of the offer is either of the following &#x60; Accepted, Approved, Offered, Priced | [optional] 
**createdByUser** | **String** | The user who created the offer. | 
**paymentParams** | [**GetOfferGetOffersIdResponse200PaymentParams**](GetOfferGetOffersIdResponse200PaymentParams.md) |  | [optional] 
**quotes** | [**GetOfferGetOffersIdResponse200Quotes**](GetOfferGetOffersIdResponse200Quotes.md) |  | 
**meterPoints** | [**GetOfferGetOffersIdResponse200MeterPoints**](GetOfferGetOffersIdResponse200MeterPoints.md) |  | 
**links** | [**GetOfferGetOffersIdResponse200Links**](GetOfferGetOffersIdResponse200Links.md) |  | 


