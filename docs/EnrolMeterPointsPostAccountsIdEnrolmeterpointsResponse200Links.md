# JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this meterpoint | 
**readings** | **String** | Link to the readings | 
**meterStructure** | **String** | Link to resource which lists the meterpoint structure - meters attached, meter registers, etc. | 
**supplyAddress** | **String** | Link to the supply address | 
**supplyStatusHistory** | **String** | Link to the supply status history | 
**estimatedUsage** | **String** | Link to the estimated usage | [optional] 


