# JuniferAuNativeApi.GetMeterReadingsGetAuMeterpointsIdReadingsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;RequiredParameterMissing&#x60; - The required &#39;fromDt&#39; query parameter was not supplied * &#x60;InvalidParameter&#x60; - The value &#39;xxx&#39; is not a valid reading status | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)

* `InvalidParameter` (value: `"InvalidParameter"`)




