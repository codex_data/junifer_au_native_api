# JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the agreement | 
**number** | **String** | Agreement number | 
**code** | **String** | Product code | 
**name** | **String** | Product name | 
**display** | **String** | Product display name | 
**durationMonths** | **Number** | Duration | 
**endDttm** | **Date** | The date when the product terminates | [optional] 
**gspGroup** | **String** | Grid supply point group | 
**directDebit** | **Boolean** | Direct debit product | [optional] 
**availableToPublic** | **Boolean** | Available to public | [optional] 
**standingCharge** | **Number** | Standing charge | 
**onlineDiscount** | **Number** | Online discount | [optional] 
**earlyTerminationCharge** | **Number** | Early termination charge | [optional] 
**dualFuelDiscount** | **Number** | Dual fuel discount | [optional] 
**unitRate** | **Number** | The unit rate for Gas | 
**unitRates** | [**[GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates]**](GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates.md) | The unit rates for Electricity | 
**fromDt** | **Number** | Date that the account started being supplied by this product | 
**toDt** | **Number** | Date that the account will stop being supplied by this product | [optional] 
**contractedToDt** | **Number** | Date that the account is contracted to this product | [optional] 
**earlyTerminationChargeToDt** | **Number** | Date that the account termination fee will stop to apply | [optional] 
**followOnCode** | **String** | Following Product Bundle code. | 
**supplyAddress** | [**GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress**](GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress.md) |  | 
**meterPoints** | [**[GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints]**](GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints.md) | Meter points | 


