# JuniferAuNativeApi.TicketsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferTicketsIdCancelTicketPost**](TicketsApi.md#juniferTicketsIdCancelTicketPost) | **POST** /junifer/tickets/{id}/cancelTicket | Cancel Ticket
[**juniferTicketsIdGet**](TicketsApi.md#juniferTicketsIdGet) | **GET** /junifer/tickets/{id} | Get ticket
[**juniferTicketsIdPut**](TicketsApi.md#juniferTicketsIdPut) | **PUT** /junifer/tickets/{id} | Update Ticket



## juniferTicketsIdCancelTicketPost

> juniferTicketsIdCancelTicketPost(id, ticketCancelReason)

Cancel Ticket

Cancels a ticket.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.TicketsApi();
let id = 3.4; // Number | Ticket ID
let ticketCancelReason = "ticketCancelReason_example"; // String | The reason for the ticket cancellation
apiInstance.juniferTicketsIdCancelTicketPost(id, ticketCancelReason, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Ticket ID | 
 **ticketCancelReason** | **String**| The reason for the ticket cancellation | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferTicketsIdGet

> GetTicketGetTicketsIdResponse200 juniferTicketsIdGet(id)

Get ticket

Gets ticket by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.TicketsApi();
let id = 3.4; // Number | Ticket ID
apiInstance.juniferTicketsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Ticket ID | 

### Return type

[**GetTicketGetTicketsIdResponse200**](GetTicketGetTicketsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferTicketsIdPut

> juniferTicketsIdPut(id, requestBody)

Update Ticket

Update a ticket.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.TicketsApi();
let id = 3.4; // Number | Ticket ID
let requestBody = new JuniferAuNativeApi.UpdateTicketPutTicketsIdRequestBody(); // UpdateTicketPutTicketsIdRequestBody | Request body
apiInstance.juniferTicketsIdPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Ticket ID | 
 **requestBody** | [**UpdateTicketPutTicketsIdRequestBody**](UpdateTicketPutTicketsIdRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

