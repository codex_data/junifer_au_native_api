# JuniferAuNativeApi.CreateAccountDebitPostAccountsIdAccountdebitsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InvalidSalesTax&#x60; - &#39;salesTaxName&#39; is not a valid type of Sales Tax * &#x60;InvalidAccountDebitReason&#x60; - &#39;NotAReason&#39; not recognised. Account debit reason must be a valid type * &#x60;GrossAmountMissing&#x60; - The required &#39;grossAmount&#39; parameter is missing * &#x60;AccountDebitReasonNameMissing&#x60; - The required &#39;accountDebitReasonName&#39; parameter is missing * &#x60;salesTaxNameMissing&#x60; - The required &#39;salesTaxName&#39; parameter is missing | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InvalidSalesTax` (value: `"InvalidSalesTax"`)

* `InvalidAccountDebitReason` (value: `"InvalidAccountDebitReason"`)

* `GrossAmountMissing` (value: `"GrossAmountMissing"`)

* `AccountDebitReasonNameMissing` (value: `"AccountDebitReasonNameMissing"`)

* `salesTaxNameMissing` (value: `"salesTaxNameMissing"`)




