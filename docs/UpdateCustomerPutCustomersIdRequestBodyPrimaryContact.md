# JuniferAuNativeApi.UpdateCustomerPutCustomersIdRequestBodyPrimaryContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Customer&#39;s primary contact title (This field is ignored) | [optional] 
**forename** | **String** | Customer&#39;s primary contact forename (This field is ignored) | [optional] 
**surname** | **String** | Customer&#39;s primary contact surname (This field is ignored) | 
**email** | **String** | Customer&#39;s primary contact email (This field is ignored) | 
**dateOfBirth** | **String** | Customer&#39;s date of birth (This field is ignored) | [optional] 
**phoneNumber1** | **String** | Customer&#39;s primary contact phone number (This field is ignored) | [optional] 
**phoneNumber2** | **String** | Customer&#39;s secondary contact phone number (This field is ignored) | [optional] 
**phoneNumber3** | **String** | Customer&#39;s third contact phone number (This field is ignored) | [optional] 


