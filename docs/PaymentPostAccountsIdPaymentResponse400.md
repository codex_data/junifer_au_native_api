# JuniferAuNativeApi.PaymentPostAccountsIdPaymentResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;PaymentMethodTypeNotMatch&#x60; - The payment method type specified doesn&#39;t match anything in Junifer * &#x60;PaymentMethodTypeNotSupported&#x60; - The payment method type specified is not supported through this API * &#x60;InvalidStatus&#x60; - The status can only be &#39;Successful&#39; or &#39;Cancelled&#39; * &#x60;StatusNotMatch&#x60; - The status specified does not match anything in Junifer | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `PaymentMethodTypeNotMatch` (value: `"PaymentMethodTypeNotMatch"`)

* `PaymentMethodTypeNotSupported` (value: `"PaymentMethodTypeNotSupported"`)

* `InvalidStatus` (value: `"InvalidStatus"`)

* `StatusNotMatch` (value: `"StatusNotMatch"`)




