# JuniferAuNativeApi.CancelAccountDebitDeleteAccountdebitsIdResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;AccountDebitCancelled&#x60; - \&quot;This account debit has already been cancelled\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `AccountDebitCancelled` (value: `"AccountDebitCancelled"`)




