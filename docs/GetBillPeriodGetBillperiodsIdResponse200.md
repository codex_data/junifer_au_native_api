# JuniferAuNativeApi.GetBillPeriodGetBillperiodsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Bill Period ID | 
**accountId** | **Number** | Account ID | 
**statusCode** | **String** | Bill Period status code Can either be &#x60; Open, Closed&#x60; | [optional] 
**fromDttm** | **Date** | From date of the Bill Period | 
**toDttm** | **Date** | To date of the Bill Period | 
**consolidatedBillPeriodId** | **Number** | Consolidated Bill Period ID | [optional] 


