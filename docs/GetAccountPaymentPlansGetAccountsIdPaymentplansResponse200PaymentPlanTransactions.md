# JuniferAuNativeApi.GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | PaymentPlanTransaction ID | 
**accountTransactionId** | **Number** | Associated Account Transaction ID | 


