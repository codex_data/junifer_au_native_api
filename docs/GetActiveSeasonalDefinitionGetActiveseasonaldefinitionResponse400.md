# JuniferAuNativeApi.GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;NoActiveSeasonalDefinition&#x60; - There is currently no active seasonal definition to return | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `NoActiveSeasonalDefinition` (value: `"NoActiveSeasonalDefinition"`)




