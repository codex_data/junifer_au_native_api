# JuniferAuNativeApi.CustomersAustraliaApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAuCustomersEnrolBusinessCustomerPost**](CustomersAustraliaApi.md#juniferAuCustomersEnrolBusinessCustomerPost) | **POST** /junifer/au/customers/enrolBusinessCustomer | Enrol business customer
[**juniferAuCustomersEnrolCustomerPost**](CustomersAustraliaApi.md#juniferAuCustomersEnrolCustomerPost) | **POST** /junifer/au/customers/enrolCustomer | Enrol customer
[**juniferAuCustomersLifeSupportPost**](CustomersAustraliaApi.md#juniferAuCustomersLifeSupportPost) | **POST** /junifer/au/customers/lifeSupport | Add life support and sensitive load details
[**juniferAuCustomersSiteAccessPost**](CustomersAustraliaApi.md#juniferAuCustomersSiteAccessPost) | **POST** /junifer/au/customers/siteAccess | Add site access and hazard details



## juniferAuCustomersEnrolBusinessCustomerPost

> AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200 juniferAuCustomersEnrolBusinessCustomerPost(requestBody)

Enrol business customer

Enrols a business customer for electricity and/or gas product. This is an Australia specific API.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersAustraliaApi();
let requestBody = new JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody(); // AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody | Request body
apiInstance.juniferAuCustomersEnrolBusinessCustomerPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody**](AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody.md)| Request body | 

### Return type

[**AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200**](AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAuCustomersEnrolCustomerPost

> AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200 juniferAuCustomersEnrolCustomerPost(requestBody)

Enrol customer

Enrols customer for electricity and/or gas product. This is an Australia specific API.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersAustraliaApi();
let requestBody = new JuniferAuNativeApi.AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody(); // AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody | Request body
apiInstance.juniferAuCustomersEnrolCustomerPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody**](AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBody.md)| Request body | 

### Return type

[**AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200**](AusEnrolCustomerPostAuCustomersEnrolcustomerResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAuCustomersLifeSupportPost

> juniferAuCustomersLifeSupportPost(requestBody)

Add life support and sensitive load details

Adds Life Support and Sensitive Load details to indicate economic, health or safety issues with loss of supply of the connection point. This is an Australia specific API.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersAustraliaApi();
let requestBody = new JuniferAuNativeApi.AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody(); // AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody | Request body
apiInstance.juniferAuCustomersLifeSupportPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody**](AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAuCustomersSiteAccessPost

> juniferAuCustomersSiteAccessPost(requestBody)

Add site access and hazard details

Adds Site Access Details and Hazard Description to Customer Property. This is an Australia specific API.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersAustraliaApi();
let requestBody = new JuniferAuNativeApi.AusSiteAccessPostAuCustomersSiteaccessRequestBody(); // AusSiteAccessPostAuCustomersSiteaccessRequestBody | Request body
apiInstance.juniferAuCustomersSiteAccessPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**AusSiteAccessPostAuCustomersSiteaccessRequestBody**](AusSiteAccessPostAuCustomersSiteaccessRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

