# JuniferAuNativeApi.GetCreditGetCreditsIdResponse200CreditFiles

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Credit file ID | 
**display** | **String** | Credit file type | 
**viewable** | **Boolean** | Flag indicating whether the credit file can be viewed | 
**links** | [**GetCreditGetCreditsIdResponse200Links1**](GetCreditGetCreditsIdResponse200Links1.md) |  | 


