# JuniferAuNativeApi.AusLifeSupportSensitiveLoadPostAuCustomersLifesupportRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nmi** | **String** | Identifier of the NMI including checksum | 
**vulnerabilityType** | **String** | Life Support or Sensitive Load | 
**lifeSupportEquipment** | **String** | AU Market allowable values for Life Support equipment type i.e. \&quot;Oxygen Concentrator\&quot;, \&quot;Intermittent Peritoneal Dialysis Machine\&quot;, \&quot;Kidney Dialysis Machine\&quot;, \&quot;Chronic Positive Airways Pressure Respirator\&quot;, \&quot;Crigler Najjar Syndrome Phototherapy Equipment\&quot;, \&quot;Ventilator For Life Support\&quot;, \&quot;Other\&quot; | 
**lifeSupportStatus** | **String** | AU Market allowable values for Life Support status i.e. \&quot;Registered - No Medical Confirmation\&quot;, \&quot;Registered - Medical Confirmation\&quot;, \&quot;Deregistered - No Medical Confirmation\&quot;, \&quot;Deregistered - Customer Advice\&quot;, \&quot;Deregistered - No Customer Response\&quot;, \&quot;None\&quot; | 
**contactTitle** | **String** | Title of the person who is the contact for the management of Life Support requirements. i.e. \&quot;Ms\&quot;, \&quot;Mrs\&quot;, \&quot;Mr\&quot;, \&quot;Dr\&quot;, \&quot;Prof\&quot; | 
**contactFirstName** | **String** | First name of the person who is the contact for the management of Life Support requirements. | 
**contactLastName** | **String** | Last name of the person who is the contact for the management of Life Support requirements. | 
**phoneNumber** | **String** | Phone number is required when preferred contact method is via Phone | 
**alternatePhoneNumber** | **String** | Alternative contact number | [optional] 
**emailAddress** | **String** | Email address is required when preferred contact method is via Email Address | [optional] 
**preferredContactMethod** | **String** | AU Market allowable values for Contact Method i.e. \&quot;Email Address\&quot;, \&quot;Phone\&quot;, \&quot;Postal Address\&quot;, \&quot;Site Address\&quot; | 
**specialNotes** | **String** | Additional notes for Sensitive Load or Life Support. Mandatory if Life Support equipment is &#39;Other&#39; to provide details | [optional] 


