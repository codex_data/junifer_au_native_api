# JuniferAuNativeApi.BillEmailsGetBillemailsIdResponse200Links1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to this email | 
**bill** | **String** | Link to the bill this email belongs to | 


