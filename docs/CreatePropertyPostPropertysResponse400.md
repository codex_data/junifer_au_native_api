# JuniferAuNativeApi.CreatePropertyPostPropertysResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | The error code | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 


