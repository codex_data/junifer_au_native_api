# JuniferAuNativeApi.CreateAccountCreditPostAccountsIdAccountcreditsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InvalidSalesTax&#x60; - &#39;salesTaxName&#39;is not a valid type of Sales Tax * &#x60;GrossAmountMissing&#x60; - The required &#39;grossAmount&#39; parameter is missing * &#x60;AccountCreditReasonNameMissing&#x60; - The required &#39;accountCreditReasonName&#39; parameter is missing * &#x60;SalesTaxNameMissing&#x60; - The required &#39;salesTaxName&#39; parameter is missing | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InvalidSalesTax` (value: `"InvalidSalesTax"`)

* `GrossAmountMissing` (value: `"GrossAmountMissing"`)

* `AccountCreditReasonNameMissing` (value: `"AccountCreditReasonNameMissing"`)

* `SalesTaxNameMissing` (value: `"SalesTaxNameMissing"`)




