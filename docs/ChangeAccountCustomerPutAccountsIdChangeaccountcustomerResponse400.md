# JuniferAuNativeApi.ChangeAccountCustomerPutAccountsIdChangeaccountcustomerResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;CustomerPrimaryContactEmailsDoNotMatch&#x60; - \&quot;The primary contact for the customer with id &#39;id&#39; has an email address which does not match that of the primary contact for the customer currently linked to the account\&quot; * &#x60;RequiredParameterMissing&#x60; - \&quot;The required &#39;id&#39; field is missing\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `CustomerPrimaryContactEmailsDoNotMatch` (value: `"CustomerPrimaryContactEmailsDoNotMatch"`)

* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)




