# JuniferAuNativeApi.CreatePropertyPostPropertysResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**propertyId** | **Number** | Newly created property ID | 


