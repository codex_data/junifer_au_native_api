# JuniferAuNativeApi.LookUpMeterPointGetAuMeterpointsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Meterpoint id | 
**identifier** | **String** | Meterpoint identifier. For meterpoints of type &#x60;NMI&#x60; this will be NMI identifier. | 
**type** | **String** | Meterpoint type | 
**meterPointServiceType** | **String** | Indicates the service type for a meter point, and whether that service type accommodates automated readings | 
**links** | [**GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links**](GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links.md) |  | 


