# JuniferAuNativeApi.GetProductsForNmiGetAuProductsTariffsfornmiResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;MissingParameters&#x60; - &#x60;nmi&#x60; parameter is missing * &#x60;InternalError&#x60; - NMI discovery request failed | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `MissingParameters` (value: `"MissingParameters"`)

* `InternalError` (value: `"InternalError"`)




