# JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consentGiven** | **Boolean** | Consent has been given (true or false) | 
**methodOfConsent** | **String** | If consent given, how it was given (see MethodOfConsent ref table for possible values) | [optional] 


