# JuniferAuNativeApi.GetPaymentPlanGetPaymentplansIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the payment plan | 
**status** | **String** | indicates the status of the payment plan | 
**createdDttm** | **Date** | DateTime when the payment plan was created | [optional] 
**collectType** | **String** | indicates how payments were collected | 
**currency** | **String** | The currency that payments will use | 
**paymentPlanTransactions** | [**[GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions]**](GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions.md) | PaymentPlanTransactions Object | 
**links** | [**GetPaymentPlanGetPaymentplansIdResponse200Links**](GetPaymentPlanGetPaymentplansIdResponse200Links.md) |  | 


