# JuniferAuNativeApi.GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;MissingParameter&#x60; - &#x60;jurisdictionCode&#x60; lookup parameter is missing * &#x60;InternalError&#x60; - NMI discovery event code is nonzero | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `MissingParameter` (value: `"MissingParameter"`)

* `InternalError` (value: `"InternalError"`)




