# JuniferAuNativeApi.GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to the suggested payment amount | 


