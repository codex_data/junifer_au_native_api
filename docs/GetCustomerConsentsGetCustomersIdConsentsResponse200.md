# JuniferAuNativeApi.GetCustomerConsentsGetCustomersIdConsentsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Consent ID | 
**consentDefinition** | **String** | Consent Definition | 
**setting** | **Boolean** | Consent Setting can be Y or N | 
**fromDt** | **Date** | Consent from date | 
**toDt** | **Date** | Consent to date | [optional] 


