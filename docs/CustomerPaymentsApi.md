# JuniferAuNativeApi.CustomerPaymentsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAuBpointDirectDebitsIdDelete**](CustomerPaymentsApi.md#juniferAuBpointDirectDebitsIdDelete) | **DELETE** /junifer/au/bpointDirectDebits/{id} | Cancel BPOINT Direct Debit Instruction
[**juniferAuBpointDirectDebitsIdGet**](CustomerPaymentsApi.md#juniferAuBpointDirectDebitsIdGet) | **GET** /junifer/au/bpointDirectDebits/{id} | Existing BPOINT Direct Debit Instruction
[**juniferAuBpointDirectDebitsPost**](CustomerPaymentsApi.md#juniferAuBpointDirectDebitsPost) | **POST** /junifer/au/bpointDirectDebits | New BPOINT Direct Debit
[**juniferAuPayPalDirectDebitsIdDelete**](CustomerPaymentsApi.md#juniferAuPayPalDirectDebitsIdDelete) | **DELETE** /junifer/au/payPalDirectDebits/{id} | Cancel PayPal Direct Debit Instruction
[**juniferAuPayPalDirectDebitsIdGet**](CustomerPaymentsApi.md#juniferAuPayPalDirectDebitsIdGet) | **GET** /junifer/au/payPalDirectDebits/{id} | Existing PayPal Direct Debit Instruction
[**juniferAuPayPalDirectDebitsPost**](CustomerPaymentsApi.md#juniferAuPayPalDirectDebitsPost) | **POST** /junifer/au/payPalDirectDebits | New PayPal Direct Debit
[**juniferAuWestpacDirectDebitsIdDelete**](CustomerPaymentsApi.md#juniferAuWestpacDirectDebitsIdDelete) | **DELETE** /junifer/au/westpacDirectDebits/{id} | Cancel Westpac Direct Debit Instruction
[**juniferAuWestpacDirectDebitsIdGet**](CustomerPaymentsApi.md#juniferAuWestpacDirectDebitsIdGet) | **GET** /junifer/au/westpacDirectDebits/{id} | Existing Westpac Direct Debit Instruction
[**juniferAuWestpacDirectDebitsPost**](CustomerPaymentsApi.md#juniferAuWestpacDirectDebitsPost) | **POST** /junifer/au/westpacDirectDebits | New Westpac Direct Debit
[**juniferSepaDirectDebitsIdDelete**](CustomerPaymentsApi.md#juniferSepaDirectDebitsIdDelete) | **DELETE** /junifer/sepaDirectDebits/{id} | Cancel SEPA Direct Debit Instruction
[**juniferSepaDirectDebitsIdGet**](CustomerPaymentsApi.md#juniferSepaDirectDebitsIdGet) | **GET** /junifer/sepaDirectDebits/{id} | Existing SEPA Direct Debit Instruction
[**juniferSepaDirectDebitsIdPut**](CustomerPaymentsApi.md#juniferSepaDirectDebitsIdPut) | **PUT** /junifer/sepaDirectDebits/{id} | Update SEPA Direct Debit Mandate
[**juniferSepaDirectDebitsPost**](CustomerPaymentsApi.md#juniferSepaDirectDebitsPost) | **POST** /junifer/sepaDirectDebits | New SEPA Direct Debit Mandate
[**juniferStripePaymentCardsIdDelete**](CustomerPaymentsApi.md#juniferStripePaymentCardsIdDelete) | **DELETE** /junifer/stripePaymentCards/{id} | Cancel Stripe Payment Card
[**juniferStripePaymentCardsIdGet**](CustomerPaymentsApi.md#juniferStripePaymentCardsIdGet) | **GET** /junifer/stripePaymentCards/{id} | Existing Stripe Payment Card Instruction
[**juniferStripePaymentCardsIdPaymentsPost**](CustomerPaymentsApi.md#juniferStripePaymentCardsIdPaymentsPost) | **POST** /junifer/stripePaymentCards/{id}/payments | Create Stripe Card Payment Collection
[**juniferStripePaymentCardsPost**](CustomerPaymentsApi.md#juniferStripePaymentCardsPost) | **POST** /junifer/stripePaymentCards | Store Stripe Payment Card



## juniferAuBpointDirectDebitsIdDelete

> juniferAuBpointDirectDebitsIdDelete(id)

Cancel BPOINT Direct Debit Instruction

Cancel BPOINT Direct Debit, delete future account payment methods for this Direct Debit

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | BPOINT Direct Debit ID
apiInstance.juniferAuBpointDirectDebitsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| BPOINT Direct Debit ID | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## juniferAuBpointDirectDebitsIdGet

> GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200 juniferAuBpointDirectDebitsIdGet(id)

Existing BPOINT Direct Debit Instruction

Gets a BPOINT Direct Debit mandate by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | BPOINT Direct Debit ID
apiInstance.juniferAuBpointDirectDebitsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| BPOINT Direct Debit ID | 

### Return type

[**GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200**](GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAuBpointDirectDebitsPost

> NewBpointDirectDebitPostAuBpointdirectdebitsResponse200 juniferAuBpointDirectDebitsPost(requestBody)

New BPOINT Direct Debit

Creates a new BPOINT Direct Debit payment method for the account specified in the &#x60;account.id&#x60; field.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let requestBody = new JuniferAuNativeApi.NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody(); // NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody | Request body
apiInstance.juniferAuBpointDirectDebitsPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody**](NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody.md)| Request body | 

### Return type

[**NewBpointDirectDebitPostAuBpointdirectdebitsResponse200**](NewBpointDirectDebitPostAuBpointdirectdebitsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAuPayPalDirectDebitsIdDelete

> juniferAuPayPalDirectDebitsIdDelete(id)

Cancel PayPal Direct Debit Instruction

Cancel PayPal Direct Debit, delete future account payment methods for this Direct Debit

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | PayPal Direct Debit ID
apiInstance.juniferAuPayPalDirectDebitsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| PayPal Direct Debit ID | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## juniferAuPayPalDirectDebitsIdGet

> GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200 juniferAuPayPalDirectDebitsIdGet(id)

Existing PayPal Direct Debit Instruction

Gets a PayPal Direct Debit mandate by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | PayPal Direct Debit ID
apiInstance.juniferAuPayPalDirectDebitsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| PayPal Direct Debit ID | 

### Return type

[**GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200**](GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAuPayPalDirectDebitsPost

> NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200 juniferAuPayPalDirectDebitsPost(requestBody)

New PayPal Direct Debit

Creates a new PayPal Direct Debit payment method for the account specified in the &#x60;account.id&#x60; field.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let requestBody = new JuniferAuNativeApi.NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody(); // NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody | Request body
apiInstance.juniferAuPayPalDirectDebitsPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody**](NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody.md)| Request body | 

### Return type

[**NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200**](NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAuWestpacDirectDebitsIdDelete

> juniferAuWestpacDirectDebitsIdDelete(id)

Cancel Westpac Direct Debit Instruction

Cancel Westpac Direct Debit, delete future account payment methods for this Direct Debit

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | Westpac Direct Debit ID
apiInstance.juniferAuWestpacDirectDebitsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Westpac Direct Debit ID | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## juniferAuWestpacDirectDebitsIdGet

> GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200 juniferAuWestpacDirectDebitsIdGet(id)

Existing Westpac Direct Debit Instruction

Gets a Westpac Direct Debit mandate by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | Westpac Direct Debit ID
apiInstance.juniferAuWestpacDirectDebitsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Westpac Direct Debit ID | 

### Return type

[**GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200**](GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAuWestpacDirectDebitsPost

> NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200 juniferAuWestpacDirectDebitsPost(requestBody)

New Westpac Direct Debit

Creates a new Westpac Direct Debit payment method for the account specified in the &#x60;account.id&#x60; field.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let requestBody = new JuniferAuNativeApi.NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody(); // NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody | Request body
apiInstance.juniferAuWestpacDirectDebitsPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody**](NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody.md)| Request body | 

### Return type

[**NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200**](NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferSepaDirectDebitsIdDelete

> juniferSepaDirectDebitsIdDelete(id)

Cancel SEPA Direct Debit Instruction

Cancel SEPA Direct Debit, delete future account payment methods for this Direct Debit

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | SEPA Direct Debit ID
apiInstance.juniferSepaDirectDebitsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| SEPA Direct Debit ID | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## juniferSepaDirectDebitsIdGet

> GetSepaDirectDebitGetSepadirectdebitsIdResponse200 juniferSepaDirectDebitsIdGet(id)

Existing SEPA Direct Debit Instruction

Gets a SEPA Direct Debit mandate by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | SEPA Direct Debit ID
apiInstance.juniferSepaDirectDebitsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| SEPA Direct Debit ID | 

### Return type

[**GetSepaDirectDebitGetSepadirectdebitsIdResponse200**](GetSepaDirectDebitGetSepadirectdebitsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferSepaDirectDebitsIdPut

> juniferSepaDirectDebitsIdPut(id, requestBody)

Update SEPA Direct Debit Mandate

Updates SEPA Direct Debit details

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | SEPA Direct Debit ID
let requestBody = new JuniferAuNativeApi.UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody(); // UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody | Request body
apiInstance.juniferSepaDirectDebitsIdPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| SEPA Direct Debit ID | 
 **requestBody** | [**UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody**](UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferSepaDirectDebitsPost

> NewSepaDirectDebitPostSepadirectdebitsResponse200 juniferSepaDirectDebitsPost(requestBody)

New SEPA Direct Debit Mandate

Creates a new SEPA Direct Debit instruction for the account specified in the &#x60;account.id&#x60; field.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let requestBody = new JuniferAuNativeApi.NewSepaDirectDebitPostSepadirectdebitsRequestBody(); // NewSepaDirectDebitPostSepadirectdebitsRequestBody | Request body
apiInstance.juniferSepaDirectDebitsPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**NewSepaDirectDebitPostSepadirectdebitsRequestBody**](NewSepaDirectDebitPostSepadirectdebitsRequestBody.md)| Request body | 

### Return type

[**NewSepaDirectDebitPostSepadirectdebitsResponse200**](NewSepaDirectDebitPostSepadirectdebitsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferStripePaymentCardsIdDelete

> juniferStripePaymentCardsIdDelete(id)

Cancel Stripe Payment Card

Cancels Go Stripe Payment Card, delete future account payment methods for this Stripe Payment Card

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | Stripe Payment Card ID
apiInstance.juniferStripePaymentCardsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Stripe Payment Card ID | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## juniferStripePaymentCardsIdGet

> GetStripePaymentCardGetStripepaymentcardsIdResponse200 juniferStripePaymentCardsIdGet(id)

Existing Stripe Payment Card Instruction

Gets a Stripe Payment Card by its ID for further instruction

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | Stripe Payment Card  ID
apiInstance.juniferStripePaymentCardsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Stripe Payment Card  ID | 

### Return type

[**GetStripePaymentCardGetStripepaymentcardsIdResponse200**](GetStripePaymentCardGetStripepaymentcardsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferStripePaymentCardsIdPaymentsPost

> CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200 juniferStripePaymentCardsIdPaymentsPost(id, amount)

Create Stripe Card Payment Collection

Schedules a Stripe Card payment. Authentication must be active

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let id = 3.4; // Number | Stripe Card Payment ID
let amount = 3.4; // Number | Payment collection amount
apiInstance.juniferStripePaymentCardsIdPaymentsPost(id, amount, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Stripe Card Payment ID | 
 **amount** | **Number**| Payment collection amount | 

### Return type

[**CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200**](CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferStripePaymentCardsPost

> juniferStripePaymentCardsPost(requestBody)

Store Stripe Payment Card

Add a new Stripe Payment Card. Authentication must be activeFrom September 14th 2019 onward expMonth, expYear and last4 should not be provided.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomerPaymentsApi();
let requestBody = new JuniferAuNativeApi.StoreCardPostStripepaymentcardsRequestBody(); // StoreCardPostStripepaymentcardsRequestBody | Request body
apiInstance.juniferStripePaymentCardsPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**StoreCardPostStripepaymentcardsRequestBody**](StoreCardPostStripepaymentcardsRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

