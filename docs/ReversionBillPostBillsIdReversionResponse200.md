# JuniferAuNativeApi.ReversionBillPostBillsIdReversionResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newVersionBillRequestId** | **Number** | Id of bill request for reversioned bill | 
**links** | [**ReversionBillPostBillsIdReversionResponse200Links**](ReversionBillPostBillsIdReversionResponse200Links.md) |  | 


