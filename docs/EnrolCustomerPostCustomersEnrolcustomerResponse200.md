# JuniferAuNativeApi.EnrolCustomerPostCustomersEnrolcustomerResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **Number** | Newly created customer ID | 


