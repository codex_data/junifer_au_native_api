# JuniferAuNativeApi.NewBpointDirectDebitPostAuBpointdirectdebitsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mandateReference** | **String** | BPOINT Direct Debit mandate reference | 
**paymentType** | **String** | Payment type. Can be either &#x60;BankAccount&#x60; or &#x60;CreditCard&#x60; | 


