# JuniferAuNativeApi.FinancialsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferActiveSeasonalDefinitionGet**](FinancialsApi.md#juniferActiveSeasonalDefinitionGet) | **GET** /junifer/activeSeasonalDefinition | Get active seasonal definition
[**juniferPaymentMethodsIdGet**](FinancialsApi.md#juniferPaymentMethodsIdGet) | **GET** /junifer/paymentMethods/{id} | Get payment method
[**juniferPaymentPlansIdGet**](FinancialsApi.md#juniferPaymentPlansIdGet) | **GET** /junifer/paymentPlans/{id} | Get payment plan
[**juniferPaymentRequestsIdGet**](FinancialsApi.md#juniferPaymentRequestsIdGet) | **GET** /junifer/paymentRequests/{id} | Get payment request
[**juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet**](FinancialsApi.md#juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet) | **GET** /junifer/paymentSchedulePeriods/calculateNextPaymentDate | Calculate next payment schedule collection date
[**juniferPaymentSchedulePeriodsIdGet**](FinancialsApi.md#juniferPaymentSchedulePeriodsIdGet) | **GET** /junifer/paymentSchedulePeriods/{id} | Get payment schedule period
[**juniferPaymentSchedulePeriodsIdPut**](FinancialsApi.md#juniferPaymentSchedulePeriodsIdPut) | **PUT** /junifer/paymentSchedulePeriods/{id} | Stop payment schedule period
[**juniferPaymentSchedulePeriodsPaymentScheduleItemsGet**](FinancialsApi.md#juniferPaymentSchedulePeriodsPaymentScheduleItemsGet) | **GET** /junifer/paymentSchedulePeriods/paymentScheduleItems | Get account&#39;s payment schedule items
[**juniferPaymentSchedulePeriodsPost**](FinancialsApi.md#juniferPaymentSchedulePeriodsPost) | **POST** /junifer/paymentSchedulePeriods | Create new payment schedule period
[**juniferPaymentsIdGet**](FinancialsApi.md#juniferPaymentsIdGet) | **GET** /junifer/payments/{id} | Get payment



## juniferActiveSeasonalDefinitionGet

> GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200 juniferActiveSeasonalDefinitionGet()

Get active seasonal definition

Gets the currently active seasonal definition for seasonally-variable Direct Debit payments

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
apiInstance.juniferActiveSeasonalDefinitionGet((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200**](GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferPaymentMethodsIdGet

> GetPaymentMethodGetPaymentmethodsIdResponse200 juniferPaymentMethodsIdGet(id)

Get payment method

Get payment method by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
let id = 3.4; // Number | Payment method ID
apiInstance.juniferPaymentMethodsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Payment method ID | 

### Return type

[**GetPaymentMethodGetPaymentmethodsIdResponse200**](GetPaymentMethodGetPaymentmethodsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferPaymentPlansIdGet

> GetPaymentPlanGetPaymentplansIdResponse200 juniferPaymentPlansIdGet(id)

Get payment plan

Get payment plan by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
let id = 3.4; // Number | Payment plan ID
apiInstance.juniferPaymentPlansIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Payment plan ID | 

### Return type

[**GetPaymentPlanGetPaymentplansIdResponse200**](GetPaymentPlanGetPaymentplansIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferPaymentRequestsIdGet

> GetPaymentRequestGetPaymentrequestsIdResponse200 juniferPaymentRequestsIdGet(id)

Get payment request

Get payment request by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
let id = 3.4; // Number | Payment plan ID
apiInstance.juniferPaymentRequestsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Payment plan ID | 

### Return type

[**GetPaymentRequestGetPaymentrequestsIdResponse200**](GetPaymentRequestGetPaymentrequestsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet

> juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet(frequency, multiple, startDttm, alignmentDttm)

Calculate next payment schedule collection date

Calculates and returns next payment schedule date based on the parameters supplied

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
let frequency = "frequency_example"; // String | The name of the frequency indicating when a payment should be collected, e.g. `Monthly, Weekly, Daily`
let multiple = 3.4; // Number | Frequency multiplier
let startDttm = "startDttm_example"; // String | Payment schedule period start date
let alignmentDttm = "alignmentDttm_example"; // String | Alignment date
apiInstance.juniferPaymentSchedulePeriodsCalculateNextPaymentDateGet(frequency, multiple, startDttm, alignmentDttm, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **frequency** | **String**| The name of the frequency indicating when a payment should be collected, e.g. &#x60;Monthly, Weekly, Daily&#x60; | 
 **multiple** | **Number**| Frequency multiplier | 
 **startDttm** | **String**| Payment schedule period start date | 
 **alignmentDttm** | **String**| Alignment date | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferPaymentSchedulePeriodsIdGet

> GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200 juniferPaymentSchedulePeriodsIdGet(id)

Get payment schedule period

Get payment schedule period by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
let id = 3.4; // Number | Payment schedule period ID
apiInstance.juniferPaymentSchedulePeriodsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Payment schedule period ID | 

### Return type

[**GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200**](GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferPaymentSchedulePeriodsIdPut

> juniferPaymentSchedulePeriodsIdPut(id, requestBody)

Stop payment schedule period

Stops payment schedule period. Exactly the same JSON structure can be passed as retrieved in Get payment schedule period, but with &#x60;toDt&#x60; field added. This method will set &#x60;PaymentSchedulePeriod.StoppedDttm&#x60; to Now.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
let id = 3.4; // Number | Payment schedule ID
let requestBody = new JuniferAuNativeApi.StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody(); // StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody | Request body
apiInstance.juniferPaymentSchedulePeriodsIdPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Payment schedule ID | 
 **requestBody** | [**StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody**](StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferPaymentSchedulePeriodsPaymentScheduleItemsGet

> GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200 juniferPaymentSchedulePeriodsPaymentScheduleItemsGet(accountId)

Get account&#39;s payment schedule items

Get payment schedule items for a given account ID. If no payment schedules can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
let accountId = 3.4; // Number | Account ID
apiInstance.juniferPaymentSchedulePeriodsPaymentScheduleItemsGet(accountId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **Number**| Account ID | 

### Return type

[**GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200**](GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferPaymentSchedulePeriodsPost

> CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200 juniferPaymentSchedulePeriodsPost(requestBody)

Create new payment schedule period

Creates a new payment schedule period.  Will raise a ticket, if one is specified in the property Payment Schedule Period Properties &gt; API Payment Schedule Period Change Ticket.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
let requestBody = new JuniferAuNativeApi.CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody(); // CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody | Request body
apiInstance.juniferPaymentSchedulePeriodsPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody**](CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody.md)| Request body | 

### Return type

[**CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200**](CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferPaymentsIdGet

> GetPaymentGetPaymentsIdResponse200 juniferPaymentsIdGet(id)

Get payment

Gets payment by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.FinancialsApi();
let id = 3.4; // Number | Payment ID
apiInstance.juniferPaymentsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Payment ID | 

### Return type

[**GetPaymentGetPaymentsIdResponse200**](GetPaymentGetPaymentsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

