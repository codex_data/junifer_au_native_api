# JuniferAuNativeApi.GetAccountTransactionsGetAccountsIdTransactionsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Account transaction identifier | 
**acceptedDttm** | **Date** | Date and time the transaction was accepted | 
**status** | **String** | Status of the single transaction - Pending, Accepted, Rejected, Authorising | 
**createdDttm** | **Date** | Date and time the transaction was created | 
**dueDt** | **Date** | Date that transaction should be paid before | [optional] 
**type** | **String** | Transaction type | 
**description** | **String** | Transaction description | [optional] 
**currency** | **String** | Currency transaction was made in | 
**currencyISO** | **String** | Currency ISO code | 
**debitAmount** | **Number** | The amount the account was debited by the transaction | 
**debitBalance** | **Number** | Unpaid balance of this transaction | [optional] 
**creditAmount** | **Number** | The amount that the account was credited by the transaction | [optional] 
**creditBalance** | **Number** | Unallocated balance of this transaction | [optional] 
**billId** | **Number** | The id of the bill linked to the transaction | [optional] 
**accountBalance** | **Number** | The account balance after the transaction was applied | [optional] 
**createdUser** | **String** | The name of the user who created the transaction. | 
**reference** | **String** | Transaction reference | 
**cancelledDttm** | **Date** | The date when the transaction was cancelled | [optional] 
**cancelAccountTransactionId** | **Number** | ID of the transaction that this transaction is cancelling. The referenced transaction will have cancelled date/time. | [optional] 
**links** | [**GetAccountTransactionsGetAccountsIdTransactionsResponse200Links**](GetAccountTransactionsGetAccountsIdTransactionsResponse200Links.md) |  | [optional] 
**orderNo** | **Number** | Order in which the transaction was accepted, null if not yet accepted | [optional] 


