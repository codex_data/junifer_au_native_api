# JuniferAuNativeApi.GetAccountCommunicationsGetAccountsIdCommunicationsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InvalidParameter&#x60; - The inputted value &#39;statusString&#39; is not a valid communication status. Valid values are &#39;xxxx,xxx, ...&#39; (a list of communicationStatus values here) | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InvalidParameter` (value: `"InvalidParameter"`)




