# JuniferAuNativeApi.NewSepaDirectDebitPostSepadirectdebitsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | SEPA Direct Debit object id | 
**accountName** | **String** | Account name | 
**iban** | **String** | IBAN, e.g. IE29AIBK93115212345678 | 
**mandateReference** | **String** | SEPA Direct Debit mandate reference | 
**authorisedDttm** | **Date** | Date when the mandate was authorised | [optional] 
**terminatedDttm** | **Date** | Date when the mandate was terminated | [optional] 
**links** | [**NewSepaDirectDebitPostSepadirectdebitsResponse200Links**](NewSepaDirectDebitPostSepadirectdebitsResponse200Links.md) |  | 


