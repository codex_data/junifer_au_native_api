# JuniferAuNativeApi.CreatePaymentSchedulePeriodPostPaymentscheduleperiodsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromDt** | **Date** | Date when the payment schedule period should start | 
**toDt** | **Date** | Date when the payment schedule period should terminate | [optional] 
**frequency** | **String** | The name of the frequency indicating when a payment should be collected, e.g. &#x60;Monthly, Weekly, Daily&#x60; | 
**frequencyMultiple** | **Number** | Frequency multiplier | 
**frequencyAlignmentDt** | **Date** | Date to align/pivot the payments | 
**seasonalPaymentFl** | **Boolean** | Flag indicating if the created payment schedule should be seasonal | 
**amount** | **Number** | Decimal amount which should customer should be charged (2 dp) | 


