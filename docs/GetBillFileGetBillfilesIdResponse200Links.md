# JuniferAuNativeApi.GetBillFileGetBillfilesIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to this file | 
**image** | **String** | Link to associated bill file image | 


