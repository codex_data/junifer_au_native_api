# JuniferAuNativeApi.GetAccountCreditNotesGetAccountsIdCreditsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The id of credit | 
**number** | **String** | The unique credit number | 
**createdDttm** | **Date** | Date and time when the credit was created within Junifer | 
**status** | **String** | Status of the credit | 
**authorisedDttm** | **Date** | Datetime the credit was authorised | [optional] 
**currency** | **String** | Currency of the credit | 
**grossAmount** | **Number** | Gross amount shown on the credit | 
**netAmount** | **Number** | Net amount shown on the credit | 
**salesTaxAmount** | **Number** | Sales tax amount shown on the credit | 
**issueDt** | **Date** | Date the credit was issued o | 
**links** | [**GetAccountCreditNotesGetAccountsIdCreditsResponse200Links**](GetAccountCreditNotesGetAccountsIdCreditsResponse200Links.md) |  | 


