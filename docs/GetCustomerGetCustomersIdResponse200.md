# JuniferAuNativeApi.GetCustomerGetCustomersIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Customer ID | 
**name** | **String** | Customer&#39;s full name | 
**number** | **String** | Customer number | 
**customerClass** | **String** | Customer class this customer belongs to | 
**customerType** | **String** | Customer type | 
**state** | **String** | Customer state. Possible values include &#x60;Prospect, Active, Terminated&#x60; | 
**title** | **String** | Customer&#39;s title | [optional] 
**forename** | **String** | Customer&#39;s first name | [optional] 
**surname** | **String** | Customer&#39;s last name | [optional] 
**companyName** | **String** | Customer&#39;s company name | [optional] 
**companyNumber** | **String** | Customer&#39;s company number | [optional] 
**companyAddress** | [**GetCustomerGetCustomersIdResponse200CompanyAddress**](GetCustomerGetCustomersIdResponse200CompanyAddress.md) |  | [optional] 
**bereavementFl** | **Boolean** | Is the Customer&#39;s Deceased | 
**marketingOptOutFl** | **Boolean** | Customer&#39;s choice for receiving marketing communications | 
**creditScore** | **Number** | Customer&#39;s credit score | [optional] 
**taxExemptReason** | **String** | Customer&#39;s tax exempt reason if applicable | [optional] 
**primaryContact** | [**GetCustomerGetCustomersIdResponse200PrimaryContact**](GetCustomerGetCustomersIdResponse200PrimaryContact.md) |  | 
**links** | [**GetCustomerGetCustomersIdResponse200Links**](GetCustomerGetCustomersIdResponse200Links.md) |  | 


