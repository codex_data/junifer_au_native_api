# JuniferAuNativeApi.AusCreateConcessionPostAuConcessionsCreateconcessionResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**concessionId** | **Number** | Newly created concession ID | 


