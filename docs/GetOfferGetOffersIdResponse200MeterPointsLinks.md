# JuniferAuNativeApi.GetOfferGetOffersIdResponse200MeterPointsLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | link to the meterpoint itself. | 


