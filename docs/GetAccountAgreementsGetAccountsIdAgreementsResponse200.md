# JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the agreement | 
**number** | **String** | Agreement number | 
**fromDt** | **Date** | The start date of the agreement | 
**orderedDt** | **Date** | Agreement creation date | 
**toDt** | **Date** | Agreement end date (exclusive) | [optional] 
**cancelled** | **Boolean** | A flag indicating whether this agreement has been cancelled | [optional] 
**links** | [**GetAccountAgreementsGetAccountsIdAgreementsResponse200Links**](GetAccountAgreementsGetAccountsIdAgreementsResponse200Links.md) |  | 
**products** | [**[GetAccountAgreementsGetAccountsIdAgreementsResponse200Products]**](GetAccountAgreementsGetAccountsIdAgreementsResponse200Products.md) | Products present in the agreement | 
**agreementType** | [**GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType**](GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType.md) |  | [optional] 


