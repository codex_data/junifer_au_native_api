# JuniferAuNativeApi.GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | ID of Bill Breakdown Line | 
**description** | **String** | Description of the Bill Breakdown Line | 
**currency** | **String** | Currency ISO code | 
**billCurrencyAmount** | **Number** | Bill Breakdown amount | 
**salesTaxRate** | **Number** | Sales Tax rate for the Bill Breakdown | 
**code** | **String** | Code for the Bill breakdown Line | 
**linkage** | **String** | Identifier for linked Asset or Property&#39;s address | 


