# JuniferAuNativeApi.GetPaymentRequestGetPaymentrequestsIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **String** | Link to the account | 


