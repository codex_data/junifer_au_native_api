# JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the asset | 
**links** | [**GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1**](GetAccountAgreementsGetAccountsIdAgreementsResponse200Links1.md) |  | 


