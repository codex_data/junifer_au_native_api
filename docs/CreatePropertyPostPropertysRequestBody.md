# JuniferAuNativeApi.CreatePropertyPostPropertysRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parentId** | **Number** | ID of parent property | [optional] 
**rootPropertyId** | **Number** | ID of the root property | [optional] 
**identifier** | **String** | Identifier of the property | [optional] 
**reference** | **String** | Property Reference | [optional] 
**propertyType** | **String** | The type of the property. This will be one of the values in the \&quot;Property Type\&quot; ref table. | [optional] 


