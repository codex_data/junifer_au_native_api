# JuniferAuNativeApi.UpdateCustomerPutCustomersIdRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Customer ID (This field is ignored) | 
**name** | **String** | Customer&#39;s full name (This field is ignored) | 
**number** | **String** | Customer number (This field is ignored) | 
**customerClass** | **String** | Customer class this customer belongs to (This field is ignored) | 
**title** | **String** | Customer&#39;s title | [optional] 
**forename** | **String** | Customer&#39;s first name | [optional] 
**surname** | **String** | Customer&#39;s last name | [optional] 
**marketingOptOutFl** | **Boolean** | Customer&#39;s choice for receiving marketing communications | 
**companyName** | **String** | Customer&#39;s company name (This field is ignored) | [optional] 
**companyNumber** | **String** | Customer&#39;s company number (This field is ignored) | [optional] 
**primaryContact** | [**UpdateCustomerPutCustomersIdRequestBodyPrimaryContact**](UpdateCustomerPutCustomersIdRequestBodyPrimaryContact.md) |  | 


