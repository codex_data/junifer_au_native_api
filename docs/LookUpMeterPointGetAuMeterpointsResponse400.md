# JuniferAuNativeApi.LookUpMeterPointGetAuMeterpointsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;RequiredParameterMissing&#x60; - The required &#39;queryDttm&#39; lookup parameter is missing | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)




