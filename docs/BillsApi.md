# JuniferAuNativeApi.BillsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferBillEmailsIdGet**](BillsApi.md#juniferBillEmailsIdGet) | **GET** /junifer/billEmails/{id} | Get bill email
[**juniferBillFilesIdGet**](BillsApi.md#juniferBillFilesIdGet) | **GET** /junifer/billFiles/{id} | Get bill file
[**juniferBillFilesIdImageGet**](BillsApi.md#juniferBillFilesIdImageGet) | **GET** /junifer/billFiles/{id}/image | Get bill file image
[**juniferBillsIdAcceptDraftPost**](BillsApi.md#juniferBillsIdAcceptDraftPost) | **POST** /junifer/bills/{id}/acceptDraft | Accept Draft bill
[**juniferBillsIdBillBreakdownLinesGet**](BillsApi.md#juniferBillsIdBillBreakdownLinesGet) | **GET** /junifer/bills/{id}/billBreakdownLines | Get bill&#39;s breakdown lines
[**juniferBillsIdGet**](BillsApi.md#juniferBillsIdGet) | **GET** /junifer/bills/{id} | Get bill
[**juniferBillsIdReversionPost**](BillsApi.md#juniferBillsIdReversionPost) | **POST** /junifer/bills/{id}/reversion | Reversion bill



## juniferBillEmailsIdGet

> BillEmailsGetBillemailsIdResponse200 juniferBillEmailsIdGet(id)

Get bill email

Gets the email associated with a bill

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillsApi();
let id = 3.4; // Number | Bill ID (not email id)
apiInstance.juniferBillEmailsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Bill ID (not email id) | 

### Return type

[**BillEmailsGetBillemailsIdResponse200**](BillEmailsGetBillemailsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBillFilesIdGet

> GetBillFileGetBillfilesIdResponse200 juniferBillFilesIdGet(id)

Get bill file

Get bill file record by its ID.  Note that the ID is of the bill file, NOT the bill. To get the correct bill files follow the link URL in the response of e.g. Get Bill API, where the correct bill file ID is prepopulated in the URL by the system.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillsApi();
let id = 3.4; // Number | Bill file ID
apiInstance.juniferBillFilesIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Bill file ID | 

### Return type

[**GetBillFileGetBillfilesIdResponse200**](GetBillFileGetBillfilesIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBillFilesIdImageGet

> GetBillFileImageGetBillfilesIdImageResponse200 juniferBillFilesIdImageGet(id)

Get bill file image

Initiates bill file image (usually PDF file) download if the file is available

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillsApi();
let id = 3.4; // Number | Bill file ID
apiInstance.juniferBillFilesIdImageGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Bill file ID | 

### Return type

[**GetBillFileImageGetBillfilesIdImageResponse200**](GetBillFileImageGetBillfilesIdImageResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBillsIdAcceptDraftPost

> juniferBillsIdAcceptDraftPost(id, opts)

Accept Draft bill

Accept Draft bill

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillsApi();
let id = 3.4; // Number | Bill ID to accept
let opts = {
  'closeAssociatedTicketsFl': true // Boolean | Flag to close tickets associated to the bill when it is accepted. Defaults to false if not set
};
apiInstance.juniferBillsIdAcceptDraftPost(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Bill ID to accept | 
 **closeAssociatedTicketsFl** | **Boolean**| Flag to close tickets associated to the bill when it is accepted. Defaults to false if not set | [optional] 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBillsIdBillBreakdownLinesGet

> GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200 juniferBillsIdBillBreakdownLinesGet(id)

Get bill&#39;s breakdown lines

Get bill&#39;s breakdown lines by its bill ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillsApi();
let id = 3.4; // Number | Bill ID
apiInstance.juniferBillsIdBillBreakdownLinesGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Bill ID | 

### Return type

[**GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200**](GetBillBreakdownLinesGetBillsIdBillbreakdownlinesResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBillsIdGet

> GetBillGetBillsIdResponse200 juniferBillsIdGet(id, opts)

Get bill

Get bill record by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillsApi();
let id = 3.4; // Number | Bill ID
let opts = {
  'contactId': 3.4 // Number | Restrict billFiles to a specific contact
};
apiInstance.juniferBillsIdGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Bill ID | 
 **contactId** | **Number**| Restrict billFiles to a specific contact | [optional] 

### Return type

[**GetBillGetBillsIdResponse200**](GetBillGetBillsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBillsIdReversionPost

> ReversionBillPostBillsIdReversionResponse200 juniferBillsIdReversionPost(id, opts)

Reversion bill

Create new version of bill

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillsApi();
let id = 3.4; // Number | Bill ID
let opts = {
  'consolidateAllClosedBillPeriodsFl': true, // Boolean | Flag to consolidate all Closed bill periods onto this bill period. Defaults to false if not specified
  'consolidateAllOpenBillPeriodsFl': true // Boolean | Flag to consolidate all Open bill periods onto this bill period. Defaults to false if not specified
};
apiInstance.juniferBillsIdReversionPost(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Bill ID | 
 **consolidateAllClosedBillPeriodsFl** | **Boolean**| Flag to consolidate all Closed bill periods onto this bill period. Defaults to false if not specified | [optional] 
 **consolidateAllOpenBillPeriodsFl** | **Boolean**| Flag to consolidate all Open bill periods onto this bill period. Defaults to false if not specified | [optional] 

### Return type

[**ReversionBillPostBillsIdReversionResponse200**](ReversionBillPostBillsIdReversionResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

