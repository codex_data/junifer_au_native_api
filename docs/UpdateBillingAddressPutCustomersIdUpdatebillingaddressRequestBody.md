# JuniferAuNativeApi.UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address1** | **String** | Address line 1 | [optional] 
**address2** | **String** | Address line 2 | [optional] 
**address3** | **String** | Address line 3 | [optional] 
**address4** | **String** | Address line 4 | [optional] 
**address5** | **String** | Address line 5 | [optional] 
**postCode** | **String** | Address post code | [optional] 
**country** | **String** | Address Country | [optional] 


