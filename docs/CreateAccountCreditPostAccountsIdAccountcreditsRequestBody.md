# JuniferAuNativeApi.CreateAccountCreditPostAccountsIdAccountcreditsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grossAmount** | **Number** | Gross amount of account credit | 
**salesTaxName** | **String** | Name of linked sales tax of account credit. Providing &#x60;\&quot;None\&quot;&#x60; will apply no sales tax to the account credit. | 
**accountCreditReasonName** | **String** | Name of linked account credit reason for account credit | 
**reference** | **String** | Text identifier for the account debit | [optional] 
**description** | **String** | Description of the account transaction linked to the account credit | [optional] 


