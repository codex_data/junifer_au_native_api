# JuniferAuNativeApi.CreateProspectPutCustomersIdCreateprospectResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prospectId** | **Number** | Newly created prospect ID | 


