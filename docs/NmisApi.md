# JuniferAuNativeApi.NmisApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAuNmisStandingDataForNMIGet**](NmisApi.md#juniferAuNmisStandingDataForNMIGet) | **GET** /junifer/au/nmis/standingDataForNMI | Get NMIs including standing data



## juniferAuNmisStandingDataForNMIGet

> GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200 juniferAuNmisStandingDataForNMIGet(jurisdictionCode, opts)

Get NMIs including standing data

Returns a list of NMIs including standing data for the NMI details given.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.NmisApi();
let jurisdictionCode = "jurisdictionCode_example"; // String | The jurisdiction the NMI falls under
let opts = {
  'meterSerial': "meterSerial_example", // String | The serial number of the meter
  'dpid': "dpid_example", // String | The Delivery Point Identifier for the address the meter is at
  'buildingOrPropertyName': "buildingOrPropertyName_example", // String | Property name for the address the meter is located at
  'locationDescriptor': "locationDescriptor_example", // String | Location descriptor for the address the meter is located at
  'lotNumber': "lotNumber_example", // String | Lot number for the address the meter is located at
  'flatOrUnitType': "flatOrUnitType_example", // String | Unit type for the address the meter is located at
  'flatOrUnitNumber': "flatOrUnitNumber_example", // String | Unit number for the address the meter is located at
  'floorOrLevelType': "floorOrLevelType_example", // String | Floor type for the address the meter is located at
  'floorOrLevelNumber': "floorOrLevelNumber_example", // String | Floor number for the address the meter is located at
  'houseNumber': "houseNumber_example", // String | House number for the address the meter is located at
  'houseNumberSuffix': "houseNumberSuffix_example", // String | House suffix for the address the meter is located at
  'streetName': "streetName_example", // String | Street name for the address the meter is located at
  'streetSuffix': "streetSuffix_example", // String | Street suffix for the address the meter is located at
  'streetType': "streetType_example", // String | Street type for the address the meter is located at
  'suburbOrPlaceOrLocality': "suburbOrPlaceOrLocality_example", // String | Suburb for the address the meter is located at
  'postcode': "postcode_example", // String | Postcode of the address the meter is located at
  'stateOrTerritory': "stateOrTerritory_example" // String | State for the address the meter is located at
};
apiInstance.juniferAuNmisStandingDataForNMIGet(jurisdictionCode, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jurisdictionCode** | **String**| The jurisdiction the NMI falls under | 
 **meterSerial** | **String**| The serial number of the meter | [optional] 
 **dpid** | **String**| The Delivery Point Identifier for the address the meter is at | [optional] 
 **buildingOrPropertyName** | **String**| Property name for the address the meter is located at | [optional] 
 **locationDescriptor** | **String**| Location descriptor for the address the meter is located at | [optional] 
 **lotNumber** | **String**| Lot number for the address the meter is located at | [optional] 
 **flatOrUnitType** | **String**| Unit type for the address the meter is located at | [optional] 
 **flatOrUnitNumber** | **String**| Unit number for the address the meter is located at | [optional] 
 **floorOrLevelType** | **String**| Floor type for the address the meter is located at | [optional] 
 **floorOrLevelNumber** | **String**| Floor number for the address the meter is located at | [optional] 
 **houseNumber** | **String**| House number for the address the meter is located at | [optional] 
 **houseNumberSuffix** | **String**| House suffix for the address the meter is located at | [optional] 
 **streetName** | **String**| Street name for the address the meter is located at | [optional] 
 **streetSuffix** | **String**| Street suffix for the address the meter is located at | [optional] 
 **streetType** | **String**| Street type for the address the meter is located at | [optional] 
 **suburbOrPlaceOrLocality** | **String**| Suburb for the address the meter is located at | [optional] 
 **postcode** | **String**| Postcode of the address the meter is located at | [optional] 
 **stateOrTerritory** | **String**| State for the address the meter is located at | [optional] 

### Return type

[**GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200**](GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

