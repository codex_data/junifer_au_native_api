# JuniferAuNativeApi.UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse404

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;NotFound&#x60; - The methodOfConsent value &#39;Telephone&#39; was not found | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `NotFound` (value: `"NotFound"`)




