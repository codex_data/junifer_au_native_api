# JuniferAuNativeApi.GetCreditGetCreditsIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this Credit | 
**account** | **String** | Link to the associated account | 


