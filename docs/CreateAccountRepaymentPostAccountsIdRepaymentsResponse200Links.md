# JuniferAuNativeApi.CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **String** | Link to the account that the payment request refers to | 


