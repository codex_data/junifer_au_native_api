# JuniferAuNativeApi.GetAccountDebitsGetAccountsIdAccountdebitsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Account debit Identifier | 
**createdDttm** | **Date** | Date and time when the debit was created | 
**acceptedDttm** | **Date** | Date and time when the debit was accepted | 
**grossAmount** | **Number** | Gross amount of account debit | 
**netAmount** | **Number** | Net amount of account debit | 
**salesTax** | **String** | Name of linked sales tax to this account debit | 
**salesTaxAmount** | **Number** | Amount of sales tax for this account debit | 
**reason** | **String** | Name of linked account debit reason | 
**reference** | **String** | Custom text reference for account debit | 
**cancelledDttm** | **Date** | Date and time when the account debit was cancelled | [optional] 


