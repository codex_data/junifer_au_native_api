# JuniferAuNativeApi.GetBillGetBillsIdResponse200BillFiles

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Bill file ID | 
**display** | **String** | Bill file type | 
**viewable** | **Boolean** | Flag indicating whether the bill file can be viewed | 
**links** | [**GetBillGetBillsIdResponse200Links1**](GetBillGetBillsIdResponse200Links1.md) |  | 


