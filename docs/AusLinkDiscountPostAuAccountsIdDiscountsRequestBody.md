# JuniferAuNativeApi.AusLinkDiscountPostAuAccountsIdDiscountsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discountDfnId** | **String** | account discount definition ID (required if no name given) | 
**discountDfnName** | **String** | account discount definition name (required if no ID given) | 
**fromDt** | **Date** | from date of the account discount | 
**toDt** | **Date** | to date of the account discount (open-ended if not given) | [optional] 


