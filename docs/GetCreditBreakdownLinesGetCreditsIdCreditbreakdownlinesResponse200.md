# JuniferAuNativeApi.GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | ID of credit Breakdown Line | 
**description** | **String** | Description of the credit Breakdown Line | 
**currency** | **String** | Currency ISO code | 
**billCurrencyAmount** | **Number** | credit Breakdown amount | 
**salesTaxRate** | **Number** | Sales Tax rate for the credit Breakdown | 
**linkage** | **String** | Identifier for linked Asset or Property&#39;s address | 


