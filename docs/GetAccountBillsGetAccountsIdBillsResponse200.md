# JuniferAuNativeApi.GetAccountBillsGetAccountsIdBillsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The id of bill | 
**billPeriodId** | **Number** | The id of bill Period | [optional] 
**number** | **String** | The unique bill number | 
**createdDttm** | **Date** | Date and time when the bill was created within Junifer | 
**status** | **String** | Status of the Bill | 
**draftReason** | **String** | A description of why the bill was put into draft, such as suspicious data or default behaviours | [optional] 
**acceptedDttm** | **Date** | Datetime the bill was accepted | [optional] 
**currency** | **String** | Currency of the bill | 
**grossAmount** | **Number** | Gross amount shown on the bill | 
**netAmount** | **Number** | Net amount shown on the bill | 
**salesTaxAmount** | **Number** | Sales tax amount shown on the bill | 
**lastShownAccountTransactionId** | **Number** | The latest account transaction that was included in this Bill&#39;s brought forward balance statement | [optional] 
**periodFrom** | **Date** | The start date of the billed period | [optional] 
**periodTo** | **Date** | The end date of the billed period | [optional] 
**issueDt** | **Date** | Date the bill was issued on | 
**dueDt** | **Date** | Date the bill was due on | [optional] 
**lessThanMinimumFl** | **Boolean** | Set to true if this Bill did not meet the &#39;minimum bill amount&#39; specified for an Account | [optional] 
**supersededFl** | **Boolean** | Flag indicating if the bill was superseded | 
**supersededByBillId** | **Number** | Id of the Bill that has been replaced this version | [optional] 
**_final** | **Boolean** | Flag indicating if this bill is final | 
**dirtyBillFl** | **Boolean** | Flag indicating if this bill is dirty | 
**consolidatedBillFl** | **Boolean** | Flag indicating if this bill is consolidated | [optional] 
**versionNumber** | **Number** | Bill&#39;s version number | 
**balance** | **Number** | Bill&#39;s balance | 
**links** | [**GetAccountBillsGetAccountsIdBillsResponse200Links**](GetAccountBillsGetAccountsIdBillsResponse200Links.md) |  | 


