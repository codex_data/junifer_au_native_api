# JuniferAuNativeApi.CustomersApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferCustomersEnrolCustomerPost**](CustomersApi.md#juniferCustomersEnrolCustomerPost) | **POST** /junifer/customers/enrolCustomer | Enrol customer
[**juniferCustomersGet**](CustomersApi.md#juniferCustomersGet) | **GET** /junifer/customers | Customer lookup
[**juniferCustomersIdAccountsGet**](CustomersApi.md#juniferCustomersIdAccountsGet) | **GET** /junifer/customers/{id}/accounts | Get customer&#39;s accounts
[**juniferCustomersIdBillingEntitiesGet**](CustomersApi.md#juniferCustomersIdBillingEntitiesGet) | **GET** /junifer/customers/{id}/billingEntities | Get customer&#39;s billing entities
[**juniferCustomersIdConsentsGet**](CustomersApi.md#juniferCustomersIdConsentsGet) | **GET** /junifer/customers/{id}/consents | Get customer&#39;s consents
[**juniferCustomersIdConsentsPut**](CustomersApi.md#juniferCustomersIdConsentsPut) | **PUT** /junifer/customers/{id}/consents | Update customer&#39;s consents
[**juniferCustomersIdContactsContactIdPut**](CustomersApi.md#juniferCustomersIdContactsContactIdPut) | **PUT** /junifer/customers/{id}/contacts/{contactId} | Update customer&#39;s contact details
[**juniferCustomersIdContactsGet**](CustomersApi.md#juniferCustomersIdContactsGet) | **GET** /junifer/customers/{id}/contacts | Contacts
[**juniferCustomersIdCreateProspectPut**](CustomersApi.md#juniferCustomersIdCreateProspectPut) | **PUT** /junifer/customers/{id}/createProspect | Create Prospect
[**juniferCustomersIdEnrolAdditionalAccountPost**](CustomersApi.md#juniferCustomersIdEnrolAdditionalAccountPost) | **POST** /junifer/customers/{id}/enrolAdditionalAccount | Enrol a new account to an existing customer
[**juniferCustomersIdGet**](CustomersApi.md#juniferCustomersIdGet) | **GET** /junifer/customers/{id} | Get customer
[**juniferCustomersIdPrimaryContactPut**](CustomersApi.md#juniferCustomersIdPrimaryContactPut) | **PUT** /junifer/customers/{id}/primaryContact | Update customer&#39;s primary contact details
[**juniferCustomersIdProspectsGet**](CustomersApi.md#juniferCustomersIdProspectsGet) | **GET** /junifer/customers/{id}/prospects | Get customer&#39;s Prospects
[**juniferCustomersIdPut**](CustomersApi.md#juniferCustomersIdPut) | **PUT** /junifer/customers/{id} | Update customer
[**juniferCustomersIdUpdateBillingAddressPut**](CustomersApi.md#juniferCustomersIdUpdateBillingAddressPut) | **PUT** /junifer/customers/{id}/updateBillingAddress | Update customer&#39;s Billing Address.



## juniferCustomersEnrolCustomerPost

> EnrolCustomerPostCustomersEnrolcustomerResponse200 juniferCustomersEnrolCustomerPost(requestBody)

Enrol customer

Enrols customer for electricity and/or gas product. This is a Great Britain specific API.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let requestBody = new JuniferAuNativeApi.EnrolCustomerPostCustomersEnrolcustomerRequestBody(); // EnrolCustomerPostCustomersEnrolcustomerRequestBody | Request body
apiInstance.juniferCustomersEnrolCustomerPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**EnrolCustomerPostCustomersEnrolcustomerRequestBody**](EnrolCustomerPostCustomersEnrolcustomerRequestBody.md)| Request body | 

### Return type

[**EnrolCustomerPostCustomersEnrolcustomerResponse200**](EnrolCustomerPostCustomersEnrolcustomerResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferCustomersGet

> LookUpCustomerGetCustomersResponse200 juniferCustomersGet(customerNumber)

Customer lookup

Searches for a customer by the specified customer number. If no customer can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let customerNumber = 3.4; // Number | Customer number
apiInstance.juniferCustomersGet(customerNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerNumber** | **Number**| Customer number | 

### Return type

[**LookUpCustomerGetCustomersResponse200**](LookUpCustomerGetCustomersResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCustomersIdAccountsGet

> GetCustomerAccountsGetCustomersIdAccountsResponse200 juniferCustomersIdAccountsGet(id)

Get customer&#39;s accounts

List customer accounts

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
apiInstance.juniferCustomersIdAccountsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 

### Return type

[**GetCustomerAccountsGetCustomersIdAccountsResponse200**](GetCustomerAccountsGetCustomersIdAccountsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCustomersIdBillingEntitiesGet

> GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200 juniferCustomersIdBillingEntitiesGet(id)

Get customer&#39;s billing entities

Get customer&#39;s billing entities

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
apiInstance.juniferCustomersIdBillingEntitiesGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 

### Return type

[**GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200**](GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCustomersIdConsentsGet

> GetCustomerConsentsGetCustomersIdConsentsResponse200 juniferCustomersIdConsentsGet(id)

Get customer&#39;s consents

Get customer&#39;s consents for DCC meters i.e. meter reading frequency. If no consents can be found an empty &#x60;results&#x60; array is returned

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
apiInstance.juniferCustomersIdConsentsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 

### Return type

[**GetCustomerConsentsGetCustomersIdConsentsResponse200**](GetCustomerConsentsGetCustomersIdConsentsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCustomersIdConsentsPut

> juniferCustomersIdConsentsPut(id, requestBody)

Update customer&#39;s consents

Updates customer&#39;s consents for DCC meters i.e. meter reading frequency.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
let requestBody = new JuniferAuNativeApi.UpdateCustomerConsentsPutCustomersIdConsentsRequestBody(); // UpdateCustomerConsentsPutCustomersIdConsentsRequestBody | Request body
apiInstance.juniferCustomersIdConsentsPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 
 **requestBody** | [**UpdateCustomerConsentsPutCustomersIdConsentsRequestBody**](UpdateCustomerConsentsPutCustomersIdConsentsRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferCustomersIdContactsContactIdPut

> juniferCustomersIdContactsContactIdPut(id, contactId, requestBody)

Update customer&#39;s contact details

Updates a customer&#39;s given contact details. Correct usage is to get the &#x60;contact&#x60; object from the &#x60;get customer contact&#x60; call then modify the &#x60;contact&#x60; object and return it using this &#x60;Update Customer&#39;s Contact&#x60; call. Our database is then updated to match the incoming &#x60;contact&#x60; object. All null fields in that incoming &#x60;contact&#x60; object are set to null in our database.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
let contactId = 3.4; // Number | Contact ID
let requestBody = new JuniferAuNativeApi.UpdateCustomerContactPutCustomersIdContactsContactidRequestBody(); // UpdateCustomerContactPutCustomersIdContactsContactidRequestBody | Request body
apiInstance.juniferCustomersIdContactsContactIdPut(id, contactId, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 
 **contactId** | **Number**| Contact ID | 
 **requestBody** | [**UpdateCustomerContactPutCustomersIdContactsContactidRequestBody**](UpdateCustomerContactPutCustomersIdContactsContactidRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferCustomersIdContactsGet

> ContactsGetCustomersIdContactsResponse200 juniferCustomersIdContactsGet(id)

Contacts

Get all contacts for given customer. If no contacts can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
apiInstance.juniferCustomersIdContactsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 

### Return type

[**ContactsGetCustomersIdContactsResponse200**](ContactsGetCustomersIdContactsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCustomersIdCreateProspectPut

> CreateProspectPutCustomersIdCreateprospectResponse200 juniferCustomersIdCreateProspectPut(id, requestBody)

Create Prospect

Create new prospect for existing customer

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
let requestBody = new JuniferAuNativeApi.CreateProspectPutCustomersIdCreateprospectRequestBody(); // CreateProspectPutCustomersIdCreateprospectRequestBody | Request body
apiInstance.juniferCustomersIdCreateProspectPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 
 **requestBody** | [**CreateProspectPutCustomersIdCreateprospectRequestBody**](CreateProspectPutCustomersIdCreateprospectRequestBody.md)| Request body | 

### Return type

[**CreateProspectPutCustomersIdCreateprospectResponse200**](CreateProspectPutCustomersIdCreateprospectResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferCustomersIdEnrolAdditionalAccountPost

> EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200 juniferCustomersIdEnrolAdditionalAccountPost(id, requestBody)

Enrol a new account to an existing customer

Creates a new account for existing customer and enrols it for electricity and/or gas product

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | ID of additional account to enrol
let requestBody = new JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody(); // EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody | Request body
apiInstance.juniferCustomersIdEnrolAdditionalAccountPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID of additional account to enrol | 
 **requestBody** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody.md)| Request body | 

### Return type

[**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferCustomersIdGet

> GetCustomerGetCustomersIdResponse200 juniferCustomersIdGet(id)

Get customer

Get customer by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
apiInstance.juniferCustomersIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 

### Return type

[**GetCustomerGetCustomersIdResponse200**](GetCustomerGetCustomersIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCustomersIdPrimaryContactPut

> juniferCustomersIdPrimaryContactPut(id, requestBody)

Update customer&#39;s primary contact details

Updates customer&#39;s primary contact details. Correct usage is to get the &#x60;primaryContact&#x60; object from the &#x60;get customer&#x60; call then modify the &#x60;primaryContact&#x60; object and return it using this &#x60;Update Customer&#39;s Primary Contact&#x60; call. Our database is then updated to match the incoming &#x60;primaryContact&#x60; object. All null fields in that incoming &#x60;primaryContact&#x60; object are set to null in our database.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
let requestBody = new JuniferAuNativeApi.UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody(); // UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody | Request body
apiInstance.juniferCustomersIdPrimaryContactPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 
 **requestBody** | [**UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody**](UpdateCustomerPrimaryContactPutCustomersIdPrimarycontactRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferCustomersIdProspectsGet

> GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200 juniferCustomersIdProspectsGet(id)

Get customer&#39;s Prospects

Gets prospects for a customer

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
apiInstance.juniferCustomersIdProspectsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 

### Return type

[**GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200**](GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCustomersIdPut

> juniferCustomersIdPut(id, requestBody)

Update customer

Updates customer record in Junifer. Only &#x60;title&#x60;, &#x60;forename&#x60;, &#x60;surname&#x60; and &#x60;marketingOptOutFl&#x60; will be updated. The rest of the fields in the request are ignored.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
let requestBody = new JuniferAuNativeApi.UpdateCustomerPutCustomersIdRequestBody(); // UpdateCustomerPutCustomersIdRequestBody | Request body
apiInstance.juniferCustomersIdPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 
 **requestBody** | [**UpdateCustomerPutCustomersIdRequestBody**](UpdateCustomerPutCustomersIdRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferCustomersIdUpdateBillingAddressPut

> juniferCustomersIdUpdateBillingAddressPut(id, requestBody)

Update customer&#39;s Billing Address.

This Api call will change a customers billing address to what is supplied in the send address, if a postcode is sent it must be a valid UK postcode and if a Country name is sent it must be one that is saved into junifer already.Although postcode, Address lines and Country parameters are marked as optional, atleast one of them must be supplied to the call.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CustomersApi();
let id = 3.4; // Number | Customer ID
let requestBody = new JuniferAuNativeApi.UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody(); // UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody | Request body
apiInstance.juniferCustomersIdUpdateBillingAddressPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Customer ID | 
 **requestBody** | [**UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody**](UpdateBillingAddressPutCustomersIdUpdatebillingaddressRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

