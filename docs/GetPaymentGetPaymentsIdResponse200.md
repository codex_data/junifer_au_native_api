# JuniferAuNativeApi.GetPaymentGetPaymentsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the payment | 
**amount** | **Number** | The amount of the payment | 
**currency** | **String** | Currency ISO code | 
**apiIdempotencyKey** | **String** | Key in UUID format (to prevent duplicate payment requests) if specified | [optional] 
**paymentMethod** | **String** | The payment method | 
**acceptedDttm** | **Date** | The date when the payment has been accepted | [optional] 
**createdDttm** | **Date** | The date when the payment was created | 
**cancelledDttm** | **Date** | The date when the payment was cancelled | [optional] 
**postedDt** | **Date** | The date when the payment has been posted | [optional] 
**requestStatus** | **String** | The status of the associated payment request. Can be one of following: &#x60;Pending, Requesting, Successful, Cancelled, FailedPendingRetry, FailedRequestingAgain&#x60; | 
**createdBy** | **String** | The source of the payment. Can be one of the following: &#x60;API User, SYSTEM User&#x60; | 
**links** | [**GetAccountPaymentsGetAccountsIdPaymentsResponse200Links**](GetAccountPaymentsGetAccountsIdPaymentsResponse200Links.md) |  | 


