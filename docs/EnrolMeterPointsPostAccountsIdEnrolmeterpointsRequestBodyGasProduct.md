# JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**previousSupplier** | **String** | Previous supplier to the mprn | [optional] 
**previousTariff** | **String** | Previous Tariff of the mprn | [optional] 
**reference** | **String** | External reference code | [optional] 
**productCode** | **String** | Gas product code (reference in Junifer) | 
**startDate** | **Date** | Set the new Agreement start date and Registration date for a newly enrolled meterpoint | [optional] 
**mprns** | [**[EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns]**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns.md) | List of MPRNs to associate with a new gas agreement | 


