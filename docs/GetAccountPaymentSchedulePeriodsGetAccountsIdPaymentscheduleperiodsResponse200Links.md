# JuniferAuNativeApi.GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this payment schedule period | 


