# JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productType** | [**[GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType]**](GetAccountProductDetailsGetAccountsIdProductdetailsResponse200ProductType.md) | Product types. Can be one of the following: &#x60;Electricity, Gas&#x60; | 


