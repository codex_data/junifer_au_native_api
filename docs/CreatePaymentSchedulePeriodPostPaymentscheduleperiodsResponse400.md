# JuniferAuNativeApi.CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;RequiredParameterMissing&#x60; - The required &#39;accountid&#39; field is missing * &#x60;PaymentSchedulePeriodAlreadyExists&#x60; - Could not create payment schedule period as it would overlap with existing payment schedule period id &#39;paymentSchedulePeriodgetId&#39; * &#x60;InvalidFrequency&#x60; - Unrecognised value in &#39;frequency&#39; field * &#x60;InvalidDates&#x60; - toDt must be after fromDt * &#x60;InvalidAmount&#x60; - The payment amount is negative * &#x60;AccountCancelled&#x60; - Account is cancelled * &#x60;AccountTerminated&#x60; - Payment schedule not permitted for terminated accounts * &#x60;NonBroughtForwardBilling&#x60; - Payment schedule not permitted for non-brought forward billing accounts * &#x60;NotAPayPoint&#x60; - Account is not a pay point * &#x60;NoActiveSeasonalDefinition&#x60; - There is currently no active seasonal definition | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)

* `PaymentSchedulePeriodAlreadyExists` (value: `"PaymentSchedulePeriodAlreadyExists"`)

* `InvalidFrequency` (value: `"InvalidFrequency"`)

* `InvalidDates` (value: `"InvalidDates"`)

* `InvalidAmount` (value: `"InvalidAmount"`)

* `AccountCancelled` (value: `"AccountCancelled"`)

* `AccountTerminated` (value: `"AccountTerminated"`)

* `NonBroughtForwardBilling` (value: `"NonBroughtForwardBilling"`)

* `NotAPayPoint` (value: `"NotAPayPoint"`)

* `NoActiveSeasonalDefinition` (value: `"NoActiveSeasonalDefinition"`)




