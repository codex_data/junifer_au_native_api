# JuniferAuNativeApi.MarketingCampaignSourcesApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferMarketingCampaignSourcesGet**](MarketingCampaignSourcesApi.md#juniferMarketingCampaignSourcesGet) | **GET** /junifer/marketingCampaignSources | List Marketing Campaign Sources



## juniferMarketingCampaignSourcesGet

> ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200 juniferMarketingCampaignSourcesGet(billingEntityCode)

List Marketing Campaign Sources

List Marketing Campaign Sources available for the target billing entity in the Junifer

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.MarketingCampaignSourcesApi();
let billingEntityCode = "billingEntityCode_example"; // String | Code of the billing entity owning the products
apiInstance.juniferMarketingCampaignSourcesGet(billingEntityCode, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **billingEntityCode** | **String**| Code of the billing entity owning the products | 

### Return type

[**ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200**](ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

