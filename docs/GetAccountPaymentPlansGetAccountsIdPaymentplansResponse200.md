# JuniferAuNativeApi.GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The id of payment plan | 
**createdDttm** | **Date** | Date and time when the payment plan was created within Junifer | 
**collectType** | **String** | the way to collect payment | 
**currency** | **String** | Currency of the payment plan | 
**paymentPlanTransactions** | [**[GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions]**](GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200PaymentPlanTransactions.md) | PaymentPlanTransactions Array | 
**links** | [**GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links**](GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200Links.md) |  | 


