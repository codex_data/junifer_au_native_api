# JuniferAuNativeApi.GetBrokerLinkageGetBrokerlinkagesIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this BrokerLinkage | 
**broker** | **String** | Link to this BrokerLinkage&#39;s broker | 
**customer** | **String** | Link to the customer if the linkage is for a customer | [optional] 
**prospect** | **String** | Link to the prospect if the linkage is for a prospect | [optional] 


