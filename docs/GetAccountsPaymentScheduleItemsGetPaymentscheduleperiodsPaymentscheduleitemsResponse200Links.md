# JuniferAuNativeApi.GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentSchedulePeriod** | **String** | Link to the payment schedule period that the payment schedule item is associated with | 
**paymentRequest** | **String** | Link to the payment request that the payment schedule item is associated with | [optional] 


