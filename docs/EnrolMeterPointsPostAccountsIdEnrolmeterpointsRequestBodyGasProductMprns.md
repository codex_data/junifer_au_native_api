# JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProductMprns

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mprn** | **String** | MPRN core of the MPRN(Request Parameter) | 


