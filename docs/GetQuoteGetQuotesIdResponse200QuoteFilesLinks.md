# JuniferAuNativeApi.GetQuoteGetQuotesIdResponse200QuoteFilesLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to this quote file only | 
**image** | **String** | Link to the quote file image (usually PDF) | 


