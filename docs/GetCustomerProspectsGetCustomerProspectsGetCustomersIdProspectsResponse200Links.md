# JuniferAuNativeApi.GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this prospect | 
**customer** | **String** | Link to this prospect&#39;s customer | 


