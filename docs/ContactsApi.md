# JuniferAuNativeApi.ContactsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferContactsIdAccountsGet**](ContactsApi.md#juniferContactsIdAccountsGet) | **GET** /junifer/contacts/{id}/accounts | Get accounts
[**juniferContactsIdGet**](ContactsApi.md#juniferContactsIdGet) | **GET** /junifer/contacts/{id} | Get contact
[**juniferContactsIdMarketingPreferencesGet**](ContactsApi.md#juniferContactsIdMarketingPreferencesGet) | **GET** /junifer/contacts/{id}/marketingPreferences | Get a contact&#39;s marketing preferences
[**juniferContactsIdMarketingPreferencesPut**](ContactsApi.md#juniferContactsIdMarketingPreferencesPut) | **PUT** /junifer/contacts/{id}/marketingPreferences | Update a contact&#39;s marketing preferences



## juniferContactsIdAccountsGet

> GetContactsAccountsGetContactsIdAccountsResponse200 juniferContactsIdAccountsGet(id, opts)

Get accounts

Get list of accounts by contact ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ContactsApi();
let id = 3.4; // Number | Contact ID
let opts = {
  'date': "date_example", // String | Date for which there is a valid account. Will default to today if not provided
  'includeFutureFl': true, // Boolean | If true, the results will include any accounts' contacts that start in the future (after the specified date)
  'includePastFl': true // Boolean | If true, the results will include any accounts' contacts that ended in the past (before the specified date)
};
apiInstance.juniferContactsIdAccountsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Contact ID | 
 **date** | **String**| Date for which there is a valid account. Will default to today if not provided | [optional] 
 **includeFutureFl** | **Boolean**| If true, the results will include any accounts&#39; contacts that start in the future (after the specified date) | [optional] 
 **includePastFl** | **Boolean**| If true, the results will include any accounts&#39; contacts that ended in the past (before the specified date) | [optional] 

### Return type

[**GetContactsAccountsGetContactsIdAccountsResponse200**](GetContactsAccountsGetContactsIdAccountsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferContactsIdGet

> GetContactGetContactsIdResponse200 juniferContactsIdGet(id)

Get contact

Get contact by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ContactsApi();
let id = 3.4; // Number | Contact ID
apiInstance.juniferContactsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Contact ID | 

### Return type

[**GetContactGetContactsIdResponse200**](GetContactGetContactsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferContactsIdMarketingPreferencesGet

> GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200 juniferContactsIdMarketingPreferencesGet(id)

Get a contact&#39;s marketing preferences

Gets a contact&#39;s marketing preferences

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ContactsApi();
let id = 3.4; // Number | Contact ID
apiInstance.juniferContactsIdMarketingPreferencesGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Contact ID | 

### Return type

[**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferContactsIdMarketingPreferencesPut

> juniferContactsIdMarketingPreferencesPut(id, requestBody)

Update a contact&#39;s marketing preferences

Updates a contact&#39;s marketing preferences. Only the values provided will be updated.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ContactsApi();
let id = 3.4; // Number | Contact ID
let requestBody = new JuniferAuNativeApi.UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody(); // UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody | Request body
apiInstance.juniferContactsIdMarketingPreferencesPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Contact ID | 
 **requestBody** | [**UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody**](UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

