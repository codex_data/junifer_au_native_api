# JuniferAuNativeApi.GetProspectGetProspectsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Prospect ID | 
**number** | **String** | Prospect number | 
**companyName** | **String** | Name of the prospected company | [optional] 
**customerClass** | **String** | Class of the prospected customer | 
**companyType** | **String** | Type of the prospected company | 
**companyNumber** | **String** | Number of the prospected company | [optional] 
**registeredAddress** | **Object** | Registered address of the prospected company | 
**billingAddress** | **Object** | Billing address of the prospected company | [optional] 
**links** | [**GetProspectGetProspectsIdResponse200Links**](GetProspectGetProspectsIdResponse200Links.md) |  | 


