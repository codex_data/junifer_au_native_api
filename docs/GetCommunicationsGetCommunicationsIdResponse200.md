# JuniferAuNativeApi.GetCommunicationsGetCommunicationsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Communication id | 
**type** | **String** | Communication type | 
**status** | **String** | the status of the communication | 
**createdDttm** | **Date** | Date and time that the communication was created | 
**files** | [**[GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files]**](GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files.md) | the files associated with this communication | 
**links** | [**GetCommunicationsGetCommunicationsIdResponse200Links**](GetCommunicationsGetCommunicationsIdResponse200Links.md) |  | 


