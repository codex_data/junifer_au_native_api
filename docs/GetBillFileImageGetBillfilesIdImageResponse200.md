# JuniferAuNativeApi.GetBillFileImageGetBillfilesIdImageResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **String** | will be sent back as application/octet-stream | 


