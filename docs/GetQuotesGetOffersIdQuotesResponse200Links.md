# JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this quote | 
**refreshQuote** | **String** | Link to another quote if this quote has been refreshed | [optional] 


