# JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Group name | 
**_class** | **String** | Group class | 
**priceContents** | [**GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents**](GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents.md) |  | 


