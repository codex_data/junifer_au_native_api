# JuniferAuNativeApi.QuoteFilesApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferQuoteFilesIdGet**](QuoteFilesApi.md#juniferQuoteFilesIdGet) | **GET** /junifer/quoteFiles/{id} | Get quote file
[**juniferQuoteFilesIdImageGet**](QuoteFilesApi.md#juniferQuoteFilesIdImageGet) | **GET** /junifer/quoteFiles/{id}/image | Get quote file image



## juniferQuoteFilesIdGet

> GetQuoteFileGetQuotefilesIdResponse200 juniferQuoteFilesIdGet(id)

Get quote file

Get quote file record by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.QuoteFilesApi();
let id = 3.4; // Number | Quote file ID
apiInstance.juniferQuoteFilesIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Quote file ID | 

### Return type

[**GetQuoteFileGetQuotefilesIdResponse200**](GetQuoteFileGetQuotefilesIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferQuoteFilesIdImageGet

> GetQuoteFileImageGetQuotefilesIdImageResponse200 juniferQuoteFilesIdImageGet(id)

Get quote file image

Initiates quote file image (usually PDF file) download if the file is available

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.QuoteFilesApi();
let id = 3.4; // Number | Quote file ID
apiInstance.juniferQuoteFilesIdImageGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Quote file ID | 

### Return type

[**GetQuoteFileImageGetQuotefilesIdImageResponse200**](GetQuoteFileImageGetQuotefilesIdImageResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

