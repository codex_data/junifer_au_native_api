# JuniferAuNativeApi.AusEnrolCustomerPostAuCustomersEnrolcustomerRequestBodyElectricityProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**previousSupplier** | **String** | Previous supplier to the meterpoint | [optional] 
**previousTariff** | **String** | Previous Tariff of the meterpoint | [optional] 
**productCode** | **String** | Electricity product code (reference in Junifer) | 
**newConnectionFl** | **Boolean** | Is this a new connection requiring a meter be fitted? | [optional] 
**upgradeMeteringFl** | **Boolean** | Is the customer upgrading to a Smart meter? | [optional] 
**meterPoints** | [**[AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints]**](AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints.md) | List of NMIs to associate with a new electricity agreement | 


