# JuniferAuNativeApi.UpdateSepaDirectDebitPutSepadirectdebitsIdRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountName** | **String** | Bank account name | 
**iban** | **String** | IBAN | 
**mandateReference** | **String** | Mandate Reference | 
**authorisedDttm** | **Date** | Date when the mandate was authorised | [optional] 
**terminatedDttm** | **Date** | Date when the mandate was terminated | [optional] 


