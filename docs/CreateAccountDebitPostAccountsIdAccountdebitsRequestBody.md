# JuniferAuNativeApi.CreateAccountDebitPostAccountsIdAccountdebitsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grossAmount** | **Number** | Gross amount of account debit | 
**salesTaxName** | **String** | Name of linked sales tax of account debit. Providing &#x60;\&quot;None\&quot;&#x60; will apply no sales tax to the account debit. | 
**accountDebitReasonName** | **String** | Name of linked account debit reason for account debit | 
**reference** | **String** | Text identifier for the account debit | [optional] 
**description** | **String** | Description of the account transaction linked to the account debit | [optional] 


