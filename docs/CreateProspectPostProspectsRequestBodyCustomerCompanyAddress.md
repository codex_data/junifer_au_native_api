# JuniferAuNativeApi.CreateProspectPostProspectsRequestBodyCustomerCompanyAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressType** | **String** | Company registered address type - defaults to standard address type | [optional] 
**address1** | **String** | Company registered address line 1 | [optional] 
**address2** | **String** | Company registered address line 2 | [optional] 
**address3** | **String** | Company registered address line 3 | [optional] 
**address4** | **String** | Company registered address line 4 | [optional] 
**address5** | **String** | Company registered address line 5 | [optional] 
**postcode** | **String** | Company registered address postcode | [optional] 
**country** | **String** | Company registered address country - see the Country ref table for valid values | [optional] 


