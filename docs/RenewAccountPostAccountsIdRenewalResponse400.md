# JuniferAuNativeApi.RenewAccountPostAccountsIdRenewalResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;MissingDetails&#x60; - If a gasProductCode is supplied then a gasSupplyProductSubType must also be supplied * &#x60;NoCreditReasonSet&#x60; - You have tried to refund an exit fee but there is no default exit fee reason for renewals set * &#x60;NotFound&#x60; - Account supplied was not found * &#x60;CanNotCreditZero&#x60; - You can not credit a value of zero * &#x60;BadRequest&#x60; - There was an error processing your request | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `MissingDetails` (value: `"MissingDetails"`)

* `NoCreditReasonSet` (value: `"NoCreditReasonSet"`)

* `NotFound` (value: `"NotFound"`)

* `CanNotCreditZero` (value: `"CanNotCreditZero"`)

* `BadRequest` (value: `"BadRequest"`)




