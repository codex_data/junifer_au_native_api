# JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** | The social media type 2 label name for the contact type (see ContactType ref table for possible values) | 
**consentGiven** | **Boolean** | Consent has been given (true or false) | 
**methodOfConsent** | **String** | If consent given, how it was given (see MethodOfConsent ref table for possible values) | [optional] 


