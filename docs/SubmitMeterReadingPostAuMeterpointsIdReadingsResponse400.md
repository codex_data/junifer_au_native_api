# JuniferAuNativeApi.SubmitMeterReadingPostAuMeterpointsIdReadingsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;BadRequest&#x60; - Invalid reading validation errors have been found * &#x60;InternalError&#x60; - Unknown error while processing readingDttm &#39;readingsDttm&#39; * &#x60;InvalidQuality&#x60; - The specified quality x is not valid * &#x60;InvalidSource&#x60; - The specified source x is not valid * &#x60;MeterNotAttachedToMeterPoint&#x60; - The meter x is not attached to the meter point at date/time y * &#x60;MeterMissingEACAQ&#x60; - The meter x does not have y for all its registers at date/time z * &#x60;MeterMissingEUC&#x60; - The meterpoint x does not have an EUC at date/time y * &#x60;ReadingMismatch&#x60; - EventDttm, Sequence Type, Source and Quality must match for each register under a meter * &#x60;MeterHasNoRegisters&#x60; - The meter x has no registers configured * &#x60;RequiredRegisterIdentifierMissing&#x60; - The meter x has multiple registers but no register identifier was specified * &#x60;InvalidRegisterIdentifier&#x60; - The meter x does not have a register with identifier y * &#x60;RegisterNotConnectedToMeterPoint&#x60; - The meter is connected to the meter point but the specified register is not connected to the meter point at the date/time x | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `BadRequest` (value: `"BadRequest"`)

* `InternalError` (value: `"InternalError"`)

* `InvalidQuality` (value: `"InvalidQuality"`)

* `InvalidSource` (value: `"InvalidSource"`)

* `MeterNotAttachedToMeterPoint` (value: `"MeterNotAttachedToMeterPoint"`)

* `MeterMissingEACAQ` (value: `"MeterMissingEACAQ"`)

* `MeterMissingEUC` (value: `"MeterMissingEUC"`)

* `ReadingMismatch` (value: `"ReadingMismatch"`)

* `MeterHasNoRegisters` (value: `"MeterHasNoRegisters"`)

* `RequiredRegisterIdentifierMissing` (value: `"RequiredRegisterIdentifierMissing"`)

* `InvalidRegisterIdentifier` (value: `"InvalidRegisterIdentifier"`)

* `RegisterNotConnectedToMeterPoint` (value: `"RegisterNotConnectedToMeterPoint"`)




