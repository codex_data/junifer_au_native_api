# JuniferAuNativeApi.GetAccountCreditGetAccountcreditsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Account credit Identifier | 
**createdDttm** | **Date** | Date and time when the credit was created | 
**acceptedDttm** | **Date** | Date and time when the credit was accepted | 
**grossAmount** | **Number** | Gross amount of account credit | 
**netAmount** | **Number** | Net amount of account credit | 
**salesTax** | **String** | Name of linked sales tax to this account credit | [optional] 
**salesTaxAmount** | **Number** | Amount of sales tax for this account credit | 
**reason** | **String** | Name of linked account credit reason | 
**reference** | **String** | Custom text reference for account credit | [optional] 
**cancelledDttm** | **Date** | Date and time when the account debit was cancelled | [optional] 


