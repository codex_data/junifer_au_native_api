# JuniferAuNativeApi.GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Prospect ID | 
**number** | **String** | Prospect number | 
**companyName** | **String** | Name of the prospected company | [optional] 
**links** | [**GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links**](GetCustomerProspectsGetCustomerProspectsGetCustomersIdProspectsResponse200Links.md) |  | 


