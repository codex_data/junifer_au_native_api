# JuniferAuNativeApi.GetContactsAccountsGetContactsIdAccountsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Account ID | 
**type** | **String** | Account type code as defined in the Account Type ref table in Junifer. This can vary per install so please consult the ref table for possible values | 
**name** | **String** | Account name | 
**number** | **String** | Account number | 
**currency** | **String** | Currency ISO code | 
**fromDt** | **Date** | Account start date | 
**toDt** | **Date** | Account end date | [optional] 
**createdDttm** | **Date** | Account creation date and time | 
**closedDttm** | **Date** | Account close date and time | [optional] 
**balance** | **Number** | Account&#39;s balance (only if AR is enabled) | [optional] 
**billingAddress** | [**GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress**](GetContactsAccountsGetContactsIdAccountsResponse200BillingAddress.md) |  | [optional] 
**billDelivery** | **String** | Method of bill delivery \&quot;None\&quot;, \&quot;Mail\&quot;, \&quot;Email\&quot;, \&quot;Both\&quot; | [optional] 
**paymentMethodType** | **String** | Account&#39;s payment method type, i.e. Direct Debit | [optional] 
**accountClass** | **String** | Account&#39;s class type \&quot;Credit\&quot;, \&quot;Statement\&quot;, \&quot;Cost Statement, \&quot;Prepay\&quot; | [optional] 
**billingCycle** | **String** | Account&#39;s billing cycle i.e. \&quot;Monthly - Anniversary\&quot;, \&quot;Annually - Anniversary\&quot;, | [optional] 
**parentAccountId** | **Number** | Parent account id of the provided account. | [optional] 
**links** | [**GetContactsAccountsGetContactsIdAccountsResponse200Links**](GetContactsAccountsGetContactsIdAccountsResponse200Links.md) |  | 


