# JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agreements** | [**[AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements]**](AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements.md) | Array of agreements to create | 


