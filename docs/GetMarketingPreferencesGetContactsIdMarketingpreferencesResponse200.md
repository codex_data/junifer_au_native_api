# JuniferAuNativeApi.GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketingMaterials** | **[Object]** | Array of marketing material string names (see MarketingMaterial ref table for possible values) | 
**marketByEmail** | [**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByEmail.md) |  | [optional] 
**marketByNumber1** | [**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber1.md) |  | [optional] 
**marketByNumber2** | [**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber2.md) |  | [optional] 
**marketByNumber3** | [**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByNumber3.md) |  | [optional] 
**marketBySms** | [**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySms.md) |  | [optional] 
**marketByPost** | [**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketByPost.md) |  | [optional] 
**marketBySocialMedia1** | [**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia1.md) |  | [optional] 
**marketBySocialMedia2** | [**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia2.md) |  | [optional] 
**marketBySocialMedia3** | [**GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3**](GetMarketingPreferencesGetContactsIdMarketingpreferencesResponse200MarketBySocialMedia3.md) |  | [optional] 


