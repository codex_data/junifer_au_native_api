# JuniferAuNativeApi.GetCommunicationsFileGetCommunicationsfilesIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Communication id | 
**links** | [**GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links**](GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links.md) |  | 


