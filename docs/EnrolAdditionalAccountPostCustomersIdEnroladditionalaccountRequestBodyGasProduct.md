# JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**previousSupplier** | **String** | Previous supplier to the MPRN | [optional] 
**previousTariff** | **String** | Previous Tariff of the MPRN | [optional] 
**reference** | **String** | External reference code | [optional] 
**productCode** | **String** | Gas product code (reference in Junifer) | 
**mprns** | [**[EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns]**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns.md) | List of MPRNs to associate with the new gas agreement | 
**domesticLargeSupplyPointFl** | **Boolean** | Is the meterpoint a large domestic supply point? Overrides the market sector specified in DES(Request Parameter) | [optional] 


