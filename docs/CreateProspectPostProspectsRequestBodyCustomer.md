# JuniferAuNativeApi.CreateProspectPostProspectsRequestBodyCustomer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billingEntity** | **String** | Billing entity name - see the Billing Entity screen for valid values | [optional] 
**customerClass** | **String** | Customer class name - see the Customer Class ref table for valid values | 
**customerType** | **String** | Customer type name - see the Customer Type ref table for valid values | 
**title** | **String** | Customer title - see the Title ref table for valid values | [optional] 
**forename** | **String** | Customer forename | [optional] 
**surname** | **String** | Customer surname | [optional] 
**companyNumber** | **String** | Company number | [optional] 
**companyAddress** | [**CreateProspectPostProspectsRequestBodyCustomerCompanyAddress**](CreateProspectPostProspectsRequestBodyCustomerCompanyAddress.md) |  | [optional] 
**primaryContact** | [**CreateProspectPostProspectsRequestBodyCustomerPrimaryContact**](CreateProspectPostProspectsRequestBodyCustomerPrimaryContact.md) |  | 


