# JuniferAuNativeApi.BillEmailsGetBillemailsIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to attachment itself | 
**image** | **String** | Link to attachment image | 


