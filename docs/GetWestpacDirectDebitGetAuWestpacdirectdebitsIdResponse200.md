# JuniferAuNativeApi.GetWestpacDirectDebitGetAuWestpacdirectdebitsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Westpac Direct Debit object id | 
**accountName** | **String** | Account name | 
**mandateReference** | **String** | Mandate Reference | [optional] 
**bankAccountNumber** | **String** | Bank account number | [optional] 
**bsbNumber** | **String** | BSB number | [optional] 
**customerNumber** | **String** | Customer&#39;s unique ID | [optional] 
**paymentType** | **String** | Payment type. Can be either &#x60;BankAccount&#x60; or &#x60;CreditCard&#x60; | 
**cardType** | **String** | Credit card type. | [optional] 
**authorisedDttm** | **Date** | Date when the Direct Debit was authorised | [optional] 
**terminatedDttm** | **Date** | Date when the Direct Debit was terminated | [optional] 
**status** | **String** | Current status of the Direct Debit | 


