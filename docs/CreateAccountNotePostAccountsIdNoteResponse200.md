# JuniferAuNativeApi.CreateAccountNotePostAccountsIdNoteResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The new Note ID | 
**createdDttm** | **Date** | Note creation date and time | 
**subject** | **String** | Note Subject | 
**type** | **String** | Note Type. Can be one of the following: &#x60;Note, Email, File, Phone, Letter&#x60; | 
**summary** | **String** | A brief summary of the Note | 
**content** | **String** | The detailed content of the Note | [optional] 


