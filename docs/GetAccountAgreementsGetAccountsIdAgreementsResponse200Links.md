# JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to the individual agreement | 
**account** | **String** | Link to the account to which the agreement is associated with | 


