# JuniferAuNativeApi.GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromDttm** | **Date** | debt recover period from datetime | 
**toDttm** | **Date** | debt recover period to datetime | 
**amount** | **Number** | debt recover amount | 


