# JuniferAuNativeApi.GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Account review period Id | 
**fromDttm** | **Date** | Account review period start date time | 
**toDttm** | **Date** | Account review period end date time | [optional] 
**reason** | **String** | Reason for the created review period. Must be the name of an account review reason as shown in the Account Review Reason ref table | 
**suppressDunningFl** | **Boolean** | A flag indicating whether dunning should be suppressed | 
**suppressBillingFl** | **Boolean** | A flag indicating whether billing should be suppressed | 
**suppressPaymentCollectionFl** | **Boolean** | A flag indicating whether payment collection should be suppressed | 
**suppressPaymentReviewFl** | **Boolean** | A flag indicating whether payment review should be suppressed | 
**propertySuppressionFl** | **Boolean** | Property-specific billing suppression | 
**description** | **String** | Account review period description | [optional] 
**createdDttm** | **Date** | Account review period creation date and time | 
**createdBy** | **String** | Username that created the account review period | 
**cancelledDttm** | **Date** | Account review period cancelled date and time | [optional] 
**cancelledBy** | **String** | Username that cancelled the account review period | [optional] 


