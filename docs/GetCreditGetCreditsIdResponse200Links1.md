# JuniferAuNativeApi.GetCreditGetCreditsIdResponse200Links1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this credit file | 


