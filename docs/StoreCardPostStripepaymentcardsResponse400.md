# JuniferAuNativeApi.StoreCardPostStripepaymentcardsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;MissingLast4&#x60; - \&quot;The required &#39;last4&#39; field is missing\&quot; * &#x60;AccountNotFound&#x60; - \&quot;Account with such &#39;account.id&#39; does not exist\&quot; * &#x60;MissingCustomer&#x60; - \&quot;The required &#39;customer&#39; field is missing\&quot; * &#x60;MissingAccountId&#x60; - \&quot;The required &#39;account.id&#39; field is missing\&quot; * &#x60;MissingExpMonth&#x60; - \&quot;The required &#39;expMonth&#39; field is missing\&quot; * &#x60;MissingExpYear&#x60; - \&quot;The required &#39;expYear&#39; field is missing\&quot; * &#x60;ProvidedExpMonth&#x60; - \&quot;The &#39;expMonth&#39; field has been provided with a Payment Method ID\&quot; * &#x60;ProvidedLast4&#x60; - \&quot;The &#39;last4&#39; field has been provided with a Payment Method ID\&quot; * &#x60;ProvidedExpYear&#x60; - \&quot;The &#39;expYear&#39; field has been provided with a Payment Method ID\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `MissingLast4` (value: `"MissingLast4"`)

* `AccountNotFound` (value: `"AccountNotFound"`)

* `MissingCustomer` (value: `"MissingCustomer"`)

* `MissingAccountId` (value: `"MissingAccountId"`)

* `MissingExpMonth` (value: `"MissingExpMonth"`)

* `MissingExpYear` (value: `"MissingExpYear"`)

* `ProvidedExpMonth` (value: `"ProvidedExpMonth"`)

* `ProvidedLast4` (value: `"ProvidedLast4"`)

* `ProvidedExpYear` (value: `"ProvidedExpYear"`)




