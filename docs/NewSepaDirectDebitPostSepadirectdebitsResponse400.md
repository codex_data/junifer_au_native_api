# JuniferAuNativeApi.NewSepaDirectDebitPostSepadirectdebitsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;IbanNotValid&#x60; - \&quot;IBAN is invalid: \&quot; + errorMessage * &#x60;AccountNotFound&#x60; - \&quot;Account with such &#39;account.id&#39; does not exist\&quot; * &#x60;RequiredParameterMissing&#x60; - \&quot;The required &#39;account.id&#39; field is missing\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `IbanNotValid` (value: `"IbanNotValid"`)

* `AccountNotFound` (value: `"AccountNotFound"`)

* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)




