# JuniferAuNativeApi.GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | BrokerLinkages ID | 
**fromDt** | **Date** | The date from which the customer/prospect&#39;s consent to be managed is valid | 
**toDt** | **Date** | The date to which the customer/prospect&#39;s consent to be managed is valid | 
**quotingPermissions** | **Boolean** | Indicates whether the customer/prospect has Quoting Permissions | 
**billingPermissions** | **Boolean** | Indicates whether the customer/prospect has Billing Permissions | 
**acceptQuotingPermissions** | **Boolean** | Indicates whether the customer/prospect has Accept Quoting Permissions | 
**links** | [**GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links**](GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links.md) |  | 


