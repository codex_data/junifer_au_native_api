# JuniferAuNativeApi.CommunicationsEmailsGetCommunicationsemailsIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to this email | 
**communication** | **String** | Link to the communication this email belongs to | 


