# JuniferAuNativeApi.ListAvailableMarketingCampaignSourcesGetMarketingcampaignsourcesResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | **String** | The list of marketing campaign sources available for the target billing entity | 


