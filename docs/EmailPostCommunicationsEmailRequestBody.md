# JuniferAuNativeApi.EmailPostCommunicationsEmailRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billingEntity** | **String** | Code of the billing entity for this web portal | 
**toAddress** | **String** | Recipient&#39;s email address | 
**subject** | **String** | Subject text | 
**body** | **String** | Email&#39;s body text | 


