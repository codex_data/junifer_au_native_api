# JuniferAuNativeApi.GetCreditGetCreditsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | credit ID | 
**number** | **String** | Credit number | 
**createdDttm** | **Date** | Credit creation date and time | 
**status** | **String** | Status of the Credit | 
**authorisedDttm** | **Date** | Datetime the Credit was authorised | [optional] 
**currency** | **String** | Currency ISO code | 
**grossAmount** | **Number** | Gross amount shown on the credit | 
**netAmount** | **Number** | Net amount shown on the credit | 
**salesTaxAmount** | **Number** | Sales tax amount shown on the credit | 
**issueDt** | **Date** | Credit issue date | 
**links** | [**GetCreditGetCreditsIdResponse200Links**](GetCreditGetCreditsIdResponse200Links.md) |  | 
**creditFiles** | [**[GetCreditGetCreditsIdResponse200CreditFiles]**](GetCreditGetCreditsIdResponse200CreditFiles.md) | Downloadable credit files | 


