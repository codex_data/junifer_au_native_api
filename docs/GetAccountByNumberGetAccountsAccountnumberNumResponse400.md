# JuniferAuNativeApi.GetAccountByNumberGetAccountsAccountnumberNumResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;MultipleMatches&#x60; - Found X accounts with number Y. Use the account lookup API to narrow down your search with additional parameters | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `MultipleMatches` (value: `"MultipleMatches"`)




