# JuniferAuNativeApi.CreatePaymentSchedulePeriodPostPaymentscheduleperiodsResponse404

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;AccountNotFound&#x60; - Account with such &#39;accountid&#39; does not exist | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `AccountNotFound` (value: `"AccountNotFound"`)




