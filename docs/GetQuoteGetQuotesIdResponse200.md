# JuniferAuNativeApi.GetQuoteGetQuotesIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Quote ID | 
**number** | **String** | Quote number | 
**quoteType** | **String** | Quote type | 
**description** | **String** | Description of the quote | 
**createdDttm** | **Date** | Date and time when the quote was created | 
**createdUser** | **String** | User who created the quote | 
**archived** | **Boolean** | Indicates whether the quote is archived | 
**contractSpend** | **Number** | Total gross value for this quote | 
**brokerMargins** | [**GetQuoteGetQuotesIdResponse200BrokerMargins**](GetQuoteGetQuotesIdResponse200BrokerMargins.md) |  | [optional] 
**meterPoints** | [**GetQuotesGetOffersIdQuotesResponse200MeterPoints**](GetQuotesGetOffersIdQuotesResponse200MeterPoints.md) |  | [optional] 
**links** | [**GetQuotesGetOffersIdQuotesResponse200Links**](GetQuotesGetOffersIdQuotesResponse200Links.md) |  | 
**quoteFiles** | [**GetQuoteGetQuotesIdResponse200QuoteFiles**](GetQuoteGetQuotesIdResponse200QuoteFiles.md) |  | [optional] 


