# JuniferAuNativeApi.GetOfferGetOffersIdResponse200QuotesLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | The link to the quote itself. | 


