# JuniferAuNativeApi.CreateAccountTicketPostAccountsIdTicketsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticketDefinitionCode** | **String** | Ticket definition code | 
**ticketStepCode** | **String** | Ticket step code to use initially | 
**summary** | **String** | Brief summary for the created ticket | 
**description** | **String** | Ticket description | [optional] 
**parentTicketId** | **Number** | Associate a ticket with a parent | [optional] 
**assignedUserTeam** | **String** | Assign a team | [optional] 


