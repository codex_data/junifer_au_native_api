# JuniferAuNativeApi.BillingEntitiesApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferBillingEntitiesIdGet**](BillingEntitiesApi.md#juniferBillingEntitiesIdGet) | **GET** /junifer/billingEntities/{id} | Get billing entity



## juniferBillingEntitiesIdGet

> GetBillingEntityGetBillingentitiesIdResponse200 juniferBillingEntitiesIdGet(id)

Get billing entity

Get billing entity by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillingEntitiesApi();
let id = 3.4; // Number | Billing Entity ID
apiInstance.juniferBillingEntitiesIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Billing Entity ID | 

### Return type

[**GetBillingEntityGetBillingentitiesIdResponse200**](GetBillingEntityGetBillingentitiesIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

