# JuniferAuNativeApi.CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accounts** | **[Object]** | The account(s) that the ticket is associated with | [optional] 
**customers** | **[Object]** | The customer(s) that the ticket is associated with | [optional] 
**tickets** | **[Object]** | The ticket(s) that the ticket is associated with | [optional] 
**communications** | **[Object]** | The communication(s) that the ticket is associated with | [optional] 
**bills** | **[Object]** | The bill(s) that the ticket is associated with | [optional] 
**paymentRequests** | **[Object]** | The payment request(s) that the ticket is associated with | [optional] 
**paymentSchedulePeriods** | **[Object]** | The payment schedule period(s) that the ticket is associated with | [optional] 
**paymentMethods** | **[Object]** | The payment method(s) that the ticket is associated with | [optional] 
**alerts** | **[Object]** | The alert(s) that the ticket is associated with | [optional] 
**billPeriods** | **[Object]** | The bill period(s) that the ticket is associated with | [optional] 
**billingEntities** | **[Object]** | The billing entities that the ticket is associated with | [optional] 
**billEmails** | **[Object]** | The bill email(s) that the ticket is associated with | [optional] 
**changeOfTenancys** | **[Object]** | The change of tenancies that the ticket is associated with | [optional] 
**assets** | **[Object]** | The assets/meterpoints that the ticket is associated with | [optional] 
**contacts** | **[Object]** | The contacts that the ticket is associated with | [optional] 


