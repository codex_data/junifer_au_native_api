# JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**senderReference** | **String** | Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected | 
**reference** | **String** | Unique reference string identifying this particular enrolment | 
**billingEntityCode** | **String** | Code of the billing entity to which the customer must be attached | [optional] 
**marketingOptOutFl** | **Boolean** | A boolean flag indicating whether the customer would like to opt out of any marketing communications | 
**submittedSource** | **String** | The source of the enrolment. This overrides the default source (WebService) if set | [optional] 
**changeOfTenancyFl** | **Boolean** | A boolean flag indicating whether the customer is a change of tenancy.(Request Parameter) | [optional] 
**supplyAddress** | [**EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodySupplyAddress.md) |  | 
**electricityProduct** | [**EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProduct.md) |  | 
**gasProduct** | [**EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasProduct.md) |  | 
**gasEstimate** | [**EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.md) |  | [optional] 
**electricityEstimate** | [**EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityEstimate.md) |  | [optional] 


