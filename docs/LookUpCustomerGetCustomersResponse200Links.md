# JuniferAuNativeApi.LookUpCustomerGetCustomersResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this customer | 
**accounts** | **String** | Link to customer&#39;s accounts | 
**securityQuestions** | **String** | Link to customers security questions and answers | 
**billingEntities** | **String** | Link to customer&#39;s billing entities | 


