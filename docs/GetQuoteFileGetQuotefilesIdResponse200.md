# JuniferAuNativeApi.GetQuoteFileGetQuotefilesIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Quote file ID | 
**display** | **String** | Quote file description | 
**viewable** | **Boolean** | Flag indicating whether the quote file can be viewed | 
**createdDttm** | **Date** | The date and time the quote file was created | 
**links** | [**GetQuoteFileGetQuotefilesIdResponse200Links**](GetQuoteFileGetQuotefilesIdResponse200Links.md) |  | 


