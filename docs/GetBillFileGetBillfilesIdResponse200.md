# JuniferAuNativeApi.GetBillFileGetBillfilesIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Bill File Id | 
**display** | **String** | Can be one of &#x60;PDF&#x60; | 
**viewable** | **Boolean** | Available to download | 
**links** | [**GetBillFileGetBillfilesIdResponse200Links**](GetBillFileGetBillfilesIdResponse200Links.md) |  | 


