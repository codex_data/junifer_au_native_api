# JuniferAuNativeApi.AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountId** | **Number** | Account ID | 
**concessionDfnId** | **Number** | Concession definition ID (required if no name given) | [optional] 
**concessionDfnName** | **String** | Concession definition name (required if no ID given) | [optional] 
**concessionCardTypeId** | **Number** | Concession card type ID (required if no name given) | [optional] 
**concessionCardTypeName** | **String** | Concession card type name (required if no ID given) | [optional] 
**cardNumber** | **String** | Card number | 
**applicantName** | **String** | Applicant name | [optional] 
**contactId** | **Number** | Contact ID | 
**fromDt** | **Date** | From date | 
**endDt** | **Date** | End date | [optional] 


