# JuniferAuNativeApi.CancelAccountCreditDeleteAccountcreditsIdResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;AccountCreditCancelled&#x60; - \&quot;This account credit has already been cancelled\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `AccountCreditCancelled` (value: `"AccountCreditCancelled"`)




