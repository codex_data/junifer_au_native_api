# JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200AgreementType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | name of the agreement type | [optional] 
**duration** | **Number** | the duration of the agreement | [optional] 


