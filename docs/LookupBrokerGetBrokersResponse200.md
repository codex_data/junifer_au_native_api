# JuniferAuNativeApi.LookupBrokerGetBrokersResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Broker&#39;s Id | 
**code** | **String** | Broker&#39;s unique code | 
**name** | **String** | Broker&#39;s Name | 
**email** | **String** | Broker&#39;s email address | [optional] 
**links** | [**LookupBrokerGetBrokersResponse200Links**](LookupBrokerGetBrokersResponse200Links.md) |  | 


