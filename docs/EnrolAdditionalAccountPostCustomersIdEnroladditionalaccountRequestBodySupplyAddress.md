# JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**careOf** | **String** | The &#39;Care of&#39; line for the address | [optional] 
**address1** | **String** | First address line | 
**address2** | **String** | Second address line | [optional] 
**address3** | **String** | Third address line | [optional] 
**town** | **String** | Town | [optional] 
**county** | **String** | County | [optional] 
**postcode** | **String** | Post code | 
**countryCode** | **String** | An ISO country code. Currently only GB is supported thus is the the only acceptable value!(Request Parameter) | 


