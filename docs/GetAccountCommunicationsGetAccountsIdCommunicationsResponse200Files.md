# JuniferAuNativeApi.GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | the id of the file | 
**links** | [**GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links**](GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links.md) |  | 


