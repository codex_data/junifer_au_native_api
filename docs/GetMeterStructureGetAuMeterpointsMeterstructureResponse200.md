# JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsMeterstructureResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Meterpoint id | 
**identifier** | **String** | Meterpoint identifier. For meterpoints of type &#x60;NMI&#x60; this will be NMI Identifier | 
**type** | **String** | Meterpoint type | 
**links** | [**GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links**](GetMeterStructureGetAuMeterpointsMeterstructureResponse200Links.md) |  | 
**meters** | [**[GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters]**](GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters.md) | Array with meters attached to this property | 


