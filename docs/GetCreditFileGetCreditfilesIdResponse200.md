# JuniferAuNativeApi.GetCreditFileGetCreditfilesIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Credit File Id | 
**display** | **String** | Can be one of &#x60;PDF&#x60; | 
**viewable** | **Boolean** | Available to download | 
**links** | [**GetCreditFileGetCreditfilesIdResponse200Links**](GetCreditFileGetCreditfilesIdResponse200Links.md) |  | 


