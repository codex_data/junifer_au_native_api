# JuniferAuNativeApi.GetCustomerGetCustomersIdResponse200PrimaryContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Contact Id | 
**contactType** | **String** | Contact Type | 
**primary** | **Boolean** | Whether contact is the primary contact | 
**title** | **String** | Customer&#39;s primary contact title | [optional] 
**forename** | **String** | Customer&#39;s primary contact forename | [optional] 
**surname** | **String** | Customer&#39;s primary contact surname | 
**initials** | **String** | Customer&#39;s primary contact initials | 
**jobTitle** | **String** | Customer&#39;s primary contact jobTitle | 
**address** | [**GetCustomerGetCustomersIdResponse200PrimaryContactAddress**](GetCustomerGetCustomersIdResponse200PrimaryContactAddress.md) |  | [optional] 
**addressType** | **String** | Internally used address type which defines number of lines and format used etc. | [optional] 
**countryCode** | **String** | Country code | [optional] 
**country** | **String** | Country name | [optional] 
**email** | **String** | Customer&#39;s primary contact email | [optional] 
**dateOfBirth** | **Date** | Customer&#39;s date of birth | [optional] 
**phoneNumber1** | **String** | Customer&#39;s primary contact phone number | [optional] 
**phoneNumber2** | **String** | Customer&#39;s secondary contact phone number | [optional] 
**phoneNumber3** | **String** | Customer&#39;s third contact phone number | [optional] 
**customerContactId** | **Number** | Customer contact Id | 
**cancelled** | **Boolean** | True if this contact is no longer associated with the customer | 
**fromDttm** | **Date** | Contacts from date time | 
**toDttm** | **Date** | Contacts to date time | [optional] 
**links** | [**ContactsGetAccountsIdContactsResponse200Links**](ContactsGetAccountsIdContactsResponse200Links.md) |  | 


