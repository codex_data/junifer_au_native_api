# JuniferAuNativeApi.NewPayPalDirectDebitPostAuPaypaldirectdebitsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mandateReference** | **String** | PayPal Direct Debit mandate reference | 


