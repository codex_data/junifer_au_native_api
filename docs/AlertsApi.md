# JuniferAuNativeApi.AlertsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAlertsIdGet**](AlertsApi.md#juniferAlertsIdGet) | **GET** /junifer/alerts/{id} | Get alert



## juniferAlertsIdGet

> GetAlertGetAlertsIdResponse200 juniferAlertsIdGet(id)

Get alert

Get alert by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AlertsApi();
let id = 3.4; // Number | Alert ID
apiInstance.juniferAlertsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Alert ID | 

### Return type

[**GetAlertGetAlertsIdResponse200**](GetAlertGetAlertsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

