# JuniferAuNativeApi.CreateAccountTicketPostAccountsIdTicketsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this ticket | [optional] 
**parentTicket** | **String** | Link to the parent ticket of the ticket. | [optional] 


