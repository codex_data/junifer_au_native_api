# JuniferAuNativeApi.CommunicationsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferCommunicationsEmailPost**](CommunicationsApi.md#juniferCommunicationsEmailPost) | **POST** /junifer/communications/email | Send email
[**juniferCommunicationsEmailsIdGet**](CommunicationsApi.md#juniferCommunicationsEmailsIdGet) | **GET** /junifer/communicationsEmails/{id} | Get communication email
[**juniferCommunicationsFilesIdGet**](CommunicationsApi.md#juniferCommunicationsFilesIdGet) | **GET** /junifer/communicationsFiles/{id} | Get communication file
[**juniferCommunicationsFilesIdImageGet**](CommunicationsApi.md#juniferCommunicationsFilesIdImageGet) | **GET** /junifer/communicationsFiles/{id}/image | Get communication file image
[**juniferCommunicationsIdGet**](CommunicationsApi.md#juniferCommunicationsIdGet) | **GET** /junifer/communications/{id} | Get Communication



## juniferCommunicationsEmailPost

> juniferCommunicationsEmailPost(requestBody)

Send email

Send an instantaneous email from Junifer server

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CommunicationsApi();
let requestBody = new JuniferAuNativeApi.EmailPostCommunicationsEmailRequestBody(); // EmailPostCommunicationsEmailRequestBody | Request body
apiInstance.juniferCommunicationsEmailPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**EmailPostCommunicationsEmailRequestBody**](EmailPostCommunicationsEmailRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferCommunicationsEmailsIdGet

> CommunicationsEmailsGetCommunicationsemailsIdResponse200 juniferCommunicationsEmailsIdGet(id)

Get communication email

Gets the email associated with a communication. Only one email/sms per communication is returned (in case of multiple contacts)

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CommunicationsApi();
let id = 3.4; // Number | Communication ID (not email id)
apiInstance.juniferCommunicationsEmailsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Communication ID (not email id) | 

### Return type

[**CommunicationsEmailsGetCommunicationsemailsIdResponse200**](CommunicationsEmailsGetCommunicationsemailsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCommunicationsFilesIdGet

> GetCommunicationsFileGetCommunicationsfilesIdResponse200 juniferCommunicationsFilesIdGet(id)

Get communication file

Get Communication specified by ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CommunicationsApi();
let id = 3.4; // Number | Communication ID
apiInstance.juniferCommunicationsFilesIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Communication ID | 

### Return type

[**GetCommunicationsFileGetCommunicationsfilesIdResponse200**](GetCommunicationsFileGetCommunicationsfilesIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCommunicationsFilesIdImageGet

> GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200 juniferCommunicationsFilesIdImageGet(id)

Get communication file image

Initiates communication file image (usually PDF file) download if the file is available

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CommunicationsApi();
let id = 3.4; // Number | Communication file ID
apiInstance.juniferCommunicationsFilesIdImageGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Communication file ID | 

### Return type

[**GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200**](GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCommunicationsIdGet

> GetCommunicationsGetCommunicationsIdResponse200 juniferCommunicationsIdGet(id)

Get Communication

Get Communication specified by ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CommunicationsApi();
let id = 3.4; // Number | Communication ID
apiInstance.juniferCommunicationsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Communication ID | 

### Return type

[**GetCommunicationsGetCommunicationsIdResponse200**](GetCommunicationsGetCommunicationsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

