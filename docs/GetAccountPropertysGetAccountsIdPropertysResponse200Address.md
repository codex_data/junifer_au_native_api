# JuniferAuNativeApi.GetAccountPropertysGetAccountsIdPropertysResponse200Address

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressType** | **String** | Internally used address type which defines number of lines and format used etc. | 
**address1** | **String** | Property address line 1 | 
**address2** | **String** | Property address line 2 | 
**address3** | **String** | Property address line 3 | 
**address4** | **String** | Property address line 4 | 
**address5** | **String** | Property address line 5 | 
**postcode** | **String** | Property address post code | 


