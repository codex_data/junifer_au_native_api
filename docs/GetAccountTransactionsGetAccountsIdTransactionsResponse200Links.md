# JuniferAuNativeApi.GetAccountTransactionsGetAccountsIdTransactionsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bill** | **String** | Link referring to associated bill if applicable | [optional] 
**payment** | **String** | Link referring to associated payment if applicable | [optional] 
**accountCredit** | **String** | Link referring to associated account credit if applicable | [optional] 
**accountDebit** | **String** | Link referring to associated account debit if applicable | [optional] 
**credit** | **String** | Link referring to associated credit if applicable | [optional] 


