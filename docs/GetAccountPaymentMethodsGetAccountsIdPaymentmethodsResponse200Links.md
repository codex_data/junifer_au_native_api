# JuniferAuNativeApi.GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this payment method | 
**directDebit** | **String** | Link to direct debit if the &#x60;paymentMethodType&#x60; is \&quot;Direct Debit\&quot; | [optional] 
**goCardlessDirectDebit** | **String** | Link to Go Cardless direct debit if the &#x60;paymentMethodType&#x60; is \&quot;Go Cardless Direct Debit\&quot; | [optional] 
**stripePaymentCard** | **String** | Link to Stripe payment card if the &#x60;paymentMethodType&#x60; is \&quot;Stripe Payment Card\&quot; | [optional] 
**account** | **String** | Link to the account to which this payment method belongs | 


