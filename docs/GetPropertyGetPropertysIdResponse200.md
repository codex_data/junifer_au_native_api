# JuniferAuNativeApi.GetPropertyGetPropertysIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the property | 
**propertyType** | **String** | The type of the property. This will be one of the values in the \&quot;Property Type\&quot; ref table. | 
**address** | [**GetPropertyGetPropertysIdResponse200Address**](GetPropertyGetPropertysIdResponse200Address.md) |  | 
**addressType** | **String** | The type of the address | 


