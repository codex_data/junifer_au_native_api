# JuniferAuNativeApi.CreateProspectPostProspectsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brokerId** | **Number** | Broker ID | 
**customer** | [**CreateProspectPostProspectsRequestBodyCustomer**](CreateProspectPostProspectsRequestBodyCustomer.md) |  | 


