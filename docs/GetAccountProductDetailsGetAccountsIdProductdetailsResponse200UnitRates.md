# JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200UnitRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate** | **Number** | Rate. Can be one of the following: &#x60;Standard, Day, Night&#x60; | 


