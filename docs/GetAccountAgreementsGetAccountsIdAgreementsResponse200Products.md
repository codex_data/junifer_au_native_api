# JuniferAuNativeApi.GetAccountAgreementsGetAccountsIdAgreementsResponse200Products

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the product | 
**type** | **String** | The type of the product | 
**internalName** | **String** | The name of the product used internally | 
**displayName** | **String** | The name of the product shown to customers on communications and webportals. This can change at any time | 
**reference** | **String** | The product&#39;s reference code | 
**assets** | [**[GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets]**](GetAccountAgreementsGetAccountsIdAgreementsResponse200Assets.md) | An array of assets associated with the product. The array can be empty if an agreement is asset independent, e.g., an one-off charge. | 


