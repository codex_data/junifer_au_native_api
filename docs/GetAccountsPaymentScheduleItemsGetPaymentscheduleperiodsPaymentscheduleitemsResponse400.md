# JuniferAuNativeApi.GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;MissingAccountIdParameter&#x60; - The required &#39;accountId&#39; parameter is missing | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `MissingAccountIdParameter` (value: `"MissingAccountIdParameter"`)




