# JuniferAuNativeApi.GetAccountPaymentsGetAccountsIdPaymentsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this payment | 
**account** | **String** | Link to the account to which the payment is linked to | 


