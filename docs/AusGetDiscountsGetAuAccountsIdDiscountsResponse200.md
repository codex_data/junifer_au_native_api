# JuniferAuNativeApi.AusGetDiscountsGetAuAccountsIdDiscountsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the DiscountDfn | 
**name** | **String** | The discount name | 
**fromDt** | **Date** | The from date of the account discount | 
**toDt** | **Date** | The to date of the account discount (not returned for open-ended discounts) | 
**billingEntityId** | **Number** | The ID of the billing entity | 
**billingEntityCode** | **String** | The code of the billing entity | [optional] 
**currency** | **String** | The ISO code of the currency | 


