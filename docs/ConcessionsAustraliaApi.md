# JuniferAuNativeApi.ConcessionsAustraliaApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAuConcessionsCreateConcessionPost**](ConcessionsAustraliaApi.md#juniferAuConcessionsCreateConcessionPost) | **POST** /junifer/au/concessions/createConcession | Create concession



## juniferAuConcessionsCreateConcessionPost

> AusCreateConcessionPostAuConcessionsCreateconcessionResponse200 juniferAuConcessionsCreateConcessionPost(requestBody)

Create concession

Creates a concession. This is an Australia specific API.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ConcessionsAustraliaApi();
let requestBody = new JuniferAuNativeApi.AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody(); // AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody | Request body
apiInstance.juniferAuConcessionsCreateConcessionPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody**](AusCreateConcessionPostAuConcessionsCreateconcessionRequestBody.md)| Request body | 

### Return type

[**AusCreateConcessionPostAuConcessionsCreateconcessionResponse200**](AusCreateConcessionPostAuConcessionsCreateconcessionResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

