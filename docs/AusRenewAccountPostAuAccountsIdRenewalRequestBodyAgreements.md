# JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalRequestBodyAgreements

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**electricityProductCode** | **String** | Product code for new electricity agreement | 
**electricitySupplyProductSubType** | **String** | Product sub type for new electricity agreement | 
**electricityStartDate** | **String** | Start date of new electricity agreement | 
**agreementIdToRenew** | **Number** | Id of the existing agreement to be replaced. If neither &#x60;agreementIdToRenew&#x60; nor &#x60;agreementNumberToRenew&#x60; are provided, a new agreement will be created alongside existing agreements instead of replacing an existing agreement. | [optional] 
**agreementNumberToRenew** | **String** | Number of the existing agreement to be replaced. If neither &#x60;agreementIdToRenew&#x60; nor &#x60;agreementNumberToRenew&#x60; are provided, a new agreement will be created alongside existing agreements instead of replacing an existing agreement. | [optional] 
**meterPointIdentifier** | **String** | Meter point identifier if creating a new asset-linked agreement. | [optional] 


