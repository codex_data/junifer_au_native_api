# JuniferAuNativeApi.GetAccountPaymentsGetAccountsIdPaymentsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InvalidParameter&#x60; - The inputted value &#39;statusString&#39;is not a valid transaction status | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InvalidParameter` (value: `"InvalidParameter"`)




