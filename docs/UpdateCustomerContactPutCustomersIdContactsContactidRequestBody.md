# JuniferAuNativeApi.UpdateCustomerContactPutCustomersIdContactsContactidRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Contact&#39;s title. Can be &#x60;Dr, Miss, Mr, Mrs, Ms, Prof&#x60; | [optional] 
**forename** | **String** | Contact&#39;s first name | [optional] 
**surname** | **String** | Contact&#39;s surname | 
**email** | **String** | Email address | [optional] 
**dateOfBirth** | **Date** | Contact&#39;s date of birth | [optional] 
**phoneNumber1** | **String** | Phone number 1 | [optional] 
**phoneNumber2** | **String** | Phone number 2 | [optional] 
**phoneNumber3** | **String** | Phone number 3 | [optional] 
**careOfField** | **String** | care of name | [optional] 


