# JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**previousSupplier** | **String** | Previous supplier to the mpan | [optional] 
**previousTariff** | **String** | Previous Tariff of the mpan | [optional] 
**reference** | **String** | External reference code | [optional] 
**newConnectionFl** | **Boolean** | Is this a new connection requiring a new meter to be fitted? If true then an X0003 will be sent with the AREGI if using utiliserve to handle flows. | [optional] 
**productCode** | **String** | Electricity product code (reference in Junifer) | 
**mpans** | [**[EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans]**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans.md) | List of MPANs to associate with a new electricity agreement | 


