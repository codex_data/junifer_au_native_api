# JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Contact title. Can be &#x60;Dr, Miss, Mr, Mrs, Ms, Prof&#x60; | [optional] 
**forename** | **String** | Contact&#39;s first name | [optional] 
**surname** | **String** | Contact&#39;s second name | 
**billingMethod** | **String** | A way for the contact to receive communications. Can be one of the following: &#x60;Both, Paper, eBilling&#x60; | 
**email** | **String** | Contact&#39;s email address to which communications will be sent in the case where &#x60;billingMethod&#x60; is &#x60;Both&#x60; or &#x60;eBilling&#x60; | [optional] 
**phone1** | **String** | Contact&#39;s primary contact telephone number | 
**phone2** | **String** | Contact&#39;s second contact telephone number | [optional] 
**phone3** | **String** | Contact&#39;s third contact telephone number | [optional] 
**dateOfBirth** | **Date** | Contact&#39;s date of birth | [optional] 
**primaryContact** | **Boolean** | A boolean flag indicating whether this contact is the primary for this customer | 
**address** | [**AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress**](AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyAddress.md) |  | [optional] 


