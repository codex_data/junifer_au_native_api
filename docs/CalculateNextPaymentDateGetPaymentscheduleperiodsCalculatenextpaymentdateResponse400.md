# JuniferAuNativeApi.CalculateNextPaymentDateGetPaymentscheduleperiodsCalculatenextpaymentdateResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;RequiredParameterMissing&#x60; - The required &#39;startDttm&#39; parameter is missing * &#x60;InvalidFrequency&#x60; - Unrecognised value in &#39;frequency&#39; param | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)

* `InvalidFrequency` (value: `"InvalidFrequency"`)




