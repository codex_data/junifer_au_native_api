# JuniferAuNativeApi.LookUpAccountGetAccountsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;MissingParameters&#x60; - One of &#39;number&#39;,&#39;surname&#39;,&#39;telephone&#39;,&#39;billingPostCode&#39; or &#39;email&#39; lookup parameter is missing * &#x60;EmptyParameter&#x60; - &#39;telephone&#39; parameter has been included in request but is empty | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `MissingParameters` (value: `"MissingParameters"`)

* `EmptyParameter` (value: `"EmptyParameter"`)




