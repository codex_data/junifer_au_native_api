# JuniferAuNativeApi.PaymentPostAccountsIdPaymentRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Number** |  | 
**paymentMethodType** | **String** | Please ask Junifer to find out what payment method types are supported in your installation. | 
**description** | **String** | A description of the payment request. Will be displayed against the transaction in the financials screen. | 
**status** | **String** | Can be Successful, Pending or PendingAuthorisation for a Direct Debit payment method type. For all other supported payment method types can be either Successful or Cancelled. If unsupplied status will default to Successful. | [optional] 
**postedDt** | **Date** | Date of when the payment was posted. | [optional] 


