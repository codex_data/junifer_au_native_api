# JuniferAuNativeApi.BillEmailsGetBillemailsIdResponse200Files

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Attachment ID | 
**links** | [**BillEmailsGetBillemailsIdResponse200Links**](BillEmailsGetBillemailsIdResponse200Links.md) |  | 


