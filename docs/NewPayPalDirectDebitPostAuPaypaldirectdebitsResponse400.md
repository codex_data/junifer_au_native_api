# JuniferAuNativeApi.NewPayPalDirectDebitPostAuPaypaldirectdebitsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;AccountNotFound&#x60; - \&quot;Account with such &#39;account.id&#39; does not exist\&quot; * &#x60;RequiredParameterMissing&#x60; - \&quot;The required &#39;mandateReference&#39; field is missing\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `AccountNotFound` (value: `"AccountNotFound"`)

* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)




