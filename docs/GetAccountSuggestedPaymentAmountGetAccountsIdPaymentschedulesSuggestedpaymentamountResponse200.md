# JuniferAuNativeApi.GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**frequency** | **String** | The frequency indicating when a payment should be collected | 
**frequencyMultiple** | **Number** | Frequency multiplier | 
**suggestedPaymentAmount** | **Number** | Suggested payment amount for the selected account | 
**links** | [**GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links**](GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200Links.md) |  | 


