# JuniferAuNativeApi.GetBrokerGetBrokersIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Broker&#39;s Id | 
**code** | **String** | Broker&#39;s unique code | 
**name** | **String** | Broker&#39;s Name | 
**email** | **String** | Broker&#39;s email address | [optional] 
**links** | [**GetBrokerGetBrokersIdResponse200Links**](GetBrokerGetBrokersIdResponse200Links.md) |  | 


