# JuniferAuNativeApi.CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromDttm** | **Date** | Account review period start date time | 
**toDttm** | **Date** | Account review period end date time | [optional] 
**reason** | **String** | Reason for the created review period. Must be the name of an account review reason as shown in the Account Review Reason ref table | 
**suppressDunningFl** | **Boolean** | A flag indicating whether dunning should be suppressed | [optional] 
**suppressBillingFl** | **Boolean** | A flag indicating whether billing should be suppressed | [optional] 
**suppressPaymentCollectionFl** | **Boolean** | A flag indicating whether payment collection should be suppressed | [optional] 
**suppressPaymentReviewFl** | **Boolean** | A flag indicating whether payment review should be suppressed | [optional] 
**propertySuppressionFl** | **Boolean** | Property-specific billing suppression | [optional] 
**description** | **String** | Free text description | [optional] 


