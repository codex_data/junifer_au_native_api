# JuniferAuNativeApi.GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to this SEPA Direct Debit | 
**paymentMethod** | **String** | Link to this SEPA Direct Debit payment method | 


