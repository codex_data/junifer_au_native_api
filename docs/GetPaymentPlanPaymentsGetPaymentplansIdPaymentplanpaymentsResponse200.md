# JuniferAuNativeApi.GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | PaymentPlan payment ID | 
**scheduledDt** | **Date** | Date when the payment is due to collect | 
**status** | **String** | The status of payments | 
**scheduledAmount** | **Number** | the amount to collect for the payment | 
**balance** | **Number** | The balance of the corresonding payment, i.e. the paidAmount - scheduledAmount | 


