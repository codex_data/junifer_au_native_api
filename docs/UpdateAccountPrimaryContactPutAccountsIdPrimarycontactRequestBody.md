# JuniferAuNativeApi.UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Primary Contact&#39;s title. Can be &#x60;Dr, Miss, Mr, Mrs, Ms, Prof&#x60; | [optional] 
**forename** | **String** | Primary contact&#39;s first name | [optional] 
**surname** | **String** | Primary contact&#39;s surname | 
**email** | **String** | Email address | [optional] 
**dateOfBirth** | **Date** | Primary contact&#39;s date of birth | [optional] 
**phoneNumber1** | **String** | Phone number 1 | [optional] 
**phoneNumber2** | **String** | Phone number 2 | [optional] 
**phoneNumber3** | **String** | Phone number 3 | [optional] 
**careOfField** | **String** | care of name | [optional] 
**billDelivery** | **String** | Contacts Bill Delivery Preference. Can be &#x60;Email&#x60;, &#x60;Mail&#x60;, &#x60;Both&#x60;, &#x60;None&#x60; | [optional] 
**receivePost** | **Boolean** | Contacts communication postal delivery preference. | [optional] 
**receiveEmail** | **Boolean** | Contacts communication email delivery preference. | [optional] 
**receiveSMS** | **Boolean** | Contacts communication SMS delivery preference. | [optional] 


