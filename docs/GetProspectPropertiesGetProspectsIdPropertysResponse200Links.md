# JuniferAuNativeApi.GetProspectPropertiesGetProspectsIdPropertysResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this prospect | 
**meterPoints** | **String** | Link to this property&#39;s meter point information | 


