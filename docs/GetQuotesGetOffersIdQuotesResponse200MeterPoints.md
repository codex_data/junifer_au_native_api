# JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200MeterPoints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Meter point ID | 
**identifier** | **String** | Meter point identifier | 
**type** | **String** | Type of meter point | 
**prices** | [**GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices**](GetQuotesGetOffersIdQuotesResponse200MeterPointsPrices.md) |  | [optional] 
**links** | [**GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks**](GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks.md) |  | 


