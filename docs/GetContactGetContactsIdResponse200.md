# JuniferAuNativeApi.GetContactGetContactsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Contact Id | 
**contactType** | **String** | Contact Type | 
**title** | **String** | Contact title | [optional] 
**forename** | **String** | Contact forename | [optional] 
**initials** | **String** | Contact initials | [optional] 
**surname** | **String** | Contact surname | 
**jobTitle** | **String** | Contact job title | [optional] 
**email** | **String** | Contact email | 
**dateOfBirth** | **Date** | Contact&#39;s date of birth | [optional] 
**phoneNumber1** | **String** | Primary contact phone number | [optional] 
**phoneNumber2** | **String** | Secondary contact phone number | [optional] 
**phoneNumber3** | **String** | Third contact phone number | [optional] 
**socialMedia1** | **String** | Contact social media type 1 | [optional] 
**socialMedia2** | **String** | Contact social media type 2 | [optional] 
**socialMedia3** | **String** | Contact social media type 3 | [optional] 
**address** | [**GetContactGetContactsIdResponse200Address**](GetContactGetContactsIdResponse200Address.md) |  | [optional] 


