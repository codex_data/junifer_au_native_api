# JuniferAuNativeApi.CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestId** | **String** | Payment request ID | 


