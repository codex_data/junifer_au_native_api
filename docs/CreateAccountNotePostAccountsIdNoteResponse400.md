# JuniferAuNativeApi.CreateAccountNotePostAccountsIdNoteResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InvalidParameter&#x60; - The type field &#39;noteMessageType&#39; is not a valid note type * &#x60;RequiredParameterMissing&#x60; - summary is a mandatory field | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InvalidParameter` (value: `"InvalidParameter"`)

* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)




