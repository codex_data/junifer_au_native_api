# JuniferAuNativeApi.GetBillingEntityGetBillingentitiesIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this billing entity | 
**parentBillingEntity** | **String** | Link to the parent of this billing entity | [optional] 


