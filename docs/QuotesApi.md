# JuniferAuNativeApi.QuotesApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferQuotesIdAcceptQuotePost**](QuotesApi.md#juniferQuotesIdAcceptQuotePost) | **POST** /junifer/quotes/{id}/acceptQuote | 
[**juniferQuotesIdGet**](QuotesApi.md#juniferQuotesIdGet) | **GET** /junifer/quotes/{id} | Get quote



## juniferQuotesIdAcceptQuotePost

> juniferQuotesIdAcceptQuotePost(id, id2)



Accepts a quote given the quote ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.QuotesApi();
let id = 3.4; // Number | Path Quote ID
let id2 = 56; // Number | Quote ID
apiInstance.juniferQuotesIdAcceptQuotePost(id, id2, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Path Quote ID | 
 **id2** | **Number**| Quote ID | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferQuotesIdGet

> GetQuoteGetQuotesIdResponse200 juniferQuotesIdGet(id)

Get quote

Returns a quote given the quote ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.QuotesApi();
let id = 3.4; // Number | Quote ID
apiInstance.juniferQuotesIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Quote ID | 

### Return type

[**GetQuoteGetQuotesIdResponse200**](GetQuoteGetQuotesIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

