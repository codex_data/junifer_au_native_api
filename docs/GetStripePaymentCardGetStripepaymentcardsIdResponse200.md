# JuniferAuNativeApi.GetStripePaymentCardGetStripepaymentcardsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the Stripe Payment Card | 
**createdDttm** | **Date** | The date and time this Stripe Payment Card was created | 
**expiryDt** | **Date** | (deprecated) Date when the payment method expires | [optional] 
**lastFourDigits** | **Number** | (deprecated) Last four digits of the Stripe Payment Card number | [optional] 
**stripeCusId** | **String** | The Stripe customer id of the associated customer | 
**stripePaymentMethodId** | **String** | the the Stripe payment method id of the associated customer | [optional] 
**links** | [**GetStripePaymentCardGetStripepaymentcardsIdResponse200Links**](GetStripePaymentCardGetStripepaymentcardsIdResponse200Links.md) |  | 


