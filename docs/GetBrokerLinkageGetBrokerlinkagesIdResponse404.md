# JuniferAuNativeApi.GetBrokerLinkageGetBrokerlinkagesIdResponse404

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;NotFound&#x60; - Could not find &#39;brokerLinkage&#39; with Id &#39;brokerLinkageId&#39; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `NotFound` (value: `"NotFound"`)




