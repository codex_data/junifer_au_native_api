# JuniferAuNativeApi.GetCustomerAccountsGetCustomersIdAccountsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this account | 
**customer** | **String** | Link to this account&#39;s customer record | 
**contacts** | **String** | Link to this account&#39;s contacts | 
**bills** | **String** | Link to account&#39;s bills | 
**payments** | **String** | Link to account&#39;s payments | 
**paymentMethods** | **String** | Link to account&#39;s payment methods | 
**agreements** | **String** | Link to account&#39;s agreements | 
**paymentSchedulePeriods** | **String** | Link to account&#39;s payment schedule periods | 
**paymentSchedulesSuggestedPaymentAmount** | **String** | Suggested payment amount for the payment schedule | 
**tickets** | **String** | Link to account&#39;s tickets | 
**productDetails** | **String** | Link to account&#39;s product details | 


