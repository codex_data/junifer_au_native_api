# JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**estimatedCost** | **Number** | Estimated cost of the new Electricity Tariff | [optional] 
**consumption** | **Number** | Customer-provided estimation of their current electricity consumption | [optional] 
**previousCost** | **Number** | Customer-provided estimation of their current electricity spend | [optional] 
**e7DaytimeConsumptionProportion** | **Number** | The assumed proportion of estimated usage attributable to the &#39;Day&#39; register of an E7 meter (a number from &#x60;0.00&#x60; to &#x60;1.00&#x60;) | [optional] 
**period** | **String** | The period for the estimated electricity values. Must be &#x60;Month&#x60; or &#x60;Year&#x60; | 


