# JuniferAuNativeApi.GetStripePaymentCardGetStripepaymentcardsIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this Stripe Payment Card | 
**paymentMethod** | **String** | Link to the paymentMethod to which this Stripe Payment Card belongs | 


