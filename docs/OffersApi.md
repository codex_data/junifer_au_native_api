# JuniferAuNativeApi.OffersApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferOffersIdGet**](OffersApi.md#juniferOffersIdGet) | **GET** /junifer/offers/{id} | Get Offer
[**juniferOffersIdQuotesGet**](OffersApi.md#juniferOffersIdQuotesGet) | **GET** /junifer/offers/{id}/quotes | Get quotes



## juniferOffersIdGet

> GetOfferGetOffersIdResponse200 juniferOffersIdGet(id)

Get Offer

Get the offer details that is associated with the given offer id

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.OffersApi();
let id = 3.4; // Number | Offer id
apiInstance.juniferOffersIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Offer id | 

### Return type

[**GetOfferGetOffersIdResponse200**](GetOfferGetOffersIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferOffersIdQuotesGet

> GetQuotesGetOffersIdQuotesResponse200 juniferOffersIdQuotesGet(id)

Get quotes

Get offer&#39;s quotes by its offer ID. If no quotes can be found an empty &#x60;results&#x60; array is returned.This API uses pagination and as such results may be restricted.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.OffersApi();
let id = 3.4; // Number | Offer ID
apiInstance.juniferOffersIdQuotesGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Offer ID | 

### Return type

[**GetQuotesGetOffersIdQuotesResponse200**](GetQuotesGetOffersIdQuotesResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

