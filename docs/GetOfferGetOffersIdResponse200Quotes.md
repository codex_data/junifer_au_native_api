# JuniferAuNativeApi.GetOfferGetOffersIdResponse200Quotes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **String** | The quotes number. | 
**createdDttm** | **Date** | The creation date and time for the quote. | 
**status** | **String** | The status of the quote. | 
**links** | [**GetOfferGetOffersIdResponse200QuotesLinks**](GetOfferGetOffersIdResponse200QuotesLinks.md) |  | 


