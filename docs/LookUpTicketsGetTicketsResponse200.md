# JuniferAuNativeApi.LookUpTicketsGetTicketsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticketId** | **Number** | Ticket Identifier | 
**parentTicketId** | **Number** | Id of any parent ticket | [optional] 
**ticketDefinitionCode** | **String** | Ticket definition code | 
**ticketDefinitionName** | **String** | Ticket definition name | 
**ticketStepCode** | **String** | Ticket step code | 
**ticketStepName** | **String** | Ticket step name | 
**status** | **String** | Ticket status &#x60;Open, Closed, Cancelled, Error&#x60; | 
**keyIdentifier** | **String** | Ticket key identifier | 
**summary** | **String** | Ticket summary | 
**description** | **String** | Ticket description | 
**createdDttm** | **Date** | Ticket creation date and time | 
**dueDttm** | **Date** | Ticket due date and time | [optional] 
**cancelDttm** | **Date** | Date and time when ticket was cancelled | [optional] 
**ticketCancelReason** | **String** | The reason ticket was cancelled | [optional] 
**assignedUserTeam** | **String** | Team assigned to deal with this ticket | 
**stepStartDttm** | **Date** | Ticket step start date and time | 
**stepScheduleDttm** | **Date** | Ticket step scheduled date and time | 
**stepDueDttm** | **Date** | Ticket step due date and time | [optional] 
**ticketEntities** | [**CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities**](CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.md) |  | 
**links** | [**CreateAccountTicketPostAccountsIdTicketsResponse200Links**](CreateAccountTicketPostAccountsIdTicketsResponse200Links.md) |  | 
**searchLimitReached** | **Boolean** | Indicates if the result set was restricted to 500 tickets (False: result set less than 500, True: result set more than 500) | 


