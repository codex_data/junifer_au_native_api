# JuniferAuNativeApi.MeterpointsAustraliaApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAuMeterPointsGet**](MeterpointsAustraliaApi.md#juniferAuMeterPointsGet) | **GET** /junifer/au/meterPoints | MeterPoint lookup
[**juniferAuMeterPointsIdMeterStructureGet**](MeterpointsAustraliaApi.md#juniferAuMeterPointsIdMeterStructureGet) | **GET** /junifer/au/meterPoints/{id}/meterStructure | Get meterpoint structure
[**juniferAuMeterPointsIdReadingsGet**](MeterpointsAustraliaApi.md#juniferAuMeterPointsIdReadingsGet) | **GET** /junifer/au/meterPoints/{id}/readings | Get meter readings
[**juniferAuMeterPointsIdReadingsPost**](MeterpointsAustraliaApi.md#juniferAuMeterPointsIdReadingsPost) | **POST** /junifer/au/meterPoints/{id}/readings | Submit meter readings
[**juniferAuMeterPointsMeterStructureGet**](MeterpointsAustraliaApi.md#juniferAuMeterPointsMeterStructureGet) | **GET** /junifer/au/meterPoints/meterStructure | Get meterpoint structure



## juniferAuMeterPointsGet

> LookUpMeterPointGetAuMeterpointsResponse200 juniferAuMeterPointsGet(meterSerialNumber, queryDttm)

MeterPoint lookup

Searches for a meterPoint by the specified meterSerialNumber and queryDttm. If no meterPoint can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.MeterpointsAustraliaApi();
let meterSerialNumber = 3.4; // Number | Serial number of the meter that is linked to the meterPoint
let queryDttm = "queryDttm_example"; // String | Time that the meter is linked to the meterPoint. Do not enter a Dttm that is before the SSD.
apiInstance.juniferAuMeterPointsGet(meterSerialNumber, queryDttm, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **meterSerialNumber** | **Number**| Serial number of the meter that is linked to the meterPoint | 
 **queryDttm** | **String**| Time that the meter is linked to the meterPoint. Do not enter a Dttm that is before the SSD. | 

### Return type

[**LookUpMeterPointGetAuMeterpointsResponse200**](LookUpMeterPointGetAuMeterpointsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAuMeterPointsIdMeterStructureGet

> GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200 juniferAuMeterPointsIdMeterStructureGet(id, effectiveDt)

Get meterpoint structure

Get meterpoint structure

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.MeterpointsAustraliaApi();
let id = 3.4; // Number | Meterpoint ID
let effectiveDt = "effectiveDt_example"; // String | Show meterpoint structure at the specific date
apiInstance.juniferAuMeterPointsIdMeterStructureGet(id, effectiveDt, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Meterpoint ID | 
 **effectiveDt** | **String**| Show meterpoint structure at the specific date | 

### Return type

[**GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200**](GetMeterStructureGetAuMeterpointsIdMeterstructureResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAuMeterPointsIdReadingsGet

> GetMeterReadingsGetAuMeterpointsIdReadingsResponse200 juniferAuMeterPointsIdReadingsGet(id, fromDt, toDt, opts)

Get meter readings

Get (billable) meter readings for the specified period

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.MeterpointsAustraliaApi();
let id = 3.4; // Number | Meterpoint ID
let fromDt = "fromDt_example"; // String | The start date of the meter reading range
let toDt = "toDt_example"; // String | The end date of the meter reading range
let opts = {
  'status': ["null"] // [String] | Array of reading status strings used to filter the search of our meter readings.  Can contain the following: `Accepted, Pending, Unknown, Removed` and if left blank will default to using `Accepted`.
};
apiInstance.juniferAuMeterPointsIdReadingsGet(id, fromDt, toDt, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Meterpoint ID | 
 **fromDt** | **String**| The start date of the meter reading range | 
 **toDt** | **String**| The end date of the meter reading range | 
 **status** | [**[String]**](String.md)| Array of reading status strings used to filter the search of our meter readings.  Can contain the following: &#x60;Accepted, Pending, Unknown, Removed&#x60; and if left blank will default to using &#x60;Accepted&#x60;. | [optional] 

### Return type

[**GetMeterReadingsGetAuMeterpointsIdReadingsResponse200**](GetMeterReadingsGetAuMeterpointsIdReadingsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAuMeterPointsIdReadingsPost

> juniferAuMeterPointsIdReadingsPost(id, requestBody)

Submit meter readings

Submits meter reading. If you don&#39;t know if a meter has technical details or not, first call \&quot;Submit meter readings\&quot;, then if it fails with an error, call the \&quot;Submit meter readings without meter technical details\&quot;.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.MeterpointsAustraliaApi();
let id = 3.4; // Number | Meterpoint ID
let requestBody = new JuniferAuNativeApi.SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody(); // SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody | Request body
apiInstance.juniferAuMeterPointsIdReadingsPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Meterpoint ID | 
 **requestBody** | [**SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody**](SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAuMeterPointsMeterStructureGet

> GetMeterStructureGetAuMeterpointsMeterstructureResponse200 juniferAuMeterPointsMeterStructureGet(id, effectiveDt)

Get meterpoint structure

Get meterpoint structure

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.MeterpointsAustraliaApi();
let id = "id_example"; // String | Meterpoint Identifier
let effectiveDt = "effectiveDt_example"; // String | Show meterpoint structure at the specific date
apiInstance.juniferAuMeterPointsMeterStructureGet(id, effectiveDt, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Meterpoint Identifier | 
 **effectiveDt** | **String**| Show meterpoint structure at the specific date | 

### Return type

[**GetMeterStructureGetAuMeterpointsMeterstructureResponse200**](GetMeterStructureGetAuMeterpointsMeterstructureResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

