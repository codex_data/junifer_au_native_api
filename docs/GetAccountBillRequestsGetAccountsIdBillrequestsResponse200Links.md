# JuniferAuNativeApi.GetAccountBillRequestsGetAccountsIdBillrequestsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this bill request | 
**bill** | **String** | Link referring to this bill request&#39;s bill, if exists | [optional] 
**account** | **String** | Link referring to this bill request&#39;s account | [optional] 


