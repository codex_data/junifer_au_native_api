# JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;NoProductFilter&#x60; - Product with code has no product filter * &#x60;InternalError&#x60; - MeterPoint is neither Electricity nor Gas | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `NoProductFilter` (value: `"NoProductFilter"`)

* `InternalError` (value: `"InternalError"`)




