# JuniferAuNativeApi.GetBrokerGetBrokersIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this broker | 
**customerBrokers** | **String** | Link referring to this broker&#39;s customerBrokers with a valid Letter of Authority for today&#39;s date | 


