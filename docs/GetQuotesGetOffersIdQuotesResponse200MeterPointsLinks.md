# JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200MeterPointsLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to this meter point only | 


