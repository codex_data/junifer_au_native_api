# JuniferAuNativeApi.TariffInformationGetAccountsIdTariffinformationResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InternalError&#x60; - Found more than two fuel types: this is not currently supported by UkTariffComparisonOperations | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InternalError` (value: `"InternalError"`)




