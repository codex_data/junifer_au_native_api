# JuniferAuNativeApi.GetMeterPointsGetPropertysIdMeterpointsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Meterpoint ID | 
**identifier** | **String** | Meterpoint identifier. For meterpoints of type &#x60;MPAN&#x60; this will be MPAN core, for &#x60;MPRN&#x60; - MPRN identifier | 
**type** | **String** | Type of the meter point. &#x60;MPAN&#x60; or &#x60;MPRN&#x60; | 
**supplyStartDate** | **Date** | Supply start date or not present if not relevant | 
**changeOfTenancyFl** | **Boolean** | Change of tenancy flag | 
**ukProfileClass** | **Number** | UK profile class of the &#x60;MPAN&#x60;. This will be one of the values in the \&quot;UK Profile Class\&quot; ref table | [optional] 
**ukGspGroup** | **String** | Meter point GSP group | 
**operationType** | **String** | Meter point operation type | 
**meterPointServiceType** | **String** | Meter point service type | 
**readingFrequencyCode** | **String** | The reading frequency code, only displayed if the meterpoint is an &#x60;MPRN&#x60; | [optional] 
**mopMarketParticipant** | **String** | The MOP Market Participant, only displayed if the meterpoint is an &#x60;MPAN&#x60;. | [optional] 
**mamMarketParticipant** | **String** | The MAM Market Participant, only displayed if the meterpoint is an &#x60;MPRN&#x60;. | [optional] 
**DNO** | **String** | Meter point distribution network operator, only displayed if the meterpoint is an &#x60;MPAN&#x60;. | [optional] 
**measurementType** | **String** | If the MeterPoint is of type &#x60;MPAN&#x60; then the measurement type will be shown. This will be Import or Export | [optional] 
**supplyStatus** | **String** | Meter point supply status i.e. &#39;Registered&#39;, &#39;NotSupplied&#39; | 


