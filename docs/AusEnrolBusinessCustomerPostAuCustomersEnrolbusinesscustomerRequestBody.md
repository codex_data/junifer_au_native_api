# JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**businessName** | **String** | The company name of the business customer. | 
**companyNumber** | **String** | The company number of the business customer. This is a free format string that can be used to record both ABNs and ACNs. | [optional] 
**accountManager** | **String** | The username of a pre-existing Junifer user who will be the account manager for this customer. | [optional] 
**customerReference** | **String** | A free format string (up to 255 chars) that is shown on the Customer Details window of the UI. | [optional] 
**proposedDate** | **Date** | The proposed date for customer to transfer or move in | 
**moveInFl** | **Boolean** | Whether the account is for move-in or not | 
**autoRaiseMeterExchangeServiceOrderFl** | **Boolean** | Whether the Meter Exchange Service Order should be automatically raised or not | 
**nominatedRpParticipantCode** | **String** | The RP Market Participant Code nominated for onboarding | [optional] 
**nominatedMpbParticipantCode** | **String** | The MPB Market Participant Code nominated for onboarding | [optional] 
**nominatedMdpParticipantCode** | **String** | The MDP Market Participant Code nominated for onboarding | [optional] 
**nominatedMpcParticipantCode** | **String** | The MPC Market Participant Code nomianted for onboarding | [optional] 
**customerNumber** | **String** | External customer number to be used | [optional] 
**accountNumber** | **String** | External account number to be used | [optional] 
**accountTypeCode** | **String** | Code of the account type to be used for the account | [optional] 
**paymentTermName** | **String** | Name of the payment term to be used for the account | [optional] 
**billingCycle** | **String** | Reference code of the billing cycle to be used for the account | [optional] 
**changeReasonCode** | **String** | CATS change reason code used for the onboarding | [optional] 
**meterReadingConfig** | **String** | Meter reading configuration | [optional] 
**senderReference** | **String** | Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected | 
**reference** | **String** | Unique reference string identifying this particular enrolment | 
**billingEntityCode** | **String** | Code of the billing entity to which the customer must be attached. In systems with more than one billing entity this field is not optional | [optional] 
**marketingOptOutFl** | **Boolean** | A boolean flag indicating whether the customer would like to opt out of any marketing communications | 
**changeOfTenancyFl** | **Boolean** | A boolean flag indicating whether the tenant is also undergoing a change of tenancy | [optional] 
**changeOfTenancyDate** | **Date** | The date of change of Tenancy | [optional] 
**contacts** | [**[AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts]**](AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyContacts.md) | Customer contacts&#39; detail&#39;s array. One of the contacts must be primary contact and must be present in the request. | 
**supplyAddress** | [**AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress**](AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress.md) |  | 
**electricityProduct** | [**AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct**](AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProduct.md) |  | 
**multipleElectricityProducts** | [**[AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts]**](AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyMultipleElectricityProducts.md) | Multiple electricity products the customer chose. Can be used for unbundled agreements. | [optional] 


