# JuniferAuNativeApi.StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**toDt** | **Date** | The date when payment schedule period should stop, usually today | 


