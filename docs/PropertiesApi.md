# JuniferAuNativeApi.PropertiesApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferPropertysIdGet**](PropertiesApi.md#juniferPropertysIdGet) | **GET** /junifer/propertys/{id} | Get property
[**juniferPropertysIdMeterPointsGet**](PropertiesApi.md#juniferPropertysIdMeterPointsGet) | **GET** /junifer/propertys/{id}/meterPoints | Get meter points
[**juniferPropertysPost**](PropertiesApi.md#juniferPropertysPost) | **POST** /junifer/propertys | 



## juniferPropertysIdGet

> GetPropertyGetPropertysIdResponse200 juniferPropertysIdGet(id)

Get property

Get property by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.PropertiesApi();
let id = 3.4; // Number | Property ID
apiInstance.juniferPropertysIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Property ID | 

### Return type

[**GetPropertyGetPropertysIdResponse200**](GetPropertyGetPropertysIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferPropertysIdMeterPointsGet

> GetMeterPointsGetPropertysIdMeterpointsResponse200 juniferPropertysIdMeterPointsGet(id, opts)

Get meter points

Get meter points for property on the current date, or lookupDttm if provided. If none can be found, will return an empty array.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.PropertiesApi();
let id = 3.4; // Number | Property ID
let opts = {
  'lookupDttm': null // Object | Will look for meterpoints on this date, if not provided it will search using today's date.
};
apiInstance.juniferPropertysIdMeterPointsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Property ID | 
 **lookupDttm** | [**Object**](.md)| Will look for meterpoints on this date, if not provided it will search using today&#39;s date. | [optional] 

### Return type

[**GetMeterPointsGetPropertysIdMeterpointsResponse200**](GetMeterPointsGetPropertysIdMeterpointsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferPropertysPost

> CreatePropertyPostPropertysResponse200 juniferPropertysPost(requestBody)



Creates a new property

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.PropertiesApi();
let requestBody = new JuniferAuNativeApi.CreatePropertyPostPropertysRequestBody(); // CreatePropertyPostPropertysRequestBody | Request body
apiInstance.juniferPropertysPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**CreatePropertyPostPropertysRequestBody**](CreatePropertyPostPropertysRequestBody.md)| Request body | 

### Return type

[**CreatePropertyPostPropertysResponse200**](CreatePropertyPostPropertysResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

