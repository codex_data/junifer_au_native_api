# JuniferAuNativeApi.CreateProspectPostProspectsRequestBodyCustomerPrimaryContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contactType** | **String** | Primary contact type name - see the Contact Type ref table for valid values | 
**title** | **String** | Primary contact title - see the Title ref table for valid values | [optional] 
**forename** | **String** | Primary contact forename | [optional] 
**surname** | **String** | Primary contact surname | [optional] 
**email** | **String** | Primary contact email | [optional] 
**dateOfBirth** | **Date** | Primary contact date of birth | [optional] 
**phoneNumber1** | **String** | Primary contact phone number 1 | [optional] 
**phoneNumber2** | **String** | Primary contact phone number 2 | [optional] 
**phoneNumber3** | **String** | Primary contact phone number 3 | [optional] 
**address** | **Object** | Primary contact address | [optional] 
**addressType** | **String** | Primary contact address type - defaults to standard address type | [optional] 
**address1** | **String** | Primary contact address line 1 | [optional] 
**address2** | **String** | Primary contact address line 2 | [optional] 
**address3** | **String** | Primary contact address line 3 | [optional] 
**address4** | **String** | Primary contact address line 4 | [optional] 
**address5** | **String** | Primary contact address line 5 | [optional] 
**postcode** | **String** | Primary contact postcode | [optional] 
**country** | **String** | Primary contact address country - see the Country ref table for valid values | [optional] 
**careOf** | **String** | Primary contact address care of | [optional] 


