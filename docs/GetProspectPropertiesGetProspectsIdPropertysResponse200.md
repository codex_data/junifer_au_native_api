# JuniferAuNativeApi.GetProspectPropertiesGetProspectsIdPropertysResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Property ID | 
**propertyType** | **String** | Property type | 
**addressType** | **String** | Address type | 
**address** | **Object** | Address of the property | 
**links** | [**GetProspectPropertiesGetProspectsIdPropertysResponse200Links**](GetProspectPropertiesGetProspectsIdPropertysResponse200Links.md) |  | 


