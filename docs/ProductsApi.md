# JuniferAuNativeApi.ProductsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAuProductsTariffsForNMIGet**](ProductsApi.md#juniferAuProductsTariffsForNMIGet) | **GET** /junifer/au/products/tariffsForNMI | Get products for nmi



## juniferAuProductsTariffsForNMIGet

> GetProductsForNmiGetAuProductsTariffsfornmiResponse200 juniferAuProductsTariffsForNMIGet(nmi, opts)

Get products for nmi

Returns a list of products that are available for the NMI details given. All prices include sales tax.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProductsApi();
let nmi = "nmi_example"; // String | Identifier of the NMI
let opts = {
  'includeNonPublic': true, // Boolean | Include only those products available to the public
  'expiredOnly': true, // Boolean | Include only those products where the \"Available To\" date has passed
  'billingEntityCode': "billingEntityCode_example" // String | Code of the billing entity owning the products
};
apiInstance.juniferAuProductsTariffsForNMIGet(nmi, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nmi** | **String**| Identifier of the NMI | 
 **includeNonPublic** | **Boolean**| Include only those products available to the public | [optional] 
 **expiredOnly** | **Boolean**| Include only those products where the \&quot;Available To\&quot; date has passed | [optional] 
 **billingEntityCode** | **String**| Code of the billing entity owning the products | [optional] 

### Return type

[**GetProductsForNmiGetAuProductsTariffsfornmiResponse200**](GetProductsForNmiGetAuProductsTariffsfornmiResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

