# JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodySupplyAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**careOf** | **String** | The &#39;Care of&#39; line for the address | 
**buildingOrPropertyName** | **String** | A free text description of the full name used to identify the physical building or property as part of its location. | [optional] 
**buildingOrPropertyName2** | **String** | The overflow from above | [optional] 
**lotNumber** | **String** | The lot reference number allocated to an address prior to street numbering. | [optional] 
**flatOrUnitType** | **String** | Specification of the type of flat or unit which is a separately identifiable portion within a building/complex. | [optional] 
**flatOrUnitNumber** | **String** | Specification of the number of the flat or unit which is a separately identifiable portion within a building/complex. | [optional] 
**floorOrLevelType** | **String** | Floor Type is used to identify the floor or level of a multi-storey building/complex.. | [optional] 
**floorOrLevelNumber** | **String** | Floor Number is used to identify the floor or level of a multi-storey building/complex. | [optional] 
**houseNumber** | **String** | The numeric reference of a house or property. | [optional] 
**houseNumberSuffix** | **String** | The numeric reference of a house or property. | [optional] 
**houseNumber2** | **String** | The numeric reference of a house or property. | [optional] 
**houseNumberSuffix2** | **String** | The numeric reference of a house or property. | [optional] 
**streetName** | **String** | Records the thoroughfare name. | [optional] 
**streetType** | **String** | Records the street type abbreviation. | [optional] 
**streetSuffix** | **String** | Records street suffixes. | [optional] 
**suburbOrPlaceOrLocality** | **String** | The full name of the general locality containing the specific address | [optional] 
**locationDescriptor** | **String** | A general field to capture various references to address locations alongside another physical location. | [optional] 
**stateOrTerritory** | **String** | Defined State or Territory abbreviation. | [optional] 
**postcode** | **String** | Post code | [optional] 
**countryCode** | **String** | An ISO country code. AU is the the only acceptable value! | 


