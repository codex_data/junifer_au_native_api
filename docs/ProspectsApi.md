# JuniferAuNativeApi.ProspectsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferBrokerLinkagesIdGet**](ProspectsApi.md#juniferBrokerLinkagesIdGet) | **GET** /junifer/brokerLinkages/{id} | Get Broker Linkage
[**juniferBrokersGet**](ProspectsApi.md#juniferBrokersGet) | **GET** /junifer/brokers | Lookup broker
[**juniferBrokersIdBrokerLinkagesGet**](ProspectsApi.md#juniferBrokersIdBrokerLinkagesGet) | **GET** /junifer/brokers/{id}/brokerLinkages | Get BrokerLinkages for Broker
[**juniferBrokersIdGet**](ProspectsApi.md#juniferBrokersIdGet) | **GET** /junifer/brokers/{id} | Get Broker
[**juniferProspectsIdAddPropertiesPost**](ProspectsApi.md#juniferProspectsIdAddPropertiesPost) | **POST** /junifer/prospects/{id}/add_properties | Add Properties
[**juniferProspectsIdGet**](ProspectsApi.md#juniferProspectsIdGet) | **GET** /junifer/prospects/{id} | Get prospect
[**juniferProspectsIdOffersGet**](ProspectsApi.md#juniferProspectsIdOffersGet) | **GET** /junifer/prospects/{id}/offers | Get prospect offers
[**juniferProspectsIdPropertysGet**](ProspectsApi.md#juniferProspectsIdPropertysGet) | **GET** /junifer/prospects/{id}/propertys | Get prospect&#39;s properties
[**juniferProspectsPost**](ProspectsApi.md#juniferProspectsPost) | **POST** /junifer/prospects | Create Prospect



## juniferBrokerLinkagesIdGet

> GetBrokerLinkageGetBrokerlinkagesIdResponse200 juniferBrokerLinkagesIdGet(id)

Get Broker Linkage

Returns Broker Linkage of a Broker by their id

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProspectsApi();
let id = "id_example"; // String | Broker Linkage id number
apiInstance.juniferBrokerLinkagesIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Broker Linkage id number | 

### Return type

[**GetBrokerLinkageGetBrokerlinkagesIdResponse200**](GetBrokerLinkageGetBrokerlinkagesIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBrokersGet

> LookupBrokerGetBrokersResponse200 juniferBrokersGet(code)

Lookup broker

Searches for a broker by the specified code. If no broker can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProspectsApi();
let code = "code_example"; // String | Broker's unique code
apiInstance.juniferBrokersGet(code, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **String**| Broker&#39;s unique code | 

### Return type

[**LookupBrokerGetBrokersResponse200**](LookupBrokerGetBrokersResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBrokersIdBrokerLinkagesGet

> GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200 juniferBrokersIdBrokerLinkagesGet(id, opts)

Get BrokerLinkages for Broker

Returns customers and prospects owned by the supplied broker. If an empty results list is returned no customers or prospects could be found

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProspectsApi();
let id = "id_example"; // String | Broker id number
let opts = {
  'type': "type_example", // String | Linkage Type. Can be one of the following: `Customer, Prospect`. Will return all if no type is specified.
  'effectiveDate': "effectiveDate_example" // String | The date for which broker linkages will be checked against. If no date is supplied the returned linkages will be valid for today's date.
};
apiInstance.juniferBrokersIdBrokerLinkagesGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Broker id number | 
 **type** | **String**| Linkage Type. Can be one of the following: &#x60;Customer, Prospect&#x60;. Will return all if no type is specified. | [optional] 
 **effectiveDate** | **String**| The date for which broker linkages will be checked against. If no date is supplied the returned linkages will be valid for today&#39;s date. | [optional] 

### Return type

[**GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200**](GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBrokersIdGet

> GetBrokerGetBrokersIdResponse200 juniferBrokersIdGet(id)

Get Broker

Gets the broker by its Id

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProspectsApi();
let id = 3.4; // Number | Broker id number
apiInstance.juniferBrokersIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Broker id number | 

### Return type

[**GetBrokerGetBrokersIdResponse200**](GetBrokerGetBrokersIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferProspectsIdAddPropertiesPost

> juniferProspectsIdAddPropertiesPost(id, requestBody)

Add Properties

Links properties to the prospect

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProspectsApi();
let id = 3.4; // Number | Prospect ID
let requestBody = null; // Object | Request body
apiInstance.juniferProspectsIdAddPropertiesPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Prospect ID | 
 **requestBody** | **Object**| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferProspectsIdGet

> GetProspectGetProspectsIdResponse200 juniferProspectsIdGet(id)

Get prospect

Gets prospect by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProspectsApi();
let id = 3.4; // Number | Prospect ID
apiInstance.juniferProspectsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Prospect ID | 

### Return type

[**GetProspectGetProspectsIdResponse200**](GetProspectGetProspectsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferProspectsIdOffersGet

> GetProspectOffersGetProspectsIdOffersResponse200 juniferProspectsIdOffersGet(id)

Get prospect offers

Gets the list of offers that are linked to a prospect by the prospect id.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProspectsApi();
let id = 3.4; // Number | Prospect's id
apiInstance.juniferProspectsIdOffersGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Prospect&#39;s id | 

### Return type

[**GetProspectOffersGetProspectsIdOffersResponse200**](GetProspectOffersGetProspectsIdOffersResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferProspectsIdPropertysGet

> GetProspectPropertiesGetProspectsIdPropertysResponse200 juniferProspectsIdPropertysGet(id)

Get prospect&#39;s properties

Returns properties owned by the supplied prospect. If an empty results list is returned no properties could be found

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProspectsApi();
let id = 3.4; // Number | Prospect ID
apiInstance.juniferProspectsIdPropertysGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Prospect ID | 

### Return type

[**GetProspectPropertiesGetProspectsIdPropertysResponse200**](GetProspectPropertiesGetProspectsIdPropertysResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferProspectsPost

> CreateProspectPostProspectsResponse200 juniferProspectsPost(requestBody)

Create Prospect

Create a new prospect with a new customer

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.ProspectsApi();
let requestBody = new JuniferAuNativeApi.CreateProspectPostProspectsRequestBody(); // CreateProspectPostProspectsRequestBody | Request body
apiInstance.juniferProspectsPost(requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | [**CreateProspectPostProspectsRequestBody**](CreateProspectPostProspectsRequestBody.md)| Request body | 

### Return type

[**CreateProspectPostProspectsResponse200**](CreateProspectPostProspectsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

