# JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Quote ID | 
**number** | **String** | Quote number | 
**quoteType** | **String** | Type of the quote | 
**description** | **String** | Description of the quote | 
**createdDttm** | **Date** | Date and time when the quote was created | 
**createdUser** | **String** | User who created the quote | 
**archived** | **Boolean** | A boolean flag to indicate if the quote has been archived or not | 
**contractSpend** | **Number** | Total gross value for this quote | 
**meterPoints** | [**GetQuotesGetOffersIdQuotesResponse200MeterPoints**](GetQuotesGetOffersIdQuotesResponse200MeterPoints.md) |  | [optional] 
**links** | [**GetQuotesGetOffersIdQuotesResponse200Links**](GetQuotesGetOffersIdQuotesResponse200Links.md) |  | 


