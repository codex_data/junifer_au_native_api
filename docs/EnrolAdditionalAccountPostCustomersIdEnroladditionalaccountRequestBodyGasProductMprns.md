# JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProductMprns

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mprn** | **String** | MPRN identifier | 


