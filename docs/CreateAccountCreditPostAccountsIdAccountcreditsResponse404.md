# JuniferAuNativeApi.CreateAccountCreditPostAccountsIdAccountcreditsResponse404

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;AccountCreditReasonNotFound&#x60; - &#39;accountCreditReasonName&#39; not recognised. Account credit reason must be a valid type * &#x60;AccountTransactionTypeNotFound&#x60; - No AccountTransactionType for AccountCredit | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `AccountCreditReasonNotFound` (value: `"AccountCreditReasonNotFound"`)

* `AccountTransactionTypeNotFound` (value: `"AccountTransactionTypeNotFound"`)




