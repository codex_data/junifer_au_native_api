# JuniferAuNativeApi.GetQuoteGetQuotesIdResponse200QuoteFiles

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Quote file ID | 
**display** | **String** | Quote file description | 
**viewable** | **Boolean** | A boolean flag to indicate if the quote file is viewable | 
**createdDttm** | **Date** | Date and time when the quote file was created | 
**links** | [**GetQuoteGetQuotesIdResponse200QuoteFilesLinks**](GetQuoteGetQuotesIdResponse200QuoteFilesLinks.md) |  | 


