# JuniferAuNativeApi.GetTicketGetTicketsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | ticket ID | 
**parentTicketId** | **Number** | The ID of the parent ticket | [optional] 
**ticketDefinitionCode** | **String** | Ticket definition code | 
**ticketDefinitionName** | **String** | Ticket definition name | 
**ticketPriority** | **String** | Ticket priority | 
**ticketStepCode** | **String** | Ticket step code | 
**ticketStepName** | **String** | Ticket step name | 
**status** | **String** | Ticket status. Can be one of following: &#x60;Open, Closed, Cancelled, Error&#x60; | 
**keyIdentifier** | **String** | Ticket key identifier | 
**summary** | **String** | Ticket summary | 
**description** | **String** | Ticket description | [optional] 
**createdDttm** | **Date** | Ticket creation date and time | 
**dueDttm** | **Date** | Ticket due date and time | [optional] 
**cancelDttm** | **Date** | Date and time when ticket was cancelled | [optional] 
**ticketCancelReason** | **String** | The reason ticket was cancelled | [optional] 
**assignedUserTeam** | **String** | Team assigned to deal with this ticket | [optional] 
**stepStartDttm** | **Date** | Ticket step start date and time | [optional] 
**stepScheduleDttm** | **Date** | Ticket step scheduled date and time | [optional] 
**stepDueDttm** | **Date** | Ticket step due date and time | [optional] 
**ticketEntities** | [**CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities**](CreateAccountTicketPostAccountsIdTicketsResponse200TicketEntities.md) |  | 
**links** | [**GetTicketGetTicketsIdResponse200Links**](GetTicketGetTicketsIdResponse200Links.md) |  | 


