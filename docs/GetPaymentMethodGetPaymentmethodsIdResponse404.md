# JuniferAuNativeApi.GetPaymentMethodGetPaymentmethodsIdResponse404

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;NotFound&#x60; - Could not find &#39;payment method&#39; with Id &#39;paymentMethodId&#39; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `NotFound` (value: `"NotFound"`)




