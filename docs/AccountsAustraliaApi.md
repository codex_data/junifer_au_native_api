# JuniferAuNativeApi.AccountsAustraliaApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAuAccountsIdDiscountsGet**](AccountsAustraliaApi.md#juniferAuAccountsIdDiscountsGet) | **GET** /junifer/au/accounts/{id}/discounts | Get discounts linked to the account specified by the ID
[**juniferAuAccountsIdDiscountsPost**](AccountsAustraliaApi.md#juniferAuAccountsIdDiscountsPost) | **POST** /junifer/au/accounts/{id}/discounts | Links a discount to the account specified by the ID
[**juniferAuAccountsIdRenewalPost**](AccountsAustraliaApi.md#juniferAuAccountsIdRenewalPost) | **POST** /junifer/au/accounts/{id}/renewal | Renew account with one or more new agreements



## juniferAuAccountsIdDiscountsGet

> AusGetDiscountsGetAuAccountsIdDiscountsResponse200 juniferAuAccountsIdDiscountsGet(id)

Get discounts linked to the account specified by the ID

Get account&#39;s discounts. This is an Australia specific API.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsAustraliaApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAuAccountsIdDiscountsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**AusGetDiscountsGetAuAccountsIdDiscountsResponse200**](AusGetDiscountsGetAuAccountsIdDiscountsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAuAccountsIdDiscountsPost

> juniferAuAccountsIdDiscountsPost(id, requestBody)

Links a discount to the account specified by the ID

Links a discount to the account specified by the ID. This is an Australia specific API.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsAustraliaApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.AusLinkDiscountPostAuAccountsIdDiscountsRequestBody(); // AusLinkDiscountPostAuAccountsIdDiscountsRequestBody | Request body
apiInstance.juniferAuAccountsIdDiscountsPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**AusLinkDiscountPostAuAccountsIdDiscountsRequestBody**](AusLinkDiscountPostAuAccountsIdDiscountsRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## juniferAuAccountsIdRenewalPost

> AusRenewAccountPostAuAccountsIdRenewalResponse200 juniferAuAccountsIdRenewalPost(id, requestBody)

Renew account with one or more new agreements

Renews the account specified by the account ID by creating one or more new agreements, which can optionally replace one or more existing agreements specified. Electricity product details of the new agreement(s) must be supplied. Will raise a ticket, if one is specified in the property &#x60;Agreement Creation Properties &gt; API Agreement Setup Ticket&#x60;. This is an Australia specific API not to be confused with &#x60;junifer/accounts/{id}/renewal&#x60;.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsAustraliaApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalRequestBody(); // AusRenewAccountPostAuAccountsIdRenewalRequestBody | Request body
apiInstance.juniferAuAccountsIdRenewalPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**AusRenewAccountPostAuAccountsIdRenewalRequestBody**](AusRenewAccountPostAuAccountsIdRenewalRequestBody.md)| Request body | 

### Return type

[**AusRenewAccountPostAuAccountsIdRenewalResponse200**](AusRenewAccountPostAuAccountsIdRenewalResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

