# JuniferAuNativeApi.GetSepaDirectDebitGetSepadirectdebitsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | SEPA Direct Debit Id | 
**accountName** | **String** | account holder name | 
**iban** | **Number** | IBAN | 
**mandateReference** | **String** | Mandate Reference | 
**authorisedDttm** | **Date** | Date when the mandate was authorised | [optional] 
**terminatedDttm** | **Date** | Date when the mandate was terminated | [optional] 
**links** | [**GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links**](GetSepaDirectDebitGetSepadirectdebitsIdResponse200Links.md) |  | 


