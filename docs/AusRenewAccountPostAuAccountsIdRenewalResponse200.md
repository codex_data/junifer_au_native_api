# JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[AusRenewAccountPostAuAccountsIdRenewalResponse200Results]**](AusRenewAccountPostAuAccountsIdRenewalResponse200Results.md) | Array with ids of new agreements created | 


