# JuniferAuNativeApi.GetAlertGetAlertsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Alert ID | 
**group** | **String** | Alert group | 
**code** | **String** | Alert code | 
**summary** | **String** | Alert summary | 
**key** | **String** | Unique alert key | 
**description** | **String** | Alert description | [optional] 
**severity** | **String** | Alert severity | 
**firstRaisedDttm** | **Date** | The date and time this alert was first raised (if not cleared) | [optional] 
**lastRaisedDttm** | **Date** | The date and time this alert was last raised (if not cleared) | [optional] 
**raisedCount** | **Number** | The number of times this alert has been raised (if not cleared) | [optional] 
**clearedDttm** | **Date** | The date and time this alert was cleared (if cleared) | [optional] 
**clearedBy** | **String** | The Junifer user that cleared this alert (if cleared) | [optional] 
**links** | [**GetAlertGetAlertsIdResponse200Links**](GetAlertGetAlertsIdResponse200Links.md) |  | 


