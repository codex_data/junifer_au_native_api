# JuniferAuNativeApi.LookUpCustomerGetCustomersResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;RequiredParameterMissing&#x60; - The required &#x60;number&#x60; lookup parameter is missing | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)




