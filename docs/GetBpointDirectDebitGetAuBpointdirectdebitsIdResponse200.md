# JuniferAuNativeApi.GetBpointDirectDebitGetAuBpointdirectdebitsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | BPOINT Direct Debit object id | 
**accountName** | **String** | Account name | 
**mandateReference** | **String** | Mandate Reference | 
**paymentType** | **String** | Payment type. Can be either &#x60;BankAccount&#x60; or &#x60;CreditCard&#x60; | 
**authorisedDttm** | **Date** | Date when the Direct Debit was authorised | [optional] 
**terminatedDttm** | **Date** | Date when the Direct Debit was terminated | [optional] 
**status** | **String** | Current status of the Direct Debit | 


