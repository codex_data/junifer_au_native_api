# JuniferAuNativeApi.GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**utilityMarket** | **[Object]** | Utility Market. Can be one of the following: &#x60;UK Electricity or UK Gas&#x60; | 


