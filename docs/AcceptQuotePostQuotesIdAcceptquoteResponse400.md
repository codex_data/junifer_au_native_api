# JuniferAuNativeApi.AcceptQuotePostQuotesIdAcceptquoteResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;BadRequest&#x60; - Unable to accept a quote for an offer that is cancelled | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `BadRequest` (value: `"BadRequest"`)




