# JuniferAuNativeApi.UpdateSepaDirectDebitPutSepadirectdebitsIdResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;IbanNotValid&#x60; - \&quot;IBAN is invalid: \&quot; + errorMessage | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `IbanNotValid` (value: `"IbanNotValid"`)




