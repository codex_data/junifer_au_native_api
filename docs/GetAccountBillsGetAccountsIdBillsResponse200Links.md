# JuniferAuNativeApi.GetAccountBillsGetAccountsIdBillsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this bill | 
**account** | **String** | Link to the billed account | 


