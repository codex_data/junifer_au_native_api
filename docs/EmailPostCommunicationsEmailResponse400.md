# JuniferAuNativeApi.EmailPostCommunicationsEmailResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;RequiredParameterMissing&#x60; - \&quot;One or more of the required query parameters are missing: &#39;toAddress&#39;, &#39;subject&#39;, &#39;body&#39;\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)




