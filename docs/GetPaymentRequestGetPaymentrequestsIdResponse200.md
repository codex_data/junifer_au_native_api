# JuniferAuNativeApi.GetPaymentRequestGetPaymentrequestsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Payment request ID | 
**currency** | **String** | Currency ISO code | 
**amount** | **Number** | Amount of money to be paid or repaid | 
**status** | **String** | Current state of the payment | 
**paymentMethodType** | **String** | Payment method type | 
**createdDttm** | **Date** | Date and time when the payment request was created | 
**description** | **String** | Payment description | [optional] 
**links** | [**GetPaymentRequestGetPaymentrequestsIdResponse200Links**](GetPaymentRequestGetPaymentrequestsIdResponse200Links.md) |  | 


