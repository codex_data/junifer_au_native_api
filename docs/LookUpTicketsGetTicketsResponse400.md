# JuniferAuNativeApi.LookUpTicketsGetTicketsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;RequiredParameterMissing&#x60; - One of &#39;parentTicketId&#39; or/and &#39;ticketDefinitionCode&#39; lookup parameter is missing * &#x60;MissingTicketDefinitionCode&#x60; - The &#39;ticketDefinitionCode&#39; is missing! In order to search using &#39;ticketStepCode&#39; field you need to give a &#39;ticketDefinitionCode&#39; * &#x60;IncorrectTicketStatus&#x60; - The &#39;ticketStatus&#39; is incorrect! Ticket status can contain the following: &#x60;Open, Closed, Cancelled, Error&#x60; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)

* `MissingTicketDefinitionCode` (value: `"MissingTicketDefinitionCode"`)

* `IncorrectTicketStatus` (value: `"IncorrectTicketStatus"`)




