# JuniferAuNativeApi.TariffInformationGetAccountsIdTariffinformationResponse200TIL

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemName** | **String** | The TIL&#39;s item name eg \&quot;Unit Rate\&quot; | [optional] 
**itemValue** | **String** | The TIL&#39;s item value eg \&quot;Fixed Rate\&quot; | [optional] 
**exclVAT** | **String** | The price value excluding VAT. | [optional] 
**inclVAT** | **String** | The price value including VAT. | [optional] 


