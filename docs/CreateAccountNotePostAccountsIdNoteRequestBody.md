# JuniferAuNativeApi.CreateAccountNotePostAccountsIdNoteRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subject** | **String** | Note Subject. Will default to &#39;General Note&#39; | [optional] 
**type** | **String** | Note Type. Can be one of the following: &#x60;Note, Email, File, Phone, Letter&#x60;. Will default to &#39;Note&#39;. | [optional] 
**summary** | **String** | A brief summary of the Note | 
**content** | **String** | The detailed content of the Note | [optional] 


