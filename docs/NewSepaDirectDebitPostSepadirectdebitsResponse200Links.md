# JuniferAuNativeApi.NewSepaDirectDebitPostSepadirectdebitsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentMethod** | **String** | Link to this SEPA Direct Debit payment method | 


