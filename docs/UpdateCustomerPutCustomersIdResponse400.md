# JuniferAuNativeApi.UpdateCustomerPutCustomersIdResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;UnrecognisedTitle&#x60; - The supplied &#39;title&#39; parameter &#x60;title&#x60; is not recognised | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `UnrecognisedTitle` (value: `"UnrecognisedTitle"`)




