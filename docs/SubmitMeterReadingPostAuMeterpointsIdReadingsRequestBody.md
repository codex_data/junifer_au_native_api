# JuniferAuNativeApi.SubmitMeterReadingPostAuMeterpointsIdReadingsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ignoreWarnings** | **Boolean** | If true, don&#39;t fail the call when non-critical validation checks fail, such as null or negative consumption, or consumption outside the thresholds defined in the validation properties.Defaults to false | [optional] 
**submissionMode** | **String** | Supports different behaviour upon submission of reads. Default behaviour is &#x60;Submit&#x60;, can also be &#x60;Replace&#x60; which will create new reads or replace any duplicates. | [optional] 
**validateOnly** | **Boolean** | If true, the read will go through all the validation checks as a normal read would, but it will not be submitted to Junifer. | [optional] 
**meterIdentifier** | **String** | Meter identifier | 
**registerIdentifier** | **String** | Meter register to which the reading belongs to. Optional if meter has only one register | 
**readingDttm** | **Date** | Date and time of the reading | 
**sequenceType** | **String** | The type of the meter reading. Can be one of the following: &#x60;Normal,First,Last&#x60; | 
**source** | **String** | The source of the meter reading. Depends on the configuration of the Junifer installation | 
**quality** | **String** | The quality of the meter reading.Depends on the configuration of the Junifer installation | 
**cumulative** | **Number** | The cumulative meter reading being submitted | 
**description** | **String** | Where the read is submitted from. | [optional] 


