# JuniferAuNativeApi.GetCommunicationsGetCommunicationsIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image** | **String** | link to the communication message | 


