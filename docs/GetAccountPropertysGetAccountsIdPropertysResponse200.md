# JuniferAuNativeApi.GetAccountPropertysGetAccountsIdPropertysResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the property | 
**propertyType** | **String** | The type of the property. This will be one of the values in the \&quot;Property Type\&quot; ref table. | 
**address** | [**GetAccountPropertysGetAccountsIdPropertysResponse200Address**](GetAccountPropertysGetAccountsIdPropertysResponse200Address.md) |  | 
**country** | **String** | Property address country | 
**countryCode** | **String** | Property address country code | 


