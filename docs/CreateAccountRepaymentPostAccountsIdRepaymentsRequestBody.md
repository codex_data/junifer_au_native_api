# JuniferAuNativeApi.CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Number** | How much money you wish to repay | 
**description** | **String** | Repayment description | [optional] 


