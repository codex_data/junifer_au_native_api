# JuniferAuNativeApi.GetPayPalDirectDebitGetAuPaypaldirectdebitsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | PayPal Direct Debit object id | 
**accountName** | **String** | Account name | 
**mandateReference** | **String** | Mandate Reference | 
**authorisedDttm** | **Date** | Date when the Direct Debit was authorised | [optional] 
**terminatedDttm** | **Date** | Date when the Direct Debit was terminated | [optional] 
**status** | **String** | Current status of the Direct Debit | 


