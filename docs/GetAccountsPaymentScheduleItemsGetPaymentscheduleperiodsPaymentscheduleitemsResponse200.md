# JuniferAuNativeApi.GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Payment Schedule Item ID | 
**currency** | **String** | Currency ISO code | 
**debtRecoveryAmount** | **Number** | The debt recovery amount associated with the bill period | 
**amount** | **Number** | The amount associated with the bill period | 
**dueDt** | **Date** | The date the payment schedule item is due | 
**links** | [**GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links**](GetAccountsPaymentScheduleItemsGetPaymentscheduleperiodsPaymentscheduleitemsResponse200Links.md) |  | 


