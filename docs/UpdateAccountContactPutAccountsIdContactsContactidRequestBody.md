# JuniferAuNativeApi.UpdateAccountContactPutAccountsIdContactsContactidRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Contact&#39;s title. Can be &#x60;Dr&#x60;, &#x60;Miss&#x60;, &#x60;Mr&#x60;, &#x60;Mrs&#x60;, &#x60;Ms&#x60;, &#x60;Prof&#x60; | [optional] 
**forename** | **String** | Contact&#39;s first name | [optional] 
**surname** | **String** | Contact&#39;s surname | 
**email** | **String** | Email address | [optional] 
**dateOfBirth** | **Date** | Contact&#39;s date of birth | [optional] 
**phoneNumber1** | **String** | Phone number 1 | [optional] 
**phoneNumber2** | **String** | Phone number 2 | [optional] 
**phoneNumber3** | **String** | Phone number 3 | [optional] 
**billDelivery** | **String** | Contacts Bill Delivery Preference. Can be &#x60;Email&#x60;, &#x60;Mail&#x60;, &#x60;Both&#x60;, &#x60;None&#x60; | [optional] 
**receivePost** | **Boolean** | Contacts communication postal delivery preference. If left out, will be set to false. | 
**receiveEmail** | **Boolean** | Contacts communication email delivery preference. If left out, will be set to false. | 
**receiveSMS** | **Boolean** | Contacts communication SMS delivery preference. If left out, will be set to false. | 


