# JuniferAuNativeApi.ContactsGetCustomersIdContactsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Contact Id | 
**contactType** | **String** | Contact Type | 
**primary** | **Boolean** | Whether the contact is the primary | 
**title** | **String** | Customer&#39;s contact title | [optional] 
**forename** | **String** | Customer&#39;s contact forename | [optional] 
**surname** | **String** | Customer&#39;s contact surname | 
**initials** | **String** | Customer&#39;s contact initials | [optional] 
**jobTitle** | **String** | Customer&#39;s contact job title | [optional] 
**email** | **String** | Customer&#39;s contact email | 
**dateOfBirth** | **Date** | Customer&#39;s date of birth | [optional] 
**phoneNumber1** | **String** | Customer&#39;s primary contact phone number | [optional] 
**phoneNumber2** | **String** | Customer&#39;s secondary contact phone number | [optional] 
**phoneNumber3** | **String** | Customer&#39;s third contact phone number | [optional] 
**customerContactId** | **Number** | Customer contact Id | 
**cancelled** | **Boolean** | True if this contact is no longer associated with the customer | 
**fromDttm** | **Date** | Contacts from date time | 
**toDttm** | **Date** | Contacts to date time | [optional] 
**links** | [**ContactsGetAccountsIdContactsResponse200Links**](ContactsGetAccountsIdContactsResponse200Links.md) |  | 


