# JuniferAuNativeApi.ContactsGetAccountsIdContactsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Contact Id | 
**contactType** | **String** | Contact Type | 
**primary** | **Boolean** | Whether the contact is the primary | 
**title** | **String** | Account&#39;s contact title | [optional] 
**forename** | **String** | Account&#39;s contact forename | [optional] 
**surname** | **String** | Account&#39;s contact surname | 
**initials** | **String** | Account&#39;s contact initials | [optional] 
**jobTitle** | **String** | Account&#39;s contact jobTitle | [optional] 
**address** | [**ContactsGetAccountsIdContactsResponse200Address**](ContactsGetAccountsIdContactsResponse200Address.md) |  | [optional] 
**email** | **String** | Account&#39;s contact email | 
**dateOfBirth** | **Date** | Account&#39;s date of birth | [optional] 
**phoneNumber1** | **String** | Account&#39;s primary contact phone number | [optional] 
**phoneNumber2** | **String** | Account&#39;s secondary contact phone number | [optional] 
**phoneNumber3** | **String** | Account&#39;s third contact phone number | [optional] 
**accountContactId** | **Number** | Account contact Id | 
**billDelivery** | **String** | Contacts Bill Delivery Preference. Can be &#x60;Email&#x60;, &#x60;Mail&#x60;, &#x60;Both&#x60; | [optional] 
**receivePost** | **Boolean** | Contacts communication postal delivery preference. | 
**receiveEmail** | **Boolean** | Contacts communication email delivery preference. | 
**receiveSMS** | **Boolean** | Contacts communication SMS delivery preference. | 
**cancelled** | **Boolean** | True if this contact is no longer associated with the customer | 
**fromDttm** | **Date** | Contacts from date time | 
**toDttm** | **Date** | Contacts to date time | [optional] 
**links** | [**ContactsGetAccountsIdContactsResponse200Links**](ContactsGetAccountsIdContactsResponse200Links.md) |  | 


