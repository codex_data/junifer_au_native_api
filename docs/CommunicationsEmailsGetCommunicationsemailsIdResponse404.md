# JuniferAuNativeApi.CommunicationsEmailsGetCommunicationsemailsIdResponse404

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;NotFound&#x60; - \&quot;No sms found for communication \&quot; + communication | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `NotFound` (value: `"NotFound"`)




