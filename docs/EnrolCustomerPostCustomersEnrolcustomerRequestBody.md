# JuniferAuNativeApi.EnrolCustomerPostCustomersEnrolcustomerRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**senderReference** | **String** | Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected | 
**reference** | **String** | Unique reference string identifying this particular enrolment | 
**billingEntityCode** | **String** | Code of the billing entity to which the customer must be attached | [optional] 
**marketingOptOutFl** | **Boolean** | A boolean flag indicating whether the customer would like to opt out of any marketing communications | 
**submittedSource** | **String** | The source of the enrolment. This overrides the default source (WebService) if set | [optional] 
**changeOfTenancyFl** | **Boolean** | A boolean flag indicating whether the customer is a change of tenancy. | [optional] 
**contacts** | [**[EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts]**](EnrolCustomerPostCustomersEnrolcustomerRequestBodyContacts.md) | Customer contacts&#39; detail&#39;s array. One of the contacts must be primary contact and must be present in the request. | 
**supplyAddress** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.md) |  | 
**directDebitInfo** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.md) |  | [optional] 
**electricityProduct** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct.md) |  | 
**gasProduct** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.md) |  | 
**gasEstimate** | [**EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.md) |  | [optional] 
**electricityEstimate** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate.md) |  | [optional] 


