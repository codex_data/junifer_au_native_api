# JuniferAuNativeApi.AusEnrolBusinessCustomerPostAuCustomersEnrolbusinesscustomerRequestBodyElectricityProductMeterPoints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**core** | **String** | NMI identifier | [optional] 


