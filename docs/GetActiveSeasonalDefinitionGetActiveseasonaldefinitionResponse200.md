# JuniferAuNativeApi.GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lowPercentage** | **Number** | The percentage decrease of direct debit amount during the low usage months | 
**highPercentage** | **Number** | The percentage increase of direct debit amount during the high usage months | 
**createdDttm** | **Date** | Date when the seasonal definition was created | 
**activeDttm** | **Date** | Date when the seasonal definition became the active definition | 
**months** | [**GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months**](GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months.md) |  | 


