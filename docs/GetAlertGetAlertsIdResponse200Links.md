# JuniferAuNativeApi.GetAlertGetAlertsIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this alert | 


