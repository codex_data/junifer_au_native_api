# JuniferAuNativeApi.GetStandingDataForNmiGetAuNmisStandingdatafornmiResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nmi** | **String** | Identifier of a NMI identified from the supplied details | [optional] 
**jurisdictionCode** | **String** | Jurisdiction code | [optional] 
**customerClassificationCode** | **String** | Customer classification code | [optional] 
**customerThresholdCode** | **String** | Customer threshold code | [optional] 
**suburbOrPlaceOrLocality** | **String** | Suburb for the address the meter is located at | [optional] 
**stateOrTerritory** | **String** | State for the address the meter is located at | [optional] 
**postcode** | **String** | Postcode of the address the meter is located at | [optional] 
**deliveryPointIdentifier** | **Number** |  | [optional] 
**streetName** | **String** | Street name for the address the meter is located at | [optional] 
**streetSuffix** | **String** | Street suffix for the address the meter is located at | [optional] 
**streetType** | **String** | Street type for the address the meter is located at | [optional] 
**buildingOrPropertyName** | **String** | Property name for the address the meter is located at | [optional] 
**buildingOrPropertyName2** | **String** | Property name for the address the meter is located at | [optional] 
**flatOrUnitNumber** | **String** | Unit number for the address the meter is located at | [optional] 
**flatOrUnitType** | **String** | Unit type for the address the meter is located at | [optional] 
**floorOrLevelNumber** | **Number** | Floor number for the address the meter is located at | [optional] 
**floorOrLevelType** | **String** | Floor type for the address the meter is located at | [optional] 
**houseNumber** | **Number** | House number for the address the meter is located at | [optional] 
**houseNumberSuffix** | **String** | House suffix for the address the meter is located at | [optional] 
**houseNumber2** | **Number** | House number for the address the meter is located at | [optional] 
**houseNumber2Suffix** | **String** | House suffix for the address the meter is located at | [optional] 
**locationDescriptor** | **String** | Location descriptor for the address the meter is located at | [optional] 
**lotNumber** | **String** | Lot number for the address the meter is located at | [optional] 
**postalDeliveryNumberPrefix** | **String** | Lot number for the address the meter is located at | [optional] 
**postalDeliveryNumberSuffix** | **String** | Lot number for the address the meter is located at | [optional] 
**postalDeliveryNumberValue** | **Number** | Lot number for the address the meter is located at | [optional] 
**postalDeliveryType** | **String** | Lot number for the address the meter is located at | [optional] 
**addressLine1** | **String** | Lot number for the address the meter is located at | [optional] 
**addressLine2** | **String** | Lot number for the address the meter is located at | [optional] 
**addressLine3** | **String** | Lot number for the address the meter is located at | [optional] 
**role** | **String** | Lot number for the address the meter is located at | [optional] 
**party** | **String** | Lot number for the address the meter is located at | [optional] 


