# JuniferAuNativeApi.CreditsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferCreditFilesIdGet**](CreditsApi.md#juniferCreditFilesIdGet) | **GET** /junifer/creditFiles/{id} | Get credit file
[**juniferCreditFilesIdImageGet**](CreditsApi.md#juniferCreditFilesIdImageGet) | **GET** /junifer/creditFiles/{id}/image | Get credit file image
[**juniferCreditsIdCreditBreakdownLinesGet**](CreditsApi.md#juniferCreditsIdCreditBreakdownLinesGet) | **GET** /junifer/credits/{id}/creditBreakdownLines | Get credit&#39;s breakdown lines
[**juniferCreditsIdGet**](CreditsApi.md#juniferCreditsIdGet) | **GET** /junifer/credits/{id} | Get Credit



## juniferCreditFilesIdGet

> GetCreditFileGetCreditfilesIdResponse200 juniferCreditFilesIdGet(id)

Get credit file

Get credit file record by its ID.  Note that the ID is of the credit file, NOT the credit. To get the correct credit files follow the link URL in the response of e.g. Get Credit API, where the correct credit file ID is prepopulated in the URL by the system.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CreditsApi();
let id = 3.4; // Number | credit file ID
apiInstance.juniferCreditFilesIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| credit file ID | 

### Return type

[**GetCreditFileGetCreditfilesIdResponse200**](GetCreditFileGetCreditfilesIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCreditFilesIdImageGet

> GetCreditFileImageGetCreditfilesIdImageResponse200 juniferCreditFilesIdImageGet(id)

Get credit file image

Initiates credit file image (usually PDF file) download if the file is available

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CreditsApi();
let id = 3.4; // Number | Credit file ID
apiInstance.juniferCreditFilesIdImageGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Credit file ID | 

### Return type

[**GetCreditFileImageGetCreditfilesIdImageResponse200**](GetCreditFileImageGetCreditfilesIdImageResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCreditsIdCreditBreakdownLinesGet

> GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200 juniferCreditsIdCreditBreakdownLinesGet(id)

Get credit&#39;s breakdown lines

Get credit&#39;s breakdown lines by its credit ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CreditsApi();
let id = 3.4; // Number | Credit ID
apiInstance.juniferCreditsIdCreditBreakdownLinesGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Credit ID | 

### Return type

[**GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200**](GetCreditBreakdownLinesGetCreditsIdCreditbreakdownlinesResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferCreditsIdGet

> GetCreditGetCreditsIdResponse200 juniferCreditsIdGet(id, opts)

Get Credit

Get credit record by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.CreditsApi();
let id = 3.4; // Number | Credit ID
let opts = {
  'contactId': 3.4 // Number | Restrict creditFiles to a specific contact
};
apiInstance.juniferCreditsIdGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Credit ID | 
 **contactId** | **Number**| Restrict creditFiles to a specific contact | [optional] 

### Return type

[**GetCreditGetCreditsIdResponse200**](GetCreditGetCreditsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

