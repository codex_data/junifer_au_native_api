# JuniferAuNativeApi.AccountsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAccountsAccountNumberNumGet**](AccountsApi.md#juniferAccountsAccountNumberNumGet) | **GET** /junifer/accounts/accountNumber/{num} | Get account by account number
[**juniferAccountsGet**](AccountsApi.md#juniferAccountsGet) | **GET** /junifer/accounts | Account lookup
[**juniferAccountsIdAccountCreditsGet**](AccountsApi.md#juniferAccountsIdAccountCreditsGet) | **GET** /junifer/accounts/{id}/accountCredits | Get account credits
[**juniferAccountsIdAccountCreditsPost**](AccountsApi.md#juniferAccountsIdAccountCreditsPost) | **POST** /junifer/accounts/{id}/accountCredits | Create account credit
[**juniferAccountsIdAccountDebitsGet**](AccountsApi.md#juniferAccountsIdAccountDebitsGet) | **GET** /junifer/accounts/{id}/accountDebits | Get account debits
[**juniferAccountsIdAccountDebitsPost**](AccountsApi.md#juniferAccountsIdAccountDebitsPost) | **POST** /junifer/accounts/{id}/accountDebits | Create account debit
[**juniferAccountsIdAccountReviewPeriodsGet**](AccountsApi.md#juniferAccountsIdAccountReviewPeriodsGet) | **GET** /junifer/accounts/{id}/accountReviewPeriods | Get account&#39;s review periods
[**juniferAccountsIdAccountReviewPeriodsPost**](AccountsApi.md#juniferAccountsIdAccountReviewPeriodsPost) | **POST** /junifer/accounts/{id}/accountReviewPeriods | Create account review period
[**juniferAccountsIdAgreementsGet**](AccountsApi.md#juniferAccountsIdAgreementsGet) | **GET** /junifer/accounts/{id}/agreements | Get account&#39;s agreements
[**juniferAccountsIdAllPayReferenceGet**](AccountsApi.md#juniferAccountsIdAllPayReferenceGet) | **GET** /junifer/accounts/{id}/allPayReference | Get account&#39;s AllPay Reference Number
[**juniferAccountsIdBillRequestsGet**](AccountsApi.md#juniferAccountsIdBillRequestsGet) | **GET** /junifer/accounts/{id}/billRequests | Get account&#39;s bill requests
[**juniferAccountsIdBillsGet**](AccountsApi.md#juniferAccountsIdBillsGet) | **GET** /junifer/accounts/{id}/bills | Get account&#39;s bills
[**juniferAccountsIdCancelAccountReviewPeriodDelete**](AccountsApi.md#juniferAccountsIdCancelAccountReviewPeriodDelete) | **DELETE** /junifer/accounts/{id}/cancelAccountReviewPeriod | Cancel account review period
[**juniferAccountsIdChangeAccountCustomerPut**](AccountsApi.md#juniferAccountsIdChangeAccountCustomerPut) | **PUT** /junifer/accounts/{id}/changeAccountCustomer | Change the customer linked to an account
[**juniferAccountsIdCommercialProductDetailsGet**](AccountsApi.md#juniferAccountsIdCommercialProductDetailsGet) | **GET** /junifer/accounts/{id}/commercial/productDetails | Get account&#39;s product details for commercial customer
[**juniferAccountsIdCommunicationsGet**](AccountsApi.md#juniferAccountsIdCommunicationsGet) | **GET** /junifer/accounts/{id}/communications | Get account&#39;s communications
[**juniferAccountsIdContactsContactIdPut**](AccountsApi.md#juniferAccountsIdContactsContactIdPut) | **PUT** /junifer/accounts/{id}/contacts/{contactId} | Update account&#39;s contact details
[**juniferAccountsIdContactsGet**](AccountsApi.md#juniferAccountsIdContactsGet) | **GET** /junifer/accounts/{id}/contacts | Contacts
[**juniferAccountsIdCreditsGet**](AccountsApi.md#juniferAccountsIdCreditsGet) | **GET** /junifer/accounts/{id}/credits | Get account&#39;s credits
[**juniferAccountsIdEnrolMeterPointsPost**](AccountsApi.md#juniferAccountsIdEnrolMeterPointsPost) | **POST** /junifer/accounts/{id}/enrolMeterPoints | Enrol additional Meter Points to existing account/site
[**juniferAccountsIdGet**](AccountsApi.md#juniferAccountsIdGet) | **GET** /junifer/accounts/{id} | Get account
[**juniferAccountsIdHotBillPost**](AccountsApi.md#juniferAccountsIdHotBillPost) | **POST** /junifer/accounts/{id}/hotBill | Hot bill
[**juniferAccountsIdNotePost**](AccountsApi.md#juniferAccountsIdNotePost) | **POST** /junifer/accounts/{id}/note | Create account note
[**juniferAccountsIdPaymentMethodsGet**](AccountsApi.md#juniferAccountsIdPaymentMethodsGet) | **GET** /junifer/accounts/{id}/paymentMethods | Get account&#39;s payment methods
[**juniferAccountsIdPaymentPlansGet**](AccountsApi.md#juniferAccountsIdPaymentPlansGet) | **GET** /junifer/accounts/{id}/paymentPlans | Get account&#39;s payment plans
[**juniferAccountsIdPaymentPost**](AccountsApi.md#juniferAccountsIdPaymentPost) | **POST** /junifer/accounts/{id}/payment | Create Payment Transaction
[**juniferAccountsIdPaymentSchedulePeriodsGet**](AccountsApi.md#juniferAccountsIdPaymentSchedulePeriodsGet) | **GET** /junifer/accounts/{id}/paymentSchedulePeriods | Get account&#39;s payment schedule periods
[**juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet**](AccountsApi.md#juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet) | **GET** /junifer/accounts/{id}/paymentSchedules/suggestedPaymentAmount | Get account&#39;s suggested payment amount
[**juniferAccountsIdPaymentsGet**](AccountsApi.md#juniferAccountsIdPaymentsGet) | **GET** /junifer/accounts/{id}/payments | Get account&#39;s payments
[**juniferAccountsIdPrimaryContactPut**](AccountsApi.md#juniferAccountsIdPrimaryContactPut) | **PUT** /junifer/accounts/{id}/primaryContact | Update Accounts&#39;s primary contact details.
[**juniferAccountsIdProductDetailsGet**](AccountsApi.md#juniferAccountsIdProductDetailsGet) | **GET** /junifer/accounts/{id}/productDetails | Get account&#39;s product details
[**juniferAccountsIdPropertysGet**](AccountsApi.md#juniferAccountsIdPropertysGet) | **GET** /junifer/accounts/{id}/propertys | Get account&#39;s properties
[**juniferAccountsIdRenewalPost**](AccountsApi.md#juniferAccountsIdRenewalPost) | **POST** /junifer/accounts/{id}/renewal | Renew account
[**juniferAccountsIdRepaymentsPost**](AccountsApi.md#juniferAccountsIdRepaymentsPost) | **POST** /junifer/accounts/{id}/repayments | Create account repayment
[**juniferAccountsIdTariffInformationGet**](AccountsApi.md#juniferAccountsIdTariffInformationGet) | **GET** /junifer/accounts/{id}/tariffInformation | Get account&#39;s tariff Information
[**juniferAccountsIdTicketsGet**](AccountsApi.md#juniferAccountsIdTicketsGet) | **GET** /junifer/accounts/{id}/tickets | Get account&#39;s tickets
[**juniferAccountsIdTicketsPost**](AccountsApi.md#juniferAccountsIdTicketsPost) | **POST** /junifer/accounts/{id}/tickets | Create account ticket
[**juniferAccountsIdTransactionsGet**](AccountsApi.md#juniferAccountsIdTransactionsGet) | **GET** /junifer/accounts/{id}/transactions | Get account&#39;s transactions



## juniferAccountsAccountNumberNumGet

> juniferAccountsAccountNumberNumGet(num)

Get account by account number

Get an account by its account number (i.e. the customer-facing identifier).The success response is identical to the Get Account API.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let num = "num_example"; // String | Account Number
apiInstance.juniferAccountsAccountNumberNumGet(num, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **num** | **String**| Account Number | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsGet

> LookUpAccountGetAccountsResponse200 juniferAccountsGet(opts)

Account lookup

Searches for an account by one or more of the specified search terms. If no account can be found an empty &#x60;results&#x60; array is returned. The query parameters are :account_number, :surname, :billingPostcode, :email. The lookup can be performed using either of them, but at least one must be provided. If more than one are specified, the account contact details are cross-checked by looking if the provided search values number belong to the same account.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let opts = {
  'number': 3.4, // Number | number
  'surname': "surname_example", // String | surname
  'telephone': "telephone_example", // String | Telephone number in any of the telephone fields in account primary contact. Right search allowed (i.e without international/area codes)d. If this parameter is present in the request it must be non-empty.
  'billingPostcode': "billingPostcode_example", // String | Billing postcode (no spaces)
  'email': "email_example" // String | email address in account primary contact record
};
apiInstance.juniferAccountsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **Number**| number | [optional] 
 **surname** | **String**| surname | [optional] 
 **telephone** | **String**| Telephone number in any of the telephone fields in account primary contact. Right search allowed (i.e without international/area codes)d. If this parameter is present in the request it must be non-empty. | [optional] 
 **billingPostcode** | **String**| Billing postcode (no spaces) | [optional] 
 **email** | **String**| email address in account primary contact record | [optional] 

### Return type

[**LookUpAccountGetAccountsResponse200**](LookUpAccountGetAccountsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdAccountCreditsGet

> GetAccountCreditsGetAccountsIdAccountcreditsResponse200 juniferAccountsIdAccountCreditsGet(id)

Get account credits

Get account credits for an account

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdAccountCreditsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAccountCreditsGetAccountsIdAccountcreditsResponse200**](GetAccountCreditsGetAccountsIdAccountcreditsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdAccountCreditsPost

> juniferAccountsIdAccountCreditsPost(id, requestBody)

Create account credit

Create account credit for account. Authentication must be active

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.CreateAccountCreditPostAccountsIdAccountcreditsRequestBody(); // CreateAccountCreditPostAccountsIdAccountcreditsRequestBody | Request body
apiInstance.juniferAccountsIdAccountCreditsPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**CreateAccountCreditPostAccountsIdAccountcreditsRequestBody**](CreateAccountCreditPostAccountsIdAccountcreditsRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdAccountDebitsGet

> GetAccountDebitsGetAccountsIdAccountdebitsResponse200 juniferAccountsIdAccountDebitsGet(id)

Get account debits

Get account debits for an account

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdAccountDebitsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAccountDebitsGetAccountsIdAccountdebitsResponse200**](GetAccountDebitsGetAccountsIdAccountdebitsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdAccountDebitsPost

> juniferAccountsIdAccountDebitsPost(id, requestBody)

Create account debit

Create account debit for account. Authentication must be active

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.CreateAccountDebitPostAccountsIdAccountdebitsRequestBody(); // CreateAccountDebitPostAccountsIdAccountdebitsRequestBody | Request body
apiInstance.juniferAccountsIdAccountDebitsPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**CreateAccountDebitPostAccountsIdAccountdebitsRequestBody**](CreateAccountDebitPostAccountsIdAccountdebitsRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdAccountReviewPeriodsGet

> GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200 juniferAccountsIdAccountReviewPeriodsGet(id)

Get account&#39;s review periods

Gets account&#39;s review periods which belong to the account specified by the ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdAccountReviewPeriodsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200**](GetAccountReviewPeriodsGetAccountsIdAccountreviewperiodsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdAccountReviewPeriodsPost

> CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200 juniferAccountsIdAccountReviewPeriodsPost(id, requestBody)

Create account review period

Creates a new account review period for the account specified by the Id

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody(); // CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody | Request body
apiInstance.juniferAccountsIdAccountReviewPeriodsPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody**](CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsRequestBody.md)| Request body | 

### Return type

[**CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200**](CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdAgreementsGet

> GetAccountAgreementsGetAccountsIdAgreementsResponse200 juniferAccountsIdAgreementsGet(id)

Get account&#39;s agreements

Get agreements associated to the account specified by the ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdAgreementsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAccountAgreementsGetAccountsIdAgreementsResponse200**](GetAccountAgreementsGetAccountsIdAgreementsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdAllPayReferenceGet

> GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200 juniferAccountsIdAllPayReferenceGet(id)

Get account&#39;s AllPay Reference Number

Get the AllPay reference number

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdAllPayReferenceGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200**](GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdBillRequestsGet

> GetAccountBillRequestsGetAccountsIdBillrequestsResponse200 juniferAccountsIdBillRequestsGet(id, opts)

Get account&#39;s bill requests

Gets bill requests which belong to the account specified by the ID. If no bill requests can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let opts = {
  'statusCode': "statusCode_example", // String | Status Code of retrieved bill requests. Can contain the following: `Scheduled, Completed, Failed, Superseded, Retrying`. If not provided then this filter won't be applied
  'typeCode': "typeCode_example" // String | Type Code of retrieved bill requests. Can contain the following: `Normal, Hot, NewVersion`. If not provided then this filter won't be applied
};
apiInstance.juniferAccountsIdBillRequestsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **statusCode** | **String**| Status Code of retrieved bill requests. Can contain the following: &#x60;Scheduled, Completed, Failed, Superseded, Retrying&#x60;. If not provided then this filter won&#39;t be applied | [optional] 
 **typeCode** | **String**| Type Code of retrieved bill requests. Can contain the following: &#x60;Normal, Hot, NewVersion&#x60;. If not provided then this filter won&#39;t be applied | [optional] 

### Return type

[**GetAccountBillRequestsGetAccountsIdBillrequestsResponse200**](GetAccountBillRequestsGetAccountsIdBillrequestsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdBillsGet

> GetAccountBillsGetAccountsIdBillsResponse200 juniferAccountsIdBillsGet(id, opts)

Get account&#39;s bills

Gets bills which belong to the account specified by the ID. If no bills can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let opts = {
  'status': "status_example" // String | Status of retrieved bills. Can contain the following: `All, Accepted, Draft`. Defaults to `Accepted` if not provided.
};
apiInstance.juniferAccountsIdBillsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **status** | **String**| Status of retrieved bills. Can contain the following: &#x60;All, Accepted, Draft&#x60;. Defaults to &#x60;Accepted&#x60; if not provided. | [optional] 

### Return type

[**GetAccountBillsGetAccountsIdBillsResponse200**](GetAccountBillsGetAccountsIdBillsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdCancelAccountReviewPeriodDelete

> juniferAccountsIdCancelAccountReviewPeriodDelete(id, accountReviewPeriodId)

Cancel account review period

Cancel an existing account review period for an account

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID.
let accountReviewPeriodId = 3.4; // Number | Account Review Period ID
apiInstance.juniferAccountsIdCancelAccountReviewPeriodDelete(id, accountReviewPeriodId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID. | 
 **accountReviewPeriodId** | **Number**| Account Review Period ID | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdChangeAccountCustomerPut

> juniferAccountsIdChangeAccountCustomerPut(id, requestBody)

Change the customer linked to an account

Change the customer linked to the account to the customer specified by the id

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody(); // ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody | Request body
apiInstance.juniferAccountsIdChangeAccountCustomerPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody**](ChangeAccountCustomerPutAccountsIdChangeaccountcustomerRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdCommercialProductDetailsGet

> GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200 juniferAccountsIdCommercialProductDetailsGet(id)

Get account&#39;s product details for commercial customer

Get the details for products associated to the commercial account specified by the ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdCommercialProductDetailsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200**](GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdCommunicationsGet

> GetAccountCommunicationsGetAccountsIdCommunicationsResponse200 juniferAccountsIdCommunicationsGet(id, opts)

Get account&#39;s communications

Get communications associated to the account specified by the ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let opts = {
  'fromDate': "fromDate_example", // String | Date from
  'toDate': "toDate_example", // String | Date to
  'testFlag': true, // Boolean | true will include test communications
  'status': ["null"] // [String] | Array of statuses strings used to filter the search of the communications. Can contain the following: `Pending, Successful, SuccessfulWithWarnings, NoDeliveries, Failed` and if left blank will default to `Successful, SuccessfulWithWarnings`
};
apiInstance.juniferAccountsIdCommunicationsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **fromDate** | **String**| Date from | [optional] 
 **toDate** | **String**| Date to | [optional] 
 **testFlag** | **Boolean**| true will include test communications | [optional] 
 **status** | [**[String]**](String.md)| Array of statuses strings used to filter the search of the communications. Can contain the following: &#x60;Pending, Successful, SuccessfulWithWarnings, NoDeliveries, Failed&#x60; and if left blank will default to &#x60;Successful, SuccessfulWithWarnings&#x60; | [optional] 

### Return type

[**GetAccountCommunicationsGetAccountsIdCommunicationsResponse200**](GetAccountCommunicationsGetAccountsIdCommunicationsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdContactsContactIdPut

> juniferAccountsIdContactsContactIdPut(id, contactId, requestBody)

Update account&#39;s contact details

Updates a account&#39;s given contact details. Correct usage is to get the &#x60;contact&#x60; object from the accounts &#x60;contacts&#x60; call then modify the &#x60;contact&#x60; object and return it using this &#x60;Update Account&#39;s Contact&#x60; call. Our database is then updated to match the incoming &#x60;contact&#x60; object. All null fields in that incoming &#x60;contact&#x60; object are set to null in our database.  Note that setting billDelivery to \&quot;None\&quot; could mean that no one will be notified when a bill is generated. Other billing activity will continue as normal. Please use with caution for Residential contacts.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let contactId = 3.4; // Number | Contact ID
let requestBody = new JuniferAuNativeApi.UpdateAccountContactPutAccountsIdContactsContactidRequestBody(); // UpdateAccountContactPutAccountsIdContactsContactidRequestBody | Request body
apiInstance.juniferAccountsIdContactsContactIdPut(id, contactId, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **contactId** | **Number**| Contact ID | 
 **requestBody** | [**UpdateAccountContactPutAccountsIdContactsContactidRequestBody**](UpdateAccountContactPutAccountsIdContactsContactidRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdContactsGet

> ContactsGetAccountsIdContactsResponse200 juniferAccountsIdContactsGet(id)

Contacts

Get all contacts for given account. If no contacts can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdContactsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**ContactsGetAccountsIdContactsResponse200**](ContactsGetAccountsIdContactsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdCreditsGet

> GetAccountCreditNotesGetAccountsIdCreditsResponse200 juniferAccountsIdCreditsGet(id, opts)

Get account&#39;s credits

Gets credits which belong to the account specified by the ID. If no credits can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let opts = {
  'status': "status_example" // String | Status of retrieved credits. Can contain the following: `All,Draft, Authorised, Rejected`. Defaults to `Accepted` if not provided.
};
apiInstance.juniferAccountsIdCreditsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **status** | **String**| Status of retrieved credits. Can contain the following: &#x60;All,Draft, Authorised, Rejected&#x60;. Defaults to &#x60;Accepted&#x60; if not provided. | [optional] 

### Return type

[**GetAccountCreditNotesGetAccountsIdCreditsResponse200**](GetAccountCreditNotesGetAccountsIdCreditsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdEnrolMeterPointsPost

> EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200 juniferAccountsIdEnrolMeterPointsPost(id, requestBody)

Enrol additional Meter Points to existing account/site

Enrols new Meter Point(s) to an existing account/site for electricity and/or gas product. Returns a list of the newly-enrolled Meter Points.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody(); // EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody | Request body
apiInstance.juniferAccountsIdEnrolMeterPointsPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBody.md)| Request body | 

### Return type

[**EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdGet

> GetAccountGetAccountsIdResponse200 juniferAccountsIdGet(id)

Get account

Gets account by its ID (not account number!)

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAccountGetAccountsIdResponse200**](GetAccountGetAccountsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdHotBillPost

> juniferAccountsIdHotBillPost(id)

Hot bill

Create a hot bill for a given Account. This will consolidate all open bill periods that precede today&#39;s date. Authentication must be active.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdHotBillPost(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdNotePost

> CreateAccountNotePostAccountsIdNoteResponse200 juniferAccountsIdNotePost(id, requestBody)

Create account note

Creates a new note for the account specified by the Id

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.CreateAccountNotePostAccountsIdNoteRequestBody(); // CreateAccountNotePostAccountsIdNoteRequestBody | Request body
apiInstance.juniferAccountsIdNotePost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**CreateAccountNotePostAccountsIdNoteRequestBody**](CreateAccountNotePostAccountsIdNoteRequestBody.md)| Request body | 

### Return type

[**CreateAccountNotePostAccountsIdNoteResponse200**](CreateAccountNotePostAccountsIdNoteResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdPaymentMethodsGet

> GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200 juniferAccountsIdPaymentMethodsGet(id)

Get account&#39;s payment methods

Get payment methods registered for the account specified by the ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdPaymentMethodsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200**](GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdPaymentPlansGet

> GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200 juniferAccountsIdPaymentPlansGet(id)

Get account&#39;s payment plans

Gets payment plans which belong to the account specified by the ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdPaymentPlansGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200**](GetAccountPaymentPlansGetAccountsIdPaymentplansResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdPaymentPost

> PaymentPostAccountsIdPaymentResponse200 juniferAccountsIdPaymentPost(id, requestBody)

Create Payment Transaction

Creates a transaction for payments that occurs outside of Junifer. No payment collection will be triggered. Requires authentication.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.PaymentPostAccountsIdPaymentRequestBody(); // PaymentPostAccountsIdPaymentRequestBody | Request body
apiInstance.juniferAccountsIdPaymentPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**PaymentPostAccountsIdPaymentRequestBody**](PaymentPostAccountsIdPaymentRequestBody.md)| Request body | 

### Return type

[**PaymentPostAccountsIdPaymentResponse200**](PaymentPostAccountsIdPaymentResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdPaymentSchedulePeriodsGet

> GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200 juniferAccountsIdPaymentSchedulePeriodsGet(id, opts)

Get account&#39;s payment schedule periods

Get account&#39;s payment schedule periods

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let opts = {
  'nextPaymentOnlyFl': true // Boolean | If `true` returns only the payment period that will be active when the next payment is collected, this may be the current payment period. If not provided then defaults to `false`.
};
apiInstance.juniferAccountsIdPaymentSchedulePeriodsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **nextPaymentOnlyFl** | **Boolean**| If &#x60;true&#x60; returns only the payment period that will be active when the next payment is collected, this may be the current payment period. If not provided then defaults to &#x60;false&#x60;. | [optional] 

### Return type

[**GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200**](GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet

> GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200 juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet(id, opts)

Get account&#39;s suggested payment amount

Get account&#39;s suggested payment amount given the payment schedule review calculation strategy

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let opts = {
  'frequency': "frequency_example", // String | The name of the frequency indicating when a payment should be collected, e.g. `Monthly, Weekly`. Defaults to `Monthly`
  'frequencyMultiple': 3.4, // Number | Frequency multiplier. Defaults to 1
  'ignoreWarnings': true // Boolean | If set to true, consumption estimation warnings will be ignored. If false, the API will return an error if there are consumption estimation warnings. Defaults to false
};
apiInstance.juniferAccountsIdPaymentSchedulesSuggestedPaymentAmountGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **frequency** | **String**| The name of the frequency indicating when a payment should be collected, e.g. &#x60;Monthly, Weekly&#x60;. Defaults to &#x60;Monthly&#x60; | [optional] 
 **frequencyMultiple** | **Number**| Frequency multiplier. Defaults to 1 | [optional] 
 **ignoreWarnings** | **Boolean**| If set to true, consumption estimation warnings will be ignored. If false, the API will return an error if there are consumption estimation warnings. Defaults to false | [optional] 

### Return type

[**GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200**](GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdPaymentsGet

> GetAccountPaymentsGetAccountsIdPaymentsResponse200 juniferAccountsIdPaymentsGet(id, opts)

Get account&#39;s payments

Get payments associated to the account specified by the ID with the corresponding transaction status.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let opts = {
  'status': ["null"], // [String] | Array of transaction status strings used to filter the search of our payments.  Can contain the following: `Pending, Accepted, Rejected, Authorising` and if left blank will default to using `Accepted`.
  'cancelFl': true // Boolean | Returns cancelled transactions if `true`. If not provided then defaults to `false`
};
apiInstance.juniferAccountsIdPaymentsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **status** | [**[String]**](String.md)| Array of transaction status strings used to filter the search of our payments.  Can contain the following: &#x60;Pending, Accepted, Rejected, Authorising&#x60; and if left blank will default to using &#x60;Accepted&#x60;. | [optional] 
 **cancelFl** | **Boolean**| Returns cancelled transactions if &#x60;true&#x60;. If not provided then defaults to &#x60;false&#x60; | [optional] 

### Return type

[**GetAccountPaymentsGetAccountsIdPaymentsResponse200**](GetAccountPaymentsGetAccountsIdPaymentsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdPrimaryContactPut

> juniferAccountsIdPrimaryContactPut(id, requestBody)

Update Accounts&#39;s primary contact details.

Updates Accounts&#39;s primary contact details. Correct usage is to get the &#x60;primaryContact&#x60; object from the &#x60;get account&#x60; call then modify the &#x60;primaryContact&#x60; information in it using this Update Account&#39;s Contact call. Our database is then updated to match the incoming &#x60;primaryContact&#x60; object. All null fields in that incoming &#x60;primaryContact&#x60; object are set to null in our database. Missing params are treated as nulls.  Note that setting billDelivery to \&quot;None\&quot; could mean that no one will be notified when a bill is generated. Other billing activity will continue as normal. Please use with caution for Residential contacts.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody(); // UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody | Request body
apiInstance.juniferAccountsIdPrimaryContactPut(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody**](UpdateAccountPrimaryContactPutAccountsIdPrimarycontactRequestBody.md)| Request body | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdProductDetailsGet

> GetAccountProductDetailsGetAccountsIdProductdetailsResponse200 juniferAccountsIdProductDetailsGet(id)

Get account&#39;s product details

Get the details for products associated to the account specified by the ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdProductDetailsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAccountProductDetailsGetAccountsIdProductdetailsResponse200**](GetAccountProductDetailsGetAccountsIdProductdetailsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdPropertysGet

> GetAccountPropertysGetAccountsIdPropertysResponse200 juniferAccountsIdPropertysGet(id)

Get account&#39;s properties

Get account&#39;s properties. If no properties can be found an empty &#x60;results&#x60; array is returned.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
apiInstance.juniferAccountsIdPropertysGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 

### Return type

[**GetAccountPropertysGetAccountsIdPropertysResponse200**](GetAccountPropertysGetAccountsIdPropertysResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdRenewalPost

> RenewAccountPostAccountsIdRenewalResponse200 juniferAccountsIdRenewalPost(id, requestBody)

Renew account

Renews an account with a new agreement based on the additional details supplied. Either electricity product details or gas product details need to be supplied for this to work.  Will raise a ticket, if one is specified in the property Agreement Creation Properties &gt; API Agreement Setup Ticket.  Will trigger change of meter mode if new or existing agreements are SMART PAYG and meter contracted to the account is in DCC mode

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.RenewAccountPostAccountsIdRenewalRequestBody(); // RenewAccountPostAccountsIdRenewalRequestBody | Request body
apiInstance.juniferAccountsIdRenewalPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**RenewAccountPostAccountsIdRenewalRequestBody**](RenewAccountPostAccountsIdRenewalRequestBody.md)| Request body | 

### Return type

[**RenewAccountPostAccountsIdRenewalResponse200**](RenewAccountPostAccountsIdRenewalResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdRepaymentsPost

> CreateAccountRepaymentPostAccountsIdRepaymentsResponse200 juniferAccountsIdRepaymentsPost(id, requestBody)

Create account repayment

Create a refund for an existing account payment. The account must have a valid default payment method set. This call is only usable when authentication is active

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody(); // CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody | Request body
apiInstance.juniferAccountsIdRepaymentsPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody**](CreateAccountRepaymentPostAccountsIdRepaymentsRequestBody.md)| Request body | 

### Return type

[**CreateAccountRepaymentPostAccountsIdRepaymentsResponse200**](CreateAccountRepaymentPostAccountsIdRepaymentsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdTariffInformationGet

> TariffInformationGetAccountsIdTariffinformationResponse200 juniferAccountsIdTariffInformationGet(id, ignoreWarnings, opts)

Get account&#39;s tariff Information

Get tariff information associated with this account at the comparison date, including tariffs that are active on the day and tariffs that end on the day.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let ignoreWarnings = true; // Boolean | If set to true, consumption estimation warnings will be ignored. If false, the API will return an error if there are consumption estimation warnings. Defaults to false
let opts = {
  'comparisonDatetime': "comparisonDatetime_example" // String | Date to get information for. Default to today if not specified.
};
apiInstance.juniferAccountsIdTariffInformationGet(id, ignoreWarnings, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **ignoreWarnings** | **Boolean**| If set to true, consumption estimation warnings will be ignored. If false, the API will return an error if there are consumption estimation warnings. Defaults to false | 
 **comparisonDatetime** | **String**| Date to get information for. Default to today if not specified. | [optional] 

### Return type

[**TariffInformationGetAccountsIdTariffinformationResponse200**](TariffInformationGetAccountsIdTariffinformationResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdTicketsGet

> GetAccountTicketsGetAccountsIdTicketsResponse200 juniferAccountsIdTicketsGet(id, opts)

Get account&#39;s tickets

Get tickets associated with this account

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let opts = {
  'status': ["null"], // [String] | Array of status which is used to filter the search of our ticket status. Can contain the following: `Open, Error, Closed, Cancelled` and if left blank will default to display all the tickets.
  'includeIndirect': true // Boolean | if false, only tickets directly linked to the Account will be returned. If true, will also return tickets that are indirectly linked to the account via Bill, AccountTransaction, DunningInst, Asset, Customer, ProductPropertyAsset and Property.
};
apiInstance.juniferAccountsIdTicketsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **status** | [**[String]**](String.md)| Array of status which is used to filter the search of our ticket status. Can contain the following: &#x60;Open, Error, Closed, Cancelled&#x60; and if left blank will default to display all the tickets. | [optional] 
 **includeIndirect** | **Boolean**| if false, only tickets directly linked to the Account will be returned. If true, will also return tickets that are indirectly linked to the account via Bill, AccountTransaction, DunningInst, Asset, Customer, ProductPropertyAsset and Property. | [optional] 

### Return type

[**GetAccountTicketsGetAccountsIdTicketsResponse200**](GetAccountTicketsGetAccountsIdTicketsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountsIdTicketsPost

> CreateAccountTicketPostAccountsIdTicketsResponse200 juniferAccountsIdTicketsPost(id, requestBody)

Create account ticket

Creates a new ticket for the account specified by the ID  Contact Gentrack before using this request. It can have serious unexpected consequences if not used correctly.

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | Account ID
let requestBody = new JuniferAuNativeApi.CreateAccountTicketPostAccountsIdTicketsRequestBody(); // CreateAccountTicketPostAccountsIdTicketsRequestBody | Request body
apiInstance.juniferAccountsIdTicketsPost(id, requestBody, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account ID | 
 **requestBody** | [**CreateAccountTicketPostAccountsIdTicketsRequestBody**](CreateAccountTicketPostAccountsIdTicketsRequestBody.md)| Request body | 

### Return type

[**CreateAccountTicketPostAccountsIdTicketsResponse200**](CreateAccountTicketPostAccountsIdTicketsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## juniferAccountsIdTransactionsGet

> GetAccountTransactionsGetAccountsIdTransactionsResponse200 juniferAccountsIdTransactionsGet(id, opts)

Get account&#39;s transactions

Gets accepted transactions for the account specified by the ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountsApi();
let id = 3.4; // Number | AccountID
let opts = {
  'cancelFl': true, // Boolean | Returns cancelled transactions if `true`. If not provided then defaults to `false`
  'status': ["null"], // [String] | Array of transaction status strings used to filter the search of account transactions.  Can contain the following: `All, Pending, Accepted, Rejected, Authorising` and if left blank will default to using `Accepted`.
  'createdDt': "createdDt_example" // String | Date that specifies which transactions select from that date onwards
};
apiInstance.juniferAccountsIdTransactionsGet(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| AccountID | 
 **cancelFl** | **Boolean**| Returns cancelled transactions if &#x60;true&#x60;. If not provided then defaults to &#x60;false&#x60; | [optional] 
 **status** | [**[String]**](String.md)| Array of transaction status strings used to filter the search of account transactions.  Can contain the following: &#x60;All, Pending, Accepted, Rejected, Authorising&#x60; and if left blank will default to using &#x60;Accepted&#x60;. | [optional] 
 **createdDt** | **String**| Date that specifies which transactions select from that date onwards | [optional] 

### Return type

[**GetAccountTransactionsGetAccountsIdTransactionsResponse200**](GetAccountTransactionsGetAccountsIdTransactionsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

