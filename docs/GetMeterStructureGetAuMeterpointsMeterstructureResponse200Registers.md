# JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Meter register ID | 
**identifier** | **String** | Meter register identifier | 
**digits** | **Number** | The amount of digits meter register has | 
**decimalPlaces** | **Number** | The amount of decimal places meter register has | 
**registerType** | **String** | Meter register type | [optional] 


