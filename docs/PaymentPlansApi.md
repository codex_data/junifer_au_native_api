# JuniferAuNativeApi.PaymentPlansApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferPaymentPlansIdPaymentPlanPaymentsGet**](PaymentPlansApi.md#juniferPaymentPlansIdPaymentPlanPaymentsGet) | **GET** /junifer/paymentPlans/{id}/paymentPlanPayments | Get paymentPlan&#39;s payments



## juniferPaymentPlansIdPaymentPlanPaymentsGet

> GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200 juniferPaymentPlansIdPaymentPlanPaymentsGet(id)

Get paymentPlan&#39;s payments

Get paymentPlan&#39;s payments

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.PaymentPlansApi();
let id = 3.4; // Number | PaymentPlan ID
apiInstance.juniferPaymentPlansIdPaymentPlanPaymentsGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| PaymentPlan ID | 

### Return type

[**GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200**](GetPaymentPlanPaymentsGetPaymentplansIdPaymentplanpaymentsResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

