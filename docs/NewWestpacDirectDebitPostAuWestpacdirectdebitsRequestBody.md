# JuniferAuNativeApi.NewWestpacDirectDebitPostAuWestpacdirectdebitsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mandateReference** | **String** | Westpac Direct Debit mandate reference | [optional] 
**bankAccountNumber** | **String** | Number of the bank account used to create the direct debit | [optional] 
**bsbNumber** | **String** | BSB number associated with the bank account used to create the direct debit | [optional] 
**customerNumber** | **String** | Customer&#39;s unique ID | [optional] 
**accountName** | **String** | Customer&#39;s account name registered with Westpac | [optional] 
**paymentType** | **String** | Payment type. Can be either &#x60;BankAccount&#x60; or &#x60;CreditCard&#x60; | 
**cardType** | **String** | Credit card type. Must be provided if the payment type is CreditCard. Acceptable values are &#x60;Visa,Mastercard,Amex,Diners,JBC,UnionPay&#x60; | [optional] 


