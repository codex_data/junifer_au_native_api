# JuniferAuNativeApi.GetProspectOffersGetProspectsIdOffersResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to the offer itself. | 


