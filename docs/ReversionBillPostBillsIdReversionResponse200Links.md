# JuniferAuNativeApi.ReversionBillPostBillsIdReversionResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this bill | 
**newVersionBillRequest** | **String** | Link to the new version of the bill | 


