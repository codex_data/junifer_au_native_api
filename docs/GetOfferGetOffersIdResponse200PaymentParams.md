# JuniferAuNativeApi.GetOfferGetOffersIdResponse200PaymentParams

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentTerm** | **String** | The offers payment term. Examples include &#x60;Standard - 30 days&#x60;. | [optional] 
**paymentMethodType** | **String** | The offers payment method type. Examples include &#x60;Direct Debit&#x60;. | [optional] 


