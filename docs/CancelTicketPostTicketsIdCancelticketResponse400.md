# JuniferAuNativeApi.CancelTicketPostTicketsIdCancelticketResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;TicketCancelled&#x60; - \&quot;This ticket has already been cancelled\&quot; * &#x60;NotFound&#x60; - \&quot;Could not find ticket with Id &#39;1&#39;\&quot; * &#x60;RequiredParameterMissing&#x60; - The required &#39;ticketCancelReason&#39; parameter is missing | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `TicketCancelled` (value: `"TicketCancelled"`)

* `NotFound` (value: `"NotFound"`)

* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)




