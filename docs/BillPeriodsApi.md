# JuniferAuNativeApi.BillPeriodsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferBillPeriodsIdGet**](BillPeriodsApi.md#juniferBillPeriodsIdGet) | **GET** /junifer/billPeriods/{id} | Get Bill Period



## juniferBillPeriodsIdGet

> GetBillPeriodGetBillperiodsIdResponse200 juniferBillPeriodsIdGet(id)

Get Bill Period

Gets bill period by ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillPeriodsApi();
let id = 3.4; // Number | Bill Period ID
apiInstance.juniferBillPeriodsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Bill Period ID | 

### Return type

[**GetBillPeriodGetBillperiodsIdResponse200**](GetBillPeriodGetBillperiodsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

