# JuniferAuNativeApi.UpdateTicketPutTicketsIdRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticketStepCode** | **String** | Ticket step code to move ticket to | 
**summary** | **String** | Brief summary for the ticket (overwrites existing Summary) | 
**description** | **String** | Ticket description | 
**parentTicketId** | **Number** | Optionally associate a ticket with a parent | [optional] 


