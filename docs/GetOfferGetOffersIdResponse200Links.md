# JuniferAuNativeApi.GetOfferGetOffersIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to the offer itself. | 
**prospect** | **String** | The associated prospect for the offer. | 
**broker** | **String** | The associated broker for the offer. | 


