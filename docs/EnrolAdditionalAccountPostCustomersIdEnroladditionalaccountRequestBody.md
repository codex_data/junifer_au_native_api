# JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**senderReference** | **String** | Identifier for the enrolling entity. This might be the name of the switching site or a web portal. References must be pre-configured in Junifer prior any enrolment requests otherwise enrolments will be rejected | 
**reference** | **String** | Unique reference string identifying this particular enrolment | 
**billingEntityCode** | **String** | Code of the billing entity to which the account must be attached. If this is empty it will default to the customer billing entity | [optional] 
**accountName** | **String** | Name for the new account. Must be different from existing account names for the customer | 
**submittedSource** | **String** | The source of the enrolment. This overrides the default source (WebService) if set | [optional] 
**changeOfTenancyFl** | **Boolean** | A boolean flag indicating whether new account is for a change of tenancy. | [optional] 
**supplyAddress** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodySupplyAddress.md) |  | 
**directDebitInfo** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo.md) |  | [optional] 
**electricityProduct** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityProduct.md) |  | 
**gasProduct** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyGasProduct.md) |  | 
**gasEstimate** | [**EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate**](EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate.md) |  | [optional] 
**electricityEstimate** | [**EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate**](EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyElectricityEstimate.md) |  | [optional] 


