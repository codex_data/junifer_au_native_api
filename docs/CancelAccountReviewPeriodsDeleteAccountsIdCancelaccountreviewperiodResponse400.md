# JuniferAuNativeApi.CancelAccountReviewPeriodsDeleteAccountsIdCancelaccountreviewperiodResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;MissingParameters&#x60; - The required &#39;accountReviewPeriodId&#39; query parameter is missing * &#x60;AccountReviewPeriod&#x60; - This account review period can&#39;t be found | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `MissingParameters` (value: `"MissingParameters"`)

* `AccountReviewPeriod` (value: `"AccountReviewPeriod"`)




