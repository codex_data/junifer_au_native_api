# JuniferAuNativeApi.AccountCreditsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAccountCreditsIdDelete**](AccountCreditsApi.md#juniferAccountCreditsIdDelete) | **DELETE** /junifer/accountCredits/{id} | Cancel account credit
[**juniferAccountCreditsIdGet**](AccountCreditsApi.md#juniferAccountCreditsIdGet) | **GET** /junifer/accountCredits/{id} | Get Account Credit



## juniferAccountCreditsIdDelete

> juniferAccountCreditsIdDelete(id)

Cancel account credit

Cancel an existing account credit for an account. This action is not reversible. Authentication must be active

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountCreditsApi();
let id = 3.4; // Number | Account credit ID number
apiInstance.juniferAccountCreditsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account credit ID number | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountCreditsIdGet

> GetAccountCreditGetAccountcreditsIdResponse200 juniferAccountCreditsIdGet(id)

Get Account Credit

Gets an account credit by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountCreditsApi();
let id = 3.4; // Number | Account Credit ID
apiInstance.juniferAccountCreditsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account Credit ID | 

### Return type

[**GetAccountCreditGetAccountcreditsIdResponse200**](GetAccountCreditGetAccountcreditsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

