# JuniferAuNativeApi.AusSiteAccessPostAuCustomersSiteaccessRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nmi** | **String** | Identifier of the NMI including checksum | 
**accessDetails** | **String** | Text describing access to the site | [optional] 
**hazardDescription** | **String** | Text identifying hazards associated with reading the meter | [optional] 


