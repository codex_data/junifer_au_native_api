# JuniferAuNativeApi.AusRenewAccountPostAuAccountsIdRenewalResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;MissingDetails&#x60; - If an electricityProductCode is supplied then an electricitySupplyProductSubType must also be supplied * &#x60;InvalidParameter&#x60; - The product subtype provided does not match any product subtype linked to the existing agreement * &#x60;BadRequest&#x60; - There was an error processing your request | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `MissingDetails` (value: `"MissingDetails"`)

* `InvalidParameter` (value: `"InvalidParameter"`)

* `BadRequest` (value: `"BadRequest"`)




