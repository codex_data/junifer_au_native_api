# JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200SupplyAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address1** | **String** | Address 1 | 
**address2** | **String** | Address 2 | 
**address3** | **String** | Address 3 | 
**address4** | **String** | Address 4 | 
**address5** | **String** | Address 5 | 
**postcode** | **String** | Post code | 


