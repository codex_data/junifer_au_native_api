# JuniferAuNativeApi.GetCustomerBillingEntitiesGetCustomersIdBillingentitiesResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Customer ID | 
**parentBillingEntity** | **String** | Parent billing entity | [optional] 
**name** | **String** | Display name | 
**code** | **String** | Identification code. Used in other API calls for product lookup, enrolment, etc. | 
**currency** | **String** | Default currency | [optional] 
**dunningSelectorComponent** | **String** | Dunning package selector | [optional] 
**numberGeneratorComponent** | **String** | Customer/account number generator | [optional] 
**txnAllocatorComponent** | **String** | Transaction allocator | [optional] 
**mailServer** | **String** | Mail Server. An absent field means it&#39;s inherited from the parent billing entity | [optional] 
**creditNotesForBbfBillingFl** | **String** | Generate Credit Notes For Brought Forward Billing Accounts flag | 
**customerFl** | **String** | Whether the &#39;Customer&#39; permission is enabled | 
**productFl** | **String** | Whether the &#39;Product&#39; permission is enabled | 
**tariffFl** | **String** | Whether the &#39;Tariff&#39; permission is enabled | 
**prospectFl** | **String** | Whether the &#39;Prospect&#39; permission is enabled | 
**reference** | **String** | Internal reference | 
**links** | [**GetBillingEntityGetBillingentitiesIdResponse200Links**](GetBillingEntityGetBillingentitiesIdResponse200Links.md) |  | 


