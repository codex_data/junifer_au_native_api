# JuniferAuNativeApi.PaymentPostAccountsIdPaymentResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **Object** | The details of the generated payment request | [optional] 


