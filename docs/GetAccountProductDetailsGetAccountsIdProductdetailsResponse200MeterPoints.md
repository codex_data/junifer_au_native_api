# JuniferAuNativeApi.GetAccountProductDetailsGetAccountsIdProductdetailsResponse200MeterPoints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The id for the meter point | 
**identifier** | **String** | Meter point identifier | 
**type** | **String** | The type of the meter point (e.g MPAN, MPRN...) | 


