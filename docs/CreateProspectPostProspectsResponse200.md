# JuniferAuNativeApi.CreateProspectPostProspectsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prospectId** | **Number** | Newly created Prospect ID | 


