# JuniferAuNativeApi.GetQuoteGetQuotesIdResponse200BrokerMargins

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brokerName** | **String** | The name of the broker which is associated with the broker margin | 
**marginValue** | **Number** | The value of the broker margin | 
**marginType** | **String** | The metric type of the broker margin. Examples include &#x60;PencePerkWh, PoundPerYearPerMeterPoint&#x60; | 
**links** | **String** | The list of links associated with the broker margins | 


