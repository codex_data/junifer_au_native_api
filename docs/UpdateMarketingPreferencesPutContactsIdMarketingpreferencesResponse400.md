# JuniferAuNativeApi.UpdateMarketingPreferencesPutContactsIdMarketingpreferencesResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;NoBody&#x60; - The request did not contain a body * &#x60;InvalidMarketingPreferencesField&#x60; - &#39;marketByTelephone&#39; is not a valid marketing preferences field * &#x60;MissingMethodOfConsent&#x60; - The methodOfConsent field is required | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `NoBody` (value: `"NoBody"`)

* `InvalidMarketingPreferencesField` (value: `"InvalidMarketingPreferencesField"`)

* `MissingMethodOfConsent` (value: `"MissingMethodOfConsent"`)




