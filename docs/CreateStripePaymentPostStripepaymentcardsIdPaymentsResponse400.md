# JuniferAuNativeApi.CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InvalidParameter&#x60; - \&quot;Payment amount must have only two decimal places.\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InvalidParameter` (value: `"InvalidParameter"`)




