# JuniferAuNativeApi.ContactsGetAccountsIdContactsResponse200Address

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**careOf** | **String** | The &#39;Care of&#39; line of the address | [optional] 
**addressType** | **String** | Internally used address type which defines number of lines and format used etc. | [optional] 
**address1** | **String** | Address 1 | [optional] 
**address2** | **String** | Address 2 | [optional] 
**address3** | **String** | Address 3 | [optional] 
**address4** | **String** | Address 4 | [optional] 
**address5** | **String** | Address 5 | [optional] 
**postcode** | **String** | Post code | [optional] 
**country** | **String** | Country name | [optional] 
**countryCode** | **String** | Country code | [optional] 


