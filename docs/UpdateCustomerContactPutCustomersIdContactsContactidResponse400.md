# JuniferAuNativeApi.UpdateCustomerContactPutCustomersIdContactsContactidResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;RequiredParameterMissing&#x60; - The supplied &#x60;surname&#x60; parameter contains no value * &#x60;UnrecognisedTitle&#x60; - The supplied &#39;title&#39; parameter &#x60;title&#x60; is not recognised | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)

* `UnrecognisedTitle` (value: `"UnrecognisedTitle"`)




