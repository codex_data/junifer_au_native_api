# JuniferAuNativeApi.CreateAccountDebitPostAccountsIdAccountdebitsResponse404

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;AccountDebitReasonNotFound&#x60; - &#39;accountDebitReasonName&#39; not recognised. Account debit reason must be a valid type * &#x60;AccountTransactionTypeNotFound&#x60; - No AccountTransactionType for AccountDebit | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `AccountDebitReasonNotFound` (value: `"AccountDebitReasonNotFound"`)

* `AccountTransactionTypeNotFound` (value: `"AccountTransactionTypeNotFound"`)




