# JuniferAuNativeApi.RenewAccountPostAccountsIdRenewalRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**electricityProductCode** | **String** | Product code for new electricity agreement | [optional] 
**electricitySupplyProductSubType** | **String** | Product sub type for new electricity agreement. Required if electricityProductCode supplied | [optional] 
**electricityStartDate** | **String** | Start date of new electricity agreement. Required if electricityProductCode supplied | [optional] 
**gasProductCode** | **String** | Product code for new gas agreement | [optional] 
**gasSupplyProductSubType** | **String** | Product sub type for new gas agreement. Required if gasProductCode supplied | [optional] 
**gasStartDate** | **String** | Start date of new gas agreement. Required if gasProductCode supplied | [optional] 
**creditExitFee** | **Boolean** | Boolean specifying if early exit fees should be refunded, this is done by adding account credit equal to the exit fee. If not supplied a default of false is used. | [optional] 


