# JuniferAuNativeApi.GetQuotesGetOffersIdQuotesResponse200MeterPointsPricesPriceContents

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate** | **Object** | Value of this class dependent rate | [optional] 
**rateName** | **Object** | Name of this class dependent rate | [optional] 
**rateUnit** | **Object** | Unit of this class dependent rate | [optional] 


