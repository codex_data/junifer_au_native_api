# JuniferAuNativeApi.CreateStripePaymentPostStripepaymentcardsIdPaymentsResponse401

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;AmountNotAuthorised&#x60; - \&quot;AmountNotAuthorised\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `AmountNotAuthorised` (value: `"AmountNotAuthorised"`)




