# JuniferAuNativeApi.TariffInformationGetAccountsIdTariffinformationResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productType** | **[Object]** | The product type. Can be \&quot;Gas\&quot; or \&quot;Electricity\&quot; | [optional] 
**productCode** | **String** | The product code. | [optional] 
**rateCount** | **String** | The number of rates. | [optional] 
**productGroup** | **String** | The product&#39;s group. | [optional] 
**agreementType** | **String** | The agreement&#39;s type. | [optional] 
**agreementMonths** | **String** | The number of months for the agreement. | [optional] 
**paymentMethod** | **String** | The payment method eg DD. | [optional] 
**accountManagementType** | **String** | The way the account is managed eg \&quot;Online\&quot; | [optional] 
**gspGroupCode** | **String** | The GSP group for the product. | [optional] 
**TIL** | [**[TariffInformationGetAccountsIdTariffinformationResponse200TIL]**](TariffInformationGetAccountsIdTariffinformationResponse200TIL.md) | The tarif information labels. | [optional] 


