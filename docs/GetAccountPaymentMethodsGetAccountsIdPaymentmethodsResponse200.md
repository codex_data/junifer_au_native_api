# JuniferAuNativeApi.GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the payment method | 
**paymentMethodType** | **String** | The type of the payment method | 
**status** | **String** | The status of the payment method. Can be one of the following: &#x60;Pending, Active, Cancelled, Failed, RequestingActivation, PendingCancellation, RequestingCancellation, FailedCancellation, Paused, CoolingOff&#x60; | 
**createdDttm** | **Date** | The date and time this payment method was created | 
**fromDttm** | **Date** | Date when the payment method becomes active | 
**toDttm** | **Date** | Date when the payment method will terminate | [optional] 
**defaultStatus** | **String** | Status indicating whether this payment method is the default one for this account. Can be one of the following: &#x60;Pending,Default,NotDefault&#x60; | 
**links** | [**GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links**](GetAccountPaymentMethodsGetAccountsIdPaymentmethodsResponse200Links.md) |  | 


