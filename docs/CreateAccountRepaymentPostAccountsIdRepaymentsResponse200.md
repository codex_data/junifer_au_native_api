# JuniferAuNativeApi.CreateAccountRepaymentPostAccountsIdRepaymentsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The id of repayment&#39;s payment request | 
**currency** | **String** | Currency ISO code | 
**amount** | **Number** | The amount of money to be repaid | 
**status** | **String** | The state in which the repayment was created. If &#x60;Pending&#x60; then no further action is needed. If &#x60;PendingAuthorisation&#x60;, then the amount supplied was higher or lower than the limit for repayments and another user will need to Authorise the repayment before it can proceed | 
**paymentMethodType** | **String** | The repayment&#39;s payment method | 
**createdDttm** | **Date** | Date and time when the repayment was created | 
**description** | **String** | Repayment description | [optional] 
**links** | [**CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links**](CreateAccountRepaymentPostAccountsIdRepaymentsResponse200Links.md) |  | 


