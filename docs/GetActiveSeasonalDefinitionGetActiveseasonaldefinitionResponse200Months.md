# JuniferAuNativeApi.GetActiveSeasonalDefinitionGetActiveseasonaldefinitionResponse200Months

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_1** | **Boolean** | Indicates if January is a high usage month | 
**_2** | **Boolean** | Indicates if February is a high usage month | 
**_3** | **Boolean** | Indicates if March is a high usage month | 
**_4** | **Boolean** | Indicates if April is a high usage month | 
**_5** | **Boolean** | Indicates if May is a high usage month | 
**_6** | **Boolean** | Indicates if June is a high usage month | 
**_7** | **Boolean** | Indicates if July is a high usage month | 
**_8** | **Boolean** | Indicates if August is a high usage month | 
**_9** | **Boolean** | Indicates if September is a high usage month | 
**_10** | **Boolean** | Indicates if October is a high usage month | 
**_11** | **Boolean** | Indicates if November is a high usage month | 
**_12** | **Boolean** | Indicates if December is a high usage month | 


