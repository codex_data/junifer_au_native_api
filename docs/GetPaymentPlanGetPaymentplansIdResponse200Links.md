# JuniferAuNativeApi.GetPaymentPlanGetPaymentplansIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this payment plan | 
**paymentMethod** | **String** | Link to account&#39;s payment method | 
**account** | **String** | Link to the account that the payment plan belongs to | 
**paymentPlanPayments** | **String** | Link to account&#39;s payment plan payments | 


