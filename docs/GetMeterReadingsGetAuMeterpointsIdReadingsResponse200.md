# JuniferAuNativeApi.GetMeterReadingsGetAuMeterpointsIdReadingsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | UtilityTimeSeriesEvent id, unless the meter read is type of &#x60;Pending&#x60; then this is the pendingMeterRead id. | 
**readingDttm** | **Date** | The date and time of the meter reading | 
**status** | **String** | The meter reading status | 
**sequenceType** | **String** | The sequence type of the meter reading | 
**source** | **String** | The source of the meter reading | 
**quality** | **String** | The quality of the meter reading | 
**cumulative** | **Number** | The cumulative value of this meter reading | [optional] 
**consumption** | **Number** | Consumption registered in this meter reading. Accepted readings only | [optional] 
**unit** | **String** | The metric unit of the meter reading. Accepted readings only | [optional] 
**receivedDttm** | **Date** | Received Dttm the date of when the system receives a read | [optional] 
**fromDttm** | **Date** | From Dttm the date of the last read. | [optional] 
**meter** | **String** | Meter identifier. Accepted readings only | [optional] 
**meterId** | **Number** | Meter id. | [optional] 
**register** | **String** | Meter register identifier to which the meter reading belongs. Accepted readings only | [optional] 
**meterRegisterId** | **Number** | Meter Register id | [optional] 
**meterMeasurementType** | **String** | Meter measurement type assigned to the meter register | [optional] 


