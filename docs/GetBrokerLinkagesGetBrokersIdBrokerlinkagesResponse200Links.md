# JuniferAuNativeApi.GetBrokerLinkagesGetBrokersIdBrokerlinkagesResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this CustomerBroker | 
**broker** | **String** | Link to this customer broker&#39;s broker | 
**customer** | **String** | Link to the customer if the linkage is for a customer | [optional] 
**prospect** | **String** | Link to the prospect if the linkage is for a prospect | [optional] 


