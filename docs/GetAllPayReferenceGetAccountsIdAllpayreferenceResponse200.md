# JuniferAuNativeApi.GetAllPayReferenceGetAccountsIdAllpayreferenceResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allPayReference** | **Number** | AllPay reference number | 


