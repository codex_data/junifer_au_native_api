# JuniferAuNativeApi.UpdateBillingAddressPutCustomersIdUpdatebillingaddressResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;Postcode&#x60; - is not a valid UK format, PostCode must be supplied capitalised * &#x60;All&#x60; - parameters of the address are optional but at least one must be populated | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `Postcode` (value: `"Postcode"`)

* `All` (value: `"All"`)




