# JuniferAuNativeApi.GetBillRequestGetBillrequestsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Id of bill request | 
**billId** | **Number** | The id of bill linked to bill request (if the associated bill has been generated) | [optional] 
**billPeriodFromDttm** | **Date** | From date time of bill period linked to bill request | 
**billPeriodToDttm** | **Date** | To date time of bill period linked to bill request | 
**statusCode** | **String** | The status code of the bill request | 
**typeCode** | **String** | The type code of the bill request | 
**taskCreatedDttm** | **Date** | The created date time of task linked to bill request | 
**taskStartDttm** | **Date** | The start date time of task linked to bill request | [optional] 
**taskFinishDttm** | **Date** | The finish date time of task linked to bill request | [optional] 
**taskUserTblUsername** | **String** | The username of the UserTbl id of the task linked to bill request | 
**serverExceptionMessage** | **String** | Server exception message of bill request if it failed | [optional] 
**testFl** | **Boolean** | Flag indicating whether the bill request is for a test bill | 
**links** | [**GetBillRequestGetBillrequestsIdResponse200Links**](GetBillRequestGetBillrequestsIdResponse200Links.md) |  | 


