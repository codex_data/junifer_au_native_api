# JuniferAuNativeApi.StopPaymentSchedulePeriodPutPaymentscheduleperiodsIdResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InvalidToDate&#x60; - toDt cannot be before &#39;fromDt&#39;. Set it to &#39;fromDt&#39; to delete the payment schedule period | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InvalidToDate` (value: `"InvalidToDate"`)




