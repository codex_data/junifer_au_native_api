# JuniferAuNativeApi.UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketByEmail** | **Boolean** | Consent has been given to receive marketing via email (true or false) | [optional] 
**marketByNumber1** | **Boolean** | Consent has been given to receive marketing via phone number type 1 (true or false) | [optional] 
**marketByNumber2** | **Boolean** | Consent has been given to receive marketing via phone number type 2 (true or false) | [optional] 
**marketByNumber3** | **Boolean** | Consent has been given to receive marketing via phone number type 3 (true or false) | [optional] 
**marketBySms** | **Boolean** | Consent has been given to receive marketing via SMS (true or false) | [optional] 
**marketByPost** | **Boolean** | Consent has been given to receive marketing via post (true or false) | [optional] 
**marketBySocialMedia1** | **Boolean** | Consent has been given to receive marketing via social media type 1 (true or false) | [optional] 
**marketBySocialMedia2** | **Boolean** | Consent has been given to receive marketing via social media type 2 (true or false) | [optional] 
**marketBySocialMedia3** | **Boolean** | Consent has been given to receive marketing via social media type 3 (true or false) | [optional] 


