# JuniferAuNativeApi.GetProductsForNmiGetAuProductsTariffsfornmiResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nmi** | **String** | Identifier of the NMI identified from the supplied details | 
**code** | **String** | Product code, also known as product reference in Junifer | 
**name** | **String** | Product name | 
**display** | **String** | Product display string | 
**availableToPublic** | **Boolean** | Flag indicating whether the product was set as publicly available in Junifer | 
**endDttm** | **Date** | The date the product is available until | 
**durationMonths** | **Number** | The duration of the product if the product is a fixed term product | [optional] 
**directDebit** | **Boolean** | A Direct Debit flag indicating whether the product is for the Direct Debit customer. | [optional] 
**prepay** | **Boolean** | A prepay flag indicating whether the product is for the prepay customer. | [optional] 
**standingCharge** | **Number** | The standing charge in cents for the product expressed as p/day. Present only if Standing Charge product item is configured for the product | [optional] 
**unitRates** | **Object** | Object holding unit rates in dollars for the product. There will be one line per unit rate on the price plan | 


