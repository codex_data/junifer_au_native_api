# JuniferAuNativeApi.BillRequestsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferBillRequestsIdGet**](BillRequestsApi.md#juniferBillRequestsIdGet) | **GET** /junifer/billRequests/{id} | Get bill request
[**juniferBillRequestsIdRestartFailedPost**](BillRequestsApi.md#juniferBillRequestsIdRestartFailedPost) | **POST** /junifer/billRequests/{id}/restartFailed | Restart failed bill request



## juniferBillRequestsIdGet

> GetBillRequestGetBillrequestsIdResponse200 juniferBillRequestsIdGet(id)

Get bill request

Get bill request record by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillRequestsApi();
let id = 3.4; // Number | BillRequest ID
apiInstance.juniferBillRequestsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| BillRequest ID | 

### Return type

[**GetBillRequestGetBillrequestsIdResponse200**](GetBillRequestGetBillrequestsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferBillRequestsIdRestartFailedPost

> juniferBillRequestsIdRestartFailedPost(id)

Restart failed bill request

Restart failed bill request - bill request&#39;s status code must not be Completed or Superseded

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.BillRequestsApi();
let id = 3.4; // Number | BillRequest ID
apiInstance.juniferBillRequestsIdRestartFailedPost(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| BillRequest ID | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

