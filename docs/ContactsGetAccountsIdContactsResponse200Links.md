# JuniferAuNativeApi.ContactsGetAccountsIdContactsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link to this contact | 


