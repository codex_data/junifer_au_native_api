# JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountId** | **Number** | Newly created account ID | 


