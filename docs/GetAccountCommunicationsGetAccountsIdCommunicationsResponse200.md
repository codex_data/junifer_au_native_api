# JuniferAuNativeApi.GetAccountCommunicationsGetAccountsIdCommunicationsResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the communication | 
**type** | **String** | the type of communication | 
**status** | **String** | the status of the communication | 
**createdDttm** | **Date** | date and time that the communication was sent | 
**testFl** | **Boolean** | Is this communication a test (never sent to the customer) | 
**files** | [**[GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files]**](GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Files.md) | the communication files | 
**links** | **String** |  | 


