# JuniferAuNativeApi.CreateAccountRepaymentPostAccountsIdRepaymentsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InvalidAmount&#x60; - Repayment amount must be greater than 0 * &#x60;NoDefaultPaymentMethod&#x60; - This request is only valid for accounts with a valid default payment method * &#x60;CantUseDefaultPaymentMethod&#x60; - This account&#39;s default payment method cannot be used for repayments | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InvalidAmount` (value: `"InvalidAmount"`)

* `NoDefaultPaymentMethod` (value: `"NoDefaultPaymentMethod"`)

* `CantUseDefaultPaymentMethod` (value: `"CantUseDefaultPaymentMethod"`)




