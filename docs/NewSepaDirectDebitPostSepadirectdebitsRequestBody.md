# JuniferAuNativeApi.NewSepaDirectDebitPostSepadirectdebitsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountName** | **String** | Account name | 
**iban** | **String** | IBAN with no spaces | 


