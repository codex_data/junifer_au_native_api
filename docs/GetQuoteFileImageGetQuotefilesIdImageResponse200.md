# JuniferAuNativeApi.GetQuoteFileImageGetQuotefilesIdImageResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **String** | Content will be sent back as application/octet-stream | 


