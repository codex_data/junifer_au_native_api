# JuniferAuNativeApi.UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketingMaterials** | **[Object]** | Array of marketing material names (see MarketingMaterial ref table for possible values) | [optional] 
**marketingPreferences** | [**UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences**](UpdateMarketingPreferencesPutContactsIdMarketingpreferencesRequestBodyMarketingPreferences.md) |  | [optional] 
**methodOfConsent** | **String** | How the contact&#39;s marketing preferences were requested (\&quot;Phone\&quot;, \&quot;Email\&quot;, \&quot;Letter\&quot;, or \&quot;Portal\&quot;) | 


