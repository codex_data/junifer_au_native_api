# JuniferAuNativeApi.BillEmailsGetBillemailsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Bill ID (not email id) | 
**deliveryStatus** | **String** | Communication email delivery status. The status will be one of the following: \&quot;Draft\&quot;, \&quot;Sending\&quot;, \&quot;Sent\&quot;, \&quot;Failed\&quot;, \&quot;Retrying\&quot;, \&quot;Cancelled\&quot;. | [optional] 
**subject** | **String** | Email subject | 
**createdDttm** | **Date** | Email creation date | 
**body** | **String** | Email body | 
**files** | [**[BillEmailsGetBillemailsIdResponse200Files]**](BillEmailsGetBillemailsIdResponse200Files.md) | Email attachments | 
**links** | [**BillEmailsGetBillemailsIdResponse200Links1**](BillEmailsGetBillemailsIdResponse200Links1.md) |  | 


