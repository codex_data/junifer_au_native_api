# JuniferAuNativeApi.NewWestpacDirectDebitPostAuWestpacdirectdebitsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;AccountNotFound&#x60; - \&quot;Account with such &#39;account.id&#39; does not exist\&quot; * &#x60;RequiredParameterMissing&#x60; - \&quot;The required &#39;mandateReference&#39; field is missing\&quot; * &#x60;InvalidParameter&#x60; - \&quot;The type field &#39;paymentType&#39; is not a valid note type\&quot; * &#x60;InvalidBankAccountDetails&#x60; - \&quot;The &#39;bsbNumber&#39; must be provided if &#39;mandateReference&#39; is not specified\&quot; * &#x60;BankAccountNumberExceedsLength&#x60; - \&quot;The length of the &#39;bankAccountNumber&#39; parameter exceeds length of 17 characters\&quot; * &#x60;AccountNameExceedsLength&#x60; - \&quot;The length of the &#39;accountName&#39; parameter exceeds length of 22 characters\&quot; * &#x60;CustomerNumberExceedsLength&#x60; - \&quot;The length of the &#39;customerNumber&#39; parameter exceeds length of 15 characters\&quot; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `AccountNotFound` (value: `"AccountNotFound"`)

* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)

* `InvalidParameter` (value: `"InvalidParameter"`)

* `InvalidBankAccountDetails` (value: `"InvalidBankAccountDetails"`)

* `BankAccountNumberExceedsLength` (value: `"BankAccountNumberExceedsLength"`)

* `AccountNameExceedsLength` (value: `"AccountNameExceedsLength"`)

* `CustomerNumberExceedsLength` (value: `"CustomerNumberExceedsLength"`)




