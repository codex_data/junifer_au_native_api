# JuniferAuNativeApi.GetMeterStructureGetAuMeterpointsMeterstructureResponse200Meters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Meter ID | 
**identifier** | **String** | Meter identifier | 
**meterType** | **String** | Meter type code. A full list of possible values can be found in the Meter Type screen. | [optional] 
**meterTypeDescription** | **String** | Meter type description. | [optional] 
**registers** | [**[GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers]**](GetMeterStructureGetAuMeterpointsMeterstructureResponse200Registers.md) | Array with meter&#39;s registers | 


