# JuniferAuNativeApi.GetCustomerGetCustomersIdResponse200CompanyAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressType** | **String** | Internally used address type which defines number of lines and format used etc. | [optional] 
**address1** | **String** | First address line | [optional] 
**address2** | **String** | Second address line | [optional] 
**address3** | **String** | Third address line | [optional] 
**address4** | **String** | Fourth address line | [optional] 
**address5** | **String** | Fifth address line | [optional] 
**postcode** | **String** | Post code | [optional] 
**countryCode** | **String** | Country code | [optional] 
**country** | **String** | Country name | [optional] 


