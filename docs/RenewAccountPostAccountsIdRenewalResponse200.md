# JuniferAuNativeApi.RenewAccountPostAccountsIdRenewalResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountNumber** | **String** | Account Number | 
**elecProductBundleId** | **String** | ID of the newly generated electricity product bundle | 
**gasProductBundleId** | **String** | ID of the newly generated gas product bundle | 


