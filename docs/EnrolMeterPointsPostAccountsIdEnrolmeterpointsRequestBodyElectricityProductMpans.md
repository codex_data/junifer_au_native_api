# JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyElectricityProductMpans

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mpanCore** | **String** | MPAN core of the MPAN | 
**measurementType** | **String** | Measurement type of the MPAN. Accepted values &#x60;Import, Export&#x60;. Defaults to Import(Request Parameter) | [optional] 


