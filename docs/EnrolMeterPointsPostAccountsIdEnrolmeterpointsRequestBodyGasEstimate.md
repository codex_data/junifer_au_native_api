# JuniferAuNativeApi.EnrolMeterPointsPostAccountsIdEnrolmeterpointsRequestBodyGasEstimate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**estimatedCost** | **Number** | Estimated cost of the new Gas Tariff | [optional] 
**estimatedSaving** | **Number** | Estimated saving of the new Gas Tariff | [optional] 
**consumption** | **Number** | Customer-provided estimation of their current gas consumption | [optional] 
**previousCost** | **Number** | Customer-provided estimation of their current gas spend | [optional] 
**period** | **String** | The period for the estimated gas values. Must be &#x60;Month&#x60; or &#x60;Year&#x60;(Request Parameter) | 


