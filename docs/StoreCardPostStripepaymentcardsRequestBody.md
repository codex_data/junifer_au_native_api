# JuniferAuNativeApi.StoreCardPostStripepaymentcardsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last4** | **String** | (deprecated) Last 4 digits of the card number | [optional] 
**expMonth** | **String** | (deprecated) Cards expiry month | [optional] 
**expYear** | **String** | (deprecated) Cards expiry year | [optional] 
**stripeCusId** | **String** | The stripe customer id string | 
**stripePaymentMethodId** | **String** | The stripe payment method id string | [optional] 


