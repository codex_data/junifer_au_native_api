# JuniferAuNativeApi.GetCommunicationsFileImageGetCommunicationsfilesIdImageResponse410

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;Gone&#x60; - File may have been removed by GDPR erasure request | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `Gone` (value: `"Gone"`)




