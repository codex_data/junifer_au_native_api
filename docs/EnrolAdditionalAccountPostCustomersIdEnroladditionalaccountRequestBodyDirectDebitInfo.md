# JuniferAuNativeApi.EnrolAdditionalAccountPostCustomersIdEnroladditionalaccountRequestBodyDirectDebitInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bankAccountSortCode** | **String** | Customer&#39;s bank&#39;s sort code | 
**bankAccountNumber** | **String** | Customer&#39;s bank&#39;s account number | 
**bankAccountName** | **String** | Customer&#39;s bank&#39;s account name | 
**regularPaymentAmount** | **Number** | The amount in pounds which customer selected to pay by Regular (Fixed) Direct Debit | [optional] 
**regularPaymentDayOfMonth** | **Number** | Day of the month when the Regular (Fixed) Direct Debit amount should be taken from the customer&#39;s bank account. Must be a value between 1-28 inclusive. | [optional] 
**seasonalPaymentFl** | **Boolean** | A boolean flag indicating whether the customer wants to pay a variable amount during the high and low seasons, defined by the active Payment Schedule Seasonal Definition(Request Parameter) | [optional] 


