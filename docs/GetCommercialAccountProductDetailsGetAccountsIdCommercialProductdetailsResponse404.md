# JuniferAuNativeApi.GetCommercialAccountProductDetailsGetAccountsIdCommercialProductdetailsResponse404

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;NoFound&#x60; - Could not find account with Id &#39;:accountID&#39; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `NoFound` (value: `"NoFound"`)




