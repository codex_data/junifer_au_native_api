# JuniferAuNativeApi.GetAccountCreditNotesGetAccountsIdCreditsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this credit | 
**account** | **String** | Link to the credit account | 


