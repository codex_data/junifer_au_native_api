# JuniferAuNativeApi.UpdateCustomerConsentsPutCustomersIdConsentsRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consentDefinition** | **String** | Consent definition. Can be &#x60;Dcc Daily Read, Dcc Half Hourly Read&#x60; | 
**setting** | **Boolean** | true or false. Can be &#x60;true, false&#x60; | 
**fromDt** | **Date** | Consent from date. Example: &#x60;2019-02-14&#x60; | 
**toDt** | **Date** | Consent to date, defaults to null date (meaning there is no end date). Example: &#x60;2021-02-14&#x60; | [optional] 


