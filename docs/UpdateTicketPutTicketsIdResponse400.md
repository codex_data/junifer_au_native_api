# JuniferAuNativeApi.UpdateTicketPutTicketsIdResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;NotFound&#x60; - Could not find ticket with Id &#39;1&#39; * &#x60;RequiredParameterMissing&#x60; - The required &#39;ticketStepCode&#39; parameter is missing | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `NotFound` (value: `"NotFound"`)

* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)




