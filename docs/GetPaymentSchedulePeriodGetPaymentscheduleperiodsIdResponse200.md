# JuniferAuNativeApi.GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The ID of the payment schedule period | 
**fromDt** | **Date** | Date when the payment schedule period becomes active | 
**toDt** | **Date** | Date when the payment schedule period terminates | [optional] 
**frequencyMultiple** | **Number** | Frequency multiplier | 
**frequency** | **String** | The frequency of payments | 
**frequencyAlignmentDt** | **Date** | Payment schedule alignment/pivot date | 
**nextPaymentDt** | **Date** | Date when the next payment is schedule to be collected | 
**seasonalPaymentFl** | **Boolean** | if the payment amount varies by season | 
**highPercentage** | **Number** | The percentage increase of direct debit amount during the high usage months (specified in the seasonal definition associated with the payment schedule) | [optional] 
**lowPercentage** | **Number** | The percentage decrease of direct debit amount during the low usage months (specified in the seasonal definition associated with the payment schedule) | [optional] 
**amount** | **Number** | The amount each payment collection will try to collect. Important: If a seasonal definition is present for the payment schedulethen the amount returned will be adjusted accordingly (if the payment at nextPaymentDt is in the high season then the returned amount will be the base amount increased by highPercentage, if the payment is in the low seasonthen the returned amount will be the base amount decreased by lowPercentage). | 
**createdDttm** | **Date** | Date and time when this payment schedule period was created | 
**debtRecoveryDetails** | [**[GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails]**](GetPaymentSchedulePeriodGetPaymentscheduleperiodsIdResponse200DebtRecoveryDetails.md) | debt recovery details | [optional] 
**links** | [**GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links**](GetAccountPaymentSchedulePeriodsGetAccountsIdPaymentscheduleperiodsResponse200Links.md) |  | 


