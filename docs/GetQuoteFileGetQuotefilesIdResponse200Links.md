# JuniferAuNativeApi.GetQuoteFileGetQuotefilesIdResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | Link referring to this quote file | 
**image** | **String** | Link to the file image (usually PDF) | 


