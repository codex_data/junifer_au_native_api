# JuniferAuNativeApi.CreateAccountReviewPeriodPostAccountsIdAccountreviewperiodsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;RequiredParameterMissing&#x60; - The required &#39;fromDt&#39; field is missing * &#x60;InvalidInputParameter&#x60; - The &#39;fromDttm&#39; is not before the &#39;toDttm&#39; | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `RequiredParameterMissing` (value: `"RequiredParameterMissing"`)

* `InvalidInputParameter` (value: `"InvalidInputParameter"`)




