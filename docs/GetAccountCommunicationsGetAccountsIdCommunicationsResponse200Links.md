# JuniferAuNativeApi.GetAccountCommunicationsGetAccountsIdCommunicationsResponse200Links

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | **String** | link to the file message | 
**image** | **String** | link to the file image (usually PDF) | 


