# JuniferAuNativeApi.GetAllPayReferenceGetAccountsIdAllpayreferenceResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InternalError&#x60; - No payment reference has been found for the account X with reference type Y | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InternalError` (value: `"InternalError"`)




