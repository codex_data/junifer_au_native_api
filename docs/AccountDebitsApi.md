# JuniferAuNativeApi.AccountDebitsApi

All URIs are relative to *https://api-au.integration.gentrack.cloud/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**juniferAccountDebitsIdDelete**](AccountDebitsApi.md#juniferAccountDebitsIdDelete) | **DELETE** /junifer/accountDebits/{id} | Cancel account debit
[**juniferAccountDebitsIdGet**](AccountDebitsApi.md#juniferAccountDebitsIdGet) | **GET** /junifer/accountDebits/{id} | Get Account Debit



## juniferAccountDebitsIdDelete

> juniferAccountDebitsIdDelete(id)

Cancel account debit

Cancel an existing account debit for an account. This action is not reversible. Authentication must be active

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountDebitsApi();
let id = 3.4; // Number | Account debit ID number
apiInstance.juniferAccountDebitsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account debit ID number | 

### Return type

null (empty response body)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## juniferAccountDebitsIdGet

> GetAccountDebitGetAccountdebitsIdResponse200 juniferAccountDebitsIdGet(id)

Get Account Debit

Gets an account debit by its ID

### Example

```javascript
import JuniferAuNativeApi from 'junifer_au_native_api';
let defaultClient = JuniferAuNativeApi.ApiClient.instance;
// Configure API key authorization: tokenAuth
let tokenAuth = defaultClient.authentications['tokenAuth'];
tokenAuth.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//tokenAuth.apiKeyPrefix = 'Token';

let apiInstance = new JuniferAuNativeApi.AccountDebitsApi();
let id = 3.4; // Number | Account Debit ID
apiInstance.juniferAccountDebitsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Account Debit ID | 

### Return type

[**GetAccountDebitGetAccountdebitsIdResponse200**](GetAccountDebitGetAccountdebitsIdResponse200.md)

### Authorization

[tokenAuth](../README.md#tokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

