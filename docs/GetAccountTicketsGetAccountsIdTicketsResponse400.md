# JuniferAuNativeApi.GetAccountTicketsGetAccountsIdTicketsResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;TicketStatusNotRecognised&#x60; - Ticket status &#39;ticketStatus&#39; not recognised | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `TicketStatusNotRecognised` (value: `"TicketStatusNotRecognised"`)




