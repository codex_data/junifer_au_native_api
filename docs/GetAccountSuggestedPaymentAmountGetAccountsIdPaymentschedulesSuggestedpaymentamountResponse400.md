# JuniferAuNativeApi.GetAccountSuggestedPaymentAmountGetAccountsIdPaymentschedulesSuggestedpaymentamountResponse400

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Field: * &#x60;InvalidFrequency&#x60; - Unrecognised value in &#39;frequency&#39; field * &#x60;InvalidFrequencyMultiple&#x60; - Frequency multiple is not a positive integer * &#x60;InternalError&#x60; - Annual projection for account is less than or equal to zero | 
**errorDescription** | **String** | The error description | [optional] 
**errorSeverity** | **String** | The error severity | 



## Enum: ErrorCodeEnum


* `InvalidFrequency` (value: `"InvalidFrequency"`)

* `InvalidFrequencyMultiple` (value: `"InvalidFrequencyMultiple"`)

* `InternalError` (value: `"InternalError"`)




